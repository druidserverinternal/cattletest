package weixin

import (
	"net/http"
	"time"

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/admin/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

// GetToken get weixin token
func GetToken(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	weixin, err := models.FindWeixinByCompanyID(db, user.CompanyID)
	if err != nil {
		logger.Warning("Find weixin by companyID failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find weixin by companyID failed:", err))
		return
	}
	if weixin == nil {
		logger.Warning("Your company not support weixin.", err)
		c.AbortWithError(http.StatusNotFound, models.Error("Your company not support weixin."))
		return
	}

	switch weixin.IsExpiry() {
	case true:
		t := time.Now().Add(models.WaitTokenRefreshSecond * time.Second)
		if weixin, err = models.RefreshWeixinToken(db, weixin.CompanyID, &t); err != nil {
			logger.Warning("Refresh token failed.", err)
			c.AbortWithError(http.StatusInternalServerError, models.Error("Refresh token failed.", err))
			return
		}
	}

	wt := models.WeixinToken {
		AccessToken	:	weixin.AccessToken,
		AppID	:	weixin.AppID,
	}

	c.JSON(http.StatusOK, &wt)
}

