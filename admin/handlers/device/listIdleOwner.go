package device

import (
	"net/http"
	"strconv"
//	

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/admin/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

// ListIdleOwner list devices without owner
func ListIdleOwner(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	rp := models.ParseRequestParameter(c)

	m := bson.M{
		"company_id": user.CompanyID,
		"deleted_at": nil,
		"owner_id": nil,
	}

	result, err := models.ListDeviceByFilter(db, m, rp, models.UserSelect)
	if err != nil {
		logger.Error("List device by filter failed.", err, m)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List device by filter failed.", err))
		return
	}


	count := 0
	if len(result) > 0 {
		count, err = models.CountDeviceByFilter(db, m)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, result)
}

