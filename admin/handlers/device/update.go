package device

import (
	"encoding/json"
	"net/http"
	
	//	"regexp"

	//	"github.com/spf13/viper"

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/admin/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

func updateValidate(c *gin.Context) (*models.Device) {
	device := models.Device{}
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&device); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	deviceID := c.Param("id")
	if !bson.IsObjectIdHex(deviceID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("device id is invalid:" + deviceID))
		return nil
	}
	device.ID = bson.ObjectIdHex(deviceID)

	return &device
}

// Update a devices
func Update(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	device := updateValidate(c)
	if device == nil {
		return
	}

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	d, err := models.CheckValidDeviceByID(db, device.ID, nil)
	if err != nil {
		logger.Error("Could not find device:", device.ID)
		c.AbortWithError(http.StatusNotFound, models.Error("Could not find device:"+device.ID.Hex()))
		return
	}
	if d.CompanyID != user.CompanyID {
		logger.Warning("You don't have permission to access")
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access"))
		return
	}

	if err = device.UpdateDescription(db); err != nil {
		c.AbortWithError(http.StatusInternalServerError, models.Error("Update device description failed:" + err.Error()))
		return
	}

	c.Status(http.StatusCreated)
}
