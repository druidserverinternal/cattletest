package device

import (
	"net/http"
	"strconv"
	

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/admin/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

// Search devices
func Search(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	sn := c.Param("sn")
	if !models.RegexpMark.MatchString(sn) {
		logger.Error("The sn is invalid.", sn)
		c.AbortWithError(http.StatusBadRequest, models.Error("The sn is invalid." + sn))
		return
	}


	m := bson.M{}
	m["company_id"] = user.CompanyID
	m["mark"] = bson.M{"$ne": nil}
	reg := "/.*" + sn + ".*/.test(this.mark)"
	m["$where"] = reg
	//  db.device.find({mark: {$ne: null},  $where: "/.*12.*/.test(obj.mark)" })

	logger.Info("Search fileter:", m)

	device, err := models.SearchDevice(db, m, models.ParseRequestParameter(c), models.UserSelect)
	if err != nil {
		logger.Error("Can't find device.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Can't find device." + err.Error()))
		return
	}

	count := 0
	if len(device) > 0 {
		count, _ = models.SearchDeviceCount(db, m)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, device)
}


