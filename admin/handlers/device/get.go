package device

import (
	//	"encoding/json"
	"net/http"
	
	//	"regexp"
	//	"strings"

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/admin/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

// Get a device by id
func Get(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	deviceID := c.Param("id")
	if !bson.IsObjectIdHex(deviceID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("DeviceId is invalid:" + deviceID))
		return
	}

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	device, err := models.FindDeviceMergeByID(db, bson.ObjectIdHex(deviceID), models.UserSelect)
	if err != nil {
		logger.Warning("Find device by ID failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find device by ID failed:", err))
		return
	}
	if device.CompanyID != user.CompanyID {
		logger.Error("You don't have permission to access this device.")
		c.AbortWithError(http.StatusForbidden, err)
		return
	}

	c.JSON(http.StatusOK, device)
}
