package device

import (
	"net/http"
	
	//	"regexp"
	//	"strings"

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/admin/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

// ListOwnerExclude list all devices by except post
func ListOwnerExclude(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	devices := listManyValidate(c)
	if devices == nil {
		return
	}

	userID := c.Param("id")
	if !bson.IsObjectIdHex(userID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("UserId is invalid:", userID))
		return
	}

	user := models.FindUserByID(db, bson.ObjectIdHex(userID))
	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user == nil {
		logger.Error("Could not found user.", userID)
		c.AbortWithError(http.StatusBadRequest, models.Error("Could not found user.", userID))
		return
	}
	if user.CompanyID != me.CompanyID {
		logger.Error("You don't have permission to access.")
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access."))
		return
	}

	m := bson.M{
		"owner_id": user.ID,
		"_id": bson.M{"$nin": devices.ID },
	}

	ds, err := models.ListDeviceMergeByFilter(db, m, nil, models.UserSelect)
	if err != nil {
		logger.Error("Find device by ID failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find device by ID failed:", err))
		return
	}

	c.JSON(http.StatusOK, ds)
}
