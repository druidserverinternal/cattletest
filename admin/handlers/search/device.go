package search

import (
	"encoding/json"
	"strconv"
	"net/http"
	

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/admin/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

func deviceValidate(c *gin.Context) (*models.Search) {
	search := models.Search{}

	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&search); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	return &search
}

// Device search
func Device(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	search := deviceValidate(c)
	if search == nil {
		return
	}

	logger.Infof("Search struct: %+v\n", search)
	m := search.Bson()
	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	m["company_id"] = user.CompanyID


	logger.Info("Search fileter:", m)

	device, err := models.SearchDevice(db, m, models.ParseRequestParameter(c), models.UserSelect)
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, models.Error("Can't find device." + err.Error()))
		return
	}

	count := 0
	if len(device) > 0 {
		count, _ = models.SearchDeviceCount(db, m)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, device)
}
