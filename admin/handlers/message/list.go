package message

import (
//	"druid/cattle/admin/handlers/common"
	"druid/cattle/admin/middleware"
	"druid/cattle/models"
//	"fmt"
	"net/http"
	"strconv"

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"
)


func List(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	userID := bson.ObjectIdHex(middleware.GetUserID(c))
	me := models.FindUserByID(db, userID)

	m := bson.M{
		"dst": me.ID,
	}
	rp := models.ParseRequestParameter(c)
	if len(rp.Sort) == 0 {
		rp.Sort = []string{"-timestamp"}
	}
	msg, err := models.ListMessageByFilter(db, m, rp)
	if err != nil {
		logger.Error("List message by filter failed.", err)
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	count := 0
	if len(msg) > 0 {
		count, err = models.CountMessageByFilter(db, m)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, msg)
}
