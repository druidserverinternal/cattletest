package message

import (
//	"druid/cattle/admin/handlers/common"
	"druid/cattle/admin/middleware"
	"druid/cattle/models"
//	"fmt"
//	"time"
	"net/http"

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"
)


func Put(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	userID := bson.ObjectIdHex(middleware.GetUserID(c))
	//me := models.FindUserByID(db, userID)

	var mm models.MessageMany
	if err := c.BindJSON(&mm); err != nil {
		logger.Error("Parse request failed, ", err)
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	msgs, err := models.ListMessageByFilter(db, bson.M{"_id": bson.M{"$in": mm.IDs}, "dst": userID}, nil)
	if err != nil {
		logger.Error("List message by filter failed.", err)
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	if len(mm.IDs) != len(msgs) {
		logger.Error("Request message ids are invalid.")
		c.AbortWithError(http.StatusBadRequest, models.Error("Request message ids are invalid."))
		return
	}
	if err := mm.UpdateRead(db); err != nil {
		logger.Error("Update message read failed.", err)
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	c.Status(http.StatusCreated)
}

