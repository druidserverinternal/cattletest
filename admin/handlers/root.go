package handlers

import (
	"druid/cattle/admin/middleware"

	"druid/cattle/admin/handlers/company"
	"druid/cattle/admin/handlers/device"
	"druid/cattle/admin/handlers/setting"
	"druid/cattle/admin/handlers/gps"
	"druid/cattle/admin/handlers/user"
	"druid/cattle/admin/handlers/search"
	"druid/cattle/admin/handlers/file"
	"druid/cattle/admin/handlers/ditu"
	"druid/cattle/admin/handlers/message"
	"druid/cattle/admin/handlers/biological/cattle"

	"github.com/gin-gonic/gin"
//	"github.com/gin-contrib/gzip"
	"github.com/itsjamie/gin-cors"

	"time"
)

// RootMux is the root handler for the cattle server
var RootMux = gin.New()

func init() {
	RootMux.Use(gin.Logger())
//	RootMux.Use(gzip.Gzip(gzip.BestSpeed))

	RootMux.Use(middleware.RequestID())
	RootMux.Use(middleware.Logger())
	RootMux.Use(gin.Recovery())
	RootMux.Use(middleware.DBConnector())
	RootMux.Use(middleware.PushConnection())
//	RootMux.Use(middleware.Etcd())

	RootMux.Use(cors.Middleware(cors.Config{
		Origins:        "*",
		Methods:        "GET, PUT, POST, DELETE",
		RequestHeaders: "Origin, X-Druid-Authentication, Content-Type, X-Result-Limit, X-Result-Offset, X-Result-Sort, Accept-Language, Accept, X-Result-Compress",
		ExposedHeaders: "X-Druid-Authentication, X-Result-Count, Content-Length, Content-Type, Content-Disposition",
		MaxAge:		time.Hour * 24 * 7,
		Credentials: true,
		ValidateHeaders: false,
	}))

//	store := persistence.NewInMemoryStore(time.Hour * 24 * 7)

	v1Mux := RootMux.Group("/v1")
	{
		{
			v1Mux.POST("/login", user.Login)
		}

		v1Mux.Use(middleware.Authenticator())

		userMux := v1Mux.Group("/user")
		{
			userMux.POST("/", user.Create)

			userMux.PUT("/id/:id/adddevice", user.AddDevice)
			userMux.PUT("/id/:id/deldevice", user.DelDevice)
			userMux.PUT("/password", user.UpdatePassword)
			userMux.PUT("/info", user.UpdateInfo)
			userMux.PUT("/profile", user.UpdateProfile)
			userMux.PUT("/profilepush", user.UpdateProfilePush)

			userMux.PUT("/password/:id", user.UpdateOtherPassword)
			userMux.PUT("/info/:id", user.UpdateOtherInfo)
			userMux.PUT("/profile/:id", user.UpdateOtherProfile)

			userMux.DELETE("/id/:id", user.Delete)

			userMux.GET("/", user.List)
			userMux.GET("/myself", user.Me)
			userMux.GET("/id/:id", user.Get)
			userMux.GET("/search/:key", user.Search)
		}

		companyMux := v1Mux.Group("/company")
		{
			companyMux.GET("/", company.Get)
		}

		deviceMux := v1Mux.Group("/device")
		{
			deviceMux.GET("/", device.List)
//			deviceMux.GET("/user/:id/nocattle", device.ListNoCamel)
			deviceMux.GET("/user/:id", device.ListOwner)
//			deviceMux.GET("/uuid/:id", device.GetByUUID)
			deviceMux.GET("/id/:id", device.Get)
			deviceMux.GET("/id/:id/area", device.ListDituArea)
			deviceMux.GET("/search/mark/:sn", device.Search)
			deviceMux.GET("/search/sn/:sn", device.SearchSN)

			deviceMux.GET("/idle/owner", device.ListIdleOwner)	// for admin

			deviceMux.POST("/many", device.ListMany)
			deviceMux.POST("/user/:id/exclude", device.ListOwnerExclude)

			deviceMux.PUT("/id/:id", device.Update)				// update device description
		}
		gpsMux := v1Mux.Group("/gps")
		{
			//		gpsMux.GET("/", gps.List)
			gpsMux.GET("/device/:id", gps.Get)
			gpsMux.GET("/device/:id/line", gps.GetLine)
			//			gpsMux.GET("/device/:id/map", gps.Map)
		}

		settingMux := v1Mux.Group("/setting")
		{
			settingMux.GET("/device/:id", setting.Get)
			settingMux.GET("/", setting.List)

			settingMux.PUT("/device/:id", setting.Update)
			settingMux.PUT("/many", setting.UpdateMany)
		}

		searchMux := v1Mux.Group("/search")
		{
			searchMux.POST("/device", search.Device)
		}

		dituMux := v1Mux.Group("/ditu")
		{
			dituMux.POST("/user/:id", ditu.Create)

			dituMux.PUT("/area/:id/adddevice/:device", ditu.AddDevice)
			dituMux.PUT("/area/:id/deldevice/:device", ditu.DelDevice)
//			dituMux.PUT("/area/:id/adddevice", ditu.AddDevice)
//			dituMux.PUT("/area/:id/deldevice", ditu.DelDevice)
			dituMux.PUT("/area/:id", ditu.Update)

			dituMux.DELETE("/area/:id", ditu.Delete)

			dituMux.GET("/area/:id/device", ditu.GetDevice)
//			dituMux.GET("/area", ditu.List)
			dituMux.GET("/area/:id", ditu.Get)
			dituMux.GET("/user/:id", ditu.ListUser)		// list user's area
		}

		biologicalMux := v1Mux.Group("/biological")
		{
			cattleMux := biologicalMux.Group("/cattle")
			{
				cattleMux.POST("/many", cattle.ListMany)

				cattleMux.DELETE("/device/:id/image/:image_id", cattle.DeleteImage)

				cattleMux.GET("/", cattle.List)
				cattleMux.GET("/device/:id", cattle.GetByDevice)
				cattleMux.GET("/user/:id", cattle.ListUser)

				cattleMux.PUT("/device/:id", cattle.Update)
				//		cattleMux.PUT("/id/:id/imageb/:name", cattle.UploadImageB)	// for base64
				cattleMux.PUT("/device/:id/image/:name", cattle.UploadImage)		//id is device id
			}
		}

		fileMux := v1Mux.Group("/file")
		{
			fileMux.GET("/id/:id", file.Download)
			//			fileMux.GET("/:id/image", file.DownloadImage)

			fileMux.GET("/id/:id/thumbnail", file.DownloadThumbnail)
		}

		messageMux := v1Mux.Group("/message")
		{
			messageMux.GET("/", message.List)
			messageMux.GET("/unread", message.Unread)


		//	messageMux.POST("/", message.Post)

			messageMux.PUT("/", message.Put)

			messageMux.DELETE("/id/:id", message.Delete)
			messageMux.PUT("/delete", message.DeleteMany)
		}

	}
}


