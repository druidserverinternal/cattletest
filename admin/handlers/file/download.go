package file

import (
	"druid/cattle/admin/middleware"
	"druid/cattle/models"

	//"strconv"
	"net/http"

	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

func Download(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	id := c.Param("id")
	if !bson.IsObjectIdHex(id) {
		logger.Error("Camel id is invalid.")
		c.AbortWithError(http.StatusBadRequest, models.Error("Camel id is invalid:", id))
		return
	}

	image, err := models.FindImageByID(db, bson.ObjectIdHex(id))
	if err != nil {
		logger.Error("Find image from request id failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find image from request id failed, ", err))
		return
	}
	if image == nil {
		if err := models.RemoveImageByImageID(db, bson.ObjectIdHex(id)); err != nil {
			logger.Error("Remove image from biological failed.", err)
			c.AbortWithError(http.StatusInternalServerError, models.Error("Remove image from biological failed.", err))
			return
		}
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	user := models.FindUserByID(db, image.OwnerID)
	if user == nil {
		logger.Warning("Find images owner failed:.")
		c.AbortWithError(http.StatusBadRequest, models.Error("Find images owner failed:."))
		return
	}
	if user.CompanyID != me.CompanyID {
		logger.Warning("You don't have permission to access.")
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access."))
		return
	}

	file, err := models.ReadFile(db, image.Point)
	if err != nil {
		logger.Error("Read file from request failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Read file from request failed, ", err))
		return
	}

	logger.Info("Start download file. length:", file.Size, len(file.Data))

//	ct := models.GetContentTypeByName(file.Name)
//	c.Header("Content-Type", ct)
//	c.Header("Content-Length", strconv.Itoa(file.Size))
	c.Header("Content-Disposition", "attachment; filename=" + image.Name)


//	c.Abort()
	c.Data(http.StatusOK, http.DetectContentType(file.Data), file.Data)
}
