package cattle

import (
	"druid/cattle/admin/middleware"
	"druid/cattle/models"

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"
	//	"io/ioutil"
	"net/http"
	//	"strconv"
	//	"strings"
)

// DeleteImage upload image to biological
func DeleteImage(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	imageID := c.Param("image_id")
	if !bson.IsObjectIdHex(imageID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("Image id is invalid:", imageID))
		return
	}

	cattleID := c.Param("id")
	if !bson.IsObjectIdHex(cattleID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("Cattle id is invalid:", cattleID))
		return
	}

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	cattle, err := models.FindCattleByDeviceID(db, bson.ObjectIdHex(cattleID))
	if err != nil {
		logger.Warning("Find cattle by device id failed.", err)
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	if cattle == nil {
		logger.Warning("Can't find cattle.")
		c.AbortWithError(http.StatusNotFound, models.Error("Can't find cattle."))
		return
	}

	if me.Role == models.RoleUser && cattle.OwnerID != me.ID {
		logger.Warning("You don't have permission to access.")
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access."))
		return
	}

	image, err := models.FindImageByID(db, bson.ObjectIdHex(imageID))
	if err != nil {
		logger.Error("Find image from request id failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find image from request id failed, ", err))
		return
	}
	if image != nil {
		if err := image.Delete(db); err != nil {
			logger.Error("Remove image obj from db failed.", err, image)
			c.AbortWithError(http.StatusInternalServerError, models.Error("Remove image obj from db failed.", err))
			return
		}
	}

	if err := cattle.RemoveImage(db, bson.ObjectIdHex(imageID)); err != nil {
		logger.Error("Remove image from biological failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Remove image from biological failed.", err))
		return
	}

	c.Status(http.StatusNoContent)
}
