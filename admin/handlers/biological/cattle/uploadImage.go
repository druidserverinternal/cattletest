package cattle

import (
	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	

	"gopkg.in/mgo.v2/bson"

	"github.com/gin-gonic/gin"
)

func uploadValidate(c *gin.Context) (*models.Image, bson.ObjectId) {
	image := &models.Image{}

	fileName := c.Param("name")
	fileName = strings.TrimSpace(fileName)
	if fileName == "" {
		c.AbortWithError(http.StatusBadRequest, models.Error("FileName is invalid."))
		return nil, ""
	}
	fn := strings.Split(fileName, ".")
	if len(fn) < 2 {
		c.AbortWithError(http.StatusBadRequest, models.Error("Filename is invalid."))
		return nil, ""
	}
	extName := strings.ToLower(fn[len(fn) - 1])
	if extName != "jpg" && extName != "jpeg" && extName != "png" {
		c.AbortWithError(http.StatusBadRequest, models.Error("File format is invalid."))
		return nil, ""
	}

	id := c.Param("id")
	if !bson.IsObjectIdHex(id) {
		c.AbortWithError(http.StatusBadRequest, models.Error("Device id is invalid:", id))
		return nil, ""
	}

	data, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Read data failed." + err.Error()))
		return nil, ""
	}
	if len(data) == 0 {
		c.AbortWithError(http.StatusBadRequest, models.Error("File size is zero."))
		return nil, ""
	}

	size, err := strconv.Atoi(c.Request.Header.Get("Content-Length"))
	if err != nil || len(data) != size {
		c.AbortWithError(http.StatusBadRequest, models.Error("Recv file length failed."))
		return nil, ""
	}
	if size == 0 {
		c.AbortWithError(http.StatusBadRequest, models.Error("FileSize is invalid."))
		return nil, ""
	}

	if size > models.MaxFileSize {
		c.AbortWithError(http.StatusBadRequest, models.Error("FileSize too large."))
		return nil, ""
	}

	image.Name = extName
	image.Data = data
	image.Size = size

	return image, bson.ObjectIdHex(id)
}

func UploadImage(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	image, deviceID := uploadValidate(c)
	if image == nil {
		return
	}
	ct := http.DetectContentType(image.Data)
	fn := strings.Split(ct, "/")
	if len(fn) != 2 {
		logger.Warning("It's not a image.")
		c.AbortWithError(http.StatusBadRequest, models.Error("It's not a image."))
		return
	}
	if fn[1] != "jpeg" && fn[1]  != "png" {
		logger.Warning("It's a invalid image.", fn[1])
		c.AbortWithError(http.StatusBadRequest, models.Error("It's a invalid image."))
		return
	}

	device, err := models.FindDeviceByID(db, deviceID, nil)
	if err != nil {
		logger.Error("Find device by id failed.", err)
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if me.Role == models.RoleUser && device.OwnerID != me.ID {
		logger.Error("The device is not yours.")
		c.AbortWithError(http.StatusInternalServerError, models.Error("The device is not yours."))
		return
	}


	cattle, err := models.FindCattleByDeviceID(db, deviceID)
	if err != nil {
		logger.Warning("Find cattle by id failed.", err)
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	if cattle == nil {
		cattle = &models.Cattle {
			ID : bson.NewObjectId(),
			OwnerID : device.OwnerID,
			OwnerName : device.OwnerName,
//			CompanyID : device.CompanyID,
//			CompanyName : device.CompanyName,
			DeviceID : device.ID,
			Mark : device.Mark,
		}
		if err := cattle.Create(db); err != nil {
			logger.Error("Create empty cattle failed.", err)
			c.AbortWithError(http.StatusBadRequest, models.Error("Create empty cattle failed.", err))
			return
		}
	}
	if len(cattle.Images) >= models.BiologicalMaxImages {
		logger.Warning("The cattle images over limit.", len(cattle.Images))
		c.AbortWithError(http.StatusBadRequest, models.Error("The cattle images over limit."))
		return
	}

//	image.CompanyID = me.CompanyID
//	image.CompanyName = me.CompanyName
	image.OwnerID = cattle.OwnerID
	image.UploadBy = me.ID
	image.Name = device.ID.Hex() + "." + image.Name

	if err := image.UploadImage(db); err != nil {
		logger.Error(err)
		c.AbortWithError(http.StatusInternalServerError, err)
		return
		
	}

	if err := cattle.InsertImage(db, image); err != nil {
		image.Delete(db)
		logger.Warning("Insert image to cattle failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Insert image to cattle failed.", err))
		return
	}

	c.Status(http.StatusCreated)
}
