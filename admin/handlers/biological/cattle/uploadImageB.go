package cattle

import (
	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	"io/ioutil"
	"net/http"
//	"strconv"
	"strings"
	
	"encoding/base64"

	"gopkg.in/mgo.v2/bson"

	"github.com/gin-gonic/gin"
)

func uploadB64Validate(c *gin.Context) (*models.Image, bson.ObjectId) {
	image := &models.Image{}

	fileName := c.Param("name")
	fileName = strings.TrimSpace(fileName)
	if fileName == "" {
		c.AbortWithError(http.StatusBadRequest, models.Error("ImageName is invalid."))
		return nil, ""
	}
	fileName = strings.ToLower(fileName)
	fn := strings.Split(fileName, ".")
	if len(fn) < 2 {
		c.AbortWithError(http.StatusBadRequest, models.Error("Imagename is invalid."))
		return nil, ""
	}
	extName := fn[len(fn) - 1]
	if extName != "jpg" && extName != "jpeg" && extName != "png" {
		c.AbortWithError(http.StatusBadRequest, models.Error("Image format is invalid."))
		return nil, ""
	}

	id := c.Param("id")
	if !bson.IsObjectIdHex(id) {
		c.AbortWithError(http.StatusBadRequest, models.Error("Cattle id is invalid:", id))
		return nil, ""
	}

	data, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Read data failed." + err.Error()))
		return nil, ""
	}
	if len(data) == 0 {
		c.AbortWithError(http.StatusBadRequest, models.Error("Image size is zero."))
		return nil, ""
	}
	size, err := base64.StdEncoding.Decode(image.Data, data)
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Decode base failed."))
		return nil, ""
	}
	if size == 0 {
		c.AbortWithError(http.StatusBadRequest, models.Error("ImageSize is invalid."))
		return nil, ""
	}
	if size > models.MaxFileSize {
		c.AbortWithError(http.StatusBadRequest, models.Error("ImageSize too large."))
		return nil, ""
	}

	image.Name = fileName
	image.Data = data
	image.Size = size

	return image, bson.ObjectIdHex(id)
}

func UploadImageB(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	image, cattleID := uploadB64Validate(c)
	if image == nil {
		return
	}

	cattle, err := models.FindCattleByID(db, cattleID)
	if err != nil {
		logger.Warning("Find cattle by id failed.", err)
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	if cattle == nil {
		logger.Warning("Can't find cattle.")
		c.AbortWithError(http.StatusNotFound, models.Error("Can't find cattle."))
		return
	}
	if len(cattle.Images) >= models.BiologicalMaxImages {
		logger.Warning("The cattle images over limit.")
		c.AbortWithError(http.StatusBadRequest, models.Error("The cattle images over limit."))
		return
	}

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if me.Role == models.RoleUser && cattle.OwnerID != me.ID {
		logger.Warning("The cattle is not yours.")
		c.AbortWithError(http.StatusForbidden, models.Error("The cattle is not yours."))
		return
	}

//	image.CompanyID = me.CompanyID
//	image.CompanyName = me.CompanyName
	image.OwnerID = cattle.OwnerID
	image.UploadBy = me.ID

	if err := image.UploadImage(db); err != nil {
		logger.Error(err)
		c.AbortWithError(http.StatusInternalServerError, err)
		return
		
	}


	if err := cattle.InsertImage(db, image); err != nil {
		image.Delete(db)
		logger.Warning("Insert image to cattle failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Insert image to cattle failed.", err))
		return
	}

	c.Status(http.StatusCreated)
}
