package cattle

import (
	"druid/cattle/admin/middleware"
	"druid/cattle/models"

	"net/http"
	"strconv"
	

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"
)

func ListUser(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	id := c.Param("id")
	if !bson.IsObjectIdHex(id) {
		c.AbortWithError(http.StatusBadRequest, models.Error("User id is invalid:", id))
		return
	}
	user := models.FindUserByID(db, bson.ObjectIdHex(id))
	if user == nil {
		logger.Warning("List user by id failed.")
		c.AbortWithError(http.StatusNotFound, models.Error("List user by id failed."))
		return
	}
	if user.CompanyID != me.CompanyID {
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access other user."))
		return
	}

	m := bson.M{"company_id": me.CompanyID }
	m["owner_id"] = user.ID

	cattle, err := models.ListCattleByFilter(db, m, models.ParseRequestParameter(c))
	if err != nil {
		logger.Warning("List cattle by id failed.", err)
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	count := 0
	if len(cattle) > 0 {
		count, err = models.CountCattleByFilter(db, m)
	}
	c.Header(models.ResultCountHeader, strconv.Itoa(count))

	c.JSON(http.StatusOK, cattle)
}
