package cattle

import (
	"druid/cattle/admin/middleware"
	"druid/cattle/models"
	"encoding/json"

	"time"
	"net/http"

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"
)

func updateValidate(c *gin.Context) (*models.Cattle) {
	var cattle models.Cattle
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&cattle); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	id := c.Param("id")
	if !bson.IsObjectIdHex(id) {
		c.AbortWithError(http.StatusBadRequest, models.Error("Cattle id is invalid:", id))
		return nil
	}
	if cattle.Gender != models.FEMALE && cattle.Gender != models.MALE {
		c.AbortWithError(http.StatusBadRequest, models.Error("Gender is invalid value."))
		return nil
	}

	cattle.DeviceID = bson.ObjectIdHex(id)

	return &cattle
}

// Update a cattle record
func Update(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	cattle := updateValidate(c)
	if cattle == nil {
		return
	}

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	device, err := models.FindDeviceByID(db, cattle.DeviceID, nil)
	if err != nil {
		logger.Error("Find device by id failed.", err)
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	if me.Role == models.RoleUser && device.OwnerID != me.ID {
		logger.Error("The device is not yours.")
		c.AbortWithError(http.StatusInternalServerError, models.Error("The device is not yours."))
		return
	}

	cattleOrgin, err := models.FindCattleByDeviceID(db, cattle.DeviceID)
	if err != nil {
		logger.Warning("Find cattle by id failed.", err)
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	if cattleOrgin == nil {
		now := time.Now()
		cattle.ID = bson.NewObjectId()
		cattle.OwnerID = device.OwnerID
		cattle.OwnerName = device.OwnerName
//		cattle.CompanyID = device.CompanyID
//		cattle.CompanyName = device.CompanyName
		cattle.DeviceID = device.ID
		cattle.Mark = device.Mark
		cattle.UpdatedAt = &now

		device.BiologicalID = cattle.ID
		device.BiologicalType = models.BiologicalTypeCattle
//		device.NickName = cattle.NickName

		if err := device.UpdateBiological(db); err != nil {
			logger.Error("Update device cattle failed.", err)
			c.AbortWithError(http.StatusBadRequest, models.Error("Update device cattle failed.", err))
			return
		}

		if err := cattle.Create(db); err != nil {
			logger.Error("Create empty cattle failed.", err)
			c.AbortWithError(http.StatusBadRequest, models.Error("Create empty cattle failed.", err))
			return
		}

		c.JSON(http.StatusOK, cattle)
		return
	}

	if device.BiologicalID == "" {
		device.BiologicalID = cattleOrgin.ID
		device.BiologicalType = models.BiologicalTypeCattle
//		device.NickName = cattle.NickName

		if err := device.UpdateBiological(db); err != nil {
			logger.Error("Update device cattle failed.", err)
			c.AbortWithError(http.StatusBadRequest, models.Error("Update device cattle failed.", err))
			return
		}
	}

	if err := cattle.Update(db); err != nil {
		logger.Error("Update cattle biological failed.", err, cattle)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Update cattle biological failed.", err))
		return
	}

	/*
	device.NickName = cattle.NickName
	if err := device.UpdateNickName(db); err != nil {
		logger.Error("Update device cattle failed.", err)
		c.AbortWithError(http.StatusBadRequest, models.Error("Update device cattle failed.", err))
		return
	}
	*/

	cattle, err = models.FindCattleByDeviceID(db, cattle.DeviceID)
	if err != nil {
		logger.Warning("Find cattle by id failed.", err)
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	c.JSON(http.StatusOK, cattle)
}
