package cattle

import (
	"druid/cattle/admin/middleware"
	"druid/cattle/models"

	"net/http"
	"strconv"

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"
)

// List biological record
func List(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	m := bson.M{"company_id": me.CompanyID }
	if me.Role == models.RoleUser {
		m["owner_id"] = me.ID
	}

	cattle, err := models.ListCattleByFilter(db, m, models.ParseRequestParameter(c))
	if err != nil {
		logger.Warning("List cattle by id failed.", err)
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	count := 0
	if len(cattle) > 0 {
		count, err = models.CountCattleByFilter(db, m)
	}
	c.Header(models.ResultCountHeader, strconv.Itoa(count))

	c.JSON(http.StatusOK, cattle)
}
