package setting

import (
	"net/http"
	"strconv"
	
	"github.com/gin-gonic/gin"

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/admin/middleware"
	"druid/cattle/models"
)

// List devices setting
func List(c *gin.Context) {
	db := middleware.GetDB(c)
//	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	rp := models.ParseRequestParameter(c)

	m := bson.M{
		"company_id": user.CompanyID, 
		"deleted_at": nil,
	}
	result, err := models.ListSettingByFilter(db, m, rp)
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, models.Error("List setting by filter failed." + err.Error()))
		return
	}

	count := 0
	if len(result) > 0 {
		count, _ = models.CountSettingByFilter(db, m)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, result)
}
