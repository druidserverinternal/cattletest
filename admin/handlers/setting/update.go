package setting

import (
	"net/http"
//	"strconv"
	"encoding/json"
	"github.com/gin-gonic/gin"

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/admin/middleware"
	"druid/cattle/models"
)

func updateSettingValidate(c *gin.Context) (*models.Setting) {
	setting := models.Setting{}
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&setting); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	deviceID := c.Param("id")
	if !bson.IsObjectIdHex(deviceID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("device id is invalid:", deviceID))
		return nil
	}
	setting.DeviceID = bson.ObjectIdHex(deviceID)

	if setting.EnvSamplingMode != models.SamplingEnable && setting.EnvSamplingMode != models.SamplingDisable {
		c.AbortWithError(http.StatusBadRequest, models.Error("EnvSamplingMode is invalid.", setting.EnvSamplingMode))
		return nil
	}
	/*
	if setting.GprsMode != models.SamplingEnable {
		c.AbortWithError(http.StatusBadRequest, models.Error("GprsMode is invalid.", setting.GprsMode))
		return nil
	}
	*/

	if setting.EnvSamplingFreq < models.EnvFreqMin || setting.EnvSamplingFreq > models.EnvFreqMax {
		c.AbortWithError(http.StatusBadRequest, models.Error("EnvSamplingFreq is invalid.", setting.EnvSamplingFreq))
		return nil
	}
	if setting.GprsFreq < models.GprsFreqMin || setting.GprsFreq > models.GprsFreqMax {
		c.AbortWithError(http.StatusBadRequest, models.Error("GprsFreq is invalid.", setting.GprsFreq))
		return nil
	}

	if setting.EnvVoltageThreshold < models.VoltageThresholdMin || setting.EnvVoltageThreshold > models.VoltageThresholdMax {
		c.AbortWithError(http.StatusBadRequest, models.Error("EnvVoltageThreshold is invalid.", setting.EnvVoltageThreshold))
		return nil
	}
	if setting.GprsVoltageThreshold < models.VoltageThresholdMin || setting.GprsVoltageThreshold > models.VoltageThresholdMax {
		c.AbortWithError(http.StatusBadRequest, models.Error("GprsVoltageThreshold is invalid.", setting.GprsVoltageThreshold))
		return nil
	}

	return &setting
}

func Update(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	setting := updateSettingValidate(c)
	if setting == nil {
		return
	}

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	device, err := models.FindValidDeviceByID(db, setting.DeviceID, nil)
	if err != nil {
		logger.Error("Device ID is invalid.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Device ID is invalid.", err))
		return
	}
	if device == nil {
		logger.Error("Device is nil.")
		c.AbortWithError(http.StatusNotFound, models.Error("Device ID is invalid."))
		return
	}

	if device.CompanyID != user.CompanyID {
		logger.Error("You don't have permission to access other.")
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access other."))
		return
	}

	setting.UpdatedBy = user.ID

	if err = setting.UpdateSet(db); err != nil {
		logger.Error("Update setting set failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Update setting set failed.", err))
		return
	}

	c.Status(http.StatusCreated)
}
