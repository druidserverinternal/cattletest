package setting

import (
	"encoding/json"
	"net/http"
	"regexp"
	"strings"

	"github.com/gin-gonic/gin"
	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	"gopkg.in/mgo.v2/bson"
)

var regexpSpNumber = regexp.MustCompile(`^\+[0-9]{13}$`)

func updateManyValidate(c *gin.Context) (*models.SettingMany) {
	settingMany := models.SettingMany{}
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&settingMany); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	if settingMany.EnvSamplingMode != models.SamplingEnable && settingMany.EnvSamplingMode != models.SamplingDisable {
		c.AbortWithError(http.StatusBadRequest, models.Error("EnvSamplingMode is invalid."))
		return nil
	}
	if settingMany.BehaviorSamplingMode != models.SamplingEnable && settingMany.BehaviorSamplingMode != models.SamplingDisable {
		c.AbortWithError(http.StatusBadRequest, models.Error("BehaviorSamplingMode is invalid."))
		return nil
	}

	if settingMany.EnvSamplingFreq < models.EnvFreqMin || settingMany.EnvSamplingFreq > models.EnvFreqMax {
		c.AbortWithError(http.StatusBadRequest, models.Error("EnvSamplingFreq is invalid."))
		return nil
	}
	if settingMany.BehaviorSamplingFreq < models.BehFreqMin || settingMany.BehaviorSamplingFreq > models.BehFreqMax {
		c.AbortWithError(http.StatusBadRequest, models.Error("BehaviorSamplingFreq is invalid."))
		return nil
	}
	if settingMany.GprsFreq < models.GprsFreqMin || settingMany.GprsFreq > models.GprsFreqMax {
		c.AbortWithError(http.StatusBadRequest, models.Error("GprsFreq is invalid."))
		return nil
	}

	if settingMany.EnvVoltageThreshold < models.VoltageThresholdMin || settingMany.EnvVoltageThreshold > models.VoltageThresholdMax {
		c.AbortWithError(http.StatusBadRequest, models.Error("EnvVoltageThreshold is invalid."))
		return nil
	}
	if settingMany.BehaviorVoltageThreshold < models.VoltageThresholdMin || settingMany.BehaviorVoltageThreshold > models.VoltageThresholdMax {
		c.AbortWithError(http.StatusBadRequest, models.Error("BehaviorVoltageThreshold is invalid."))
		return nil
	}
	if settingMany.GprsVoltageThreshold < models.VoltageThresholdMin || settingMany.GprsVoltageThreshold > models.VoltageThresholdMax {
		c.AbortWithError(http.StatusBadRequest, models.Error("GprsVoltageThreshold is invalid."))
		return nil
	}
	if settingMany.OtaVoltageThreshold < models.VoltageThresholdMin || settingMany.OtaVoltageThreshold > models.VoltageThresholdMax {
		c.AbortWithError(http.StatusBadRequest, models.Error("OtaVoltageThreshold is invalid."))
		return nil
	}

	if settingMany.GprsType != models.GprsTypeInterval && settingMany.GprsType != models.GprsTypeTable {
		c.AbortWithError(http.StatusBadRequest, models.Error("GprsType is invalid."))
		return nil
	}

	settingMany.GprsMode = models.SamplingEnable 

	settingMany.SpNumber = strings.TrimSpace(settingMany.SpNumber)
	if !regexpSpNumber.MatchString(settingMany.SpNumber) {
		c.AbortWithError(http.StatusBadRequest, models.Error("SpNumber must be a valid sp number."))
		return nil
	}

	return &settingMany
}

// UpdateFirmware a settingMany
func UpdateMany(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	settingMany := updateManyValidate(c)
	if settingMany == nil {
		return
	}

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	m := bson.M{
		"company_id": user.CompanyID, 
		"device_id": bson.M{"$in": settingMany.Devices}, 
	}

	settings, err := models.ListSettingByFilter(db, m, nil)
	if err != nil || len(settingMany.Devices) != len(settings) {
		logger.Error("Could not find some setting")
		c.AbortWithError(http.StatusBadRequest, models.Error("Could not find some setting."))
		return
	}

	if err = settingMany.Update(db, user.ID); err != nil {
		logger.Error("Update many setting failed:", err)
		c.AbortWithError(http.StatusBadRequest, models.Error("Update many setting failed:", err))
		return
	}

	c.Status(http.StatusCreated)
}
