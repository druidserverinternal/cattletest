package setting

import (
	"net/http"
	

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/admin/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

func getValidate(c *gin.Context) (string) {
	deviceID := c.Param("id")
	if !bson.IsObjectIdHex(deviceID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("Device is is invalid:" + deviceID))
		return ""
	}
	return deviceID
}

// Get a device setting
func Get(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	deviceID := getValidate(c)
	if deviceID == "" {
		return
	}

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	device, err := models.FindDeviceByID(db, bson.ObjectIdHex(deviceID), nil)
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Device is invalid:", deviceID))
		return
	}

	if err := me.CheckDeviceOwner(device); err != nil {
		logger.Warning("Check owner device failed.", err)
		c.AbortWithError(http.StatusForbidden, models.Error("Check owner device failed.", err))
		return
	}

	setting, err := models.FindSettingByDeviceID(db, device.ID)
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, models.Error("Can't find device setting." + err.Error()))
		return
	}

	c.JSON(http.StatusOK, setting)
}
