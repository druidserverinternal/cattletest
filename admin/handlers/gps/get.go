package gps

import (
	"net/http"
	"strconv"
	
//	"time"

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/admin/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

func getValidate(c *gin.Context) (string) {
	deviceID := c.Param("id")
	if !bson.IsObjectIdHex(deviceID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("DeviceID is invalid:" + deviceID))
		return ""
	}
	return deviceID
}

// Get one of device record with device
func Get(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	deviceID := getValidate(c)
	if deviceID == "" {
		return
	}

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	device, err := models.FindDeviceByID(db, bson.ObjectIdHex(deviceID), nil)
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Find Device is invalid:" + deviceID))
		return
	}

	if device.CompanyID != user.CompanyID {
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access"))
		return
	}

	if err := user.CheckDeviceOwner(device); err != nil {
		logger.Warning(err)
		c.AbortWithError(http.StatusForbidden, err)
		return
	}

	m := bson.M{ "device_id": device.ID}

	begin, _ := models.ConvertStringToTime(c.Query(models.QueryTimeBegin))
	end, _ := models.ConvertStringToTime(c.Query(models.QueryTimeEnd))
	last := c.Query(models.QueryTimeLast)

	if begin != nil && end != nil {
		begin = device.CheckBeginTime(begin)
		m["timestamp"] = bson.M{"$gte": begin, "$lt": end}
	} else {
		begin = device.GetBeginTime(last)
        m["timestamp"] = bson.M{"$gte": begin}
    }

	rp := models.ParseRequestParameter(c)
	if rp.LimitOrign == 0 {
        rp.Limit = 0
    }

	result, err := models.ListGPSByFilter(db, m, rp, models.UserSelect)
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, models.Error("Can't find device gps"))
		return
	}

	count := 0
	if len(result) > 0 {
		count, _ = models.CountGPSByFilter(db, m)
	}
	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, result)
}
