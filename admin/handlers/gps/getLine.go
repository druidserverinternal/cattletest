package gps

import (
	"net/http"
//	"time"

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"
	
	"druid/cattle/admin/middleware"
	"druid/cattle/models"
)

// GetLine one of device record with device
func GetLine(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	deviceID := getValidate(c)
	if deviceID == "" {
		return
	}

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	device, err := models.FindDeviceByID(db, bson.ObjectIdHex(deviceID), nil)
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Find Device is invalid:" + deviceID))
		return
	}

	if device.CompanyID != user.CompanyID {
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access"))
		return
	}

	if err := user.CheckDeviceOwner(device); err != nil {
		logger.Warning(err)
		c.AbortWithError(http.StatusForbidden, err)
		return
	}

	m := bson.M{ "device_id": device.ID}

    begin, _ := models.ConvertStringToTime(c.Query(models.QueryTimeBegin))
    end, _ := models.ConvertStringToTime(c.Query(models.QueryTimeEnd))
    last := c.Query(models.QueryTimeLast)

    if begin != nil && end != nil {
        begin = device.CheckBeginTime(begin)
        m["timestamp"] = bson.M{"$gte": begin, "$lt": end}
    } else {
        begin = device.GetBeginTime(last)
        m["timestamp"] = bson.M{"$gte": begin}
    }

	rp := models.ParseRequestParameter(c)
	if rp.LimitOrign == 0 {
        rp.Limit = 0
    }

	result, err := models.ListGPSByFilter(db, m, rp, models.UserSelect)
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, models.Error("Can't find device gps"))
		return
	}

	gs := models.DouglasPeucker(result, 1)

	//	logger.Info("mongo spend:", time.Now().Sub(begin))

	c.JSON(http.StatusOK, gs)
}

