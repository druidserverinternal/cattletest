package user

import (
	"druid/cattle/admin/middleware"
	"druid/cattle/models"

	"net/http"
	
	"strconv"
	"github.com/gin-gonic/gin"

	"gopkg.in/mgo.v2/bson"
)

// List myself company's users
func List(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	rp := models.ParseRequestParameter(c)
	users, err := models.ListCommonUsersByCompanyID(db, user.CompanyID, rp)
	if err != nil {
		logger.Warning("List common user by company id failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List user from company failed." + err.Error()))
		return
	}

	/*
	usrOwner, err := models.CountOwnerGroup(db, user.CompanyID)
	if err != nil {
		logger.Warning("Count owner device failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Count device owner failed.", err))
		return
	}
	for _, usr := range usrOwner {
		for _, u := range users {
			if u.ID == usr.ID {
				u.DeviceCount = usr.DeviceCount
			}
		}
	}

	usrOwner, err := models.CountDituOwnerGroup(db, user.ID)
	if err != nil {
		logger.Warning("Count ditu owner device failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Count ditu owner failed.", err))
		return
	}
	for _, usr := range usrOwner {
		for _, u := range users {
			if u.ID == usr.ID {
				u.DituCount = usr.DituCount
			}
		}
	}
	*/

	count := 0
	if len(users) > 0 {
		count, err = models.CountCommonUsersByCompanyID(db, user.CompanyID)
	}


	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, users)
}
