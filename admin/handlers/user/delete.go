package user

import (
	"druid/cattle/admin/middleware"
	"druid/cattle/models"

	"fmt"
	"net/http"
	"github.com/gin-gonic/gin"

	"gopkg.in/mgo.v2/bson"
)

func deleteValidate(c *gin.Context) (string) {
	userID := c.Param("id")
	if !bson.IsObjectIdHex(userID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("User id is invalid:" + userID))
		return ""
	}

	return userID
}

// Delete a user
func Delete(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	userID := deleteValidate(c)
	if userID == "" {
		return
	}

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	user := models.FindUserByID(db, bson.ObjectIdHex(userID))
	if user == nil {
		c.AbortWithError(http.StatusNotFound, models.Error(fmt.Sprintf("User:%s cannot be found", userID)))
		return
	}

	if user.CompanyID != me.CompanyID {
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access"))
		return
	}

	if user.Role != models.RoleUser {
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access it"))
		return
	}

	if user.TotalDevice > 0 {
		if err := user.FixTotalDevice(db); err != nil {
			logger.Error("Fix total device failed.", err)
			c.AbortWithError(http.StatusInternalServerError, models.Error("Fix total device failed.", err))
			return
		}

		if user.TotalDevice > 0 {
			logger.Error("Could not delete had device user.")
			c.AbortWithError(http.StatusBadRequest, models.Error("Could not delete had device user."))
			return
		}
	}


	user.DeletedBy = me.ID

	if err := user.Delete(db); err != nil {
		c.AbortWithError(http.StatusInternalServerError, models.Error("Delete company internal failed," + err.Error()))
		return
	}
	logger.Infof("User:%s delete user:%s", middleware.GetUserID(c), user.UserName)

	c.Status(http.StatusNoContent)
}
