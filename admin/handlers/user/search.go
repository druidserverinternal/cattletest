package user

import (
	"strconv"
	"net/http"
	
	"strings"

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/admin/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

func Search(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	key := c.Param("key")
	key = strings.TrimSpace(key)
	if key == "" {
		logger.Error("Request search key invalid.")
		c.AbortWithError(http.StatusBadRequest, models.Error("Request search key invalid."))
		return
	}

	logger.Info("Search key: ", key)
	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	
	d := []bson.M{}
	d = append(d, bson.M{"username" : bson.M{"$regex": &bson.RegEx{Pattern: key, Options: "i"}}})
	d = append(d, bson.M{"phone" : bson.M{"$regex": &bson.RegEx{Pattern: key, Options: "i"}}})
	d = append(d, bson.M{"id_card" : bson.M{"$regex": &bson.RegEx{Pattern: key, Options: "i"}}})
	d = append(d, bson.M{"address" : bson.M{"$regex": &bson.RegEx{Pattern: key, Options: "i"}}})

	m := bson.M{ "company_id": user.CompanyID, "role": models.RoleUser, "deleted_at": nil, "$or": d }

	users, err := models.ListUserByFilter(db, m, models.ParseRequestParameter(c))
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, models.Error("Can't find user." + err.Error()))
		return
	}

	/*
	us := []bson.ObjectId{}
	for _, u := range users {
		us = append(us, u.ID)
	}

	usrOwner, err := models.CountDeviceOwner(db, us)
	if err != nil {
		logger.Warning("Count owner device failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Count device owner failed.", err))
		return
	}
	for _, usr := range usrOwner {
		for _, u := range users {
			if u.ID == usr.ID {
				u.DeviceCount = usr.DeviceCount
			}
		}
	}
	*/

	count := 0
	if len(users) > 0 {
		count, _ = models.CountUserByFilter(db, m)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, users)
}
