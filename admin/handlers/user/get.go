package user

import (
	"druid/cattle/admin/middleware"
	"druid/cattle/models"

	"fmt"
	"net/http"
	
	"github.com/gin-gonic/gin"

	"gopkg.in/mgo.v2/bson"
)

func getValidate(c *gin.Context) (string) {
	userID := c.Param("id")
	if !bson.IsObjectIdHex(userID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("User id is invalid:" + userID))
		return ""
	}

	return userID
}

// Get a any user
func Get(c *gin.Context) {
	db := middleware.GetDB(c)
	//logger := middleware.GetLogger(c)

	userID := getValidate(c)
	if userID == "" {
		return
	}

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	user := models.FindUserByID(db, bson.ObjectIdHex(userID))
	if user == nil {
		c.AbortWithError(http.StatusNotFound, models.Error(fmt.Sprintf("User:%s cannot be found", userID)))
		return
	}
	if user.CompanyID != me.CompanyID {
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access"))
		return
	}

	/*
	m := bson.M{ "owner_id": user.ID, }
	count, err := models.CountDeviceByFilter(db, m)
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, models.Error("Count device by user owner failed."))
		return
	}

	user.DeviceCount = count
	*/

	c.JSON(http.StatusOK, user)
}
