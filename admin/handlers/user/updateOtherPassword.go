package user

import (
	"druid/cattle/admin/middleware"
	"druid/cattle/models"

	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	
	"github.com/gin-gonic/gin"

	"github.com/spf13/viper"
	"gopkg.in/mgo.v2/bson"
)

func updateOtherPasswordValidate(c *gin.Context) (*models.User) {
	var user models.User
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&user); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	user.OldPassword = strings.TrimSpace(user.OldPassword)
	if !models.RegexpPwd.MatchString(user.OldPassword) {
		c.AbortWithError(http.StatusBadRequest, models.Error("oldpassword must be a valid oldpassword."))
		return nil
	}

	user.Password = strings.TrimSpace(user.Password)
	if !models.RegexpPwd.MatchString(user.Password) {
		c.AbortWithError(http.StatusBadRequest, models.Error("password must be a valid password."))
		return nil
	}

	userID := c.Param("id")
	if !bson.IsObjectIdHex(userID) {
		c.AbortWithError(http.StatusBadRequest, models.Error(fmt.Sprintf("User id:%s is invalid", userID)))
		return nil
	}
	user.ID = bson.ObjectIdHex(userID)

	return &user
}

// Update user password
func UpdateOtherPassword(c *gin.Context) {
	db := middleware.GetDB(c)
	//logger := middleware.GetLogger(c)

	userReq := updateOtherPasswordValidate(c)
	if userReq == nil {
		return
	}

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	user := models.FindUserByID(db, userReq.ID)
	if user == nil {
		c.AbortWithError(http.StatusNotFound, models.Error(fmt.Sprintf("User:%s cannot be found", userReq.ID)))
		return
	}

	if user.CompanyID != me.CompanyID {
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access user"))
		return
	}

	if user.Role != models.RoleUser {
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access it"))
		return
	}

	if err := models.PasswordVerify(userReq.OldPassword, me.HashedPassword); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("The verify password is invalid!"))
		return
	}

	if !userReq.HashPassword() {
		c.AbortWithError(http.StatusBadRequest, models.Error("The password is invalid."))
		return
	}

	token := models.NewToken(userReq.ID, viper.GetString("authentication.secret"))
	userReq.Token = token.String()
	userReq.Expiry = &token.Expiry

	if err := userReq.UpdatePassword(db); err != nil {
		c.AbortWithError(http.StatusInternalServerError, models.Error("Update user failed." + err.Error()))
		return
	}

	c.Status(http.StatusCreated)
}
