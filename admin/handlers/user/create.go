package user

import (
	"druid/cattle/admin/middleware"
	"druid/cattle/models"

	"encoding/json"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"

	//"github.com/spf13/viper"
	"gopkg.in/mgo.v2/bson"
)

func createValidate(c *gin.Context) *models.User {
	var user models.User
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&user); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	user.UserName = strings.TrimSpace(user.UserName)
	if !models.RegexpUser.MatchString(user.UserName) {
		c.AbortWithError(http.StatusBadRequest, models.Error("username must be a valid name."))
		return nil
	}

	user.Password = strings.TrimSpace(user.Password)
	if !models.RegexpPwd.MatchString(user.Password) {
		c.AbortWithError(http.StatusBadRequest, models.Error("password must be a valid password."))
		return nil
	}

	user.Address = strings.TrimSpace(user.Address)
	if user.Address == "" {
		c.AbortWithError(http.StatusBadRequest, models.Error("address must be a valid value."))
		return nil
	}

	user.Phone = strings.TrimSpace(user.Phone)
	if !models.RegexpPhone.MatchString(user.Phone) {
		c.AbortWithError(http.StatusBadRequest, models.Error("phone must be a valid phone."))
		return nil
	}

	/*
		user.IDCard = strings.TrimSpace(user.IDCard)
		if !models.RegexpIDCard.MatchString(user.IDCard) {
			c.AbortWithError(http.StatusBadRequest, models.Error("idcard must be a valid idcard."))
			return nil
		}
	*/

	user.Role = models.RoleUser

	if user.Profile == nil {
		user.Profile = &models.UserProfile{
			PageSize: models.ProfilePageMin,
			TimeZone: models.ProfileTimezoneChina,
			Language: models.ProfileLanguageCH,
		}
	} else {
		if user.Profile.PageSize < models.ProfilePageMin || user.Profile.PageSize > models.ProfilePageMax  {
			user.Profile.PageSize = models.ProfilePageMin
		}

		if user.Profile.TimeZone < models.ProfileTimezoneMin || user.Profile.PageSize > models.ProfileTimezoneMax {
			user.Profile.TimeZone = models.ProfileTimezoneChina
		}

		if user.Profile.Language < models.ProfileLanguageBegin || user.Profile.Language > models.ProfileLanguageEnd {
			user.Profile.Language = models.ProfileLanguageCH
		}
	}

	return &user
}

// Create a user
func Create(c *gin.Context) {
	db := middleware.GetDB(c)
	//logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	reqUser := createValidate(c)
	if reqUser == nil {
		return
	}

	origUser, err := models.FindUserByUserName(db, reqUser.UserName)
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find username failed!"+err.Error()))
		return
	}
	if origUser != nil {
		c.AbortWithError(http.StatusServiceUnavailable, models.Error("The user was exsists."))
		return
	}

	if reqUser.Password == "" || !reqUser.HashPassword() {
		c.AbortWithError(http.StatusBadRequest, models.Error("The password is invalid."))
		return
	}

	reqUser.ID = bson.NewObjectId()
	reqUser.CreatorID = user.ID
	reqUser.CompanyID = user.CompanyID
	reqUser.CompanyName = user.CompanyName

	if err = reqUser.Create(db); err != nil {
		c.AbortWithError(http.StatusInternalServerError, models.Error("Create user failed."+err.Error()))
		return
	}

	c.JSON(http.StatusOK, reqUser)
}
