package user

import (
	"druid/cattle/admin/middleware"
	"druid/cattle/models"
//	"encoding/json"

	
	"net/http"
	"github.com/gin-gonic/gin"

	"gopkg.in/mgo.v2/bson"

//	"strings"
)
func updatePushSetValidate(c *gin.Context) (*models.PushSet) {
	var profile models.PushSet
	if err := c.BindJSON(&profile); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}
	return &profile
}

// UpdateProfilePush update myself push set
func UpdateProfilePush(c *gin.Context) {
	db := middleware.GetDB(c)
	//	logger := middleware.GetLogger(c)

	ps := updatePushSetValidate(c)
	if ps == nil {
		return
	}

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	user.PushSet = ps

	if err := user.UpdatePushSet(db); err != nil {
		c.AbortWithError(http.StatusInternalServerError, models.Error("Update myself push set failed." + err.Error()))
		return
	}

	c.Status(http.StatusCreated)
}
