package user

import (
	"druid/cattle/admin/middleware"
	"druid/cattle/models"

	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	
	"github.com/gin-gonic/gin"

	"gopkg.in/mgo.v2/bson"
)

func updateOtherInfoValidate(c *gin.Context) (*models.User) {
	var user models.User
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&user); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}


	user.Address = strings.TrimSpace(user.Address)
	if user.Address == "" {
		c.AbortWithError(http.StatusBadRequest, models.Error("address must be a valid value."))
		return nil
	}


	user.Phone = strings.TrimSpace(user.Phone)
	if user.Phone != "" && !models.RegexpPhone.MatchString(user.Phone) {
		c.AbortWithError(http.StatusBadRequest, models.Error("phone must be a valid phone."))
		return nil
	}

	/*
	user.IDCard = strings.TrimSpace(user.IDCard)
	if user.IDCard != "" && !models.RegexpIDCard.MatchString(user.IDCard) {
		c.AbortWithError(http.StatusBadRequest, models.Error("idcard must be a valid value."))
		return nil
	}
	*/

	userID := c.Param("id")
	if !bson.IsObjectIdHex(userID) {
		c.AbortWithError(http.StatusBadRequest, models.Error(fmt.Sprintf("User id:%s is invalid", userID)))
		return nil
	}
	user.ID = bson.ObjectIdHex(userID)

	return &user
}

// Update user password
func UpdateOtherInfo(c *gin.Context) {
	db := middleware.GetDB(c)
	//logger := middleware.GetLogger(c)

	userReq := updateOtherInfoValidate(c)
	if userReq == nil {
		return
	}

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	user := models.FindUserByID(db, userReq.ID)
	if user == nil {
		c.AbortWithError(http.StatusNotFound, models.Error(fmt.Sprintf("User:%s cannot be found", userReq.ID)))
		return
	}

	if user.CompanyID != me.CompanyID {
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access user"))
		return
	}

	if user.Role != models.RoleUser {
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access it"))
		return
	}

	if err := userReq.UpdateInfo(db); err != nil {
		c.AbortWithError(http.StatusInternalServerError, models.Error("Update user info failed." + err.Error()))
		return
	}


	c.Status(http.StatusCreated)
}
