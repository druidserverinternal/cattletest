package user

import (
	"druid/cattle/admin/middleware"
	"druid/cattle/models"
	"encoding/json"

	
	"net/http"
	"github.com/gin-gonic/gin"

	"gopkg.in/mgo.v2/bson"

//	"strings"
)

func updateProfileValidate(c *gin.Context) (*models.UserProfile) {
	var profile models.UserProfile
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&profile); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	if profile.PageSize < models.ProfilePageMin || profile.PageSize > models.ProfilePageMax {
		c.AbortWithError(http.StatusBadRequest, models.Error("pagesize must be a valid value."))
		return nil
	}

	if profile.TimeZone < models.ProfileTimezoneMin || profile.PageSize > models.ProfileTimezoneMax {
		c.AbortWithError(http.StatusBadRequest, models.Error("timezone must be a valid value."))
		return nil
	}

	if profile.Language < models.ProfileLanguageBegin || profile.Language > models.ProfileLanguageEnd {
		c.AbortWithError(http.StatusBadRequest, models.Error("language must be a valid value."))
		return nil
	}

	return &profile
}

// UpdateProfile update myself profile
func UpdateProfile(c *gin.Context) {
	db := middleware.GetDB(c)
	//	logger := middleware.GetLogger(c)

	profile := updateProfileValidate(c)
	if profile == nil {
		return
	}

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	user.Profile = profile

	if err := user.UpdateProfile(db); err != nil {
		c.AbortWithError(http.StatusInternalServerError, models.Error("Update myself profile failed." + err.Error()))
		return
	}

	c.Status(http.StatusCreated)
}
