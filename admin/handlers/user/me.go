package user

import (
	"druid/cattle/admin/middleware"
	"druid/cattle/models"

	"net/http"
	"github.com/gin-gonic/gin"

	"gopkg.in/mgo.v2/bson"
)

// Me mine user info
func Me(c *gin.Context) {
	db := middleware.GetDB(c)
	//	logger := middleware.GetLogger(ctx)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	c.JSON(http.StatusOK, user)
}
