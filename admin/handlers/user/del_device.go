package user

import (
	"druid/cattle/admin/middleware"
	"druid/cattle/models"

	"encoding/json"
	"fmt"
	"net/http"
	
	"github.com/gin-gonic/gin"

	"gopkg.in/mgo.v2/bson"
)

func delDeviceValidate(c *gin.Context) (*models.DeviceMany, string) {
	var device models.DeviceMany
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&device); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil, ""
	}

	userID := c.Param("id")
	if !bson.IsObjectIdHex(userID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("UserID is invalid:", userID))
		return nil, ""
	}

	return &device, userID
}

func DelDevice(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	devices, userID := delDeviceValidate(c)
	if devices == nil {
		return
	}

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	user := models.FindUserByID(db, bson.ObjectIdHex(userID))
	if user == nil {
		c.AbortWithError(http.StatusBadRequest, models.Error(fmt.Sprintf("User:%s cannot be found", user.ID)))
		return
	}
	if user.Role != models.RoleUser {
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access it."))
		return
	}
	if user.CompanyID != me.CompanyID {
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access user."))
		return
	}

	if len(devices.ID) == 0 {
		c.Status(http.StatusCreated)
		return
	}

	if err := models.CleanOwnerFromDevice(db, devices.ID); err != nil {
		logger.Error("Clean owner device failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Remove owner devices failed:", err))
		return
	}

	if err := user.FixTotalDevice(db); err != nil {
		logger.Error("Fix total device failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Fix total device failed.", err))
		return
	}


	c.Status(http.StatusNoContent)
}
