package room

import (
	"druid/cattle/admin/middleware"
	"druid/cattle/models"

	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	
	"time"
	"github.com/gin-gonic/gin"

	"gopkg.in/mgo.v2/bson"
)

func updateValidate(c *gin.Context) (*models.Room) {
	var room models.Room
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&room); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	room.RoomName = strings.TrimSpace(room.RoomName)
	if room.RoomName == "" {
		c.AbortWithError(http.StatusBadRequest, models.Error("room name must be a valid value."))
		return nil
	}

	roomID := c.Param("id")
	if !bson.IsObjectIdHex(roomID) {
		c.AbortWithError(http.StatusBadRequest, models.Error(fmt.Sprintf("Room id:%s is invalid", roomID)))
		return nil
	}
	room.ID = bson.ObjectIdHex(roomID)

	return &room
}

// Update room info
func Update(c *gin.Context) {
	db := middleware.GetDB(c)
	//logger := middleware.GetLogger(c)

	roomReq := updateValidate(c)
	if roomReq == nil {
		return
	}

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if me.Role != models.RoleUser {
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access."))
		return
	}

	room, _ := models.FindRoomByID(db, roomReq.ID)
	if room == nil {
		c.AbortWithError(http.StatusNotFound, models.Error(fmt.Sprintf("Room:%s cannot be found", roomReq.ID)))
		return
	}

	if room.UserID != me.ID {
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access room"))
		return
	}

	if err := models.RoomLockTimeout(db, room.ID, time.Now().Add(time.Second)); err != nil {
		c.AbortWithError(http.StatusInternalServerError, models.Error("Lock room failed," + err.Error()))
		return
	}
	defer models.RoomUnLock(db, room.ID)

	if err := roomReq.Update(db); err != nil {
		c.AbortWithError(http.StatusInternalServerError, models.Error("Update room failed." + err.Error()))
		return
	}

	if err := models.UpdateDeviceByRoomID(db, roomReq.ID, bson.M{"room_name": roomReq.RoomName}); err != nil {
		c.AbortWithError(http.StatusInternalServerError, models.Error("Update device room info failed." + err.Error()))
		return
	}

	c.Status(http.StatusCreated)
}
