package room

import (
	"encoding/json"
	"net/http"
	
//	"fmt"
	//	"regexp"

	//	"github.com/spf13/viper"

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/admin/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

func addDeviceValidate(c *gin.Context) (*models.DeviceMany) {
	deviceMany := models.DeviceMany{}
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&deviceMany); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	if len(deviceMany.ID) == 0 {
		c.AbortWithError(http.StatusBadRequest, models.Error("device_id must be a valid value."))
		return nil
	}

	roomID := c.Param("id")
	if !bson.IsObjectIdHex(roomID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("RoomID is invalid:" + roomID))
		return nil
	}

	deviceMany.RoomID = bson.ObjectIdHex(roomID)

	return &deviceMany
}

func AddDevice(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	devices := addDeviceValidate(c)
	if devices == nil {
		return
	}
	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.Role != models.RoleUser {
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access."))
		return
	}
	devices.CompanyID = user.CompanyID

	ds, err := devices.ListValidDevicesByUser(db, user.ID, nil)
	if err != nil || len(devices.ID) != len(ds) {
		logger.Error("Check devices failed.", devices)
		c.AbortWithError(http.StatusBadRequest, models.Error("Could not find some device.", err))
		return
	}

	room, _ := models.FindRoomByID(db, devices.RoomID)
	if room == nil {
		c.AbortWithError(http.StatusNotFound, models.Error("Room cannot be found:", room.ID))
		return
	}

	devices.RoomName = room.RoomName
	if err := devices.UpdateDeviceRoom(db); err != nil {
		c.AbortWithError(http.StatusInternalServerError, models.Error("Update device room info failed.", err))
		return
	}

	c.Status(http.StatusCreated)
}

