package room

import (
	"druid/cattle/admin/middleware"
	"druid/cattle/models"

	"encoding/json"
	"net/http"
	"strings"
	
	"github.com/gin-gonic/gin"

	//"github.com/spf13/viper"
	"gopkg.in/mgo.v2/bson"

)

func createValidate(c *gin.Context) (*models.Room) {
	var room models.Room
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&room); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	if len(ditu.RoomName) == 0 || len(ditu.RoomName) > models.MaxRoomName {
		c.AbortWithError(http.StatusBadRequest, models.Error("roomname must be a valid name."))
		return nil
	}

	return &room
}

// Create a room
func Create(c *gin.Context) {
	db := middleware.GetDB(c)
	//logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.Role != models.RoleUser {
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access."))
		return
	}

	reqRoom := createValidate(c)
	if reqRoom == nil {
		return
	}

	reqRoom.ID = bson.NewObjectId()
	reqRoom.UserID = user.ID

	if err := reqRoom.Create(db); err != nil {
		c.AbortWithError(http.StatusInternalServerError, models.Error("Create room failed." + err.Error()))
		return
	}

	c.JSON(http.StatusOK, reqRoom)
}
