package ditu

import (
	"net/http"
	"strconv"

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"

	"druid/cattle/admin/middleware"
	"druid/cattle/models"
)
func List(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	rp := models.ParseRequestParameter(c)

	result := []*models.DituArea{}
	var err error
	result, err = models.ListDituAreaByCompanyID(db, user.CompanyID, rp)
	if err != nil {
		logger.Error("List ditu area by company id failed. ", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List ditu area by company id failed.", err))
		return
	}

	count := 0
	if len(result) > 0 {
		count, err = models.CountDituAreaByCompanyID(db, user.CompanyID)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, result)
}
