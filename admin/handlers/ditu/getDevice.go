package ditu

import (
	"fmt"
	"net/http"
//	"strconv"

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"

	"druid/cattle/admin/middleware"
	"druid/cattle/models"
)

func GetDevice(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	id := c.Param("id")
	if !bson.IsObjectIdHex(id) {
		logger.Error("Area id is invalid.")
		c.AbortWithError(http.StatusBadRequest, models.Error(fmt.Sprintf("Area id:%s is invalid id", id)))
		return
	}

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	orignArea, err := models.FindDituAreaByID(db, bson.ObjectIdHex(id))
	if err != nil {
		logger.Error("Can't find ditu area by id.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Can't find ditu area by id.", err))
		return
	}

	if orignArea.CompanyID != user.CompanyID {
		logger.Error("You don't have permission to access other.")
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access other."))
		return
	}

	result, err := models.ListDeviceByFilter(db, bson.M{"_id": bson.M{"$in": orignArea.DevicesID}}, nil, models.UserSelect)
	if err != nil {
		logger.Error("List ditu area device failed. ", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List ditu area device failed.", err))
		return
	}

	c.JSON(http.StatusOK, result)
}
