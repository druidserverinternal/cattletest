package ditu

import (
	"fmt"
	"net/http"

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"

	"druid/cattle/admin/middleware"
	"druid/cattle/models"
)

func AddDevice(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	id := c.Param("id")
	if !bson.IsObjectIdHex(id) {
		c.AbortWithError(http.StatusBadRequest, models.Error(fmt.Sprintf("Ditu id:%s is invalid id", id)))
		return 
	}
	deviceID := c.Param("device")
	if !bson.IsObjectIdHex(id) {
		c.AbortWithError(http.StatusBadRequest, models.Error(fmt.Sprintf("Device id:%s is invalid id", id)))
		return 
	}

	device, err := models.FindDeviceByID(db, bson.ObjectIdHex(deviceID), nil)
	if err != nil {
		logger.Error("Find device by request failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Check device by request failed.", err))
		return
	}

	orignArea, err := models.FindDituAreaByID(db, bson.ObjectIdHex(id))
	if err != nil {
		logger.Error("Can't find ditu area by id.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Can't find ditu area by id.", err))
		return
	}

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if orignArea.CompanyID != me.CompanyID || device.CompanyID != me.CompanyID {
		logger.Error("You don't have permission to access other.")
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access other."))
		return
	}

	count, err := models.CountDituAreaByDeviceID(db, bson.ObjectIdHex(deviceID))
	if err != nil {
		logger.Error("Count area by device failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Count area by device failed.", err))
		return
	}
	if count >= models.MaxDituArea {
		logger.Error("The device had max area.")
		c.AbortWithError(http.StatusBadRequest, models.Error("The device had max area."))
		return
	}

	if err := orignArea.AddDevice(db, device); err != nil {
		logger.Error("Added device to area failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Added device to area failed.", err))
		return
	}

	if err := device.UpdateTotalArea(db); err != nil {
		logger.Error("Update total area failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Update total area failed.", err))
		return
	}

	if err := orignArea.UpdateTotalDevice(db); err != nil {
		logger.Error("Update total device failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Update total device failed.", err))
		return
	}

	c.Status(http.StatusCreated)
}
