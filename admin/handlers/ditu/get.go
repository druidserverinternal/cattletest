package ditu

import (
	"fmt"
	"net/http"

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"

	"druid/cattle/admin/middleware"
	"druid/cattle/models"
)

func Get(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	id := c.Param("id")
	if !bson.IsObjectIdHex(id) {
		logger.Error("Area id is invalid.", id)
		c.AbortWithError(http.StatusBadRequest, models.Error(fmt.Sprintf("Area id:%s is invalid id", id)))
		return
	}

	area, err := models.FindDituAreaByID(db, bson.ObjectIdHex(id))
	if err != nil {
		logger.Error("Can't find ditu area by id.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Can't find ditu area by id.", err))
		return
	}

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if area.CompanyID != me.CompanyID {
		logger.Error("You don't have permission to access.")
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access"))
		return
	}

	dh, err := models.ListDituHistoryByAreaID(db, area.ID)
	if err != nil {
		logger.Error("List ditu history by id.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List ditu history by id.", err))
		return
	}
	for _, d := range dh {
		area.DevicesID = append(area.DevicesID, d.DeviceID)
	}
	if len(dh) != area.TotalDevice {
		if err := area.UpdateTotalDevice(db); err != nil {
			logger.Error("Update total device failed.", err)
			c.AbortWithError(http.StatusInternalServerError, models.Error("Update total device failed.", err))
			return
		}
		area.TotalDevice = len(dh)
	}

	c.JSON(http.StatusOK, area)
}
