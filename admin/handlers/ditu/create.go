package ditu

import (
	"fmt"
	"net/http"
	"time"

	"gopkg.in/mgo.v2/bson"

	"github.com/gin-gonic/gin"
	//	"github.com/kellydunn/golang-geo"

	"druid/cattle/admin/middleware"
	"druid/cattle/models"
)

func createValidate(c *gin.Context) (*models.DituArea) {
	var ditu models.DituArea
	if err := c.BindJSON(&ditu); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	if ditu.ID != "" {
		c.AbortWithError(http.StatusBadRequest, models.Error("ID is invalid."))
		return nil
	}

	if ditu.Distance > 10000 {
		c.AbortWithError(http.StatusBadRequest, models.Error("The Round is too large."))
		return nil
	}

	if len(ditu.Description) > 90 {
		c.AbortWithError(http.StatusBadRequest, models.Error("Description too large."))
		return nil
	}

	if len(ditu.AreaName) == 0 || len(ditu.AreaName) > models.MaxDituAreaName {
		c.AbortWithError(http.StatusBadRequest, models.Error("Area name is invalid."))
		return nil
	}

	if ditu.Type != models.DituAreaTypePolygon && ditu.Type != models.DituAreaTypeRound {
		c.AbortWithError(http.StatusBadRequest, models.Error("Area type is invalid."))
		return nil
	}

	if ditu.MsgType != models.DituAreaMsgTypeIn && ditu.MsgType != models.DituAreaMsgTypeOut {
		c.AbortWithError(http.StatusBadRequest, models.Error("Area msg type is invalid."))
		return nil
	}
	return &ditu
}

func Create(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	ditu  := createValidate(c)
	if ditu == nil {
		logger.Error("Parse request failed.")
		return
	}

	userID := c.Param("id")
	if !bson.IsObjectIdHex(userID) {
		logger.Error("Create ditu to user invalid.")
		c.AbortWithError(http.StatusBadRequest, models.Error(fmt.Sprintf("User id:%s is invalid", userID)))
		return 
	}

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	user := models.FindUserByID(db, bson.ObjectIdHex(userID))
	if user.Role != models.RoleUser {
		logger.Error("You can't alloc ditu to admin.")
		c.AbortWithError(http.StatusForbidden, models.Error("You can't alloc ditu to admin."))
		return
	}

	if user.CompanyID != me.CompanyID {
		logger.Error("You don't have permission to access other.")
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access other."))
		return
	}

	ditu.CompanyID = me.CompanyID
	ditu.OwnerID = user.ID
	ditu.OwnerName = user.UserName

	switch ditu.Type {
	case models.DituAreaTypeRound:
		if ditu.Point == nil || ditu.Distance == 0 {
			logger.Error("Round is invalid.")
			c.AbortWithError(http.StatusBadRequest, models.Error("Round is invalid."))
			return
		}
	case models.DituAreaTypePolygon:
		gp := models.NewGeoPolygon(ditu.Polygon)

		if !gp.IsClosed() {
			logger.Error("Polygon is invalid.")
			c.AbortWithError(http.StatusBadRequest, models.Error("Polygon is invalid."))
			return
		}
	default:
		logger.Error("Type is invalid:", ditu.Type)
		c.AbortWithError(http.StatusBadRequest, models.Error("Type is invalid."))
		return
	}

	t := time.Now()
	ditu.UpdatedAt = &t
	ditu.ID = bson.NewObjectId()

	if err := ditu.Create(db); err != nil {
		logger.Error("Create map area failed. ", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Create map area failed.", err))
		return
	}

	if err := user.FixTotalArea(db); err != nil {
		logger.Error("Fix total map area failed. ", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Fix total map area failed.", err))
		return
	}

	c.JSON(http.StatusCreated, ditu)
}
