package ditu

import (
	"fmt"
	"net/http"

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"

	"druid/cattle/admin/middleware"
	"druid/cattle/models"
)

func updateValidate(c *gin.Context) (*models.DituArea) {
	var ditu models.DituArea
	if err := c.BindJSON(&ditu); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	id := c.Param("id")
	if !bson.IsObjectIdHex(id) {
		c.AbortWithError(http.StatusBadRequest, models.Error(fmt.Sprintf("Area id:%s is invalid id", id)))
		return nil
	}
	ditu.ID = bson.ObjectIdHex(id)

	if len(ditu.AreaName) == 0 || len(ditu.AreaName) > 90 {
		c.AbortWithError(http.StatusBadRequest, models.Error("Area name is invalid."))
		return nil
	}
	if len(ditu.Description) > 90 {
		c.AbortWithError(http.StatusBadRequest, models.Error("Description too large."))
		return nil
	}

	if ditu.Type != models.DituAreaTypePolygon && ditu.Type != models.DituAreaTypeRound {
		c.AbortWithError(http.StatusBadRequest, models.Error("Area type is invalid."))
		return nil
	}

	if ditu.MsgType != models.DituAreaMsgTypeIn && ditu.MsgType != models.DituAreaMsgTypeOut {
		c.AbortWithError(http.StatusBadRequest, models.Error("Area msg type is invalid."))
		return nil
	}

	return &ditu
}

func Update(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	ditu := updateValidate(c)
	if ditu == nil {
		logger.Error("Parse request failed.")
		return
	}

	orignArea, err := models.FindDituAreaByID(db, ditu.ID)
	if err != nil {
		logger.Error("Can't find ditu area by id.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Can't find ditu area by id.", err))
		return
	}

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if orignArea.CompanyID != user.CompanyID {
		logger.Error("You don't have permission to access other.")
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access other."))
		return
	}
	if user.Role == models.RoleUser && orignArea.OwnerID != user.ID {
		logger.Error("You don't have permission to access, you are not admin.")
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access."))
		return
	}

	switch ditu.Type {
	case models.DituAreaTypeRound:
		if ditu.Point == nil || ditu.Distance == 0 {
			logger.Error("Round is invalid.")
			c.AbortWithError(http.StatusBadRequest, models.Error("Round is invalid."))
			return
		}
	case models.DituAreaTypePolygon:
		gp := models.NewGeoPolygon(ditu.Polygon)
		if !gp.IsClosed() {
			logger.Error("Polygon is invalid.")
			c.AbortWithError(http.StatusBadRequest, models.Error("Polygon is invalid."))
			return
		}
	default:
		logger.Error("Type is invalid:", ditu.Type)
		c.AbortWithError(http.StatusBadRequest, models.Error("Type is invalid."))
		return
	}

	if err := ditu.Update(db); err != nil {
		logger.Error("Update map area failed. ", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Update map area failed.", err))
		return
	}

	if orignArea.AreaName != ditu.AreaName {
		if err := models.UpdateAreaNameByAreaID(db, orignArea.ID, ditu.AreaName); err != nil {
			logger.Error("Update area history name failed. ", err)
			c.AbortWithError(http.StatusInternalServerError, models.Error("Update area history name failed.", err))
			return
		}
	}


	c.Status(http.StatusCreated)
}
