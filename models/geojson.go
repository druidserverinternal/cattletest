package models

import (
	"github.com/paulmach/go.geo"
//	"github.com/paulmach/go.geo/reducers"
)


type Feature struct {
	Type		string		`json:"type"`
	Geometry	*GeoPoint	`json:"geometry"`
}

type Geo struct {
	Type	string		`json:"type"`
	Features	[]*Feature	`json:"features"`
}

func DouglasPeucker(gpss []*GPS, threshold float64) []*GPS {
	if len(gpss) <= 3 {
		return gpss
	}

	mask := make([]byte, len(gpss))
	mask[0] = 1
	mask[len(gpss) - 1] = 1


	found := dpWorker(gpss, threshold, mask)
	newPoints := make([]*GPS, 0, found)

	for i, v := range mask {
		if v == 1 {
			newPoints = append(newPoints, gpss[i])
		}
	}

	return newPoints
}

func dpWorker(points []*GPS, threshold float64, mask []byte) int {

	found := 0

	var stack []int
	stack = append(stack, 0, len(points)-1)

	l := &geo.Line{}
	for len(stack) > 0 {
		start := stack[len(stack)-2]
		end := stack[len(stack)-1]

		// modify the line in place
		a := l.A()
		a[0], a[1] = points[start].Longitude, points[start].Latitude

		b := l.B()
		b[0], b[1] = points[end].Longitude, points[end].Latitude

		maxDist := 0.0
		maxIndex := 0
		for i := start + 1; i < end; i++ {
			point := geo.NewPoint(points[i].Longitude, points[i].Latitude)
			dist := l.SquaredDistanceFrom(point)

			if dist > maxDist {
				maxDist = dist
				maxIndex = i
			}
		}

		if maxDist > threshold*threshold {
			found++
			mask[maxIndex] = 1

			stack[len(stack)-1] = maxIndex
			stack = append(stack, maxIndex, end)
		} else {
			stack = stack[:len(stack)-2]
		}
	}

	return found
}
