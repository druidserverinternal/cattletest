package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"time"
)

type DituHistory struct {
	ID			bson.ObjectId `json:"id" bson:"_id,omitempty"`
	AreaID		bson.ObjectId `json:"area_id" bson:"area_id"`
	AreaName	string        `json:"area_name,omitempty" bson:"area_name,omitempty"`
	DeviceID	bson.ObjectId   `json:"device_id" bson:"device_id"`
	Mark		int             `json:"mark,omitempty" bson:"mark,omitempty"`
	MsgType		int				`json:"msg_type,omitempty" bson:"msg_type,omitempty"`
	SendedAt	*time.Time		`json:"sended_at,omitempty" bson:"sended_at,omitempty"`
	OwnerID		bson.ObjectId   `json:"owner_id" bson:"owner_id"`
}

func (dh *DituHistory)Create(db *mgo.Session) error {
	c := db.DB(DruidDB).C(DituHistoryCollection)

	if _, err := c.Upsert(
			bson.M{
				"area_id":	dh.AreaID,
				"device_id": dh.DeviceID,
			},
			bson.M{ 
				"$set": dh,
			},
		); err != nil {
		return err
	}

	return nil
}

func MarkDituHistorySended(db *mgo.Session, deviceID, areaID bson.ObjectId) (error) {
	c := db.DB(DruidDB).C(DituHistoryCollection)

	if err := c.Update(bson.M{"device_id": deviceID, "area_id": areaID}, bson.M{"$set": bson.M{"sended_at": time.Now()}}); err != nil {
		return err
	}

	return nil
}

func MarkDituHistoryUnSended(db *mgo.Session, deviceID, areaID bson.ObjectId) (error) {
	c := db.DB(DruidDB).C(DituHistoryCollection)

	if err := c.Update(bson.M{"device_id": deviceID, "area_id": areaID}, bson.M{"$unset": bson.M{"sended_at": 1}}); err != nil {
		return ErrorConvert(err)
	}

	return nil
}



func FindDituHistory(db *mgo.Session, deviceID, areaID bson.ObjectId) (*DituHistory, error) {
	c := db.DB(DruidDB).C(DituHistoryCollection)
	dh := &DituHistory{}

	if err := c.Find(bson.M{"device_id": deviceID, "area_id": areaID}).One(dh); err != nil {
		return nil, ErrorConvert(err)
	}

	return dh, nil
}

/*
func DeleteFromDituHistory(db *mgo.Session, ds []bson.ObjectId, areaID bson.ObjectId) (error) {
	c := db.DB(DruidDB).C(DituHistoryCollection)

	if _, err := c.RemoveAll(bson.M{"device_id": bson.M{"$in": ds}, "area_id": areaID}); err != nil {
		return ErrorConvert(err)
	}

	return nil
}
*/

func DeleteDituHistoryByOwnerID(db *mgo.Session, ownerID bson.ObjectId) (error) {
	c := db.DB(DruidDB).C(DituHistoryCollection)

	if _, err := c.RemoveAll(bson.M{"owner_id": ownerID}); err != nil {
		return ErrorConvert(err)
	}

	return nil
}

func DeleteDituHistoryByDeviceID(db *mgo.Session, ids []bson.ObjectId) (error) {
	c := db.DB(DruidDB).C(DituHistoryCollection)

	if _, err := c.RemoveAll(bson.M{"device_id": bson.M{"$in": ids}}); err != nil {
		return ErrorConvert(err)
	}

	return nil
}

func DeleteDituHistoryByFilter(db *mgo.Session, m bson.M) error {
	c := db.DB(DruidDB).C(DituHistoryCollection)
	if _, err := c.RemoveAll(m); err != nil {
		return ErrorConvert(err)
	}

	return nil
}

func ListDituHistoryByDeviceID(db *mgo.Session, id bson.ObjectId) ([]*DituHistory, error) {
	c := db.DB(DruidDB).C(DituHistoryCollection)
	area := []*DituHistory{}
	return area, MongoList(c, bson.M{"device_id": id}, &area, nil, nil)
}

func ListDituHistoryByAreaID(db *mgo.Session, id bson.ObjectId) ([]*DituHistory, error) {
	c := db.DB(DruidDB).C(DituHistoryCollection)
	area := []*DituHistory{}
	return area, MongoList(c, bson.M{"area_id": id}, &area, nil, nil)
}

func CountDituAreaByDeviceID(db *mgo.Session, id bson.ObjectId) (count int, err error) {
	c := db.DB(DruidDB).C(DituHistoryCollection)
	return MongoCount(c, bson.M{"device_id": id})
}

func CountDeviceByAreaID(db *mgo.Session, id bson.ObjectId) (count int, err error) {
	c := db.DB(DruidDB).C(DituHistoryCollection)
	return MongoCount(c, bson.M{"area_id": id})
}


func UpdateAreaNameByAreaID(db *mgo.Session, areaID bson.ObjectId, areaName string) (error) {
	c := db.DB(DruidDB).C(DituHistoryCollection)

	if err := c.Update(bson.M{"area_id": areaID}, bson.M{"$set": bson.M{"area_name": areaName}}); err != nil {
		return ErrorConvert(err)
	}

	return nil
}

func DeleteAreaHistoryByAreaID(db *mgo.Session, areaID bson.ObjectId) (error) {
	c := db.DB(DruidDB).C(DituHistoryCollection)

	if _, err := c.RemoveAll(bson.M{"area_id": areaID}); err != nil {
		return ErrorConvert(err)
	}

	return nil
}





