package models

import (
	"errors"
	"time"
	//	"fmt"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

/*
type Base struct {
	ID				bson.ObjectId	`json:"id" bson:"_id,omitempty"`
}
*/

// mongodb db name and collection name
const (
	DruidDB  = "cattle"
	DruidMap = "map"

	CompanyCollection        = "company"
	UserCollection           = "user"
	FirmwareCollection       = "firmware"
	DeviceCollection         = "device"
	SettingCollection        = "setting"
	GPSCollection            = "gps"
	CellularCollection       = "cellular"
	BehaviorCollection       = "behavior"
	Behavior2Collection      = "behavior2"
	SIMCollection            = "sim"
	StatusCollection         = "status"
	CattleCollection          = "cattle"
	BiologicalTypeCollection = "biological_type"
	DituAreaCollection       = "ditu_area"
	DituHistoryCollection    = "ditu_history"
	GPSStatisticsCollection  = "gps_statistics"
	RoomCollection           = "room"
	StatisticsODBACollection = "statistics_odba"
	StatisticsProcessCollection = "statistics_process"
	//	FileCollection      = "file"
	WeixinCollection          = "weixin"
	SettingHistoryCollection  = "setting_history"
	FirmwareHistoryCollection = "firmware_history"
	ImageCollection           = "image"
	//	StatisticsCollection     = "statistics"

	BillHistoryCollection = "bill_history"

	OwnerDruidOldCollection = "owner_druid_old"
	MessageCollection       = "message"
	PushSettingCollection   = "push_setting"

	StatisticsRoleCollection         = "statistics_role"
	GPSStatisticsTimestampCollection = "gps_statistics_timestamp"

	SNCollection = "sn"

	// for sync mongodb
	SyncMongoCollection = "sync_mongo"

	// for file store
	FilesCollection = "files"

	// for map offset
	OffsetCollection = "offset"

	//for app versions
	AppVersionCollection = "app_version"

	//for BehaviorAlgorithm
	BehaviorAlgoCollection = "behavior_algo"

	//for ODBAAlgorithm
	ODBAAlgoCollection = "odba_algo"

	//for BehaviorMotion History
	BehaviorTypeHistoryCollection = "behavior_type_history"
	
	//for ODBAParameter  
	ODBAParameterCollection = "odba_parameter"

	//for app versions 
	AppVersionHistroyCollection = "app_version_histroy"
	
	//for ODBAParameter  
	ODBALovingCollection = "odba_loving"
)

const (
	DeviceTypeNeckRing2G = 1
	DeviceTypeBackpack   = 2
	DeviceTypeNeckRing3G = 3
)

const (
	SNTypeBiological = "biological"
	SNTypeCattle      = "cattle"
)

const (
	LocationAccuracy    = 1E7
	AltitudeAccuracy    = 1E2
	TemperatureAccuracy = 1E1
	VoltageAccuracy     = 1E3
	HorizontalAccuracy  = 1E1
	VerticalAccuracy    = 1E1
	SpeedAccuracy       = 1E1
	CourseAccuracy      = 1E1
)

const (
	LockTimeout        = time.Minute
	HTTPRequestTimeout = time.Second * 10
)

type AggregateCount struct {
	Count	int		`json:"count" bson:"count"`
}

// ZeroObjectID is zero mongo id
var ZeroObjectID bson.ObjectId

// DeleteByDBRef is delete document by dbref
func DeleteByDBRef(db *mgo.Session, myRef mgo.DBRef) error {
	if myRef.Collection != "" {
		c := db.DB(myRef.Database).C(myRef.Collection)
		if err := c.UpdateId(myRef.Id, bson.M{"$set": bson.M{"deleted_at": time.Now()}}); err != nil {
			return err
		}
	}
	return nil
}

func RecoverByDBRef(db *mgo.Session, myRef mgo.DBRef) error {
	if myRef.Collection != "" {
		c := db.DB(myRef.Database).C(myRef.Collection)
		if err := c.UpdateId(myRef.Id, bson.M{"$unset": bson.M{"deleted_at": 1}}); err != nil {
			return err
		}
	}
	return nil
}

/*
func DeleteByID(db *mgo.Session, name string, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(name)
	if err := c.UpdateId(id, bson.M{"$set": bson.M{"deleted_at": time.Now()}}); err != nil {
		return err
	}
	return nil
}

func DeleteByItem(db *mgo.Session, name string, m bson.M) error {
	c := db.DB(DruidDB).C(name)
	if err := c.Update(m, bson.M{"$set": bson.M{"deleted_at": time.Now()}}); err != nil {
		return err
	}
	return nil
}
*/

// ErrorConvert is check error is item unexists
func ErrorConvert(err error) error {
	switch err {
	case mgo.ErrNotFound:
		return nil
	default:
		return err
	}
}

func MongoListLimit(c *mgo.Collection, m interface{}, result interface{}, rp *RequestParameter, se bson.M) error {
	q := c.Find(m).Limit(MaxLimit * 1.5)
	if rp != nil {
		if len(rp.Sort) > 0 {
			q = q.Sort(rp.Sort...)
		}
		q = q.Skip(rp.Offset)
		if rp.Limit > 0 {
			q = q.Limit(rp.Limit)
		}
	}
	if se != nil {
		q = q.Select(se)
	}
	if err := q.All(result); err != nil {
		return ErrorConvert(err)
	}

	return nil
}

// MongoFind find a collection
func MongoFind(c *mgo.Collection, m interface{}, result interface{}, sort string, se bson.M) error {
	q := c.Find(m)
	if sort != "" {
		q = q.Sort(sort)
	}
	q = q.Limit(1)
	if se != nil {
		q = q.Select(se)
	}
	return q.One(result)
}

// MongoList all of sim info
func MongoList(c *mgo.Collection, m interface{}, result interface{}, rp *RequestParameter, se bson.M) error {
	q := c.Find(m)
	if rp != nil {
		if len(rp.Sort) > 0 {
			q = q.Sort(rp.Sort...)
		}
		q = q.Skip(rp.Offset)
		if rp.Limit > 0 {
			q = q.Limit(rp.Limit)
		}
	}
	if se != nil {
		q = q.Select(se)
	}
	if err := q.All(result); err != nil {
		return ErrorConvert(err)
	}

	return nil
}

type MergeArg struct {
	Match	bson.M
	FinalMatch	bson.M
	From	string
	LocalField	string
	ForeignField	string
	As			string
	AddField	bson.M
}

func MongoCountByFinalMatch(c *mgo.Collection, arg *MergeArg) (int, error) {
	query := []bson.M{
		bson.M{
			"$match": arg.Match,
		},
	}

	lookup :=  bson.M{
				"from":			arg.From,
				"localField":	arg.LocalField,
				"foreignField":	arg.ForeignField,
				"as":			arg.As,
			}
	query = append(query, bson.M{"$lookup": lookup})

	ar := bson.M{"$arrayElemAt": InterfaceArray("$" + arg.As, 0)}
	replaceRoot := bson.M{
				"newRoot":	bson.M{
					"$mergeObjects": InterfaceArray(ar, "$$ROOT"),
				},
			}
	query = append(query, bson.M{"$replaceRoot": replaceRoot})

	if arg.AddField != nil {
		query = append(query, bson.M{"$addFields": arg.AddField})
	}


	if arg.FinalMatch != nil {
		query = append(query, bson.M{"$match": arg.FinalMatch})
	}

	query = append(query, bson.M{"$group": bson.M{"_id": nil, "count": bson.M{"$sum": 1}}})

	count := AggregateCount{}
	pipe := c.Pipe(query)
	if err := pipe.One(&count); err != nil {
		return 0, err
	}
	return count.Count, nil
}

func MongoListMergeFinalMatch(c *mgo.Collection, arg *MergeArg, result interface{}, rp *RequestParameter, sel bson.M) error {
	query := []bson.M{
		bson.M{
			"$match": arg.Match,
		},
	}

	lookup :=  bson.M{
				"from":			arg.From,
				"localField":	arg.LocalField,
				"foreignField":	arg.ForeignField,
				"as":			arg.As,
			}
	query = append(query, bson.M{"$lookup": lookup})

	ar := bson.M{"$arrayElemAt": InterfaceArray("$" + arg.As, 0)}
	replaceRoot := bson.M{
				"newRoot":	bson.M{
					"$mergeObjects": InterfaceArray(ar, "$$ROOT"),
				},
			}
	query = append(query, bson.M{"$replaceRoot": replaceRoot})

	if arg.AddField != nil {
		query = append(query, bson.M{"$addFields": arg.AddField})
	}

	if sel != nil {
		query = append(query, bson.M{"$project": sel})
	}

	if arg.FinalMatch != nil {
		query = append(query, bson.M{"$match": arg.FinalMatch})
	}

	if rp != nil {
		if len(rp.Sort) > 0 {
			q := bson.M{}
			for _, s := range rp.Sort {
				if s[0] == '-' {
					q[s[1:]] = -1
				} else {
					q[s] = 1
				}
			}
			query = append(query, bson.M{"$sort": q})
		}
		if rp.Offset > 0 {
			query = append(query, bson.M{"$skip": rp.Offset})
		}
		if rp.Limit > 0 {
			query = append(query, bson.M{"$limit": rp.Limit})
		}
	}


	pipe := c.Pipe(query)
	if err := pipe.All(result); err != nil {
		return ErrorConvert(err)
	}
	return nil
}


func MongoListMerge(c *mgo.Collection, arg *MergeArg, result interface{}, rp *RequestParameter, sel bson.M) error {
//	var mo []interface{}
//	var ar []interface{}
//	ar = append(ar, "$data")
//	ar = append(ar, 0)
//	mo = append(mo, bson.M{"$arrayElemAt": ar})
//	mo = append(mo, "$$ROOT")

	query := []bson.M{
		bson.M{
			"$match": arg.Match,
		},
	}

	if rp != nil {
		if len(rp.Sort) > 0 {
			q := bson.M{}
			for _, s := range rp.Sort {
				if s[0] == '-' {
					q[s[1:]] = -1
				} else {
					q[s] = 1
				}
			}
			query = append(query, bson.M{"$sort": q})
		}
		if rp.Offset > 0 {
			query = append(query, bson.M{"$skip": rp.Offset})
		}
		if rp.Limit > 0 {
			query = append(query, bson.M{"$limit": rp.Limit})
		}
	}

	lookup :=  bson.M{
				"from":			arg.From,
				"localField":	arg.LocalField,
				"foreignField":	arg.ForeignField,
				"as":			arg.As,
			}
	query = append(query, bson.M{"$lookup": lookup})

	ar := bson.M{"$arrayElemAt": InterfaceArray("$" + arg.As, 0)}
	replaceRoot := bson.M{
				"newRoot":	bson.M{
					"$mergeObjects": InterfaceArray(ar, "$$ROOT"),
				},
			}
	query = append(query, bson.M{"$replaceRoot": replaceRoot})

	if arg.AddField != nil {
		query = append(query, bson.M{"$addFields": arg.AddField})
	}

	if sel != nil {
		query = append(query, bson.M{"$project": sel})
	}

	/*
	pipe := c.Pipe([]bson.M{
		bson.M{
			"$match": m,
		}, bson.M{
			"$lookup": bson.M{
				"from":			CattleCollection,
				"localField":	"_id",
				"foreignField":	"device_id",
				"as":			"data",
			},
		}, bson.M{
			"$replaceRoot": bson.M{
				"newRoot":	bson.M{
					"$mergeObjects": mo,
				},
			},
		}, bson.M{
			"$project": bson.M{
				"data": 0,
			},
		},
		})
		*/

	// db.device.aggregate([ {$match: {uuid: "3e0033000651353334393332"}}, {$lookup: { from: "bird", localField: "_id", foreignField: "device_id", as: "data" }}, {$replaceRoot: { newRoot: { $mergeObjects: [ { $arrayElemAt: [ "$data", 0 ] }, "$$ROOT" ] } }}, { $project: { data: 0 } }])
	pipe := c.Pipe(query)
	if err := pipe.All(result); err != nil {
		return ErrorConvert(err)
	}
	return nil
}

func MongoGetMerge(c *mgo.Collection, arg *MergeArg, result interface{}, sel bson.M) error {
	/*
	var mo []interface{}
	var ar []interface{}
	ar = append(ar, "$data")
	ar = append(ar, 0)
	mo = append(mo, bson.M{"$arrayElemAt": ar})
	mo = append(mo, "$$ROOT")
	*/

	query := []bson.M{
		bson.M{
			"$match": arg.Match,
		},
	}

	lookup :=  bson.M{
				"from":			arg.From,
				"localField":	arg.LocalField,
				"foreignField":	arg.ForeignField,
				"as":			"data",
			}
	query = append(query, bson.M{"$lookup": lookup})

	ar := bson.M{"$arrayElemAt": InterfaceArray("$" + arg.As, 0)}
	replaceRoot := bson.M{
				"newRoot":	bson.M{
					"$mergeObjects": InterfaceArray(ar, "$$ROOT"),
			//		"$mergeObjects": mo,
				},
			}
	query = append(query, bson.M{"$replaceRoot": replaceRoot})

	if arg.AddField != nil {
		query = append(query, bson.M{"$addFields": arg.AddField})
	}

	if sel != nil {
		query = append(query, bson.M{"$project": sel})
	}

	pipe := c.Pipe(query)
	if err := pipe.One(result); err != nil {
		return ErrorConvert(err)
	}
	return nil
}

func CollectionCount(c *mgo.Collection) (count int, err error) {
	if count, err = c.Count(); err != nil {
		return 0, ErrorConvert(err)
	}
	return count, nil
}

// GPSCount get count
func MongoCount(c *mgo.Collection, m interface{}) (count int, err error) {
	if count, err = c.Find(m).Count(); err != nil {
		return 0, ErrorConvert(err)
	}
	return count, nil
}

func MongoRecover(c *mgo.Collection, m interface{}) error {
	if _, err := c.UpdateAll(m, bson.M{"$unset": bson.M{"deleted_at": 1}}); err != nil {
		return ErrorConvert(err)
	}
	return nil
}

// MongoDelete mark deleted_at
func MongoDelete(c *mgo.Collection, m interface{}) error {
	if _, err := c.UpdateAll(m, bson.M{"$set": bson.M{"deleted_at": time.Now()}}); err != nil {
		return ErrorConvert(err)
	}
	return nil
}

// MongoDestroy destroy deleted device
func MongoDestroy(c *mgo.Collection, m interface{}) error {
	_, err := c.RemoveAll(m)
	if err != nil {
		return err
	}
	return nil
}

// DestroyByDBRef is destroy document by dbref
func DestroyByDBRef(db *mgo.Session, myRef mgo.DBRef) error {
	if myRef.Collection != "" {
		c := db.DB(myRef.Database).C(myRef.Collection)
		return MongoDestroy(c, bson.M{"_id": myRef.Id})
	}
	return nil
}

func CheckChangeInfo(info *mgo.ChangeInfo) error {
	if info.Matched == 0 && info.Removed == 0 && info.Updated == 0 && info.UpsertedId == nil {
		return errors.New("Change null result")
	}
	return nil
}

// MongoLock a field
func MongoLock(c *mgo.Collection, m bson.M) error {
	change := mgo.Change{
		Update:    bson.M{"$set": bson.M{"lock": time.Now()}},
		ReturnNew: true,
	}
	m["lock"] = nil
	info, err := c.Find(m).Apply(change, nil)
	if ErrorConvert(err) != nil {
		return nil
	}

	if info != nil {
		if info.Updated == 0 {
			return errors.New("field is invalid")
		}
		return nil
	}

	step := time.Now().Add(0 - LockTimeout)
	change2 := mgo.Change{
		Update: bson.M{"$set": bson.M{"lock": time.Now()}},
	}
	m["lock"] = bson.M{"$lt": &step}
	info, err = c.Find(m).Apply(change2, nil)
	if ErrorConvert(err) != nil {
		return nil
	}

	if info != nil {
		if info.Updated == 0 {
			return errors.New("field is still invalid")
		}
		return nil
	}
	return errors.New("field is locked")
}

// MongoUnLock a field
func MongoUnLock(c *mgo.Collection, m interface{}) error {
	if _, err := c.UpdateAll(m, bson.M{"$unset": bson.M{"lock": 1}}); err != nil {
		return err
	}
	return nil
}

func MongoLockTimeout(c *mgo.Collection, m bson.M, u time.Time) error {
	for {
		if time.Now().After(u) {
			return errors.New("Lock mongodb timeout.")
		}
		if err := MongoLock(c, m); err != nil {
			//			fmt.Println("Still locked, ", err, time.Now(), u)
			time.Sleep(time.Millisecond * 100)
			continue
		}
		return nil
	}
}

func MongoAtomInsert(c *mgo.Collection, m interface{}, obj interface{}, result interface{}) (interface{}, error) {
	change := mgo.Change{
		Update:    bson.M{"$setOnInsert": obj},
		Upsert:    true,
		ReturnNew: true,
	}

	info, err := c.Find(m).Apply(change, result)
	if err != nil {
		return nil, err
	} else if info.UpsertedId == "" {
		return nil, errors.New("Upsert null.")
	}

	return result, nil
}

func MongoFindUpdateAndInsert(c *mgo.Collection, m interface{}, obj interface{}, result interface{}) (interface{}, error) {
	change := mgo.Change{
		Update:    obj,
		Upsert:    true,
		ReturnNew: true,
	}

	info, err := c.Find(m).Apply(change, result)
	if err != nil {
		return nil, err
	} else if info.Updated == 0 && info.UpsertedId == "" {
		return nil, errors.New("Upsert null.")
	}

	return result, nil
}

