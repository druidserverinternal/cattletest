package models

import (
	"errors"
	"gopkg.in/mgo.v2/bson"
	"reflect"
	"strings"
	"time"

	"bytes"
	"github.com/tealeg/xlsx"
	"strconv"
)

const (
	ExcelUUID   = "UUID"
	ExcelSN     = "SN"
	ExcelImsi   = "IMSI"
	ExcelNumber = "TelNo"
)

type Export struct {
	Search `json:"search,omitempty"`
	Type   string   `json:"type"`
	Field  []string `json:"field"`
}

// GPSToExcel convert gps to excel
func GPSToExcel(gpss []*GPS) ([]byte, error) {
	file := xlsx.NewFile()
	sheet, err := file.AddSheet("Sheet1")
	if err != nil {
		return nil, err
	}

	row := sheet.AddRow()
	to := reflect.TypeOf(gpss[0]).Elem()
	for i := 0; i < to.NumField(); i++ {
		row.AddCell().Value = to.Field(i).Name
	}

	for _, gps := range gpss {
		row = sheet.AddRow()

		to := reflect.TypeOf(gps).Elem()
		vo := reflect.ValueOf(gps).Elem()
		for i := 0; i < vo.NumField(); i++ {
			f := to.Field(i)
			v := vo.Field(i)

			cell := row.AddCell()
			cell.Value = ""

			switch f.Type.String() {
			case "string":
				cell.Value = v.Interface().(string)
			case "int32":
				val := v.Interface().(int32)
				cell.Value = strconv.Itoa(int(val))
			case "int":
				val := v.Interface().(int)
				cell.Value = strconv.Itoa(int(val))
			case "float64":
				val := v.Interface().(float64)
				cell.Value = strconv.FormatFloat(val, 'f', -1, 64)
			case "*time.Time":
				if t := v.Interface().(*time.Time); t != nil {
					cell.Value = t.Format(time.RFC3339)
				}
			case "bson.ObjectId":
				cell.Value = v.Interface().(bson.ObjectId).Hex()
			}
		}
	}

	style := xlsx.NewStyle()
	style.Alignment.Horizontal = "right"
	style.ApplyAlignment = true
	for _, col := range sheet.Cols {
		col.SetStyle(style)
	}

	buff := new(bytes.Buffer)
	file.Write(buff)
	return buff.Bytes(), nil
}

type DeviceIndex struct {
	UUID  string
	Index string
}

// ExcelToDeviceIndex convert excel index
func ExcelToDeviceIndex(data []byte) ([]*DeviceIndex, error) {
	file, err := xlsx.OpenBinary(data)
	if err != nil {
		return nil, err
	}

	di := []*DeviceIndex{}
	for _, sheet := range file.Sheets {
		if len(sheet.Rows) < 2 {
			continue
		}

		cell := sheet.Rows[0].Cells
		snIdx := -1
		uuidIdx := -1
		for i, _ := range cell {
			key := cell[i].String()
			switch strings.ToUpper(key) {
			case ExcelSN:
				snIdx = i
			case ExcelUUID:
				uuidIdx = i
			}
		}
		if snIdx < 0 || uuidIdx < 0 {
			continue
		}

		for i, row := range sheet.Rows {
			if i == 0 {
				continue
			}

			cell := row.Cells

			uuid := cell[uuidIdx].String()
			if err != nil {
				return nil, errors.New("Convert uuid failed.")
			}
			sn := cell[snIdx].String()
			if err != nil {
				continue
			}
			if uuid == "" || sn == "" {
				continue
			}
			di = append(di, &DeviceIndex{strings.ToLower(uuid), sn})
		}
	}
	return di, nil
}

// ExcelToSIM convert excel index
func ExcelToSIM(data []byte) ([]*SIM, error) {
	file, err := xlsx.OpenBinary(data)
	if err != nil {
		return nil, err
	}

	di := []*SIM{}
	for _, sheet := range file.Sheets {
		if len(sheet.Rows) < 2 {
			continue
		}

		cell := sheet.Rows[0].Cells
		imsiIdx := -1
		numberIdx := -1
		for i, _ := range cell {
			key := cell[i].String()
			switch key {
			case ExcelImsi:
				imsiIdx = i
			case ExcelNumber:
				numberIdx = i
			}
		}
		if imsiIdx < 0 || numberIdx < 0 {
			continue
		}

		for i, row := range sheet.Rows {
			if i == 0 {
				continue
			}
			cell := row.Cells

			imsi := cell[imsiIdx].String()
			if err != nil {
				return nil, errors.New("Convert imsi failed.")
			}
			number := cell[numberIdx].String()
			if err != nil {
				return nil, errors.New("Convert number failed.")
			}
			di = append(di, &SIM{
				IMSI:      imsi,
				SIMNumber: number})
		}
	}
	return di, nil
}

// ExcelToDevice convert excel index
func ExcelToDevice(data []byte) ([]string, error) {
	file, err := xlsx.OpenBinary(data)
	if err != nil {
		return nil, err
	}

	di := []string{}
	for _, sheet := range file.Sheets {
		if len(sheet.Rows) < 2 {
			continue
		}

		cell := sheet.Rows[0].Cells
		uuidIdx := -1
		for i, _ := range cell {
			key := cell[i].String()
			switch strings.ToUpper(key) {
			case ExcelUUID:
				uuidIdx = i
			}
		}
		if uuidIdx < 0 {
			continue
		}

		for i, row := range sheet.Rows {
			if i == 0 {
				continue
			}

			cell := row.Cells

			uuid := cell[uuidIdx].String()
			if err != nil {
				return nil, errors.New("Convert uuid failed.")
			}
			if uuid == "" {
				continue
			}
			di = append(di, strings.ToLower(uuid))
		}
	}
	return di, nil
}

func CheckFields(field string, fields []string) bool {
	for _, s := range fields {
		if field == s {
			return true
		}
	}
	return false
}

func GetTag(tag string) string {
	strs := strings.Split(tag, ",")
	if strs[0] == "-" {
		return ""
	}
	return strs[0]
}

func FieldToExcel(in interface{}, fields []string) ([]byte, error) {
	inters := reflect.ValueOf(in)
	if inters.Len() == 0 {
		return nil, Error("Can't convert null data.")
	}

	file := xlsx.NewFile()
	sheet, err := file.AddSheet("Sheet1")
	if err != nil {
		return nil, err
	}

	// reflect.ValueOf(inter).Index(0).Field(0).Type().String()

	value := inters.Index(0)
	if value.Kind() == reflect.Ptr {
		value = value.Elem()
	}

	colName := sheet.AddRow()
	for i := 0; i < value.NumField(); i++ {
		tag := GetTag(value.Type().Field(i).Tag.Get("json"))
		if CheckFields(tag, fields) {
			colName.AddCell().Value = tag
		}
	}

	for i := 0; i < inters.Len(); i++ {
		row := sheet.AddRow()

		vo := inters.Index(i)
		if vo.Kind() == reflect.Ptr {
			vo = vo.Elem()
		}

		to := vo.Type()
		for i := 0; i < vo.NumField(); i++ {
			f := to.Field(i)
			v := vo.Field(i)

			if !CheckFields(GetTag(f.Tag.Get("json")), fields) {
				continue
			}

			cell := row.AddCell()
			cell.Value = ""

			switch f.Type.String() {
			case "string":
				cell.Value = v.Interface().(string)
			case "int32":
				val := v.Interface().(int32)
				cell.Value = strconv.Itoa(int(val))
			case "int":
				val := v.Interface().(int)
				cell.Value = strconv.Itoa(int(val))
			case "float64":
				val := v.Interface().(float64)
				cell.Value = strconv.FormatFloat(val, 'f', -1, 64)
			case "*time.Time":
				if t := v.Interface().(*time.Time); t != nil {
					cell.Value = t.Format(time.RFC3339)
				}
			case "bson.ObjectId":
				cell.Value = v.Interface().(bson.ObjectId).Hex()
			}
		}
	}

	style := xlsx.NewStyle()
	style.Alignment.Horizontal = "right"
	style.ApplyAlignment = true
	for _, col := range sheet.Cols {
		col.SetStyle(style)
	}

	buff := new(bytes.Buffer)
	file.Write(buff)
	return buff.Bytes(), nil
}
