package models

import (
	/*
		"fmt"
	*/
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
)

// Status is status object with mongodb
type Status struct {
	ID              bson.ObjectId `json:"id" bson:"_id,omitempty"`
	DeviceID        bson.ObjectId `json:"device_id" bson:"device_id"`
	UUID            string        `json:"uuid" bson:"uuid"`
	Mark            int           `json:"mark" bson:"mark,omitempty"`
	Owner           string        `json:"owner,omitempty" bson:"owner,omitempty"`
	FirmwareVersion uint32        `json:"firmware_version" bson:"firmware_version"`
	Timestamp       *time.Time    `json:"timestamp" bson:"timestamp"`
	UpdatedAt       *time.Time    `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
	StatusType      uint32        `json:"status_type" bson:"status_type,omitempty"`
	StatusValue     uint32        `json:"status_value" bson:"status_value,omitempty"`
}

// Upload a status
func (status *Status) Upload(db *mgo.Session) error {
	c := db.DB(DruidDB).C(StatusCollection)
	if err := c.Insert(status); err != nil {
		return err
	}
	return nil
}

func (status *Status) UpsertByTimestamp(db *mgo.Session) error {
	c := db.DB(DruidDB).C(StatusCollection)
	if _, err := c.Upsert(
		bson.M{"device_id": status.DeviceID, "timestamp": status.Timestamp},
		bson.M{"$set": status}); err != nil {
		return err
	}
	return nil
}

// ListStatusByDeviceID list all of status by device id
func ListStatusByDeviceID(db *mgo.Session, id bson.ObjectId, rp *RequestParameter) ([]*Status, error) {
	c := db.DB(DruidDB).C(StatusCollection)
	status := []*Status{}
	return status, MongoList(c, bson.M{"device_id": id}, &status, rp, nil)
}

// ListStatus all of status
func ListStatus(db *mgo.Session, rp *RequestParameter) ([]*Status, error) {
	c := db.DB(DruidDB).C(StatusCollection)
	status := []*Status{}
	return status, MongoList(c, bson.M{"deleted_at": nil}, &status, rp, nil)
}

// ListStatusByFilter filter of status
func ListStatusByFilter(db *mgo.Session, m interface{}, rp *RequestParameter, se bson.M) ([]*Status, error) {
	c := db.DB(DruidDB).C(StatusCollection)
	result := []*Status{}
	return result, MongoList(c, m, &result, rp, se)
}

// DeleteStatusByDeviceID a device all of status
func DeleteStatusByDeviceID(db *mgo.Session, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(StatusCollection)
	return MongoDelete(c, bson.M{"device_id": id})
}

// DestroyStatusByDeviceID a device all of status
func DestroyStatusByDeviceID(db *mgo.Session, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(StatusCollection)
	return MongoDestroy(c, bson.M{"device_id": id})
}

// StatusCount get count device
func StatusCount(db *mgo.Session) (count int, err error) {
	c := db.DB(DruidDB).C(StatusCollection)
	return MongoCount(c, bson.M{"deleted_at": nil})
}

// StatusCountByDeviceID get count status of one device
func StatusCountByDeviceID(db *mgo.Session, id bson.ObjectId) (count int, err error) {
	c := db.DB(DruidDB).C(StatusCollection)
	return MongoCount(c, bson.M{"device_id": id})
}

func RecoverStatusByDeviceID(db *mgo.Session, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(StatusCollection)
	return MongoRecover(c, bson.M{"device_id": id})
}

func UpdateStatusMark(db *mgo.Session, id bson.ObjectId, mark int) error {
	c := db.DB(DruidDB).C(StatusCollection)
	if _, err := c.UpdateAll(
		bson.M{"device_id": id},
		bson.M{"$set": bson.M{
			"mark": mark}}); err != nil {
		return err
	}
	return nil
}

// GetStatusQueue device hardware and firmware version
func GetStatusQueue(db *mgo.Session) *mgo.Bulk {
	c := db.DB(DruidDB).C(StatusCollection)
	return c.Bulk()
}

// SearchStatus search status , filter by m and rp
func SearchStatus(db *mgo.Session, m bson.M, rp *RequestParameter) ([]*Status, error) {
	c := db.DB(DruidDB).C(StatusCollection)
	status := []*Status{}
	return status, MongoList(c, m, &status, rp, nil)
}

// SearchStatusCount search status count
func SearchStatusCount(db *mgo.Session, m bson.M) (count int, err error) {
	c := db.DB(DruidDB).C(StatusCollection)
	return MongoCount(c, m)
}
