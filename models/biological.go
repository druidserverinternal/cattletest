package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	BiologicalTypeCattle	=	"cattle"

	BiologicalGenderUnknown	=	0
	BiologicalGenderMale	=	1
	BiologicalGenderFemale	=	2
)

const (
	CattleSpecieOther	=	0
	CattleSpecieHeSiTan	=	1
	CattleSpecieJuanShan	=	2
	CattleSpecieAiErXia	=	3
	CattleSpecieChinaHeSiTan	=	4
	CattleSpecieRuiSiHeNiu	=	5

	CattleSpecieUnknown	=	6

	CattleSpecieCow = 100
	CattleSpecieSheep = 101
	CattleSpeciePig = 102
	CattleSpecieHorse = 103
	CattleSpecieCamel = 104 
	CattleSpecieDonkey = 105
)

// Biological interface
type Biological interface {
	FindByDeviceID(*mgo.Session, bson.ObjectId) error
	GetDBRef() mgo.DBRef
	InsertImage(*mgo.Session, *Image) error
	GetID() bson.ObjectId
	RemoveImage(*mgo.Session, []bson.ObjectId) error
	CleanImage(*mgo.Session)
}

const (
	BiologicalMaxImages		=	3
)

type BiologicalType struct {
	ID		bson.ObjectId   `json:"id" bson:"_id,omitempty"`
	Type	string			`json:"type" bson:"type"`
	Species	[]string		`json:"species" bson:"species"`		
}

type BiologicalMany struct {
	ID          []bson.ObjectId `json:"id" bson:"_id"`
}

func ListBiologicalSpecies(db *mgo.Session, t string) ([]*BiologicalType, error) {
	c := db.DB(DruidDB).C(BiologicalTypeCollection)
	types := []*BiologicalType{}
	return types, MongoList(c, bson.M{"type": t}, &types, nil, nil)
}




