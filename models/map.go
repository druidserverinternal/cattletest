package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)



type OffsetPoint struct {
	Lng         int				`bson:"lng"`
	Lat         int				`bson:"lat"`
}

const (
	PointLocationEarth      =       0
	PointLocationChina      =       1
	PointLocationHongKong   =       2
	PointLocationTaiwan     =       3
)

type Offset struct {
	Point           *OffsetPoint		`bson:"point"`

	XOffset         int16       `bson:"x_offset"`
	YOffset         int16       `bson:"y_offset"`

	PointLocation   int         `bson:"point_location"`
}

func ListOffsetByPoints(db *mgo.Session, m bson.M, rp *RequestParameter) ([]*Offset, error) {
	c := db.DB(DruidMap).C(OffsetCollection)
	result := []*Offset{}
	return result, MongoList(c, m, &result, rp, nil)
}

func FindOffsetByLocation(db *mgo.Session, lng, lat float64) (*Offset, error) {
	c := db.DB(DruidMap).C(OffsetCollection)
	result := &Offset{}

	if err := c.Find(bson.M{"point": &OffsetPoint{int(lng * 100), int(lat * 100)}}).One(result); err != nil {
		return result, ErrorConvert(err)
	}
	return result, nil
}




