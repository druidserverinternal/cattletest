package models

import (
	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	MotionODBALimen1 = 0.05 //ODBA计算阀值
	MotionODBALimen2 = 0.25
)

type BehaviorParaneter struct {
	ODBA    float32 `json:"odba,omitempty" bson:"odba,omitempty"`
	ODBAX   float32 `json:"odba_x,omitempty" bson:"odba_x,omitempty"`
	ODBAY   float32 `json:"odba_y,omitempty" bson:"odba_y,omitempty"`
	ODBAZ   float32 `json:"odba_z,omitempty" bson:"odba_z,omitempty"`
	MeandlX float32 `json:"meandl_x,omitempty" bson:"meandl_x,omitempty"`
	MeandlY float32 `json:"meandl_y,omitempty" bson:"meandl_y,omitempty"`
	MeandlZ float32 `json:"meandl_z,omitempty" bson:"meandl_z,omitempty"`
	R       int     `json:"r"`
}

type BehaviorAlgoTreeCell struct { //行为算法处理数据参数tree
	LeftDaughter  int     `json:"left_daughter" bson:"left_daughter"`
	RightDaughter int     `json:"right_daughter" bson:"right_daughter"`
	SplitVar      int     `json:"split_var" bson:"split_var"`
	SplitPoint    float32 `json:"split_point" bson:"split_point"`
	Status        int     `json:"status" bson:"status"`
	Prediction    int     `json:"prediction" bson:"prediction"`
}

type BehaviorAlgorithm struct {
	ID           bson.ObjectId `json:"_id" bson:"_id,omitempty"`
	DeletedAt    *time.Time    `json:"-" bson:"deleted_at,omitempty"`
	UpdatedAt    *time.Time    `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
	Version      int           `json:"version,omitempty" bson:"version,omitempty"`
	MotionLimen1 float32       `json:"motion_limen1,omitempty" bson:"motion_limen1"`
	MotionLimen2 float32       `json:"motion_limen2,omitempty" bson:"motion_limen2"`

	Trees [][]*BehaviorAlgoTreeCell `json:"tress" bson:"tress"`
}

type ODBAAlgorithm struct {
	ID           bson.ObjectId `json:"_id" bson:"_id,omitempty"`
	DeletedAt    *time.Time    `json:"-" bson:"deleted_at,omitempty"`
	UpdatedAt    *time.Time    `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
	Version      int           `json:"version,omitempty" bson:"version"`
	ActThreshold float32       `json:"act_threshold,omitempty" bson:"act_threshold"`
	ActSlop      float32       `json:"act_slope,omitempty" bson:"act_slope"`
}

func (behaverAlgo *BehaviorAlgorithm) Create(db *mgo.Session) error {
	c := db.DB(DruidDB).C(BehaviorAlgoCollection)

	t := time.Now()
	behaverAlgo.UpdatedAt = &t
	err := c.Insert(behaverAlgo)
	if err != nil {
		return err
	}
	return nil
}

func (odbaAlgo *ODBAAlgorithm) Create(db *mgo.Session) error {
	c := db.DB(DruidDB).C(ODBAAlgoCollection)

	t := time.Now()
	odbaAlgo.UpdatedAt = &t 
	err := c.Insert(odbaAlgo)
	if err != nil {
		return err 
	}
	return nil 
}

func GetODBAAlgorithm(db *mgo.Session) *ODBAAlgorithm {
	c := db.DB(DruidDB).C(ODBAAlgoCollection)

	odbaAlgo := ODBAAlgorithm{}
	err := c.Find(nil).Sort("-updated_at").One(&odbaAlgo)
	if err != nil {
		return nil 
	}
	return &odbaAlgo
}

func GetBehaviorAlgorithm(db *mgo.Session) *BehaviorAlgorithm {
	c := db.DB(DruidDB).C(BehaviorAlgoCollection)

	beAlgo := BehaviorAlgorithm{}
	err := c.Find(nil).Sort("-updated_at").One(&beAlgo)
	if err != nil {
		return nil
	}
	return &beAlgo
}

func RecursiveTheBehaviorTrees(Tree []*BehaviorAlgoTreeCell, coord int, parameter []float32) int {
	if Tree[coord].Status == -1 {
		return Tree[coord].Prediction
	}
	num := Tree[coord].SplitVar - 1
	if num < 0 {
		return 0
	}
	if parameter[num] <= Tree[coord].SplitPoint {
		return RecursiveTheBehaviorTrees(Tree, Tree[coord].LeftDaughter-1, parameter)
	} else {
		return RecursiveTheBehaviorTrees(Tree, Tree[coord].RightDaughter-1, parameter)
	}
	return 0
}
