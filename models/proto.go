package models

import (
	"encoding/binary"
	//   "net"
)

const (
	// MSG_HEAD_SIZE is msg header
	MSG_HEAD_SIZE = 8

	// MSG_MAX_SIZE max msg length
	MSG_MAX_SIZE = 100 * 1024

	// MSG_BODY_MAX_SIZE is max body length
	MSG_BODY_MAX_SIZE = 100*1024 - MSG_HEAD_SIZE
)



// MsgHead is protobuf header
type MsgHead struct {
	MsgType uint32
	MsgLen  uint32
}

// MakeMsgHead2 make a protobuf header
func MakeMsgHead2(data []byte, head *MsgHead) error {
	head.MsgType = BytesToLittleUint32(data[:4])
	head.MsgLen = BytesToLittleUint32(data[4:8])

	return nil
}

// MakeMsgHead make a protobuf header
func MakeMsgHead(data []byte) (*MsgHead, error) {
	var msg MsgHead
	msg.MsgType = BytesToLittleUint32(data[:4])
	msg.MsgLen = BytesToLittleUint32(data[4:8])

	return &msg, nil
}

// GetPBMsgCmd get msg type from data
func GetPBMsgCmd(data []byte) uint32 {
	return BytesToLittleUint32(data[:4])
}

// GetPBMsgSize get msg size from data
func GetPBMsgSize(data []byte) uint32 {
	return BytesToLittleUint32(data[4:8])
}

// BytesToLittleUint32 convert binary to little endian int
func BytesToLittleUint32(data []byte) uint32 {
	return binary.LittleEndian.Uint32(data[:4])
}

