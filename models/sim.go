package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"strings"
	"time"
)

const (
	SIMISPNone    = 0
	SIMISPChecked = 1
)

// SIM is sim struct
type SIM struct {
	ID          bson.ObjectId `json:"id" bson:"_id,omitempty"`
	UpdatedAt   *time.Time    `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
	DeletedAt   *time.Time    `json:"-" bson:"deleted_at,omitempty"`
	IMSI        string        `json:"imsi" bson:"imsi"`
	SIMNumber   string        `json:"sim_number" bson:"sim_number"`
	Description string        `json:"description,omitempty" bson:"description,omitempty"`
	Device      *Device       `json:"device,omitempty" bson:"device"`
	ICCID       string        `json:"iccid" bson:"iccid"`
	SIMType     int32         `json:"sim_type" bson:"sim_type,omitempty"`
	Checked     int32         `json:"checked" bson:"checked,omitempty"`
}

type GetSIMByIMSIRequest struct {
	IMSI string
	From string
}

// Verify sim info
func (sim *SIM) Verify() bool {
	sim.IMSI = strings.TrimSpace(sim.IMSI)
	sim.SIMNumber = strings.TrimSpace(sim.SIMNumber)
	if sim.IMSI == "" || sim.SIMNumber == "" {
		return false
	}

	return true
}

func (sim *SIM) MarkChecked(db *mgo.Session) error {
	c := db.DB(DruidDB).C(SIMCollection)
	if err := c.Update(bson.M{"imsi": sim.IMSI},
		bson.M{"$set": bson.M{
			"checked":    SIMISPChecked,
			"updated_at": time.Now(),
		}}); err != nil {
		return err
	}
	return nil
}

// Upload a new sim
func (sim *SIM) Upload(db *mgo.Session) error {
	c := db.DB(DruidDB).C(SIMCollection)
	now := time.Now()
	sim.UpdatedAt = &now
	if err := c.Insert(sim); err != nil {
		return err
	}
	return nil
}

func (sim *SIM) UpdateType(db *mgo.Session) error {
	c := db.DB(DruidDB).C(SIMCollection)
	if _, err := c.Upsert(bson.M{"imsi": sim.IMSI},
		bson.M{"$set": bson.M{
			"sim_type":   sim.SIMType,
			"checked":    sim.Checked,
			"updated_at": time.Now(),
		}}); err != nil {
		return err
	}
	return nil
}

func (sim *SIM) UpdateISP(db *mgo.Session) error {
	c := db.DB(DruidDB).C(SIMCollection)
	if _, err := c.Upsert(bson.M{"imsi": sim.IMSI},
		bson.M{"$set": bson.M{
			"sim_number": sim.SIMNumber,
			"iccid":      sim.ICCID,
			"sim_type":   sim.SIMType,
			"checked":    sim.Checked,
			"updated_at": time.Now(),
		}}); err != nil {
		return err
	}
	return nil
}

// Update a sim info
func (sim *SIM) Update(db *mgo.Session) error {
	c := db.DB(DruidDB).C(SIMCollection)
	now := time.Now()
	if err := c.Update(bson.M{"imsi": sim.IMSI},
		bson.M{"$set": bson.M{
			"sim_number": sim.SIMNumber,
			//			"imsi":        sim.IMSI,
			"updated_at":  &now,
			"description": sim.Description}}); err != nil {
		return err
	}
	return nil
}

// UpdateNumberByIMSI update sim number by imsi
func (sim *SIM) UpdateNumberByIMSI(db *mgo.Session) error {
	c := db.DB(DruidDB).C(SIMCollection)
	now := time.Now()
	if _, err := c.Upsert(bson.M{"imsi": sim.IMSI},
		bson.M{"$set": bson.M{
			"sim_number": sim.SIMNumber,
			"updated_at": &now}}); err != nil {
		return err
	}
	return nil
}

func (sim *SIM) UpdateBindDevice(db *mgo.Session, device *Device) error {
	c := db.DB(DruidDB).C(SIMCollection)
	if err := c.Update(bson.M{"imsi": sim.IMSI}, bson.M{"$set": bson.M{"device": device, "updated_at": time.Now()}}); err != nil {
		return err
	}
	return nil
}

func UnBindSIMDevice(db *mgo.Session, imsi string) error {
	c := db.DB(DruidDB).C(SIMCollection)
	if err := c.Update(bson.M{"imsi": imsi}, bson.M{"$unset": bson.M{"device": 1}}); err != nil {
		return ErrorConvert(err)
	}
	return nil
}

func NewDeviceWithSIM(db *mgo.Session, imsi string) (*SIM, error) {
	sim, err := GetSIMFromISP(imsi)
	if err != nil {
		return nil, err
	}

	s, err := FindSIMByIMSI(db, imsi)
	if err != nil {
		return nil, Error("Find sim by imsi failed,", err)
	}

	if sim.SIMNumber == "" {
		if err := sim.UpdateType(db); err != nil {
			return nil, Error("Update sim type failed,", err)
		}
	} else {
		if err := sim.UpdateISP(db); err != nil {
			return nil, Error("Update sim info failed,", err)
		}
	}

	s, err = FindSIMByIMSI(db, imsi)
	if err != nil {
		return nil, Error("ReFind sim by imsi failed,", err)
	}
	return s, nil
}

// DeleteSIMByID delete one sim by id
func DeleteSIMByID(db *mgo.Session, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(SIMCollection)
	return MongoDelete(c, bson.M{"_id": id})
}

// GetSIMNumberByIMSI get a sim by imsi
func GetSIMNumberByIMSI(db *mgo.Session, imsi string) string {
	if imsi == "" {
		return ""
	}
	sim, err := FindSIMByIMSI(db, imsi)
	if err != nil {
		return ""
	}
	return sim.SIMNumber
}

// FindSIMByID find one sim by id
func FindSIMByID(db *mgo.Session, id bson.ObjectId) (*SIM, error) {
	c := db.DB(DruidDB).C(SIMCollection)
	sim := SIM{}
	if err := c.Find(bson.M{"_id": id}).One(&sim); err != nil {
		return nil, err
	}
	return &sim, nil
}

// FindSIMByICCID find one sim by iccid
func FindSIMByICCID(db *mgo.Session, num string) (*SIM, error) {
	c := db.DB(DruidDB).C(SIMCollection)
	sim := SIM{}
	if err := c.Find(bson.M{"iccid": num, "deleted_at": nil}).One(&sim); err != nil {
		return nil, err
	}
	return &sim, nil
}

// FindSIMByNumber find a sim by number
func FindSIMByNumber(db *mgo.Session, num string) (*SIM, error) {
	c := db.DB(DruidDB).C(SIMCollection)
	sim := SIM{}
	if err := c.Find(bson.M{"sim_number": num, "deleted_at": nil}).One(&sim); err != nil {
		return nil, err
	}
	return &sim, nil
}

// FindSIMByIMSI find a sim by imsi
func FindSIMByIMSI(db *mgo.Session, imsi string) (*SIM, error) {
	c := db.DB(DruidDB).C(SIMCollection)
	sim := SIM{}
	if err := c.Find(bson.M{"imsi": imsi, "deleted_at": nil}).One(&sim); err != nil {
		return nil, ErrorConvert(err)
	}
	return &sim, nil
}

// DeleteSIMByNumber delete a sim
func DeleteSIMByNumber(db *mgo.Session, number string) error {
	c := db.DB(DruidDB).C(SIMCollection)
	return MongoDelete(c, bson.M{"sim_number": number})
}

// ListSIM all of sim info
func ListSIM(db *mgo.Session, rp *RequestParameter) ([]*SIM, error) {
	c := db.DB(DruidDB).C(SIMCollection)
	sim := []*SIM{}
	return sim, MongoList(c, bson.M{"deleted_at": nil}, &sim, rp, nil)
}

// SearchSIM by sim info
func SearchSIM(db *mgo.Session, m bson.M, rp *RequestParameter) ([]*SIM, error) {
	c := db.DB(DruidDB).C(SIMCollection)
	sim := []*SIM{}
	return sim, MongoList(c, m, &sim, rp, nil)
}

// SIMCount get count device
func SIMCount(db *mgo.Session) (count int, err error) {
	c := db.DB(DruidDB).C(SIMCollection)
	return MongoCount(c, bson.M{"deleted_at": nil})
}

// SearchSIMCount search sim count
func SearchSIMCount(db *mgo.Session, m bson.M) (count int, err error) {
	c := db.DB(DruidDB).C(SIMCollection)
	return MongoCount(c, m)
}
