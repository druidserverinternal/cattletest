package models

import (
	//	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"time"
)


type SyncMongo struct {
	ID              bson.ObjectId `json:"id" bson:"_id,omitempty"`
	UpdatedAt       *time.Time    `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
	Timestamp       *time.Time    `json:"timestamp,omitempty" bson:"timestamp,omitempty"`
	UUID            string        `json:"uuid,,omitempty" bson:"uuid,omitempty"`
	Mark            int           `json:"mark,omitempty" bson:"mark,omitempty"`
	SN				string         `json:"sn,omitempty" bson:"sn,omitempty"`
}

// ListSyncMongo list all of devices
func ListSyncMongo(db *mgo.Session) ([]*SyncMongo, error) {
	c := db.DB(DruidDB).C(SyncMongoCollection)
	devices := []*SyncMongo{}

	return devices, MongoList(c, nil, &devices, nil, nil)
}

func (sync *SyncMongo)Update(db *mgo.Session) error {
	c := db.DB(DruidDB).C(SyncMongoCollection)

	if err := c.UpdateId(sync.ID, bson.M{"$set": bson.M{
					"updated_at": sync.UpdatedAt,
					"mark": sync.Mark,
					"sn": sync.SN,
				}}); err != nil {
		return err
	}
	return nil

}



