package models

import (
	"os/signal"
	"os"
	"syscall"

	log "github.com/sirupsen/logrus"
)

func Signal(f func()error ) {
	c:= make(chan os.Signal, 2)
	signal.Notify(c)

	for {
		s := <- c
		switch s {
		case syscall.SIGTERM, syscall.SIGKILL, syscall.SIGINT:
			log.Info("recv exit sig:", s)
			f()
			os.Exit(0)
		default:
			log.Info("unknown sig:", s)
			continue
		}
	}
}




