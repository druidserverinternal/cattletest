package models

import (
	"time"
)

const shortForm = "2006-01-02"
const dateForm = "2006-01"

var TimestampBegin = time.Date(2016, time.January, 1, 0, 0, 0, 0, time.UTC)

func ConvertStringToTime(s string) (*time.Time, error) {
	t, err := time.Parse(time.RFC3339Nano, s)
	if err != nil {
		return nil, err
	}
	return &t, nil
}

func ConvertTimeToDate(t *time.Time) string {
	return t.Format(shortForm)
}

func ConvertDateToTime(date string) (*time.Time, error) {
	t, err := time.Parse(shortForm, date)
	if err != nil {
		return nil, err
	}
	return &t, nil
}

func DateVerify(date string) bool {
	_, err := time.Parse(dateForm, date)
	if err != nil {
		return false
	}
	return true
}

func ConvertTimeToRange(t *time.Time) (*time.Time, *time.Time) {
	y, m, d := t.Date()
	begin := time.Date(y, m, d, 0, 0, 0, 0, time.UTC)
	end := time.Date(y, m, d, 23, 59, 59, 999, time.UTC)

	return &begin, &end
}

func ConvertTimeToOffsetTime(t time.Time, offset int) (time.Time) {
	t = t.Add(time.Duration(offset) * time.Minute)
	y, mon, d := t.Date()
	return time.Date(y, mon, d, 0, 0, 0, 0, time.UTC)
}


func AddOffsetToTime(t time.Time, offset int) (time.Time) {
	return t.Add(time.Duration(offset) * time.Minute)
}

func GetZoneMonday(t time.Time, offset int) (time.Time) {
	y, w := t.Add(time.Duration(offset) * time.Minute).ISOWeek()

	return time.Date(y, 1, (w - 1) * 7 + 1, 0, 0, 0, 0, time.UTC)
}

func ConvertDateToMonday(date string) (string) {
	t, err := time.Parse(shortForm, date)
	if err != nil {
		return ""
	}
	y, w := t.ISOWeek()

	return time.Date(y, 1, (w - 1) * 7 + 1, 0, 0, 0, 0, time.UTC).Format(shortForm)
}

func ConvertDateToMonthOne(date string) (string) {
	t, err := time.Parse(shortForm, date)
	if err != nil {
		return ""
	}
	y, m, _ := t.Date()

	return time.Date(y, m, 1, 0, 0, 0, 0, time.UTC).Format(shortForm)
}

func ConvertTimeToZoneBegin(t time.Time, offset int) (time.Time) {
	t = t.Add(time.Duration(offset) * time.Minute)
	y, mon, d := t.Date()
	return time.Date(y, mon, d, 0, 0, 0, 0, time.UTC).Add(time.Duration(-offset) * time.Minute)
}

func ConvertTimeToOffsetDate(t time.Time, offset int) (string) {
	t = t.Add(time.Duration(offset) * time.Minute)
	y, mon, d := t.Date()
	return time.Date(y, mon, d, 0, 0, 0, 0, time.UTC).Format(shortForm)
}

/*
func ConvertTimeToBegin(t time.Time) (*time.Time) {
	y, mon, d := t.Date()
	begin := time.Date(y, mon, d, 0, 0, 0, 0, time.UTC)

	return &begin
}
*/

func GetPrevMonth(t time.Time) (time.Time) {
	t = t.AddDate(0, -1, 0)
	y, m, _ := t.Date()

	return time.Date(y, m, 1, 0, 0, 0, 0, time.UTC)
}

func GetZoneBegin(t time.Time, offset int) (time.Time) {
	t = t.Add(time.Duration(offset) * time.Minute)
	y, mon, d := t.Date()

	return time.Date(y, mon, d, 0, 0, 0, 0, time.UTC)
}

func GetZoneMonthOne(t time.Time, offset int) (time.Time) {
	t = t.Add(time.Duration(offset) * time.Minute)
	y, m, _ := t.Date()

	return time.Date(y, m, 1, 0, 0, 0, 0, time.UTC)
}

func GetNextMonth(t time.Time) (time.Time) {
	t = t.AddDate(0, 1, 0)
	y, m, _ := t.Date()

	return time.Date(y, m, 1, 0, 0, 0, 0, time.UTC)
}

func GetCurrentMonday(t time.Time) (time.Time) {
	for {
		if t.Weekday() == time.Monday {
			y, m, d := t.Date()
			return time.Date(y, m, d, 0, 0, 0, 0, time.UTC)
		}
		t = t.AddDate(0, 0, -1)
	}
}

func GetNextMonday(t time.Time) (time.Time) {
	for {
		t = t.AddDate(0, 0, 1)
		if t.Weekday() == time.Monday {
			y, m, d := t.Date()
			return time.Date(y, m, d, 0, 0, 0, 0, time.UTC)
		}
	}
}

func GetTimestampEnd() (time.Time) {
	return time.Now().AddDate(0, 0, 5)
}

func VerifyTimestamp(t *time.Time) bool {
	if t.Before(TimestampBegin) || t.After(GetTimestampEnd()) {
		return false
	}
	return true
}




