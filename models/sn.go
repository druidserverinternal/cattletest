package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type SN struct {
	ID         bson.ObjectId `json:"id" bson:"_id,omitempty"`
	SystemType string        `json:"system_type,omitempty" bson:"system_type,omitempty"`
	SN         int           `json:"sn,omitempty" bson:"sn,omitempty"`
}

func GetLatestSN(db *mgo.Session, dt string) (*SN, error) {
	c := db.DB(DruidDB).C(SNCollection)

	change := mgo.Change{
		Update:    bson.M{"$inc": bson.M{"sn": 1}},
		Upsert:    true,
		ReturnNew: true,
	}

	sn := &SN{}
	info, err := c.Find(bson.M{"system_type": dt}).Apply(change, sn)
	if err != nil {
		return nil, err
	}

	return sn, CheckChangeInfo(info)
}

func FindSNByDeviceType(db *mgo.Session, dt string) (*SN, error) {
	c := db.DB(DruidDB).C(SNCollection)

	sn := &SN{}
	change := mgo.Change{
		Update:    bson.M{"$setOnInsert": &SN{SystemType: dt, SN: 2000}},
		Upsert:    true,
		ReturnNew: true,
	}

	info, err := c.Find(bson.M{"system_type": dt}).Apply(change, sn)
	if err != nil {
		return nil, err
	}

	return sn, CheckChangeInfo(info)
}
