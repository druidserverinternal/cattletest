package models

import (
	/*


		"golang.org/x/crypto/bcrypt"
		"fmt"
	*/
	"encoding/hex"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	//	"gopkg.in/guregu/null.v3"
	"regexp"
	"time"
)

// RegexpIDCard is regexp with id card
var RegexpIDCard = regexp.MustCompile(`^[0-9xX]{18}$`)

// RegexpUser is regexp with username
var RegexpUser = regexp.MustCompile(`^[A-Za-z0-9.+-_@]{3,30}$`)

// RegexpPwd is regexp with sha256 password
var RegexpPwd = regexp.MustCompile(`^[A-Za-z0-9]{64}$`)

// RegexpEmail is regexp with mail
var RegexpEmail = regexp.MustCompile(`^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$`)

// RegexpPhone is regexp with mail
var RegexpPhone = regexp.MustCompile(`^[0-9\+\-]{3,20}$`)

// This	is user role
const (
	RoleRoot    = "root"
	RoleManager = "manager"
	RoleAdmin   = "admin"
	RoleUser    = "user"
)

/*
const (
	RoleLevelRoot    = 0
	RoleLevelManager = 1
	RoleLevelAdmin   = 2
	RoleLevelUser    = 3
)
*/

const (
	RolePermissionsNone  = 0
	RolePermissionsRead  = 1
	RolePermissionsWrite = 2
	RolePermissionsAll   = 3
)
const (
	AppPermissionsNone  = 0
	AppPermissionsRead  = 1
	AppPermissionsWrite = 2
	AppPermissionsAll   = 3
)

const (
	ProfilePageMax = 1000
	ProfilePageMin = 10

	ProfileTimezoneMax   = 14 * 60
	ProfileTimezoneMin   = -12 * 60
	ProfileTimezoneChina = 8 * 60

	ProfileLanguageBegin = 0
	ProfileLanguageEN    = 0
	ProfileLanguageCH    = 1
	ProfileLanguageEnd   = 1
)

type Permissions struct {
	// for manager
	SIMAuth      int `json:"sim_auth" bson:"sim_auth,omitempty"`
	SearchAuth   int `json:"search_auth" bson:"search_auth,omitempty"`
	CompanyAuth  int `json:"company_auth" bson:"company_auth,omitempty"`
	DeviceAuth   int `json:"device_auth" bson:"device_auth,omitempty"`
	DataAuth     int `json:"data_auth" bson:"data_auth,omitempty"`
	PlatformAuth int `json:"platform_auth" bson:"platform_auth,omitempty"`
	FirmwareAuth int `json:"firmware_auth" bson:"firmware_auth,omitempty"`
	AppAuth      int `json:"app_auth" bson:"app_auth,omitempty"`

	// for manager and user
	SettingAuth int `json:"setting_auth" bson:"setting_auth,omitempty"`
	UserAuth    int `json:"user_auth" bson:"user_auth,omitempty"`
	ExportAuth  int `json:"export_auth" bson:"export_auth,omitempty"`

	// for user
	BiologicalAuth int `json:"biological_auth" bson:"biological_auth,omitempty"`
	EnvAuth        int `json:"env_auth" bson:"env_auth,omitempty"`
	BhvAuth        int `json:"bhv_auth" bson:"bhv_auth,omitempty"`
	AnalysisAuth   int `json:"analysis_auth" bson:"analysis_auth,omitempty"`
}

type PushSet struct {
	Dead   bool `json:"dead" bson:"dead"`
	Area   bool `json:"area" bson:"area"`
	Export bool `json:"export" bson:"export"`
}

type UserProfile struct {
	PageSize int `json:"page_size" bson:"page_size,omitempty"`
	TimeZone int `json:"time_zone" bson:"time_zone,omitempty"`
	Language int `json:"language" bson:"language,omitempty"`
}

// User struct represents the user object in the database
type User struct {
	ID             bson.ObjectId `json:"id" bson:"_id,omitempty"`
	UserName       string        `json:"username" bson:"username,omitempty"`
	Password       string        `json:"password,omitempty" bson:"-"`
	OldPassword    string        `json:"old_password,omitempty" bson:"-"`
	HashedPassword string        `json:"-" bson:"hashed_password,omitempty"`
	DeletedAt      *time.Time    `json:"-" bson:"deleted_at,omitempty"`
	DeletedBy      bson.ObjectId `json:"-" bson:"deleted_by,omitempty"`
	UpdatedAt      *time.Time    `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
	Address        string        `json:"address,omitempty" bson:"address,omitempty"`
	Phone          string        `json:"phone,omitempty" bson:"phone,omitempty"`
	Email          string        `json:"email,omitempty" bson:"email,omitempty"`
	IDCard         string        `json:"id_card,omitempty" bson:"id_card,omitempty"`
	Token          string        `json:"-" bson:"token,omitempty"`
	Expiry         *time.Time    `json:"-" bson:"expiry,omitempty"`
	Role           string        `json:"role,omitempty" bson:"role,omitempty"`
	CreatorID      bson.ObjectId `json:"creator_id,omitempty" bson:"creator_id,omitempty"`
	CompanyID      bson.ObjectId `json:"company_id,omitempty" bson:"company_id,omitempty"`
	CompanyName    string        `json:"company_name,omitempty" bson:"company_name,omitempty"`
	Description    string        `json:"description" bson:"description,omitempty"`
	Permissions    *Permissions  `json:"permissions,omitempty" bson:"permissions,omitempty"`
	Profile        *UserProfile  `json:"profile" bson:"profile,omitempty"`
	PushSet        *PushSet      `json:"push_set,omitempty" bson:"push_set,omitempty"`
	TotalDevice    int           `json:"total_device" bson:"total_device"`
	TotalArea      int           `json:"total_area" bson:"total_area"`
	TotalRoom      int           `json:"total_room" bson:"total_room"`
	//	DituCount      int				 `json:"ditu_count" bson:"ditu_count,omitempty"`
}

func (p *Permissions) VerifyManager() bool {
	if p.CompanyAuth < RolePermissionsNone || p.CompanyAuth > RolePermissionsAll {
		return false
	}

	if p.DeviceAuth < RolePermissionsNone || p.DeviceAuth > RolePermissionsAll {
		return false
	}

	if p.ExportAuth < RolePermissionsNone || p.ExportAuth > RolePermissionsAll {
		return false
	}

	if p.FirmwareAuth < RolePermissionsNone || p.FirmwareAuth > RolePermissionsAll {
		return false
	}
	if p.PlatformAuth < RolePermissionsNone || p.PlatformAuth > RolePermissionsAll {
		return false
	}
	if p.SettingAuth < RolePermissionsNone || p.SettingAuth > RolePermissionsAll {
		return false
	}
	if p.DataAuth < RolePermissionsNone || p.DataAuth > RolePermissionsAll {
		return false
	}
	if p.SIMAuth < RolePermissionsNone || p.SIMAuth > RolePermissionsAll {
		return false
	}
	if p.UserAuth < RolePermissionsNone || p.UserAuth > RolePermissionsAll {
		return false
	}
	if p.SearchAuth < RolePermissionsNone || p.SearchAuth > RolePermissionsRead {
		return false
	}

	return true
}

func (user *User) CheckPermissions() bool {
	if user.Permissions == nil {
		return true
	}

	return user.Permissions.VerifyManager()
}

func (user *User) CheckBhvRead() bool {
	if user.Permissions == nil {
		return false
	}

	if user.Permissions.BhvAuth != RolePermissionsRead && user.Permissions.BhvAuth != RolePermissionsAll {
		return false
	}
	return true
}

func (user *User) CheckBhvWrite() bool {
	if user.Permissions == nil {
		return false
	}

	if user.Permissions.BhvAuth != RolePermissionsWrite && user.Permissions.BhvAuth != RolePermissionsAll {
		return false
	}
	return true
}

func (user *User) CheckEnvRead() bool {
	if user.Permissions == nil {
		return false
	}

	if user.Permissions.EnvAuth != RolePermissionsRead && user.Permissions.EnvAuth != RolePermissionsAll {
		return false
	}
	return true
}

func (user *User) CheckEnvWrite() bool {
	if user.Permissions == nil {
		return false
	}

	if user.Permissions.EnvAuth != RolePermissionsWrite && user.Permissions.EnvAuth != RolePermissionsAll {
		return false
	}
	return true
}

func (user *User) CheckBiologicalRead() bool {
	if user.Permissions == nil {
		return false
	}

	if user.Permissions.BiologicalAuth != RolePermissionsRead && user.Permissions.BiologicalAuth != RolePermissionsAll {
		return false
	}
	return true
}

func (user *User) CheckBiologicalWrite() bool {
	if user.Permissions == nil {
		return false
	}

	if user.Permissions.BiologicalAuth != RolePermissionsWrite && user.Permissions.BiologicalAuth != RolePermissionsAll {
		return false
	}
	return true
}

func (user *User) CheckSettingRead() bool {
	if user.Permissions == nil {
		return false
	}

	if user.Permissions.SettingAuth != RolePermissionsRead && user.Permissions.SettingAuth != RolePermissionsAll {
		return false
	}
	return true
}

func (user *User) CheckSettingWrite() bool {
	if user.Permissions == nil {
		return false
	}

	if user.Permissions.SettingAuth != RolePermissionsWrite && user.Permissions.SettingAuth != RolePermissionsAll {
		return false
	}
	return true
}

func (user *User) CheckUserRead() bool {
	if user.Permissions == nil {
		return false
	}

	if user.Permissions.UserAuth != RolePermissionsRead && user.Permissions.UserAuth != RolePermissionsAll {
		return false
	}
	return true
}

func (user *User) CheckUserWrite() bool {
	if user.Permissions == nil {
		return false
	}

	if user.Permissions.UserAuth != RolePermissionsWrite && user.Permissions.UserAuth != RolePermissionsAll {
		return false
	}
	return true
}

func (user *User) CheckExportRead() bool {
	if user.Permissions == nil {
		return false
	}

	if user.Permissions.ExportAuth != RolePermissionsRead && user.Permissions.ExportAuth != RolePermissionsAll {
		return false
	}
	return true
}

func (user *User) CheckPlatformRead() bool {
	if user.Permissions == nil {
		return false
	}

	if user.Permissions.PlatformAuth != RolePermissionsRead && user.Permissions.PlatformAuth != RolePermissionsAll {
		return false
	}
	return true
}

func (user *User) CheckPlatformWrite() bool {
	if user.Permissions == nil {
		return false
	}

	if user.Permissions.PlatformAuth != RolePermissionsWrite && user.Permissions.PlatformAuth != RolePermissionsAll {
		return false
	}
	return true
}

func (user *User) CheckDataRead() bool {
	if user.Permissions == nil {
		return false
	}

	if user.Permissions.DataAuth != RolePermissionsRead && user.Permissions.DataAuth != RolePermissionsAll {
		return false
	}
	return true
}

func (user *User) CheckDataWrite() bool {
	if user.Permissions == nil {
		return false
	}

	if user.Permissions.DataAuth != RolePermissionsWrite && user.Permissions.DataAuth != RolePermissionsAll {
		return false
	}
	return true
}

func (user *User)CheckAppRead()bool{
	if user.Permissions == nil {
		return false
	}
	
	if user.Permissions.AppAuth != AppPermissionsRead && user.Permissions.DataAuth != AppPermissionsAll {
		return false
	}
	return true
}

func (user *User) CheckAppWrite() bool {
	if user.Permissions == nil {
		return false
	}
	
	if user.Permissions.AppAuth != AppPermissionsWrite  && user.Permissions.DataAuth != AppPermissionsAll {
		return false
	}
	return true
}

func (user *User) CheckSearchRead() bool {
	if user.Permissions == nil {
		return false
	}

	if user.Permissions.SearchAuth != RolePermissionsRead && user.Permissions.SearchAuth != RolePermissionsAll {
		return false
	}
	return true
}

func (user *User) CheckSearchWrite() bool {
	if user.Permissions == nil {
		return false
	}

	if user.Permissions.SearchAuth != RolePermissionsWrite && user.Permissions.SearchAuth != RolePermissionsAll {
		return false
	}
	return true
}

func (user *User) CheckCompanyRead() bool {
	if user.Permissions == nil {
		return false
	}

	if user.Permissions.CompanyAuth != RolePermissionsRead && user.Permissions.CompanyAuth != RolePermissionsAll {
		return false
	}
	return true
}

func (user *User) CheckCompanyWrite() bool {
	if user.Permissions == nil {
		return false
	}

	if user.Permissions.CompanyAuth != RolePermissionsWrite && user.Permissions.CompanyAuth != RolePermissionsAll {
		return false
	}
	return true
}

func (user *User) CheckDeviceRead() bool {
	if user.Permissions == nil {
		return false
	}

	if user.Permissions.DeviceAuth != RolePermissionsRead && user.Permissions.DeviceAuth != RolePermissionsAll {
		return false
	}
	return true
}

func (user *User) CheckDeviceWrite() bool {
	if user.Permissions == nil {
		return false
	}

	if user.Permissions.DeviceAuth != RolePermissionsWrite && user.Permissions.DeviceAuth != RolePermissionsAll {
		return false
	}
	return true
}

func (user *User) CheckSIMRead() bool {
	if user.Permissions == nil {
		return false
	}

	if user.Permissions.SIMAuth != RolePermissionsRead && user.Permissions.SIMAuth != RolePermissionsAll {
		return false
	}
	return true
}

func (user *User) CheckSIMWrite() bool {
	if user.Permissions == nil {
		return false
	}

	if user.Permissions.SIMAuth != RolePermissionsWrite && user.Permissions.SIMAuth != RolePermissionsAll {
		return false
	}
	return true
}

func (user *User) CheckFirmwareRead() bool {
	if user.Permissions == nil {
		return false
	}

	if user.Permissions.FirmwareAuth != RolePermissionsRead && user.Permissions.FirmwareAuth != RolePermissionsAll {
		return false
	}
	return true
}

func (user *User) CheckFirmwareWrite() bool {
	if user.Permissions == nil {
		return false
	}

	if user.Permissions.FirmwareAuth != RolePermissionsWrite && user.Permissions.FirmwareAuth != RolePermissionsAll {
		return false
	}
	return true
}

// CheckToken check token valid
func (user *User) CheckTokenValid() bool {
	if user.Token == "" {
		return false
	}
	if user.Expiry.Before(time.Now()) {
		return false
	}
	return true
}

// UpdateToken update new token to mongodb
func (user *User) UpdateToken(db *mgo.Session) error {
	c := db.DB(DruidDB).C(UserCollection)
	err := c.UpdateId(user.ID, bson.M{"$set": bson.M{"token": user.Token, "expiry": user.Expiry, "updated_at": time.Now()}})
	if err != nil {
		return err
	}

	return nil
}

// Create a new user
func (user *User) Create(db *mgo.Session) error {
	c := db.DB(DruidDB).C(UserCollection)

	_, err := MongoAtomInsert(c, bson.M{"username": user.UserName, "deleted_at": nil}, user, user)
	if err != nil {
		return err
	}
	return nil
}

/*
// GetAuthDevices get user auth devices
func (user *User) GetAuthDevices() []bson.ObjectId {
	devicesID := []bson.ObjectId{}

	for _, authDevice := range user.AuthDevices {
		devicesID = append(devicesID, authDevice.DeviceID)
	}
	return devicesID
}

// GetAuthDevice user auth device
func (user *User) GetAuthDevice(deviceID bson.ObjectId) *AuthorizationDevice {
	for _, authDevice := range user.AuthDevices {
		if authDevice.DeviceID == deviceID {
			return authDevice
		}
	}
	return nil
}
*/

/*
// CheckAuthDevice user auth device
func (user *User) CheckAuthDevice(deviceID bson.ObjectId) bool {
	if user.Role == RoleAdmin {
		return true
	}
	for _, authDevice := range user.AuthDevices {
		if authDevice.DeviceID == deviceID {
			return true
		}
	}
	return false
}
*/

/*
func (user *User) CheckOwnerDevice(deviceID bson.ObjectId) bool {
	if user.Role == RoleAdmin {
		return true
	}
	for _, ownerID := range user.OwnerDevices {
		if ownerID == deviceID {
			return true
		}
	}
	return false
}
*/

func (user *User) CheckDeviceOwner(device *Device) error {
	if device == nil {
		return Error("Device is invalid.")
	}

	if user.CompanyID != device.CompanyID {
		return Error("You don't have permission to access this device.")
	}
	if user.Role == RoleAdmin {
		return nil
	}

	if device.OwnerID != user.ID {
		return Error("You don't have permission to access this device.")
	}

	return nil
}

func (user *User) FixOwnerDevices(db *mgo.Session, devices []*Device) error {
	c := db.DB(DruidDB).C(UserCollection)

	ids := []bson.ObjectId{}
	for _, d := range devices {
		ids = append(ids, d.ID)
	}

	if err := c.UpdateId(user.ID, bson.M{"$set": bson.M{"owner_devices": ids}}); err != nil {
		return err
	}
	return nil
}

/*
// UpdateAuthDevice user auth device
func (user *User) UpdateAuthDevice(db *mgo.Session) error {
	c := db.DB(DruidDB).C(UserCollection)

	err := c.UpdateId(user.ID,
		bson.M{"$set": bson.M{
			"auth_devices": user.AuthDevices,
			"updated_at":   time.Now()}})
	if err != nil {
		return err
	}
	return nil
}
*/

// UpdatePassword a user password
func (user *User) UpdatePassword(db *mgo.Session) error {
	c := db.DB(DruidDB).C(UserCollection)

	err := c.UpdateId(user.ID,
		bson.M{"$set": bson.M{
			"hashed_password": user.HashedPassword,
			"token":           user.Token,
			"expiry":          user.Expiry,
			"updated_at":      time.Now()}})
	if err != nil {
		return err
	}
	return nil
}

func (user *User) CheckManagerPermissions() bool {
	if user.Permissions == nil {
		return true
	}

	return user.Permissions.VerifyManager()
}

func (user *User) UpdatePushSet(db *mgo.Session) error {
	c := db.DB(DruidDB).C(UserCollection)

	err := c.UpdateId(user.ID,
		bson.M{"$set": bson.M{
			"push_set":   user.PushSet,
			"updated_at": time.Now(),
		}})
	if err != nil {
		return err
	}
	return nil
}

func (user *User) UpdateProfile(db *mgo.Session) error {
	c := db.DB(DruidDB).C(UserCollection)

	err := c.UpdateId(user.ID,
		bson.M{"$set": bson.M{
			"profile.page_size": user.Profile.PageSize,
			"profile.time_zone": user.Profile.TimeZone,
			"profile.language":  user.Profile.Language,
			"updated_at":        time.Now(),
		}})
	if err != nil {
		return err
	}
	return nil
}

// UpdateInfo myself infomation
func (user *User) UpdateInfo(db *mgo.Session) error {
	c := db.DB(DruidDB).C(UserCollection)
	err := c.UpdateId(user.ID, bson.M{"$set": bson.M{
		"phone":       user.Phone,
		"address":     user.Address,
		"email":       user.Email,
		"updated_at":  time.Now(),
		"id_card":     user.IDCard,
		"description": user.Description,
	}})
	if err != nil {
		return err
	}
	return nil
}

// UpdateManager update usernf infomation
func (user *User) UpdateManager(db *mgo.Session) error {
	c := db.DB(DruidDB).C(UserCollection)
	err := c.UpdateId(user.ID, bson.M{"$set": bson.M{
		"phone":       user.Phone,
		"email":       user.Email,
		"address":     user.Address,
		"updated_at":  time.Now(),
		"description": user.Description,
		"permissions": user.Permissions,
	}})
	if err != nil {
		return err
	}
	return nil
}

// Update a user infomation
func (user *User) Update(db *mgo.Session) error {
	c := db.DB(DruidDB).C(UserCollection)

	err := c.UpdateId(user.ID, bson.M{"$set": user})
	if err != nil {
		return err
	}
	return nil
}

// Delete mark a user deleted from mongodb
func (user *User) Delete(db *mgo.Session) error {
	c := db.DB(DruidDB).C(UserCollection)

	if err := RemoveOwnerFromBiologicalByOwnerID(db, []bson.ObjectId{user.ID}); err != nil {
		return Error("Remove biological owner by devices id failed:", err)
	}

	if err := DeleteDituAreaByOwnerID(db, user.ID); err != nil {
		return Error("Remove area by user failed:", err)
	}

	if err := DeleteRoomByOwnerID(db, user.ID); err != nil {
		return Error("Remove room by user failed:", err)
	}

	if err := RemoveOwnerFromDeviceByOwnerID(db, user.ID); err != nil {
		return Error("Remove owner devices failed:", err)
	}

	return MongoDelete(c, bson.M{"_id": user.ID})
}

// HashPasswrod generate hashed password
func (user *User) HashPassword() bool {
	hexPassword, err := hex.DecodeString(user.Password)
	if err != nil {
		return false
	}

	hashedPassword, err := bcrypt.GenerateFromPassword(hexPassword, bcrypt.DefaultCost)
	if err != nil {
		return false
	}
	user.HashedPassword = string(hashedPassword)
	return true
}

/*
func (user *User) FindNewAuthDevice(request []*AuthorizationDevice) []bson.ObjectId {
	var added []bson.ObjectId

	for _, d := range request {
		ad := AuthDevices(user.AuthDevices)
		if !ad.Find(d.DeviceID) {
			added = append(added, d.DeviceID)
		}
	}

	return added
}

func (user *User) FindDelAuthDevice(request []*AuthorizationDevice) []bson.ObjectId {
	var deled []bson.ObjectId

	for _, d := range user.AuthDevices {
		ad := AuthDevices(request)
		if !ad.Find(d.DeviceID) {
			deled = append(deled, d.DeviceID)
		}
	}

	return deled
}
*/

func (user *User) AddOwnerDevice(db *mgo.Session, deviceID bson.ObjectId) error {
	c := db.DB(DruidDB).C(UserCollection)

	if err := c.UpdateId(user.ID,
		bson.M{
			"$addToSet": bson.M{
				"owner_devices": deviceID}}); err != nil {
		return err
	}

	return nil
}

func (user *User) AddTotalArea(db *mgo.Session, l int) error {
	c := db.DB(DruidDB).C(UserCollection)

	if err := c.UpdateId(user.ID,
		bson.M{
			"$inc": bson.M{
				"total_area": l}}); err != nil {
		return err
	}

	return nil
}

func (user *User) AddTotalDevice(db *mgo.Session, l int) error {
	c := db.DB(DruidDB).C(UserCollection)

	if err := c.UpdateId(user.ID,
		bson.M{
			"$inc": bson.M{
				"total_device": l}}); err != nil {
		return err
	}

	return nil
}

func (user *User) RemoveDevices(db *mgo.Session, ids []bson.ObjectId) error {
	c := db.DB(DruidDB).C(UserCollection)

	if err := c.UpdateId(user.ID,
		bson.M{
			"$pull": bson.M{
				"owner_devices": bson.M{"$in": ids}}}); err != nil {
		return err
	}

	return nil
}

func (user *User) IsAdmin() bool {
	if user.Role == RoleAdmin {
		return true
	}
	return false
}

func (user *User) IsUser() bool {
	if user.Role == RoleUser {
		return true
	}
	return false
}

func (user *User) IsManager() bool {
	if user.Role == RoleManager {
		return true
	}
	return false
}

func (user *User) IsRoot() bool {
	if user.Role == RoleRoot {
		return true
	}
	return false
}

// UpdateTotal a device total
func (user *User) UpdateTotalDevice(db *mgo.Session) error {
	c := db.DB(DruidDB).C(UserCollection)
	if err := c.UpdateId(user.ID,
		bson.M{"$set": bson.M{
			"total_device": user.TotalDevice,
		}}); err != nil {
		return err
	}
	return nil
}

func (user *User) UpdateTotalRoom(db *mgo.Session) error {
	c := db.DB(DruidDB).C(UserCollection)
	if err := c.UpdateId(user.ID,
		bson.M{"$set": bson.M{
			"total_room": user.TotalRoom,
		}}); err != nil {
		return err
	}
	return nil
}

func (user *User) UpdateTotalArea(db *mgo.Session) error {
	c := db.DB(DruidDB).C(UserCollection)
	if err := c.UpdateId(user.ID,
		bson.M{"$set": bson.M{
			"total_area": user.TotalArea,
		}}); err != nil {
		return err
	}
	return nil
}

func (user *User) FixTotalArea(db *mgo.Session) error {
	count, err := CountDituAreaByOwnerID(db, user.ID)
	if err != nil {
		return Error("Count area failed.", err)
	}

	if user.TotalArea != count {
		user.TotalArea = count
		if err := user.UpdateTotalArea(db); err != nil {
			return Error("Update total device failed.", err)
		}
	}

	return nil
}

func (user *User) FixTotalRoom(db *mgo.Session) error {
	count, err := CountRoomByUserID(db, user.ID)
	if err != nil {
		return Error("Count room failed.", err)
	}

	if user.TotalRoom != count {
		user.TotalRoom = count
		if err := user.UpdateTotalRoom(db); err != nil {
			return Error("Update total room failed.", err)
		}
	}

	return nil
}

func (user *User) FixTotalDevice(db *mgo.Session) error {
	count, err := CountDeviceByUserID(db, user.ID)
	if err != nil {
		return Error("Count device failed.", err)
	}

	if user.TotalDevice != count {
		user.TotalDevice = count
		if err := user.UpdateTotalDevice(db); err != nil {
			return Error("Update total device failed.", err)
		}
	}

	return nil
}

// PasswordVerify verify original password with bcrypt password
func PasswordVerify(password, hashedPassword string) error {
	hexPassword, err := hex.DecodeString(password)
	if err != nil {
		return err
	}
	err = bcrypt.CompareHashAndPassword([]byte(hashedPassword), hexPassword)
	if err != nil {
		return err
	}
	return nil
}

// DeleteUsersByCompanyID delete all of company's user
func DeleteUsersByCompanyID(db *mgo.Session, company_id bson.ObjectId) error {
	c := db.DB(DruidDB).C(UserCollection)
	return MongoDestroy(c, bson.M{"company_id": company_id})
}

// ListUsers list all users
func ListUsers(db *mgo.Session, rp *RequestParameter) ([]*User, error) {
	c := db.DB(DruidDB).C(UserCollection)
	users := []*User{}
	return users, MongoList(c, bson.M{"deleted_at": nil}, &users, rp, nil)
}

// ListAllUsersByCompanyID list all of company's user
func ListAllUsersByCompanyID(db *mgo.Session, id bson.ObjectId, rp *RequestParameter) ([]*User, error) {
	c := db.DB(DruidDB).C(UserCollection)
	users := []*User{}
	return users, MongoList(c, bson.M{"company_id": id, "deleted_at": nil}, &users, rp, nil)
}

func CountAllUsersByCompanyID(db *mgo.Session, id bson.ObjectId) (count int, err error) {
	c := db.DB(DruidDB).C(UserCollection)
	return MongoCount(c, bson.M{"company_id": id, "deleted_at": nil})
}

func ListCommonUsersByCompanyID(db *mgo.Session, id bson.ObjectId, rp *RequestParameter) ([]*User, error) {
	c := db.DB(DruidDB).C(UserCollection)
	users := []*User{}
	return users, MongoList(c, bson.M{"company_id": id, "role": RoleUser, "deleted_at": nil}, &users, rp, nil)
}

func CountCommonUsersByCompanyID(db *mgo.Session, id bson.ObjectId) (count int, err error) {
	c := db.DB(DruidDB).C(UserCollection)
	return MongoCount(c, bson.M{"company_id": id, "role": RoleUser, "deleted_at": nil})
}

/*
func RoleCheck(role string) bool {
	switch role {
	case RoleAdmin, RoleUser:
		return true
	default:
		return false
	}
}
*/

// UserRoleVerify verify admin create user
func UserRoleVerify(userRole string, creatorRole string) bool {
	if creatorRole == RoleAdmin && userRole == RoleUser {
		return true
	}
	return true
}

// CheckUserRoleIsRoot check requestor is root
func CheckUserRoleIsRoot(db *mgo.Session, id bson.ObjectId) bool {
	user := FindUserByID(db, id)
	if user == nil {
		return false
	}
	if user.Role != RoleRoot {
		return false
	}
	return true
}

// UpdateUserCompanyByCompanyID update user's company name
func UpdateUserCompanyByCompanyID(db *mgo.Session, company *Company) error {
	c := db.DB(DruidDB).C(UserCollection)
	if err := c.UpdateId(bson.M{"company_id": company.ID},
		bson.M{"$set": bson.M{"company_name": company.CompanyName}}); err != nil {
		return err
	}
	return nil
}

// UserCountByCompanyID get count
func UserCountByCompanyID(db *mgo.Session, id bson.ObjectId) (count int, err error) {
	c := db.DB(DruidDB).C(UserCollection)
	return MongoCount(c, bson.M{"company_id": id, "deleted_at": nil})
}

// UserCount get count
func UserCount(db *mgo.Session) (count int, err error) {
	c := db.DB(DruidDB).C(UserCollection)
	return MongoCount(c, bson.M{"deleted_at": nil})
}

// FindUserByUserName find a user exists by username
func FindUserByUserName(db *mgo.Session, name string) (*User, error) {
	c := db.DB(DruidDB).C(UserCollection)

	var user User
	if err := c.Find(bson.M{"username": name, "deleted_at": nil}).One(&user); err != nil {
		return nil, ErrorConvert(err)
	}
	return &user, nil
}

// FindUserByID find a user exists by id
func FindUserByID(db *mgo.Session, id bson.ObjectId) *User {
	c := db.DB(DruidDB).C(UserCollection)

	user := User{}
	err := c.Find(bson.M{"_id": id, "deleted_at": nil}).One(&user)
	if err != nil {
		return nil
	}
	return &user
}

// DeleteUserByID mark a user deleted from mongodb
func DeleteUserByID(db *mgo.Session, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(UserCollection)
	return MongoDelete(c, bson.M{"_id": id})
}

/*
func FindUserByAuthDevice(db *mgo.Session, ownerID bson.ObjectId) ([]*User, error) {
	c := db.DB(DruidDB).C(UserCollection)

	var user []*User
	if err := c.Find(bson.M{
		"company_id":             companyID,
		"role":                   RoleUser,
		"deleted_at":             nil,
		"auth_devices.device_id": deviceID}).All(&user); err != nil {
		return user, ErrorConvert(err)
	}
	return user, nil
}
*/

func FindUserByCompanyAdmin(db *mgo.Session, companyID bson.ObjectId) ([]*User, error) {
	c := db.DB(DruidDB).C(UserCollection)

	var user []*User
	if err := c.Find(bson.M{"company_id": companyID, "role": RoleAdmin, "deleted_at": nil}).All(&user); err != nil {
		return user, ErrorConvert(err)
	}
	return user, nil
}

func PullDeviceFromUserAuthDevice(db *mgo.Session, ids []bson.ObjectId) error {
	c := db.DB(DruidDB).C(UserCollection)

	if err := c.Update(
		bson.M{"auth_devices.device_id": bson.M{"$in": ids}},
		bson.M{"$pull": bson.M{"auth_devices": bson.M{"device_id": bson.M{"$in": ids}}}}); err != nil {
		return ErrorConvert(err)
	}
	return nil
}

func GetUserQueue(db *mgo.Session) *mgo.Bulk {
	c := db.DB(DruidDB).C(UserCollection)
	return c.Bulk()
}

/*
func FindUserByDeviceEffect(db *mgo.Session, device *Device) []bson.ObjectId {
	users, _ := FindUserByCompanyAdmin(db, device.CompanyID)
	if device.OwnerID != "" {
		user := FindUserByID(db, device.OwnerID)
		if user != nil {
			users = append(users, user)
		}
	}

	us := []bson.ObjectId{}
	for _, u := range users {
		us = append(us, u.ID)
	}

	return us
}
*/

// ListUserByFilter all of user
func ListUserByFilter(db *mgo.Session, m bson.M, rp *RequestParameter) ([]*User, error) {
	c := db.DB(DruidDB).C(UserCollection)
	users := []*User{}
	return users, MongoList(c, m, &users, rp, nil)
}

// CountUserByFilter get count user
func CountUserByFilter(db *mgo.Session, m bson.M) (count int, err error) {
	c := db.DB(DruidDB).C(UserCollection)
	return MongoCount(c, m)
}
