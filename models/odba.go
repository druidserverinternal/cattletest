package models

import (
	"fmt"
	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	ODBAActThreshold = 0.3
	ODBAActSlop      = 0.5
)

type ODBAParameter struct {
	ID             bson.ObjectId `json:"id,omitempty" bson:"_id,omitempty"`
	DeviceID       bson.ObjectId `json:"device_id" bson:"device_id,omitempty"`
	Mark           int           `json:"mark,omitempty" bson:"mark,omitempty"`
	OwnerID        bson.ObjectId `json:"owner_id" bson:"owner_id,omitempty"`
	NickName       string        `json:"nickname" bson:"nickname,omitempty"`
	UpdatedAt      *time.Time    `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
	TimeNum        uint32        `json:"time_num" bson:"time_num"`
	Activity       []int         `json:"-" bson:"activity,omitempty"`
	ActPerecentage float32       `json:"-" bson:"act_percentage,omitempty"`
	ActNow         float32       `json:"-" bson:"act_now,omitempty"`
	ActSlope       float32       `json:"-" bson:"act_slope,omitempty"`
	ActValidNum    int           `json:"act_valid_num" bson:"act_valid_num"`
	ActTotalNum    int           `json:"act_total_num" bson:"act_total_num"`
	Survive        int           `json:"survive" bson:"survive"`
}

type ODBALoving struct {
	ID        bson.ObjectId `json:"id,omitempty" bson:"_id,omitempty"`
	DeviceID  bson.ObjectId `json:"device_id" bson:"device_id,omitempty"`
	Mark      int           `json:"mark,omitempty" bson:"mark,omitempty"`
	OwnerID   bson.ObjectId `json:"owner_id" bson:"owner_id,omitempty"`
	NickName  string        `json:"nickname" bson:"nickname,omitempty"`
	UpdatedAt *time.Time    `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
	TimeNum   uint32        `json:"time_num" bson:"time_num"`
	TimeKeep  uint32        `json:"time_keep,omitempty" bson:"time_keep,omitempty"`
}

func GetODBAParameterBulk(db *mgo.Session) *mgo.Bulk {
	c := db.DB(DruidDB).C(ODBAParameterCollection)
	return c.Bulk()
}

func GetODBALovingBulk(db *mgo.Session) *mgo.Bulk {
	c := db.DB(DruidDB).C(ODBALovingCollection)
	return c.Bulk()
}

func (odbaParameter *ODBAParameter) Create(db *mgo.Session) error {
	c := db.DB(DruidDB).C(ODBAParameterCollection)

	t := time.Now()
	odbaParameter.UpdatedAt = &t
	err := c.Insert(odbaParameter)
	if err != nil {
		return err
	}
	return nil
}

func ODBAParameterListCreate(db *mgo.Session, odbaPs []*ODBAParameter) error {
	c := db.DB(DruidDB).C(ODBAParameterCollection)

	t := time.Now()
	for i, _ := range odbaPs {
		odbaPs[i].UpdatedAt = &t
	}
	err := c.Insert(odbaPs)
	if err != nil {
		return err
	}
	return nil

}

func GetODBACountWithDeviceId(db *mgo.Session, id bson.ObjectId) (count int, err error) {
	c := db.DB(DruidDB).C(ODBAParameterCollection)
	return MongoCount(c, bson.M{"device_id": id})
}

func (odbaParameter *ODBAParameter) UpdateAll(db *mgo.Session) error {
	c := db.DB(DruidDB).C(ODBAParameterCollection)

	t := time.Now()
	odbaParameter.UpdatedAt = &t

	err := c.UpdateId(odbaParameter.ID, bson.M{"$set": odbaParameter})
	if err != nil {
		return err
	}
	return nil
}

func (odbaParamter *ODBAParameter) Upsert(db *mgo.Session) error {
	c := db.DB(DruidDB).C(ODBAParameterCollection)

	t := time.Now()
	odbaParamter.UpdatedAt = &t
	_, err := c.Upsert(bson.M{"device_id": odbaParamter.DeviceID, "time_num": odbaParamter.TimeNum}, bson.M{"$set": odbaParamter})
	if err != nil {
		return err
	}
	return nil
}

func GetODBAParameterWithTimeNum(db *mgo.Session, deviceId bson.ObjectId, timeNum uint32) *ODBAParameter {
	c := db.DB(DruidDB).C(ODBAParameterCollection)

	odbaParameter := ODBAParameter{}
	err := c.Find(bson.M{"device_id": deviceId, "time_num": timeNum}).One(&odbaParameter)
	if err != nil {
		//fmt.Println("没有找到：-", err)
		return nil
	}
	return &odbaParameter
}

func ListODBAParameterWithDeviceId(db *mgo.Session, id bson.ObjectId, rp *RequestParameter) ([]*ODBAParameter, error) {
	c := db.DB(DruidDB).C(ODBAParameterCollection)
	odbaParameters := []*ODBAParameter{}
	return odbaParameters, MongoList(c, bson.M{"device_id": id}, &odbaParameters, rp, nil)
}

func ListODBAParameterRange(db *mgo.Session, id bson.ObjectId, timeStartNum uint32, timeEndNum uint32, rp *RequestParameter) ([]*ODBAParameter, int, error) {

	c := db.DB(DruidDB).C(ODBAParameterCollection)
	odbaParameters := []*ODBAParameter{}
	m := bson.M{"device_id": id, "time_num": bson.M{"$gte": timeStartNum, "$lt": timeEndNum}}
	Rp := RequestParameter{}
	rp = &Rp
	rp.Sort = append(rp.Sort, "+time_num")
	err := MongoList(c, m, &odbaParameters, rp, nil)
	if err != nil {
		return nil, 0, err
	}
	count, _ := MongoCount(c, m)
	return odbaParameters, count, nil
}

func ListODBAParameterRangeManeyID(db *mgo.Session, ids []bson.ObjectId, timeStartNum uint32, timeEndNum uint32, rp *RequestParameter) ([]*ODBAParameter, int, error) {

	c := db.DB(DruidDB).C(ODBAParameterCollection)
	odbaParameters := []*ODBAParameter{}
	m := bson.M{"device_id": bson.M{"$in": ids}, "time_num": bson.M{"$gte": timeStartNum, "$lt": timeEndNum}}
	Rp := RequestParameter{}
	rp = &Rp
	rp.Sort = append(rp.Sort, "+device_id")
	rp.Sort = append(rp.Sort, "+time_num")
	err := MongoList(c, m, &odbaParameters, rp, nil)
	if err != nil {
		return nil, 0, err
	}
	count, _ := MongoCount(c, m)
	return odbaParameters, count, nil
}

func (odbaLoving *ODBALoving) Create(db *mgo.Session) error {
	c := db.DB(DruidDB).C(ODBALovingCollection)

	t := time.Now()
	odbaLoving.UpdatedAt = &t
	err := c.Insert(odbaLoving)
	if err != nil {
		return err
	}
	return nil
}

func UpdateODBALovingWithEndOne(db *mgo.Session, deviceId bson.ObjectId, endTime uint32) error {
	c := db.DB(DruidDB).C(ODBALovingCollection)

	odbaLoving := ODBALoving{}
	err := c.Find(bson.M{"device_id": deviceId}).Sort("-time_num").One(&odbaLoving)
	if err != nil {
		return ErrorConvert(err)
		return err
	}

	if odbaLoving.TimeKeep != 0 {
		return fmt.Errorf("TimeKeep have value")
	}

	if err := c.UpdateId(odbaLoving.ID,
		bson.M{"$set": bson.M{
			"time_keep": endTime - odbaLoving.TimeNum,
		}}); err != nil {
		return err
	}
	return nil

}

func (odbaLoving *ODBALoving) UpdateAll(db *mgo.Session) error {
	c := db.DB(DruidDB).C(ODBALovingCollection)

	t := time.Now()
	odbaLoving.UpdatedAt = &t

	err := c.UpdateId(odbaLoving.ID, bson.M{"$set": odbaLoving})
	if err != nil {
		return err
	}
	return nil
}

func GetODBALovingWithTimeNum(db *mgo.Session, deviceId bson.ObjectId, timeNum uint32) *ODBALoving {
	c := db.DB(DruidDB).C(ODBALovingCollection)

	odbaLoving := ODBALoving{}
	err := c.Find(bson.M{"device_id": deviceId, "time_num": timeNum}).One(&odbaLoving)
	if err != nil {
		return nil
	}
	return &odbaLoving
}

func ListODBALovingRange(db *mgo.Session, id bson.ObjectId, timeStartNum uint32, timeEndNum uint32, rp *RequestParameter) ([]*ODBALoving, int, error) {

	c := db.DB(DruidDB).C(ODBALovingCollection)
	odbaLovings := []*ODBALoving{}
	m := bson.M{"device_id": id, "time_num": bson.M{"$gte": timeStartNum, "$lt": timeEndNum}}
	Rp := RequestParameter{}
	rp = &Rp
	rp.Sort = append(rp.Sort, "+time_num")
	err := MongoList(c, m, &odbaLovings, rp, nil)
	if err != nil {
		return nil, 0, err
	}
	count, _ := MongoCount(c, m)
	return odbaLovings, count, nil
}

func ListODBALovingRangeManey(db *mgo.Session, ids []bson.ObjectId, timeStartNum uint32, timeEndNum uint32, rp *RequestParameter) ([]*ODBALoving, int, error) {

	c := db.DB(DruidDB).C(ODBALovingCollection)
	odbaLovings := []*ODBALoving{}
	m := bson.M{"device_id": bson.M{"$in": ids}, "time_num": bson.M{"$gte": timeStartNum, "$lt": timeEndNum}}
	Rp := RequestParameter{}
	rp = &Rp
	rp.Sort = append(rp.Sort, "+device_id")
	rp.Sort = append(rp.Sort, "+time_num")
	err := MongoList(c, m, &odbaLovings, rp, nil)
	if err != nil {
		return nil, 0, err
	}
	count, _ := MongoCount(c, m)
	return odbaLovings, count, nil
}

func RemoveAllODBALovingByDeviceID(db *mgo.Session, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(ODBALovingCollection)
	return MongoDestroy(c, bson.M{"device_id": id})
}
