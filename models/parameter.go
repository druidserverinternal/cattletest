package models

import (
//	"net/http"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
)

const (
	// MaxResult is mongo result limit
	MaxResult = 20

	// ResultLimitHeader is request result limit
	ResultLimitHeader = "X-Result-Limit"

	// ResultOffsetHeader is request result offset
	ResultOffsetHeader = "X-Result-Offset"

	// ResultSortHeader is request result sort
	ResultSortHeader = "X-Result-Sort"

	// ResultCountHeader is request result count
	ResultCountHeader = "X-Result-Count"

	ResultCompress = "X-Result-Compress"

	// ResultLimitParam url limit
	ResultLimitParam = "limit"

	// ResultOffsetParam url offset
	ResultOffsetParam = "offset"

	// ResultSortParam url sort
	ResultSortParam = "sort"

)

const (
	MaxLimit = 1000
)

// RequestParameter is request parameter struct
type RequestParameter struct {
	Offset     int
	Limit      int
	LimitOrign int
	Sort       []string
}

// ParseRequestParameter get request param, convert to RequestParameter
func ParseRequestParameter(c *gin.Context) *RequestParameter {
	offset := getOffset(c)
	limit := getLimit(c)
	sort := getSort(c)

	rp := RequestParameter{
		Limit	:	limit,
		Offset	:	offset,
		Sort	:	sort,
	}

	rp.LimitOrign = rp.Limit
	if rp.Limit > MaxLimit || rp.Limit <= 0 {
		rp.Limit = MaxLimit
	}

	return &rp
}

func getSort(c *gin.Context) []string {
	sorts := c.Query(ResultSortParam)
	if sorts == "" {
		sorts = c.Request.Header.Get(ResultSortHeader)
		if sorts == "" {
			return []string{}
		}
	}

	sort := []string{}
	ss := strings.Split(sorts, ",")
	for _, s := range ss {
		if s != "" {
			sort = append(sort, s)
		}
	}

	return sort
}

func getLimit(c *gin.Context) int {
	limit := c.Query(ResultLimitParam)
	if limit == "" {
		limit = c.Request.Header.Get(ResultLimitHeader)
		if limit == "" {
			return 0
		}
	}

	l, _ := strconv.Atoi(limit)
	return l
}

func getOffset(c *gin.Context) int {
	offset := c.Query(ResultOffsetParam)
	if offset == "" {
		offset = c.Request.Header.Get(ResultOffsetHeader)
		if offset == "" {
			return 0
		}
	}

	o, _ := strconv.Atoi(offset)
	return o
}

