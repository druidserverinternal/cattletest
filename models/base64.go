package models

import (
	"encoding/base64"
)

const base64Encode = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-/"

func Base64ToBytes(msg string) ([]byte, error) {
	dec := base64.NewEncoding(base64Encode)
	return dec.DecodeString(msg)
}



