package models

import (
	"bytes"
	"archive/zip"
)


func WriteToZip(file string, data []byte) ([]byte, error) {
	buf := new(bytes.Buffer)
	z := zip.NewWriter(buf)

	f, err := z.Create(file)
	if err != nil {
		z.Close()
		return nil, err
	}
	_, err = f.Write(data)
	if err != nil {
		z.Close()
		return nil, err
	}

	z.Close()
	return buf.Bytes(), nil
}

