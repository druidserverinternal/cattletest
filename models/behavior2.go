package models

import (
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	TimeCellHour  = 1 //3小时
	TimeCellDay   = 2 //1天
	TimeCellMonth = 3 //5天
)

const (
	DeadTime = time.Hour * 3
)

// Behavior2 is Behavior2 struct
type Behavior2 struct {
	ID              bson.ObjectId `json:"id,omitempty" bson:"_id,omitempty"`
	DeviceID        bson.ObjectId `json:"device_id" bson:"device_id"`
	Mark            int           `json:"mark,omitempty" bson:"mark,omitempty"`
//	CompanyID       bson.ObjectId `json:"company_id,omitempty" bson:"company_id,omitempty"`
//	CompanyName     string        `json:"company_name,omitempty" bson:"company_name,omitempty"`
	UUID            string        `json:"uuid,omitempty" bson:"uuid,omitempty"`
	FirmwareVersion int32         `json:"firmware_version,omitempty" bson:"firmware_version,omitempty"`
	Timestamp       *time.Time    `json:"timestamp,omitempty" bson:"timestamp,omitempty"`
	DeletedAt       *time.Time    `json:"deleted_at,omitempty" bson:"deleted_at,omitempty"`
	UpdatedAt       *time.Time    `json:"updated_at,omitempty" bson:"updated_at,omitempty"`

	ODBA    int32 `json:"odba" bson:"odba"`
	ODBAX   int32 `json:"odba_x,omitempty" bson:"odba_x"`
	ODBAY   int32 `json:"odba_y,omitempty" bson:"odba_y"`
	ODBAZ   int32 `json:"odba_z,omitempty" bson:"odba_z"`
	MeandlX int32 `json:"meandl_x,omitempty" bson:"meandl_x"`
	MeandlY int32 `json:"meandl_y,omitempty" bson:"meandl_y"`
	MeandlZ int32 `json:"meandl_z,omitempty" bson:"meandl_z"`

	Motion int `json:"motion,omitempty" bson:"motion,omitempty"`
	Act    int `json:"act,omitempty" bson:"act,omitempty"`
}

type BehaviorTypeHistory struct {
	ID                   bson.ObjectId `json:"id,omitempty" bson:"_id,omitempty"`
	DeviceID             bson.ObjectId `json:"device_id" bson:"device_id"`
	CompanyID            bson.ObjectId `json:"company_id,omitempty" bson:"company_id,omitempty"`
	CompanyName          string        `json:"company_name,omitempty" bson:"company_name,omitempty"`
	UpdatedAt            *time.Time    `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
	Mark                 int           `json:"mark,omitempty" bson:"mark,omitempty"`
	NickName             string        `json:"nickname" bson:"nickname,omitempty"`
	OwnerID              bson.ObjectId `json:"owner_id,omitempty" bson:"owner_id,omitempty"`
	TimeNum              uint32        `json:"time_num" bson:"time_num"`
	Motions              []int         `json:"-" bson:"motions,omitempty"`
	MotionEatNum         int           `json:"motion_eat_num" bson:"motion_eat_num"`
	MotionRuminationNum  int           `json:"motion_rumination_num" bson:"motion_rumination_num"`
	MotionOtherLowNum    int           `json:"motion_other_low_num" bson:"motion_other_low_num"`
	MotionOtherMediumNum int           `json:"motion_other_medium_num" bson:"motion_ohter_medium_num"`
	MotionOtherHightNum  int           `json:"motion_other_hight_num" bson:"motion_other_hight_num"`
}

type BehaviorAnalyzeReq struct {
	DeviceIDs []*bson.ObjectId `json:"device_ids,omitempty"`
	RoomID    bson.ObjectId    `json:"room_id,omitempty"`
	TimeStart string           `json:"time_start,omitempty"`
	TimeEnd   string           `json:"time_end",omitempty`
	Survive   int              `json:"survive,omitempty"`
	Species   int              `json:"species",omitempty`
	TimeCell  int              `json:"time_cell,omitempty"`
}

func GetBehavior2Bulk(db *mgo.Session) *mgo.Bulk {
	c := db.DB(DruidDB).C(Behavior2Collection)
	return c.Bulk()
}

func GetBehaviorTypeHisoryBulk(db *mgo.Session) *mgo.Bulk {
	c := db.DB(DruidDB).C(BehaviorTypeHistoryCollection)
	return c.Bulk()
}
func (behaviorTypeHistory *BehaviorTypeHistory) Create(db *mgo.Session) error {
	c := db.DB(DruidDB).C(BehaviorTypeHistoryCollection)

	t := time.Now()
	behaviorTypeHistory.UpdatedAt = &t
	err := c.Insert(behaviorTypeHistory)
	if err != nil {
		return err
	}
	return nil
}

func BehaviorTypeHistoryListCreate(db *mgo.Session, behaviorTypeHistorys []*BehaviorTypeHistory) error {
	c := db.DB(DruidDB).C(BehaviorTypeHistoryCollection)
	t := time.Now()

	for i, _ := range behaviorTypeHistorys {
		behaviorTypeHistorys[i].UpdatedAt = &t
	}

	err := c.Insert(behaviorTypeHistorys)
	if err != nil {
		return err
	}
	return nil
}

func (behaviorTypeHistory *BehaviorTypeHistory) UpdateAll(db *mgo.Session) error {
	c := db.DB(DruidDB).C(BehaviorTypeHistoryCollection)

	err := c.UpdateId(behaviorTypeHistory.ID, bson.M{"$set": behaviorTypeHistory})
	if err != nil {
		return err
	}
	return nil
}

func (behavior *Behavior2) VerifyTimestamp() bool {
	if behavior.Timestamp.Before(TimestampBegin) || behavior.Timestamp.After(GetTimestampEnd()) {
		return false
	}
	return true
}

// SearchBehavior2 search behavior fileter by request param
func SearchBehavior2(db *mgo.Session, m bson.M, rp *RequestParameter, se bson.M) ([]*Behavior2, error) {
	c := db.DB(DruidDB).C(Behavior2Collection)
	behavior := []*Behavior2{}
	return behavior, MongoList(c, m, &behavior, rp, se)
}

// SearchBehavior2Count search behavior count
func SearchBehavior2Count(db *mgo.Session, m bson.M) (count int, err error) {
	c := db.DB(DruidDB).C(Behavior2Collection)
	return MongoCount(c, m)
}

// DeleteBehavior2ByDeviceID delete all of device Behavior
func DeleteBehavior2ByDeviceID(db *mgo.Session, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(Behavior2Collection)
	return MongoDelete(c, bson.M{"device_id": id})
}

// DestroyBehavior2ByDeviceID delete all of device Behavior
func DestroyBehavior2ByDeviceID(db *mgo.Session, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(Behavior2Collection)
	return MongoDestroy(c, bson.M{"device_id": id})
}


func GetBehaviorTypeHisoryWithTimeNum(db *mgo.Session, deviceId bson.ObjectId, timeNum uint32) *BehaviorTypeHistory {
	c := db.DB(DruidDB).C(BehaviorTypeHistoryCollection)

	behaviorTypeHistory := BehaviorTypeHistory{}
	err := c.Find(bson.M{"device_id": deviceId, "time_num": timeNum}).One(&behaviorTypeHistory)
	if err != nil {
		return nil
	}
	return &behaviorTypeHistory
}

func ClearManyBehavior2Company(db *mgo.Session, ids []bson.ObjectId) error {
	c := db.DB(DruidDB).C(Behavior2Collection)
	if _, err := c.UpdateAll(
		bson.M{"device_id": bson.M{
			"$in": ids}},
		bson.M{"$unset": bson.M{
			"company_id":   1,
			"company_name": 1,
		}}); err != nil {
		return err
	}
	return nil
}

func RecoverBehavior2ByDeviceID(db *mgo.Session, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(Behavior2Collection)
	return MongoRecover(c, bson.M{"device_id": id})
}


func GetBehaviorTypeHisoryWithDeviceId(db *mgo.Session, deviceId bson.ObjectId, rp *RequestParameter) ([]*BehaviorTypeHistory, error) {
	c := db.DB(DruidDB).C(BehaviorTypeHistoryCollection)
	behaviorTypeHistorys := []*BehaviorTypeHistory{}
	return behaviorTypeHistorys, MongoList(c, bson.M{"device_id": deviceId, "deleted_at": nil}, &behaviorTypeHistorys, rp, nil)
}

func ListBehaviorTypeHistoryRange(db *mgo.Session, id bson.ObjectId, timeStartNum uint32, timeEndNum uint32, rp *RequestParameter) ([]*BehaviorTypeHistory, int, error) {
	c := db.DB(DruidDB).C(BehaviorTypeHistoryCollection)
	behaviorTypeHistorys := []*BehaviorTypeHistory{}
	m := bson.M{"device_id": id, "time_num": bson.M{"$gte": timeStartNum, "$lt": timeEndNum}}
	Rp := RequestParameter{}
	rp = &Rp
	rp.Sort = append(rp.Sort, "+time_num")
	err := MongoList(c, m, &behaviorTypeHistorys, rp, nil)
	if err != nil {
		return nil, 0, err
	}
	count, _ := MongoCount(c, m)
	return behaviorTypeHistorys, count, nil
}

func ListBehaviorTypeHistoryRangeManyID(db *mgo.Session, ids []bson.ObjectId, timeStartNum uint32, timeEndNum uint32, rp *RequestParameter) ([]*BehaviorTypeHistory, int, error) {
	c := db.DB(DruidDB).C(BehaviorTypeHistoryCollection)
	behaviorTypeHistorys := []*BehaviorTypeHistory{}
	m := bson.M{"device_id": bson.M{"$in": ids}, "time_num": bson.M{"$gte": timeStartNum, "$lt": timeEndNum}}
	Rp := RequestParameter{}
	rp = &Rp
	rp.Sort = append(rp.Sort, "+device_id")
	rp.Sort = append(rp.Sort, "+time_num")
	err := MongoList(c, m, &behaviorTypeHistorys, rp, nil)
	if err != nil {
		return nil, 0, err
	}
	count, _ := MongoCount(c, m)
	return behaviorTypeHistorys, count, nil
}

func (behavior2 *Behavior2) Create(db *mgo.Session) error {
	c := db.DB(DruidDB).C(Behavior2Collection)

	t := time.Now()
	behavior2.UpdatedAt = &t
	err := c.Insert(behavior2)
	if err != nil {
		return err
	}
	return nil
}

func Behavior2ListCreate(db *mgo.Session, behaviors []*Behavior2) error {
	c := db.DB(DruidDB).C(Behavior2Collection)
	t := time.Now()

	for i, _ := range behaviors {
		behaviors[i].UpdatedAt = &t
	}

	err := c.Insert(behaviors)
	if err != nil {
		return err
	}
	return nil
}

// ListValidBehavior2 list all of Behavior by device
func ListValidBehavior2(db *mgo.Session, rp *RequestParameter, se bson.M) ([]*Behavior2, error) {
	c := db.DB(DruidDB).C(Behavior2Collection)
	behaviors := []*Behavior2{}
	return behaviors, MongoList(c, bson.M{"deleted_at": nil}, &behaviors, rp, se)
}

func CountValidBehavior2(db *mgo.Session) (int, error) {
	c := db.DB(DruidDB).C(Behavior2Collection)
	return MongoCount(c, bson.M{"deleted_at": nil})
}

// FindBehavior2ByID find a behavior by id
func FindBehavior2ByID(db *mgo.Session, id bson.ObjectId, se bson.M) (*Behavior2, error) {
	c := db.DB(DruidDB).C(Behavior2Collection)
	behavior := Behavior2{}
	if err := MongoFind(c, bson.M{"_id": id}, &behavior, "", se); err != nil {
		return nil, err
	}
	return &behavior, nil
}

// ListBehavior2ByDeviceID list all of behaviors by device
func ListBehavior2ByDeviceIDWithNoMotion(db *mgo.Session, id bson.ObjectId, rp *RequestParameter) ([]*Behavior2, error) {
	c := db.DB(DruidDB).C(Behavior2Collection)
	behaviors := []*Behavior2{}
	dd,_ := time.ParseDuration("24h")
	t := time.Now().Add(dd)
	return behaviors, MongoList(c, bson.M{"device_id": id, "timestamp": bson.M{"$lt":t},"motion": nil, "deleted_at": nil}, &behaviors, rp, nil)
}

func ListBehavior2ByDeviceIDWithNoAct(db *mgo.Session, id bson.ObjectId, rp *RequestParameter) ([]*Behavior2, error) {
	c := db.DB(DruidDB).C(Behavior2Collection)
	behaviors := []*Behavior2{}
	dd,_ := time.ParseDuration("24h")
	t := time.Now().Add(dd)
	return behaviors, MongoList(c, bson.M{"device_id": id, "timestamp": bson.M{"$lt":t},"act": nil, "deleted_at": nil}, &behaviors, rp, nil)
}

// ListBehavior2ByDeviceID list all of behaviors by device
func ListBehavior2ByDeviceID(db *mgo.Session, id bson.ObjectId, rp *RequestParameter) ([]*Behavior2, error) {
	c := db.DB(DruidDB).C(Behavior2Collection)
	behaviors := []*Behavior2{}
	dd,_ := time.ParseDuration("24h")
	t := time.Now().Add(dd)
	return behaviors, MongoList(c, bson.M{"device_id": id, "timestamp": bson.M{"$lt":t},"deleted_at": nil}, &behaviors, rp, nil)
}

// CountBehavior2ByFilter list all of behaviors by filter
func CountBehavior2ByFilter(db *mgo.Session, m bson.M) (int, error) {
	c := db.DB(DruidDB).C(Behavior2Collection)
	return MongoCount(c, m)
}

// CountBehavior2ByDeviceID list all of behaviors by device
func CountBehavior2ByDeviceID(db *mgo.Session, id bson.ObjectId) (int, error) {
	c := db.DB(DruidDB).C(Behavior2Collection)
	return MongoCount(c, bson.M{"device_id": id, "deleted_at": nil})
}

// GetBehavior2Queue device hardware and firmware version
func GetBehavior2Queue(db *mgo.Session) *mgo.Bulk {
	c := db.DB(DruidDB).C(Behavior2Collection)
	return c.Bulk()
}

func (beh *Behavior2) UpsertByTimestamp(db *mgo.Session) error {
	c := db.DB(DruidDB).C(Behavior2Collection)
	if _, err := c.Upsert(
		bson.M{"device_id": beh.DeviceID, "timestamp": beh.Timestamp},
		bson.M{"$set": beh}); err != nil {
		return err
	}
	return nil
}

func (beh *Behavior2) UpdateMotion(db *mgo.Session) error {
	c := db.DB(DruidDB).C(Behavior2Collection)
	if err := c.UpdateId(beh.ID,
		bson.M{"$set": bson.M{
			"motion": beh.Motion,
		}}); err != nil {
		return err
	}
	return nil
}
func (beh *Behavior2) UpdateAct(db *mgo.Session) error {
	c := db.DB(DruidDB).C(Behavior2Collection)
	if err := c.UpdateId(beh.ID,
		bson.M{"$set": bson.M{
			"act": beh.Act,
		}}); err != nil {
		return err
	}
	return nil
}

func (beh *Behavior2) UpdateODBA(db *mgo.Session) error {
	c := db.DB(DruidDB).C(Behavior2Collection)
	if err := c.UpdateId(beh.ID,
		bson.M{"$set": bson.M{
			"odba": beh.ODBA,
		}}); err != nil {
		return err
	}
	return nil
}

// FindBehavior2ByTimestamp find a behavior2 by timestamp
func FindBehavior2ByTimestamp(db *mgo.Session, id bson.ObjectId, t *time.Time, se bson.M) *Behavior2 {
	c := db.DB(DruidDB).C(Behavior2Collection)
	behavior := Behavior2{}
	if err := MongoFind(c, bson.M{"device_id": id, "timestamp": t}, &behavior, "", se); err != nil {
		return nil
	}
	return &behavior
}

// FindLatestBehavior2ByFilter find a behavior by timestamp
func FindLatestBehavior2ByFilter(db *mgo.Session, m bson.M) (*Behavior2, error) {
	c := db.DB(DruidDB).C(Behavior2Collection)
	behavior := Behavior2{}
	if err := MongoFind(c, m, &behavior, "-timestamp", nil); err != nil {
		return nil, ErrorConvert(err)
	}
	return &behavior, nil
}

// FindLatestBehavior2ByTimestamp find a behavior by timestamp
func FindLatestBehavior2ByTimestamp(db *mgo.Session, id bson.ObjectId, se bson.M) (*Behavior2, error) {
	c := db.DB(DruidDB).C(Behavior2Collection)
	behavior := Behavior2{}
	if err := MongoFind(c, bson.M{"device_id": id}, &behavior, "-timestamp", se); err != nil {
		return nil, ErrorConvert(err)
	}
	return &behavior, nil
}

// ListBehavior2ByFilter behavior2 by filter
func ListBehavior2ByFilter(db *mgo.Session, m interface{}, rp *RequestParameter, se bson.M) ([]*Behavior2, error) {
	c := db.DB(DruidDB).C(Behavior2Collection)
	result := []*Behavior2{}
	return result, MongoList(c, m, &result, rp, se)
}

func FindLastSleepFromBehavior2(db *mgo.Session, id bson.ObjectId) (*Behavior2, error) {
	c := db.DB(DruidDB).C(Behavior2Collection)
	beh := &Behavior2{}

	err := c.Find(bson.M{"device_id": id, "timestamp": bson.M{"$gt": TimestampBegin}, "odba": bson.M{"$gt": 150}}).Sort("-timestamp").Limit(1).One(beh)
	if err != nil {
		if ErrorConvert(err) != nil {
			return nil, err
		} else {
			err = c.Find(bson.M{"device_id": id, "timestamp": bson.M{"$gt": TimestampBegin}}).Sort("timestamp").Limit(1).One(beh)
			if err != nil {
				return nil, err
			}
			return beh, nil
		}
	}

	err = c.Find(bson.M{"device_id": id, "timestamp": bson.M{"$gt": beh.Timestamp}}).Sort("timestamp").Limit(1).One(beh)
	if err != nil {
		return nil, ErrorConvert(err)
	}
	return beh, nil
}


func CheckDead(db *mgo.Session, device *Device) (bool, error) {
	if device.LastBeh2Timestamp == nil || device.LastBeh2Timestamp.Before(TimestampBegin) {
		if device.Survive != SurviveLive {
			if err := device.UnMarkSurvive(db); err != nil {
				return false, err
			}
		}
	}

	beh, err := FindLastSleepFromBehavior2(db, device.ID)
	if err != nil {
		return false, Error("Find last sleep time failed.", err)
	}
	if beh == nil {
		if device.Survive != SurviveLive {
			if err := device.UnMarkSurvive(db); err != nil {
				return false, Error("unmark survive failed.")
			}
		}
		return false, nil
	}

	if device.Survive == SurviveDead {
		if beh.Timestamp.Add(DeadTime).After(*(device.LastBeh2Timestamp)) {
			if err := device.UnMarkSurvive(db); err != nil {
				return false, Error("unmark survive failed.")
			}
		}
		return false, nil
	}

	if beh.Timestamp.Add(DeadTime).Before(*(device.LastBeh2Timestamp)) {
		if err := device.MarkSurvive(db, SurviveDead, beh.Timestamp); err != nil {
			return false, Error("mark survive live failed.")
		}
		return true, nil
	}

	return false, nil
}
