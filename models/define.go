package models

const (
	QueryTimeBegin = "begin"
	QueryTimeEnd   = "end"
	QueryTimeLast  = "last"

	QueryDate = "date"
)
