package models

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

const (
	AlertTypeBirdDead   = "bird"
	AlertTypeDeviceDead = "device"
)

type Alert struct {
	BirdDead   int     `json:"bird_dead,omitempty" bson:"bird_dead,omitempty"` //minute
	DeviceDead int     `json:"device_dead,omitempty" bson:"device,omitempty"`  //day
	BatteryLow float64 `json:"battery_low,omitempty" bson:"battery_low,omitempty"`
}

type AlertTimestamp struct {
	ID       bson.ObjectId `json:"id,omitempty" bson:"_id,omitempty"`
	DeviceID bson.ObjectId `json:"device_id,omitempty" bson:"device_id,omitempty"`
	UUID     string        `json:"uuid" bson:"uuid,omitempty"`
	Mark     int           `json:"mark" bson:"mark,omitempty"`

	Type string `json:"type" bson:"type,omitempty"`

	UpdatedAt *time.Time `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
	Timestamp *time.Time `json:"timestamp,omitempty" bson:"timestamp,omitempty"`
}
