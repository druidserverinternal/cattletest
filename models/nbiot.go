package models

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"

	logger "github.com/sirupsen/logrus"

	"github.com/spf13/viper"
)

var NBAccessToken string  //login获取的认证值 每隔半小时跟新一次
var NBRefreshToken string //刷新认证
var NBExpiresIn int       // 有效时间

const (
	NBServeId_SendMsg = "000010"
)

const (
	NBCallBack_Url_SendMsg            = "sendmsg"
	NBCallBack_Url_DeviceAdded        = "deviceadded"
	NBCallBack_Url_DeviceInfoChanged  = "deviceinfochanged"
	NBCallBack_Url_DeviceDataChanged  = "devicedatachanged"
	NBCallBack_Url_DeviceDeleted      = "devicedeleted"
	NBCallBack_Url_DeviceEvent        = "deviceevent"
	NBCallBack_Url_MessageConfirm     = "messageconfirm"
	NBCallBack_Url_CommandRsp         = "commandrsp"
	NBCallBack_Url_ServiceInfoChanged = "serviceinfochanged"
	NBCallBack_Url_RuleEvent          = "ruleevent"
	NBCallBack_Url_BindDevice         = "binddevice"
)
const (
	NBSeverName_SendMsg = "SendMsgToDevice"
)
const (
	NBiot_URL_Login               = "/iocm/app/sec/v1.1.0/login"
	NBiot_URL_Refresh             = "/iocm/app/sec/v1.1.0/refreshToken"
	NBiot_URL_Sub                 = "/iocm/app/sub/v1.2.0/subscribe"
	NBiot_URL_Reg_Devices         = "/iocm/app/reg/v1.1.0/devices"
	NBiot_URL_Device_Data_History = "/iocm/app/data/v1.1.0/deviceDataHistory"
	NBiot_URL_SendMsg             = "/iocm/app/cmd/v1.4.0/deviceCommands"
	//NBiot_URL_Login = "/iocm/app/sec/v1.1.0/login"
)

type NBCallBackResp struct {
	NotifyType string           `json:"notifyType,omitempty"`
	DeviceId   string           `json:"deviceId,omitempty"`
	GatewayId  string           `json:"gatewayId,omitempty"`
	RequestId  string           `json:"requestId,omitempty"`
	Service    NBCallBackServic `json:"service,omitempty"`
	EventTime  string           `json:"eventTime,omitempty"`
}

type NBCallBackServic struct {
	ServiceId   string                 `json:"serviceId,omitempty"`
	ServiceType string                 `json:"serviceType,omitempty"`
	Data        map[string]interface{} `json:"data,omitempty"`
}

type NBMsgBody struct {
	Title       string `json:"title"`
	Name        string `json:"name"`
	Message     string `json:"message"`
	Description string `json:"description,omitempty"`
	Version     string `json:"version"`
}
type NBiotLoginBoy struct {
	AccessToken  string `json:"accessToken"`
	TokenType    string `json:"tokenType"`
	ExpiresIn    int    `json:"expiresIn"`
	RefreshToken string `json:"refreshToken"`
	Scope        string `json:"scope"`
}
type NBDeviceDataHistory struct {
	DeviceId  string `json:"deviceId"`
	GatewayId string `json:"gatewayId"`
	/*
		ServiceId   string `json:"serviceId"`
		Property    string `json:"property"`
		AppId       string `json:"appId"`
		PageNo      string `json:"pageNo"`
		PageSize    string `json:"pageSize"`
		StartTime   string `json:"startTime"`
		EndTime     string `json:"endTime"`
		AccessToken string `json:"accessToken"`
	*/
}
type NBDeviceDataResp struct {
	TotalCount            int                    `json:"totalCount,omitempty"`
	PageNo                int                    `json:"pageNo,omitempty"`
	PageSize              int                    `json:"pageSize,omitempty"`
	DeviceDataHistoryDTOs []DeviceDataHistoryDTO `json:"deviceDataHistoryDTOs,omitempty"`
}
type DeviceDataHistoryDTO struct {
	DeviceId  string                 `json:"deviceId"`
	ServiceId string                 `json:"serviceId"`
	GatewayId string                 `json:"gatewayId"`
	AppId     string                 `json:"appId"`
	Data      map[string]interface{} `json:"data"`
	Timestamp string                 `json:"timestamp"`
}
type NBiotSub struct {
	NotifyType  string `json:"notifyType"`
	Callbackurl string `json:"callbackurl"`
}
type NBiotSubBody struct {
	NotifyType  string `json:"notifyType"`
	Callbackurl string `json:"callbackurl"`
}
type NBTest struct {
	Latitude       int `json:"Latitude"`
	Altitude       int `json:"Altitude"`
	BatteryVoltage int `json:"BatteryVoltage"`
	Temperature    int `json:"Temperature"`
	Speed          int `json:"Speed"`
	Course         int `json:"Course"`
	SignalStrength int `json:"SignalStrength"`
	FixTime        int `json:"FixTime"`
	Timestamp      int `json:"Timestamp"`
	Longitude      int `json:"Longitude"`
}

/////////////////////////////////////////////////////////////////
//测试专区
type NBTest2 struct {
	Light string `json:"Light"`
}

type NBPostDeviceCommandReqTest2 struct {
	DeviceId    string              `json:"deviceId"`
	Command     NBCommandDTOV4Test2 `json:"command"`
	Callbackurl string              `json:"callbackUrl,omitempty"`
	ExpireTime  int                 `json:"expireTime,omitempty"` //超时时间 单位秒  第N秒后超时 默认时间48小时= 48 * 60 * 60
}
type NBCommandDTOV4Test2 struct {
	ServiceId string                 `json:"serviceId"`
	Method    string                 `json:"method"`
	Paras     map[string]interface{} `json:"paras,omitempty"`
	//Paras *NBMsgBody `json:"paras,omitempty"`
}

////////////////////////////////////////////////////////
//////////////////////////////////////////////////

type NBPostDeviceCommandReq struct {
	DeviceId    string         `json:"deviceId"`
	Command     NBCommandDTOV4 `json:"command"`
	Callbackurl string         `json:"callbackUrl,omitempty"`
	ExpireTime  int            `json:"expireTime,omitempty"` //超时时间 单位秒  第N秒后超时 默认时间48小时= 48 * 60 * 60
}
type NBCommandDTOV4 struct {
	ServiceId string  `json:"serviceId"`
	Method    string  `json:"method"`
	Paras     *NBTest `json:"paras,omitempty"`
	//Paras *NBMsgBody `json:"paras,omitempty"`
}
type NBRefreshBody struct {
	Appid        string `json:"appId"`
	Secret       string `json:"secret"`
	RefreshToken string `json:"refreshToken"`
}

func SendDeviceMsgTest2(callBackBody *NBCallBackResp) (httpStatus int, code int, err error) {
	address := viper.GetString("nbiot.address")
	appid := viper.GetString("nbiot.appid")
	client := GetNbiotClient()
	allAddress := address + NBiot_URL_SendMsg + "?" + "appId=" + appid
	deviceCommands := NBPostDeviceCommandReqTest2{}
	deviceCommands.DeviceId = callBackBody.DeviceId
	deviceCommands.Command.ServiceId = callBackBody.Service.ServiceId //NBServeId_SendMsg
	deviceCommands.Command.Method = "setting"                         //NBSeverName_SendMsg
	deviceCommands.Command.Paras = callBackBody.Service.Data
	deviceCommands.ExpireTime = 0
	deviceCommands.Callbackurl = NBCallBack_Url_SendMsg

	b, _ := json.Marshal(deviceCommands)
	body := bytes.NewBuffer([]byte(b))
	resp, err := NBHttpsReq("POST", client, allAddress, body)
	if err != nil {
		body, _ := ioutil.ReadAll(resp.Body)
		fmt.Println(string(body))

		return 0, -1, err
	}
	body1, _ := ioutil.ReadAll(resp.Body)
	fmt.Println(string(body1))

	httpStatus = resp.StatusCode
	return httpStatus, 0, nil
}

func SendDeviceMsgTest3(nbMsgBody *NBTest2, deviceId string) (httpStatus int, code int, err error) {
	mapp := make(map[string]interface{})
	mapp["Light"] = nbMsgBody.Light
	address := viper.GetString("nbiot.address")
	appid := viper.GetString("nbiot.appid")
	client := GetNbiotClient()
	allAddress := address + NBiot_URL_SendMsg + "?" + "appId=" + appid
	deviceCommands := NBPostDeviceCommandReqTest2{}
	deviceCommands.DeviceId = deviceId
	deviceCommands.Command.ServiceId = "LightInfo" //NBServeId_SendMsg
	deviceCommands.Command.Method = "setting"      //NBSeverName_SendMsg
	deviceCommands.Command.Paras = mapp
	deviceCommands.ExpireTime = 0
	deviceCommands.Callbackurl = NBCallBack_Url_SendMsg

	b, _ := json.Marshal(deviceCommands)
	body := bytes.NewBuffer([]byte(b))
	resp, err := NBHttpsReq("POST", client, allAddress, body)
	if err != nil {
		body, _ := ioutil.ReadAll(resp.Body)
		fmt.Println(string(body))

		return 0, -1, err
	}
	body1, _ := ioutil.ReadAll(resp.Body)
	fmt.Println(string(body1))

	httpStatus = resp.StatusCode
	return httpStatus, 0, nil
}
func SendDeviceMsgTest(nbMsgBody *NBTest, deviceId string) (httpStatus int, code int, err error) {
	address := viper.GetString("nbiot.address")
	appid := viper.GetString("nbiot.appid")
	client := GetNbiotClient()
	allAddress := address + NBiot_URL_SendMsg + "?" + "appId=" + appid
	deviceCommands := NBPostDeviceCommandReq{}
	deviceCommands.DeviceId = deviceId
	deviceCommands.Command.ServiceId = "GpsInfo" //NBServeId_SendMsg
	deviceCommands.Command.Method = "setting"    //NBSeverName_SendMsg
	deviceCommands.Command.Paras = nbMsgBody
	deviceCommands.ExpireTime = 0
	deviceCommands.Callbackurl = NBCallBack_Url_SendMsg

	b, _ := json.Marshal(deviceCommands)
	body := bytes.NewBuffer([]byte(b))
	resp, err := NBHttpsReq("POST", client, allAddress, body)
	if err != nil {
		body, _ := ioutil.ReadAll(resp.Body)
		fmt.Println(string(body))

		return 0, -1, err
	}
	body1, _ := ioutil.ReadAll(resp.Body)
	fmt.Println(string(body1))

	httpStatus = resp.StatusCode
	return httpStatus, 0, nil
}

/*
func SendDeviceMsg(nbMsgBody *NBMsgBody, deviceId string) (httpStatus int, code int, err error) {
	address := viper.GetString("nbiot.address")
	appid := viper.GetString("nbiot.appid")
	client := GetNbiotClient()
	allAddress := address + NBiot_URL_SendMsg + "?" +
		"appId=" + appid
	deviceCommands := NBiotDeviceCommandReq{}
	deviceCommands.Body.DeviceId = deviceId
	deviceCommands.Body.Command.ServiceId = NBServeId_SendMsg
	deviceCommands.Body.Command.Method = NBSeverName_SendMsg
	deviceCommands.Body.Command.Paras = nbMsgBody
	deviceCommands.Body.Callbackurl = NBCallBack_Url_SendMsg

	b, _ := json.Marshal(deviceCommands)
	body := bytes.NewBuffer([]byte(b))
	resp, err := NBHttpsReq("POST", client, allAddress, body)
	if err != nil {
		return 0, -1, err
	}
	httpStatus = resp.StatusCode
	return httpStatus, 0, nil
}
*/

func NBSubAllTopic() {
	alltopic := []string{
		"deviceAdded",
		"deviceInfoChanged",
		"deviceDataChanged",
		"deviceDeleted",
		"deviceEvent",
		"messageConfirm",
		"commandRsp",
		"serviceInfoChanged",
		"ruleEvent",
		"bindDevice",
	}
	baseCallBack := viper.GetString("nbiot.callback")
	allCallBack := []string{
		baseCallBack + NBCallBack_Url_DeviceAdded,
		baseCallBack + NBCallBack_Url_DeviceInfoChanged,
		baseCallBack + NBCallBack_Url_DeviceDataChanged,
		baseCallBack + NBCallBack_Url_DeviceDeleted,
		baseCallBack + NBCallBack_Url_DeviceEvent,
		baseCallBack + NBCallBack_Url_MessageConfirm,
		baseCallBack + NBCallBack_Url_CommandRsp,
		baseCallBack + NBCallBack_Url_ServiceInfoChanged,
		baseCallBack + NBCallBack_Url_RuleEvent,
		baseCallBack + NBCallBack_Url_BindDevice,
	}

	address := viper.GetString("nbiot.address")
	allAddress := address + NBiot_URL_Sub
	for i := 0; i < len(alltopic); i++ {
		subBody := NBiotSubBody{}
		subBody.NotifyType = alltopic[i]
		subBody.Callbackurl = allCallBack[i]

		client := GetNbiotClient()
		b, err := json.Marshal(subBody)
		body := bytes.NewBuffer([]byte(b))
		resp, err := NBHttpsReq("POST", client, allAddress, body)
		if err != nil {
			logger.Debugf("Sub topice---%s is error, err :%s ", alltopic[i], err.Error)
			continue
		}
		if resp.StatusCode != http.StatusCreated && resp.StatusCode != http.StatusOK {
			logger.Debugf("Sub topice---%s is error, code :%d ", alltopic[i], resp.StatusCode)
			continue
		}
		logger.Infof("Sub topice---%s is success   address: %s", alltopic[i], allCallBack[i])
	}
}
func NBHttpsReq(way string, client *http.Client, address string, Body io.Reader) (*http.Response, error) {
	req, err := http.NewRequest(way, address, Body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("app_key", viper.GetString("nbiot.appid"))
	req.Header.Set("Authorization", "Bearer "+NBAccessToken)

	return client.Do(req)
}

func GetNbDeviceHistoryData(nbDeviceData *NBDeviceDataHistory) (*NBDeviceDataResp, int, error) {
	address := viper.GetString("nbiot.address")
	client := GetNbiotClient()
	allAddress := address + NBiot_URL_Device_Data_History + "?" +
		"deviceId=" + nbDeviceData.DeviceId +
		"&gatewayId=" + nbDeviceData.GatewayId
	/*	"&serviceId=" + nbDeviceData.ServiceId +
		"&pageNo=" + nbDeviceData.PageNo +
		"&pageSize=" + nbDeviceData.PageSize +
		"&startTime=" + nbDeviceData.StartTime+
		"&endTime=" + nbDeviceData.EndTime +
		"&property=" + nbDeviceData.Property +
		"&appId=" + viper.GetString("nbiot.appid")
	*/
	resp, err := NBHttpsReq("GET", client, allAddress, nil)
	if err != nil {
		return nil, -1, err
	}

	fmt.Println("-------", resp.Status)
	fmt.Println("-------", resp.StatusCode)
	defer resp.Body.Close()
	var bodyResp NBDeviceDataResp
	decoder := json.NewDecoder(resp.Body)
	if err := decoder.Decode(&bodyResp); err != nil {
		body, _ := ioutil.ReadAll(resp.Body)
		fmt.Println(string(body))
		return nil, -1, err
	}

	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println(string(body))
	return &bodyResp, resp.StatusCode, nil
}
func NBiotSubscibe(nboitSub *NBiotSub) (int, error) {

	address := viper.GetString("nbiot.address")
	appid := viper.GetString("nbiot.appid")
	subBody := NBiotSubBody{}
	subBody.NotifyType = nboitSub.NotifyType
	subBody.Callbackurl = nboitSub.Callbackurl
	fmt.Println("notifyType: ", subBody.NotifyType)
	fmt.Println("callbackurl: ", subBody.Callbackurl)

	b, err := json.Marshal(subBody)
	body := bytes.NewBuffer([]byte(b))
	client := GetNbiotClient()
	req, err := http.NewRequest("POST", address+NBiot_URL_Sub, body)
	if err != nil {
		return -1, err // handle error
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("app_key", appid)
	req.Header.Set("Authorization", "Bearer "+NBAccessToken)
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Post  error:", err)
		return -1, err
	}
	fmt.Println("-------", resp.Status)
	fmt.Println("-------", resp.StatusCode)

	defer resp.Body.Close()
	var loginBody NBiotLoginBoy
	decoder := json.NewDecoder(resp.Body)
	if err := decoder.Decode(&loginBody); err != nil {
		body, _ := ioutil.ReadAll(resp.Body)
		fmt.Println(string(body))
		return -1, err
	}
	respBody, _ := ioutil.ReadAll(resp.Body)
	fmt.Println(string(respBody))

	return resp.StatusCode, nil
}

func NBRefresh() (int, error) {
	address := viper.GetString("nbiot.address")
	appid := viper.GetString("nbiot.appid")
	secert := viper.GetString("nbiot.secret")
	refreshBody := NBRefreshBody{}
	refreshBody.Appid = appid
	refreshBody.RefreshToken = NBRefreshToken
	refreshBody.Secret = secert

	b, _ := json.Marshal(refreshBody)
	body := bytes.NewBuffer([]byte(b))

	client := GetNbiotClient()
	req, err := http.NewRequest("POST", address+NBiot_URL_Refresh, body)
	if err != nil {
		return -1, err // handle error
	}
	req.Header.Set("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		return -1, err
	}

	httpStatus := resp.StatusCode
	defer resp.Body.Close()
	var refreshRspoBody NBiotLoginBoy
	decoder := json.NewDecoder(resp.Body)
	if err := decoder.Decode(&refreshRspoBody); err != nil {
		return httpStatus, err
	}
	NBAccessToken = refreshRspoBody.AccessToken
	NBRefreshToken = refreshRspoBody.RefreshToken
	NBExpiresIn = refreshRspoBody.ExpiresIn
	return httpStatus, nil
}

func NBiotLogin() (*NBiotLoginBoy, int, error) {
	address := viper.GetString("nbiot.address")
	appid := viper.GetString("nbiot.appid")
	secert := viper.GetString("nbiot.secret")

	client := GetNbiotClient()
	resp, err := client.Post(address+NBiot_URL_Login, "application/x-www-form-urlencoded", bytes.NewBuffer([]byte("appId="+appid+"&secret="+secert)))
	if err != nil {
		return nil, -1, err
	}
	httpStatus := resp.StatusCode
	defer resp.Body.Close()
	var loginBody NBiotLoginBoy
	decoder := json.NewDecoder(resp.Body)
	if err := decoder.Decode(&loginBody); err != nil {
		body, _ := ioutil.ReadAll(resp.Body)
		fmt.Println(string(body))
		return nil, httpStatus, err
	}
	NBAccessToken = loginBody.AccessToken
	NBRefreshToken = loginBody.RefreshToken
	NBExpiresIn = loginBody.ExpiresIn
	fmt.Println("a new Token :", NBAccessToken)
	fmt.Println("a new refreshToken :", NBRefreshToken)
	return &loginBody, httpStatus, nil
}

func GetNbiotClient() *http.Client {
	cert := viper.GetString("nbiot.tls.cert")
	key := viper.GetString("nbiot.tls.key")
	cliCrt, err := tls.LoadX509KeyPair(cert, key)
	if err != nil {
		fmt.Println("Loadx509keypair err:", err)
		return nil
	}
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true,
			Certificates:       []tls.Certificate{cliCrt},
		},
	}

	client := &http.Client{Transport: tr}
	return client
}
