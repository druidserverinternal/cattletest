package models

import (
//	"encoding/base64"
//	"encoding/json"

//	"fmt"
	"github.com/0987363/jpush-api-go-client"
)


type JPush struct {
//	Payload		*jpushclient.PayLoad
	Client		*jpushclient.PushClient
}

func NewJPush(secret, appKey string) *JPush {
	client := jpushclient.NewPushClient(secret, appKey) 
//		Server	:	"https://api.jpush.cn/v3/push",
//		Auth	:	"Basic " + base64.StdEncoding.EncodeToString([]byte(appKey+":"+secret)),
//	fmt.Println("auth:", client)
	return &JPush {
		Client	:	client,
	}
}

func (jp *JPush)SendCN(alert string, dst []string, release bool) error {
	var pf jpushclient.Platform
	pf.Add(jpushclient.ANDROID)
	pf.Add(jpushclient.IOS)

	var ad jpushclient.Audience
	ad.SetTag([]string{"cn"})
	ad.SetAlias(dst)

	var notice jpushclient.Notice
	notice.SetAlert(alert)
//	notice.SetAndroidNotice(&jpushclient.AndroidNotice{Alert: "AndroidNotice"})
//	notice.SetIOSNotice(&jpushclient.IOSNotice{Alert: "IOSNotice"})

	payload := jpushclient.NewPushPayLoad()
	payload.SetPlatform(&pf)
	payload.SetAudience(&ad)
	payload.SetNotice(&notice)
	payload.Options.ApnsProduction = release

	bytes, _ := payload.ToBytes()


//	c := jpushclient.NewPushClient("04a027ab23e38896a5667e24", "a2be1eaae0867b88c087eb7c")
	_, err := jp.Client.Send(bytes)
	if err != nil {
		return err
	}

	return nil
}

func (jp *JPush)Send(alert string, dst []string, release bool) error {
	var pf jpushclient.Platform
	pf.Add(jpushclient.ANDROID)
	pf.Add(jpushclient.IOS)

	var ad jpushclient.Audience
	ad.SetTagNot([]string{"cn"})
	ad.SetAlias(dst)

	var notice jpushclient.Notice
	notice.SetAlert(alert)
//	notice.SetAndroidNotice(&jpushclient.AndroidNotice{Alert: "AndroidNotice"})
//	notice.SetIOSNotice(&jpushclient.IOSNotice{Alert: "IOSNotice"})

	payload := jpushclient.NewPushPayLoad()
	payload.SetPlatform(&pf)
	payload.SetAudience(&ad)
	payload.SetNotice(&notice)
	payload.Options.ApnsProduction = release

	bytes, _ := payload.ToBytes()

	_, err := jp.Client.Send(bytes)
	if err != nil {
		return err
	}

	return nil
}

func (jp *JPush)SendAll(alert string, dst []string, release bool) error {
	var pf jpushclient.Platform
	pf.Add(jpushclient.ANDROID)
	pf.Add(jpushclient.IOS)

	var ad jpushclient.Audience
	ad.SetAlias(dst)

	var notice jpushclient.Notice
	notice.SetAlert(alert)
//	notice.SetAndroidNotice(&jpushclient.AndroidNotice{Alert: "AndroidNotice"})
//	notice.SetIOSNotice(&jpushclient.IOSNotice{Alert: "IOSNotice"})

	payload := jpushclient.NewPushPayLoad()
	payload.SetPlatform(&pf)
	payload.SetAudience(&ad)
	payload.SetNotice(&notice)
	payload.Options.ApnsProduction = release

	bytes, _ := payload.ToBytes()

	_, err := jp.Client.Send(bytes)
	if err != nil {
		return err
	}

	return nil
}

