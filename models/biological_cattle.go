package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
)

const (
	BulkMaxCount = 2000
)

type Cattle struct {
	ID                   bson.ObjectId   `json:"id" bson:"_id,omitempty"`
	UpdatedAt            *time.Time      `json:"updated_at" bson:"updated_at,omitempty"`
	UpdatedBy            bson.ObjectId   `json:"updated_by,omitempty" bson:"updated_by,omitempty"`
	DeviceID             bson.ObjectId   `json:"device_id" bson:"device_id,omitempty"`
	OwnerID	             bson.ObjectId   `json:"owner_id" bson:"owner_id,omitempty"`
	OwnerName             string   `json:"owner_name" bson:"owner_name,omitempty"`
//	CompanyID      bson.ObjectId          `json:"company_id,omitempty" bson:"company_id,omitempty"`
//	CompanyName      string          `json:"company_name,omitempty" bson:"company_name,omitempty"`
	Timestamp            *time.Time      `json:"timestamp,omitempty" bson:"timestamp,omitempty"`
	Mark            int           `json:"mark,omitempty" bson:"mark,omitempty"`
//	UUID                 string          `json:"uuid" bson:"uuid,omitempty"`
//	SN	           string         `json:"sn,omitempty" bson:"sn,omitempty"`
//	Age                  int			`json:"age" bson:"age,omitempty"`
	
    NickName			string			 `json:"nickname" bson:"nickname,omitempty"`
	BirthDate           *time.Time		 `json:"birth_date,omitempty" bson:"birth_date,omitempty"`
	Gender				 int			 `json:"gender" bson:"gender,omitempty"`
	Weight				 int			 `json:"weight" bson:"weight,omitempty"`
	Height				 int			 `json:"height" bson:"height,omitempty"`
	Bust				 int			 `json:"bust" bson:"bust,omitempty"`			// xiongwei
	Cannon				 int			 `json:"cannon" bson:"cannon,omitempty"`		// guanwei
	CoatColor			 string			 `json:"coat_color" bson:"coat_color,omitempty"`
	Description          string          `json:"description" bson:"description,omitempty"`

	Images               []bson.ObjectId `json:"images" bson:"images"`

	Behavior			 int			 `json:"behavior" bson:"behavior"`
	BehaviorBegin		 *time.Time		 `json:"behavior_begin,omitempty" bson:"behavior_begin,omitempty"`
	Species				 int			 `json:"species" bson:"species,omitempty"`
}

// GetID is find a setting by device id
func (cattle Cattle) GetID() bson.ObjectId {
	return cattle.ID
}

func (cattle Cattle) GetDBRef() mgo.DBRef {
	return mgo.DBRef{
		Collection: CattleCollection,
		Id:         cattle.ID,
		Database:   DruidDB,
	}
}

// RemoveImages remove some image id
func (cattle Cattle) RemoveImages(db *mgo.Session, images []bson.ObjectId) error {
	c := db.DB(DruidDB).C(CattleCollection)
	if err := c.UpdateId(cattle.ID, bson.M{"$pull": bson.M{"images": bson.M{"$in": images}}}); err != nil {
		return err
	}
	return nil
}

func (cattle Cattle) Remove(db *mgo.Session) error {
	c := db.DB(DruidDB).C(CattleCollection)
	if err := c.RemoveId(cattle.ID); err != nil {
		return err
	}
	return nil
}

// RemoveImage a image id
func (cattle Cattle) RemoveImage(db *mgo.Session, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(CattleCollection)
	if err := c.UpdateId(cattle.ID, bson.M{"$pull": bson.M{"images": id}}); err != nil {
		return err
	}
	return nil
}

// InsertImage a image id
func (cattle Cattle) InsertImage(db *mgo.Session, image *Image) error {
	c := db.DB(DruidDB).C(CattleCollection)
	if err := c.Update(bson.M{"device_id": cattle.DeviceID}, bson.M{"$addToSet": bson.M{"images": image.ID}}); err != nil {
		return err
	}

	return nil
}

func AddOwnerToBiological(db *mgo.Session, user *User, ids []bson.ObjectId) error {
	c := db.DB(DruidDB).C(CattleCollection)

	if _, err := c.UpdateAll(
					bson.M{
						"device_id": bson.M{"$in": ids}}, 
					bson.M{
						"$set": bson.M{
							"owner_id": user.ID, 
							"owner_name": user.UserName,
						}}); err != nil {
		return ErrorConvert(err)
	}
	return nil
}


func (cattle *Cattle) UpdateByMark(db *mgo.Session) error {
	c := db.DB(DruidDB).C(CattleCollection)
	if err := c.Update(bson.M{"mark" : cattle.Mark},
					bson.M{"$set":
						bson.M{
							"behavior":	cattle.Behavior,
							"behavior_begin": cattle.BehaviorBegin,
							"species": cattle.Species,
						}}); err != nil {
		return err
	}

	return nil
}

func (cattle *Cattle) Update(db *mgo.Session) error {
	c := db.DB(DruidDB).C(CattleCollection)
	if err := c.Update(bson.M{"device_id" : cattle.DeviceID},
					bson.M{"$set":
						bson.M{
							"nickname": cattle.NickName,
							"birth_date": cattle.BirthDate,
							"gender": cattle.Gender,
							"height": cattle.Height,
							"weight": cattle.Weight,
							"bust": cattle.Bust,
							"cannon": cattle.Cannon,
							"coat_color": cattle.CoatColor,
							"description": cattle.Description,
							"species": cattle.Species,
							"updated_at": time.Now(),
						}}); err != nil {
		return err
	}

	return nil
}

func (cattle *Cattle)Create(db *mgo.Session) error {
	c := db.DB(DruidDB).C(CattleCollection)

	err := c.Insert(cattle)
	if err != nil {
		return err
	}

	return nil
}

func (cattle *Cattle) BindDevice(db *mgo.Session, device *Device) error {
	c := db.DB(DruidDB).C(CattleCollection)
	if err := c.UpdateId(cattle.ID,
		bson.M{"$set": bson.M{
			"device_id": device.ID,
//			"uuid": device.UUID,
//			"sn": device.SN,
//			"mark":  device.Mark,
		}}); err != nil {
		return ErrorConvert(err)
	}
	return nil
}


func (cattle *Cattle) RemoveDevice(db *mgo.Session) error {
	c := db.DB(DruidDB).C(CattleCollection)
	if err := c.UpdateId(cattle.ID,
		bson.M{"$unset": bson.M{
			"device_id": 1,
//			"uuid": 1,
//			"sn": 1,
//			"mark":  1,
		}}); err != nil {
		return ErrorConvert(err)
	}
	return nil
}

func RemoveCattleByOwnerID(db *mgo.Session, ownerID bson.ObjectId) error {
	c := db.DB(DruidDB).C(CattleCollection)
	if _, err := c.RemoveAll(bson.M{"owner_id": ownerID}); err != nil {
		return ErrorConvert(err)
	}
	return nil
}

func RemoveOwnerFromBiologicalByOwnerID(db *mgo.Session, ids []bson.ObjectId) error {
	c := db.DB(DruidDB).C(CattleCollection)

	if _, err := c.UpdateAll(
						bson.M{"device_id": bson.M{"$in": ids}},
						bson.M{
							"$unset": bson.M{
								"owner_id": 1, 
								"owner_name": 1, 
							}}); err != nil {
		return ErrorConvert(err)
	}
	return nil
}


func RemoveOwnerFromBiological(db *mgo.Session, ids []bson.ObjectId) error {
	c := db.DB(DruidDB).C(CattleCollection)

	if _, err := c.UpdateAll(
						bson.M{ "device_id": bson.M{ "$in": ids}}, 
						bson.M{
							"$unset": bson.M{
								"owner_id": 1, 
								"owner_name": 1, 
							}}); err != nil {
		return ErrorConvert(err)
	}
	return nil
}

func RemoveDevicesFromCattle(db *mgo.Session, ids []bson.ObjectId) error {
	c := db.DB(DruidDB).C(CattleCollection)
	if _, err := c.UpdateAll(bson.M{"device_id": bson.M{"$in": ids}},
		bson.M{"$unset": bson.M{
			"device_id": 1,
//			"uuid": 1,
//			"sn": 1,
//			"mark":  1,
		}}); err != nil {
		return ErrorConvert(err)
	}
	return nil
}

func CountCattleByFilter(db *mgo.Session, m bson.M) (int, error) {
	c := db.DB(DruidDB).C(CattleCollection)
	return MongoCount(c, m)
}

func ListCattleByFilter(db *mgo.Session, m bson.M, rp *RequestParameter) ([]*Cattle, error) {
	c := db.DB(DruidDB).C(CattleCollection)
	cattle := []*Cattle{}
	return cattle, MongoList(c, m, &cattle, rp, nil)
}

func ListCattleBySpecies(db *mgo.Session, species string) ([]*Cattle, error) {
	c := db.DB(DruidDB).C(CattleCollection)
	cattle := []*Cattle{}
	if err := c.Find(bson.M{"species": species}).All(&cattle); err != nil {
		return cattle, ErrorConvert(err)
	}
	return cattle, nil
}


func ListCattleByOwnerID(db *mgo.Session, id bson.ObjectId) ([]*Cattle, error) {
	c := db.DB(DruidDB).C(CattleCollection)
	cattle := []*Cattle{}
	if err := c.Find(bson.M{"owner_id": id}).All(&cattle); err != nil {
		return cattle, ErrorConvert(err)
	}
	return cattle, nil
}

func FindCattleByDeviceID(db *mgo.Session, id bson.ObjectId) (*Cattle, error) {
	c := db.DB(DruidDB).C(CattleCollection)
	cattle := &Cattle{}
	if err := c.Find(bson.M{"device_id": id}).One(cattle); err != nil {
		return nil, ErrorConvert(err)
	}
	return cattle, nil
}

func FindCattleByID(db *mgo.Session, id bson.ObjectId) (*Cattle, error) {
	c := db.DB(DruidDB).C(CattleCollection)
	cattle := &Cattle{}
	if err := c.FindId(id).One(cattle); err != nil {
		return nil, ErrorConvert(err)
	}
	return cattle, nil
}

func RemoveImageByImageID(db *mgo.Session, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(CattleCollection)
	if err := c.Update(bson.M{"images": id}, bson.M{"$pull": bson.M{"images": id}}); err != nil {
		return err
	}
	return nil
}


func ListBiologicalByFilter(db *mgo.Session, m bson.M, rp *RequestParameter, se bson.M)([]*Cattle,error) {
	c := db.DB(DruidDB).C(CattleCollection)
	cattles := []*Cattle{}
	return cattles, MongoList(c, m , &cattles, rp ,se)
}

func CountBiologicalByFilter(db *mgo.Session, m bson.M) (count int ,err error){
	c := db.DB(DruidDB).C(CattleCollection)
	return MongoCount(c,m)
}
