package models

import (
	//	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"time"
)

const (
	MaxRoomName	=	15
)

type Room struct {
	ID              bson.ObjectId `json:"id" bson:"_id,omitempty"`
	UpdatedAt       *time.Time    `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
	DeletedAt       *time.Time    `json:"deleted_at,omitempty" bson:"deleted_at,omitempty"`
	DeletedBy       bson.ObjectId `json:"deleted_by,omitempty" bson:"deleted_by,omitempty"`

	UserID       bson.ObjectId `json:"user_id,omitempty" bson:"user_id,omitempty"`

	Description     string        `json:"description,omitempty" bson:"description,omitempty"`

	RoomMark		int32			`json:"room_mark,omitempty" bson:"room_mark,omitempty"`
	RoomName		string			`json:"room_name,omitempty" bson:"room_name,omitempty"`

	DeviceCount			int			`json:"device_count" bson:"-"`
	Lock			*time.Time		`json:"-", bson:"lock,omitempty"`
}

func (room *Room)Create(db *mgo.Session) (error) {
	c := db.DB(DruidDB).C(RoomCollection)

	err := c.Insert(room)
	if err != nil {
		return err
	}
	return nil
}

func (room *Room)Update(db *mgo.Session) (error) {
	c := db.DB(DruidDB).C(RoomCollection)

	err := c.UpdateId(room.ID,
		bson.M{"$set": bson.M{
			"room_mark":		room.RoomMark,
			"room_name":		room.RoomName,
			"description":      room.Description,
			"updated_at":      time.Now()}})
	if err != nil {
		return err
	}
	return nil
}

func (room *Room)Delete(db *mgo.Session) (error) {
	c := db.DB(DruidDB).C(RoomCollection)

	err := c.RemoveId(room.ID)
	if err != nil {
		return ErrorConvert(err)
	}
	return nil
}

func (room *Room)CountDevice(db *mgo.Session) (count int, err error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	return MongoCount(c, bson.M{"room_id": room.ID})
}

func DeleteRoomByOwnerID(db *mgo.Session, ownerID bson.ObjectId) (error) {
	c := db.DB(DruidDB).C(RoomCollection)

	if _, err := c.RemoveAll(bson.M{"user_id": ownerID}); err != nil {
		return ErrorConvert(err)
	}
	return nil
}

func RoomLockTimeout(db *mgo.Session, id bson.ObjectId, u time.Time) error {
	c := db.DB(DruidDB).C(RoomCollection)
	return MongoLockTimeout(c, bson.M{"_id": id}, u)
}

func RoomLock(db *mgo.Session, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(RoomCollection)
	return MongoLock(c, bson.M{"_id": id})
}

func RoomUnLock(db *mgo.Session, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(RoomCollection)
	return MongoUnLock(c, bson.M{"_id": id})
}

func FindRoomByID(db *mgo.Session, id bson.ObjectId) (*Room, error) {
	c := db.DB(DruidDB).C(RoomCollection)
	room := Room{}
	if err := c.FindId(id).One(&room); err != nil {
		return nil, err
	}
	return &room, nil
}

func ListRoomByUserID(db *mgo.Session, id bson.ObjectId, rp *RequestParameter) ([]*Room, error) {
	c := db.DB(DruidDB).C(RoomCollection)
	room := []*Room{}
	return room, MongoList(c, bson.M{"user_id": id}, &room, rp, nil)
}


func CountRoomByUserID(db *mgo.Session, id bson.ObjectId) (int, error) {
	c := db.DB(DruidDB).C(RoomCollection)
	return MongoCount(c, bson.M{"user_id": id})
}





