package models

import (
	"druid/public"
)

const (
	ChannelElevation           = "elevation"
	ChannelGeocoding           = "geocoding"
	ChannelGeofence            = "geofence"
	ChannelJpush               = "jpush"
	ChannelDead                = "dead_check"
	ChannelBiological          = "biological"
	ChannelBiologicalODBA      = "biological_odba"
	ChannelBiologicalAlgorithm = "biological_algorithm"
	ChannelSignal              = "signal"
	ChannelDeviceSMS           = "device_sms"
	ChannelCheckUpload         = "check_upload"
	ChannelCheckSetting        = "check_setting"
)

const (
	MaxChannel = 10000
)

func GetTopic(topic string) string {
	return public.MQBaseCattle + "-" + topic
}

func GetChannel(channel string) string {
	return public.MQBaseCattle + "-" + channel
}

func GetTopicGPS() string {
	return public.MQBaseCattle + "-" + public.TopicGPS
}
func GetTopicLastGPS() string {
	return public.MQBaseCattle + "-" + public.TopicLastGPS
}
func GetTopicBehavior() string {
	return public.MQBaseCattle + "-" + public.TopicBehavior
}
func GetTopicBehavior2() string {
	return public.MQBaseCattle + "-" + public.TopicBehavior2
}
func GetTopicDevice() string {
	return public.MQBaseCattle + "-" + public.TopicDevice
}
func GetTopicMessage() string {
	return public.MQBaseCattle + "-" + public.TopicMessage
}
func GetTopicDeviceSMS() string {
	return public.MQBasePlatform + "-" + public.TopicDeviceSMS
}
func GetTopicStatus() string {
	return public.MQBaseCattle + "-" + public.TopicStatus
}

func GetChannelElevation() string {
	return public.MQBaseCattle + "-" + ChannelElevation
}
func GetChannelGeocoding() string {
	return public.MQBaseCattle + "-" + ChannelGeocoding
}
func GetChannelGeofence() string {
	return public.MQBaseCattle + "-" + ChannelGeofence
}
func GetChannelJpush() string {
	return public.MQBaseCattle + "-" + ChannelJpush
}
func GetChannelDead() string {
	return public.MQBaseCattle + "-" + ChannelDead
}
func GetChannelSignal() string {
	return public.MQBaseCattle + "-" + ChannelSignal
}
func GetChannelBiological() string {
	return public.MQBaseCattle + "-" + ChannelBiological
}
func GetChannelBiologicalODBA() string {
	return public.MQBaseCattle + "-" + ChannelBiologicalODBA
}
func GetChannelBiologicalAlgorithm() string {
	return public.MQBaseCattle + "-" + ChannelBiologicalAlgorithm
}
func GetChannelDeviceSMS() string {
	return public.MQBaseCattle + "-" + ChannelDeviceSMS
}
func GetChannelCheckUpload() string {
	return public.MQBaseCattle + "-" + ChannelCheckUpload
}
func GetChannelCheckSetting() string {
	return public.MQBaseCattle + "-" + ChannelCheckSetting
}
