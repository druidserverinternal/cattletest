package models

import (
	"math/rand"
	"time"
	"strings"

	"gopkg.in/mgo.v2/bson"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

var letterString = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
var letterInt = []rune("123456789")

func RandStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterString[rand.Intn(len(letterString))]
	}
	return string(b)
}

func RandIntRunes() int {
	r := letterInt[rand.Intn(len(letterInt))]
	return int(r - '0')
}

func GetContentTypeByName(fileName string) string {
	fn := strings.Split(fileName, ".")
	if len(fn) < 2 {
		return "application/octet-stream"
	}
	extName := fn[len(fn) - 1]

	switch extName {
	case "jpg", "jpeg":
		return "image/jpeg"
	case "png":
		return "image/png"
	default:
		return "application/octet-stream"
	}
}

func ConvertObjectIdToString(ids []bson.ObjectId) []string {
	ss := []string{}
	for _, id := range ids {
		ss = append(ss, id.Hex())
	}
	return ss
}

func StringSplit(str, s string) []string {
	f := func(c rune) bool {
		for _, v := range s {
			if c == v {
				return true
			}
		}
		return false
	}

	return strings.FieldsFunc(str, f)
}


