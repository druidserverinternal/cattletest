package models

import (
	"druid/public"

	"time"
	"encoding/json"
)

const (
	EtcdTimeout	=	5 * time.Second

	RequestFromBird		=		"bird"
	RequestFromCattle		=		"cattle"
)


func GetPushKey() (string) {
	return GetServiceKey("cattle", "SendAlert")
}

func GetServiceKey(_type, service string) (string) {
	return "/v1/" + _type + "/" + service
}

func GetSIMFromISP(imsi string) (*SIM, error) {
	key := ""
	t := public.CheckSIMType(imsi)
	switch t {
	case public.SIMTypeCmccTotal:
		key = GetServiceKey("platform", "GetSIMFromCMCCByIMSI")
	case public.SIMTypeCuccTotal:
		key = GetServiceKey("platform", "GetSIMFromCUCCByIMSI")
	case public.SIMTypeCtccTotal:
		return &SIM{ IMSI:imsi, Checked:SIMISPChecked, SIMType:t }, nil
	default:
		return nil, Error("IMSI is unknown!", imsi)
	}

	b, err := public.GetFromService(key, imsi)
	if err != nil {
		return nil, err
	}
	if b == nil {
		switch t {
		case public.SIMTypeCmccTotal:
			t = public.SIMTypeCmcc
		case public.SIMTypeCuccTotal:
			t = public.SIMTypeCucc
		}

		return &SIM{ IMSI:imsi, Checked:SIMISPChecked, SIMType:t}, nil
	}
	if len(b) == 0 {
		 return nil, Error("Result is empty.")
	}

	sim := &SIM{}
	if err := json.Unmarshal(b, sim); err != nil {
		return nil, err
	}
	return sim, nil
}

func SendMessageToPush(msg interface{}) ([]byte, error) {
	key := GetPushKey()

	b, err := public.PostToService(key, msg)
	if err != nil {
		return nil, err
	}
	
	return b, nil
}





