package models

import (
	//		"fmt"
	"gopkg.in/mgo.v2/bson"
	//"strings"
	"reflect"
	"time"
)

// Search is search object
type Search struct {
	ID             bson.ObjectId `json:"id,omitempty" search:"_id"`

	// gps
	DeviceID       bson.ObjectId `json:"device_id,omitempty" search:"device_id"`
	Timestamp      []*time.Time  `json:"timestamp,omitempty" search:"timestamp"`
	Longitude      []float64     `json:"longitude,omitempty" search:"longitude"`
	Latitude       []float64     `json:"latitude,omitempty" search:"latitude"`
	Altitude       []float64     `json:"altitude,omitempty" search:"altitude"`
	Temperature    []float64     `json:"temperature,omitempty" search:"temperature"`
	Humidity       []int32       `json:"humidity,omitempty" search:"humidity"`
	Light          []int32       `json:"light,omitempty" search:"light"`
	Pressure       []int32       `json:"pressure,omitempty" search:"pressure"`
	UsedStar       []int32       `json:"used_star,omitempty" search:"used_star"`
	Viewstar       []int32       `json:"view_star,omitempty" search:"view_star"`
	Dimension      []int32       `json:"dimension,omitempty" search:"dimension"`
	Speed          []float64     `json:"speed,omitempty" search:"speed"`
	Horizontal     []float64     `json:"horizontal,omitempty" search:"horizontal"`
	Vertical       []float64     `json:"vertical,omitempty" search:"vertical"`
	Course         []float64     `json:"course,omitempty" search:"course"`
	BatteryVoltage []float64     `json:"battery_voltage,omitempty" search:"battery_voltage"`
	SignalStrength []int32       `json:"signal_strength,omitempty" search:"signal_strength"`

	// device
	UUID            string  `json:"uuid,omitempty" search:"uuid"`
	FirmwareVersion []int32 `json:"firmware_version,omitempty" search:"firmware_version"`

	UpdatedAt       []*time.Time  `json:"updated_at,omitempty" search:"updated_at"`
	DeviceType      []int32       `json:"device_type,omitempty" search:"device_type"`
	HardwareVersion []int32       `json:"hardware_version,omitempty" search:"hardware_version"`
	CompanyID       bson.ObjectId `json:"company_id,omitempty" search:"company_id"`
	CompanyName     string        `json:"company_name,omitempty" search:"company_name"`
	Imsi            string        `json:"imsi,omitempty" search:"imsi"`
	SimNumber       string        `json:"sim_number,omitempty" search:"sim_number"`
	Mac             string        `json:"mac,omitempty" search:"mac"`
	Description     string        `json:"description,omitempty" search:"description"`
	Mark            []int           `json:"mark,omitempty" search:"mark"`
	BiologicalType  string        `json:"biological_type,omitempty" search:"biological_type"`
	RoomID     bson.ObjectId `json:"room_id" bson:"room_id,omitempty"`
	Survive    int           `json:"survive" bson:"survive"`

	// behavior
	SleepTime      []int32 `json:"sleep_time,omitempty" search:"sleep_time"`
	OtherTime      []int32 `json:"other_time,omitempty" search:"other_time"`
	ActivityTime   []int32 `json:"activity_time,omitempty" search:"activity_time"`
	FlyTime        []int32 `json:"fly_time,omitempty" search:"fly_time"`
	PeckTime       []int32 `json:"peck_time,omitempty" search:"peck_time"`
	CrawlTime      []int32 `json:"crawl_time,omitempty" search:"crawl_time"`
	RunTime        []int32 `json:"run_time,omitempty" search:"run_time"`
	TotalExpend    []int32 `json:"total_expend,omitempty" search:"total_expend"`
	SleepExpend    []int32 `json:"sleep_expend,omitempty" search:"sleep_expend"`
	OtherExpend    []int32 `json:"other_expend,omitempty" search:"other_expend"`
	ActivityExpend []int32 `json:"activity_expend,omitempty" search:"activity_expend"`
	FlyExpend      []int32 `json:"fly_expend,omitempty" search:"fly_expend"`
	PeckExpend     []int32 `json:"peck_expend,omitempty" search:"peck_expend"`
	CrawlExpend    []int32 `json:"crawl_expend,omitempty" search:"crawl_expend"`
	RunExpend      []int32 `json:"run_expend,omitempty" search:"run_expend"`

	// biological
	Species				 int			 `json:"species" bson:"species,omitempty"`

	// cellular 
	ErrorFlag		[]uint32	`json:"error_flag" bson:"error_flag"`
	TotalTime		[]int32	`json:"total_time" bson:"total_time"`
	HwtTime			[]int32	`json:"hwt_time" bson:"hwt_time"`
	SimTime			[]int32	`json:"sim_time" bson:"sim_time"`
	NumTime			[]int32	`json:"num_time" bson:"num_time"`
	RssiTime		[]int32	`json:"rssi_time" bson:"rssi_time"`
	RegisterTime	[]int32	`json:"register_time" bson:"register_time"`
	AttachTime		[]int32	`json:"attach_time" bson:"attach_time"`
	PdpTime			[]int32	`json:"pdp_time" bson:"pdp_time"`
	ConnectionTime	[]int32	`json:"connection_time" bson:"connection_time"`
	CommunicationTime []int32	`json:"communication_time" bson:"communication_time"`
	NetworkOperator	[]uint32	`json:"network_operator" bson:"network_operator"`
	BitErrorRate	[]int32	`json:"bit_error_rate" bson:"bit_error_rate"`
	RadioAccessTechnology			[]int32	`json:"radio_access_technology" bson:"radio_access_technology"`
	SmsTime			[]int32	`json:"sms_time" bson:"sms_time"`
	ExitFlag		[]int32	`json:"exit_flag" bson:"exit_flag"`
}

var typeRegistry = make(map[reflect.Type]func(interface{}) interface{})

func init() {
	var st []*time.Time
	typeRegistry[reflect.TypeOf(st)] = TimeConvert
	var si []int32
	typeRegistry[reflect.TypeOf(si)] = RangeConvert
	var sii []int
	typeRegistry[reflect.TypeOf(sii)] = IntRangeConvert
	var sui []uint32
	typeRegistry[reflect.TypeOf(sui)] = UintRangeConvert
	var o bson.ObjectId
	typeRegistry[reflect.TypeOf(o)] = ObjectIDConvert
	var s string
	typeRegistry[reflect.TypeOf(s)] = StringConvert
	var sf []float64
	typeRegistry[reflect.TypeOf(sf)] = Float64Convert
	var i int
	typeRegistry[reflect.TypeOf(i)] = IntConvert
}

// RunFuncByType run bson convert by item type
func RunFuncByType(t reflect.Type, param interface{}) interface{} {
	f := typeRegistry[t]
	if f == nil {
		return nil
	}
	return f(param)
}

// IntConvert return regex bson
func IntConvert(inter interface{}) interface{} {
	data := inter.(int)
	if data == 0 {
		return nil
	}
	return inter
}

// StringConvert return regex bson
func StringConvert(inter interface{}) interface{} {
	data := inter.(string)
	if data != "" {
		return bson.M{"$regex": &bson.RegEx{Pattern: data, Options: "i"}}
	}
	return nil
}

func UintRangeConvert(inter interface{}) interface{} {
	data := inter.([]uint32)
	switch len(data) {
	case 1:
		return data[0]
	case 2:
		return bson.M{"$gte": data[0], "$lte": data[1]}
	default:
		return nil
	}
}


func IntRangeConvert(inter interface{}) interface{} {
	data := inter.([]int)
	switch len(data) {
	case 1:
		return data[0]
	case 2:
		return bson.M{"$gte": data[0], "$lte": data[1]}
	default:
		return nil
	}
}



// Float64Convert return int bson
func Float64Convert(inter interface{}) interface{} {
	data := inter.([]float64)
	//	fmt.Println(data, len(data))
	switch len(data) {
	case 1:
		return data[0]
	case 2:
		return bson.M{"$gte": data[0], "$lte": data[1]}
	default:
		return nil
	}
}

// RangeConvert return int bson
func RangeConvert(inter interface{}) interface{} {
	data := inter.([]int32)
	switch len(data) {
	case 1:
		return data[0]
	case 2:
		return bson.M{"$gte": data[0], "$lte": data[1]}
	default:
		return nil
	}
}

// TimeConvert return time bson
func TimeConvert(inter interface{}) interface{} {
	data := inter.([]*time.Time)
	switch len(data) {
	case 1:
		return bson.M{"$gte": data[0], "$lt": time.Now()}
	case 2:
		return bson.M{"$gte": data[0], "$lte": data[1]}
	default:
		return nil
	}
}

// ObjectIDConvert check inter is objectid
func ObjectIDConvert(inter interface{}) interface{} {
	id := inter.(bson.ObjectId)
	if id.Hex() == "" {
		return nil
	}
	return inter
	/*
		ids := inter.([]bson.ObjectId)
		if len(ids) == 0 {
			return nil
		}
		return bson.M{"$in": ids}
	*/
}

// Bson convert search struct to bson.M
func (search *Search) Bson() bson.M {
	m := bson.M{}

	to := reflect.TypeOf(search).Elem()
	vo := reflect.ValueOf(search).Elem()
	for i := 0; i < to.NumField(); i++ {
		f := to.Field(i)
		v := RunFuncByType(f.Type, vo.Field(i).Interface())
		if v != nil {
			//			fmt.Println(f.Name, f.Tag.Get("search"))
			m[f.Tag.Get("search")] = v
		}
	}
	m["deleted_at"] = nil

	return m
}
