package models

import (
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type GeoPoint struct {
	Type        string    `json:"type" bson:"type"`
	Coordinates []float64 `json:"coordinates" bson:"coordinates"`
}

const (
	SrcFromDevice	=		0
	SrcFromSMS		=		1
)

// GPS is gps object with mongodb
type GPS struct {
	ID              bson.ObjectId `json:"id,omitempty" bson:"_id,omitempty"`
	DeviceID        bson.ObjectId `json:"device_id" bson:"device_id,omitempty"`
//	CompanyID       bson.ObjectId `json:"company_id,omitempty" bson:"company_id,omitempty"`
//	CompanyName     string        `json:"company_name,omitempty" bson:"company_name,omitempty"`
	UUID            string        `json:"uuid" bson:"uuid,omitempty"`
	FirmwareVersion int32         `json:"firmware_version" bson:"firmware_version"`
	UpdatedAt       *time.Time    `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
	DeletedAt       *time.Time    `json:"deleted_at,omitempty" bson:"deleted_at,omitempty"`
	Timestamp       *time.Time    `json:"timestamp,omitempty" bson:"timestamp,omitempty"`
	Mark            int           `json:"mark,omitempty" bson:"mark,omitempty"`
	SMS             int           `json:"sms,omitempty" bson:"sms,omitempty"`
//	SMSAt           *time.Time    `json:"sms_at,omitempty" bson:"sms_at,omitempty"`
	Owner           string        `json:"owner,omitempty" bson:"owner,omitempty"`

	Loc       *GeoPoint `json:"loc,omitempty" bson:"loc,omitempty"`
	Longitude float64   `json:"longitude,omitempty" bson:"longitude,omitempty"`
	Latitude  float64   `json:"latitude,omitempty" bson:"latitude,omitempty"`

	RelativeAltitude float64 `json:"relative_altitude" bson:"relative_altitude,omitempty"`
	GroundAltitude   float64 `json:"ground_altitude" bson:"ground_altitude,omitempty"`
	Altitude       float64 `json:"altitude" bson:"altitude,omitempty"`
	Temperature    float64 `json:"temperature" bson:"temperature,omitempty"`
	Humidity       int32   `json:"humidity" bson:"humidity,omitempty"`
	Light          int32   `json:"light" bson:"light,omitempty"`
	Pressure       int32   `json:"pressure" bson:"pressure,omitempty"`
	UsedStar       int32   `json:"used_star" bson:"used_star,omitempty"`
	Viewstar       int32   `json:"view_star" bson:"view_star,omitempty"`
	Dimension      int32   `json:"dimension" bson:"dimension,omitempty"`
	Speed          float64 `json:"speed" bson:"speed,omitempty"`
	Horizontal     float64 `json:"horizontal" bson:"horizontal,omitempty"`
	Vertical       float64 `json:"vertical" bson:"vertical,omitempty"`
	Course         float64 `json:"course" bson:"course,omitempty"`
	BatteryVoltage float64 `json:"battery_voltage" bson:"battery_voltage,omitempty"`
	SignalStrength int32   `json:"signal_strength" bson:"signal_strength,omitempty"`
	PointLocation	int		`json:"point_location" bson:"point_location,omitempty"`
	FixTime			int32	`json:"fix_time" bson:"fix_time,omitempty" csv:"-" excel:"Time for Positioning"`
}

// Device struct is device object with mongodb
type DeviceGPSCount struct {
	ID        bson.ObjectId `json:"id" bson:"_id,omitempty"`
	UpdatedAt *time.Time    `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
	Count     int           `json:"count" bson:"count"`
}

type GPSSlice []*GPS

func (slice GPSSlice) Len() int {
	return len(slice)
}

func (slice GPSSlice) Less(i, j int) bool {
	return slice[i].Timestamp.Before(*(slice[j].Timestamp))
}

func (slice GPSSlice) Swap(i, j int) {
	slice[i], slice[j] = slice[j], slice[i]
}

func NewPoint(coordinate []float64) *GeoPoint {
	return &GeoPoint{"Point", coordinate}
}

func (gps *GPS) UploadAPP(db *mgo.Session) error {
	c := db.DB(DruidDB).C(GPSCollection)
	if _, err := c.Upsert(bson.M{"device_id": gps.DeviceID, "timestamp": gps.Timestamp, "sms": SrcFromSMS}, bson.M{"$set": gps}); err != nil {
		return err
	}
	return nil
}


// Upload a gps
func (gps *GPS) Upload(db *mgo.Session) error {
	c := db.DB(DruidDB).C(GPSCollection)
	if err := c.Insert(gps); err != nil {
		return err
	}
	return nil
}

// UpsertByTimestampSMS a gps
func (gps *GPS) UpsertByTimestampSMS(db *mgo.Session) error {
	c := db.DB(DruidDB).C(GPSCollection)
	if _, err := c.Upsert(
		bson.M{"device_id": gps.DeviceID, "timestamp": gps.Timestamp, "sms": gps.SMS},
		bson.M{"$set": gps}); err != nil {
		return err
	}
	return nil
}


// UpsertByTimestamp a gps
func (gps *GPS) UpsertByTimestamp(db *mgo.Session) error {
	c := db.DB(DruidDB).C(GPSCollection)
	if _, err := c.Upsert(
		bson.M{"device_id": gps.DeviceID, "timestamp": gps.Timestamp},
		bson.M{"$set": gps}); err != nil {
		return err
	}
	return nil
}

func FindGPSByTimestamp(db *mgo.Session, deviceID bson.ObjectId, t *time.Time, se bson.M) (*GPS, error) {
	c := db.DB(DruidDB).C(GPSCollection)
	gps := &GPS{}

	if err := MongoFind(c, bson.M{"device_id": deviceID, "timestamp": t}, gps, "", se); err != nil {
		return nil, ErrorConvert(err)
	}
	return gps, nil
}

// UpdateElevation update elevation with gps
func (gps *GPS) UpdateElevation(db *mgo.Session) error {
	c := db.DB(DruidDB).C(GPSCollection)
	if _, err := c.UpdateAll(
		bson.M{"device_id": gps.DeviceID, "timestamp": gps.Timestamp},
		bson.M{"$set": bson.M{
			"relative_altitude": gps.RelativeAltitude,
			"ground_altitude":   gps.GroundAltitude,
		}}); err != nil {
		return err
	}
	return nil
}

// UpdateElevationByID with gps
func (gps *GPS) UpdateElevationByID(db *mgo.Session) error {
	c := db.DB(DruidDB).C(GPSCollection)
	if err := c.UpdateId(gps.ID,
		bson.M{"$set": bson.M{
			"relative_altitude": gps.RelativeAltitude,
			"ground_altitude":   gps.GroundAltitude,
		}}); err != nil {
		return err
	}
	return nil
}

func (gps *GPS) VerifyTimestamp() bool {
	if gps.Timestamp.Before(TimestampBegin) || gps.Timestamp.After(GetTimestampEnd()) {
		return false
	}
	return true
}

// GetGPSQueue device hardware and firmware version
func GetGPSQueue(db *mgo.Session) *mgo.Bulk {
	c := db.DB(DruidDB).C(GPSCollection)
	return c.Bulk()
}

/*
// UpdateManyGPSCompany device hardware and firmware version
func UpdateManyGPSCompany(db *mgo.Session, ids []bson.ObjectId, companyID bson.ObjectId, companyName string) error {
	c := db.DB(DruidDB).C(GPSCollection)
	if _, err := c.UpdateAll(
		bson.M{"device_id": bson.M{
			"$in": ids}},
		bson.M{"$set": bson.M{
			"company_id":   companyID,
			"company_name": companyName}}); err != nil {
		return err
	}
	return nil
}
*/

func ClearManyGPSCompany(db *mgo.Session, ids []bson.ObjectId) error {
	c := db.DB(DruidDB).C(GPSCollection)
	if _, err := c.UpdateAll(
		bson.M{"device_id": bson.M{
			"$in": ids}},
		bson.M{"$unset": bson.M{
			"company_id":   1,
			"company_name": 1}}); err != nil {
		return err
	}
	return nil
}

// UpdateGPSFixTime update gps fix time
func UpdateGPSFixTime(db *mgo.Session, id bson.ObjectId, ft int32) error {
	c := db.DB(DruidDB).C(GPSCollection)
	if err := c.UpdateId(id, bson.M{"$set": bson.M{"fix_time": ft}}); err != nil {
		return err
	}
	return nil
}


// UpdateGPSMark device hardware and firmware version
func UpdateGPSMark(db *mgo.Session, id bson.ObjectId, mark int) error {
	c := db.DB(DruidDB).C(GPSCollection)
	if _, err := c.UpdateAll(
		bson.M{"device_id": id},
		bson.M{"$set": bson.M{
			"mark": mark}}); err != nil {
		return err
	}
	return nil
}

/*
// UpdateGPSCompany device hardware and firmware version
func UpdateGPSCompany(db *mgo.Session, id bson.ObjectId, companyID bson.ObjectId, companyName string) error {
	c := db.DB(DruidDB).C(GPSCollection)
	if _, err := c.UpdateAll(
		bson.M{"device_id": id},
		bson.M{"$set": bson.M{
			"company_id":   companyID,
			"company_name": companyName}}); err != nil {
		return err
	}
	return nil
}
*/

// FindLatestGPSByFilter find a latest gps record
func FindLatestGPSByFilter(db *mgo.Session, m bson.M) (*GPS, error) {
	c := db.DB(DruidDB).C(GPSCollection)
	gps := GPS{}
	if err := MongoFind(c, m, &gps, "-timestamp", nil); err != nil {
		return nil, ErrorConvert(err)
	}
	return &gps, nil
}

func FindDeviceLatestValidGPS(db *mgo.Session, deviceID bson.ObjectId) (*GPS, error) {
	c := db.DB(DruidDB).C(GPSCollection)
	gps := GPS{}
	if err := MongoFind(c, bson.M{"device_id": deviceID, "longitude": bson.M{"$lt": 200}}, &gps, "-timestamp", nil); err != nil {
		return nil, ErrorConvert(err)
	}
	return &gps, nil
}

// ListGPSByFilter all of company devices
func ListGPSByFilter(db *mgo.Session, m interface{}, rp *RequestParameter, se bson.M) ([]*GPS, error) {
	c := db.DB(DruidDB).C(GPSCollection)
	result := []*GPS{}
	return result, MongoList(c, m, &result, rp, se)
}

// ListGPSByFilterLimit all of company devices
func ListGPSByFilterLimit(db *mgo.Session, m interface{}, rp *RequestParameter, se bson.M) ([]*GPS, error) {
	c := db.DB(DruidDB).C(GPSCollection)
	result := []*GPS{}
	return result, MongoListLimit(c, m, &result, rp, se)
}

// CountGPSByFilter get count device of company
func CountGPSByFilter(db *mgo.Session, m interface{}) (count int, err error) {
	c := db.DB(DruidDB).C(GPSCollection)
	return MongoCount(c, m)
}

/*
// ListValidGPSByCompanyID list undeleted only
func ListValidGPSByCompanyID(db *mgo.Session, companyID bson.ObjectId, rp *RequestParameter, se bson.M) ([]*GPS, error) {
	c := db.DB(DruidDB).C(GPSCollection)
	gps := []*GPS{}
	return gps, MongoList(c, bson.M{"company_id": companyID, "deleted_at": nil}, &gps, rp, se)
}

// GetGPSValidCountByCompanyID get count
func GetGPSValidCountByCompanyID(db *mgo.Session, companyID bson.ObjectId) (count int, err error) {
	c := db.DB(DruidDB).C(GPSCollection)
	return MongoCount(c, bson.M{"company_id": companyID, "deleted_at": nil})
}
*/

// ListGPS all of gps by gps, list undeleted only
func ListGPS(db *mgo.Session, rp *RequestParameter, se bson.M) ([]*GPS, error) {
	c := db.DB(DruidDB).C(GPSCollection)
	gps := []*GPS{}
	return gps, MongoList(c, nil, &gps, rp, se)
}

// ListValidGPS all of gps by gps, list undeleted only
func ListValidGPS(db *mgo.Session, rp *RequestParameter, se bson.M) ([]*GPS, error) {
	c := db.DB(DruidDB).C(GPSCollection)
	gps := []*GPS{}
	return gps, MongoList(c, bson.M{"deleted_at": nil}, &gps, rp, se)
}

// ListGPSByDeviceID list all of gps by device id, this could be requery deleted device
func ListGPSByDeviceID(db *mgo.Session, id bson.ObjectId, rp *RequestParameter, se bson.M) ([]*GPS, error) {
	c := db.DB(DruidDB).C(GPSCollection)
	gps := []*GPS{}
	return gps, MongoList(c, bson.M{"device_id": id}, &gps, rp, se)
}

// ListGPSByUUID list all of gps by gps
func ListGPSByUUID(db *mgo.Session, uuid string, rp *RequestParameter, se bson.M) ([]*GPS, error) {
	c := db.DB(DruidDB).C(GPSCollection)
	gps := []*GPS{}
	return gps, MongoList(c, bson.M{"uuid": uuid}, &gps, rp, se)
}

// FindLatestGPSByTimestamp find a behivior by timestamp
func FindLatestGPSByTimestamp(db *mgo.Session, id bson.ObjectId) (*GPS, error) {
	c := db.DB(DruidDB).C(GPSCollection)
	gps := GPS{}
	if err := MongoFind(c, bson.M{"device_id": id}, &gps, "-timestamp", nil); err != nil {
		return nil, ErrorConvert(err)
	}
	return &gps, nil
}

// CountGPSByDeviceID list all of gps by device
func CountGPSByDeviceID(db *mgo.Session, id bson.ObjectId)  (int, error) {
	c := db.DB(DruidDB).C(GPSCollection)
	return MongoCount(c, bson.M{"device_id": id})
}


// SearchGPS search gps , filter by m and rp
func SearchGPS(db *mgo.Session, m bson.M, rp *RequestParameter, se bson.M) ([]*GPS, error) {
	c := db.DB(DruidDB).C(GPSCollection)
	gps := []*GPS{}
	return gps, MongoList(c, m, &gps, rp, se)
}

// SearchGPSCount search gps count
func SearchGPSCount(db *mgo.Session, m interface{}) (count int, err error) {
	c := db.DB(DruidDB).C(GPSCollection)
	return MongoCount(c, m)
}

// GetGPSCount get count
func GetGPSCount(db *mgo.Session) (count int, err error) {
	c := db.DB(DruidDB).C(GPSCollection)
	return CollectionCount(c)
}

// GetGPSValidCount get count
func GetGPSValidCount(db *mgo.Session) (count int, err error) {
	c := db.DB(DruidDB).C(GPSCollection)
	return MongoCount(c, bson.M{"deleted_at": nil})
}

// GPSFilterCount get count
func GPSFilterCount(db *mgo.Session, m interface{}) (count int, err error) {
	c := db.DB(DruidDB).C(GPSCollection)
	return MongoCount(c, m)
}

// GetGPSCountByDeviceID get count gps of one device
func GetGPSCountByDeviceID(db *mgo.Session, id bson.ObjectId) (count int, err error) {
	c := db.DB(DruidDB).C(GPSCollection)
	return MongoCount(c, bson.M{"device_id": id})
}

// DeleteGPSByDeviceID delete all of device gps
func DeleteGPSByDeviceID(db *mgo.Session, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(GPSCollection)
	return MongoDelete(c, bson.M{"device_id": id})
}

// DestroyGPSByDeviceID delete all of device gps
func DestroyGPSByDeviceID(db *mgo.Session, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(GPSCollection)
	return MongoDestroy(c, bson.M{"device_id": id})
}

// TotalGPS count version count
func TotalGPS(db *mgo.Session) ([]*Device, error) {
	c := db.DB(DruidDB).C(GPSCollection)
	d := []*Device{}
	pipe := c.Pipe(
		[]bson.M{bson.M{
			"$group": bson.M{
				"_id":        "$device_id",
				"total_gps":    bson.M{"$sum": 1},
		}}})

	if err := pipe.All(&d); err != nil {
		return d, ErrorConvert(err)
	}
	return d, nil
}


// GPSCountByAll statistics firmware version count
func GPSCountByAll(db *mgo.Session) ([]*DeviceGPSCount, error) {
	c := db.DB(DruidDB).C(GPSCollection)
	d := []*DeviceGPSCount{}
	pipe := c.Pipe([]bson.D{
		bson.D{{
			Name:  "$sort",
			Value: bson.M{"updated_at": -1}}},
		bson.D{{
			Name: "$group",
			Value: bson.M{
				"_id":        "$device_id",
				"count":      bson.M{"$sum": 1},
				"updated_at": bson.M{"$first": "$updated_at"}}}},
		bson.D{{
			Name:  "$sort",
			Value: bson.M{"count": -1}}}})

	// aggregate([{$sort: {updated_at: -1}}, {$group:{_id:"$device_id", count: {$sum:1}, update: {$first: "$updated_at"}}}])
	if err := pipe.All(&d); err != nil {
		return d, ErrorConvert(err)
	}
	return d, nil
}

func GPSFindAndModify(db *mgo.Session, m interface{}, change *mgo.Change) error {
	c := db.DB(DruidDB).C(GPSCollection)

	if _, err := c.Find(m).Apply(*change, nil); err != nil {
		return err
	}
	return nil
}

// GPSCountByDevices statistics firmware version count
func GPSCountByDevices(db *mgo.Session, ids []bson.ObjectId) ([]*DeviceGPSCount, error) {
	c := db.DB(DruidDB).C(GPSCollection)
	// ([{$match:{device_id:{$in: [ObjectId("582423182326efb5de18820c"), ObjectId("5824236a2326efb5de188210")]}}},{$group:{_id:"$device_id", count:{$sum:1}}}]);
	d := []*DeviceGPSCount{}
	pipe := c.Pipe([]bson.M{
		bson.M{
			"$match": bson.M{
				"device_id": bson.M{"$in": ids}}},
		bson.M{
			"$group": bson.M{
				"_id":   "$device_id",
				"count": bson.M{"$sum": 1}}}})

	if err := pipe.All(&d); err != nil {
		return d, ErrorConvert(err)
	}
	return d, nil
}

func RecoverGPSByDeviceID(db *mgo.Session, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(GPSCollection)
	return MongoRecover(c, bson.M{"device_id": id})
}

/*
func IdleGPSByCompanyID(db *mgo.Session, company_id bson.ObjectId) error {
	c := db.DB(DruidDB).C(GPSCollection)
	if _, err := c.UpdateAll(bson.M{"company_id": company_id}, bson.M{"$unset": bson.M{"company_id": 1, "company_name": 1}}); err != nil {
		return ErrorConvert(err)
	}
	return nil
}
*/

// DeleteGPSByID delete all of device gps
func DeleteGPSByID(db *mgo.Session, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(GPSCollection)
	return MongoDelete(c, bson.M{"_id": id})
}

/*
func FindDeviceLatestValidGPS(db *mgo.Session, deviceID bson.ObjectId) (*GPS, error) {
	c := db.DB(DruidDB).C(GPSCollection)
	gps := GPS{}
	if err := c.Find(bson.M{"device_id": deviceID, "longitude": bson.M{"$lt": 200}}).Sort("-timestamp").Limit(1).One(&gps); err != nil {
		return nil, ErrorConvert(err)
	}
	return &gps, nil
}

*/

