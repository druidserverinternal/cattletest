package models

import (
	"gopkg.in/mgo.v2"
)

var mongoDB *mgo.Session

func ConnectMongoDB(dataURL string) (error) {
    db, err := mgo.Dial(dataURL)
    if err != nil {
        return err
    }
    db.SetMode(mgo.Monotonic, true)
    mongoDB = db
    return nil
}

func GetMongoDB() *mgo.Session {
    if mongoDB == nil {
        return nil
    }
    d := mongoDB.Copy()
    d.SetMode(mgo.Monotonic, true)
    return d
}

