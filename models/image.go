package models


import (
	"image/png"
	"image/jpeg"
	"bytes"
	"image"
	"strings"
	"net/http"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"github.com/nfnt/resize"
//	_ "image/jpeg"
)

type Image struct {
	ID   bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Name string      `json:"name" bson:"name"`
	Size int         `json:"size" bson:"size"`
	MD5  string      `json:"md5" bson:"-"`
	Data []byte      `json:"-" bson:"-"`
	UploadBy        bson.ObjectId `json:"upload_by" bson:"upload_by,omitempty"`
	OwnerID			bson.ObjectId `json:"owner_id" bson:"owner_id,omitempty"`
//	CompanyID       bson.ObjectId `json:"company_id,omitempty" bson:"company_id,omitempty"`
//	CompanyName     string        `json:"company_name,omitempty" bson:"company_name,omitempty"`
	Point           bson.ObjectId `json:"point,omitempty" bson:"point,omitempty"`
}

func FindImageByID(db *mgo.Session, id bson.ObjectId) (*Image, error) {
	c := db.DB(DruidDB).C(ImageCollection)
	image := Image{}
	if err := c.FindId(id).One(&image); err != nil {
		return nil, ErrorConvert(err)
	}
	return &image, nil
}

func DeleteImageByIDs(db *mgo.Session, ids []bson.ObjectId) error {
	c := db.DB(DruidDB).C(ImageCollection)
	images := []*Image{}
	if err := c.Find(bson.M{"_id": bson.M{"$in": ids}}).All(&images); ErrorConvert(err) != nil {
		return err
	}

	for _, image := range images {
		if err := image.Delete(db); ErrorConvert(err) != nil {
			return err
		}
	}

	return nil
}

func (image *Image)Delete(db *mgo.Session) error {
	c := db.DB(DruidDB).C(ImageCollection)
	
	if image.Point != "" {
		if err := DeleteFileByID(db, image.Point); err != nil {
			return Error("Delete file failed.", err.Error())
		}
	}

	if err := c.RemoveId(image.ID); err != nil {
		return err
	}

	return nil
}

// Upload image to server
func (image *Image) Upload(db *mgo.Session) error {
	c := db.DB(DruidDB).C(ImageCollection)

	if err := c.Insert(image); err != nil {
		DeleteFileByID(db, image.Point)
		return err
	}
	return nil
}


func (image *Image) UploadImage(db *mgo.Session) error {
	file := &File{
		Data	:	image.Data,
		Size	:	image.Size,
		Name	:	image.Name,
	}
	if err := file.Upload(db); err != nil {
		return Error("Upload image to db failed.", err)
	}
	image.Point = file.ID.(bson.ObjectId)
	image.ID = bson.NewObjectId()
	image.MD5 = file.MD5 

	if err := image.Upload(db); err != nil {
		DeleteFileByID(db, file.ID)
		return Error("Upload image struct to db failed.", err)
	}

	return nil
}

func EncodeStreamToJPEG(m *image.Image,  quality int) []byte {
	var buf bytes.Buffer
	op := &jpeg.Options{}
	if quality > 0 {
		op.Quality = quality
	} else {
		op = nil
	}
	if err := jpeg.Encode(&buf, *m, op); err != nil {
		return nil
	}
	return buf.Bytes()
}

func DecodeJPEGToStream(data []byte) (*image.Image, error) {
	buf := bytes.NewReader(data)
	m, err := jpeg.Decode(buf)
	if err != nil {
		return nil, err
	}
	return &m, nil
}

func DecodePNGToStream(data []byte) (*image.Image, error) {
	buf := bytes.NewReader(data)
	m, err := png.Decode(buf)
	if err != nil {
		return nil, err
	}
	return &m, nil
}

func ConvertImageToThumbnail(data []byte) ([]byte, error) {
	t := http.DetectContentType(data)
	fn := strings.Split(t, "/")
	if len(fn) != 2 {
		return nil, Error("It's not a image!")
	}

	var stream *image.Image
	var err error
	switch fn[1] {
	case "jpeg":
		stream, err = DecodeJPEGToStream(data)
	case "png":
		stream, err = DecodePNGToStream(data)
	default:
		return nil, Error("It's not a valid image!")
	}
	if err != nil {
		return nil, Error("Decode image to stream failed.", err)
	}

	dst := resize.Thumbnail(100, 100, *stream, resize.Lanczos3)

	img := EncodeStreamToJPEG(&dst, 0)
	if img == nil {
		return nil, Error("Encode stream to image failed!")
	}

	return img, nil
}


func ConvertImageToJpeg(data []byte, quality int) ([]byte, error) {
	t := http.DetectContentType(data)
	fn := strings.Split(t, "/")
	if len(fn) != 2 {
		return nil, Error("It's not a image!")
	}

	var stream *image.Image
	var err error
	switch fn[1] {
	case "jpeg":
		stream, err = DecodeJPEGToStream(data)
	case "png":
		stream, err = DecodePNGToStream(data)
	default:
		return nil, Error("It's not a valid image!")
	}
	if err != nil {
		return nil, Error("Decode image to stream failed.", err)
	}

	img := EncodeStreamToJPEG(stream, quality)
	if img == nil {
		return nil, Error("Encode stream to image failed!")
	}

	return img, nil
}









