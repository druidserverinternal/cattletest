package models

import (
	"druid/public"
	"github.com/spf13/viper"

	log "github.com/sirupsen/logrus"
)

const (
	PlatformTypeName	=	"cattle"
)

func DelService(service string, path string) error {
	if service == "" {
		return Error("Service is invalid.")
	}

	address := viper.GetString("address")
	if address[0] == ':' {
		address = "127.0.0.1" + address
	}

//	key := "/" + release + "/v1/cattle/" + service + "/" + address

	value := "http://" + address + path
	log.Info("Register service:", service, " path:", value)

	return public.UnRegisterService(PlatformTypeName, service, value)
}


func RegService(service string, path string) error {
	if service == "" {
		return Error("Service is invalid.")
	}

	address := viper.GetString("address")
	if address[0] == ':' {
		address = "127.0.0.1" + address
	}

//	key := "/" + release + "/v1/cattle/" + service + "/" + address

	value := "http://" + address + path
	log.Info("Register service:", service, " path:", value)

	return public.RegisterService(PlatformTypeName, service, value)
}

func RegSocket(service string) error {
	if service == "" {
		return Error("Service is invalid.")
	}

	value := viper.GetString("service.address")
	if value == "" {
		return Error("Service address is invalid.")
	}
	if value[0] == ':' {
		value = "127.0.0.1" + value
	}

//	key := "/v1/black/" + service + "/" + address

	log.Info("Register service:", service, " path:", value)

	return public.RegisterService(PlatformTypeName, service, value)
}

func DelSocket(service string) error {
	if service == "" {
		return Error("Service is invalid.")
	}

	value := viper.GetString("service.address")
	if value == "" {
		return Error("Service address is invalid.")
	}
	if value[0] == ':' {
		value = "127.0.0.1" + value
	}

//	key := "/v1/black/" + service + "/" + address

	log.Info("UnRegister service:", service, " path:", value)

	return public.UnRegisterService(PlatformTypeName, service, value)
}




