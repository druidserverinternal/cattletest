package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

//	"fmt"
	"strconv"
	"time"
)

const (
	TypeSystem		= 1
	TypeUser		= 2
	TypeDevice		= 3
	TypeKeep		= 4
	TypeBiological	= 5

	TypeArea		= 6
)

const (
	MsgTypeBiologicalNormal	= 0
	MsgTypeBiologicalLow	= 1
	MsgTypeBiologicalHigh	= 2
	MsgTypeBiologicalStatic	= 3
)

const (
	MsgTypeDeviceNormal	= 0
	MsgTypeDeviceSingalWeak	= 1
)

const (
	LevelInfo		=	0
	LevelWarning	=	1
	LevelError		=	2
)

const (
	MsgTypeString = iota
	MsgTypeProto
	MsgTypeJson
)

type Message struct {
	ID    bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Dst   bson.ObjectId `json:"dst,omitempty" bson:"dst"`
	Dsts   []bson.ObjectId `json:"dsts,omitempty" bson:"-"`
	Type  int           `json:"type" bson:"type,omitempty"`
	Level int           `json:"level" bson:"level,omitempty"`

	Target    []bson.ObjectId `json:"target" bson:"target,omitempty"`
	TargetStr []string        `json:"target_str" bson:"target_str,omitempty"`

	Title   string      `json:"title,omitempty" bson:"title,omitempty"`
	Msg     string	`json:"msg" bson:"msg,omitempty"`
	MsgCN     string	`json:"msg_cn" bson:"msg_cn,omitempty"`
	MsgType int         `json:"msg_type" bson:"msg_type,omitempty"`

	Src       bson.ObjectId `json:"src,omitempty" bson:"src,omitempty"`
	SrcName   string        `json:"src_name,omitempty" bson:"src_name,omitempty"`
	Timestamp time.Time     `json:"timestamp" bson:"timestamp"`

	ReadedAt *time.Time `json:"readed_at,omitempty" bson:"readed_at,omitempty"`
}

/*
func GetMessageQueue(db *mgo.Session) *mgo.Bulk {
	c := db.DB(DruidDB).C(MessageCollection)
	return c.Bulk()
}
*/

func (msg *Message) Save(db *mgo.Session) error {
	c := db.DB(DruidDB).C(MessageCollection)

	for _, id := range msg.Dsts {
		msg.Dst = id
		if err := c.Insert(msg); err != nil {
			return err
		}
	}

	return nil
}

func (msg *Message)GetDsts() []string {
	ss := []string{}
	for _, id := range msg.Dsts {
		ss = append(ss, id.Hex())
	}
	return ss
}

/*
func (msg *Message) SendToUsers(db *mgo.Session, conn *net.Conn, users []*User) {
	msg.Dsts = users

	msg.SendTo(db, conn)
}

func (msg *Message) SendTo(db *mgo.Session, conn *net.Conn) error {
	c := db.DB(DruidDB).C(MessageCollection)
	err := c.Insert(msg)
	if err != nil {
		return err
	}
	if conn == nil {
		return errors.New("Push connection is nil.")
	}

	enc := gob.NewEncoder(*conn)
	return enc.Encode(msg)
}

*/
type MessageMany struct {
	IDs []bson.ObjectId `json:"id" bson:"_id,omitempty"`
	//	Read		*time.Time
}

func (msg *MessageMany) UpdateRead(db *mgo.Session) error {
	c := db.DB(DruidDB).C(MessageCollection)

	if _, err := c.UpdateAll(
		bson.M{"_id": bson.M{
			"$in": msg.IDs}},
		bson.M{"$set": bson.M{
			"readed_at": time.Now()}}); err != nil {
		return err
	}

	return nil
}

func (msg *MessageMany) Delete(db *mgo.Session) error {
	c := db.DB(DruidDB).C(MessageCollection)

	if _, err := c.RemoveAll(bson.M{"_id": bson.M{"$in": msg.IDs}}); err != nil {
		return err
	}

	return nil
}

func NewMessageDituAreaIn(device *Device, area *DituArea, users []bson.ObjectId, ps *PushSetting) *Message {
	msg := &Message{
		Type:      TypeArea,
		Level:     LevelWarning,
		Src:       device.ID,
		SrcName:   strconv.Itoa(device.Mark),
		Dsts:		users,
		MsgType:	DituAreaMsgTypeIn,
		Target:    []bson.ObjectId{area.ID},
		TargetStr: []string{area.AreaName},
		Timestamp: time.Now(),
		Msg:       ps.GetMsgEN(device.Mark, area.AreaName),
		MsgCN:	   ps.GetMsgCN(device.Mark, area.AreaName),	
	}

	return msg
}

func NewMessageDituAreaOut(device *Device, area *DituArea, users []bson.ObjectId, ps *PushSetting) *Message {
	msg := &Message{
		Type:      TypeArea,
		Level:     LevelWarning,
		Src:       device.ID,
		SrcName:   strconv.Itoa(device.Mark),
		Dsts:		users,
		MsgType:	DituAreaMsgTypeOut,
		Target:    []bson.ObjectId{area.ID},
		TargetStr: []string{area.AreaName},
		Timestamp: time.Now(),
		Msg:       ps.GetMsgEN(device.Mark, area.AreaName),
		MsgCN:	   ps.GetMsgCN(device.Mark,area.AreaName),	
	}

	return msg
}

func NewMessageBiologicalDead(device *Device, users []bson.ObjectId, ps *PushSetting) *Message {
	msg := &Message{
		Type:      TypeBiological,
		Level:     LevelError,
		Src:       device.ID,
		SrcName:   strconv.Itoa(device.Mark),
		Dsts:      users,
		Timestamp: time.Now(),
		Msg:       ps.GetMsgEN(device.Mark),
		MsgCN:     ps.GetMsgCN(device.Mark),
	}

	return msg
}

func NewMessageBiological(device *Device, users []bson.ObjectId, ps *PushSetting) *Message {
	msg := &Message{
		Type:      TypeBiological,
		Level:     LevelWarning,
		Src:       device.ID,
		SrcName:   strconv.Itoa(device.Mark),
		Dsts:		users,
		Target:    []bson.ObjectId{device.ID},
		TargetStr: []string{device.NickName},
		Timestamp: time.Now(),
		Msg:       ps.GetMsgEN(device.Mark),
		MsgCN:	   ps.GetMsgCN(device.Mark),	
	}

	switch ps.Class {
	case PushClassBiologicalHigh:
		msg.MsgType = MsgTypeBiologicalHigh
	case PushClassBiologicalLow:
		msg.MsgType = MsgTypeBiologicalLow
	default:
		msg.MsgType = MsgTypeBiologicalNormal
	}

	return msg
}

func NewMessageExport(user *User, ds []*Device, users []bson.ObjectId, ps *PushSetting) *Message {
	tar := []bson.ObjectId{}
	tars := []string{}
	for _, d := range ds {
		tar = append(tar, d.ID)
		tars = append(tars, strconv.Itoa(d.Mark))
	}
	msg := &Message{
		Type:      TypeUser,
		Level:     LevelWarning,
		Src:       user.ID,
		SrcName:   user.UserName,
		Dsts:		users,
		Target:    tar,
		TargetStr: tars,
		Timestamp: time.Now(),
		Msg:       ps.GetMsgEN(user.UserName, tars),
		MsgCN:	   ps.GetMsgCN(user.UserName, tars),	
	}

	return msg
}

/*
func NewMessageSetting(user *User) *Message {
	msg := &Message{
		Type:      TypeDevice,
		Level:     LevelWarning,
		Src:       user.ID,
		SrcName:   user.UserName,
		Timestamp: time.Now(),
		Msg:       "Device parameters of Device has been modified!",
	}

	return msg
}

func NewMessageRemoveDeviceFromCompany(user *User) *Message {
	msg := &Message{
		Type:      TypeDevice,
		Level:     LevelWarning,
		Src:       user.ID,
		SrcName:   user.UserName,
		Timestamp: time.Now(),
		Msg:       "Device has been removed from your company!",
	}

	return msg
}

func NewMessageNewDeviceToCompany(user *User) *Message {
	msg := &Message{
		Type:      TypeDevice,
		Level:     LevelWarning,
		Src:       user.ID,
		SrcName:   user.UserName,
		Timestamp: time.Now(),
		Msg:       "You are permitted to manage Device!",
	}
	return msg
}

func NewMessageAddAuthDevice(user *User) *Message {
	msg := &Message{
		Type:      TypeDevice,
		Level:     LevelInfo,
		Src:       user.ID,
		SrcName:   user.UserName,
		Timestamp: time.Now(),
		Msg:       "You are permitted to manage Device!",
	}

	return msg
}

func NewMessageDelAuthDevice(user *User) *Message {
	msg := &Message{
		Type:      TypeDevice,
		Level:     LevelInfo,
		Src:       user.ID,
		SrcName:   user.UserName,
		Timestamp: time.Now(),
		Msg:       "Device has been removed from your account!",
	}

	return msg
}

func NewMessageDeleteDevice(user *User) *Message {
	msg := &Message{
		Type:      TypeDevice,
		Level:     LevelError,
		Src:       user.ID,
		SrcName:   user.UserName,
		Timestamp: time.Now(),
		Msg:       "Device has been deleted from your company!",
	}

	return msg
}

func NewMessagePassword(user *User) *Message {
	msg := &Message{
		Type:      TypeUser,
		Level:     LevelWarning,
		Src:       user.ID,
		SrcName:   user.UserName,
		Timestamp: time.Now(),
		Msg:       "Password has been modified!",
	}

	return msg
}
*/

func ListMessageByFilter(db *mgo.Session, m interface{}, rp *RequestParameter) ([]*Message, error) {
	c := db.DB(DruidDB).C(MessageCollection)
	result := []*Message{}
	return result, MongoList(c, m, &result, rp, nil)
}

func CountMessageByFilter(db *mgo.Session, m interface{}) (int, error) {
	c := db.DB(DruidDB).C(MessageCollection)
	return MongoCount(c, m)
}

func SendExportMessage(db *mgo.Session, user *User, devices []*Device) (error) {
	ps, err := FindPushSettingByClass(db, PushClassExport)
	if err != nil {
		return err
	}

	us := ps.GetUsers(db, "", user.CompanyID)
	if us == nil {
		return nil
	}

	msg := NewMessageExport(user, devices, us, ps)

	return msg.SendMQ(db)
}

func SendAreaMessage(db *mgo.Session, device *Device, area *DituArea) (error) {
	var msg *Message
	if area.MsgType == DituAreaMsgTypeIn {
		ps, err := FindPushSettingByClass(db, PushClassAreaIn)
		if err != nil {
			return err
		}

		us := ps.GetUsers(db, device.OwnerID, device.CompanyID)
		if us == nil {
			return nil
		}

		msg = NewMessageDituAreaIn(device, area, us, ps)
	} else {
		ps, err := FindPushSettingByClass(db, PushClassAreaOut)
		if err != nil {
			return err
		}

		us := ps.GetUsers(db, device.OwnerID, device.CompanyID)
		if us == nil {
			return nil
		}

		msg = NewMessageDituAreaOut(device, area, us, ps)
	}

	return msg.SendMQ(db)
}

func SendBiologicalMessage(db *mgo.Session, device *Device, pushType int) (error) {
	if device.OwnerID == "" || device.CompanyID == "" {
		return nil
	}

	ps, err := FindPushSettingByClass(db, pushType)
	if err != nil {
		return err
	}

	us := ps.GetUsers(db, device.OwnerID, device.CompanyID)
	if us == nil {
		return nil
	}

	msg := NewMessageBiological(device, us, ps)

	return msg.SendMQ(db)
}

func SendDeadMessage(db *mgo.Session, device *Device) (error) {
	if device.OwnerID == "" || device.CompanyID == "" {
		return nil
	}

	ps, err := FindPushSettingByClass(db, PushClassDead)
	if err != nil {
		return err
	}

	us := ps.GetUsers(db, device.OwnerID, device.CompanyID)
	if us == nil {
		return nil
	}

	msg := NewMessageBiologicalDead(device, us, ps)

	return msg.SendMQ(db)
}


/*
func SendBiologicalHighMessage(db *mgo.Session, device *Device) (error) {
	ps, err := FindPushSettingByClass(db, PushClassBiologicalHigh)
	if err != nil {
		return err
	}

	us := ps.GetUsers(db, device.OwnerID, device.CompanyID)
	if us == nil {
		return nil
	}

	msg := NewMessageBiological(device, us, ps)

	return msg.SendMQ(db)
}

func SendBiologicalLowMessage(db *mgo.Session, device *Device) (error) {
	ps, err := FindPushSettingByClass(db, PushClassBiologicalLow)
	if err != nil {
		return err
	}

	us := ps.GetUsers(db, device.OwnerID, device.CompanyID)
	if us == nil {
		return nil
	}

	msg := NewMessageBiological(device, us, ps)

	return msg.SendMQ(db)
}
*/

/*
func (msg *Message)Send(db *mgo.Session) ([]byte, error){
	if err := msg.Save(db); err != nil {
		return nil, err
	}

	return SendMessageToPush(msg)
}
*/

func FindMessageByID(db *mgo.Session, id bson.ObjectId) (*Message, error){
	c := db.DB(DruidDB).C(MessageCollection)
	result := Message{}

	if err := c.FindId(id).One(&result); err != nil {
		return nil, ErrorConvert(err)
	}
	return &result, nil
}

func (msg *Message) Delete(db *mgo.Session) error {
	c := db.DB(DruidDB).C(MessageCollection)

	if err := c.RemoveId(msg.ID); err != nil {
		return err
	}

	return nil
}

func (msg *Message) SendMQ(db *mgo.Session) ( error) {
	if err := msg.Save(db); err != nil {
		return err
	}

	return SendMessageToNSQD(msg, GetTopicMessage())
}






