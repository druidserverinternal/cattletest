package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"time"
	"io/ioutil"
	"crypto/sha1"
//	"bytes"
	"net/http"
	"encoding/json"
	"strconv"
	"encoding/hex"
	"mime"
)

const (
	WeixinConfig	=	1
	WeixinAccessToken	=	2
)

const (
	WaitTokenRefreshSecond	=	5
)

const (
	RefreshNow	=	iota
	RefreshDelay
	RefreshNo
)


type Weixin struct {
	ID					bson.ObjectId   `json:"id" bson:"_id,omitempty"`

	AccessToken			string			`json:"access_token" bson:"access_token,omitempty"`
	Ticket				string			`json:"-" bson:"ticket,omitempty"`

	Expiry	*time.Time		`json:"-" bson:"expiry,omitempty"`
	Refresh	*time.Time		`json:"-" bson:"refresh,omitempty"`

	AppID				string `json:"appid,omitempty" bson:"appid,omitempty"`
	Secret				string `json:"secret,omitempty" bson:"secret,omitempty"`

	UpdatedAt			*time.Time             `json:"updated_at,omitempty" bson:"updated_at,omitempty"`

	CompanyID			bson.ObjectId          `json:"company_id,omitempty" bson:"company_id,omitempty"`
	CompanyName			string                 `json:"company_name,omitempty" bson:"company_name,omitempty"`
}

type WeixinSignature struct {
	NonceStr			string	`json:"noncestr,omitempty"`
	Timestamp			int64	`json:"timestamp,omitempty"`
	URL					string	`json:"url,omitempty"`
//	AccessToken			string	`json:"access_token,omitempty"`
	Signature			string	`json:"signature,omitempty"`
}

type WeixinToken struct {
	AccessToken			string	`json:"access_token,omitempty"`
	AppID				string `json:"appid,omitempty" bson:"appid,omitempty"`
	Ticket				string	`json:"ticket,omitempty"`
	ExpiresIn			int64	`json:"expires_in,omitempty"`
	ErrCode				int64	`json:"errcode,omitempty"`
	ErrMsg				string	`json:"errmsg,omitempty"`
}

type WeixinMedia struct {
	Type			string	`json:"type,omitempty"`
	MediaID			string `json:"media_id,omitempty" bson:"media_id,omitempty"`
	Name			string `json:"name,omitempty" bson:"name,omitempty"`
	Data			[]byte 
}

func (wm *WeixinMedia)DownloadImage(db *mgo.Session, token string) error {
	client := http.Client {
		Timeout: HTTPRequestTimeout,
	}

	path := "http://file.api.weixin.qq.com/cgi-bin/media/get?access_token=" + token +"&media_id=" + wm.MediaID
	resp, err := client.Get(path)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	ct := resp.Header.Get("Content-Type")
	if ct != "image/jpeg" && ct != "image/png" {
		token := &WeixinToken{}
		decoder := json.NewDecoder(resp.Body)
		if err := decoder.Decode(&token); err != nil {
			return err
		}

		return Error("Get image failed:", token.ErrCode, token.ErrMsg, path)
	}

	cl := resp.Header.Get("Content-Length")
	l, err := strconv.Atoi(cl)
	if err != nil {
		return err
	}
	if l > MaxFileSize {
		return Error("Image too large.")
	}

	cd := resp.Header.Get("Content-disposition")
	_, params, err := mime.ParseMediaType(cd)
	if err != nil {
		return Error("Parse media type failed:", err, resp.Header, *wm)
	}
	wm.Name = params["filename"]

	wm.Data, _ = ioutil.ReadAll(resp.Body)
	if len(wm.Data) != l {
		return Error("Read image length failed.")
	}

	return nil
}

func (weixin *Weixin)UpdateAccessToken(db *mgo.Session) error {
	c := db.DB(DruidDB).C(WeixinCollection)
	err := c.UpdateId(weixin.ID, 
				bson.M{ "$set": 
					bson.M{
						"ticket": weixin.Ticket, 
						"access_token": weixin.AccessToken, 
						"expiry": weixin.Expiry, 
						"refresh": weixin.Refresh, 
						"updated_at": time.Now()}})
	if err != nil {
		return err
	}

	return nil
}

func (weixin *Weixin)IsExpiry() bool {
	if weixin.Expiry == nil || weixin.Refresh == nil {
		return true
	}

	if weixin.Refresh.Before(time.Now()) {
		return true
	}

	return false
}

func (sign *WeixinSignature)GenSign(jsToken string) {
	n := RandIntRunes()
	sign.NonceStr = RandStringRunes(n)
	sign.Timestamp = time.Now().Unix()
	str := "jsapi_ticket=" + jsToken + "&noncestr=" + sign.NonceStr + "&timestamp=" + strconv.FormatInt(sign.Timestamp, 10) + "&url=" + sign.URL
	h := sha1.New()
	h.Write([]byte(str))
	s := hex.EncodeToString(h.Sum(nil))

	sign.Signature = s
}

func FindWeixinByCompanyID(db *mgo.Session, id bson.ObjectId) (*Weixin, error) {
	c := db.DB(DruidDB).C(WeixinCollection)
	weixin := Weixin{}
	if err := c.Find(bson.M{"company_id": id}).One(&weixin); err != nil {
		return nil, ErrorConvert(err)
	}
	return &weixin, nil
}

func RefreshWeixinToken(db *mgo.Session, companyID bson.ObjectId, t *time.Time) (*Weixin, error) {
	c := db.DB(DruidDB).C(WeixinCollection)

	if t == nil {
		if err := MongoLock(c, bson.M{"company_id": companyID}); err != nil {
			return nil, err
		}
	} else {
		if err := MongoLockTimeout(c, bson.M{"company_id": companyID}, *t); err != nil {
			return nil, err
		}
	}
	defer MongoUnLock(c, bson.M{"company_id": companyID})

	weixin, err := FindWeixinByCompanyID(db, companyID)
	if err != nil {
		return nil, err
	}

	if weixin.AppID == "" || weixin.Secret == "" {
		return nil, Error("Appid or secret are invalid.")
	}

	if weixin.Refresh != nil && weixin.Refresh.After(time.Now()) {
		return weixin, nil
	}

	if err := weixin.RequestAccessToken(); err != nil {
		return nil, err
	}

	if err := weixin.RequestTicket(); err != nil {
		return nil, err
	}

	if err := weixin.UpdateAccessToken(db); err != nil {
		return nil, err
	}

	return weixin, nil
}

func (weixin *Weixin)RequestAccessToken() (error) {
	client := http.Client {
		Timeout: HTTPRequestTimeout,
	}
	resp, err := client.Get("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + weixin.AppID +"&secret=" + weixin.Secret)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	token := &WeixinToken{}
	decoder := json.NewDecoder(resp.Body)
	if err := decoder.Decode(&token); err != nil {
		return err
	}

	if token.AccessToken != "" && token.ExpiresIn > 0 {
		weixin.AccessToken = token.AccessToken
		expiry := time.Now().Add(time.Duration(token.ExpiresIn) * time.Second)
		weixin.Expiry = &expiry

		if token.ExpiresIn <= 60 {
			weixin.Refresh = weixin.Expiry
		} else {
			refresh := time.Now().Add(time.Duration(token.ExpiresIn - 60) * time.Second)
			weixin.Refresh = &refresh
		}

		return nil
	}

	return Error(token.ErrMsg)
}

func (weixin *Weixin)RequestTicket() (error) {
	client := http.Client {
		Timeout: HTTPRequestTimeout,
	}
	resp, err := client.Get("https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=" + weixin.AccessToken + "&type=jsapi")
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	token := &WeixinToken{}
	decoder := json.NewDecoder(resp.Body)
	if err := decoder.Decode(&token); err != nil {
		return err
	}

	if token.ErrCode == 0 && token.ExpiresIn > 0 {
		weixin.Ticket = token.Ticket
		return nil
	}

	return Error(token.ErrMsg)
}




