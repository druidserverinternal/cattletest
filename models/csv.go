package models

import (
	"github.com/gocarina/gocsv"
)

func DataToCSV(inters interface{}) ([]byte, error) {
	data, err := gocsv.MarshalBytes(inters)
	if err != nil {
		return nil, err
	}

	return data, nil
}




