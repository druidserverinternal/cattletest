package models

import (
	"fmt"
	"time"
	//	"os"
	"encoding/xml"
	"strconv"

	"github.com/spf13/viper"
)

type ListStyle struct {
	ListItemType string `xml:"listItemType,omitempty"`
}

type LineString struct {
	Tessellate   int    `xml:"tessellate"`
	AltitudeMode string `xml:"altitudeMode"`
	Coordinates  string `xml:"coordinates"`
}

type LineStyle struct {
	Color string `xml:"color"`
	Width int    `xml:"width"`
}

type IconStyle struct {
	Scale float32 `xml:"scale,omitempty"`
	Href  string  `xml:"Icon>href,omitempty"`
	Color string  `xml:"color,omitempty"`
}

type Style struct {
	ID        string     `xml:"id,attr,omitempty"`
	IconStyle *IconStyle `xml:"IconStyle,omitempty"`
	ListStyle *ListStyle `xml:"ListStyle,omitempty"`
	LineStyle *LineStyle `xml:"LineStyle,omitempty"`
}

type KmlPoint struct {
	AltitudeMode string `xml:"altitudeMode,omitempty"`
	Coordinates  string `xml:"coordinates"`
}

type Placemark struct {
	Begin       *time.Time  `xml:"TimeSpan>begin,omitempty"`
	End         *time.Time  `xml:"TimeSpan>end,omitempty"`
	StyleUrl    string      `xml:"styleUrl,omitempty"`
	Point       *KmlPoint   `xml:"Point,omitempty"`
	Description string      `xml:"description,omitempty"`
	Name        string      `xml:"name,omitempty"`
	Visibility  int         `xml:"visibility,omitempty"`
	Style       *Style      `xml:"Style,omitempty"`
	LineString  *LineString `xml:"LineString,omitempty"`
}

type LookAt struct {
	Longitude float64 `xml:"longitude,omitempty"`
	Latitude  float64 `xml:"latitude,omitempty"`
	Altitude  float64 `xml:"Altitude,omitempty"`
	Range     int     `xml:"range,omitempty"`
}

type Folder struct {
	Name        string       `xml:"name"`
	Open        int          `xml:"open"`
	Visibility  int          `xml:"visibility"`
	Description string       `xml:"description,omitempty"`
	LookAt      *LookAt      `xml:"LookAt,omitempty"`
	Style       *Style       `xml:"Style,omitempty"`
	Placemarks  []*Placemark `xml:"Placemark,omitempty"`
}

type Description struct {
	Description string `xml:",cdata"`
}
type KML struct {
	XMLName     xml.Name     `xml:"kml"`
	Xmlns       string       `xml:"xmlns,attr,omitempty"`
	Name        string       `xml:"Document>name,omitempty"`
	Open        int          `xml:"Document>open,omitempty"`
	Description *Description `xml:"Document>description,omitempty"`
	LookAt      *LookAt      `xml:"Document>LookAt,omitempty"`
	Styles      [2]Style     `xml:"Document>Style,omitempty"`
	Folders     [2]Folder    `xml:"Document>Folder,omitempty"`
	Placemarks  Placemark    `xml:"Document>Placemark,omitempty"`
}

func GPSToKml(gpss []*GPS) ([]byte, error) {
	kml := &KML{
		Xmlns:       "http://earth.google.com/kml/2.2",
		Name:        "Device:" + gpss[0].UUID,
		Open:        1,
		Description: &Description{"Company of <a href=\" http://druidtech.cn\">druidtech.cn</a><br><br>"},
	}

	length := len(gpss)
	placemarks := make([]*Placemark, length)
	kml.Folders[0].Placemarks = placemarks[:0]
	kml.Folders[0].Name = "Animation"
	kml.Folders[0].Open = 1
	kml.Folders[0].Visibility = 1
	kml.Folders[0].Description = "This is Animation for GPS"
	kml.Folders[0].Style = &Style{ListStyle: &ListStyle{"checkHideChildren"}}

	placemarks = make([]*Placemark, length)
	kml.Folders[1].Placemarks = placemarks[:0]
	kml.Folders[1].Name = "GPS Record"
	kml.Folders[1].Open = 0
	kml.Folders[1].Visibility = 1
	kml.Folders[1].Description = "This is GPS Record"

	stepImage := viper.GetString("kml.step")
	coordinateImage := viper.GetString("kml.coordinate")
	if stepImage == "" {
		stepImage = "http://maps.google.com/mapfiles/kml/paddle/ylw-stars.png"
	}
	if coordinateImage == "" {
		coordinateImage = "http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png"
	}

	kml.Styles[0].ID = "seeadler-dot-icon"
	kml.Styles[0].IconStyle = &IconStyle{1, stepImage, ""}

	kml.Styles[1].ID = "tag-icon"
	kml.Styles[1].IconStyle = &IconStyle{Href: coordinateImage, Color: "ffC2E350"}

	kml.Placemarks.Name = "GPS Line"
	kml.Placemarks.Visibility = 1
	kml.Placemarks.Style = &Style{LineStyle: &LineStyle{"ffC2E350", 1}}
	kml.Placemarks.LineString = &LineString{0, "", ""}

	lookAt := (*LookAt)(nil)

	for i, gps := range gpss {
		if gps.Longitude > 181 {
			continue
		}

		coo := strconv.FormatFloat(gps.Longitude, 'f', -1, 64)
		coo += ","
		coo += strconv.FormatFloat(gps.Latitude, 'f', -1, 64)

		point := coo
		coo += ","
		coo += strconv.FormatFloat(gps.Altitude, 'f', -1, 64)

		placemark0 := &Placemark{}
		placemark1 := &Placemark{}
		var t time.Time

		if i+1 == length {
			t = gps.Timestamp.Add(time.Hour * 24)
		} else {
			t = *gpss[i+1].Timestamp
		}
		placemark0.Begin = gps.Timestamp
		placemark0.End = &t

		if lookAt == nil {
			lookAt = &LookAt{
				Altitude:  gps.Altitude,
				Latitude:  gps.Latitude,
				Longitude: gps.Longitude,
				Range:     50000,
			}
			kml.LookAt = lookAt
			kml.Folders[0].LookAt = lookAt
			kml.Folders[1].LookAt = lookAt
		}

		placemark0.StyleUrl = "#seeadler-dot-icon"
		placemark0.Point = &KmlPoint{"absolute", coo}

		placemark1.StyleUrl = "#tag-icon"
		placemark1.Point = &KmlPoint{"", point}

		desc := fmt.Sprintf("Time: %s\nFix Number: %d\nSignal Strength: %d\nTemperature: %.1f°C\n"+
			"Location Longitude: %f°\nLocation Latitude: %f°\nLocation Altitude: %.2fm\n"+
			"Speed: %.2fm/s\nHeading: %.1f°\nGPS HDOP: %.1fm\nGPS VDOP: %.1fm\nSatellite: %d\nFix Mode: %dD\n"+
			"BatteryVoltage: %.3fV\nLight: %dLux\nHumidity: %d%%RH\nPressure: %dhPa",
			gps.Timestamp.Format("2006-01-02 15:04:05.000"), i, gps.SignalStrength, gps.Temperature,
			gps.Longitude, gps.Latitude, gps.Altitude,
			gps.Speed, gps.Course, gps.Horizontal, gps.Vertical, gps.UsedStar, gps.Dimension,
			gps.BatteryVoltage, gps.Light, gps.Humidity, gps.Pressure)
		placemark0.Description = desc
		placemark1.Description = desc

		kml.Folders[0].Placemarks = append(kml.Folders[0].Placemarks, placemark0)
		kml.Folders[1].Placemarks = append(kml.Folders[1].Placemarks, placemark1)

		if kml.Placemarks.LineString.Coordinates != "" {
			kml.Placemarks.LineString.Coordinates += " "
		}
		kml.Placemarks.LineString.Coordinates += point
	}

	output, err := xml.MarshalIndent(kml, "", "    ")
	if err != nil {
		return nil, err
	}
	return output, nil
}
