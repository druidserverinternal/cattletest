package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"time"
//	"strconv"
)

// DeviceMany is Update many device
type DeviceMany struct {
	ID          []bson.ObjectId `json:"id" bson:"_id"`
	CompanyID   bson.ObjectId   `json:"company_id,omitempty" bson:"company_id,omitempty"`
	CompanyName string          `json:"-" bson:"company_name,omitempty"`
	RoomID		bson.ObjectId   `json:"room_id,omitempty" bson:"room_id,omitempty"`
	RoomName	string          `json:"-" bson:"room_name,omitempty"`
	StockTime       *time.Time    `json:"stock_time,omitempty" bson:"stock_time,omitempty"`
	DescriptionRoot string             `json:"description_root,omitempty" bson:"description_root,omitempty"`
}

func (device *DeviceMany) UpdateStock(db *mgo.Session) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if _, err := c.UpdateAll(
		bson.M{"_id": bson.M{
			"$in": device.ID}},
		bson.M{"$set": bson.M{
			"stock_time": device.StockTime}}); err != nil {
		return err
	}
	return nil
}


func (device *DeviceMany)ListValidDevicesByCompanyID(db *mgo.Session, se bson.M) ([]*Device, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	devices := []*Device{}
	if err := c.Find(bson.M{"_id": bson.M{"$in": device.ID}, "company_id": device.CompanyID}).Select(se).All(&devices); err != nil {
		return nil, err
	}
	return devices, nil
}

func (device *DeviceMany)ListValidDevicesByUser(db *mgo.Session, userID bson.ObjectId, se bson.M) ([]*Device, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	devices := []*Device{}
	if err := c.Find(bson.M{"_id": bson.M{"$in": device.ID}, "owner_id": userID}).Select(se).All(&devices); err != nil {
		return nil, err
	}
	return devices, nil
}

// UpdateMany device hardware and firmware version
func (device *DeviceMany) UpdateDeviceCompany(db *mgo.Session) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if _, err := c.UpdateAll(
		bson.M{"_id": bson.M{
			"$in": device.ID}},
		bson.M{"$set": bson.M{
			"company_id":   device.CompanyID,
			"company_name": device.CompanyName}}); err != nil {
		return err
	}
	return nil
}

func (device *DeviceMany) ClearDeviceCompany(db *mgo.Session) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if _, err := c.UpdateAll(
		bson.M{"_id": bson.M{"$in": device.ID}},
		bson.M{"$unset": bson.M{
			"company_id":   1,
			"company_name": 1,
			"owner_id": 1,
			"owner_name": 1,
			"room_id": 1,
			"room_name": 1,
		}}); err != nil {
		return err
	}
	return nil
}

// Update a device
func (device *DeviceMany) UpdateDescription(db *mgo.Session) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if _, err := c.UpdateAll(
		bson.M{"_id": bson.M{
			"$in": device.ID}},
		bson.M{"$set": bson.M{
			"description_root": device.DescriptionRoot, }}); err != nil {
		return err
	}
	return nil
}

func (device *DeviceMany) ClearSettingCompany(db *mgo.Session) error {
	c := db.DB(DruidDB).C(SettingCollection)
	if _, err := c.UpdateAll(
		bson.M{"device_id": bson.M{"$in": device.ID}},
		bson.M{"$unset": bson.M{
			"company_id":   1,
			"company_name": 1,
		}}); err != nil {
		return err
	}
	return nil
}

// UpdateSettingCompany device hardware and firmware version
func (device *DeviceMany) UpdateSettingCompany(db *mgo.Session) error {
	c := db.DB(DruidDB).C(SettingCollection)
	if _, err := c.UpdateAll(
		bson.M{"device_id": bson.M{
			"$in": device.ID}},
		bson.M{"$set": bson.M{
			"company_id":   device.CompanyID,
			"company_name": device.CompanyName}}); err != nil {
		return err
	}
	return nil
}

func (device *DeviceMany) UpdateDeviceRoom(db *mgo.Session) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if _, err := c.UpdateAll(
		bson.M{"_id": bson.M{
			"$in": device.ID}},
		bson.M{"$set": bson.M{
			"room_id":   device.RoomID,
			"room_name": device.RoomName }}); err != nil {
		return err
	}
	return nil
}

func (device *DeviceMany) RemoveDeviceRoom(db *mgo.Session) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if _, err := c.UpdateAll(
		bson.M{"_id": bson.M{
			"$in": device.ID}},
		bson.M{"$unset": bson.M{
			"room_id":   1,
			"room_name": 1,
		}}); err != nil {
		return err
	}
	return nil
}





