package models

import (
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Cellular struct {
	ID              bson.ObjectId `json:"id,omitempty" bson:"_id,omitempty"`
	UUID            string        `json:"uuid" bson:"uuid,omitempty" csv:"UUID" excel:"UUID"`
	Mark            int           `json:"mark,omitempty" bson:"mark,omitempty" csv:"S/N" excel:"S/N"`
	DeviceID        bson.ObjectId `json:"device_id" bson:"device_id,omitempty"`
	UpdatedAt       *time.Time    `json:"updated_at,omitempty" bson:"updated_at,omitempty"`

	Timestamp       *time.Time    `json:"timestamp,omitempty" bson:"timestamp,omitempty"`
	Longitude		float64   `json:"longitude,omitempty" bson:"longitude,omitempty"`
	Latitude		float64   `json:"latitude,omitempty" bson:"latitude,omitempty"`
	Loc				*GeoPoint `json:"loc,omitempty" bson:"loc,omitempty"`
	BatteryVoltage  float64 `json:"battery_voltage" bson:"battery_voltage,omitempty"`
	FirmwareVersion int32         `json:"firmware_version" bson:"firmware_version,omitempty"`
	ErrorFlag		uint32	`json:"error_flag" bson:"error_flag"`
	TotalTime		int32	`json:"total_time" bson:"total_time"`
	HwtTime			int32	`json:"hwt_time" bson:"hwt_time"`
	SimTime			int32	`json:"sim_time" bson:"sim_time"`
	NumTime			int32	`json:"num_time" bson:"num_time"`
	RssiTime		int32	`json:"rssi_time" bson:"rssi_time"`
	RegisterTime	int32	`json:"register_time" bson:"register_time"`
	AttachTime		int32	`json:"attach_time" bson:"attach_time"`
	PdpTime			int32	`json:"pdp_time" bson:"pdp_time"`
	ConnectionTime	int32	`json:"connection_time" bson:"connection_time"`
	CommunicationTime int32	`json:"communication_time" bson:"communication_time"`
	NetworkOperator	uint32	`json:"network_operator" bson:"network_operator"`
	SignalStrength   int32  `json:"signal_strength" bson:"signal_strength"`
	BitErrorRate	int32	`json:"bit_error_rate" bson:"bit_error_rate"`
	RadioAccessTechnology			int32	`json:"radio_access_technology" bson:"radio_access_technology"`
	Temperature      float64 `json:"temperature" bson:"temperature,omitempty"`
	SmsTime			int32	`json:"sms_time" bson:"sms_time"`
	ExitFlag		int32	`json:"exit_flag" bson:"exit_flag"`
}

// UpsertByTimestamp a cellular
func (cellular *Cellular) UpsertByTimestamp(db *mgo.Session) error {
	c := db.DB(DruidDB).C(CellularCollection)
	if _, err := c.Upsert(
		bson.M{"device_id": cellular.DeviceID, "timestamp": cellular.Timestamp},
		bson.M{"$set": cellular}); err != nil {
		return err
	}
	return nil
}

// GetCellularQueue device hardware and firmware version
func GetCellularQueue(db *mgo.Session) *mgo.Bulk {
	c := db.DB(DruidDB).C(CellularCollection)
	return c.Bulk()
}

// ListCellular all of cellular by cellular, list undeleted only
func ListCellular(db *mgo.Session, rp *RequestParameter) ([]*Cellular, error) {
	c := db.DB(DruidDB).C(CellularCollection)
	cellular := []*Cellular{}
	return cellular, MongoList(c, bson.M{"deleted_at": nil}, &cellular, rp, nil)
}

// CountCellular get count device
func CountCellular(db *mgo.Session) (count int, err error) {
	c := db.DB(DruidDB).C(CellularCollection)
	return CollectionCount(c)
}

// ListCellularByFilter all of company devices
func ListCellularByFilter(db *mgo.Session, m interface{}, rp *RequestParameter) ([]*Cellular, error) {
	c := db.DB(DruidDB).C(CellularCollection)
	result := []*Cellular{}
	return result, MongoList(c, m, &result, rp, nil)
}

// CountCellularByFilter get count device of company
func CountCellularByFilter(db *mgo.Session, m interface{}) (count int, err error) {
	c := db.DB(DruidDB).C(CellularCollection)
	return MongoCount(c, m)
}

// SearchCellular search cellular , filter by m and rp
func SearchCellular(db *mgo.Session, m bson.M, rp *RequestParameter) ([]*Cellular, error) {
	c := db.DB(DruidDB).C(CellularCollection)
	cellular := []*Cellular{}
	return cellular, MongoList(c, m, &cellular, rp, nil)
}

// SearchCellularCount search cellular count
func SearchCellularCount(db *mgo.Session, m bson.M) (count int, err error) {
	c := db.DB(DruidDB).C(CellularCollection)
	return MongoCount(c, m)
}

// DeleteCellularByDeviceID all of setting
func DeleteCellularByDeviceID(db *mgo.Session, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(CellularCollection)
	return MongoDelete(c, bson.M{"device_id": id})
}

// DestroyCellularByDeviceID all of setting
func DestroyCellularByDeviceID(db *mgo.Session, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(CellularCollection)
	return MongoDestroy(c, bson.M{"device_id": id})
}

