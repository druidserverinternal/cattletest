package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"time"
	//	"fmt"
)

type DeviceMerge struct {
	ID              bson.ObjectId `json:"id" bson:"_id,omitempty"`
	UpdatedAt       *time.Time    `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
	Timestamp       *time.Time    `json:"-" bson:"timestamp,omitempty"`
	DeletedAt       *time.Time    `json:"deleted_at,omitempty" bson:"deleted_at,omitempty"`
	DeletedBy       bson.ObjectId `json:"deleted_by,omitempty" bson:"deleted_by,omitempty"`
	UUID            string        `json:"uuid" bson:"uuid,omitempty"`
	DeviceType      int32         `json:"device_type" bson:"device_type"`
	HardwareVersion int32         `json:"hardware_version" bson:"hardware_version"`
	FirmwareVersion int32         `json:"firmware_version" bson:"firmware_version"`
	CompanyID       bson.ObjectId `json:"company_id" bson:"company_id,omitempty"`
	CompanyName     string        `json:"company_name" bson:"company_name,omitempty"`
	IMSI            string        `json:"imsi,omitempty" bson:"imsi,omitempty"`
	SIMNumber       string        `json:"sim_number,omitempty" bson:"sim_number,omitempty"`
	Mac             string        `json:"mac,omitempty" bson:"mac,omitempty"`
	//	Description     string        `json:"description,omitempty" bson:"description,omitempty"`
	Mark           int     `json:"mark" bson:"mark,omitempty"`
	SN             string  `json:"sn,omitempty" bson:"sn,omitempty"`
	BatteryVoltage float64 `json:"battery_voltage" bson:"battery_voltage,omitempty"`

	// remove lastxxx, add need fields
	Temperature   float64   `json:"temperature" bson:"temperature,omitempty"`
	Loc           *GeoPoint `json:"loc,omitempty" bson:"loc,omitempty"`
	Longitude     float64   `json:"longitude" bson:"longitude,omitempty"`
	Latitude      float64   `json:"latitude" bson:"latitude,omitempty"`
	PointLocation int       `json:"point_location" bson:"point_location,omitempty"`
	LocationTimestamp  *time.Time `json:"location_timestamp,omitempty" bson:"location_timestamp,omitempty"`

	TotalGPS       int `json:"total_gps" bson:"total_gps,omitempty"`
	TotalBehavior  int `json:"total_behavior" bson:"total_behavior,omitempty"`
	TotalBehavior2 int `json:"total_behavior2" bson:"total_behavior2,omitempty"`
	TotalArea      int `json:"total_area" bson:"total_area,omitempty"`

	RoomID            bson.ObjectId `json:"room_id" bson:"room_id,omitempty"`
	RoomName          string        `json:"room_name" bson:"room_name,omitempty"`
	OwnerID           bson.ObjectId `json:"owner_id" bson:"owner_id,omitempty"`
	OwnerName         string        `json:"owner_name" bson:"owner_name,omitempty"`
	Survive           int           `json:"survive" bson:"survive"`
	Activity          int           `json:"activity" bson:"activity,omitempty"`       // 弱 1   中 2    强3
	Motion            int           `json:"motion,omitempty" bson:"motion,omitempty"` //新增动作 1 取食，2 反刍，3 其他
	StockTime         *time.Time    `json:"stock_time,omitempty" bson:"stock_time,omitempty"`
	AttachedAt        *time.Time    `json:"attached_at,omitempty" bson:"attached_at,omitempty"`
	LastBeh2Timestamp *time.Time    `json:"-" bson:"last_beh2_timestamp,omitempty"`

	SignalStrength        int32  `json:"signal_strength" bson:"signal_strength,omitempty"`
	BitErrorRate          int32  `json:"bit_error_rate,omitempty" bson:"bit_error_rate"`
	RadioAccessTechnology int32  `json:"radio_access_technology,omitempty" bson:"radio_access_technology"`
	NetworkOperator       uint32 `json:"network_operator,omitempty" bson:"network_operator"`

	// biological cattle
	NickName      string          `json:"nickname" bson:"nickname,omitempty"`
	BirthDate     *time.Time      `json:"birth_date,omitempty" bson:"birth_date,omitempty"`
	Gender        int             `json:"gender" bson:"gender,omitempty"`
	Weight        int             `json:"weight" bson:"weight,omitempty"`
	Height        int             `json:"height" bson:"height,omitempty"`
	Bust          int             `json:"bust" bson:"bust,omitempty"`     // xiongwei
	Cannon        int             `json:"cannon" bson:"cannon,omitempty"` // guanwei
	CoatColor     string          `json:"coat_color" bson:"coat_color,omitempty"`
	Description   string          `json:"description" bson:"description,omitempty"`
	Images        []bson.ObjectId `json:"images" bson:"images"`
	Behavior      int             `json:"behavior" bson:"behavior"`
	BehaviorBegin *time.Time      `json:"behavior_begin,omitempty" bson:"behavior_begin,omitempty"`
	Species       int             `json:"species" bson:"species,omitempty"`

	// for user with web
	GeoFence []*DituArea `json:"geo_fence,omitempty" bson:"-"`
}

func ListDeviceMergeFinalMatchByFilter(db *mgo.Session, m bson.M, fm bson.M, rp *RequestParameter, se bson.M) ([]*DeviceMerge, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	d := []*DeviceMerge{}

	args := &MergeArg{
		Match:        m,
		FinalMatch:        fm,
		From:         CattleCollection,
		LocalField:   "_id",
		ForeignField: "device_id",
		As:           "data",
		AddField:     bson.M{"description": bson.M{"$arrayElemAt": InterfaceArray("$data.description", 0)}},
	}

	return d, MongoListMergeFinalMatch(c, args, &d, rp, se)
}


func ListDeviceMergeByFilter(db *mgo.Session, m bson.M, rp *RequestParameter, se bson.M) ([]*DeviceMerge, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	d := []*DeviceMerge{}

	args := &MergeArg{
		Match:        m,
		From:         CattleCollection,
		LocalField:   "_id",
		ForeignField: "device_id",
		As:           "data",
		AddField:     bson.M{"description": bson.M{"$arrayElemAt": InterfaceArray("$data.description", 0)}},
	}

	return d, MongoListMerge(c, args, &d, rp, se)
}

// FindDeviceMergeByID find one device by id
func FindDeviceMergeByID(db *mgo.Session, id bson.ObjectId, se bson.M) (*DeviceMerge, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	d := &DeviceMerge{}

	args := &MergeArg{
		Match:        bson.M{"_id": id},
		From:         CattleCollection,
		LocalField:   "_id",
		ForeignField: "device_id",
		As:           "data",
		AddField:     bson.M{"description": bson.M{"$arrayElemAt": InterfaceArray("$data.description", 0)}},
	}
	return d, MongoGetMerge(c, args, d, se)
}

func ListDeviceMergeByRoomID(db *mgo.Session, id bson.ObjectId, rp *RequestParameter, se bson.M) ([]*DeviceMerge, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	devices := []*DeviceMerge{}
	args := &MergeArg{
		Match:        bson.M{"room_id": id},
		From:         CattleCollection,
		LocalField:   "_id",
		ForeignField: "device_id",
		As:           "data",
		AddField:     bson.M{"description": bson.M{"$arrayElemAt": InterfaceArray("$data.description", 0)}},
	}
	return devices, MongoListMerge(c, args, &devices, rp, se)
}
