package models

import (
	"crypto/md5"
	"encoding/binary"
	"encoding/hex"
	"errors"
	"time"
	"mime/multipart"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const midPath = "/firmware/"

const maxSize = 1024 * 1024 * 2

// Firmware struct represents the user object in the database
type Firmware struct {
	ID              bson.ObjectId `json:"id" bson:"_id,omitempty"`
	DeletedAt       *time.Time    `json:"-" bson:"deleted_at,omitempty"`
	DeviceType      int32         `json:"device_type" bson:"device_type,omitempty"`
	HardwareVersion int32         `json:"hardware_version" bson:"hardware_version,omitempty"`
	FirmwareVersion int32         `json:"firmware_version" bson:"firmware_version,omitempty"`
	Path            string        `json:"path,omitempty" bson:"path,omitempty"`
	Name            string        `json:"name,omitempty" bson:"name,omitempty"`
	MD5             string        `json:"md5,omitempty" bson:"md5,omitempty"`
	Size            int           `json:"size,omitempty" bson:"size,omitempty"`
	Point           bson.ObjectId `json:"point,omitempty" bson:"point,omitempty"`
	Data            []byte        `json:"-" bson:"-"`
	File            *multipart.FileHeader        `json:"-" bson:"-"`
	//	UpdatedAt       *time.Time     `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
}

// Upload2 firmware to server
func (firmware *Firmware) Upload2(db *mgo.Session) error {
	c := db.DB(DruidDB).C(FirmwareCollection)
	m := md5.Sum(firmware.Data)
	firmware.MD5 = hex.EncodeToString(m[:])

	if err := c.Insert(firmware); err != nil {
		DeleteFileByID(db, firmware.Point)
		return err
	}
	return nil
}

/*
// Upload firmware to server
func (firmware *Firmware) UploadToMongo(db *mgo.Session, data []byte) error {
	c := db.DB(DruidDB).C(FirmwareCollection)
	if err := c.Insert(firmware); err != nil {
		return err
	}
	return nil
}
*/

func (firmware *Firmware) ParseFirmware(data []byte) error {
	if int(binary.LittleEndian.Uint32(data[8:12])) != len(data) {
		return errors.New("Firmware size verify failed.")
	}

	firmware.FirmwareVersion = int32(binary.LittleEndian.Uint32(data[12:16]))
	firmware.HardwareVersion = int32(binary.LittleEndian.Uint32(data[16:20]))
	firmware.DeviceType = int32(binary.LittleEndian.Uint32(data[20:24]))

	if firmware.FirmwareVersion < 0 ||
		firmware.FirmwareVersion > 99999 ||
		firmware.HardwareVersion <= 0 ||
		firmware.HardwareVersion > 99999 ||
		firmware.DeviceType <= 0 ||
		firmware.DeviceType > 99999 {
		return errors.New("Firmware version verify failed.")
	}
	return nil
}

func (firmware *Firmware) VerifyFirmware() error {
	data := firmware.Data
	if firmware.Size != len(data) {
		return errors.New("Firmware size recv verify failed.")
	}

	if int(binary.LittleEndian.Uint32(data[8:12])) != len(data) {
		return errors.New("Firmware size verify failed.")
	}
	if int32(binary.LittleEndian.Uint32(data[12:16])) != firmware.FirmwareVersion {
		return errors.New("Firmware version verify failed.")
	}
	if int32(binary.LittleEndian.Uint32(data[16:20])) != firmware.HardwareVersion {
		return errors.New("Hardware version verify failed.")
	}
	if int32(binary.LittleEndian.Uint32(data[20:24])) != firmware.HardwareVersion {
		return errors.New("Device type verify failed.")
	}
	return nil
}

func (firmware *Firmware) FindFirmwareByVersion(db *mgo.Session) (*Firmware, error) {
	c := db.DB(DruidDB).C(FirmwareCollection)
	f := Firmware{}

	if err := c.Find(bson.M{
		"device_type":      firmware.DeviceType,
		"hardware_version": firmware.HardwareVersion,
		"firmware_version": firmware.FirmwareVersion}).One(&f); err != nil {
		return nil, ErrorConvert(err)
	}

	return &f, nil
}

// ListFirmwares list all of firmware
func ListFirmwares(db *mgo.Session, rp *RequestParameter) ([]*Firmware, error) {
	c := db.DB(DruidDB).C(FirmwareCollection)
	firmwares := []*Firmware{}
	return firmwares, MongoList(c, bson.M{"deleted_at": nil}, &firmwares, rp, nil)
}

// ListLatestFirmwares list latest all of firmware
func ListFirmwareVersion(db *mgo.Session, rp *RequestParameter) ([]*FirmwareVersion, error) {
	c := db.DB(DruidDB).C(FirmwareCollection)
	fw := []*FirmwareVersion{}

	pipe := c.Pipe([]bson.M{
		bson.M{
			"$group": bson.M{
				"_id": bson.M{
					"dt": "$device_type",
					"hv": "$hardware_version"},
				"max": bson.M{"$max": "$firmware_version"}}},
		bson.M{
			"$project": bson.M{
				"device_type":      "$_id.dt",
				"hardware_version": "$_id.hv",
				"firmware_version": "$max"}}})

	if err := pipe.All(&fw); err != nil {
		return nil, ErrorConvert(err)
	}
	return fw, nil
}

// DeleteFirmwareByID delete a firmware by id
func DeleteFirmwareByID(db *mgo.Session, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(FirmwareCollection)
	if err := c.RemoveId(id); err != nil {
		return err
	}
	return nil
}

// FindFirmwareByID find a firmware by id
func FindFirmwareByID(db *mgo.Session, id bson.ObjectId) (*Firmware, error) {
	c := db.DB(DruidDB).C(FirmwareCollection)
	firmware := Firmware{}

	if err := c.FindId(id).One(&firmware); err != nil {
		return nil, err
	}

	return &firmware, nil
}

// CountFirmware get count
func CountFirmware(db *mgo.Session) (count int, err error) {
	c := db.DB(DruidDB).C(FirmwareCollection)
	return MongoCount(c, bson.M{"deleted_at": nil})
}
