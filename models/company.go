package models

import (
	/*
		"golang.org/x/crypto/bcrypt"
	*/
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	//	"gopkg.in/guregu/null.v3"
	"regexp"
	"time"
)

// RegexpCompanyName is verify company name
var RegexpCompanyName = regexp.MustCompile(`^[A-Za-z0-9\p{Han}\+\-_]+$`)

// Company struct represents the user object in the database
type Company struct {
	ID        bson.ObjectId `json:"id" bson:"_id,omitempty"`
	DeletedAt *time.Time    `json:"-" bson:"updated_at,omitempty"`
	//	UpdatedAt   time.Time     `json:"updated_at" bson:"updated_at,omitempty"`
	CompanyName string `json:"company_name" bson:"company_name,omitempty"`
	Phone       string `json:"phone" bson:"phone,omitempty"`
	Address     string `json:"address" bson:"address,omitempty"`
	DeviceCount int    `json:"device_count" bson:"-"`
	UserCount   int    `json:"user_count" bson:"-"`
	Alert       *Alert `json:"alert,omitempty" bson:"alert,omitempty"`
	Money		float32		  `json:"money,omitempty" bson:"money,omitempty"`
}

func (company *Company) UpdateAlert(db *mgo.Session) error {
	c := db.DB(DruidDB).C(CompanyCollection)
	if err := c.UpdateId(company.ID, bson.M{"$set": bson.M{"alert": company.Alert}}); err != nil {
		return err
	}
	return nil
}

// Create a new company
func (company *Company) Create(db *mgo.Session) error {
//	company.ID = bson.NewObjectId()
	//	company.UpdatedAt = time.Now()

	c := db.DB(DruidDB).C(CompanyCollection)
	_, err := MongoAtomInsert(c, bson.M{"company_name": company.CompanyName, "deleted_at": nil}, company, company)
	if err != nil {
		return err
	}
	return nil
}

//Update a company infomation
func (company *Company) Update(db *mgo.Session) error {
	//	company.ID = bson.NewObjectId()
	//	company.UpdatedAt = time.Now()

	c := db.DB(DruidDB).C(CompanyCollection)
	if err := c.UpdateId(company.ID, bson.M{"$set": company}); err != nil {
		return err
	}
	return nil
}

// DeleteCompanyByID mark a company deleted by id
func DeleteCompanyByID(db *mgo.Session, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(CompanyCollection)
	/*
	if err := c.RemoveId(id); err != nil {
		return err
	}
	return nil
	*/
	return MongoDelete(c, bson.M{"_id": id})
}

// ListCompanies list all of companies
func ListCompanies(db *mgo.Session, rp *RequestParameter) ([]*Company, error) {
	c := db.DB(DruidDB).C(CompanyCollection)
	companies := []*Company{}
	return companies, MongoList(c, bson.M{"deleted_at": nil}, &companies, rp, nil)
}

// GetCompanyNameByID get a company name by company id
func GetCompanyNameByID(db *mgo.Session, id bson.ObjectId) string {
	company := FindCompanyByID(db, id)
	if company != nil {
		return company.CompanyName
	}

	return ""
}

// FindCompanyByName find a company by company name
func FindCompanyByName(db *mgo.Session, name string) *Company {
	c := db.DB(DruidDB).C(CompanyCollection)

	company := Company{}
	err := c.Find(bson.M{"company_name": name, "deleted_at": nil}).One(&company)
	if err != nil {
		return nil
	}
	return &company
}

// FindCompanyByID find a company by company id
func FindCompanyByID(db *mgo.Session, id bson.ObjectId) *Company {
	c := db.DB(DruidDB).C(CompanyCollection)
	company := Company{}

	err := c.Find(bson.M{"_id": id, "deleted_at": nil}).One(&company)
	if err != nil {
		return nil
	}

	return &company
}

// CompanyCount count all of company
func CompanyCount(db *mgo.Session) (count int, err error) {
	c := db.DB(DruidDB).C(CompanyCollection)
	return MongoCount(c, bson.M{"deleted_at": nil})
}

func CompanyLockTimeout(db *mgo.Session, id bson.ObjectId, u time.Time) error {
	c := db.DB(DruidDB).C(CompanyCollection)
	return MongoLockTimeout(c, bson.M{"_id": id}, u)
}
