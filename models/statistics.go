package models

import (
	// "fmt"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	StatisticsTypeGPS = "gps"
)

const (
	StatisticsRolePosition = "position"
)

const (
	StatisticsProcessTypeUnknown = 0
	StatisticsProcessTypeODBA    = 1
)

type StatisticsProcess struct {
	ID        bson.ObjectId `json:"id,omitempty" bson:"_id,omitempty"`
	UpdatedAt *time.Time    `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
	Type      int           `json:"type" bson:"type"`
}

type GPSStatisticsRequest struct {
	DeviceID bson.ObjectId `json:"device_id,omitempty"`
	Date     []string      `json:"date,omitempty"`
}

type GPSStatisticsTimestamp struct {
	ID       bson.ObjectId `json:"id,omitempty" bson:"_id,omitempty"`
	DeviceID bson.ObjectId `json:"device_id,omitempty" bson:"device_id,omitempty"`
	UUID     string        `json:"uuid" bson:"uuid,omitempty"`
	Mark     int           `json:"mark" bson:"mark,omitempty"`

	UpdatedAt *time.Time `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
	Timestamp *time.Time `json:"timestamp,omitempty" bson:"timestamp,omitempty"`
}

const (
	StatisticsODBATypeDay   = 0
	StatisticsODBATypeWeek  = 1
	StatisticsODBATypeMonth = 2
)

type StatisticsODBA struct {
	ID          bson.ObjectId `json:"id,omitempty" bson:"_id,omitempty"`
	DeviceID    bson.ObjectId `json:"device_id,omitempty" bson:"device_id,omitempty"`
	UUID        string        `json:"uuid" bson:"uuid,omitempty"`
	OwnerID     bson.ObjectId `json:"owner_id" bson:"owner_id,omitempty"`
	Mark        int           `json:"mark" bson:"mark,omitempty"`
	Index       int           `json:"index" bson:"index"`
	Date        string        `json:"date,omitempty" bson:"date,omitempty"`
	Type        int           `json:"type" bson:"type"`
	ODBA        int           `json:"odba" bson:"odba"`
	ChangeIndex int           `json:"change_index" bson:"-"`
	//	Count       int           `json:"-" bson:"count"`
	NickName string        `json:"nickname" bson:"-"`
	Avatar   bson.ObjectId `json:"avatar" bson:"-"`
}

func (so *StatisticsODBA) Insert(db *mgo.Session) error {
	c := db.DB(DruidDB).C(StatisticsODBACollection)
	if _, err := c.Upsert(bson.M{"device_id": so.DeviceID, "type": so.Type, "date": so.Date}, bson.M{"$set": so}); err != nil {
		return err
	}
	return nil
}

func GetStatisticsODBAQueue(db *mgo.Session) *mgo.Bulk {
	c := db.DB(DruidDB).C(StatisticsODBACollection)
	return c.Bulk()
}

func (sp *StatisticsProcess) Insert(db *mgo.Session) error {
	c := db.DB(DruidDB).C(StatisticsProcessCollection)
	if _, err := c.Upsert(bson.M{"type": sp.Type}, bson.M{"$set": bson.M{"updated_at": sp.UpdatedAt}}); err != nil {
		return err
	}
	return nil
}

func FindFirstStatisticsODBAByFilter(db *mgo.Session, m bson.M) (*StatisticsODBA, error) {
	c := db.DB(DruidDB).C(StatisticsODBACollection)
	so := StatisticsODBA{}
	if err := MongoFind(c, m, &so, "date", nil); err != nil {
		return nil, ErrorConvert(err)
	}
	return &so, nil
}

func FindStatisticsProcessByType(db *mgo.Session, t int) (*StatisticsProcess, error) {
	c := db.DB(DruidDB).C(StatisticsProcessCollection)
	sp := &StatisticsProcess{}
	if err := c.Find(bson.M{"type": t}).One(sp); err != nil {
		return nil, ErrorConvert(err)
	}
	return sp, nil
}

type StatisticsODBASlice []*StatisticsODBA

func (slice StatisticsODBASlice) Len() int {
	return len(slice)
}

func (slice StatisticsODBASlice) Less(i, j int) bool {
	return slice[i].ODBA > slice[j].ODBA
}

func (slice StatisticsODBASlice) Swap(i, j int) {
	slice[i], slice[j] = slice[j], slice[i]
}

func (gst *GPSStatisticsTimestamp) Update(db *mgo.Session) error {
	c := db.DB(DruidDB).C(GPSStatisticsTimestampCollection)
	if _, err := c.UpsertId(gst.DeviceID, bson.M{"$set": gst}); err != nil {
		return err
	}
	return nil
}

/*
func FindGPSStatisticsTimestampByDeviceID(db *mgo.Session, id bson.ObjectId) (*GPSStatisticsTimestamp, error) {
	c := db.DB(DruidDB).C(GPSStatisticsTimestampCollection)

	gst := GPSStatisticsTimestamp{}
	if err := FindDeviceByID(db, id, &gst, nil); err != nil {
		return nil, ErrorConvert(err)
	}
	return &gst, nil
}
*/

type GPSStatistics struct {
	ID       bson.ObjectId `json:"id" bson:"_id,omitempty"`
	DeviceID bson.ObjectId `json:"device_id" bson:"device_id,omitempty"`
	UUID     string        `json:"uuid" bson:"uuid,omitempty"`
	Mark     int           `json:"mark" bson:"mark,omitempty"`
	Date     string        `json:"date" bson:"date,omitempty"`
	//	UpdatedAtBegin *time.Time    `json:"updated_at_begin" bson:"updated_at_begin,omitempty"`
	//	UpdatedAtEnd   *time.Time    `json:"updated_at_end" bson:"updated_at_end,omitempty"`
	//	TimestampBegin *time.Time    `json:"timestamp_begin" bson:"timestamp_begin,omitempty"`
	//	TimestampEnd   *time.Time    `json:"timestamp_end" bson:"timestamp_end,omitempty"`
	Count        int `json:"count" bson:"count,omitempty"`
	SuccessCount int `json:"success_count" bson:"success_count"`
	FailureCount int `json:"failure_count" bson:"failure_count"`
	Percentage   int `json:"percentage" bson:"percentage"`
}

// FirmwareVersion struct
type FirmwareVersion struct {
	CompanyID       bson.ObjectId `json:"company_id,omitempty" bson:"company_id,omitempty"`
	CompanyName     string        `json:"company_name,omitempty" bson:"company_name,omitempty"`
	FirmwareVersion int32         `json:"firmware_version,omitempty" bson:"firmware_version,omitempty"`
	Count           int           `json:"count" bson:"count"`
}

func (gs *GPSStatistics) Insert(db *mgo.Session) error {
	c := db.DB(DruidDB).C(GPSStatisticsCollection)
	if _, err := c.Upsert(bson.M{"device_id": gs.DeviceID, "date": gs.Date}, gs); err != nil {
		return err
	}
	return nil
}

func GroupStatisticsODBAByFilter(db *mgo.Session, m bson.M) ([]*StatisticsODBA, error) {
	c := db.DB(DruidDB).C(StatisticsODBACollection)
	odbas := []*StatisticsODBA{}
	// ([{$group: {_id: "$device_id", count: {$sum: "$odba"}}}, {$sort: {count: -1}}])
	pipe := c.Pipe(
		[]bson.M{
			bson.M{
				"$match": m,
			},
			bson.M{
				"$group": bson.M{
					"_id":  "$device_id",
					"odba": bson.M{"$sum": "$odba"},
				}},
			bson.M{
				"$sort": bson.M{"odba": -1},
			},
		})

	if err := pipe.All(&odbas); err != nil {
		return odbas, ErrorConvert(err)
	}
	return odbas, nil
}

func ListStatisticsODBAByFilter(db *mgo.Session, m bson.M, rp *RequestParameter, se bson.M) ([]*StatisticsODBA, error) {
	c := db.DB(DruidDB).C(StatisticsODBACollection)
	odbas := []*StatisticsODBA{}
	return odbas, MongoList(c, m, &odbas, rp, se)
}

func GetGPSStatistcsQueue(db *mgo.Session) *mgo.Bulk {
	c := db.DB(DruidDB).C(GPSStatisticsCollection)
	return c.Bulk()
}

func ListGPSStatistcsBySearch(db *mgo.Session, m bson.M, rp *RequestParameter) ([]*GPSStatistics, error) {
	c := db.DB(DruidDB).C(GPSStatisticsCollection)
	gs := []*GPSStatistics{}

	return gs, MongoList(c, m, &gs, rp, nil)
}

func GetLatestGPSStatistcsByDeviceID(db *mgo.Session, deviceID bson.ObjectId) (*GPSStatistics, error) {
	c := db.DB(DruidDB).C(GPSStatisticsCollection)
	gs := GPSStatistics{}

	if err := c.Find(bson.M{"device_id": deviceID}).Sort("-updated_at_end").Limit(1).One(&gs); err != nil {
		return nil, ErrorConvert(err)
	}
	return &gs, nil
}

func GPSStatistcsSearchCount(db *mgo.Session, m bson.M) (int, error) {
	c := db.DB(DruidDB).C(GPSStatisticsCollection)
	return MongoCount(c, m)
}

// CountFirmwareVersion statistics firmware version count
func CountFirmwareVersion(db *mgo.Session) (*[]*FirmwareVersion, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	fv := []*FirmwareVersion{}
	pipe := c.Pipe([]bson.M{
		bson.M{
			"$group": bson.M{
				"_id": bson.M{
					"id": "$company_id",
					"v":  "$firmware_version"},
				"company_name": bson.M{"$first": "$company_name"},
				"count":        bson.M{"$sum": 1}}},
		bson.M{
			"$project": bson.M{
				"company_id":       "$_id.id",
				"company_name":     1,
				"_id":              0,
				"firmware_version": "$_id.v",
				"count":            1}}})

	/*	$group: {
			_id: { "id": "$company_id", "v": "$firmware_version" },
			company_name: { $first: "$company_name" },
			count : {$sum : 1}
		}}, {
		$project: {
			company_id: "$_id.id",
			_id: 0,
			company_name: 1,
			version: "$_id.v",
			count: 1}}
	*/
	if err := pipe.All(&fv); err != nil {
		return nil, ErrorConvert(err)
	}
	return &fv, nil
}

func ListStatisticsRole(db *mgo.Session) ([]*GPSStatisticsTimestamp, error) {
	c := db.DB(DruidDB).C(GPSStatisticsTimestampCollection)

	sr := []*GPSStatisticsTimestamp{}
	return sr, MongoList(c, nil, &sr, nil, nil)
}

func FindGPSStatisticsTimestampByType(db *mgo.Session, t string, role string) (*GPSStatisticsTimestamp, error) {
	c := db.DB(DruidDB).C(GPSStatisticsTimestampCollection)

	sr := GPSStatisticsTimestamp{}
	if err := c.Find(bson.M{"role": role, "type": t}).One(&sr); err != nil {
		return nil, ErrorConvert(err)
	}
	return &sr, nil
}
