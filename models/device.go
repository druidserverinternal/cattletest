package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"math"
	"regexp"
	"strings"
	"time"
	//	"strconv"
)

const (
	CurrentFinishTimeout = 10
)

const (
	SurviveLive     = 0
	SurviveDead     = 1
	SurviveAbnormal = 2

	SurviveLowExpend    = 3
	SurviveStaticExpend = 4
	SurviveHighExpend   = 5

	SurviveLoving = 6
)

const (
	//新增动作 1 取食，2 反刍，3 其他
	MotionKnown       = 0
	MotionEat         = 1 // 取食
	MotionRumination  = 2 // 反刍
	MotionOtherLow    = 3 // otherLow
	MotionOtherMedium = 4 // otherMedium
	MotionOtherHight  = 5 // otherHight
)

const (
	ActivityLevel0 = 0
	ActivityLevel1 = 1
	ActivityLevel2 = 2
	ActivityLevel3 = 3
)

var RegexpMark = regexp.MustCompile(`^[0-9]+$`)

type DeviceList []*Device

const (
	DeviceConnectionLost = -1
	DeviceConnectionSMS  = 1
	DeviceConnectionOk   = 0
)

var UserSelect = bson.M{
	/* device fields */
	"imsi":                    0,
	"sim_number":              0,
	"mac":                     0,
	"description_root":        0,
	"loc":                     0,
	"stock_time":              0,
	"bit_error_rate":          0,
	"radio_access_technology": 0,
	"network_operator":        0,

	/* behavior fields */
	"run_expend":   0,
	"crawl_expend": 0,
	"peck_expend":  0,
	"fly_expend":   0,
	"other_expend": 0,
	"run_time":     0,
	"crawl_time":   0,
	"peck_time":    0,
	"fly_time":     0,
	"other_time":   0,
}

var AdminSelect = bson.M{
	"imsi":                    0,
	"sim_number":              0,
	"mac":                     0,
	"description_root":        0,
	"loc":                     0,
	"stock_time":              0,
	"bit_error_rate":          0,
	"radio_access_technology": 0,
	"network_operator":        0,

	"run_expend":   0,
	"crawl_expend": 0,
	"peck_expend":  0,
	"fly_expend":   0,
	"other_expend": 0,
	"run_time":     0,
	"crawl_time":   0,
	"peck_time":    0,
	"fly_time":     0,
	"other_time":   0,
}

// Device struct is device object with mongodb
type Device struct {
	ID              bson.ObjectId `json:"id" bson:"_id,omitempty"`
	UpdatedAt       *time.Time    `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
	Timestamp       *time.Time    `json:"-" bson:"timestamp,omitempty"`
	DeletedAt       *time.Time    `json:"deleted_at,omitempty" bson:"deleted_at,omitempty"`
	DeletedBy       bson.ObjectId `json:"deleted_by,omitempty" bson:"deleted_by,omitempty"`
	UUID            string        `json:"uuid" bson:"uuid,omitempty"`
	DeviceType      int32         `json:"device_type" bson:"device_type"`
	HardwareVersion int32         `json:"hardware_version" bson:"hardware_version"`
	FirmwareVersion int32         `json:"firmware_version" bson:"firmware_version"`
	CompanyID       bson.ObjectId `json:"company_id" bson:"company_id,omitempty"`
	CompanyName     string        `json:"company_name" bson:"company_name,omitempty"`
	IMSI            string        `json:"imsi,omitempty" bson:"imsi,omitempty"`
	SIMNumber       string        `json:"sim_number,omitempty" bson:"sim_number,omitempty"`
	Mac             string        `json:"mac,omitempty" bson:"mac,omitempty"`
	Description     string        `json:"description,omitempty" bson:"description,omitempty"`
	DescriptionRoot string        `json:"description_root,omitempty" bson:"description_root,omitempty"`
	BiologicalID    bson.ObjectId `json:"biological_id" bson:"biological_id,omitempty"`
	BiologicalType  string        `json:"biological_type" bson:"biological_type,omitempty"`
	//	Biological		*Cattle        `json:"biological,omitempty" bson:"biological,omitempty"`
	//	BiologicalRef   mgo.DBRef     `json:"-" bson:"biological_ref,omitempty"`
	Mark            int    `json:"mark" bson:"mark,omitempty"`
	SN              string `json:"sn,omitempty" bson:"sn,omitempty"`
	//	Owner           string        `json:"owner,omitempty" bson:"owner,omitempty"`
	BatteryVoltage float64 `json:"battery_voltage" bson:"battery_voltage,omitempty"`

	// remove lastxxx, add need fields
	Temperature   float64   `json:"temperature" bson:"temperature,omitempty"`
	Loc           *GeoPoint `json:"loc,omitempty" bson:"loc,omitempty"`
	Longitude     float64   `json:"longitude" bson:"longitude,omitempty"`
	Latitude      float64   `json:"latitude" bson:"latitude,omitempty"`
	PointLocation int       `json:"point_location" bson:"point_location,omitempty"`
	LocationTimestamp  *time.Time `json:"location_timestamp,omitempty" bson:"location_timestamp,omitempty"`
	//	LastGPS         *GPS          `json:"last_gps,omitempty" bson:"last_gps,omitempty"`
	//	LastBehavior    *Behavior     `json:"last_behavior,omitempty" bson:"last_behavior,omitempty"`
	//	LastValidGPS    *GPS          `json:"last_valid_gps,omitempty" bson:"last_valid_gps,omitempty"`

	TotalGPS       int `json:"total_gps" bson:"total_gps,omitempty"`
	TotalBehavior2 int `json:"total_behavior2" bson:"total_behavior2,omitempty"`

	TotalArea int `json:"total_area" bson:"total_area,omitempty"`

	TodayGPS          int        `json:"-" bson:"today_gps,omitempty"`
	TodayBeh          int        `json:"-" bson:"today_beh,omitempty"`
	TodayBeh2         int        `json:"-" bson:"today_beh2,omitempty"`
	LastGPSTimestamp  *time.Time `json:"-" bson:"last_gps_timestamp,omitempty"`
	LastBeh2Timestamp *time.Time `json:"-" bson:"last_beh2_timestamp,omitempty"`

	GPSID       bson.ObjectId `json:"-" bson:"gps_id,omitempty"`
	Behavior2ID bson.ObjectId `json:"-" bson:"behavior2_id,omitempty"`
	BehaviorID  bson.ObjectId `json:"-" bson:"behavior_id,omitempty"`

	RoomID     bson.ObjectId `json:"room_id" bson:"room_id,omitempty"`
	RoomName   string        `json:"room_name" bson:"room_name,omitempty"`
	OwnerID    bson.ObjectId `json:"owner_id" bson:"owner_id,omitempty"`
	OwnerName  string        `json:"owner_name" bson:"owner_name,omitempty"`
	Survive    int           `json:"survive" bson:"survive"`
	Activity   int           `json:"activity" bson:"activity,omitempty"`
	Motion     int           `json:"motion,omitempty" bson:"motion,omitempty"` //新增动作 1 取食，2 反刍，3 其他
	StockTime  *time.Time    `json:"stock_time,omitempty" bson:"stock_time,omitempty"`
	NickName   string        `json:"nickname,omitempty" bson:"-"`
	AttachedAt *time.Time    `json:"attached_at,omitempty" bson:"attached_at,omitempty"`

	SignalStrength        int32  `json:"signal_strength" bson:"signal_strength,omitempty"`
	BitErrorRate          int32  `json:"bit_error_rate,omitempty" bson:"bit_error_rate"`
	RadioAccessTechnology int32  `json:"radio_access_technology,omitempty" bson:"radio_access_technology"`
	NetworkOperator       uint32 `json:"network_operator,omitempty" bson:"network_operator"`

	// for user with web
	GeoFence			[]*DituArea		`json:"geo_fence,omitempty" bson:"-"`	
	BehaviorVers        int       `json:"-" bson:"behavior_vers,omitempty"`  //用于行为数据版本管理
	ODBAVers            int        `json:"-" bson:"odba_vers,omitempty"`   // 用于发情，活动量分析版本管理

	//	LostStatus		int			`json:"lost_status" bson:"lost_status"`
	//	LostTime		int			`json:"lost_time" bson:"lost_time"`
	//	SurviveTime		*time.Time	  `json:"survive_time,omitempty" bson:"survive_time,omitempty"`
	//	TotalExpendAvg  int32		`json:"total_expend_avg" bson:"total_expend_avg"`
	//	Behavior			 int			 `json:"behavior" bson:"behavior"`
	//	BehaviorBegin		 *time.Time		 `json:"behavior_begin,omitempty" bson:"behavior_begin,omitempty"`
}

// Update device hardware and firmware version
func (device *Device) UpdateDeviceCompany(db *mgo.Session) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if err := c.UpdateId(device.ID,
		bson.M{
			"$set": bson.M{
				"company_id":   device.CompanyID,
				"company_name": device.CompanyName,
			},
			"$unset": bson.M{
				"owner_id":   1,
				"owner_name": 1,
				"room_id":    1,
				"room_name":  1,
			}}); err != nil {
		return err
	}
	return nil
}

// Verify device update info
func (device *Device) Verify() bool {
	device.UUID = strings.TrimSpace(device.UUID)
	device.IMSI = strings.TrimSpace(device.IMSI)
	device.Mac = strings.TrimSpace(device.Mac)

	if device.UUID == "" || device.IMSI == "" {
		return false
	}

	return true
}

/*
// UploadByUUID a device
func (device *Device) UploadByUUID(db *mgo.Session) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if _, err := c.Upsert(bson.M{"uuid": device.UUID},
		bson.M{"$set": bson.M{
			"uuid":         device.UUID,
			"imsi":         device.IMSI,
			"mac":          device.Mac,
			"company_id":   device.CompanyID,
			"company_name": device.CompanyName,
			"sim_number":   device.SIMNumber}}); err != nil {
		return err
	}
	return nil
}
*/

/*
// Register a device
func (device *Device) Register(db *mgo.Session) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if _, err := c.Upsert(bson.M{"uuid": device.UUID}, bson.M{"$set": device}); err != nil {
		return err
	}
	return nil
}
*/

/*
// UpdateTotal a device total
func (device *Device) UpdateTotal(db *mgo.Session) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if err := c.UpdateId(device.ID,
		bson.M{"$set": bson.M{
			"total_gps":	device.TotalGPS,
			"total_beh":    device.TotalBeh,
		}}); err != nil {
		return err
	}
	return nil
}
*/

// Update a device
func (device *Device) UpdateDescription(db *mgo.Session) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if err := c.UpdateId(device.ID,
		bson.M{"$set": bson.M{
			"description": device.Description}}); err != nil {
		return err
	}
	return nil
}

func (device *Device) UpdateMotion(db *mgo.Session) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if err := c.UpdateId(device.ID,
		bson.M{"$set": bson.M{
			"motion": device.Motion,
		}}); err != nil {
		return err
	}
	return nil
}

func (device *Device) UpdateParameterVersionODBA(db *mgo.Session) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if err := c.UpdateId(device.ID,
		bson.M{"$set": bson.M{
			"odba_vers": device.ODBAVers,
		}}); err != nil {
		return err
	}
	return nil
}


func (device *Device) UpdateParameterVersionBeh(db *mgo.Session) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if err := c.UpdateId(device.ID,
		bson.M{"$set": bson.M{
			"behavior_vers": device.BehaviorVers,
		}}); err != nil {
		return err
	}
	return nil
}
func (device *Device) UpdateSurvive(db *mgo.Session) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if err := c.UpdateId(device.ID,
		bson.M{"$set": bson.M{
			"survive":  device.Survive,
			"activity": device.Activity,
		}}); err != nil {
		return err
	}
	return nil
}

func UpdateDeviceWithSurvive(db *mgo.Session, deviceID bson.ObjectId, survive int) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if err := c.UpdateId(deviceID,
		bson.M{"$set": bson.M{
			"survive": survive,
		}}); err != nil {
		return err
	}
	return nil
}

// UpdateVersion device hardware and firmware version
func (device *Device) UpdateVersion(db *mgo.Session) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if err := c.UpdateId(device.ID,
		bson.M{"$set": bson.M{
			"device_type":      device.DeviceType,
			"hardware_version": device.HardwareVersion,
			"firmware_version": device.FirmwareVersion,
		}}); err != nil {
		return err
	}
	return nil
}

func (device *Device) IncArea(db *mgo.Session, inc int) error {
	c := db.DB(DruidDB).C(DeviceCollection)

	_, err := c.UpsertId(device.ID, bson.M{"$inc": bson.M{"total_area": inc}})
	if err != nil {
		return err
	}

	return nil
}

func UpdateTotalAreaByDeviceID(db *mgo.Session, id bson.ObjectId, fix int) error {
	c := db.DB(DruidDB).C(DeviceCollection)

	count, err := CountDituAreaByDeviceID(db, id)
	if err != nil {
		return Error("Count ditu area by device failed:", err)
	}

	if err := c.UpdateId(id,
		bson.M{"$set": bson.M{
			"total_area": count + fix,
		}}); err != nil {
		return err
	}
	return nil
}

func (device *Device) UpdateTotalArea(db *mgo.Session) error {
	c := db.DB(DruidDB).C(DeviceCollection)

	count, err := CountDituAreaByDeviceID(db, device.ID)
	if err != nil {
		return Error("Count ditu area by device failed:", err)
	}

	if err := c.UpdateId(device.ID,
		bson.M{"$set": bson.M{
			"total_area": count,
		}}); err != nil {
		return err
	}
	return nil
}

/*
func (device *Device) UpdateNickName(db *mgo.Session) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if err := c.UpdateId(device.ID,
		bson.M{"$set": bson.M{
			"nickname": device.NickName,
		}}); err != nil {
		return err
	}
	return nil
}
*/

// UpdateBiological is find a setting by device uuid
func (device *Device) UpdateBiological(db *mgo.Session) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if err := c.UpdateId(device.ID,
		bson.M{"$set": bson.M{
		//	"nickname":        device.NickName,
			"biological_type": device.BiologicalType,
			"biological_id":   device.BiologicalID}}); err != nil {
		return err
	}
	return nil
}

// UpdateBiological is find a setting by device uuid
func (device *Device) RemoveBiological(db *mgo.Session) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if err := c.UpdateId(device.ID,
		bson.M{"$unset": bson.M{
			"biological_type": 1,
			"biological_id":   1}}); err != nil {
		return err
	}
	return nil
}

func (device *Device) UpsertByUUID(db *mgo.Session) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if _, err := c.Upsert(bson.M{"uuid": device.UUID}, bson.M{"$set": device}); err != nil {
		return err
	}
	return nil
}

// Register a device
func (device *Device) Register(db *mgo.Session) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if _, err := MongoAtomInsert(c, bson.M{"uuid": device.UUID}, device, device); err != nil {
		return err
	}
	return nil
}

// UpdateSIM a device
func (device *Device) UpdateSIM(db *mgo.Session) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if err := c.UpdateId(device.ID,
		bson.M{"$set": bson.M{
			"sim_number": device.SIMNumber,
			"updated_at": time.Now(),
		}}); err != nil {
		return err
	}
	return nil
}

// UpdateStatus a device
func (device *Device) UpdateStatus(db *mgo.Session) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if err := c.UpdateId(device.ID,
		bson.M{"$set": bson.M{
			"imsi":                    device.IMSI,
			"mac":                    device.Mac,
			"sim_number":              device.SIMNumber,
			"device_type":             device.DeviceType,
			"firmware_version":        device.FirmwareVersion,
			"hardware_version":        device.HardwareVersion,
			"battery_voltage":         device.BatteryVoltage,
			"signal_strength":         device.SignalStrength,
			"bit_error_rate":          device.BitErrorRate,
			"radio_access_technology": device.RadioAccessTechnology,
			"network_operator":        device.NetworkOperator,
			"updated_at":              time.Now(),
		}}); err != nil {
		return err
	}
	return nil
}

func (device *Device) RegisterExists(db *mgo.Session) error {
	c := db.DB(DruidDB).C(DeviceCollection)

	change := mgo.Change{
		Update:    bson.M{"$set": device},
		Upsert:    true,
		ReturnNew: true,
	}

	info, err := c.Find(bson.M{"_id": device.ID}).Apply(change, device)
	if err != nil {
		return err
	}

	return CheckChangeInfo(info)
}

// DeviceUpdateAt update updateAt
func DeviceUpdateAt(db *mgo.Session, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(DeviceCollection)

	if err := c.UpdateId(id, bson.M{"$set": bson.M{"updated_at": time.Now(), "lost_time": 0}}); err != nil {
		return err
	}
	return nil
}

func (device *Device) UnMarkSurvive(db *mgo.Session) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if err := c.UpdateId(device.ID,
		bson.M{"$unset": bson.M{"survive": 1, "survive_time": 1}}); err != nil {
		return ErrorConvert(err)
	}
	return nil
}

func (device *Device) MarkSurvive(db *mgo.Session, survive int, t *time.Time) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if err := c.UpdateId(device.ID,
		bson.M{"$set": bson.M{"survive": survive, "survive_time": t}}); err != nil {
		return ErrorConvert(err)
	}
	return nil
}

func (device *Device) CheckBeginTime(begin *time.Time) *time.Time {
	if begin == nil {
		return nil
	}
	if device.StockTime != nil && begin.Before(*(device.StockTime)) {
		begin = device.StockTime
	}
	if device.AttachedAt != nil && begin.Before(*(device.AttachedAt)) {
		begin = device.AttachedAt
	}

	return begin
}

func (device *Device) GetBeginTime(last string) *time.Time {
	var begin time.Time
	if device.UpdatedAt != nil {
		begin = *device.UpdatedAt
	} else {
		begin = time.Now()
	}

	begin = begin.AddDate(0, 0, -7)

	return device.CheckBeginTime(&begin)
}

/*

func (device *Device)UpdateCurrentFinish(db *mgo.Session) error {
	curr := false
	if device.TodayBeh == 1 {
		if device.LastBehavior == nil {
			device.TodayBeh = 0
			curr = true
		} else if !IsCurrentFinish(device.LastBehavior.Timestamp) {
			device.TodayBeh = 0
			curr = true
		}
	}

	if device.TodayGPS == 1 {
		if device.LastGPS == nil {
			device.TodayGPS = 0
			curr = true
		} else if !IsCurrentFinish(device.LastGPS.Timestamp) {
			device.TodayGPS = 0
			curr = true
		}
	}

	if curr {
		return device.UpdateFinishTag(db)
	}

	return nil
}

*/

func (device *Device) GetBiologicalRef() *mgo.DBRef {
	if device.BiologicalID == "" || device.BiologicalType == "" {
		return nil
	}
	return &mgo.DBRef{
		Collection: device.BiologicalType,
		Id:         device.BiologicalID,
		Database:   DruidDB,
	}
}

func (device *Device) AddOwner(db *mgo.Session, user *User) error {
	c := db.DB(DruidDB).C(DeviceCollection)

	if err := c.UpdateId(device.ID,
		bson.M{
			"$set": bson.M{
				"owner_id":     user.ID,
				"owner_name":   user.UserName,
				"company_id":   user.CompanyID,
				"company_name": user.CompanyName},
			"$unset": bson.M{
				"room_id":   1,
				"room_name": 1}}); err != nil {
		return ErrorConvert(err)
	}
	return nil
}

func (device *Device) UpdateDataStatus(db *mgo.Session, m bson.M) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if err := c.UpdateId(device.ID,
		bson.M{"$set": m}); err != nil {
		return err
	}
	return nil
}

func UpdateTotalAreaByDevices(db *mgo.Session, ids []bson.ObjectId, inc int) error {
	c := db.DB(DruidDB).C(DeviceCollection)

	_, err := c.Upsert(bson.M{"_id": bson.M{"$in": ids}}, bson.M{"$inc": bson.M{"total_area": inc}})
	if err != nil {
		return err
	}

	return nil
}

func GetDeviceQueue(db *mgo.Session) *mgo.Bulk {
	c := db.DB(DruidDB).C(DeviceCollection)
	return c.Bulk()
}

func RemoveUserFromDevices(db *mgo.Session, ids []bson.ObjectId) error {
	c := db.DB(DruidDB).C(DeviceCollection)

	if err := c.Update(
		bson.M{"_id": bson.M{"$in": ids}},
		bson.M{"$unset": bson.M{
			"owner_id":   1,
			"owner_name": 1,
			"room_id":    1,
			"room_name":  1,
		}}); err != nil {
		return ErrorConvert(err)
	}

	return nil
}

func CleanOwnerFromDevice(db *mgo.Session, ids []bson.ObjectId) error {
	if err := RemoveOwnerFromBiologicalByOwnerID(db, ids); err != nil {
		return Error("Remove biological owner by devices id failed:", err)
	}

	if err := DeleteDituHistoryByDeviceID(db, ids); err != nil {
		return Error("Remove area by user failed:", err)
	}

	if err := RemoveOwnerFromDeviceByDeviceID(db, ids); err != nil {
		return Error("Remove owner devices failed:", err)
	}

	return nil
}

// ListDeviceByIDs list many device by ids
func ListDeviceByIDs(db *mgo.Session, ids []bson.ObjectId, se bson.M) ([]*Device, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	result := []*Device{}
	return result, MongoList(c, bson.M{"_id": bson.M{"$in": ids}}, &result, nil, se)
}

func AddOwnerToDevices(db *mgo.Session, user *User, ids []bson.ObjectId) error {
	c := db.DB(DruidDB).C(DeviceCollection)

	if _, err := c.UpdateAll(
		bson.M{
			"_id": bson.M{"$in": ids}},
		bson.M{
			"$set": bson.M{
				"owner_id":   user.ID,
				"owner_name": user.UserName,
			}}); err != nil {
		return ErrorConvert(err)
	}
	return nil
}

func RemoveOwnerFromDeviceByOwnerID(db *mgo.Session, ownerID bson.ObjectId) error {
	c := db.DB(DruidDB).C(DeviceCollection)

	if _, err := c.UpdateAll(
		bson.M{"owner_id": ownerID},
		bson.M{
			"$unset": bson.M{
				"owner_id":   1,
				"owner_name": 1,
				"room_id":    1,
				"room_name":  1,
				"total_area": 1,
			}}); err != nil {
		return ErrorConvert(err)
	}
	return nil
}

func RemoveOwnerFromDeviceByDeviceID(db *mgo.Session, ids []bson.ObjectId) error {
	c := db.DB(DruidDB).C(DeviceCollection)

	if _, err := c.UpdateAll(
		bson.M{"_id": bson.M{"$in": ids}},
		bson.M{
			"$unset": bson.M{
				"owner_id":   1,
				"owner_name": 1,
				"room_id":    1,
				"room_name":  1,
				"total_area": 1,
			}}); err != nil {
		return ErrorConvert(err)
	}
	return nil
}

func CountDeviceOwner(db *mgo.Session, ids []bson.ObjectId) ([]*User, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	users := []*User{}
	pipe := c.Pipe([]bson.M{
		bson.M{"$match": bson.M{"owner_id": bson.M{"$in": ids}}},
		bson.M{
			"$group": bson.M{
				"_id":          "$owner_id",
				"device_count": bson.M{"$sum": 1}}}})

	if err := pipe.All(&users); err != nil {
		return users, ErrorConvert(err)
	}
	return users, nil
}

func CountOwnerGroup(db *mgo.Session, companyID bson.ObjectId) ([]*User, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	users := []*User{}
	pipe := c.Pipe([]bson.M{
		bson.M{"$match": bson.M{"company_id": companyID, "owner_id": bson.M{"$ne": nil}}},
		bson.M{
			"$group": bson.M{
				"_id":          "$owner_id",
				"device_count": bson.M{"$sum": 1}}}})

	if err := pipe.All(&users); err != nil {
		return users, ErrorConvert(err)
	}
	return users, nil
}

/*
func PullDituAreaFromDevices(db *mgo.Session, devices []bson.ObjectId, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(DeviceCollection)

	if _, err := c.UpdateAll(bson.M{"_id": bson.M{"$in": devices}}, bson.M{"$pull": bson.M{"ditu_area": bson.M{"_id": id}}}); err != nil {
		return ErrorConvert(err)
	}
	return nil
}

func CleanDevicesDituAreaByIDs(db *mgo.Session, ids []bson.ObjectId) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if _, err := c.UpdateAll(bson.M{"_id": bson.M{"$in": ids}}, bson.M{"$unset": bson.M{"ditu_area": 1}}); err != nil {
		return err
	}
	return nil
}
*/

// FindDeviceByUUID is find a setting by device uuid
func FindDeviceByUUID(db *mgo.Session, uuid string, se bson.M) (*Device, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	device := Device{}
	if err := MongoFind(c, bson.M{"uuid": uuid}, &device, "", nil); err != nil {
		return nil, ErrorConvert(err)
	}
	return &device, nil
}

// CheckDeviceByUUID is find a setting by device uuid
func CheckDeviceByUUID(db *mgo.Session, uuid string, se bson.M) (*Device, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	device := Device{}
	if err := c.Find(bson.M{"uuid": uuid}).Select(se).One(&device); err != nil {
		return nil, err
	}
	return &device, nil
}

// CheckValidDeviceByUUID is find a valid device by uuid
func CheckValidDeviceByUUID(db *mgo.Session, uuid string, se bson.M) (*Device, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	device := Device{}
	if err := c.Find(bson.M{"uuid": uuid, "deleted_at": nil}).Select(se).One(&device); err != nil {
		return nil, err
	}
	return &device, nil
}

// CheckValidDeviceByID find one device by id
func CheckValidDeviceByID(db *mgo.Session, id bson.ObjectId, se bson.M) (*Device, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	device := Device{}
	if err := c.Find(bson.M{"_id": id, "deleted_at": nil}).Select(se).One(&device); err != nil {
		return nil, err
	}
	return &device, nil
}

// FindValidDeviceByID find one device by id
func FindValidDeviceByID(db *mgo.Session, id bson.ObjectId, se bson.M) (*Device, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	device := Device{}
	if err := c.Find(bson.M{"_id": id, "deleted_at": nil}).Select(se).One(&device); err != nil {
		return nil, ErrorConvert(err)
	}
	return &device, nil
}

// FindDeviceByID find one device by id
func FindDeviceByID(db *mgo.Session, id bson.ObjectId, se bson.M) (*Device, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	device := Device{}
	if err := c.Find(bson.M{"_id": id}).Select(se).One(&device); err != nil {
		return nil, err
	}
	return &device, nil
}

// FindDeviceByIDs find one device by id
func FindDeviceByIDs(db *mgo.Session, ids []bson.ObjectId, se bson.M) ([]*Device, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	device := []*Device{}
	if err := c.Find(bson.M{"_id": bson.M{"$in": ids}}).Select(se).All(&device); err != nil {
		return device, ErrorConvert(err)
	}
	return device, nil
}

// CheckDevicesByManyID find one device by id
func CheckDevicesByManyID(db *mgo.Session, ids []bson.ObjectId, se bson.M) ([]*Device, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	devices := []*Device{}
	if err := c.Find(bson.M{"_id": bson.M{"$in": ids}, "deleted_at": nil}).Select(se).All(&devices); err != nil {
		return nil, err
	}
	return devices, nil
}

func CheckDevicesByManyUUID(db *mgo.Session, ids []string, se bson.M) ([]*Device, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	devices := []*Device{}
	if err := c.Find(bson.M{"uuid": bson.M{"$in": ids}, "deleted_at": nil}).Select(se).All(&devices); err != nil {
		return nil, err
	}
	return devices, nil
}

// DeleteDeviceByID delete one device by id
func DeleteDeviceByID(db *mgo.Session, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	return MongoDelete(c, bson.M{"_id": id})
}

// DestroyDeviceByID destroy one device by id when device is deleted
func DestroyDeviceByID(db *mgo.Session, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	return MongoDestroy(c, bson.M{"_id": id, "deleted_at": bson.M{"$ne": nil}})
}

// ListDeviceByFilter all of company devices
func ListDeviceByFilter(db *mgo.Session, m bson.M, rp *RequestParameter, se bson.M) ([]*Device, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	devices := []*Device{}
	return devices, MongoList(c, m, &devices, rp, se)
}

// CountDeviceByFilter get count device of company
func CountDeviceByFilter(db *mgo.Session, m bson.M) (count int, err error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	return MongoCount(c, m)
}

// ListDeviceByCompanyID all of company devices
func ListDeviceByCompanyID(db *mgo.Session, id bson.ObjectId, rp *RequestParameter, se bson.M) ([]*Device, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	devices := []*Device{}
	return devices, MongoList(c, bson.M{"company_id": id}, &devices, rp, se)
}

// ListDeviceByOwnerID list all of owner devices
func ListDeviceByOwnerID(db *mgo.Session, ownerID bson.ObjectId, se bson.M) ([]*Device, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	devices := []*Device{}
	return devices, MongoList(c, bson.M{"owner_id": ownerID}, &devices, nil, se)
}

// ListDeviceByOwner all of company devices
func ListDeviceByOwner(db *mgo.Session, owner string, se bson.M) ([]*Device, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	devices := []*Device{}
	return devices, MongoList(c, bson.M{"owner_name": owner}, &devices, nil, se)
}

func CountDeviceByCompanyID(db *mgo.Session, id bson.ObjectId) (count int, err error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	return MongoCount(c, bson.M{"company_id": id})
}

// TotalDevice count version count
func TotalDevice(db *mgo.Session) ([]*User, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	d := []*User{}
	pipe := c.Pipe([]bson.M{
		bson.M{
			"$group": bson.M{
				"_id":          "$owner_id",
				"total_device": bson.M{"$sum": 1},
			}}})

	// aggregate([{$sort: {updated_at: -1}}, {$group:{_id:"$device_id", count: {$sum:1}, update: {$first: "$updated_at"}}}])
	if err := pipe.All(&d); err != nil {
		return d, ErrorConvert(err)
	}
	return d, nil
}

// DeviceCount get count device
func DeviceCount(db *mgo.Session) (count int, err error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	return MongoCount(c, bson.M{"deleted_at": nil})
}

// DeviceDeletedCount get deleted count of device
func DeviceDeletedCount(db *mgo.Session) (count int, err error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	return MongoCount(c, bson.M{"deleted_at": bson.M{"$ne": nil}})
}

// ListIdleDevice all of devices
func ListIdleDevice(db *mgo.Session, rp *RequestParameter, se bson.M) ([]*Device, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	devices := []*Device{}

	return devices, MongoList(c, bson.M{"deleted_at": nil, "company_id": nil}, &devices, rp, se)
}

// DeviceIdleCount get count idle device
func DeviceIdleCount(db *mgo.Session) (count int, err error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	return MongoCount(c, bson.M{"deleted_at": nil, "company_id": nil})
}

// ListDevice all of devices
func ListDevice(db *mgo.Session, rp *RequestParameter, se bson.M) ([]*Device, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	devices := []*Device{}

	return devices, MongoList(c, bson.M{"deleted_at": nil}, &devices, rp, se)
}

// ListAllDevice all of devices
func ListAllDevice(db *mgo.Session, rp *RequestParameter, se bson.M) ([]*Device, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	devices := []*Device{}

	return devices, MongoList(c, nil, &devices, rp, se)
}

// ListDeviceByDeleted list all of deleted devices
func ListDeviceByDeleted(db *mgo.Session, rp *RequestParameter, se bson.M) ([]*Device, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	devices := []*Device{}

	return devices, MongoList(c, bson.M{"deleted_at": bson.M{"$ne": nil}}, &devices, rp, se)
}

// SearchDevice find a device
func SearchDevice(db *mgo.Session, m bson.M, rp *RequestParameter, se bson.M) ([]*Device, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	devices := []*Device{}

	return devices, MongoList(c, m, &devices, rp, se)
}

func CountDeviceMergeFinalMatchByFilter(db *mgo.Session, m bson.M, fm bson.M) (count int, err error) {
	c := db.DB(DruidDB).C(DeviceCollection)

	args := &MergeArg{
		Match:        m,
		FinalMatch:        fm,
		From:         CattleCollection,
		LocalField:   "_id",
		ForeignField: "device_id",
		As:           "data",
		AddField:     bson.M{"description": bson.M{"$arrayElemAt": InterfaceArray("$data.description", 0)}},
	}

	return MongoCountByFinalMatch(c, args)
}

// SearchDeviceCount search device count
func SearchDeviceCount(db *mgo.Session, m bson.M) (count int, err error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	return MongoCount(c, m)
}

// GetDeviceIDByUUID is find a setting by device uuid
func GetDeviceIDByUUID(db *mgo.Session, uuid string, se bson.M) *bson.ObjectId {
	device, _ := FindDeviceByUUID(db, uuid, se)
	if device != nil {
		return &device.ID
	}
	return nil
}

func DeviceLockTimeout(db *mgo.Session, id bson.ObjectId, u time.Time) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	return MongoLockTimeout(c, bson.M{"_id": id}, u)
}

func DeviceLock(db *mgo.Session, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	return MongoLock(c, bson.M{"_id": id})
}

func DeviceUnLock(db *mgo.Session, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	return MongoUnLock(c, bson.M{"_id": id})
}

func DevicesUnLock(db *mgo.Session, ids []bson.ObjectId) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	return MongoUnLock(c, bson.M{"_id": bson.M{"$in": ids}})
}

// RecoverDeviceByID one device by id
func RecoverDeviceByID(db *mgo.Session, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(DeviceCollection)

	return MongoRecover(c, bson.M{"_id": id})
}

func IdleDeviceByCompanyID(db *mgo.Session, company_id bson.ObjectId) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if _, err := c.UpdateAll(bson.M{"company_id": company_id}, bson.M{"$unset": bson.M{"company_id": 1, "company_name": 1}}); err != nil {
		return ErrorConvert(err)
	}
	return nil
}

/*
func CleanDevicesAreaByCompanyID(db *mgo.Session, companyID bson.ObjectId) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if _, err := c.UpdateAll(bson.M{"company_id": companyID}, bson.M{"$unset": bson.M{"ditu_area": 1}}); err != nil {
		return err
	}
	return nil
}
*/

func FreeDeviceByRoomID(db *mgo.Session, room_id bson.ObjectId) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if _, err := c.UpdateAll(
		bson.M{"room_id": room_id},
		bson.M{
			"$unset": bson.M{
				"room_id":   1,
				"room_name": 1}}); err != nil {
		return ErrorConvert(err)
	}
	return nil
}

// ListDeviceByRoomID all of company devices
func ListDeviceByRoomID(db *mgo.Session, id bson.ObjectId, rp *RequestParameter, se bson.M) ([]*Device, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	devices := []*Device{}
	return devices, MongoList(c, bson.M{"room_id": id}, &devices, rp, se)
}

func CountDeviceByRoomID(db *mgo.Session, id bson.ObjectId, rp *RequestParameter) (int, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	return MongoCount(c, bson.M{"room_id": id})
}

func CountDeviceByUserID(db *mgo.Session, id bson.ObjectId) (int, error) {
	c := db.DB(DruidDB).C(DeviceCollection)
	return MongoCount(c, bson.M{"owner_id": id})
}

func UpdateDeviceByRoomID(db *mgo.Session, room_id bson.ObjectId, m bson.M) error {
	c := db.DB(DruidDB).C(DeviceCollection)
	if _, err := c.UpdateAll(bson.M{"room_id": room_id}, bson.M{"$set": m}); err != nil {
		return ErrorConvert(err)
	}
	return nil
}

func IsCurrentFinish(t *time.Time) bool {
	if math.Abs(time.Now().Sub(*t).Minutes()) <= CurrentFinishTimeout {
		return true
	}
	return false
}
