package models

import (
	"errors"
	"fmt"
)

type ErrorBody struct {
	Code  int    `json:"code"`
	Error string `json:"error"`
	Msg   string `json:"msg"`
}

func Error(v ...interface{}) error {
	return errors.New(fmt.Sprintln(v))
}



