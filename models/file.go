package models

import (
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"mime"
	"mime/multipart"
	"net/http"

	"gopkg.in/mgo.v2"
)

const (
	MaxFileSize = 1024 * 1024 * 2
)

const (
	FileTypeFirmware = "firmware"
	FileTypeImage = "image"
)

type File struct {
	ID   interface{} `json:"-" bson:"_id,omitempty"`
	Name string      `json:"name" bson:"name"`
	Size int         `json:"size" bson:"size"`
	MD5  string      `json:"md5" bson:"md5"`
	Data []byte      `json:"-" bson:"-"`
}

// Upload file to server
func (file *File) Upload(db *mgo.Session) error {
	f, err := db.DB(DruidDB).GridFS(FilesCollection).Create(file.Name)
	if err != nil {
		return err
	}

	n, err := f.Write(file.Data)
	if err != nil {
		f.Close()
		return err
	}

	if n != file.Size {
		f.Close()
		return Error("Insert file size error.", file.Size, n)
	}
	f.Close()

	file.ID = f.Id()
	file.MD5 = f.MD5()

	return nil
}

func ReadFile(db *mgo.Session, id interface{}) (*File, error) {
	f, err := db.DB(DruidDB).GridFS(FilesCollection).OpenId(id)
	if err != nil {
		return nil, err
	}

	defer f.Close()

	file := File{}
	file.Size = int(f.Size())
	if file.Size > MaxFileSize {
		return nil, errors.New("File size too large.")
	}
	file.Data = make([]byte, file.Size)

	n, err := f.Read(file.Data)
	if err != nil {
		return nil, err
	}
	file.Size = n
	file.Name = f.Name()
	file.MD5 = f.MD5()
	file.ID = f.Id()

	return &file, nil
}


func ReadFileByLimit(db *mgo.Session, id interface{}, offset int64, length int64) (*File, error) {
	f, err := db.DB(DruidDB).GridFS(FilesCollection).OpenId(id)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	pos, err := f.Seek(offset, 0)
	if err != nil || pos != offset {
		return nil, err
	}
	if length > f.Size() {
		return nil, errors.New("Request length too large.")
	}

	file := File{}
	file.Data = make([]byte, length)

	n, err := f.Read(file.Data)
	if err != nil {
		return nil, err
	}
	file.Size = n
	return &file, nil
	/*
	file.Size = n
	file.Name = f.Name()
	file.MD5 = f.MD5()

	return &file, nil
	*/
}

func DeleteFileByID(db *mgo.Session, id interface{}) error {
	if err := db.DB(DruidDB).GridFS(FilesCollection).RemoveId(id); err != nil {
	//	return err
		return ErrorConvert(err)
	}
	return nil
}

// MultipartParse parse multipart/related request http
func MultipartParse(r *http.Request, v interface{}) ([]byte, error) {
	mediaType, params, err := mime.ParseMediaType(r.Header.Get("Content-Type"))
	if err != nil {
		return nil, errors.New("Request content type is invalid.")
	}

	//	body, err := ioutil.ReadAll(r.Body)
	//	fmt.Println(string(body))

	p1 := false
	if mediaType == "multipart/related" {
		mr := multipart.NewReader(r.Body, params["boundary"])

		//		body, err := ioutil.ReadAll(mr)
		//		fmt.Println(string(body))
		//		return nil, err

		for {
			p, err := mr.NextPart()
			if err == io.EOF {
				break
			}
			if err != nil {
				return nil, err
			}
			slurp, err := ioutil.ReadAll(p)
			if err != nil {
				return nil, err
			}

			switch p.Header.Get("Content-Type") {
			case "application/json; charset=UTF-8":
				if err := json.Unmarshal(slurp, v); err != nil {
					return nil, err
				}
				p1 = true
			case "application/octet-stream":
				if p1 == true {
					return slurp, nil
				}
			default:
				return nil, errors.New("Unknown body content type: " + p.Header.Get("Content-Type"))
			}
		}
	}

	return nil, errors.New("Unknown content type: " + mediaType)
}

