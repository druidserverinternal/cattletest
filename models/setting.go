package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"time"
	
	"druid/public"
)

const (
	VoltageThresholdMin     =   3.7 
	VoltageThresholdMax     =   4.0 

	SamplingEnable      =   1   
	SamplingDisable     =   2   

	EnvFreqMin      =   60  
	EnvFreqMax      =   3600*24*7

	BehFreqMin      =   30 
	BehFreqMax      =   3600

	GprsFreqMin     =   300 
	GprsFreqMax     =   3600*24*30

	GprsTypeInterval		=	0
	GprsTypeTable			=	1

	CUCCSpNumber	=	"+8614528001111"
	CUCCIotSpNumber	=	"106550010646"
	CMCCIotSpNumber	=	"1064899051201"
)

// Setting is device setting from mongodb
type Setting struct {
	ID                       bson.ObjectId `json:"id" bson:"_id,omitempty"`
	UUID                     string        `json:"uuid" bson:"uuid,omitempty"`
	UpdatedAt                *time.Time    `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
	UpdatedBy                bson.ObjectId `json:"updated_by" bson:"updated_by,omitempty"`
	DeviceID                 bson.ObjectId `json:"device_id" bson:"device_id,omitempty"`
//	CompanyID                bson.ObjectId `json:"company_id,omitempty" bson:"company_id,omitempty"`
//	CompanyName              string        `json:"company_name,omitempty" bson:"company_name,omitempty"`
	Mark                     int           `json:"mark,omitempty" bson:"mark,omitempty"`
	EnvSamplingMode          int32         `json:"env_sampling_mode" bson:"env_sampling_mode,omitempty"`
	EnvSamplingFreq          int32         `json:"env_sampling_freq" bson:"env_sampling_freq,omitempty"`
	BehaviorSamplingMode     int32         `json:"behavior_sampling_mode" bson:"behavior_sampling_mode,omitempty"`
	BehaviorSamplingFreq     int32         `json:"behavior_sampling_freq" bson:"behavior_sampling_freq,omitempty"`
	GprsMode                 int32         `json:"gprs_mode" bson:"gprs_mode,omitempty"`
	GprsFreq                 int32         `json:"gprs_freq" bson:"gprs_freq,omitempty"`
	EnvVoltageThreshold      float64       `json:"env_voltage_threshold" bson:"env_voltage_threshold,omitempty"`
	BehaviorVoltageThreshold float64       `json:"behavior_voltage_threshold" bson:"behavior_voltage_threshold,omitempty"`
	GprsVoltageThreshold     float64       `json:"gprs_voltage_threshold" bson:"gprs_voltage_threshold,omitempty"`
	OtaVoltageThreshold      float64       `json:"ota_voltage_threshold" bson:"ota_voltage_threshold,omitempty"`
	SpNumber                 string        `json:"sp_number,omitempty" bson:"sp_number,omitempty"`
	GprsVersion              int32         `json:"gprs_version" bson:"gprs_version,omitempty"`
	FirmwareID               bson.ObjectId `json:"firmware_id,omitempty" bson:"firmware_id,omitempty"`
	FirmwareVersion          int32         `json:"firmware_version,omitempty" bson:"firmware_version,omitempty"`
	DownloadedAt             *time.Time    `json:"downloaded_at,omitempty" bson:"downloaded_at"`
	SMSMode          int32         `json:"sms_mode,omitempty" bson:"sms_mode"`
	SMSFreq          int32         `json:"sms_freq,omitempty" bson:"sms_freq"`
	GpsAccuracyThresholdH    int32         `json:"gps_accuracy_threshold_h,omitempty" bson:"gps_accuracy_threshold_h"`
	GpsAccuracyThresholdV    int32         `json:"gps_accuracy_threshold_v,omitempty" bson:"gps_accuracy_threshold_v"`
	GpsFixTimeout   int32         `json:"gps_fix_timeout,omitempty" bson:"gps_fix_timeout"`
	GprsRetries		int32         `json:"gprs_retries,omitempty" bson:"gprs_retries"`
	GprsType   int32         `json:"gprs_type,omitempty" bson:"gprs_type"`
	GprsTimeTable   string         `json:"gprs_time_table,omitempty" bson:"gprs_time_table"`
	//	Redirect                 string        `json:"redirect,omitempty" bson:"redirect,omitempty"`

	GprsPowerSavingMode		int32	`json:"gprs_power_saving_mode" bson:"gprs_power_saving_mode"`
	GprsPowerSavingDistance	int32	`json:"gprs_power_saving_distance" bson:"gprs_power_saving_distance"`
	GprsPowerSavingTime		int32	`json:"gprs_power_saving_time" bson:"gprs_power_saving_time"`
}

type History struct {
	ID                       bson.ObjectId `json:"id" bson:"_id,omitempty"`
	UUID                     string        `json:"uuid" bson:"uuid,omitempty"`
	UpdatedAt                *time.Time    `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
	UpdatedBy                bson.ObjectId `json:"updated_by,omitempty" bson:"updated_by,omitempty"`
	DeviceID                 bson.ObjectId `json:"device_id,omitempty" bson:"device_id,omitempty"`
//	CompanyID                bson.ObjectId `json:"company_id,omitempty" bson:"company_id,omitempty"`
//	CompanyName              string        `json:"company_name,omitempty" bson:"company_name,omitempty"`
	Mark                     int           `json:"mark,omitempty" bson:"mark,omitempty"`
	EnvSamplingMode          int32         `json:"env_sampling_mode,omitempty" bson:"env_sampling_mode,omitempty"`
	EnvSamplingFreq          int32         `json:"env_sampling_freq,omitempty" bson:"env_sampling_freq,omitempty"`
	BehaviorSamplingMode     int32         `json:"behavior_sampling_mode,omitempty" bson:"behavior_sampling_mode,omitempty"`
	BehaviorSamplingFreq     int32         `json:"behavior_sampling_freq,omitempty" bson:"behavior_sampling_freq,omitempty"`
	GprsMode                 int32         `json:"gprs_mode,omitempty" bson:"gprs_mode,omitempty"`
	GprsFreq                 int32         `json:"gprs_freq,omitempty" bson:"gprs_freq,omitempty"`
	EnvVoltageThreshold      float64       `json:"env_voltage_threshold,omitempty" bson:"env_voltage_threshold,omitempty"`
	BehaviorVoltageThreshold float64       `json:"behavior_voltage_threshold,omitempty" bson:"behavior_voltage_threshold,omitempty"`
	GprsVoltageThreshold     float64       `json:"gprs_voltage_threshold,omitempty" bson:"gprs_voltage_threshold,omitempty"`
	OtaVoltageThreshold      float64       `json:"ota_voltage_threshold,omitempty" bson:"ota_voltage_threshold,omitempty"`
	SpNumber                 string        `json:"sp_number,omitempty" bson:"sp_number,omitempty"`
	GprsVersion              int32         `json:"gprs_version,omitempty" bson:"gprs_version,omitempty"`
	FirmwareID               bson.ObjectId `json:"firmware_id,omitempty" bson:"firmware_id,omitempty"`
	FirmwareVersion          int32         `json:"firmware_version,omitempty" bson:"firmware_version,omitempty"`
	DownloadedAt             *time.Time    `json:"downloaded_at,omitempty" bson:"downloaded_at,omitempty"`
	SMSMode          int32         `json:"sms_mode,omitempty" bson:"sms_mode,omitempty"`
	SMSFreq          int32         `json:"sms_freq,omitempty" bson:"sms_freq,omitempty"`
	GpsAccuracyThresholdH    int32         `json:"gps_accuracy_threshold_h,omitempty" bson:"gps_accuracy_threshold_h,omitempty"`
	GpsAccuracyThresholdV    int32         `json:"gps_accuracy_threshold_v,omitempty" bson:"gps_accuracy_threshold_v,omitempty"`
	GpsFixTimeout   int32         `json:"gps_fix_timeout,omitempty" bson:"gps_fix_timeout,omitempty"`
	GprsRetries		int32         `json:"gprs_retries,omitempty" bson:"gprs_retries,omitempty"`
	GprsType   int32         `json:"gprs_type,omitempty" bson:"gprs_type"`
	GprsTimeTable   string         `json:"gprs_time_table,omitempty" bson:"gprs_time_table,omitempty"`

	GprsPowerSavingMode		int32	`json:"gprs_power_saving_mode,omitempty" bson:"gprs_power_saving_mode,omitempty"`
	GprsPowerSavingDistance	int32	`json:"gprs_power_saving_distance,omitempty" bson:"gprs_power_saving_distance,omitempty"`
	GprsPowerSavingTime		int32	`json:"gprs_power_saving_time,omitempty" bson:"gprs_power_saving_time,omitempty"`
}


type SettingMany struct {
	Devices []bson.ObjectId `json:"devices" bson:"-"`
	Setting
	//	FirmwareID      bson.ObjectId   `json:"firmware_id,omitempty" bson:"firmware_id,omitempty"`
	//	FirmwareVersion int32           `json:"firmware_version,omitempty" bson:"firmware_version,omitempty"`
}

// Update devices setting firmware id
func ClearSettingFirmware(db *mgo.Session, deviceID bson.ObjectId) error {
	c := db.DB(DruidDB).C(SettingCollection)
	if err := c.Update(
		bson.M{"device_id": deviceID},
		bson.M{"$unset": bson.M{
			"firmware_id":      1,
			"firmware_version": 1}}); err != nil {
		return err
	}
	return nil
}

// Update devices setting firmware id
func (setting *SettingMany) UpdateFirmware(db *mgo.Session) error {
	c := db.DB(DruidDB).C(SettingCollection)
	if _, err := c.UpdateAll(
		bson.M{"device_id": bson.M{
			"$in": setting.Devices}},
		bson.M{
			"$set": bson.M{
				"updated_at":                 time.Now(),
				"firmware_id":      setting.FirmwareID,
				"firmware_version": setting.FirmwareVersion}}); err != nil {
		return err
	}
	return nil
}

func (setting *SettingMany) Update(db *mgo.Session, updatedBy bson.ObjectId) error {
	c := db.DB(DruidDB).C(SettingCollection)

	if _, err := c.UpdateAll(
		bson.M{"device_id": bson.M{
			"$in": setting.Devices}},
		bson.M{
			"$set": bson.M{
				"behavior_sampling_mode":     setting.BehaviorSamplingMode,
				"behavior_sampling_freq":     setting.BehaviorSamplingFreq,
				"behavior_voltage_threshold": setting.BehaviorVoltageThreshold,
				"env_sampling_mode":          setting.EnvSamplingMode,
				"env_sampling_freq":          setting.EnvSamplingFreq,
				"env_voltage_threshold":      setting.EnvVoltageThreshold,
//				"gprs_mode":                  setting.GprsMode,
				"gprs_freq":                  setting.GprsFreq,
				"gprs_voltage_threshold":     setting.GprsVoltageThreshold,
				"sp_number":                  setting.SpNumber,
				"ota_voltage_threshold":      setting.OtaVoltageThreshold,
				"downloaded_at":              nil,
				"updated_at":                 time.Now(),
				"updated_by":                 updatedBy,
				"sms_mode":					  setting.SMSMode,
				"sms_freq":					  setting.SMSFreq,
				"gps_accuracy_threshold_h":	  setting.GpsAccuracyThresholdH,
				"gps_accuracy_threshold_v":	  setting.GpsAccuracyThresholdV,
				"gps_fix_timeout":			  setting.GpsFixTimeout,
				"gprs_retries":				  setting.GprsRetries,
				"gprs_time_table":			  setting.GprsTimeTable,
				"gprs_type":				  setting.GprsType,
				"gprs_power_saving_mode":	  setting.GprsPowerSavingMode,
				"gprs_power_saving_distance":  setting.GprsPowerSavingDistance,
				"gprs_power_saving_time":	  setting.GprsPowerSavingTime,
			}}); err != nil {
		return err
	}
	return nil
}

/*
func (setting *SettingMany) UpdateSet(db *mgo.Session, updatedBy bson.ObjectId) error {
	c := db.DB(DruidDB).C(SettingCollection)

	now := time.Now()
	change := &mgo.Change{
		Update: bson.M{
			"$set": bson.M{
				"behavior_sampling_mode":     setting.BehaviorSamplingMode,
				"behavior_sampling_freq":     setting.BehaviorSamplingFreq,
				"behavior_voltage_threshold": setting.BehaviorVoltageThreshold,
				"env_sampling_mode":          setting.EnvSamplingMode,
				"env_sampling_freq":          setting.EnvSamplingFreq,
				"env_voltage_threshold":      setting.EnvVoltageThreshold,
//				"gprs_mode":                  setting.GprsMode,
				"gprs_freq":                  setting.GprsFreq,
				"gprs_voltage_threshold":     setting.GprsVoltageThreshold,
				"updated_at":                 &now,
				"updated_by":                 setting.UpdatedBy,
				"downloaded_at":              nil,
			
			}},
		ReturnNew: false,
	}

	if _, err := c.Find(bson.M{"device_id": bson.M{"$in": setting.Devices}}).Apply(*change, nil); err != nil {
		return err
	}

	return nil
}
*/

// UpdateSet device setting
func (setting *Setting) UpdateSet(db *mgo.Session) error {
	c := db.DB(DruidDB).C(SettingCollection)
	m := bson.M{
//		"behavior_sampling_mode":     setting.BehaviorSamplingMode,
//		"behavior_sampling_freq":     setting.BehaviorSamplingFreq,
//		"behavior_voltage_threshold": setting.BehaviorVoltageThreshold,
		"env_sampling_mode":          setting.EnvSamplingMode,
		"env_sampling_freq":          setting.EnvSamplingFreq,
		"env_voltage_threshold":      setting.EnvVoltageThreshold,
//		"gprs_mode":                  setting.GprsMode,
		"gprs_freq":                  setting.GprsFreq,
		"gprs_voltage_threshold":     setting.GprsVoltageThreshold,
		"updated_at":                 time.Now(),
		"updated_by":                 setting.UpdatedBy,
		"downloaded_at":              nil,
	}

	if err := c.Update(bson.M{"device_id": setting.DeviceID}, bson.M{"$set": m}); err != nil {
		return err
	}
	return nil
}

func (setting *Setting) Update(db *mgo.Session) error {
	c := db.DB(DruidDB).C(SettingCollection)
	if err := c.Update(bson.M{"device_id": setting.DeviceID}, bson.M{"$set": setting}); err != nil {
		return err
	}

	return nil
}

// UpdateSettingGPSMark device
func UpdateSettingMark(db *mgo.Session, id bson.ObjectId, mark int) error {
	c := db.DB(DruidDB).C(SettingCollection)
	if _, err := c.UpdateAll(
		bson.M{"device_id": id},
		bson.M{"$set": bson.M{
			"mark": mark}}); err != nil {
		return err
	}
	return nil
}

// FindAndModifySettingByM is find a setting by bson.M and modify it
func FindAndModifySettingByM(db *mgo.Session, m bson.M, change *mgo.Change) error {
	c := db.DB(DruidDB).C(SettingCollection)
	if _, err := c.Find(m).Apply(*change, nil); err != nil {
		return err
	}
	return nil
}

func GetCUCCIotSetting(device *Device) *Setting {
	return &Setting{
		DeviceID:                 device.ID,
		UUID:                     device.UUID,
		Mark:                     device.Mark,
//		CompanyID:                device.CompanyID,
//		CompanyName:              device.CompanyName,
		BehaviorSamplingMode:     SamplingEnable,
		BehaviorSamplingFreq:     600,
		EnvSamplingMode:          SamplingEnable,
		EnvSamplingFreq:          3600,
		GprsMode:                 SamplingEnable,
		GprsFreq:                 7200,
		GprsVersion:              2419200,
		EnvVoltageThreshold:      3.75,
		BehaviorVoltageThreshold: 3.75,
		GprsVoltageThreshold:     3.75,
		OtaVoltageThreshold:      3.75,
		SpNumber:                 CUCCSpNumber,
		UpdatedAt:                device.UpdatedAt,
		DownloadedAt:             nil,
		SMSMode	:				SamplingEnable,
		SMSFreq	:				86400,
		GpsAccuracyThresholdH:	20000,
		GpsAccuracyThresholdV:	30000,
		GpsFixTimeout:			150,
		GprsRetries:			1,
		GprsTimeTable:			"100000001000000010000000",
		GprsType:			    GprsTypeInterval,
		GprsPowerSavingMode:		2,
		GprsPowerSavingDistance:	200,
		GprsPowerSavingTime:		14400,
	}
}

func GetCUCCSetting(device *Device) *Setting {
	return &Setting{
		DeviceID:                 device.ID,
		UUID:                     device.UUID,
		Mark:                     device.Mark,
//		CompanyID:                device.CompanyID,
//		CompanyName:              device.CompanyName,
		BehaviorSamplingMode:     SamplingEnable,
		BehaviorSamplingFreq:     600,
		EnvSamplingMode:          SamplingEnable,
		EnvSamplingFreq:          3600,
		GprsMode:                 SamplingEnable,
		GprsFreq:                 7200,
		GprsVersion:              2419200,
		EnvVoltageThreshold:      3.75,
		BehaviorVoltageThreshold: 3.75,
		GprsVoltageThreshold:     3.75,
		OtaVoltageThreshold:      3.75,
		SpNumber:                 CUCCSpNumber,
		UpdatedAt:                device.UpdatedAt,
		DownloadedAt:             nil,
		SMSMode	:				SamplingEnable,
		SMSFreq	:				86400,
		GpsAccuracyThresholdH:	20000,
		GpsAccuracyThresholdV:	30000,
		GpsFixTimeout:			150,
		GprsRetries:			1,
		GprsTimeTable:			"100000001000000010000000",
		GprsType:			    GprsTypeInterval,
		GprsPowerSavingMode:		2,
		GprsPowerSavingDistance:	200,
		GprsPowerSavingTime:		14400,
	}


}

func GetCMCCIotSetting(device *Device) *Setting {
	return &Setting{
		DeviceID:                 device.ID,
		UUID:                     device.UUID,
		Mark:                     device.Mark,
//		CompanyID:                device.CompanyID,
//		CompanyName:              device.CompanyName,
		BehaviorSamplingMode:     SamplingEnable,
		BehaviorSamplingFreq:     600,
		EnvSamplingMode:          SamplingEnable,
		EnvSamplingFreq:          3600,
		GprsMode:                 SamplingEnable,
		GprsFreq:                 7200,
		GprsVersion:              2419200,
		EnvVoltageThreshold:      3.75,
		BehaviorVoltageThreshold: 3.75,
		GprsVoltageThreshold:     3.75,
		OtaVoltageThreshold:      3.75,
		SpNumber:                 CMCCIotSpNumber,
		UpdatedAt:                device.UpdatedAt,
		DownloadedAt:             nil,
		SMSMode	:				SamplingEnable,
		SMSFreq	:				86400,
		GpsAccuracyThresholdH:	20000,
		GpsAccuracyThresholdV:	30000,
		GpsFixTimeout:			150,
		GprsRetries:			1,
		GprsTimeTable:			"100000001000000010000000",
		GprsType:			    GprsTypeInterval,
		GprsPowerSavingMode:		2,
		GprsPowerSavingDistance:	200,
		GprsPowerSavingTime:		14400,
	}
}


func GetInitialSetting(device *Device, simType int32) *Setting {
	switch simType {
	case public.SIMTypeCuccIot:
		return GetCUCCIotSetting(device)
	case public.SIMTypeCmccIot:
		return GetCMCCIotSetting(device)
	default:
		return GetCUCCSetting(device)
	}
}

// SettingInit is init a new setting
func SettingInit(db *mgo.Session, device *Device, simType int32) (*Setting, error) {
	c := db.DB(DruidDB).C(SettingCollection)

	setting := GetInitialSetting(device, simType)

	if device.DeviceType == 3 {
		setting.GpsAccuracyThresholdH = 30000
	}
	if _, err := c.Upsert(bson.M{"device_id": device.ID}, bson.M{"$setOnInsert": setting}); err != nil {
		return nil, err
	}
	return setting, nil
}

// FindSettingByUUID is find a setting by uuid
func FindSettingByUUID(db *mgo.Session, uuid string) (*Setting, error) {
	c := db.DB(DruidDB).C(SettingCollection)
	setting := Setting{}
	if err := c.Find(bson.M{"uuid": uuid}).One(&setting); err != nil {
		return nil, ErrorConvert(err)
	}
	return &setting, nil
}

// FindSettingByDeviceID is find a setting by device id
func FindSettingByDeviceID(db *mgo.Session, id bson.ObjectId) (*Setting, error) {
	c := db.DB(DruidDB).C(SettingCollection)
	setting := Setting{}
	if err := c.Find(bson.M{"device_id": id}).One(&setting); err != nil {
		return nil, err
	}
	return &setting, nil
}

// CheckSettingByDeviceID is find a setting by device id
func CheckSettingByDeviceID(db *mgo.Session, id bson.ObjectId) (*Setting, error) {
	c := db.DB(DruidDB).C(SettingCollection)
	setting := Setting{}
	if err := c.Find(bson.M{"device_id": id}).One(&setting); err != nil {
		return nil, ErrorConvert(err)
	}
	return &setting, nil
}

// CheckSettingByManyDeviceID is find many setting by device id
func CheckSettingByManyDeviceID(db *mgo.Session, ids []bson.ObjectId) ([]*Setting, error) {
	c := db.DB(DruidDB).C(SettingCollection)
	setting := []*Setting{}
	if err := c.Find(bson.M{"device_id": bson.M{"$in": ids}}).All(&setting); err != nil {
		return setting, ErrorConvert(err)
	}
	return setting, nil
}

// FindSettingByID is find a setting by id
func FindSettingByID(db *mgo.Session, id bson.ObjectId) (*Setting, error) {
	c := db.DB(DruidDB).C(SettingCollection)
	setting := Setting{}
	if err := c.Find(bson.M{"_id": id}).One(&setting); err != nil {
		return nil, ErrorConvert(err)
	}
	return &setting, nil
}

// ListValidSetting all of setting
func ListValidSetting(db *mgo.Session, rp *RequestParameter) ([]*Setting, error) {
	c := db.DB(DruidDB).C(SettingCollection)
	setting := []*Setting{}
	return setting, MongoList(c, bson.M{"deleted_at": nil}, &setting, rp, nil)
}

// ListSettingByFilter all of company devices
func ListSettingByFilter(db *mgo.Session, m bson.M, rp *RequestParameter) ([]*Setting, error) {
	c := db.DB(DruidDB).C(SettingCollection)
	settings := []*Setting{}
	return settings, MongoList(c, m, &settings, rp, nil)
}

// CountSettingByFilter get count device of company
func CountSettingByFilter(db *mgo.Session, m bson.M) (count int, err error) {
	c := db.DB(DruidDB).C(SettingCollection)
	return MongoCount(c, m)
}

/*
// ListValidSettingByCompanyID all of setting
func ListValidSettingByCompanyID(db *mgo.Session, companyID bson.ObjectId, rp *RequestParameter) ([]*Setting, error) {
	c := db.DB(DruidDB).C(SettingCollection)
	setting := []*Setting{}
	return setting, MongoList(c, bson.M{"company_id": companyID, "deleted_at": nil}, &setting, rp, nil)
}
*/

// DeleteSettingByDeviceID all of setting
func DeleteSettingByDeviceID(db *mgo.Session, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(SettingCollection)
	return MongoDelete(c, bson.M{"device_id": id})
}

// DestroySettingByDeviceID all of setting
func DestroySettingByDeviceID(db *mgo.Session, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(SettingCollection)
	return MongoDestroy(c, bson.M{"device_id": id})
}

// GetSettingValidCount count all of setting
func GetSettingValidCount(db *mgo.Session) (count int, err error) {
	c := db.DB(DruidDB).C(SettingCollection)
	return MongoCount(c, bson.M{"deleted_at": nil})
}

/*
// GetSettingValidCountByCompanyID count all of setting
func GetSettingValidCountByCompanyID(db *mgo.Session, companyID bson.ObjectId) (count int, err error) {
	c := db.DB(DruidDB).C(SettingCollection)
	return MongoCount(c, bson.M{"company_id": companyID, "deleted_at": nil})
}
*/

func RecoverSettingByDeviceID(db *mgo.Session, id bson.ObjectId) error {
	c := db.DB(DruidDB).C(SettingCollection)
	return MongoRecover(c, bson.M{"device_id": id})
}

func MarkSettingDownload(db *mgo.Session, deviceID bson.ObjectId) error {
	c := db.DB(DruidDB).C(SettingCollection)
	if err := c.Update(
		bson.M{"device_id": deviceID, "downloaded_at": nil},
		bson.M{"$set": bson.M{"downloaded_at": time.Now()}}); err != nil {
		return ErrorConvert(err)
	}
	return nil
}

func (setting *Setting)MarkDownload(db *mgo.Session) error {
	c := db.DB(DruidDB).C(SettingCollection)
	if err := c.UpdateId(setting.ID,
		bson.M{"$set": bson.M{"downloaded_at": setting.DownloadedAt }}); err != nil {
		return ErrorConvert(err)
	}
	return nil
}

func (setting *Setting)SaveHistory(db *mgo.Session) error {
	c := db.DB(DruidDB).C(SettingHistoryCollection)
	setting.ID = ""
	if err := c.Insert(setting); err != nil {
		return ErrorConvert(err)
	}
	return nil
}

func (setting *Setting)SaveFirmwareHistory(db *mgo.Session) error {
	c := db.DB(DruidDB).C(FirmwareHistoryCollection)

	if err := c.Insert(bson.M{
		"device_id"	:	setting.DeviceID,
		"uuid"		:	setting.UUID,
		"mark"		:	setting.Mark,
		"firmware_version":	setting.FirmwareVersion,
		"updated_at":	time.Now(),
	}); err != nil {
		return ErrorConvert(err)
	}
	return nil
}

func ListFirmwareHistoryByDeviceID(db *mgo.Session, id bson.ObjectId, rp *RequestParameter) ([]*History, error) {
	c := db.DB(DruidDB).C(FirmwareHistoryCollection)
	settings := []*History{}
	return settings, MongoList(c, bson.M{"device_id": id}, &settings, rp, nil)
}

func CountFirmwareHistoryByDeviceID(db *mgo.Session, id bson.ObjectId) (count int, err error) {
	c := db.DB(DruidDB).C(FirmwareHistoryCollection)
	return MongoCount(c, bson.M{"device_id": id})
}

func ListFirmwareHistory(db *mgo.Session, rp *RequestParameter) ([]*History, error) {
	c := db.DB(DruidDB).C(FirmwareHistoryCollection)
	setting := []*History{}
	return setting, MongoList(c, nil, &setting, rp, nil)
}

func CountFirmwareHistory(db *mgo.Session) (count int, err error) {
	c := db.DB(DruidDB).C(FirmwareHistoryCollection)
	return MongoCount(c, nil)
}


func ListSettingHistoryByDeviceID(db *mgo.Session, id bson.ObjectId, rp *RequestParameter) ([]*History, error) {
	c := db.DB(DruidDB).C(SettingHistoryCollection)
	settings := []*History{}
	return settings, MongoList(c, bson.M{"device_id": id}, &settings, rp, nil)
}

func CountSettingHistoryByDeviceID(db *mgo.Session, id bson.ObjectId) (count int, err error) {
	c := db.DB(DruidDB).C(SettingHistoryCollection)
	return MongoCount(c, bson.M{"device_id": id})
}

func ListSettingHistory(db *mgo.Session, rp *RequestParameter) ([]*History, error) {
	c := db.DB(DruidDB).C(SettingHistoryCollection)
	setting := []*History{}
	return setting, MongoList(c, nil, &setting, rp, nil)
}

func CountSettingHistory(db *mgo.Session) (count int, err error) {
	c := db.DB(DruidDB).C(SettingHistoryCollection)
	return MongoCount(c, nil)
}




