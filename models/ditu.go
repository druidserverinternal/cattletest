package models

import (
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"github.com/kellydunn/golang-geo"
)

type PointSearch [2]float64
type PolygonSearch [][2]float64

type DituSearch struct {
	Type    string            `json:"type,omitempty" bson:"type,omitempty"`
	Max     int               `json:"max,omitempty" bson:"max,omitempty"`
	Point   *PointSearch      `json:"point,omitempty" bson:"point,omitempty"`
	Polygon *[1]PolygonSearch `json:"polygon,omitempty" bson:"polygon,omitempty"`
}

const (
	DituAreaTypeRound   = "Round"
	DituAreaTypePolygon = "Polygon"
)

const (
	MaxDituArea     = 3
	MaxDituAreaName = 15
)

type Point struct {
	Lng float64 `json:"lng" bson:"lng"`
	Lat float64 `json:"lat" bson:"lat"`
}

type Polygon struct {
	Points []*Point `json:"points,omitempty" bson:"points,omitempty"`
}

const (
	DituAreaMsgTypeClose = 0
	DituAreaMsgTypeIn    = 1
	DituAreaMsgTypeOut   = 2
)

type DituArea struct {
	ID        bson.ObjectId `json:"id" bson:"_id,omitempty"`
	UpdatedAt *time.Time    `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
	AreaID    bson.ObjectId `json:"-" bson:"area_id,omitempty"`
	AreaName  string        `json:"area_name,omitempty" bson:"area_name,omitempty"`
	CompanyID bson.ObjectId `json:"company_id,omitempty" bson:"company_id,omitempty"`
	//	CompanyName     string        `json:"company_name,omitempty" bson:"company_name,omitempty"`
	//	UserID          bson.ObjectId `json:"user_id,omitempty" bson:"user_id,omitempty"`

	Type     string   `json:"type,omitempty" bson:"type,omitempty"`
	Distance int      `json:"distance,omitempty" bson:"distance,omitempty"`
	Point    *Point   `json:"point,omitempty" bson:"point,omitempty"`
	Polygon  *Polygon `json:"polygon,omitempty" bson:"polygon,omitempty"`

	TotalDevice int             `json:"total_device" bson:"total_device,omitempty"`
	DevicesID   []bson.ObjectId `json:"devices_id,omitempty" bson:"-"`
	Description string          `json:"description,omitempty" bson:"description,omitempty"`

	MsgType   int           `json:"msg_type,omitempty" bson:"msg_type,omitempty"`
	SendedAt  *time.Time    `json:"sended_at,omitempty" bson:"sended_at,omitempty"`
	OwnerID   bson.ObjectId `json:"owner_id" bson:"owner_id,omitempty"`
	OwnerName string        `json:"owner_name" bson:"owner_name,omitempty"`
}

func (ditu *DituArea) Create(db *mgo.Session) error {
	c := db.DB(DruidDB).C(DituAreaCollection)

	now := time.Now()
	ditu.UpdatedAt = &now

	if err := c.Insert(ditu); err != nil {
		return err
	}

	return nil
}

func (ditu *DituArea) Update(db *mgo.Session) error {
	c := db.DB(DruidDB).C(DituAreaCollection)

	if err := c.UpdateId(ditu.ID, bson.M{
		"$set": bson.M{
			"updated_at":  time.Now(),
			"area_name":   ditu.AreaName,
			"type":        ditu.Type,
			"msg_type":    ditu.MsgType,
			"distance":    ditu.Distance,
			"point":       ditu.Point,
			"polygon":     ditu.Polygon,
			"description": ditu.Description}}); err != nil {
		return err
	}

	return nil
}

func FindSlice(id bson.ObjectId, ids []bson.ObjectId) bool {
	for _, i := range ids {
		if id == i {
			return true
		}
	}
	return false
}

/*
func (ditu *DituArea) SendMessage(db *mgo.Session, device *Device) ( error) {
	dh, err := FindDituHistory(db, device.ID, ditu.ID)
	if err != nil {
		return err
	}

	if dh == nil || dh.SendedAt.Before(*ditu.UpdatedAt) {
		if err := SendAreaMessage(db, device, ditu); err != nil {
			return err
		}

		return MarkDituHistorySended(db, device.ID, ditu.ID)
	}

	return nil
}
*/

func (ditu *DituArea) AddDevice(db *mgo.Session, device *Device) error {
	dh := DituHistory{
		AreaID:   ditu.ID,
		AreaName: ditu.AreaName,
		DeviceID: device.ID,
		Mark:     device.Mark,
		OwnerID:  ditu.OwnerID,
	}
	return dh.Create(db)
}

func (ditu *DituArea) UpdateTotalDevice(db *mgo.Session) error {
	c := db.DB(DruidDB).C(DituAreaCollection)

	count, err := CountDeviceByAreaID(db, ditu.ID)
	if err != nil {
		return Error("Count ditu area by device failed:", err)
	}

	if err := c.UpdateId(ditu.ID,
		bson.M{"$set": bson.M{
			"total_device": count,
		}}); err != nil {
		return err
	}
	return nil
}

func DeleteDituAreaByOwnerID(db *mgo.Session, ownerID bson.ObjectId) error {
	c := db.DB(DruidDB).C(DituAreaCollection)

	if err := DeleteDituHistoryByOwnerID(db, ownerID); err != nil {
		return err
	}

	if _, err := c.RemoveAll(bson.M{"owner_id": ownerID}); err != nil {
		return ErrorConvert(err)
	}

	return nil
}
func CountDituOwnerGroup(db *mgo.Session, ownerID bson.ObjectId) ([]*User, error) {
	c := db.DB(DruidDB).C(DituAreaCollection)
	users := []*User{}
	pipe := c.Pipe([]bson.M{
		bson.M{"$match": bson.M{"owner_id": ownerID}},
		bson.M{
			"$group": bson.M{
				"_id":        "$owner_id",
				"ditu_count": bson.M{"$sum": 1}}}})

	if err := pipe.All(&users); err != nil {
		return users, ErrorConvert(err)
	}
	return users, nil
}

func FindDituAreaByID(db *mgo.Session, id bson.ObjectId) (*DituArea, error) {
	c := db.DB(DruidDB).C(DituAreaCollection)
	ditu := DituArea{}
	if err := c.FindId(id).One(&ditu); err != nil {
		return nil, err
	}
	return &ditu, nil
}

func ListDituAreaByOwnerID(db *mgo.Session, id bson.ObjectId, rp *RequestParameter) ([]*DituArea, error) {
	c := db.DB(DruidDB).C(DituAreaCollection)
	area := []*DituArea{}
	return area, MongoList(c, bson.M{"owner_id": id}, &area, rp, nil)
}

func CountDituAreaByOwnerID(db *mgo.Session, id bson.ObjectId) (count int, err error) {
	c := db.DB(DruidDB).C(DituAreaCollection)
	return MongoCount(c, bson.M{"owner_id": id})
}

func ListDituAreaByCompanyID(db *mgo.Session, id bson.ObjectId, rp *RequestParameter) ([]*DituArea, error) {
	c := db.DB(DruidDB).C(DituAreaCollection)
	area := []*DituArea{}
	return area, MongoList(c, bson.M{"company_id": id}, &area, rp, nil)
}

func ListDituAreaByDeviceID(db *mgo.Session, id bson.ObjectId, rp *RequestParameter, se bson.M) ([]*DituArea, error) {
	c := db.DB(DruidDB).C(DituAreaCollection)
	area := []*DituArea{}

	/*
	if err := MongoList(c, bson.M{"device_id": id}, &area, nil, nil); err != nil {
		return nil, err
	}

	for _, a := range area {
		ar, err := FindDituAreaByID(db, a.AreaID)
		if err != nil {
			return nil, err
		}
		a.AreaName = ar.AreaName
	}
	return area, nil
	*/

    args := &MergeArg{
        Match:        bson.M{"device_id": id},
        From:         DituAreaCollection,
        LocalField:   "area_id",
        ForeignField: "_id",
        As:           "data",
	}

	return area, MongoListMerge(c, args, &area, rp, se)

}

func CountDituAreaByCompanyID(db *mgo.Session, id bson.ObjectId) (count int, err error) {
	c := db.DB(DruidDB).C(DituAreaCollection)
	return MongoCount(c, bson.M{"company_id": id, "deleted_at": nil})
}

func FindviceByID(db *mgo.Session, id bson.ObjectId) (*DituArea, error) {
	c := db.DB(DruidDB).C(DituAreaCollection)
	area := DituArea{}
	if err := c.FindId(id).One(&area); err != nil {
		return nil, err
	}
	return &area, nil
}

func NewGeoPoint(p *Point) *geo.Point {
	return geo.NewPoint(p.Lat, p.Lng)
}

func NewGeoPolygon(po *Polygon) *geo.Polygon {
	var gp []*geo.Point
	for _, p := range po.Points {
		gp = append(gp, NewGeoPoint(p))
	}

	return geo.NewPolygon(gp)
}

func (ditu *DituArea) Contains(lng, lat float64) bool {
	switch ditu.Type {
	case DituAreaTypePolygon:
		geoPo := NewGeoPolygon(ditu.Polygon)
		geoP := NewGeoPoint(&Point{lng, lat})
		return geoPo.Contains(geoP)
	case DituAreaTypeRound:
		geoP1 := NewGeoPoint(&Point{lng, lat})
		geoP2 := NewGeoPoint(ditu.Point)
		dist := geoP1.GreatCircleDistance(geoP2) * 1000
		if dist <= float64(ditu.Distance) {
			return true
		}
	default:
		return false
	}

	return false
}

func (ditu *DituArea) CheckContains(lng, lat float64) bool {
	if ditu.MsgType == DituAreaMsgTypeIn {
		if ditu.Contains(lng, lat) {
			return true
		}
	} else if ditu.MsgType == DituAreaMsgTypeOut {
		if !ditu.Contains(lng, lat) {
			return true
		}
	}
	return false
}

/*
func (ditu *DituArea) RemoveDevices(db *mgo.Session, ids []bson.ObjectId) error {
	c := db.DB(DruidDB).C(DituAreaCollection)

	if err := DeleteFromDituHistory(db, ids, ditu.ID); err != nil {
		return err
	}

	if err := c.UpdateId(ditu.ID,
				bson.M{
					"$pullAll": bson.M{
						"devices_id": ids,
					},
				}); err != nil {
		return ErrorConvert(err)
	}

	return nil
}
*/

func (ditu *DituArea) AddDevices(db *mgo.Session, ids []bson.ObjectId) error {
	c := db.DB(DruidDB).C(DituAreaCollection)

	//  { $addToSet: { tags: { $each: [ "camera", "electronics", "accessories" ] } } }
	if err := c.UpdateId(ditu.ID,
		bson.M{
			"$addToSet": bson.M{
				"devices_id": bson.M{
					"$each": ids,
				},
			},
		}); err != nil {
		return err
	}

	return nil
}

func (ditu *DituArea) Delete(db *mgo.Session) error {
	c := db.DB(DruidDB).C(DituAreaCollection)

	if err := DeleteDituHistoryByFilter(db, bson.M{"area_id": ditu.ID}); err != nil {
		return Error("Delete ditu history failed.", err)
	}

	if err := c.RemoveId(ditu.ID); err != nil {
		return err
	}
	return nil
}

func GetDituQueue(db *mgo.Session) *mgo.Bulk {
	c := db.DB(DruidDB).C(DituAreaCollection)
	return c.Bulk()
}

/*
func RemoveDevicesFromDitu(db *mgo.Session, ids []bson.ObjectId) error {
	c := db.DB(DruidDB).C(DituAreaCollection)

	if err := CleanDituHistoryByDevicesID(db, ids); err != nil {
		return err
	}

	if _, err := c.UpdateAll(bson.M{"devices_id": bson.M{"$in": ids}}, bson.M{"$pullAll": bson.M{"devices_id": ids}}); err != nil {
		return ErrorConvert(err)
	}

	return nil
}
*/

func CleanDituAreaByCompanyID(db *mgo.Session, companyID bson.ObjectId) error {
	c := db.DB(DruidDB).C(DituAreaCollection)
	return MongoDestroy(c, bson.M{"company_id": companyID})
}
