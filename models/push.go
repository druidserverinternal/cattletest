package models

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"fmt"
)

const (
	PushClassExport		=	1
	PushClassAreaIn		=	2
	PushClassAreaOut	=	3
	PushClassDead		=	4

	PushClassBiologicalHigh	=	5
	PushClassBiologicalLow		=	6

	PushClassSignalWeak		=	7

	PushClassBiologicalStatic		=	8			// biological behavior static

	PushClassFinish		=	9
)

type PushSetting struct {
	ID				bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Class			int			  `json:"class" bson:"class"`
	AdminSwitch		bool		`json:"admin_switch" bson:"admin_switch"`
	UserSwitch		bool		`json:"user_switch" bson:"user_switch"`
	MsgEN			string		`json:"msg_en" bson:"msg_en"`
	MsgCN			string		`json:"msg_cn" bson:"msg_cn"`
}

func (ps *PushSetting) GetUsers(db *mgo.Session, userID, companyID bson.ObjectId) ([]bson.ObjectId) {
	if ps == nil || !ps.IsSend() {
		return nil
	}

	us := []bson.ObjectId{}
	if ps.AdminSwitch {
		users, err := FindUserByCompanyAdmin(db, companyID)
		if err != nil {
			return nil
		}

		for _, u := range users {
			us = append(us, u.ID)
		}
	}
	if ps.UserSwitch {
		if userID != "" {
			us = append(us, userID)
		}
	}

	if len(us) > 0 {
		return us
	}

	return nil
}

func (ps *PushSetting) GetMsgCN(args ...interface{}) string {
	if ps.MsgCN == "" {
		return ps.MsgCN
	}

	return fmt.Sprintf(ps.MsgCN, args...)
}

func (ps *PushSetting) GetMsgEN(args ...interface{}) string {
	if ps.MsgEN == "" {
		return ps.MsgEN
	}

	return fmt.Sprintf(ps.MsgEN, args...)
}

func (ps *PushSetting) IsSend() bool {
	if !ps.AdminSwitch && !ps.UserSwitch {
		return false
	}

	if ps.MsgEN == "" && ps.MsgCN == "" {
		return false
	}

	return true
}

func (ps *PushSetting) CheckClass() bool {
	if ps.Class < PushClassExport || ps.Class > PushClassFinish {
		return false
	}

	return true
}


func (ps *PushSetting) Create(db *mgo.Session) error {
	c := db.DB(DruidDB).C(PushSettingCollection)

	if err := c.Insert(ps); err != nil {
		return err
	}

	return nil
}

func (ps *PushSetting) Update(db *mgo.Session) error {
	c := db.DB(DruidDB).C(PushSettingCollection)

	if err := c.UpdateId(ps.ID, bson.M{
		"$set": bson.M{
			"admin_switch": ps.AdminSwitch,
			"user_switch":	ps.UserSwitch,
			"msg_en":	ps.MsgEN,
			"msg_cn":	ps.MsgCN,
		}}); err != nil {
		return err
	}

	return nil
}


func FindPushSettingByClass(db *mgo.Session, class int) (*PushSetting, error) {
	c := db.DB(DruidDB).C(PushSettingCollection)
	ps := &PushSetting{}

	if err := c.Find(bson.M{"class": class}).One(ps); err != nil {
		return nil, ErrorConvert(err)
	}

	return ps, nil
}

// ListPushSetting list all of push setting
func ListPushSetting(db *mgo.Session, rp *RequestParameter) ([]*PushSetting, error) {
    c := db.DB(DruidDB).C(PushSettingCollection)
    ps := []*PushSetting{}
    return ps, MongoList(c, nil, &ps, rp, nil)
}

func CountPushSetting(db *mgo.Session) (count int, err error) {
    c := db.DB(DruidDB).C(PushSettingCollection)
    return MongoCount(c, nil)
}

func FindPushSettingByID(db *mgo.Session, id bson.ObjectId) (*PushSetting, error) {
    c := db.DB(DruidDB).C(PushSettingCollection)
    ps := &PushSetting{}

    if err := c.FindId(id).One(ps); err != nil {
        return nil, ErrorConvert(err)
    }

    return ps, nil
}



