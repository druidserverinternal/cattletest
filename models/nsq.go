package models

import (
	"sync"
	"bytes"
	"encoding/gob"
//	"sync/atomic"
	"time"

	"github.com/nsqio/go-nsq"
	"github.com/spf13/viper"
//	log "github.com/sirupsen/logrus"
//	"log"
)

type NSQReader struct {
	sync.RWMutex
	topic        string
	channel      string
	ch			 chan []byte
	consumer     *nsq.Consumer
}

type NSQWriter struct {
	sync.RWMutex
//	bufrw        *bufio.ReadWriter
//	connectTime  time.Time
//	Address			 []byte chan
	Producers	map[string]*nsq.Producer
	buffer		bytes.Buffer
}

func (nr *NSQReader) HandleMessage(message *nsq.Message) error {
	nr.ch <- message.Body
	/*
	nr.Lock()
	nr.bufrw.Write(message.Body)
	nr.bufrw.WriteString("\n")
	nr.bufrw.Flush()
	nr.Unlock()
	atomic.AddUint64(&streamServer.messageCount, 1)
	*/
	return nil
}

func (nr *NSQReader) Close() {
	nr.Lock()
	defer nr.Unlock()

	nr.consumer.Stop()
}

func (nw *NSQWriter) Close() {
	nw.Lock()
	defer nw.Unlock()

	for _, p := range nw.Producers {
		p.Stop()
	}
}


func (nw *NSQWriter) SendObj(obj interface{}, topic string) error {
	nw.Lock()
	defer nw.Unlock()
	
	nw.buffer.Reset()
	if err := gob.NewEncoder(&nw.buffer).Encode(obj); err != nil {
		return Error("Encode gob failed:", err)
	}
	
	return nw.send(nw.buffer.Bytes(), topic)
}

func (nw *NSQWriter) send(data []byte, topic string) error {
	var err error
	for _, p := range nw.Producers {
		if err = p.Ping(); err != nil {
		//	log.Error("Check producer ping failed.", k, err)
			continue
		}
		err = p.Publish(topic, data)
		if err != nil {
		//	log.Error("Publish producer failed.", k, err)
			continue
		}
	}
//	atomic.AddUint64(&streamServer.messageCount, 1)

	return err
}

func NewNSQWriter(addrs []string) (*NSQWriter, error) {
	cfg := nsq.NewConfig()
	nw := &NSQWriter{
		Producers	:	make(map[string]*nsq.Producer),
	}

	var err2 error
	for _, addr := range addrs {
		producer, err := nsq.NewProducer(addr, cfg)
		if err != nil {
			err2 = err
	//		log.Error("Create nsq.Producer failed,", addr, err)
			continue
		} 
		producer.SetLogger(nil, nsq.LogLevelError)
		nw.Producers[addr] = producer
	}

	if len(nw.Producers) == 0 {
		return nil, Error("Init producers failed:", err2)
	}
	return nw, nil
}

func NewNSQReader(topicName, channelName string, ch chan []byte, addrs []string) (*NSQReader, error) {
	if len(addrs) == 0 {
		return nil, Error("Address is invalid.")
	}

	cfg := nsq.NewConfig()
	cfg.LookupdPollInterval = time.Second * 2
//	cfg.UserAgent = fmt.Sprintf("nsq_pubsub/%s go-nsq/%s", version.Binary, nsq.VERSION)
//	cfg.MaxInFlight = *maxInFlight
	r, err := nsq.NewConsumer(topicName, channelName, cfg)
	if err != nil {
		return nil, Error("Create consumer failed:", err)
	}
	r.SetLogger(nil, nsq.LogLevelError)

	nr := &NSQReader{
		topic:       topicName,
		channel:     channelName,
		consumer:    r,
		ch:			ch,
	}

	r.AddHandler(nr)

	if err := r.ConnectToNSQLookupds(addrs); err != nil {
		return nil, Error("Connect to nsqlookupds failed:", err, addrs)
	}

	return nr, nil
}

func SendMessageToNSQD(obj interface{}, topic string) error {
	addrs := StringSplit(viper.GetString("nsq.nsqd.address"), ", ")
	if len(addrs) == 0 {
		return Error("Nsqd address is invalid.")
	}

	nw, err := NewNSQWriter(addrs)
	if nw == nil {
		return Error("Init nsq writer failed.", err)
	}
	defer nw.Close()

	if err := nw.SendObj(obj, topic); err != nil {
		return Error("Send device to mq failed:", err)
	}

	return nil
}

