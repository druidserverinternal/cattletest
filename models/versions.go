package models

import (
	"fmt"
	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	APK_Status_Up_ing         = 1
	APK_Status_Up_ed          = 2
	APK_Status_Up_Err         = 3
	APK_Status_Issue_Base_ing = 4
	APK_Status_Issue_Base_ed  = 5
	APK_Status_Issue_Base_Err = 6
	APK_Status_Issue_Up_ing   = 7
	APK_Status_Issue_Up_ed    = 8
	APK_Status_Issue_Up_Err   = 9
)

type AppInfo struct {
	ChangeLog string `json:"change_log,omitempty" bson:"change_log,omitempty"`
	Info      string `json:"info,omitempty" bson:"info,omitempty"`
}

type AppConfig struct {
	Code      int    `json:"code"`
	Verison   string `json:"version"`
	Path      string `json:"path"`
	Tips      string `json:"tips"`
	ForceCode int    `json:"force_code"`
}

type AppVersion struct {
	ID        bson.ObjectId `json:"id" bson:"_id,omitempty"`
	DeletedAt *time.Time    `json:"-" bson:"deleted_at,omitempty"`
	DeletedBy bson.ObjectId `json:"-" bson:"deleted_by,omitempty"`
	UpdatedAt *time.Time    `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
	Name      string        `json:"name"  bson:"name"`
	Code      int        `json:"code"  bson:"code"`
	Version   string        `json:"version" bson:"version"`
	ChangeLog string        `json:"change_log,omitempty" bson:"change_log,omitempty"`
	Info      string        `json:"info,omitempty" bson:"info,omitempty"`
	Path      string        `json:"path,omitempty" bson:"path,omitempty"`
	Status    int           `json:"status,omitempty" bson:"status,omitempty"` //1上传中   2 已经上传 3 上传失败    4 发布基准包中  5 已发布的基准包  6 基准包发布失败    7 发布升级包中   8 已发布的升级包  9 发布升级包失败
	Admin     string        `json:"admin,omitempty" bson:"admin,omitempty"`
	ForceCode int        `json:"force_code,omitempty" bson:"force_code,omitempty"`
}

type VersionInfo struct {
	Code    string `json:"code"`
	Version string `json:"version"`
	Path    string `json:"path"`
	Tips    string `json:"tips"`
}

type VersionCell struct {
	Version string     `json:"version" bson:"version"`
	Time    *time.Time `json:"time" bson:"time"`
	Updates string     `json:"updates" bson:"updates"`
}
type VersionHistoryInfo struct {
	ID        bson.ObjectId  `json:"-" bson:"_id,omitempty"`
	Verison   string         `json:"version" bson:"version"`
	Tips      []*VersionCell `json:"tips,omitempty" bson:"tips,omitempty"`
	UpdatedAt *time.Time     `json:"-" bson:"updated_at,omitempty"`
}

func (versionHistoryInfo *VersionHistoryInfo) Creat(db *mgo.Session) error {
	c := db.DB(DruidDB).C(AppVersionHistroyCollection)
	t := time.Now()
	versionHistoryInfo.UpdatedAt = &t
	_, err := MongoAtomInsert(c, bson.M{"version": versionHistoryInfo.Verison}, versionHistoryInfo, nil)

	if err != nil {
		return err
	}
	return nil
}

func GetVersionHistoryInfo(db *mgo.Session, version string) (*VersionHistoryInfo, error) {
	c := db.DB(DruidDB).C(AppVersionHistroyCollection)

	AppVersion := VersionHistoryInfo{}
	err := c.Find(bson.M{"version": version}).One(&AppVersion)
	if err != nil {
		return nil,err
	}
	return &AppVersion , nil
}

func (versionHistoryInfo *VersionHistoryInfo) Update(db *mgo.Session) error {
	c := db.DB(DruidDB).C(AppVersionHistroyCollection)
	t := time.Now()
	versionHistoryInfo.UpdatedAt = &t
	err := c.UpdateId(versionHistoryInfo.ID, bson.M{"$set": versionHistoryInfo})
	if err != nil {
		return err
	}
	return nil
}
func (appVersion *AppVersion) Create(db *mgo.Session) error {
	c := db.DB(DruidDB).C(AppVersionCollection)
	if appVersion.UpdatedAt == nil {
		t := time.Now()
		appVersion.UpdatedAt = &t
	}
	appVersion.ID = bson.NewObjectId()
	if appVersion.IsExist(db) == true {
		return fmt.Errorf("this Version  is  Exist ")
	}
	_, err := MongoAtomInsert(c, bson.M{"name": appVersion.Name, "deleted_at": nil}, appVersion, appVersion)
	if err != nil {
		return err
	}
	return nil
}

func (appVersion *AppVersion) IsExist(db *mgo.Session) bool {
	c := db.DB(DruidDB).C(AppVersionCollection)

	thisAppVersion := new(*AppVersion)
	err := c.Find(bson.M{"name": appVersion.Name, "deleted_at": nil}).One(thisAppVersion)
	if err != nil || thisAppVersion == nil {
		return false
	}
	return true
}

func AppVersionList(db *mgo.Session, rp *RequestParameter) ([]*AppVersion, int, error) {
	c := db.DB(DruidDB).C(AppVersionCollection)
	appVersions := []*AppVersion{}
	m := bson.M{"deleted_at": nil}
	count, err := MongoCount(c, m)
	if err != nil {
		return nil, 0, err
	}
	err = MongoList(c, m, &appVersions, rp, nil)
	if err != nil {
		return nil, 0, err
	}
	return appVersions, count, nil
}

func (appVersion *AppVersion) Update(db *mgo.Session) error {
	c := db.DB(DruidDB).C(AppVersionCollection)
	t := time.Now()
	appVersion.UpdatedAt = &t
	err := c.UpdateId(appVersion.ID, bson.M{"$set": appVersion})
	if err != nil {
		return err
	}
	return nil
}

func AppClearOldForceCode(db *mgo.Session, code int) error {
	c := db.DB(DruidDB).C(AppVersionCollection)
	err := c.Update(bson.M{"status": code}, bson.M{"$set": bson.M{"status": APK_Status_Up_ed}})
	if err != nil {
		return err
	}
	return nil
}

func (appVersion *AppVersion) Delete(db *mgo.Session) error {
	c := db.DB(DruidDB).C(AppVersionCollection)

	return MongoDelete(c, bson.M{"_id": appVersion.ID})
}

func FindAppVersionById(db *mgo.Session, id bson.ObjectId) *AppVersion {
	c := db.DB(DruidDB).C(AppVersionCollection)

	appVersion := AppVersion{}
	err := c.Find(bson.M{"_id": id, "deleted_at": nil}).One(&appVersion)
	if err != nil {
		return nil
	}
	return &appVersion
}

func FindAppVersionByStatus(db *mgo.Session, code int) *AppVersion {
	c := db.DB(DruidDB).C(AppVersionCollection)

	appVersion := AppVersion{}
	err := c.Find(bson.M{"status": code, "deleted_at": nil}).One(&appVersion)
	if err != nil {
		return nil
	}
	return &appVersion
}
