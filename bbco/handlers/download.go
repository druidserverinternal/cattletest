package handlers

import (
	"druid/cattle/bbco/common"
	"druid/cattle/models"
	"druid/cattle/protocol"

	"github.com/golang/protobuf/proto"

	"gopkg.in/mgo.v2/bson"

	"fmt"
	"strings"
	"github.com/sirupsen/logrus"
)

type Download struct {
}

func (obj *Download)Handle(remote string, cache []byte) ([]byte, uint32) {
	rsp := obj.parseReq(remote, cache)
	msg := MakePBMsgExample(protocol.HeaderType_TypeDownloadRsp, rsp)
	return msg, *rsp.Code
}

// ParseDownloadMsgReq parse download request
func (obj *Download)parseReq(remote string, data []byte) *protocol.DownloadRsp {
	msg := ParsePBMsgDownloadReq(data)
	iden := msg.GetIden()
	if iden == nil {
		return &protocol.DownloadRsp{
			Code: proto.Uint32(common.MSG_CODE_IDEN_INVALID),
		}
	}

	db := common.GetDB().Copy()
	defer db.Close()

	device, err := models.CheckDeviceByUUID(db, fmt.Sprintf("%x", iden.GetUUID()), nil)
	if err != nil {
		return &protocol.DownloadRsp{
			Code: proto.Uint32(common.MSG_CODE_NOT_FOUND_DEV),
		}
	}

	logger := common.GetLogger(logrus.Fields{"method": "download", "remote": strings.Split(remote, ":")[0], "uuid": device.UUID, "sn": device.Mark})
	logger.Info("Start download message")

	if msg.GetFileId() == "" {
		logger.Error("Could not found file id")
		return &protocol.DownloadRsp{
			Code: proto.Uint32(common.MSG_CODE_NOT_FOUND_DEV),
		}
	}

	firmware, _ := models.FindFirmwareByID(db, bson.ObjectIdHex(msg.GetFileId()))
	if firmware == nil {
		logger.Error("Could not find firmware:", msg.GetFileId())
		models.ClearSettingFirmware(db, device.ID)
		logger.Error("Clear firmware from:", device.Mark, device.UUID)
		return &protocol.DownloadRsp{
			Code: proto.Uint32(common.MSG_CODE_NOT_FOUND_DEV),
		}
	}
	logger.Infof("Read Firmware begin:%d length:%d\n", msg.GetBegin(), msg.GetLength())
	file, err := models.ReadFileByLimit(db, firmware.Point, int64(msg.GetBegin()), int64(msg.GetLength()))
//	file, err := firmware.Read(db, int(msg.GetBegin()), int(msg.GetLength()))
	if err != nil {
		logger.Error("Could not read firmware, ", err)
		return &protocol.DownloadRsp{
			Code: proto.Uint32(common.MSG_CODE_READ_FILE_FAILED),
		}
	}

	return &protocol.DownloadRsp{
		Code:   proto.Uint32(common.MSG_CODE_OK),
		Begin:  proto.Int32(msg.GetBegin()),
		Length: proto.Int32(int32(file.Size)),
		Data:   file.Data,
	}
}
