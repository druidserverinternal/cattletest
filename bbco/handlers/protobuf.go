package handlers

import (
	//  "bytes"
	"druid/cattle/bbco/common"
	"druid/cattle/protocol"
	"github.com/golang/protobuf/proto"
)

// MakePBMsgSampleRsp make a sample response
func MakePBMsgSampleRsp(ht protocol.HeaderType, code uint32) []byte {
	msg := &protocol.SampleRsp{
		Code: proto.Uint32(code),
	}
	return common.MakePBMsg(uint32(ht), msg)
}

// MakePBMsgSettingRsp make a setting response
func MakePBMsgSettingRsp(ht protocol.HeaderType, msg *protocol.SettingRsp) []byte {
	return common.MakePBMsg(uint32(ht), msg)
}

// MakePBMsgDownloadRsp make a download response
func MakePBMsgExample(ht protocol.HeaderType, msg proto.Message) []byte {
	return common.MakePBMsg(uint32(ht), msg)
}

// ParsePBMsgGPS2Req parse gps v2 request
func ParsePBMsgGPS2Req(data []byte) *protocol.GPS2Req {
	m := &protocol.GPS2Req{}
	if common.ParsePBMsg(data, m) == nil {
		return nil
	}
	return m
}

// ParsePBMsgCellularReq parse Cellular request
func ParsePBMsgCellularReq(data []byte) *protocol.CellularReq {
	m := &protocol.CellularReq{}
	if common.ParsePBMsg(data, m) == nil {
		return nil
	}
	return m
}


// ParsePBMsgGPSReq parse gps request
func ParsePBMsgGPSReq(data []byte) *protocol.GPSReq {
	m := &protocol.GPSReq{}
	if common.ParsePBMsg(data, m) == nil {
		return nil
	}
	return m
}

// ParsePBMsgRegisterReq parse behavior request
func ParsePBMsgRegisterReq(data []byte) *protocol.RegisterReq {
	m := &protocol.RegisterReq{}
	if common.ParsePBMsg(data, m) == nil {
		return nil
	}
	return m
}

// ParsePBMsgBehaviorReq parse behavior request
func ParsePBMsgBehaviorReq(data []byte) *protocol.BehaviorReq {
	m := &protocol.BehaviorReq{}
	if common.ParsePBMsg(data, m) == nil {
		return nil
	}
	return m
}

// ParsePBMsgBehavior2Req parse behavior request
func ParsePBMsgBehavior2Req(data []byte) *protocol.Behavior2Req {
	m := &protocol.Behavior2Req{}
	if common.ParsePBMsg(data, m) == nil {
		return nil
	}
	return m
}

// ParsePBMsgSettingReq parse setting request
func ParsePBMsgSettingReq(data []byte) *protocol.SettingReq {
	m := &protocol.SettingReq{}
	if common.ParsePBMsg(data, m) == nil {
		return nil
	}
	return m
}

// ParsePBMsgStatusReq parse behavior request
func ParsePBMsgStatusReq(data []byte) *protocol.StatusReq {
	m := &protocol.StatusReq{}
	if common.ParsePBMsg(data, m) == nil {
		return nil
	}
	return m
}

// ParsePBMsgDownloadReq parse setting request
func ParsePBMsgDownloadReq(data []byte) *protocol.DownloadReq {
	m := &protocol.DownloadReq{}
	if common.ParsePBMsg(data, m) == nil {
		return nil
	}
	return m
}

// ParsePBMsgIdentity parse behavior request
func ParsePBMsgIdentity(data []byte) *protocol.IdentityMsg {
	m := &protocol.IdentityMsg{}
	if common.ParsePBMsg(data, m) == nil {
		return nil
	}
	return m
}
