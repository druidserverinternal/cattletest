package handlers

import (
	"druid/cattle/bbco/common"
	"druid/cattle/models"
	"druid/cattle/protocol"

//	"gopkg.in/mgo.v2/bson"

	"fmt"
	"time"
	"strings"
//	"strconv"
	"github.com/golang/protobuf/proto"
	"github.com/sirupsen/logrus"
)

type Register struct {
}

func (obj *Register)Handle(remote string, cache []byte) ([]byte, uint32) {
	rsp := obj.parseReq(remote, cache)
	msg := MakePBMsgExample(protocol.HeaderType_TypeRegisterRsp, rsp)
	fmt.Printf("msg:%x\n", msg)
	return msg, *rsp.Code
}

// RegisterToDevice is convert protobuf to internal struct
func RegisterToDevice(in *protocol.RegisterReq) *models.Device {
	t := time.Now()
	return &models.Device{
		UpdatedAt:       &t,
		IMSI:            in.GetIMSI(),
		Mac:             strings.ToLower(in.GetMac()),
		DeviceType:      in.GetDeviceType(),
		FirmwareVersion: in.GetFirmwareVersion(),
		HardwareVersion: in.GetHardwareVersion(),
		BatteryVoltage: float64(in.GetVoltage()) / models.VoltageAccuracy,
		SignalStrength:		in.GetSignalStrength(),
		BitErrorRate:		in.GetBitErrorRate(),
		RadioAccessTechnology: in.GetRadioAccessTechnology(),
		NetworkOperator:	in.GetNetworkOperator(),
	}
}

// parseReq parse register request
func (obj *Register)parseReq(remote string, data []byte) *protocol.RegisterRsp {
	msg := ParsePBMsgRegisterReq(data)
	iden := msg.GetIden()

	uuid := fmt.Sprintf("%x", iden.GetUUID())
	if uuid == "" {
		return &protocol.RegisterRsp{
			Code: proto.Uint32(common.MSG_CODE_DATA_INVALID),
		}
	}

	logger := common.GetLogger(logrus.Fields{"method": "register", "remote": strings.Split(remote, ":")[0], "uuid": uuid})
	logger.Info("Start register message")

	db := common.GetDB().Copy()
	defer db.Close()

	regDevice := RegisterToDevice(msg)
	regDevice.UUID = uuid

	d, err := models.FindDeviceByUUID(db, uuid, nil)
	if err != nil {
		return &protocol.RegisterRsp{
			Code: proto.Uint32(common.MSG_CODE_DATABASE_ERROR),
		}
	}

	if d == nil {
		s, err := models.GetLatestSN(db, models.SNTypeBiological)
		if err != nil {
			logger.Info("Get latest sn failed.", err)
		} else {
			regDevice.Mark = s.SN
		}

		logger.Info("Start register new device.", regDevice)
		if err := regDevice.Register(db); err != nil {
			logger.Error("Upload register failed:", err)
			return &protocol.RegisterRsp{
				Code: proto.Uint32(common.MSG_CODE_DATABASE_ERROR),
			}
		}
		d = regDevice
	} else {
		logger.Info("Start update device with imsi.")
		if regDevice.IMSI != d.IMSI {
			if err := models.UnBindSIMDevice(db, d.IMSI); err != nil {
				return &protocol.RegisterRsp{
					Code: proto.Uint32(common.MSG_CODE_DATABASE_ERROR),
				}
			}

			d.IMSI = regDevice.IMSI
			d.SIMNumber = ""
		}
		d.FirmwareVersion = regDevice.FirmwareVersion
		d.HardwareVersion = regDevice.HardwareVersion
		d.DeviceType = regDevice.DeviceType
		d.BatteryVoltage = regDevice.BatteryVoltage
		d.Mac = regDevice.Mac

		logger.Info("Start update device status:", d)
		if err := d.UpdateStatus(db); err != nil {
			logger.Error("Update device status failed:", err)
			return &protocol.RegisterRsp{
				Code: proto.Uint32(common.MSG_CODE_DATABASE_ERROR),
			}
		}
	}

	logger = common.LoggerFields(logger, logrus.Fields{"sn": d.Mark})

	sim, err := models.FindSIMByIMSI(db, d.IMSI)
	if err != nil {
		return &protocol.RegisterRsp{
			Code: proto.Uint32(common.MSG_CODE_DATABASE_ERROR),
		}
	}
	if sim == nil || sim.Checked != models.SIMISPChecked {
		logger.Info("Start find sim info from remote.")
		s, err := models.NewDeviceWithSIM(db, d.IMSI)
		if err != nil {
			logger.Error("Request sim info from remote failed:", err)
			return &protocol.RegisterRsp{
				Code: proto.Uint32(common.MSG_CODE_REMOTE_SERVICE_FAILED),
			}
		} else {
			sim = s
		}
	}

	if d.SIMNumber == "" && sim.SIMNumber != "" {
		logger.Infof("Start update device sim number.%+v\n", d)
		d.SIMNumber = sim.SIMNumber
		if err := d.UpdateSIM(db); err != nil {
			logger.Error("Update device sim number failed:", err)
			return &protocol.RegisterRsp{
				Code: proto.Uint32(common.MSG_CODE_DATABASE_ERROR),
			}
		}
	}
	if sim.Checked == models.SIMISPChecked {
		if sim.Device == nil || sim.Device.ID != d.ID {
			logger.Info("Start update sim with device.")
			if err := sim.UpdateBindDevice(db, d); err != nil {
				logger.Error("Sim update device failed:", err)
			}
		}
	}

	logger.Info("Start check setting exists.")
	s, err := models.CheckSettingByDeviceID(db, d.ID)
	if err != nil {
		logger.Error("Find setting by deviceid failed.", err)
		return &protocol.RegisterRsp{
			Code: proto.Uint32(common.MSG_CODE_DATABASE_ERROR),
		}
	} 
	if s == nil {
		logger.Info("Start init setting.")
		if _, err := models.SettingInit(db, d, sim.SIMType); err != nil {
			logger.Error("Setting init failed.", err)
			return &protocol.RegisterRsp{
				Code: proto.Uint32(common.MSG_CODE_DATABASE_ERROR),
			}
		}
	}

	if err = models.SendMessageToNSQD(d, models.GetTopicDevice()); err != nil {
		logger.Error("Send device to mq failed:", err)
	}

	pr := &protocol.RegisterRsp{
		Code: proto.Uint32(common.MSG_CODE_OK),
		SN: proto.Int32(int32(d.Mark)),
		SimType : proto.Int32(sim.SIMType),
		Timestamp : proto.Uint32(uint32(time.Now().Unix())),
	}

	return pr
}
