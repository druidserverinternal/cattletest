package handlers

import (
	"druid/cattle/bbco/common"
	"druid/cattle/models"
	"druid/cattle/protocol"
	"druid/public"

	"github.com/golang/protobuf/proto"
	"github.com/sirupsen/logrus"

	"strings"
	"fmt"
)
type Setting struct {
}

func (obj *Setting)Handle(remote string, cache []byte) ([]byte, uint32) {
	rsp := obj.parseReq(remote, cache)
	msg := MakePBMsgExample(protocol.HeaderType_TypeSettingRsp, rsp)
	return msg, *rsp.Code
}

// ParseSettingMsgReq parse setting request
func (obj *Setting)parseReq(remote string, data []byte) *protocol.SettingRsp {
	msg := ParsePBMsgSettingReq(data)
	iden := msg.GetIden()
	if iden == nil {
		return &protocol.SettingRsp{
			Code: proto.Uint32(common.MSG_CODE_IDEN_INVALID),
		}
	}

	db := common.GetDB().Copy()
	defer db.Close()

	device, err := models.CheckDeviceByUUID(db, fmt.Sprintf("%x", iden.GetUUID()), nil)
	if err != nil {
		return &protocol.SettingRsp{
			Code: proto.Uint32(common.MSG_CODE_NOT_FOUND_DEV),
		}
	}

	logger := common.GetLogger(logrus.Fields{"method": "setting", "remote": strings.Split(remote, ":")[0], "uuid": device.UUID, "sn": device.Mark})
	logger.Info("Start setting message.")

	setting, err := models.CheckSettingByDeviceID(db, device.ID)
	if err != nil {
		logger.Error("Could not found setting by device id:", device.ID, err)
		return &protocol.SettingRsp{
			Code: proto.Uint32(common.MSG_CODE_NOT_FOUND_SETTING),
		}
	}
	if setting == nil {
		if setting, err = models.SettingInit(db, device, public.SIMTypeDefault); err != nil {
			logger.Error("Setting init failed.", err)
			return &protocol.SettingRsp{
				Code: proto.Uint32(common.MSG_CODE_DATABASE_ERROR),
			}
		}
	}
	logger.Infof("Download setting:%+v", setting)

	return &protocol.SettingRsp{
		Code:                     proto.Uint32(common.MSG_CODE_OK),
		EnvSamplingMode:          proto.Int32(setting.EnvSamplingMode),
		EnvSamplingFreq:          proto.Int32(setting.EnvSamplingFreq),
		BehaviorSamplingMode:     proto.Int32(setting.BehaviorSamplingMode),
		BehaviorSamplingFreq:     proto.Int32(setting.BehaviorSamplingFreq),
		GprsMode:                 proto.Int32(setting.GprsMode),
		GprsFreq:                 proto.Int32(setting.GprsFreq),
		EnvVoltageThreshold:      proto.Int32(int32(setting.EnvVoltageThreshold * models.VoltageAccuracy)),
		BehaviorVoltageThreshold: proto.Int32(int32(setting.BehaviorVoltageThreshold * models.VoltageAccuracy)),
		GprsVoltageThreshold:     proto.Int32(int32(setting.GprsVoltageThreshold * models.VoltageAccuracy)),
		OtaVoltageThreshold:      proto.Int32(int32(setting.OtaVoltageThreshold * models.VoltageAccuracy)),
		SpNumber:                 proto.String(setting.SpNumber),
		GprsVersion:              proto.Int32(setting.GprsVersion),
		FirmwareID:               proto.String(setting.FirmwareID.Hex()),
		FirmwareVersion:          proto.Int32(setting.FirmwareVersion),
		SMSMode	:				proto.Int32(setting.SMSMode),
		SMSFreq	:				proto.Int32(setting.SMSFreq),
		GpsAccuracyThresholdH:	proto.Int32(setting.GpsAccuracyThresholdH),
		GpsAccuracyThresholdV:	proto.Int32(setting.GpsAccuracyThresholdV),
		GpsFixTimeout:			proto.Int32(setting.GpsFixTimeout),
		GprsRetries:			proto.Int32(setting.GprsRetries),
		GprsType:				proto.Int32(setting.GprsType),
		GprsTimeTable:			proto.String(setting.GprsTimeTable),

		GprsPowerSavingMode:		proto.Int32(setting.GprsPowerSavingMode),
		GprsPowerSavingDistance:	proto.Int32(setting.GprsPowerSavingDistance),
		GprsPowerSavingTime:		proto.Int32(setting.GprsPowerSavingTime),
	}
}

