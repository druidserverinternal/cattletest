package handlers


import (
	"druid/cattle/protocol"
)

type Mux struct {
	mux     map[protocol.HeaderType]Message
}


var HandlersMux = Mux { make(map[protocol.HeaderType]Message) }

func init() {
	HandlersMux.RegisterHandler(protocol.HeaderType_TypeGPS2Req, &GPS2{})
    HandlersMux.RegisterHandler(protocol.HeaderType_TypeBehavior2Req, &Behavior2{})
    HandlersMux.RegisterHandler(protocol.HeaderType_TypeStatusReq, &Status{})
    HandlersMux.RegisterHandler(protocol.HeaderType_TypeRegisterReq, &Register{})
    HandlersMux.RegisterHandler(protocol.HeaderType_TypeSettingReq, &Setting{})
    HandlersMux.RegisterHandler(protocol.HeaderType_TypeDownloadReq, &Download{})
    HandlersMux.RegisterHandler(protocol.HeaderType_TypeCellularReq, &Cellular{})
}


