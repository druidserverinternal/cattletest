package handlers

import (
	"druid/cattle/bbco/common"
	"druid/cattle/models"
	"druid/cattle/protocol"

	"fmt"
	"strings"
	"time"
	"github.com/sirupsen/logrus"

	"gopkg.in/mgo.v2/bson"
)

type Status struct {
}

func (obj *Status)Handle(remote string, cache []byte) ([]byte, uint32) {
	code := obj.parseReq(remote, cache)
	msg := MakePBMsgSampleRsp(protocol.HeaderType_TypeStatusRsp, code)
	return msg, code
}

// StatusToModels is convert protobuf to internal struct
func StatusToModels(in *protocol.Status) *models.Status {
	t := time.Unix(int64(in.GetTimestamp()), 0)
	return &models.Status{
		Timestamp: &t,
		FirmwareVersion: uint32(in.GetFirmwareVersion()),
		StatusType:  in.GetStatusType(),
		StatusValue: in.GetStatusValue(),
	}
}

// ParseStatusMsgReq parse status request
func (obj *Status)parseReq(remote string, data []byte) uint32 {
	msg := ParsePBMsgStatusReq(data)
	iden := msg.GetIden()
	if iden == nil {
		return common.MSG_CODE_IDEN_INVALID
	}

	statusInfo := msg.GetStatusInfo()
	if iden == nil {
		return common.MSG_CODE_DATA_INVALID
	}

	db := common.GetDB().Copy()
	defer db.Close()

	device, err := models.CheckDeviceByUUID(db, fmt.Sprintf("%x", iden.GetUUID()), nil)
	if err != nil {
		return common.MSG_CODE_NOT_FOUND_DEV
	}

	logger := common.GetLogger(logrus.Fields{"method": "status", "remote": strings.Split(remote, ":")[0], "uuid": device.UUID, "sn": device.Mark})
	logger.Info("Start status message")

	models.DeviceUpdateAt(db, device.ID)


	logger.Info("Status length:", len(statusInfo))
	q := models.GetStatusQueue(db)
	for _, status := range statusInfo {
		m := StatusToModels(status)
		m.DeviceID = device.ID
		m.UUID = device.UUID
		m.Mark = device.Mark
//		m.Owner = device.Owner
		now := time.Now()
		m.UpdatedAt = &now

		logger.Infof("Upsert status record:%+v", m)
		q.Upsert( bson.M{"device_id": m.DeviceID, "timestamp": m.Timestamp}, bson.M{"$set": m})
		/*
		if err := m.UpsertByTimestamp(db); err != nil {
			logger.Error("Upsert status failed:", err)
			return common.MSG_CODE_DATABASE_ERROR
		}
		*/
	}
	if _, err := q.Run(); err != nil {
		logger.Error("Upload status failed:", err)
		return common.MSG_CODE_DATABASE_ERROR
	}

	if err = models.SendMessageToNSQD(device, models.GetTopicDevice()); err != nil {
		logger.Error("Send device to mq failed:", err)
	}

	return common.MSG_CODE_OK
}
