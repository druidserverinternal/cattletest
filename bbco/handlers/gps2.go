package handlers

import (
	"druid/cattle/bbco/common"
	"druid/cattle/models"
	"druid/cattle/protocol"

	"fmt"
	"time"
	"strings"

	"github.com/sirupsen/logrus"

//	"github.com/kellydunn/golang-geo"

	"gopkg.in/mgo.v2/bson"
)

type GPS2 struct {
}

func (gps *GPS2)Handle(remote string, cache []byte) ([]byte, uint32) {
	code := gps.parseReq(remote, cache)
	msg := MakePBMsgSampleRsp(protocol.HeaderType_TypeGPSRsp, code)
	return msg, code
}

// GpsToModels is convert protobuf to internal struct
func Gps2ToModels(in *protocol.GPS2) *models.GPS {
	t := time.Unix(int64(in.GetTimestamp()), 0)
	long := float64(in.GetLongitude()) / models.LocationAccuracy
	lat := float64(in.GetLatitude()) / models.LocationAccuracy
	gps := &models.GPS{
		Timestamp:      &t,
		Longitude:      long,
		Latitude:       lat,
		Altitude:       float64(in.GetAltitude()) / models.AltitudeAccuracy,
		Temperature:    float64(in.GetTemperature()) / models.TemperatureAccuracy,
		Humidity:       in.GetHumidity(),
		Light:          in.GetLight(),
		Pressure:       in.GetPressure(),
		Dimension:      in.GetDimension(),
		Viewstar:       in.GetViewStar(),
		UsedStar:       in.GetUsedStar(),
		BatteryVoltage: float64(in.GetBatteryVoltage()) / models.VoltageAccuracy,
		Horizontal:     float64(in.GetHorizontal()) / models.HorizontalAccuracy,
		Vertical:       float64(in.GetVertical()) / models.VerticalAccuracy,
		Speed:          float64(in.GetSpeed()) / models.SpeedAccuracy,
		Course:         float64(in.GetCourse()) / models.CourseAccuracy,
		SignalStrength: in.GetSignalStrength(),
		FixTime: in.GetFixTime(),
	}
	if long > 190 {
		return gps
	}

	gps.Loc = models.NewPoint([]float64{long, lat})
	return gps
}

// parseGpsMsgReq parse gps request
func (gps *GPS2)parseReq(remote string, data []byte) uint32 {
	msg := ParsePBMsgGPS2Req(data)
	iden := msg.GetIden()
	if iden == nil {
		return common.MSG_CODE_IDEN_INVALID
	}

	gpsInfo := msg.GetGPSInfo()
	if gpsInfo == nil {
		return common.MSG_CODE_DATA_INVALID
	}

	db := common.GetDB().Clone()
	defer db.Close()


	device, err := models.CheckDeviceByUUID(db, fmt.Sprintf("%x", iden.GetUUID()), nil)
	if err != nil {
		return common.MSG_CODE_NOT_FOUND_DEV
	}

	logger := common.GetLogger(logrus.Fields{"method": "gps2", "remote": strings.Split(remote, ":")[0], "uuid": device.UUID, "sn": device.Mark})
	logger.Info("Start gps2 message")

	models.DeviceUpdateAt(db, device.ID)

	logger.Info("Gps length:", len(gpsInfo))
	if len(gpsInfo) == 0 {
		return common.MSG_CODE_OK
	}

	gpss := []*models.GPS{}
	q := models.GetGPSQueue(db)
	for _, gps := range gpsInfo {
		m := Gps2ToModels(gps)
		m.DeletedAt = device.DeletedAt
		m.DeviceID = device.ID
//		m.CompanyID = device.CompanyID
//		m.CompanyName = device.CompanyName
		m.FirmwareVersion = device.FirmwareVersion
		m.UUID = device.UUID
		m.Mark = device.Mark
		m.DeletedAt = device.DeletedAt
		now := time.Now()
		m.UpdatedAt = &now

		if !m.VerifyTimestamp() {
			logger.Error("Ignore time over range gps2:%+v", m)
			continue
		}

		if msg.GetSMS() == 1 {
			m.SMS = models.SrcFromSMS
			logger.Infof("Received sms record:%+v\n", m)
		} else {
			logger.Infof("Received network record:%+v", m)
		}

		if m.Longitude < 190 {
			offset, err := models.FindOffsetByLocation(db, m.Longitude, m.Latitude)
			if err != nil {
				logger.Error("Find offset by location failed.")
			}
			m.PointLocation = offset.PointLocation

//			device.LastValidGPS = m
		}

		if m.FirmwareVersion < 11 && m.FixTime == 0 {
			t := m.Timestamp.Unix()
			ft := t % 300
			if ft >= 280 {
				m.FixTime = int32(ft - 300)
			} else {
				m.FixTime = int32(ft)
			}
		}

		logger.Infof("Insert new gps2 record:%+v", m)
		q.Upsert( bson.M{"device_id": m.DeviceID, "timestamp": m.Timestamp, "sms": m.SMS }, bson.M{"$set": m})
		
	/*
		if err := m.UpsertByTimestampSMS(db); err != nil {
			logger.Error("Upload gps failed:", err)
			return common.MSG_CODE_DATABASE_ERROR
		}
		*/
		gpss = append(gpss, m)
	}
	if _, err := q.Run(); err != nil {
		logger.Error("Upload gps failed:", err)
		return common.MSG_CODE_DATABASE_ERROR
	}

	for _, gps := range gpss {
		if err = models.SendMessageToNSQD(gps, models.GetTopicGPS()); err != nil {
			logger.Error("Send device to gps mq failed:", err)
		}
	}

	if err = models.SendMessageToNSQD(device, models.GetTopicDevice()); err != nil {
		logger.Error("Send device to mq failed:", err)
	}

	return common.MSG_CODE_OK
}

