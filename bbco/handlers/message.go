package handlers


import (

	"druid/cattle/protocol"
)

type Message interface {
	Handle(string, []byte) ([]byte, uint32)
}

func (handler *Mux)RegisterHandler(ht protocol.HeaderType, msg Message) {
	handler.mux[ht] = msg
}

func (handler *Mux)GetHandler(ht protocol.HeaderType) Message {
	return handler.mux[ht]
}

