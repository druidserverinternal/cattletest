package handlers

import (
	"druid/cattle/bbco/common"
	"druid/cattle/models"
	"druid/cattle/protocol"

	"fmt"
	"time"
	"strings"
	"github.com/sirupsen/logrus"

	"gopkg.in/mgo.v2/bson"
)

type Cellular struct {
}

func (obj *Cellular)Handle(remote string, cache []byte) ([]byte, uint32) {
	code := obj.parseReq(remote, cache)
	msg := MakePBMsgSampleRsp(protocol.HeaderType_TypeCellularRsp, code)
	return msg, code
}

// CellularToModels is convert protobuf to internal struct
func CellularToModels(in *protocol.Cellular) *models.Cellular {
	t := time.Unix(int64(in.GetTimestamp()), 0)
	long := float64(in.GetLongitude()) / models.LocationAccuracy
	lat := float64(in.GetLatitude()) / models.LocationAccuracy
	cellular := &models.Cellular{
		Timestamp				:	&t,
		FirmwareVersion			:	int32(in.GetFirmwareVersion()),
		Longitude				:	long,
		Latitude				:	lat,
		BatteryVoltage			:	float64(in.GetBatteryVoltage()) / models.VoltageAccuracy,
		ErrorFlag				:	uint32(in.GetErrorFlag()),
		TotalTime				:	int32(in.GetTotalTime()),
		HwtTime					:	int32(in.GetHwtTime()),
		SimTime					:   int32(in.GetSimTime()),
		NumTime					:   int32(in.GetNumTime()),
		RssiTime				:   int32(in.GetRssiTime()),
		RegisterTime			:   int32(in.GetRegisterTime()),
		AttachTime				:   int32(in.GetAttachTime()),
		PdpTime					:   int32(in.GetPdpTime()),
		ConnectionTime			:   int32(in.GetConnectionTime()),
		CommunicationTime		:   int32(in.GetCommunicationTime()),
		NetworkOperator			:	uint32(in.GetNetworkOperator()),
		SignalStrength			:	int32(in.GetSignalStrength()),
		BitErrorRate			:	int32(in.GetBitErrorRate()),
		RadioAccessTechnology	:	int32(in.GetRadioAccessTechnology()),
		Temperature				:   float64(in.GetTemperature()) / models.TemperatureAccuracy,
		SmsTime					:	int32(in.GetSmsTime()),
		ExitFlag				:	int32(in.GetExitFlag()),
	}

	if long > 190 {
		return cellular
	}

	cellular.Loc = models.NewPoint([]float64{long, lat})
	
	return cellular
}

// ParseCellularMsgReq parse cellular request
func (obj *Cellular)parseReq(remote string, data []byte) uint32 {
	msg := ParsePBMsgCellularReq(data)
	iden := msg.GetIden()
	if iden == nil {
		return common.MSG_CODE_IDEN_INVALID
	}

	cellularInfo := msg.GetCellularInfo()
	if iden == nil {
		return common.MSG_CODE_DATA_INVALID
	}

	db := common.GetDB().Copy()
	defer db.Close()

	device, err := models.CheckDeviceByUUID(db, fmt.Sprintf("%x", iden.GetUUID()), nil)
	if err != nil {
		return common.MSG_CODE_NOT_FOUND_DEV
	}

	logger := common.GetLogger(logrus.Fields{"method": "cellular", "remote": strings.Split(remote, ":")[0], "uuid": device.UUID, "sn": device.Mark})
	logger.Info("Start cellular message")

	models.DeviceUpdateAt(db, device.ID)

	logger.Info("Cellular length:", len(cellularInfo))
	q := models.GetCellularQueue(db)
	for _, cellular := range cellularInfo {
		m := CellularToModels(cellular)
		m.DeviceID = device.ID
		m.UUID = device.UUID
		m.Mark = device.Mark
		now := time.Now()
		m.UpdatedAt = &now

		logger.Infof("Upsert cellular record:%+v", m)
		q.Upsert( bson.M{"device_id": m.DeviceID, "timestamp": m.Timestamp}, bson.M{"$set": m})
		/*
		if err := m.UpsertByTimestamp(db); err != nil {
			logger.Error("Upsert cellular failed:", err)
			return common.MSG_CODE_DATABASE_ERROR
		}
		*/
	}
	if _, err := q.Run(); err != nil {
		logger.Error("Upload cellular failed:", err)
		return common.MSG_CODE_DATABASE_ERROR
	}

	if err = models.SendMessageToNSQD(device, models.GetTopicDevice()); err != nil {
		logger.Error("Send device to mq failed:", err)
	}

	return common.MSG_CODE_OK
}
