package handlers

import (
	"druid/cattle/bbco/common"
	"druid/cattle/models"
	"druid/cattle/protocol"

	"fmt"
	"time"
	"strings"

	"github.com/sirupsen/logrus"
	"gopkg.in/mgo.v2/bson"
)

// Behavior2ToModels is convert protobuf to internal struct
func Behavior2ToModels(in *protocol.Behavior2) *models.Behavior2 {
	t := time.Unix(int64(in.GetTimestamp()), 0)
	beh2 := &models.Behavior2{
		Timestamp:      &t,
		ODBA:      in.GetODBA(),
		ODBAX:      in.GetODBAX(),
		ODBAY:      in.GetODBAY(),
		ODBAZ:      in.GetODBAZ(),
		MeandlX:    in.GetMeandlX(),
		MeandlY:    in.GetMeandlY(),
		MeandlZ:    in.GetMeandlZ(),
	}

	if beh2.ODBA == 0 {
		beh2.ODBA = beh2.ODBAX + beh2.ODBAY + beh2.ODBAZ
	}
	return beh2
}

type Behavior2 struct {
}

func (behavior *Behavior2)Handle(remote string, cache []byte) ([]byte, uint32) {
	code := behavior.parseReq(remote, cache)
	msg := MakePBMsgSampleRsp(protocol.HeaderType_TypeBehavior2Rsp, code)
	return msg, code
}

// ParseBehavior2MsgReq parse behavior request
func (behavior *Behavior2)parseReq(remote string, data []byte) uint32 {
	msg := ParsePBMsgBehavior2Req(data)
	iden := msg.GetIden()
	if iden == nil {
		return common.MSG_CODE_IDEN_INVALID
	}

	behaviorInfo := msg.GetBehaviorInfo()
	if iden == nil {
		return common.MSG_CODE_DATA_INVALID
	}

	db := common.GetDB().Copy()
	defer db.Close()

	device, err := models.CheckDeviceByUUID(db, fmt.Sprintf("%x", iden.GetUUID()), nil)
	if err != nil {
		return common.MSG_CODE_NOT_FOUND_DEV
	}

	logger := common.GetLogger(logrus.Fields{"method": "behavior2", "remote": strings.Split(remote, ":")[0], "uuid": device.UUID, "sn": device.Mark})
	logger.Info("Start behavior2 message")

	models.DeviceUpdateAt(db, device.ID)

	logger.Info("Behavior2 length:", len(behaviorInfo))
	q := models.GetBehavior2Queue(db)
	for _, behavior := range behaviorInfo {
		t := time.Unix(int64(behavior.GetTimestamp()), 0)
		g := models.FindBehavior2ByTimestamp(db, device.ID, &t, nil)
		if g != nil {
			logger.Warning("Find exists behavior2 record:", device.ID, g.Timestamp)
			continue
		}

		m := Behavior2ToModels(behavior)
		m.DeviceID = device.ID
		m.UUID = device.UUID
		m.Mark = device.Mark
		m.FirmwareVersion = device.FirmwareVersion
		m.DeletedAt = device.DeletedAt
		now := time.Now()
		m.UpdatedAt = &now

		if !m.VerifyTimestamp() {
			logger.Error("Ignore time over range behavior2:%+v", m)
			continue
		}

		logger.Infof("Insert new beh2 record:%+v", m)
		q.Upsert( bson.M{"device_id": m.DeviceID, "timestamp": m.Timestamp}, bson.M{"$set": m})
		/*
		if err := m.UpsertByTimestamp(db); err != nil {
			logger.Error("Upload behavior2 failed:", err)
			return common.MSG_CODE_DATABASE_ERROR
		}
		*/
	}
	if _, err := q.Run(); err != nil {
		logger.Error("Upload behavior2 failed:", err)
		return common.MSG_CODE_DATABASE_ERROR
	}

	if err = models.SendMessageToNSQD(device, models.GetTopicDevice()); err != nil {
		logger.Error("Send device to mq failed:", err)
	}

	if err = models.SendMessageToNSQD(device, models.GetTopicBehavior2()); err != nil {
		logger.Error("Send behavior2 to mq failed:", err)
	}

	return common.MSG_CODE_OK
}

