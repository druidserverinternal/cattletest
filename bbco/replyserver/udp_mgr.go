package replyserver

import (
	"net"
	"runtime/debug"
//	"time"
	//   "encoding/binary"
	"druid/cattle/bbco/common"
	"druid/cattle/bbco/handlers"
	"druid/cattle/protocol"

	"github.com/sirupsen/logrus"
)

// UDPConnMgr is udp connection manager
type UDPConnMgr struct {
	Cache    []byte
	Conn     *net.UDPConn
	Client     *net.UDPAddr
	Size     uint32
	Head     MsgHead
	Handlers *handlers.Mux
}

// NewUDPMgr make a new udp manager
func NewUDPMgr(conn *net.UDPConn, handlers *handlers.Mux) *UDPConnMgr {
	return &UDPConnMgr{
		Cache:    make([]byte, common.MSG_UDP_MAX_SIZE),
		Conn:     conn,
		Size:     0,
		Handlers: handlers,
	}
}

func (mgr *UDPConnMgr) Address() string {
	return mgr.Client.String()
}
func (mgr *UDPConnMgr) Write(data []byte) error {
	_, err := mgr.Conn.WriteToUDP(data, mgr.Client)
	if err != nil {
		return err
	} 
	return nil
}

// Run is udp connection master
func (mgr *UDPConnMgr) Run() {
	logger := common.GetLogger(logrus.Fields{"method": "udp_manager"})

	defer func() {
		if err := recover(); err != nil {
			logger.Errorf("%s\n%s", err, string(debug.Stack()))
		}
	}()

	for run {
		n, addr, err := mgr.Conn.ReadFromUDP(mgr.Cache)
		logger := common.GetLogger(logrus.Fields{"method": "udp_manager", "remote": addr.String()})
		if err != nil {
			logger.Error("recv udp failed:", err)
			continue
		}
		logger.Debugf("read data:%x", mgr.Cache[:n])

		if n <= common.MSG_HEAD_SIZE {
			continue
		}
		mgr.Size = uint32(n)
		mgr.Client = addr

		head := mgr.ReadHead(logger)
		if head == nil {
			return
		}
		if !mgr.Authenticator(logger) {
			logger.Error("Crc16 verify failed. continue the connection.")
			return
		}

		if code := mgr.ParseMsg(head.MsgType, logger); code != common.MSG_CODE_OK {
			logger.Info("Parse msg failed :", code)
			return
		}
	}
}

func (mgr *UDPConnMgr) ReadHead(logger *logrus.Entry) *MsgHead {
	head := &mgr.Head

	if mgr.Size < common.MSG_HEAD_SIZE {
		logger.Error("recv head len:", mgr.Size, " real headsize:", common.MSG_HEAD_SIZE)
		return nil
	}

	err := MakeMsgHead2(mgr.Cache[:common.MSG_HEAD_SIZE], head)
	if err != nil || head.MsgLen > common.MSG_BODY_MAX_SIZE {
		logger.Error("udp recv failed err:", err)
		logger.Errorf("udp recv failed msg %d, :%+v, %x", mgr.Size, head, mgr.Cache)
		return nil
	}

	if mgr.Size != head.MsgLen + common.MSG_HEAD_SIZE {
		logger.Warn("recv len != total len")
		return nil
	}

	logger.Debug("data recv len:", mgr.Size, " msg len:", head.MsgLen)
	return head
}

// Authenticator is verify token
func (mgr *UDPConnMgr) Authenticator(logger *logrus.Entry) bool {
	auth := mgr.ParsePBMsgAuthenticator(mgr.Cache[:mgr.Size])
	if auth == nil {
		logger.Errorf("parse authenticator failed: %x", mgr.Cache[:mgr.Size])
		return false
	}

	iden := auth.GetIden()
	if iden == nil {
		logger.Errorf("parse identity failed: %x", mgr.Cache[:mgr.Size])
		return false
	}

	if TokenVerify(iden.GetUUID(), iden.GetToken()) {
		return true
	}

	logger.Infof("Crc16 verify failed:%x", iden.GetToken())
	return false
}

// ParseMsg pasrse data and return result code
func (mgr *UDPConnMgr) ParseMsg(t uint32, logger *logrus.Entry) (code uint32) {
	defer func() {
		logger.Debug("return result code :", code)
	}()

	logger.Debug("start parse data:", mgr.Cache[:mgr.Size])

	obj := mgr.Handlers.GetHandler(protocol.HeaderType(t))
	if obj == nil {
		logger.Error("unknown cmd:", t, " size:", mgr.Size, " str:", mgr.Cache[:mgr.Size])
		return common.MSG_CODE_UNKNOWN_CMD
	}

	msg, code := obj.Handle(mgr.Client.String(), mgr.Cache[:mgr.Size])
	_, err := mgr.Conn.WriteToUDP(msg, mgr.Client)
	if err != nil {
		logger.Error("Send to client failed:", err)
		return common.MSG_CODE_UNKNOWN_CMD
	}
	return code
}

// ParsePBMsgAuthenticator parse  request
func (mgr *UDPConnMgr)ParsePBMsgAuthenticator(data []byte) *protocol.Authenticator {
	m := &protocol.Authenticator{}
	if common.ParsePBMsg(data, m) == nil {
		return nil
	}
	return m
}
