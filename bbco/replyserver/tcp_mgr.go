package replyserver

import (
	"net"
	"runtime/debug"
	"sync"
	"time"
	//   "encoding/binary"
	"druid/cattle/bbco/common"
	"druid/cattle/bbco/handlers"
	"druid/cattle/protocol"

	"github.com/sirupsen/logrus"
)

// TCPConnMgr is tcp connection manager
type TCPConnMgr struct {
	Cache    []byte
	Conn     net.Conn
	Size     uint32
	Head     MsgHead
	Handlers *handlers.Mux
}

// NewTCPMgr make a new tcp manager
func NewTCPMgr(conn net.Conn, handlers *handlers.Mux) *TCPConnMgr {
	return &TCPConnMgr{
		Cache:    make([]byte, common.MSG_MAX_SIZE),
		Conn:     conn,
		Size:     0,
		Handlers: handlers,
	}
}

func (mgr *TCPConnMgr) Address() string {
	return mgr.Conn.RemoteAddr().String()
}


func (mgr *TCPConnMgr) Write(data []byte) error {
	_, err := mgr.Conn.Write(data)
	if err != nil {
		return err
	} 
	return nil
}


// Run is tcp connection master
func (mgr *TCPConnMgr) Run(idx uint64, wg *sync.WaitGroup) {
	logger := common.GetLogger(logrus.Fields{"method": "tcp_manager", "remote": mgr.Conn.RemoteAddr().String()})

	defer wg.Done()
	defer func() {
		if err := recover(); err != nil {
			logger.Errorf("%s\n%s", err, string(debug.Stack()))
		}
	}()

	logger.Debug("start new tcp mgr:", idx)
	defer func() {
		logger.Debug("close connection:", idx)
		mgr.Conn.Close()
	}()

	for run {
		head := mgr.Recv(logger)
		if head == nil || mgr.Size == 0 {
			return
		}
		if !mgr.Authenticator(logger) {
			logger.Error("Crc16 verify failed. disconnect the connection:", idx)
			return
		}

		if code := mgr.ParseMsg(head.MsgType, logger); code != common.MSG_CODE_OK {
			logger.Info("Parse msg failed :", code)
			return
		}
	}
}

// Recv data from tcp connection
func (mgr *TCPConnMgr) Recv(logger *logrus.Entry) *MsgHead {
	totalLen := uint32(0)
	begin := uint32(0)
	stat := STAT_MSG_HEAD
	head := &mgr.Head

	for run {
		//		logger.Debug("start read data, stat:", stat)
		mgr.Conn.SetDeadline(time.Now().Add(60 * time.Second))

		switch stat {
		case STAT_MSG_HEAD:
			n, err := mgr.Conn.Read(mgr.Cache[:common.MSG_HEAD_SIZE])
			if err != nil {
				logger.Errorf("tcp read err:%s time:%s", err, time.Now())
				return nil
			}

			if n < common.MSG_HEAD_SIZE {
				logger.Error("recv head len:", n, " real headsize:", common.MSG_HEAD_SIZE)
				return nil
			}

			err = MakeMsgHead2(mgr.Cache[:common.MSG_HEAD_SIZE], head)
			if err != nil || head.MsgLen > common.MSG_BODY_MAX_SIZE {
				logger.Error("tcp recv failed err:", err)
				logger.Errorf("tcp recv failed msg %d, :%+v, %x", n, head, mgr.Cache[:n])
				return nil
			}
			totalLen = head.MsgLen + common.MSG_HEAD_SIZE
			logger.Debug("head total len:", totalLen, "head:", mgr.Cache[:common.MSG_HEAD_SIZE])

			if head.MsgLen == 0 {
				mgr.Size = totalLen
				return head
			}

			stat = STAT_DATA
			begin = common.MSG_HEAD_SIZE
			logger.Debug("Parse msg header success.")
			continue
		case STAT_DATA:
			n, err := mgr.Conn.Read(mgr.Cache[begin:totalLen])
			if err != nil {
				logger.Error("tcp read failed:", err)
				return nil
			}

			begin += uint32(n)

			logger.Debug("data recv len:", n, " begin:", begin, " total:", totalLen)
			if begin < totalLen {
				logger.Warn("recv len < total len")
				continue
			}

			mgr.Size = totalLen
			logger.Debugf("read data:%x", mgr.Cache[:totalLen])
			return head
		default:
			logger.Error("tcp recver jumped to unknown stat")
			return nil
		}
	}
	return nil
}

// Authenticator is verify token
func (mgr *TCPConnMgr) Authenticator(logger *logrus.Entry) bool {
	auth := mgr.ParsePBMsgAuthenticator(mgr.Cache[:mgr.Size])
	if auth == nil {
		logger.Errorf("parse authenticator failed: %x\n", mgr.Cache[:mgr.Size])
		return false
	}

	iden := auth.GetIden()
	if iden == nil {
		logger.Errorf("parse identity failed: %x\n", mgr.Cache[:mgr.Size])
		return false
	}

	if TokenVerify(iden.GetUUID(), iden.GetToken()) {
		return true
	}

	logger.Infof("Crc16 verify failed:%x", iden.GetToken())
	return false
}

// ParseMsg pasrse data and return result code
func (mgr *TCPConnMgr) ParseMsg(t uint32, logger *logrus.Entry) (code uint32) {
	defer func() {
		logger.Debug("return result code :", code)
	}()

	logger.Debug("start parse data:", mgr.Cache[:mgr.Size])

	obj := mgr.Handlers.GetHandler(protocol.HeaderType(t))
	if obj == nil {
		logger.Error("unknown cmd:", t, " size:", mgr.Size, " str:", mgr.Cache[:mgr.Size])
		return common.MSG_CODE_UNKNOWN_CMD
	}

	rsp, code := obj.Handle(mgr.Conn.RemoteAddr().String(), mgr.Cache[:mgr.Size])
	mgr.Conn.Write(rsp)
	return code
}

// ParsePBMsgAuthenticator parse  request
func (mgr *TCPConnMgr)ParsePBMsgAuthenticator(data []byte) *protocol.Authenticator {
	m := &protocol.Authenticator{}
	if common.ParsePBMsg(data, m) == nil {
		return nil
	}
	return m
}
