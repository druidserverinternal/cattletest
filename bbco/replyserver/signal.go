package replyserver

import (
	"net"
	"sync"
)

var run	bool = true
var wg sync.WaitGroup
var listen net.Listener
var udpListen *net.UDPConn

func Stop() error {
	run = false
	listen.Close()
	udpListen.Close()
	return nil
}


