package replyserver

import (
	"bytes"
	"encoding/binary"
	//   "net"
)

const (
	// STAT_MSG_HEAD is tcp recv flag
	STAT_MSG_HEAD uint32 = iota

	// STAT_DATA is tcp recv flag
	STAT_DATA
)

// BytesToLittleUint32 convert binary to little endian int
func BytesToLittleUint32(data []byte) uint32 {
	return binary.LittleEndian.Uint32(data[:4])
}

// BytesToBigUint32 convert binary to big endian int
func BytesToBigUint32(data []byte) uint32 {
	return binary.BigEndian.Uint32(data[:4])
}

// ClearZero return slice until 0x00
func ClearZero(data []byte) []byte {
	index := bytes.IndexByte(data, 0)
	return data[:index]
}

// ExtendByte extend a byte
func ExtendByte(slice []byte, element byte) []byte {
	n := len(slice)
	if n == cap(slice) {
		newSlice := make([]byte, len(slice), len(slice)+2)
		copy(newSlice, slice)
		slice = newSlice
	}
	slice = slice[0 : n+1]
	slice[n] = element
	return slice
}

// SliceNil return a nil slice
func SliceNil() []byte {
	return nil
}
