package replyserver

import (
	"druid/cattle/bbco/handlers"

	log "github.com/sirupsen/logrus"
	"net"
)

// Server is main loop service
func Server(ln net.Listener, handlers *handlers.Mux) {
	listen = ln
	idx := uint64(0)
	for run {
		conn, err := ln.Accept()
		if err != nil {
			log.Error("accept failed:", err)
			continue
		}

		mgr := NewTCPMgr(conn, handlers)
		wg.Add(1)
		go mgr.Run(idx, &wg)
		idx++
	}
	wg.Wait()
}

func ServerUDP(address string, handlers *handlers.Mux) {
	ServerAddr,err := net.ResolveUDPAddr("udp", address)
	if err != nil {
		log.Fatal("Resolve udp addr ", address, " failed:", err)
		return
	}
	conn, err := net.ListenUDP("udp", ServerAddr)
	if err != nil {
		log.Fatal("Listen udp addr ", address, " failed:", err)
		return
	}
	udpListen = conn

	for i := 0; i < 100; i++ {
		go func() {
			mgr := NewUDPMgr(conn, handlers)

			for run {
				mgr.Run()
			}
		}()
	}
}




