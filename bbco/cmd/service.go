package cmd

import (
	"druid/cattle/models"

	log "github.com/sirupsen/logrus"
)

func Service() error {
	log.Info("start register service.")

	if err := models.RegSocket("RecvListen"); err != nil {
		return err
	}

	return nil
}

func DelService() error {
	log.Info("start delete service.")
	if err := models.DelSocket("RecvListen"); err != nil {
		return err
	}

	return nil
}

