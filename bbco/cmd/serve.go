package cmd

import (
	"druid/cattle/bbco/replyserver"
	"druid/cattle/bbco/handlers"
	"druid/cattle/bbco/common"
	"druid/cattle/models"
	"druid/public"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	log "github.com/sirupsen/logrus"

	"net"
)

const defaultAddress = ":8000"

// serveCmd represents the serve command
var serveCmd = &cobra.Command{
	Use:    "serve",
	Short:  "Start manager server",
	PreRun: LoadConfiguration,
	Run:    serve,
}

func init() {
	RootCmd.AddCommand(serveCmd)

	serveCmd.Flags().StringP(
		"address",
		"a",
		defaultAddress,
		"Address the server binds to",
	)
	viper.BindPFlag("address", serveCmd.Flags().Lookup("address"))
}

func serve(cmd *cobra.Command, args []string) {
	common.LoggerConnInit()

	if err := common.ConnectDB(
		viper.GetString("database"),
	); err != nil {
		log.Fatal("connect to db: ", err)
	}

	if err := public.ConnectEtcd(
		viper.GetString("etcd.address"),
		viper.GetString("etcd.ca"),
		viper.GetString("etcd.cert"),
		viper.GetString("etcd.key"),
	); err != nil {
		log.Fatalln("connect to etcd: ", err)
	}

	address := viper.GetString("address")
	go models.Signal(replyserver.Stop)

	go replyserver.ServerUDP(address, &handlers.HandlersMux)

	ln, err := net.Listen("tcp", address)
	if err != nil {
		log.Fatal("failed to listen:", err)
	}

	log.Infof("Starting bbco server on %s.", address)
	replyserver.Server(ln, &handlers.HandlersMux)
}
