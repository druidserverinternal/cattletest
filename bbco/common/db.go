package common

import (
	"gopkg.in/mgo.v2"
)

var db *mgo.Session

// ConnectDB connect to a remote mongo
func ConnectDB(dataURL string) (err error) {
	db, err = mgo.Dial(dataURL)
	if err != nil {
		return err
	}

	db.SetMode(mgo.Monotonic, true)
	return err
}

// GetDB returns the db pointer from context or nil if db has not been connected
func GetDB() *mgo.Session {
	return db
}
