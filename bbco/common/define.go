package common

import (
)

const (
	// MSG_HEAD_SIZE is msg header
	MSG_HEAD_SIZE = 8

	// MSG_MAX_SIZE max msg length
	MSG_MAX_SIZE = 100 * 1024

	// udp length
	MSG_UDP_MAX_SIZE = 1024

	// MSG_BODY_MAX_SIZE is max body length
	MSG_BODY_MAX_SIZE = 100*1024 - MSG_HEAD_SIZE
)

const (
	// MSG_CODE_OK is success code
	MSG_CODE_OK uint32 = iota

	// MSG_CODE_AUTH_FAILED login failed
	MSG_CODE_AUTH_FAILED

	// MSG_CODE_NOT_AUTH did not login
	MSG_CODE_NOT_AUTH

	// MSG_CODE_NOT_FOUND_DEV did not found device
	MSG_CODE_NOT_FOUND_DEV

	// MSG_CODE_UNKNOWN_CMD is unknown cmd
	MSG_CODE_UNKNOWN_CMD

	// MSG_CODE_PARSE_PB_FAIL is parse proto failed
	MSG_CODE_PARSE_PB_FAIL

	// MSG_CODE_PB_ENCODE_FAIL is encode proto failed
	MSG_CODE_PB_ENCODE_FAIL

	// MSG_CODE_DATA_INVALID is data invalid
	MSG_CODE_DATA_INVALID

	// MSG_CODE_IDEN_INVALID is data invalid
	MSG_CODE_IDEN_INVALID

	// MSG_CODE_DATABASE_ERROR is update mongo failed
	MSG_CODE_DATABASE_ERROR

	// MSG_CODE_NOT_FOUND_SETTING is not found setting from mongo
	MSG_CODE_NOT_FOUND_SETTING

	// MSG_CODE_NOT_FOUND_FILE is not found firmware from mongo
	MSG_CODE_NOT_FOUND_FILE

	// MSG_CODE_READ_FILE_FAILED is not found firmware from mongo
	MSG_CODE_READ_FILE_FAILED

	// MSG_CODE_LOCK_DEVICE_FAILED is not found firmware from mongo
	MSG_CODE_LOCK_DEVICE_FAILED

	// MSG_CODE_UNKNOWN_SIM is accepted search sim from cucc and cmcc
	MSG_CODE_UNKNOWN_SIM

	// MSG_CODE_WAIT_SIM is accepted search sim from cucc and cmcc, wait response
	MSG_CODE_WAIT_SIM

	MSG_CODE_REMOTE_SERVICE_FAILED
)

