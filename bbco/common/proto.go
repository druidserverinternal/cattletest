package common

import (
	"encoding/binary"
	//  "bytes"
	"github.com/golang/protobuf/proto"

	log "github.com/sirupsen/logrus"
)

// ParsePBMsg parse protobuf msg
func ParsePBMsg(data []byte, msg proto.Message) proto.Message {
	ret := proto.Unmarshal(data[MSG_HEAD_SIZE:], msg)
	if ret != nil {
		log.Error("Parse pb failed, ", ret)
		return nil
	}
	return msg
}

func MakePBMsg(code uint32, msg proto.Message) []byte {
	data, err := proto.Marshal(msg)
	if err != nil {
		log.Error("Encode pb failed:", err)
		return []byte{}
	}

	bodyLen := len(data)
	cache := make([]byte, bodyLen+MSG_HEAD_SIZE)
	binary.LittleEndian.PutUint32(cache[0:4], code)
	binary.LittleEndian.PutUint32(cache[4:8], uint32(bodyLen))
	copy(cache[MSG_HEAD_SIZE:], data)
	return cache
}
