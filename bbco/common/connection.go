package common

import (
	"net"
	"errors"

	"github.com/spf13/viper"
)


const PushConnectionKey = "PushConnection"

var pushConn = (*net.Conn)(nil)

func PushConnection() error {
	if address := viper.GetString("push_address"); address != "" {
		conn, err := net.Dial("udp", address)
		if err != nil {
			return errors.New("Dial to push server failed, address:" + address)
		}
		pushConn = &conn
		return nil
	}
	return errors.New("Did not found push server.")
}

func GetPushConnection() (*net.Conn, error) {
	if pushConn == nil {
		if err := PushConnection(); err != nil {
			return nil, err
		}
	}
	return pushConn, nil
}
