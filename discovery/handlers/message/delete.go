package message

import (
//	"druid/cattle/discovery/handlers/common"
	"druid/cattle/discovery/middleware"
	"druid/cattle/models"
//	"fmt"
//	"time"
	"net/http"

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"
)


func Delete(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	userID := bson.ObjectIdHex(middleware.GetUserID(c))
//	me := models.FindUserByID(db, userID)

	id := c.Param("id")
	if !bson.IsObjectIdHex(id) {
		logger.Error("Message id is invalid.")
		c.AbortWithError(http.StatusBadRequest, models.Error("Message id is invalid:", id))
		return
	}

	m, err := models.FindMessageByID(db, bson.ObjectIdHex(id))
	if err != nil {
		logger.Error("Find message by id failed.", err)
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	if m != nil {
		if m.Dst != userID {
			logger.Error("You don't have permission to access.")
			c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access."))
			return
		}
		if err := m.Delete(db); err != nil {
			logger.Error("Delete message failed.", err)
			c.AbortWithError(http.StatusInternalServerError, models.Error("Delete message failed."))
			return
		}
	}
	c.Status(http.StatusNoContent)
}

