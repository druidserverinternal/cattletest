package gps

import (
	"net/http"
	"strconv"
	

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

// List all of gps record with device
func List(c *gin.Context) {
	db := middleware.GetDB(c)
//	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	rp := models.ParseRequestParameter(c)
	m := bson.M{
		"company_id": user.CompanyID,
		"deleted_at": nil,
		"owner_id": user.ID,
	}

	result, err := models.ListGPSByFilter(db, m, rp, models.UserSelect)
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, models.Error("List gps by filter failed." + err.Error()))
		return
	}

	count := 0
	if len(result) > 0 {
		count, _ = models.CountGPSByFilter(db, m)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, result)
}
