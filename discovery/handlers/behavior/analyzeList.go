package behavior

import (
	"druid/cattle/discovery/middleware"
	"druid/cattle/models"
	"encoding/json"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

func AnanlyzeListValidate(c *gin.Context) *models.BehaviorAnalyzeReq {
	var behaviorAnalyzeReq models.BehaviorAnalyzeReq
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&behaviorAnalyzeReq); err != nil {
		//		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		//		return nil
	}
	/*
		if bson.IsObjectIdHex(AnanlyzeListValidate.CompanyID) == false {
			c.AbortWithError(http.StatusBadRequest, models.Error("CompanyID must be a valid CompanyID."))
			return nil
		}
	*/
	return &behaviorAnalyzeReq
}

func AnanlyzeList(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	behaviorAnalyzeReq := AnanlyzeListValidate(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	rp := models.ParseRequestParameter(c)
	if rp.LimitOrign == 0 {
		rp.Limit = 0
	}

	count := 0
	devices := []*models.DeviceMerge{}
	var err error
	if len(behaviorAnalyzeReq.DeviceIDs) < 1 {

		m := bson.M{"owner_id": user.ID}
		if behaviorAnalyzeReq.RoomID != "" {
			m["room_id"] = behaviorAnalyzeReq.RoomID
		}

		if behaviorAnalyzeReq.Survive != 0 { //models.SurviveLoving
			m["survive"] = behaviorAnalyzeReq.Survive
		}

		if behaviorAnalyzeReq.Species != -1 {
			if behaviorAnalyzeReq.Species == 0 {
				m["species"] = bson.M{"$ne": 1}
			} else {
				m["species"] = behaviorAnalyzeReq.Species
			}
		}

		devices, err = models.ListDeviceMergeByFilter(db, m, rp, nil)
		if err != nil {
			c.AbortWithError(http.StatusInternalServerError, models.Error("Can't find devices "))
			return
		}
		count, err = models.CountDeviceByFilter(db, m)
		if err != nil {
			logger.Error("Count Device Merage By Filter err :", err)
		}

	} else {
		m := bson.M{"owner_id": user.ID, "_id": bson.M{"$in": behaviorAnalyzeReq.DeviceIDs}}

		devices, err = models.ListDeviceMergeByFilter(db, m, rp, nil)
		if err != nil {
			c.AbortWithError(http.StatusInternalServerError, models.Error("Can't find devices "))
			return
		}
		if len(devices) != len(behaviorAnalyzeReq.DeviceIDs) {
			c.AbortWithError(http.StatusInternalServerError, models.Error("Can't find devices "))
			return
		}
	}

	BehaviorAnalyzeresp := [][]*models.BehaviorTypeHistory{}

	timeS := time.Now().Unix()/3600*3600 - 3600*24 //默认查看一天的数据
	timeE := time.Now().Unix() / 3600 * 3600

	if behaviorAnalyzeReq.TimeStart != "" {
		timeStart, err := models.ConvertStringToTime(behaviorAnalyzeReq.TimeStart)
		if err == nil {
			timeS = timeStart.Unix() / 3600 * 3600
		}
	}

	if behaviorAnalyzeReq.TimeEnd != "" {
		timeEnd, err := models.ConvertStringToTime(behaviorAnalyzeReq.TimeEnd)
		if err == nil {
			timeE = timeEnd.Unix() / 3600 * 3600
		}
	}

	AddTime := uint32(3600)
	if behaviorAnalyzeReq.TimeCell != 0 {
		AddTime = uint32(3600 * behaviorAnalyzeReq.TimeCell)
	}

	//获取所有 优化减少读取数据库的次数，但是增加了内存使用
	allDevicesIds := []bson.ObjectId{}
	for _, thisDevice := range devices {
		allDevicesIds = append(allDevicesIds, thisDevice.ID)
	}
	allBehaviorTypeHistorys, _, err := models.ListBehaviorTypeHistoryRangeManyID(db, allDevicesIds, uint32(timeS), uint32(timeE), nil)
	if err != nil {
		logger.Error("List BehaviorAnalyze History failed")
		c.AbortWithError(http.StatusInternalServerError, models.Error("List BehaviorAnalyze History failed"))
		return
	}

	//如果没有一个数据，还是添加一个空，保证是二维
	if len(allBehaviorTypeHistorys) == 0 {
		DBhaviorHistorys := []*models.BehaviorTypeHistory{}
		BehaviorAnalyzeresp = append(BehaviorAnalyzeresp, DBhaviorHistorys)
	}

	//按deviceID 拆分
	start_i := 0
	for ii := 0; ii < len(allBehaviorTypeHistorys); {

		flog := 0
		for kk := start_i; kk < len(allBehaviorTypeHistorys); kk++ {
			if allBehaviorTypeHistorys[kk].DeviceID != allBehaviorTypeHistorys[start_i].DeviceID {
				ii = kk
				flog = kk
				break
			}
			if kk == len(allBehaviorTypeHistorys)-1 {
				ii = kk + 1
				flog = kk + 1
				break
			}
		}
		behaviorTypeHistorys := allBehaviorTypeHistorys[start_i:flog]
		start_i = flog

		if len(behaviorTypeHistorys) < 1 {
			continue
		}
		device := models.DeviceMerge{}
		for _, thisDevice := range devices {
			if thisDevice.ID == behaviorTypeHistorys[0].DeviceID {
				device = *thisDevice
			}
		}

		//分析时间
		DBhaviorHistorys := []*models.BehaviorTypeHistory{}

		if len(behaviorTypeHistorys) == 0 {
			BehaviorAnalyzeresp = append(BehaviorAnalyzeresp, DBhaviorHistorys)
			continue
		}
		//处理之前的数据
		timeThis := uint32(timeS)
		for {
			if timeThis+AddTime <= behaviorTypeHistorys[0].TimeNum {
				cellBeh := models.BehaviorTypeHistory{}
				cellBeh.TimeNum = timeThis
				cellBeh.DeviceID = device.ID
				cellBeh.Mark = device.Mark
				cellBeh.NickName = device.NickName
				DBhaviorHistorys = append(DBhaviorHistorys, &cellBeh)
				timeThis = timeThis + AddTime
			} else {
				break
			}
		}

		newCell := &(models.BehaviorTypeHistory{})
		for i := 0; i < len(behaviorTypeHistorys); i++ {
			if timeThis+AddTime <= behaviorTypeHistorys[i].TimeNum {
				newCell.TimeNum = timeThis
				newCell.CompanyID = device.CompanyID
				newCell.CompanyName = device.CompanyName
				newCell.DeviceID = device.ID
				newCell.Mark = device.Mark
				newCell.NickName = device.NickName
				DBhaviorHistorys = append(DBhaviorHistorys, newCell)

				newCell = &(models.BehaviorTypeHistory{})
				newCell.MotionEatNum = 0
				newCell.MotionOtherHightNum = 0
				newCell.MotionOtherLowNum = 0
				newCell.MotionOtherMediumNum = 0
				newCell.MotionRuminationNum = 0
				timeThis = timeThis + AddTime
			}

			newCell.MotionEatNum = newCell.MotionEatNum + behaviorTypeHistorys[i].MotionEatNum
			newCell.MotionRuminationNum = newCell.MotionRuminationNum + behaviorTypeHistorys[i].MotionRuminationNum
			newCell.MotionOtherLowNum = newCell.MotionOtherLowNum + behaviorTypeHistorys[i].MotionOtherLowNum
			newCell.MotionOtherMediumNum = newCell.MotionOtherMediumNum + behaviorTypeHistorys[i].MotionOtherMediumNum
			newCell.MotionOtherHightNum = newCell.MotionOtherHightNum + behaviorTypeHistorys[i].MotionOtherHightNum

			if i == len(behaviorTypeHistorys)-1 {
				newCell.TimeNum = timeThis
				newCell.CompanyID = device.CompanyID
				newCell.CompanyName = device.CompanyName
				newCell.DeviceID = device.ID
				newCell.Mark = device.Mark
				newCell.NickName = device.NickName
				DBhaviorHistorys = append(DBhaviorHistorys, newCell)
			}
		}

		BehaviorAnalyzeresp = append(BehaviorAnalyzeresp, DBhaviorHistorys)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, BehaviorAnalyzeresp)

}
