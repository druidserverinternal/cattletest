package behavior

import (
	"druid/cattle/discovery/middleware"
	"druid/cattle/models"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)


func GetOBDA(c *gin.Context) {

	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	deviceID := c.Param("id")
	timeNum := c.Param("num")
	if deviceID == "" || timeNum == "" {
		c.AbortWithError(http.StatusBadRequest, models.Error("DeviceID or timeType is invalid:"+deviceID+"  timeNum :"+timeNum))
		return
	}

	t, err := strconv.Atoi(timeNum)
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("DeviceID or timeType is invalid:"+deviceID+"  timetype :"+timeNum))
		return
	}
	if t > 24 * 7 * 31 {
		c.AbortWithError(http.StatusBadRequest, models.Error("time is too large:", t))
		return
	}

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	rp := models.ParseRequestParameter(c)
	if rp.LimitOrign == 0 {
		rp.Limit = 0
	}
	rp.Sort = []string{"timestamp"}

	thisDevice, err := models.FindDeviceByID(db, bson.ObjectIdHex(deviceID), nil)
	if err != nil || thisDevice.OwnerID != user.ID {
		c.AbortWithError(http.StatusInternalServerError, models.Error("Can't find devices "))
		return
	}
	if thisDevice.LastBeh2Timestamp == nil {
		data := []*models.Behavior2{}
		c.JSON(http.StatusOK, data)
		return
	}

	h, _ := time.ParseDuration("-" + timeNum + "h")
	timeS := thisDevice.LastBeh2Timestamp.Add(h)

	m := bson.M{"device_id": thisDevice.ID, "timestamp": bson.M{"$gte": timeS, "$lte": time.Now().AddDate(0, 0, 1) }}
	s := bson.M{"device_id":1, "odba":1, "timestamp":1}

	allODBA, err := models.ListBehavior2ByFilter(db, m, rp, s)
	if err != nil {
		logger.Error("List Beh2 History failed:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List Beh2 History failed", err))
		return
	}

	c.JSON(http.StatusOK, allODBA)
}
