package user

import (
	"encoding/json"
	"net/http"
	//"regexp"
	"strings"
	
	"github.com/gin-gonic/gin"

	"github.com/spf13/viper"

	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

)

func loginValidate(c *gin.Context) *models.User {
	logger := middleware.GetLogger(c)
	var user models.User
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&user); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	user.UserName = strings.TrimSpace(user.UserName)
	if !models.RegexpUser.MatchString(user.UserName) {
		logger.Error( "Username is invalid.", user.UserName)
		c.AbortWithError(http.StatusBadRequest, models.Error("username must be a valid name."))
		return nil
	}

	user.Password = strings.TrimSpace(user.Password)
	if !models.RegexpPwd.MatchString(user.Password) {
		logger.Error( "Password is invalid.", user.Password)
		c.AbortWithError(http.StatusBadRequest, models.Error("password must be a valid password."))
		return nil
	}

	return &user
}

// Login in with username and sha256 password
func Login(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	reqUser := loginValidate(c)
	if reqUser == nil {
		return
	}

	user, err := models.FindUserByUserName(db, reqUser.UserName)
	if err != nil {
		logger.Error( "Find username failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find username failed!" + err.Error()))
		return
	}
	if user == nil {
		logger.Error( "The user was invalid.")
		c.AbortWithError(http.StatusBadRequest, models.Error("The user was invalid."))
		return
	}

	if user.Role != models.RoleUser {
		logger.Error("You don't have permission to login.", user)
		c.AbortWithError(http.StatusBadRequest, models.Error("You don't have permission to login."))
		return
	}

	if err := models.PasswordVerify(reqUser.Password, user.HashedPassword); err != nil {
		logger.Error( "The password was invalid.")
		c.AbortWithError(http.StatusBadRequest, models.Error("Password is invalid."))
		return
	}

	if !user.CheckTokenValid() {
		token := models.NewToken(user.ID, viper.GetString("authentication.secret"))
		user.Token = token.String()
		user.Expiry = &token.Expiry

		if err = user.UpdateToken(db); err != nil {
		logger.Error( "Update token failed. ", err)
			c.AbortWithError(http.StatusBadRequest, models.Error("Update token failed:" + err.Error()))
			return
		}
		logger.Infof(
			"User:%s update new token: %s in mongo.",
			user.UserName, token.String())
	}

	c.Header(middleware.AuthenticationHeader,user.Token)
	c.JSON(http.StatusOK, user)
}

