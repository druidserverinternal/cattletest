package user

import (
	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	"encoding/json"
	"strings"
	"net/http"
	
	"github.com/gin-gonic/gin"

	"gopkg.in/mgo.v2/bson"

)

func updateInfoValidate(c *gin.Context) (*models.User) {
	var user models.User
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&user); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	user.Address = strings.TrimSpace(user.Address)
	if user.Address == "" {
		c.AbortWithError(http.StatusBadRequest, models.Error("address must be a valid value."))
		return nil
	}


	user.Phone = strings.TrimSpace(user.Phone)
	if user.Phone != "" && !models.RegexpPhone.MatchString(user.Phone) {
		c.AbortWithError(http.StatusBadRequest, models.Error("phone must be a valid phone."))
		return nil
	}

	user.IDCard = strings.TrimSpace(user.IDCard)
	if user.IDCard != "" && !models.RegexpIDCard.MatchString(user.IDCard) {
		c.AbortWithError(http.StatusBadRequest, models.Error("idcard must be a valid value."))
		return nil
	}


	return &user
}

// UpdateInfo myself password
func UpdateInfo(c *gin.Context) {
	db := middleware.GetDB(c)
	//	logger := middleware.GetLogger(c)

	userReq := updateInfoValidate(c)
	if userReq == nil {
		return
	}

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	userReq.ID = user.ID

	if err := userReq.UpdateInfo(db); err != nil {
		c.AbortWithError(http.StatusInternalServerError, models.Error("Update user info failed." + err.Error()))
		return
	}

	c.Status(http.StatusCreated)
}
