package user

import (
	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	"encoding/json"
	"strings"
	"net/http"
	
	"github.com/gin-gonic/gin"

	"github.com/spf13/viper"
	"gopkg.in/mgo.v2/bson"
)

func updatePasswordValidate(c *gin.Context) (*models.User) {
	var user models.User
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&user); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	user.Password = strings.TrimSpace(user.Password)
	if !models.RegexpPwd.MatchString(user.Password) {
		c.AbortWithError(http.StatusBadRequest, models.Error("password must be a valid password."))
		return nil
	}
	user.OldPassword = strings.TrimSpace(user.OldPassword)
	if !models.RegexpPwd.MatchString(user.OldPassword) {
		c.AbortWithError(http.StatusBadRequest, models.Error("old password must be a valid password."))
		return nil
	}

	return &user
}

// UpdatePassword myself password
func UpdatePassword(c *gin.Context) {
	db := middleware.GetDB(c)
	//logger := middleware.GetLogger(c)

	userReq := updatePasswordValidate(c)
	if userReq == nil {
		return
	}

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if err := models.PasswordVerify(userReq.OldPassword, user.HashedPassword); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("The verify password is invalid!"))
		return
	}

	if !userReq.HashPassword() {
		c.AbortWithError(http.StatusBadRequest, models.Error("The password is invalid."))
		return
	}

	userReq.ID = user.ID
	token := models.NewToken(user.ID, viper.GetString("authentication.secret"))
	userReq.Token = token.String()
	userReq.Expiry = &token.Expiry

	if err := userReq.UpdatePassword(db); err != nil {
		c.AbortWithError(http.StatusInternalServerError, models.Error("Update user password failed." + err.Error()))
		return
	}

	c.Status(http.StatusCreated)
}

