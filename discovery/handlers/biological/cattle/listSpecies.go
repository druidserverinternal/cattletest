package cattle

import (
	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	"net/http"

	"github.com/gin-gonic/gin"
)

// ListSpecies list cattle species 
func ListSpecies(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	species, err := models.ListBiologicalSpecies(db, models.BiologicalTypeCattle)
	if err != nil {
		logger.Error("List cattle Species failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List cattle Species failed.", err))
		return
	}

	c.JSON(http.StatusOK, species)
}


