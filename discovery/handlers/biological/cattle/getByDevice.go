package cattle

import (
	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	"net/http"

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"
)

// GetByDevice get a biological record by device id
func GetByDevice(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	id := c.Param("id")
	if !bson.IsObjectIdHex(id) {
		c.AbortWithError(http.StatusBadRequest, models.Error("Cattle id is invalid:", id))
		return
	}

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	device, err := models.CheckValidDeviceByID(db, bson.ObjectIdHex(id), nil)
	if err != nil {
		logger.Warning("Find device by id failed.", err)
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	if me.Role == models.RoleUser && device.OwnerID != me.ID {
		logger.Warning("You don't have permission to access.")
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access."))
		return
	}

	cattle, err := models.FindCattleByDeviceID(db, bson.ObjectIdHex(id))
	if err != nil {
		logger.Warning("Find cattle by id failed.", err)
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	if cattle == nil {
		c.Status(http.StatusOK)
		return
	}

	c.JSON(http.StatusOK, cattle)
}
