package cattle

import (
	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"
	//	"io/ioutil"
	"net/http"
	//	"strconv"
	//	"strings"
)

// DeleteImage upload image to biological
func Delete(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	id := c.Param("id")
	if !bson.IsObjectIdHex(id) {
		c.AbortWithError(http.StatusBadRequest, models.Error("Cattle id is invalid:", id))
		return
	}

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	cattle, err := models.FindCattleByID(db, bson.ObjectIdHex(id))
	if err != nil {
		logger.Warning("Find cattle by id failed.", err)
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	if cattle == nil {
		logger.Warning("Can't find cattle.")
		c.AbortWithError(http.StatusNotFound, models.Error("Can't find cattle."))
		return
	}

	if me.Role == models.RoleUser && cattle.OwnerID != me.ID {
		logger.Warning("You don't have permission to access.")
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access."))
		return
	}
	if err := models.DeleteImageByIDs(db, cattle.Images); err != nil {
		logger.Error("Remove image from filefs failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Remove image to biological failed.", err))
		return
	}

	if err := cattle.RemoveImages(db, cattle.Images); err != nil {
		logger.Error("Insert image to biological failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Insert image to biological failed.", err))
		return
	}

	if cattle.DeviceID != "" {
		device, err := models.CheckValidDeviceByID(db, cattle.DeviceID, nil)
		if err != nil {
			logger.Warning("Find device by cattle id failed.", err)
			c.AbortWithError(http.StatusInternalServerError, models.Error("Find device by cattle id failed.", err))
			return
		}

		if err := device.RemoveBiological(db); err != nil {
			logger.Error("Remove biological failed.", err)
			c.AbortWithError(http.StatusInternalServerError, models.Error("Remove device biological failed.", err))
			return
		}
	}

	if err := cattle.Remove(db); err != nil {
		logger.Error("Remove biological failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Remove biological failed.", err))
		return
	}

	c.Status(http.StatusNoContent)
}
