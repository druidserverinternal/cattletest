package cattle

import (
	"druid/cattle/discovery/middleware"
	"druid/cattle/models"
//	"encoding/json"

	"net/http"
	"time"

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"
)

func createValidate(c *gin.Context) (*models.Cattle) {
	var cattle models.Cattle
	if err := c.BindJSON(&cattle); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}
	if cattle.Gender < models.BiologicalGenderUnknown && cattle.Gender > models.BiologicalGenderFemale {
		c.AbortWithError(http.StatusBadRequest, models.Error("Gender is invalid value.", cattle.Gender))
		return nil
	}


	return &cattle
}

// Upload a bird record
func Create(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	cattle := createValidate(c)
	if cattle == nil {
		return
	}

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	cattle.OwnerID = me.ID

	var device *models.Device
	if cattle.DeviceID != "" {
		var err error
		device, err = models.CheckValidDeviceByID(db, cattle.DeviceID, nil)
		if err != nil {
			logger.Warning("Find valid device by id failed.", cattle.DeviceID)
			c.AbortWithError(http.StatusBadRequest, models.Error("Find valid device by id failed.", cattle.DeviceID))
			return
		}
		if device.OwnerID != cattle.OwnerID {
			logger.Warning("Cant' bind different user's cattle.")
			c.AbortWithError(http.StatusForbidden, models.Error("Cant' bind different user's cattle."))
			return
		}
	}


	now := time.Now()
	cattle.UpdatedAt = &now
//	cattle.CompanyID = me.CompanyID


	cattle.ID = bson.NewObjectId()
	if err := cattle.Create(db); err != nil {
		logger.Warning("Create cattle biological failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Create cattle biological failed.", err))
		return
	}

	if device != nil {
		device.BiologicalType = models.CattleCollection
		device.BiologicalID = cattle.ID
		device.NickName = cattle.NickName

		if err := device.UpdateBiological(db); err != nil {
			logger.Warning("Update device cattle biological failed.", err)
			c.AbortWithError(http.StatusInternalServerError, models.Error("Update device cattle biological failed.", err))
			return
		}
	}

	c.JSON(http.StatusOK, cattle)
}
