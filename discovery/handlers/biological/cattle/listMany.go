package cattle

import (
	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	"strconv"
	"net/http"
	"encoding/json"

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"
)


func listManyValidate(c *gin.Context) (*models.DeviceMany) {
	devices := models.DeviceMany{}
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&devices); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	if len(devices.ID) == 0 {
		c.AbortWithError(http.StatusBadRequest, models.Error("Request devices id are nil."))
		return nil
	}

	return &devices
}

// ListMany biological record
func ListMany(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	devices := listManyValidate(c)
	if devices == nil {
		return
	}

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	m := bson.M{
		"device_id": bson.M{"$in": devices.ID },
		"company_id": me.CompanyID,
	}
	if me.Role == models.RoleUser {
		m["owner_id"] = me.ID
	}

	cattle, err := models.ListCattleByFilter(db, m, nil)
	if err != nil {
		logger.Warning("List cattle by id failed.", err)
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	count := 0
	if len(cattle) > 0 {
		count, err = models.CountCattleByFilter(db, m)
	}
	c.Header(models.ResultCountHeader, strconv.Itoa(count))

	c.JSON(http.StatusOK, cattle)
}
