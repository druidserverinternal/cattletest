package cattle

import (
	"druid/cattle/discovery/middleware"
	"druid/cattle/models"
	"encoding/json"

	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func updateByMarkValidate(c *gin.Context) (*models.Cattle) {
	var cattle models.Cattle
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&cattle); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	id := c.Param("id")

	cattle.Mark, _ = strconv.Atoi(id)

	return &cattle
}

// UpdateByMark update a cattle record by mark
func UpdateByMark(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	cattle := updateByMarkValidate(c)
	if cattle == nil {
		return
	}

	if err := cattle.UpdateByMark(db); err != nil {
		logger.Error("Update cattle biological failed.", err, cattle)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Update cattle biological failed.", err))
		return
	}

	c.Status(http.StatusNoContent)
}
