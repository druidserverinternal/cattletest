package cattle

import (
	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	"net/http"

	"github.com/gin-gonic/gin"
)

// GetSpecies list cattle species 
func GetSpecies(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	t := c.Param("species")
	if t == "" {
		logger.Error("Get cattle Species failed.")
		c.AbortWithError(http.StatusBadRequest, models.Error("Get cattle Species failed."))
		return
	}


	species, err := models.ListCattleBySpecies(db, t)
	if err != nil {
		logger.Error("Get cattle Species failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Get cattle Species failed.", err))
		return
	}

	c.JSON(http.StatusOK, species)
}


