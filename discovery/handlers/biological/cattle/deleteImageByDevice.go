package cattle

import (
	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"
	//	"io/ioutil"
	"net/http"
	//	"strconv"
	//	"strings"
)

// DeleteImageByDevice delete image by id
func DeleteImageByDevice(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	imageID := c.Param("image_id")
	if !bson.IsObjectIdHex(imageID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("Image id is invalid:", imageID))
		return
	}

	deviceID := c.Param("id")
	if !bson.IsObjectIdHex(deviceID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("Device id is invalid:", deviceID))
		return
	}

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	device, err := models.CheckValidDeviceByID(db, bson.ObjectIdHex(deviceID), nil)
	if err != nil {
		logger.Warning("Find device by id failed.", err)
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	if me.Role == models.RoleUser && device.OwnerID != me.ID {
		logger.Warning("You don't have permission to access.")
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access."))
		return
	}

	cattle, err := models.FindCattleByDeviceID(db, device.ID)
	if err != nil {
		logger.Warning("Find cattle by id failed.", err)
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	if cattle == nil {
		logger.Warning("Can't find cattle.")
		c.AbortWithError(http.StatusNotFound, models.Error("Can't find cattle."))
		return
	}

	image, err := models.FindImageByID(db, bson.ObjectIdHex(imageID))
	if err != nil {
		logger.Error("Find image from request id failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find image from request id failed, ", err))
		return
	}
	if image != nil {
		if err := image.Delete(db); err != nil {
			logger.Error("Remove image obj from db failed.", err, image)
			c.AbortWithError(http.StatusInternalServerError, models.Error("Remove image obj from db failed.", err))
			return
		}
	}

	if err := cattle.RemoveImage(db, bson.ObjectIdHex(imageID)); err != nil {
		logger.Error("Remove image from biological failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Remove image from biological failed.", err))
		return
	}

	c.Status(http.StatusNoContent)
}
