package cattle

import (
	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	"net/http"
	"encoding/json"
	"strings"
	"time"
	

	"gopkg.in/mgo.v2/bson"

	"github.com/gin-gonic/gin"
)

func wxImageValidate(c *gin.Context) (*models.WeixinMedia, bson.ObjectId) {
	wm := models.WeixinMedia{}
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&wm); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil, ""
	}

	wm.Type = strings.TrimSpace(wm.Type)
	if wm.Type == "" || wm.Type != "image" {
		c.AbortWithError(http.StatusBadRequest, models.Error("Request type is invalid."))
		return nil, ""
	}

	wm.MediaID = strings.TrimSpace(wm.MediaID)
	if wm.MediaID == "" {
		c.AbortWithError(http.StatusBadRequest, models.Error("Request media id is nil."))
		return nil, ""
	}

	/*
	wm.Name = strings.TrimSpace(wm.Name)
	if wm.Name == "" {
		c.AbortWithError(http.StatusBadRequest, models.Error("Request name is nil."))
		return nil, ""
	}
	*/

	id := c.Param("id")
	if !bson.IsObjectIdHex(id) {
		c.AbortWithError(http.StatusBadRequest, models.Error("Cattle id is invalid:", id))
		return nil, ""
	}


	return &wm, bson.ObjectIdHex(id)
}

func WeixinImage(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	wm, cattleID := wxImageValidate(c)
	if wm == nil {
		return
	}

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	weixin, err := models.FindWeixinByCompanyID(db, me.CompanyID)
	if err != nil {
		logger.Warning("Find weixin by companyID failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find weixin by companyID failed:", err))
		return
	}
	if weixin == nil {
		logger.Warning("Your company not support weixin.", err)
		c.AbortWithError(http.StatusNotFound, models.Error("Your company not support weixin."))
		return
	}

	cattle, err := models.FindCattleByID(db, cattleID)
	if err != nil {
		logger.Warning("Find cattle by id failed.", err)
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	if cattle == nil {
		logger.Warning("Can't find cattle.")
		c.AbortWithError(http.StatusNotFound, models.Error("Can't find cattle."))
		return
	}


	if len(cattle.Images) >= models.BiologicalMaxImages {
		logger.Warning("The cattle images over limit.")
		c.AbortWithError(http.StatusBadRequest, models.Error("The cattle images over limit."))
		return
	}

	switch weixin.IsExpiry() {
	case true:
		t := time.Now().Add(models.WaitTokenRefreshSecond * time.Second)
		if weixin, err = models.RefreshWeixinToken(db, weixin.CompanyID, &t); err != nil {
			logger.Warning("Refresh token failed.", err)
			c.AbortWithError(http.StatusInternalServerError, models.Error("Refresh token failed.", err))
			return
		}
	}

	if err := wm.DownloadImage(db, weixin.AccessToken); err != nil {
		logger.Warning("Download image from weixin server failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Download image from weixin server failed.", err))
		return
	}
	image := &models.Image {
		Data	:	wm.Data,
		Size	:	len(wm.Data),
		Name	:	wm.Name,
	}

//	image.CompanyID = me.CompanyID
//	image.CompanyName = me.CompanyName
	image.OwnerID = cattle.OwnerID
	image.UploadBy = me.ID

	if err := image.UploadImage(db); err != nil {
		logger.Error(err)
		c.AbortWithError(http.StatusInternalServerError, err)
		return
		
	}

	if err := cattle.InsertImage(db, image); err != nil {
		image.Delete(db)
		logger.Warning("Insert image to cattle failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Insert image to cattle failed.", err))
		return
	}

	c.Status(http.StatusCreated)
}
