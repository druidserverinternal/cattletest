package cattle

import (
	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	"strconv"
	"net/http"

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"
)

// ListIdle biological record
func ListIdle(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if me.Role != models.RoleUser {
		logger.Warning("This is user's api.")
		c.AbortWithStatus(http.StatusForbidden)
		return
	}

	m := bson.M{"company_id": me.CompanyID, "device_id": nil, "owner_id": me.ID}

	cattle, err := models.ListCattleByFilter(db, m, models.ParseRequestParameter(c))
	if err != nil {
		logger.Warning("List cattle by id failed.", err)
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	count := 0
	if len(cattle) > 0 {
		count, err = models.CountCattleByFilter(db, m)
	}
	c.Header(models.ResultCountHeader, strconv.Itoa(count))

	c.JSON(http.StatusOK, cattle)
}
