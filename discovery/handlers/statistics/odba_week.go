package statistics

import (
	"druid/cattle/discovery/middleware"
	"druid/cattle/models"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

func ODBAByWeek(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	year, mon, day := time.Now().Date()
	date := time.Date(year, mon, day, 0, 0, 0, 0, time.UTC)

	last := date.AddDate(0, 0, -1)
	d := last.Weekday()
	if d != time.Sunday {
		last = last.AddDate(0, 0, -int(d))
	}
	begin := last.AddDate(0, 0, -6)
	s_date := models.ConvertTimeToDate(&begin)
	//end := last.AddDate(0, 0, 1)

	//m := bson.M{"owner_id": me.ID, "date": bson.M{"$gte": begin, "$lt": end}}
	m := bson.M{"owner_id": me.ID, "date": s_date, "type": models.StatisticsODBATypeWeek }

	rp := &models.RequestParameter{
		Sort: []string{"-odba"},
		Limit: 10,
	}

	odbas, err := models.ListStatisticsODBAByFilter(db, m, rp, nil) 
	if err != nil {
		logger.Errorln("Group odbas failed:",err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Group odbas failed:"))
		return
	}

	lastDate := begin.AddDate(0,0,-7)

	err = ODBAGetChangeIndex(db,&lastDate,models.StatisticsODBATypeWeek,odbas)
	if err != nil {
		logger.Errorln("List odbas failed:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List last odbas failed:"))
		return
	}

	c.JSON(http.StatusOK, odbas)

	/*
	ds := []bson.ObjectId{}
	for _, o := range odbas {
		ds = append(ds, o.ID)
	}

	mm := bson.M{"_id": bson.M{"$in": ds}}
	devices, err := models.ListDeviceMergeByFilter(db, mm, nil, nil) 
	if err != nil {
		logger.Errorln("List device merge failed:",err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List device merge failed:"))
		return
	}

	for _, o := range odbas {
		for _, device := range devices {
			if o.ID == device.ID {
				o.Mark = device.Mark
			}
		}
	}
	
	c.JSON(http.StatusOK, odbas)
	*/
}
