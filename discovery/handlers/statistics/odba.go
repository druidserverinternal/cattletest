package statistics

import (
	"druid/cattle/discovery/middleware"
	"druid/cattle/models"
	"net/http"

	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

func Ananlyze(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	deviceID := c.Param("id")
	timeType := c.Param("type")
	if deviceID == "" || timeType == "" {
		c.AbortWithError(http.StatusBadRequest, models.Error("DeviceID or timeType is invalid:"+deviceID+"  timetype :"+timeType))
		return
	}
	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	rp := models.ParseRequestParameter(c)
	if rp.LimitOrign == 0 {
		rp.Limit = 0
	}

	thisDevice, err := models.FindDeviceMergeByID(db, bson.ObjectIdHex(deviceID), nil)
	if err != nil || thisDevice.OwnerID != user.ID {
		c.AbortWithError(http.StatusInternalServerError, models.Error("Can't find devices "))
		return
	}
	if thisDevice.LastBeh2Timestamp == nil {
		data := []*models.StatisticsODBA{}
		c.JSON(http.StatusOK, data)
		return
	}

	timeE := thisDevice.LastBeh2Timestamp.Unix() / 3600 * 3600
	timeS := timeE
	switch timeType {
	case "day":
		timeS = timeE - 3600*24
	case "week":
		timeS = timeE - 3600*24*7
	case "month":
		timeS = timeE - 3600*24*30
	default:
		{
			c.AbortWithError(http.StatusBadRequest, models.Error(" timeType is invalid:"+timeType))
			return
		}
	}

	allODBAHistorys, _, err := models.ListODBAParameterRange(db, thisDevice.ID, uint32(timeS), uint32(timeE), nil)
	if err != nil {
		logger.Error("List BehaviorAnalyze History failed")
		c.AbortWithError(http.StatusInternalServerError, models.Error("List BehaviorAnalyze History failed"))
		return
	}

	for i := 0; i < len(allODBAHistorys); i++ {
		allODBAHistorys[i].Mark = thisDevice.Mark
		allODBAHistorys[i].NickName = thisDevice.NickName
	}
	c.JSON(http.StatusOK, allODBAHistorys)

}
