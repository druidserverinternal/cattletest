package statistics

import (
	"druid/cattle/discovery/middleware"
	"druid/cattle/models"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

func ODBAByUp(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	y, mon, d := time.Now().AddDate(0, 0, -1).Date()
	date := time.Date(y, mon, d, 0, 0, 0, 0, time.UTC)

	s_date := models.ConvertTimeToDate(&date)
	m := bson.M{"owner_id": me.ID, "date": s_date, "type": models.StatisticsODBATypeDay}

	rp := &models.RequestParameter{
		Sort: []string{"-odba"},
	}

	odbas, err := models.ListStatisticsODBAByFilter(db, m, rp, nil)
	if err != nil {
		logger.Errorln("List odbas failed:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List odbas failed:"))
		return
	}

	lastDate := date.AddDate(0, 0, -1)

	err = ODBAGetChangeIndex(db, &lastDate, models.StatisticsODBATypeDay, odbas)
	if err != nil {
		logger.Errorln("List odbas failed:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List last odbas failed:"))
		return
	}

	up(odbas)

	if len(odbas) < 10 {
		c.JSON(http.StatusOK, odbas)
		return
	}
	c.JSON(http.StatusOK, odbas[:10])
}

func up(odbas []*models.StatisticsODBA) {

	for k := 0; k < 10; k++ {
		for i := len(odbas) - 1; i > 0+k; i-- {
			if odbas[i].ChangeIndex > odbas[i-1].ChangeIndex {
				o := odbas[i]
				odbas[i] = odbas[i-1]
				odbas[i-1] = o
			}
		}
	}
}
