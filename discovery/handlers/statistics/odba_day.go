package statistics

import (
	"druid/cattle/discovery/middleware"
	"druid/cattle/models"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func ODBAByDay(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	y, mon, d := time.Now().AddDate(0, 0, -1).Date()
	date := time.Date(y, mon, d, 0, 0, 0, 0, time.UTC)

	s_date := models.ConvertTimeToDate(&date)
	m := bson.M{"owner_id": me.ID, "date": s_date , "type": models.StatisticsODBATypeDay}

	rp := &models.RequestParameter{
		Sort:  []string{"-odba"},
		Limit: 10,
	}

	odbas, err := models.ListStatisticsODBAByFilter(db, m, rp, nil)
	if err != nil {
		logger.Errorln("List odbas failed:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List odbas failed:"))
		return
	}

	lastDate := date.AddDate(0, 0, -1)

	err = ODBAGetChangeIndex(db,&lastDate, models.StatisticsODBATypeDay, odbas)
	if err != nil {
		logger.Errorln("List odbas failed:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List last odbas failed:"))
		return
	}

	c.JSON(http.StatusOK, odbas)
}

func ODBAGetChangeIndex(db *mgo.Session, lastDate *time.Time, timeType int, odbas []*models.StatisticsODBA) error {

	odbaIds := []bson.ObjectId{}
	for _, d := range odbas {
		odbaIds = append(odbaIds, d.DeviceID)
	}


	s_date := models.ConvertTimeToDate(lastDate)
	m2 := bson.M{"device_id": bson.M{"$in": odbaIds}, "date": s_date, "type": timeType}

	odbas2, err := models.ListStatisticsODBAByFilter(db, m2, nil, nil)
	if err != nil {
		return err
	}


	cattles, err := models.ListCattleByFilter(db, bson.M{"device_id": bson.M{"$in": odbaIds}},nil)
	if err != nil {
		return err
	}	

	for i, _ := range odbas {
		for _, o := range odbas2 {
			if odbas[i].DeviceID == o.DeviceID {
				odbas[i].ChangeIndex =  o.Index - odbas[i].Index
				break
			}
		}

		for _, c := range cattles {
			if odbas[i].DeviceID == c.DeviceID {
				odbas[i].NickName = c.NickName 
				if len(c.Images)>0 {
					odbas[i].Avatar = c.Images[0]
				}
				break 
			}
		} 
	}


	return nil
}
