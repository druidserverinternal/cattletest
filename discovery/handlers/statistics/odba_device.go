
package statistics

import (
	"druid/cattle/discovery/middleware"
	"druid/cattle/models"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

func ODBAByDevice(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	deviceID := c.Param("id")
	if bson.IsObjectIdHex(deviceID) == false {
		c.AbortWithError(http.StatusBadRequest, models.Error("DeviceID  is invalid:"+deviceID))
		return 
	}

	year, mon, d := time.Now().AddDate(0, 0, -1).Date()

	end := time.Date(year, mon, d, 0, 0, 0, 0, time.UTC)
	begin := end.AddDate(0,0,-6)
	s_end := models.ConvertTimeToDate(&end)
	s_begin := models.ConvertTimeToDate(&begin)

	m := bson.M{"owner_id": me.ID, "device_id":bson.ObjectIdHex(deviceID), "date": bson.M{"$gte": s_begin, "$lt": s_end},"type": models.StatisticsODBATypeDay }

	rp := &models.RequestParameter{
		Sort: []string{"+date"},
		Limit: 10,
	}

	odbas, err := models.ListStatisticsODBAByFilter(db, m, rp, nil) 
	if err != nil {
		logger.Errorln("Group odbas failed:",err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Group odbas failed:"))
		return
	}

	c.JSON(http.StatusOK, odbas)

}
