
package statistics

import (
	"druid/cattle/discovery/middleware"
	"druid/cattle/models"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

func ODBAByMonth(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	year, mon, _ := time.Now().Date()
	if mon != 1 {
		mon = mon -1
	} else {
		mon = 12 
		year = year -1 
	}
	date := time.Date(year, mon, 1, 0, 0, 0, 0, time.UTC)
	s_date := models.ConvertTimeToDate(&date)

	m := bson.M{"owner_id": me.ID, "date": s_date, "type": models.StatisticsODBATypeMonth }

	rp := &models.RequestParameter{
		Sort: []string{"-odba"},
		Limit: 10,
	}

	odbas, err := models.ListStatisticsODBAByFilter(db, m, rp, nil) 
	if err != nil {
		logger.Errorln("Group odbas failed:",err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Group odbas failed:"))
		return
	}

	
	if mon != 1 {
		mon = mon -1
	} else {
		mon = 12 
		year = year -1 
	}
	lastDate := time.Date(year, mon, 1, 0, 0, 0, 0, time.UTC)
	
	err = ODBAGetChangeIndex(db,&lastDate,models.StatisticsODBATypeMonth,odbas)
	if err != nil {
		logger.Errorln("List odbas failed:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List last odbas failed:"))
		return
	}

	c.JSON(http.StatusOK, odbas)

}
