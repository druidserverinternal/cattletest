package company

import (
	"gopkg.in/mgo.v2/bson"
	"net/http"

	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

// Get himself company infomation
func Get(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	userID := middleware.GetUserID(c)
	user := models.FindUserByID(db, bson.ObjectIdHex(userID))

	company := models.FindCompanyByID(db, user.CompanyID)
	if company == nil {
		logger.Warning("The company id is invalid:", user.ID)
		c.AbortWithError(http.StatusBadRequest, models.Error("The company is invalid."))
		return
	}
	company.DeviceCount, _ = models.CountDeviceByCompanyID(db, company.ID)
	company.UserCount, _ = models.UserCountByCompanyID(db, company.ID)

	c.JSON(http.StatusOK, company)
}
