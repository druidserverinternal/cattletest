package weixin

import (
	"net/http"
	"time"
	"strings"
	"encoding/json"

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

func signatureValidate(c *gin.Context) (*models.WeixinSignature) {
	var sign models.WeixinSignature
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&sign); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	sign.URL = strings.TrimSpace(sign.URL)
	if sign.URL == "" {
		c.AbortWithError(http.StatusBadRequest, models.Error("Request signature url is nil."))
		return nil
	}

	return &sign
}

func GetSignature(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	sign := signatureValidate(c)
	if sign == nil {
		return
	}

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	weixin, err := models.FindWeixinByCompanyID(db, user.CompanyID)
	if err != nil {
		logger.Warning("Find weixin by companyID failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find weixin by companyID failed:", err))
		return
	}
	if weixin == nil {
		logger.Warning("Your company not support weixin.", err)
		c.AbortWithError(http.StatusNotFound, models.Error("Your company not support weixin."))
		return
	}

	switch weixin.IsExpiry() {
	case true:
		t := time.Now().Add(models.WaitTokenRefreshSecond * time.Second)
		if weixin, err = models.RefreshWeixinToken(db, weixin.CompanyID, &t); err != nil {
			logger.Warning("Refresh token failed.", err)
			c.AbortWithError(http.StatusInternalServerError, models.Error("Refresh token failed.", err))
			return
		}
	}

	sign.GenSign(weixin.Ticket)

	c.JSON(http.StatusOK, sign)
}

