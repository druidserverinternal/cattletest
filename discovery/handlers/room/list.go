package room

import (
	"net/http"
	
	"strconv"

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

// List himself company rooms
func List(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.Role != models.RoleUser {
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access."))
		return
	}

	rp := models.ParseRequestParameter(c)
	result, err := models.ListRoomByUserID(db, user.ID, rp)
	if err != nil {
		logger.Error("List room by company id failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List room by filter failed." + err.Error()))
		return
	}

	for i, room := range result {
		cnt, err := room.CountDevice(db)
		if err != nil {
			logger.Error("Get room device count failed.", err)
			c.AbortWithError(http.StatusInternalServerError, models.Error("Get room device count failed." + err.Error()))
			return
		}
		result[i].DeviceCount = cnt
	}

	count := 0
	if len(result) > 0 {
		count, _ = models.CountRoomByUserID(db, user.ID)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, result)
}

