package room

import (
	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	"encoding/json"
	"net/http"
//	"strings"
	
	"github.com/gin-gonic/gin"

	//"github.com/spf13/viper"
	"gopkg.in/mgo.v2/bson"

)

func createValidate(c *gin.Context) (*models.Room) {
	var room models.Room
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&room); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	if len(room.RoomName) == 0 || len(room.RoomName) > models.MaxRoomName {
		c.AbortWithError(http.StatusBadRequest, models.Error("roomname must be a valid name."))
		return nil
	}

	return &room
}

// Create a room
func Create(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	reqRoom := createValidate(c)
	if reqRoom == nil {
		return
	}

	reqRoom.ID = bson.NewObjectId()
	reqRoom.UserID = me.ID

	if err := reqRoom.Create(db); err != nil {
		c.AbortWithError(http.StatusInternalServerError, models.Error("Create room failed." + err.Error()))
		return
	}

	if err := me.FixTotalRoom(db); err != nil {
		logger.Error("Fix total room failed. ", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Fix total room failed.", err))
		return
	}

	c.JSON(http.StatusOK, reqRoom)
}
