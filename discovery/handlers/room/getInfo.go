package room

import (
	//	"encoding/json"
	"net/http"
	
	//	"regexp"
	//	"strings"

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

// Get a room by id
func GetInfo(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	roomID := c.Param("id")
	if !bson.IsObjectIdHex(roomID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("RoomId is invalid:" + roomID))
		return
	}

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.Role != models.RoleUser {
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access," + roomID))
		return
	}

	room, _ := models.FindRoomByID(db, bson.ObjectIdHex(roomID))
	if room == nil {
		logger.Warning("Find room by ID failed.")
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find room by ID failed:" + roomID))
		return
	}

	if room.UserID != user.ID {
		logger.Warning("You don't have permission to access")
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access"))
		return
	}

	cnt, err := room.CountDevice(db)
	if err != nil {
		logger.Error("Get room device count failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Get room device count failed." + err.Error()))
		return
	}
	room.DeviceCount = cnt

	c.JSON(http.StatusOK, room)
}

