package room

import (
	"net/http"
	
	"strconv"

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

// ListIdle himself company rooms
func ListIdle(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.Role != models.RoleUser {
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access."))
		return
	}

	rp := models.ParseRequestParameter(c)

	m := bson.M{
		"company_id": user.CompanyID,
		"owner_id": user.ID,
		"room_id": nil,
		"deleted_at": nil,
	}

	result, err := models.ListDeviceByFilter(db, m, rp, models.UserSelect)
	if err != nil {
		logger.Error("List device by filter failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List device by filter failed." + err.Error()))
		return
	}

	count := 0
	if len(result) > 0 {
		count, err = models.CountDeviceByFilter(db, m)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, result)
}
