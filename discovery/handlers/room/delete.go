package room

import (
	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	"fmt"
	"net/http"
	
	"github.com/gin-gonic/gin"

	"gopkg.in/mgo.v2/bson"
)

func deleteValidate(c *gin.Context) (string) {
	roomID := c.Param("id")
	if !bson.IsObjectIdHex(roomID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("Room id is invalid:" + roomID))
		return ""
	}

	return roomID
}

// Delete a room
func Delete(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	roomID := deleteValidate(c)
	if roomID == "" {
		return
	}

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if me.Role != models.RoleUser {
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access," + roomID))
		return
	}

	room, _ := models.FindRoomByID(db, bson.ObjectIdHex(roomID))
	if room == nil {
		c.AbortWithError(http.StatusNotFound, models.Error(fmt.Sprintf("Room:%s cannot be found", roomID)))
		return
	}

	if room.UserID != me.ID {
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access"))
		return
	}

	if err := models.FreeDeviceByRoomID(db, room.ID); err != nil {
		c.AbortWithError(http.StatusInternalServerError, models.Error("Free device room by room id failed," + err.Error()))
		return
	}

	if err := room.Delete(db); err != nil {
		c.AbortWithError(http.StatusInternalServerError, models.Error("Delete room id:," + room.ID.Hex() + " failed, " + err.Error()))
		return
	}

	if err := me.FixTotalRoom(db); err != nil {
		logger.Error("Fix total room failed. ", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Fix total room failed.", err))
		return
	}


	c.Status(http.StatusNoContent)
}
