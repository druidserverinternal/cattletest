package device

import (
	"encoding/json"
	"net/http"
	//	"regexp"

	//	"github.com/spf13/viper"

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

func bindValidate(c *gin.Context) (*models.Device) {
	device := models.Device{}
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&device); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	deviceID := c.Param("id")
	if !bson.IsObjectIdHex(deviceID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("device id is invalid:" + deviceID))
		return nil
	}
	device.ID = bson.ObjectIdHex(deviceID)

	return &device
}

// Bind a devices with cattle
func Bind(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	device := bindValidate(c)
	if device == nil {
		return
	}

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	d, err := models.CheckValidDeviceByID(db, device.ID, nil)
	if err != nil {
		logger.Error("Could not find device:", device.ID)
		c.AbortWithError(http.StatusNotFound, models.Error("Could not find device:"+device.ID.Hex()))
		return
	}

	if err := me.CheckDeviceOwner(d); err != nil {
		logger.Warning("Check owner device failed.", err)
		c.AbortWithError(http.StatusForbidden, models.Error("Check owner device failed.", err))
		return
	}

	if device.BiologicalID == "" || device.BiologicalType == "" {
		logger.Warning("amel id and type is invalid.")
		c.AbortWithError(http.StatusBadRequest, models.Error("Cattle id and type is invalid."))
		return
	}

	if d.BiologicalID != "" {
		cattle, err := models.FindCattleByID(db, d.BiologicalID)
		if err != nil {
			logger.Warning("Find exists cattle failed.", err)
			c.AbortWithError(http.StatusInternalServerError, models.Error("Find exists cattle failed.", err))
			return
		}
		if cattle != nil {
			logger.Warning("Can't bind cattle to exists device.")
			c.AbortWithError(http.StatusInternalServerError, models.Error("an't bind cattle to exists device."))
			return
		}
	}


	cattle, err := models.FindCattleByID(db, device.BiologicalID)
	if cattle == nil {
		logger.Warning("Find cattle failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find cattle failed.", device.BiologicalID, err))
		return
	}
	if cattle.OwnerID != d.OwnerID {
		logger.Warning("Can't bind other user's cattle.")
		c.AbortWithError(http.StatusBadRequest, models.Error("Can't bind other user's cattle."))
		return
	}
	if cattle.DeviceID != "" {
		logger.Warning("Can't bind exists cattle to other device.")
		c.AbortWithError(http.StatusBadRequest, models.Error("Can't bind exists cattle to other device."))
		return
	}

	d.BiologicalID = device.BiologicalID
	d.BiologicalType = device.BiologicalType
	if err = d.UpdateBiological(db); err != nil {
		c.AbortWithError(http.StatusInternalServerError, models.Error("Update device description failed:" + err.Error()))
		return
	}

	if err = cattle.BindDevice(db, d); err != nil {
		c.AbortWithError(http.StatusInternalServerError, models.Error("Bind device to biological failed:" + err.Error()))
		return
	}

	c.Status(http.StatusCreated)
}
