package device

import (
	"net/http"
	"strconv"

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"

	"druid/cattle/discovery/middleware"
	"druid/cattle/models"
)

func ListDituArea(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	id := c.Param("id")
	if !bson.IsObjectIdHex(id) {
		logger.Error("Device id is invalid.")
		c.AbortWithError(http.StatusBadRequest, models.Error("Device id is invalid :", id))
		return
	}

	device, err := models.CheckValidDeviceByID(db, bson.ObjectIdHex(id), nil)
	if err != nil {
		logger.Error("Device ID is invalid.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Device ID is invalid.", err))
		return
	}
	if device.OwnerID != user.ID {
		logger.Error("You don't have permission to access.")
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access."))
		return
	}

	result, err := models.ListDituAreaByDeviceID(db, device.ID, nil, models.UserSelect)
	if err != nil {
		logger.Error("List ditu area by device id failed. ", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List ditu area by device id failed.", err))
		return
	}
	for _, r := range result {
		r.ID = r.AreaID
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(len(result)))
	c.JSON(http.StatusOK, result)
}
