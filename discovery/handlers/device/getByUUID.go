package device

import (
	"net/http"
	"strings"
	

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

// GetByUUID find device by uuid
func GetByUUID(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	uuid := c.Param("id")
	uuid = strings.ToLower(strings.TrimSpace(uuid))
	if uuid == "" {
		c.AbortWithError(http.StatusBadRequest, models.Error("UUID is invalid:", uuid))
		return
	}

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	device, err := models.CheckValidDeviceByUUID(db, uuid, models.UserSelect)
	if err != nil {
		logger.Error("Find device failed:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find device failed:" + err.Error()))
		return
	}

	if device.CompanyID != "" && device.CompanyID != user.CompanyID {
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access."))
		return
	}

	if user.Role == models.RoleUser {
		if device.OwnerID == "" {
			c.JSON(http.StatusOK, device)
			return
		}
		if device.OwnerID != user.ID {
			c.AbortWithError(http.StatusBadRequest, models.Error("The device is other user's."))
			return
		}
	}

	c.JSON(http.StatusOK, device)
}
