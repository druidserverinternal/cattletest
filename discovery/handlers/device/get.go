package device

import (
	//	"encoding/json"
	"net/http"
	
	//	"regexp"
	//	"strings"

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

// Get a device by id
func Get(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	deviceID := c.Param("id")
	if !bson.IsObjectIdHex(deviceID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("DeviceId is invalid:" + deviceID))
		return
	}

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	device, err := models.FindDeviceMergeByID(db, bson.ObjectIdHex(deviceID), models.UserSelect)
	if err != nil {
		logger.Warning("Find device by ID failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find device by ID failed:", err))
		return
	}

	if device.OwnerID != user.ID {
		logger.Error("You don't have permission to access this device.")
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access this device."))
		return
	}

	areas, err := models.ListDituAreaByDeviceID(db, device.ID, nil, models.UserSelect)
	if err != nil {
		logger.Warning("List area by device ID failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List area by device ID failed:", err))
		return
	}
	for _, r := range areas {
		r.ID = r.AreaID
	}
	device.GeoFence = areas

	c.JSON(http.StatusOK, device)
}
