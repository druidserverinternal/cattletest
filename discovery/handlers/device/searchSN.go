package device

import (
	"net/http"
	"strconv"
	

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

// SearchSN devices
func SearchSN(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	sn := c.Param("sn")
	if !models.RegexpMark.MatchString(sn) {
		logger.Error("The sn is invalid.", sn)
		c.AbortWithError(http.StatusBadRequest, models.Error("The sn is invalid." + sn))
		return
	}


	m := bson.M{}
	m["owner_id"] = user.ID
	m["mark"] = bson.M{"$ne": nil}
	reg := "/.*" + sn + ".*/.test(this.mark)"
	m["$where"] = reg
	//  db.device.find({mark: {$ne: null},  $where: "/.*12.*/.test(obj.mark)" })

	logger.Info("Search fileter:", m)
	rp := &models.RequestParameter {
		Limit : 10,
		Sort: []string{"mark"},
	}

	device, err := models.SearchDevice(db, m, rp, models.UserSelect)
	if err != nil {
		logger.Error("Can't find device.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Can't find device." + err.Error()))
		return
	}

	ds := []bson.ObjectId{}
	for _, d := range device {
		ds = append(ds, d.ID)
	}
	deviceM, err := models.ListDeviceMergeByFilter(db, bson.M{"_id": bson.M{"$in": ds}}, nil, models.UserSelect)
	if err != nil {
		logger.Error("List device merge failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List device merge failed.", err))
		return
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(len(deviceM)))
	c.JSON(http.StatusOK, deviceM)
}

