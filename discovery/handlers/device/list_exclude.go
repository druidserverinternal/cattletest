package device

import (
	"net/http"
	"strconv"
	
	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"

	"druid/cattle/discovery/middleware"
	"druid/cattle/models"
)

func listExcludeValidate(c *gin.Context) (*models.DeviceMany) {
	devices := models.DeviceMany{}
	if err := c.BindJSON(&devices); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	return &devices
}

// ListExclude get many devices by id
func ListExclude(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	devices := listExcludeValidate(c)
	if devices == nil {
		return
	}

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	m := bson.M{
		"owner_id": user.ID,
		"_id": bson.M{"$nin": devices.ID},
	}

	rp := models.ParseRequestParameter(c)
//	rp.Limit = 10000

	result, err := models.ListDeviceMergeByFilter(db, m,  rp, models.UserSelect)
	if err != nil {
		logger.Error("List device by filter failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List device by filter failed.", err))
		return
	}

	count := 0
	if len(result) > 0 {
		count, err = models.CountDeviceByFilter(db, m)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, result)
}
