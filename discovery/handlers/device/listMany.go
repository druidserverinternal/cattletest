package device

import (
	"net/http"
	
	"encoding/json"
	//	"regexp"
	//	"strings"

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

func listManyValidate(c *gin.Context) (*models.DeviceMany) {
	devices := models.DeviceMany{}
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&devices); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	if len(devices.ID) == 0 {
		ds := []*models.Device{}
		c.JSON(http.StatusOK, ds)
		return nil
	}

	return &devices
}


// ListMany list many devices by id
func ListMany(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	devices := listManyValidate(c)
	if devices == nil {
		return
	}

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	m := bson.M{
		"_id": bson.M{"$in": devices.ID },
		"owner_id": me.ID,
	}

	ds, err := models.ListDeviceMergeByFilter(db, m, nil, models.UserSelect)
	if err != nil {
		logger.Error("Find device by ID failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find device by ID failed:", err))
		return
	}
	if len(ds) != len(devices.ID) {
		logger.Error("Some device are not yours.", devices)
		c.AbortWithError(http.StatusForbidden, models.Error("Some devices are not yours"))
		return
	}

	c.JSON(http.StatusOK, ds)
}
