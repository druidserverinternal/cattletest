package device

import (
	"net/http"
	
	//	"regexp"

	//	"github.com/spf13/viper"

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)


func UnBind(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	deviceID := c.Param("id")
	if !bson.IsObjectIdHex(deviceID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("device id is invalid:" + deviceID))
		return
	}

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	d, err := models.CheckValidDeviceByID(db, bson.ObjectIdHex(deviceID), nil)
	if d == nil {
		logger.Error("Could not find device:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Could not find device:", err))
		return
	}

	if err := me.CheckDeviceOwner(d); err != nil {
		logger.Warning("Check owner device failed.", err)
		c.AbortWithError(http.StatusForbidden, models.Error("Check owner device failed.", err))
		return
	}

	if d.BiologicalID == "" {
		c.Status(http.StatusNoContent)
		return
	}

	cattle, err := models.FindCattleByID(db, d.BiologicalID)
	if cattle == nil {
		logger.Warning("Find cattle failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find cattle failed.", err))
		return
	}

	if err = cattle.RemoveDevice(db); err != nil {
		c.AbortWithError(http.StatusInternalServerError, models.Error("Remove device from biological failed:" + err.Error()))
		return
	}

	if err = d.RemoveBiological(db); err != nil {
		c.AbortWithError(http.StatusInternalServerError, models.Error("Remove cattle from device failed:" + err.Error()))
		return
	}
	c.Status(http.StatusNoContent)
}
