package device

import (
	"net/http"
	"strconv"
	

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)


func searchValidate(c *gin.Context) (*models.Search) {
	search := models.Search{}
	if err := c.BindJSON(&search); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	return &search
}

// Search devices
func Search(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	search := searchValidate(c)
	if search == nil {
		return
	}

	m := bson.M{}
	m["owner_id"] = bson.ObjectIdHex(middleware.GetUserID(c))

	if search.RoomID != "" {
		m["room_id"] = search.RoomID
	}

	fm := bson.M(nil)
	if search.Species != -1 {
		fm = bson.M{"species": search.Species}
	}

	if search.Survive != -1 {
		m["survive"] = search.Survive
	}

	logger.Info("Search fileter:", m, fm)

	device, err := models.ListDeviceMergeFinalMatchByFilter(db, m, fm, models.ParseRequestParameter(c), models.UserSelect)
	if err != nil {
		logger.Error("List device by filter failed.", err, m)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List device by filter failed.", err))
		return
	}

	count := 0
	if len(device) > 0 {
		count, err = models.CountDeviceMergeFinalMatchByFilter(db, m, fm)
		if err != nil {
			logger.Error("count device by filter failed.", err, m)
			c.AbortWithError(http.StatusInternalServerError, models.Error("List device by filter failed.", err))
			return
		}
	}

	logger.Info("Search result:", device, count)

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, device)
}


