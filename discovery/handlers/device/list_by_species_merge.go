package device

import (
	"net/http"
	"strconv"
	
	"gopkg.in/mgo.v2/bson"

	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

func ListBySpecies(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	sp, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Species is invalid:", err))
		return
	}
	if sp >= models.CattleSpecieUnknown {
		c.AbortWithError(http.StatusBadRequest, models.Error("Species is invalid:", sp))
		return
	}

	rp := models.ParseRequestParameter(c)
	device, err := models.ListDeviceMergeByFilter(db, bson.M{"owner_id": bson.ObjectIdHex(middleware.GetUserID(c)), "species": sp}, rp, models.UserSelect)
	if err != nil {
		logger.Error("List device by species failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List device by species failed:", err))
		return
	}

	c.JSON(http.StatusOK, device)
}


