package device

import (
	"encoding/json"
	"net/http"
	"strings"
	

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

func registerValidate(c *gin.Context) (*models.Device) {
	device := models.Device{}
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&device); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	device.UUID = strings.ToLower(strings.TrimSpace(device.UUID))
	device.Mac = strings.ToLower(device.Mac)

	return &device
}

// Register a devices
func Register(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	device := registerValidate(c)
	if device == nil {
		return
	}
	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.Role != models.RoleUser {
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access."))
		return
	}

	d, err := models.CheckValidDeviceByUUID(db, device.UUID, nil)
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find device failed:" + err.Error()))
		return
	}

	if d.CompanyID != "" && d.CompanyID != user.CompanyID {
		logger.Error("You did not have permissions to do.", d.CompanyID)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to do.", d.CompanyID))
		return
	}
	if d.OwnerID == user.ID {
		c.Status(http.StatusCreated)
		return
	}
	if d.OwnerID != "" {
		logger.Error("Register another user device failed.")
		c.AbortWithError(http.StatusForbidden, models.Error("Register another user device failed."))
		return
	}

	if err := d.AddOwner(db, user); err != nil {
		logger.Error("Register add owner failed:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Register add owner failed.", err))
		return
	}

	if err := user.FixTotalDevice(db); err != nil {
		logger.Error("Fix total device failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Fix total device failed.", err))
		return
	}


	/*
	if err := user.AddOwnerDevice(db, d.ID); err != nil {
		logger.Error("Register owner device failed:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Register owner device failed.", err))
		return
	}
	*/

	c.Status(http.StatusCreated)
}

