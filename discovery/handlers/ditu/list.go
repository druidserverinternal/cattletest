package ditu

import (
	"net/http"
	"strconv"

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"

	"druid/cattle/discovery/middleware"
	"druid/cattle/models"
)
func List(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	rp := models.ParseRequestParameter(c)

	result := []*models.DituArea{}
	var err error
	result, err = models.ListDituAreaByOwnerID(db, user.ID, rp)
	if err != nil {
		logger.Error("List ditu area by user id failed. ", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List ditu area by user id failed.", err))
		return
	}
	for _, area := range result {
		dh, err := models.ListDituHistoryByAreaID(db, area.ID)
		if err != nil {
			logger.Error("List ditu history by id.", err)
			c.AbortWithError(http.StatusInternalServerError, models.Error("List ditu history by id.", err))
			return
		}
		for _, d := range dh {
			area.DevicesID = append(area.DevicesID, d.DeviceID)
		}
	}


	count := 0
	if len(result) > 0 {
		count, err = models.CountDituAreaByOwnerID(db, user.ID)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, result)
}
