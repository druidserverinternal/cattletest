package ditu

import (
	"net/http"
	"strconv"

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"

	"druid/cattle/discovery/middleware"
	"druid/cattle/models"
)

func ListUser(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	userID := c.Param("id")
	if !bson.IsObjectIdHex(userID) {
		logger.Error("List ditu by user invalid.", userID)
		c.AbortWithError(http.StatusBadRequest, models.Error("List ditu by user invalid.", userID))
		return
	}

	rp := models.ParseRequestParameter(c)
	result, err := models.ListDituAreaByOwnerID(db, bson.ObjectIdHex(userID), rp)
	if err != nil {
		logger.Error("List ditu area by user failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List ditu area by user failed.", err))
		return
	}

	count := 0
	if len(result) > 0 {
		count, err = models.CountDituAreaByOwnerID(db, bson.ObjectIdHex(userID))
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, result)
}
