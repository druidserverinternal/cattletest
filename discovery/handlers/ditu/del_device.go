package ditu

import (
	"fmt"
	"net/http"

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"

	"druid/cattle/discovery/middleware"
	"druid/cattle/models"
)

func DelDevice(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	id := c.Param("id")
	if !bson.IsObjectIdHex(id) {
		c.AbortWithError(http.StatusBadRequest, models.Error(fmt.Sprintf("Ditu id:%s is invalid id", id)))
		return 
	}
	deviceID := c.Param("device")
	if !bson.IsObjectIdHex(id) {
		c.AbortWithError(http.StatusBadRequest, models.Error(fmt.Sprintf("Device id:%s is invalid id", id)))
		return 
	}

	device, err := models.FindDeviceByID(db, bson.ObjectIdHex(deviceID), nil)
	if err != nil {
		logger.Error("Find device by request failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Check device by request failed.", err))
		return
	}

	orignArea, err := models.FindDituAreaByID(db, bson.ObjectIdHex(id))
	if err != nil {
		logger.Error("Can't find ditu area by id.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Can't find ditu area by id.", err))
		return
	}

	if device.OwnerID != bson.ObjectIdHex(middleware.GetUserID(c)) || orignArea.OwnerID != bson.ObjectIdHex(middleware.GetUserID(c)) {
		logger.Error("You don't have permission to access other.")
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access other."))
		return
	}

	if err := models.DeleteDituHistoryByFilter(db, bson.M{"device_id": device.ID, "area_id": orignArea.ID}); err != nil {
		logger.Error("Delete device from area failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Remove device from area failed.", err))
		return
	}


	if err := device.UpdateTotalArea(db); err != nil {
		logger.Error("Update total area failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Update total area failed.", err))
		return
	}

	if err := orignArea.UpdateTotalDevice(db); err != nil {
		logger.Error("Update total device failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Update total device failed.", err))
		return
	}



	c.Status(http.StatusCreated)
}
