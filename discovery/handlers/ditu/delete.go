package ditu

import (
	"fmt"
	"net/http"

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"

	"druid/cattle/discovery/middleware"
	"druid/cattle/models"
)

func Delete(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	id := c.Param("id")
	if !bson.IsObjectIdHex(id) {
		logger.Error("Area id is invalid.")
		c.AbortWithError(http.StatusBadRequest, models.Error(fmt.Sprintf("Area id:%s is invalid id", id)))
		return
	}

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	area, err := models.FindDituAreaByID(db, bson.ObjectIdHex(id))
	if err != nil {
		logger.Error("Can't find ditu area by id.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Can't find ditu area by id.", err))
		return
	}

	if area.OwnerID != me.ID {
		logger.Error("You don't have permission to access, you are not admin.")
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access."))
		return
	}

	dh, err := models.ListDituHistoryByAreaID(db, area.ID)
	if err != nil {
		logger.Error("List ditu area failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List ditu area failed.", err))
		return
	}

	for _, d := range dh {
		if err := models.UpdateTotalAreaByDeviceID(db, d.DeviceID, -1); err != nil {
			logger.Error("Update total area failed.", err)
			c.AbortWithError(http.StatusInternalServerError, models.Error("Update total area failed.", err))
			return
		}
	}

	if err := models.DeleteAreaHistoryByAreaID(db, area.ID); err != nil {
		logger.Error("Delete area history failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Delete area history failed:", err))
		return
	}

	if err := area.Delete(db); err != nil {
		logger.Error("Delete area failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Delete area failed:", err))
		return
	}

	if err := me.FixTotalArea(db); err != nil {
		logger.Error("Fix total map area failed. ", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Fix total map area failed.", err))
		return
	}

	c.Status(http.StatusNoContent)
}

