package file

import (
	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

//	"strconv"
	"net/http"

	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

func DownloadThumbnail(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	id := c.Param("id")
	if !bson.IsObjectIdHex(id) {
		logger.Error("Camel id is invalid.")
		c.AbortWithError(http.StatusBadRequest, models.Error("Camel id is invalid:", id))
		return
	}

	image, err := models.FindImageByID(db, bson.ObjectIdHex(id))
	if err != nil {
		logger.Error("Find image from request id failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find image from request id failed, ", err))
		return
	}
	if image == nil {
		if err := models.RemoveImageByImageID(db, bson.ObjectIdHex(id)); err != nil {
			logger.Error("Remove image from biological failed.", err)
			c.AbortWithError(http.StatusInternalServerError, models.Error("Remove image from biological failed.", err))
			return
		}
		c.Status(http.StatusNotFound)
		return
	}

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if image.OwnerID != user.ID {
		logger.Warning("You don't have permission to access it.")
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access it."))
		return
	}

	file, err := models.ReadFile(db, image.Point)
	if err != nil {
		logger.Error("Read file from request failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Read file from request failed, ", err))
		return
	}

	logger.Info("type:", http.DetectContentType(file.Data))


	file.Data, err = models.ConvertImageToThumbnail(file.Data)
	if err != nil {
		logger.Error("Compress image failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Compress image failed, ", err))
		return
	}
	file.Name = "thumbnail.jpg"

	logger.Info("Start download file. length:", file.Size, len(file.Data))
	logger.Info("type:", http.DetectContentType(file.Data))

//	ct := models.GetContentTypeByName(file.Name)
//	c.Header("Content-Type", ct)
//	c.Header("Content-Length", strconv.Itoa(file.Size))
	c.Header("Content-Disposition", "attachment; filename=" + file.Name)


//	c.Abort()
	c.Data(http.StatusOK, http.DetectContentType(file.Data), file.Data)
}
