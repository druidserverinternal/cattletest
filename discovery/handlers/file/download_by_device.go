package file

import (
	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	//"strconv"
	"net/http"

	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

func DownloadByDevice(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	deviceID := c.Param("id")
	if !bson.IsObjectIdHex(deviceID) {
		logger.Error("Device id is invalid.")
		c.AbortWithError(http.StatusBadRequest, models.Error("Device id is invalid:", deviceID))
		return
	}

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	device, err := models.FindDeviceMergeByID(db, bson.ObjectIdHex(deviceID), models.UserSelect)
	if err != nil {
		logger.Warning("Find device by ID failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find device by ID failed:", err))
		return
	}

	if device.OwnerID != me.ID {
		logger.Error("You don't have permission to access this device.")
		c.AbortWithError(http.StatusForbidden, models.Error("You don't have permission to access this device."))
		return
	}

	imageID := c.Param("image_id")
	if !bson.IsObjectIdHex(imageID) {
		logger.Error("Image id is invalid.")
		c.AbortWithError(http.StatusBadRequest, models.Error("Image id is invalid:", imageID))
		return
	}

	for _, ii := range device.Images {
		if ii != bson.ObjectIdHex(imageID) {
			continue
		}

		image, err := models.FindImageByID(db, bson.ObjectIdHex(imageID))
		if err != nil {
			logger.Error("Find image from request id failed.", err)
			c.AbortWithError(http.StatusInternalServerError, models.Error("Find image from request id failed, ", err))
			return
		}
		if image == nil {
			if err := models.RemoveImageByImageID(db, bson.ObjectIdHex(imageID)); err != nil {
				logger.Error("Remove image from biological failed.", err)
				c.AbortWithError(http.StatusInternalServerError, models.Error("Remove image from biological failed.", err))
				return
			}
			c.AbortWithStatus(http.StatusNotFound)
			return
		}

		file, err := models.ReadFile(db, image.Point)
		if err != nil {
			logger.Error("Read file from request failed.", err)
			c.AbortWithError(http.StatusInternalServerError, models.Error("Read file from request failed, ", err))
			return
		}

		logger.Info("Start download file. length:", file.Size, len(file.Data))

		c.Header("Content-Disposition", "attachment; filename=" + image.Name)

		c.Data(http.StatusOK, http.DetectContentType(file.Data), file.Data)
		return
	}

	logger.Error("Image id is invalid.", imageID)
	c.AbortWithError(http.StatusForbidden, models.Error("Image id is invalid,", imageID))
	return
}
