package handlers

import (
	"druid/cattle/discovery/middleware"

	"druid/cattle/discovery/handlers/behavior"
	"druid/cattle/discovery/handlers/biological/cattle"
	"druid/cattle/discovery/handlers/company"
	"druid/cattle/discovery/handlers/device"
	"druid/cattle/discovery/handlers/ditu"
	"druid/cattle/discovery/handlers/file"
	"druid/cattle/discovery/handlers/gps"
	"druid/cattle/discovery/handlers/message"
	"druid/cattle/discovery/handlers/odba"
	"druid/cattle/discovery/handlers/room"
	"druid/cattle/discovery/handlers/search"
	"druid/cattle/discovery/handlers/user"
	"druid/cattle/discovery/handlers/weixin"
	"druid/cattle/discovery/handlers/statistics"

	"github.com/0987363/cache"
	"github.com/0987363/cache/persistence"
	"github.com/gin-gonic/gin"
	"github.com/itsjamie/gin-cors"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"

	"strings"
	"time"
)

// RootMux is the root handler for the cattle server
var RootMux = gin.New()

func Init() {
	RootMux.Use(gin.Logger())
	//	RootMux.Use(gzip.Gzip(gzip.BestSpeed))

	RootMux.Use(middleware.RequestID())
	RootMux.Use(middleware.Logger())
	RootMux.Use(gin.Recovery())
	RootMux.Use(middleware.DBConnector())
	RootMux.Use(middleware.PushConnection())
	//	RootMux.Use(middleware.Etcd())

	RootMux.Use(cors.Middleware(cors.Config{
		Origins:         "*",
		Methods:         "GET, PUT, POST, DELETE, OPTIONS",
		RequestHeaders:  "Origin, X-Druid-Authentication, Content-Type, X-Result-Limit, X-Result-Offset, X-Result-Sort, Accept-Language, Accept, X-Result-Compress",
		ExposedHeaders:  "X-Druid-Authentication, X-Result-Length, X-Result-Count, Content-Length, Content-Type, Content-Disposition",
		MaxAge:          time.Hour * 24 * 7,
		Credentials:     true,
		ValidateHeaders: false,
	}))

	cache.SetPageKey("discovery:1:" + viper.GetString("release"))

	var store persistence.CacheStore
	memAddrs := viper.GetString("memcached")
	addrs := strings.Split(memAddrs, ",")
	if memAddrs == "" || len(addrs) == 0 {
		store = persistence.NewInMemoryStore(time.Hour)
		log.Info("Init memory store success.")
	} else {
		memStore := persistence.NewMemcachedStore(addrs, time.Hour)
		memStore.Client.Timeout = time.Second * 5
		store = memStore
		log.Info("Init memcached store success.", addrs, len(addrs))
	}

	v1Mux := RootMux.Group("/v1")
	{
		v1Mux.POST("/login", user.Login)

		v1Mux.Use(middleware.Authenticator())

		userMux := v1Mux.Group("/user")
		{
			userMux.PUT("/password", user.UpdatePassword)
			userMux.PUT("/info", user.UpdateInfo)
			userMux.PUT("/profile", user.UpdateProfile)

			userMux.GET("/myself", user.Me)
		}

		companyMux := v1Mux.Group("/company")
		{
			companyMux.GET("/", company.Get)
		}

		deviceMux := v1Mux.Group("/device")
		{
			deviceMux.GET("/", device.List)
			deviceMux.GET("/uuid/:id", device.GetByUUID)
			deviceMux.GET("/species/:id", device.ListBySpecies)
			deviceMux.GET("/id/:id", device.Get)
			deviceMux.GET("/id/:id/area", device.ListDituArea)
			deviceMux.GET("/search/mark/:sn", device.SearchSN)

			deviceMux.GET("/idle/room", device.ListIdleRoom)

			deviceMux.POST("/search", device.Search)
			deviceMux.POST("/register", device.Register)
			deviceMux.POST("/many", device.ListMany)
			deviceMux.POST("/exclude", device.ListExclude)

			deviceMux.PUT("/id/:id", device.Update) // update device description
			//			deviceMux.PUT("/id/:id/addcattle", device.Bind)
			//			deviceMux.PUT("/id/:id/delcattle", device.UnBind)
		}
		gpsMux := v1Mux.Group("/gps")
		{
			//		gpsMux.GET("/device/:id/last", gps.GetLast)
			gpsMux.GET("/device/:id", gps.Get)
			//			gpsMux.GET("/device/:id/map", gps.Map)
		}

		behaviorMux := v1Mux.Group("/behavior")
		{
			behaviorMux.GET("/device/:id", behavior.Get)

			//behaviorMux.GET("/init", behavior.InitBehAlgo)
			behaviorMux.POST("/analyze/", behavior.AnanlyzeList)
			behaviorMux.GET("/analyze/device/:id/timetype/:type", behavior.Ananlyze)
		}

		odbaMux := v1Mux.Group("/odba")
		{
			odbaMux.POST("/analyze/", odba.AnanlyzeList)
			odbaMux.POST("/loving/", odba.LovingList)
			odbaMux.GET("/analyze/device/:id/timetype/:type", odba.Ananlyze)
			odbaMux.GET("/loving/device/:id/timetype/:type", odba.Loving)
			odbaMux.GET("/device/:id/time/:num",behavior.GetOBDA)
		}

		statisticsMux := v1Mux.Group("/statistics")
		{
			statisticsMux.GET("/odba/day", statistics.ODBAByDay)
			statisticsMux.GET("/odba/week", statistics.ODBAByWeek)
			statisticsMux.GET("/odba/month", statistics.ODBAByMonth)
			statisticsMux.GET("/odba/device/id/:id",statistics.ODBAByDevice)
			statisticsMux.GET("/odba/up",statistics.ODBAByUp)
			statisticsMux.GET("/odba/down",statistics.ODBAByDown)
		}

		searchMux := v1Mux.Group("/search")
		{
			searchMux.POST("/device", search.Device)
		}

		dituMux := v1Mux.Group("/ditu")
		{
			dituMux.POST("/", ditu.Create)

			dituMux.PUT("/area/:id/adddevice/:device", ditu.AddDevice)
			dituMux.PUT("/area/:id/deldevice/:device", ditu.DelDevice)
			//			dituMux.PUT("/area/:id/adddevice", ditu.AddDevices)
			//			dituMux.PUT("/area/:id/deldevice", ditu.DelDevices)
			dituMux.PUT("/area/:id", ditu.Update)

			dituMux.DELETE("/area/:id", ditu.Delete)

			dituMux.GET("/area/:id/device", ditu.GetDevice)
			dituMux.GET("/area", ditu.List)
			dituMux.GET("/area/:id", ditu.Get)
		}

		roomMux := v1Mux.Group("/room")
		{
			roomMux.POST("/", room.Create)
			roomMux.PUT("/id/:id/adddevice", room.AddDevice)
			roomMux.PUT("/id/:id/deldevice", room.RemoveDevice)

			roomMux.PUT("/id/:id", room.Update)

			roomMux.DELETE("/id/:id", room.Delete)

			roomMux.GET("/", room.List)
			roomMux.GET("/id/:id/info", room.GetInfo)
			roomMux.GET("/id/:id/device", room.GetDevice)
		}

		biologicalMux := v1Mux.Group("/biological")
		{
			cattleMux := biologicalMux.Group("/cattle")
			{
				//				cattleMux.POST("/", cattle.Create)
				//				cattleMux.POST("/many", cattle.ListMany)

				//				cattleMux.DELETE("/id/:id", cattle.Delete)
				cattleMux.DELETE("/id/:id/image/:image_id", cattle.DeleteImage)
				cattleMux.DELETE("/device/:id/image/:image_id", cattle.DeleteImageByDevice)

				//				cattleMux.GET("/id/:id", cattle.Get)
				cattleMux.GET("/device/:id", cattle.GetByDevice)
				//				cattleMux.GET("/", cattle.List)

				//				cattleMux.GET("/unused", cattle.ListIdle)
				//				cattleMux.GET("/species", cattle.ListSpecies)
				//				cattleMux.GET("/species/:species", cattle.GetSpecies)

				//				cattleMux.PUT("/mark/:id", cattle.UpdateByMark)
				cattleMux.PUT("/device/:id", cattle.Update)
				//				cattleMux.PUT("/:id/imageb/:name", cattle.UploadImageB)	// for base64
				cattleMux.PUT("/device/:id/image/:name", cattle.UploadImage) //id is cattle id
				cattleMux.PUT("/device/:id/image", cattle.WeixinImage)
			}
		}

		fileMux := v1Mux.Group("/file")
		{
			fileMux.GET("/id/:id", file.Download)
			//			fileMux.GET("/:id/image", file.DownloadImage)

			fileMux.GET("/id/:id/thumbnail", file.DownloadThumbnail)

			fileMux.GET("/device/:id/image/:image_id", cache.CachePage(store, time.Hour*48, file.DownloadByDevice))
			fileMux.GET("/device/:id/image/:image_id/thumbnail", cache.CachePage(store, time.Hour*48, file.DownloadByDeviceThumbnail))
		}

		weixinMux := v1Mux.Group("/weixin")
		{
			weixinMux.GET("/token", weixin.GetToken)

			weixinMux.POST("/signature", weixin.GetSignature)
		}

		messageMux := v1Mux.Group("/message")
		{
			messageMux.GET("/unread", message.Unread)
			messageMux.GET("/", message.List)
			messageMux.GET("/type/:type", message.ListType)

			messageMux.PUT("/", message.Put)
			//    messageMux.POST("/", message.Post)

			messageMux.DELETE("/id/:id", message.Delete)
			messageMux.PUT("/delete", message.DeleteMany)
		}
	}
}
