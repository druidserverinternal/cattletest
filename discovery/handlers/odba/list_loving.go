package odba

import (
	"druid/cattle/discovery/middleware"
	"druid/cattle/models"
	"encoding/json"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

func lovingListValidate(c *gin.Context) *models.BehaviorAnalyzeReq {
	var behaviorAnalyzeReq models.BehaviorAnalyzeReq
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&behaviorAnalyzeReq); err != nil {
		//		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		//		return nil
	}
	return &behaviorAnalyzeReq
}

func LovingList(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	odbaAnalyzeReq := lovingListValidate(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))

	rp := models.ParseRequestParameter(c)
	if rp.LimitOrign == 0 {
		rp.Limit = 0
	}

	count := 0
	devices := []*models.DeviceMerge{}
	var err error
	if len(odbaAnalyzeReq.DeviceIDs) < 1 {
		m := bson.M{"owner_id": user.ID}
		if odbaAnalyzeReq.RoomID != "" {
			m["room_id"] = odbaAnalyzeReq.RoomID
		}

		if odbaAnalyzeReq.Survive != 0 { //models.SurviveLoving
			m["survive"] = odbaAnalyzeReq.Survive
		}

		if odbaAnalyzeReq.Species != -1 {
			if odbaAnalyzeReq.Species == 0 {
				m["species"] = bson.M{"$ne": 1}
			} else {
				m["species"] = odbaAnalyzeReq.Species
			}
		}

		devices, err = models.ListDeviceMergeByFilter(db, m, rp, nil)
		if err != nil {
			c.AbortWithError(http.StatusInternalServerError, models.Error("Can't find devices "))
			return
		}
		count, err = models.CountDeviceByFilter(db, m)
		if err != nil {
			logger.Error("Count Device Merage By Filter err :", err)
		}

	} else {
		m := bson.M{"owner_id": user.ID, "_id": bson.M{"$in": odbaAnalyzeReq.DeviceIDs}}

		devices, err = models.ListDeviceMergeByFilter(db, m, rp, nil)
		if err != nil {
			c.AbortWithError(http.StatusInternalServerError, models.Error("Can't find devices "))
			return
		}
		if len(devices) != len(odbaAnalyzeReq.DeviceIDs) {
			c.AbortWithError(http.StatusInternalServerError, models.Error("Can't find devices "))
			return
		}

	}

	odbaAnalyzeresp := [][]*models.ODBALoving{}

	timeS := time.Now().Unix()/3600*3600 - 3600*24 //默认查看一天的数据
	timeE := time.Now().Unix() / 3600 * 3600

	if odbaAnalyzeReq.TimeStart != "" {
		timeStart, err := models.ConvertStringToTime(odbaAnalyzeReq.TimeStart)
		if err == nil {
			timeS = timeStart.Unix() / 3600 * 3600
		}
	}

	if odbaAnalyzeReq.TimeEnd != "" {
		timeEnd, err := models.ConvertStringToTime(odbaAnalyzeReq.TimeEnd)
		if err == nil {
			timeE = timeEnd.Unix() / 3600 * 3600
		}
	}

	//获取所有 优化减少读取数据库的次数，但是增加了内存使用
	allDevicesIds := []bson.ObjectId{}
	for _, thisDevice := range devices {
		allDevicesIds = append(allDevicesIds, thisDevice.ID)
	}
	allODBALovings, _, err := models.ListODBALovingRangeManey(db, allDevicesIds, uint32(timeS), uint32(timeE), nil)
	if err != nil {
		logger.Error("List ODBALoing History failed")
		c.AbortWithError(http.StatusInternalServerError, models.Error("List BehaviorAnalyze History failed"))
		return
	}

	if len(allODBALovings) == 0 {
		odbaCells := []*models.ODBALoving{}
		odbaAnalyzeresp = append(odbaAnalyzeresp, odbaCells)
	}
	//按deviceID 拆分
	start_i := 0
	for ii := 0; ii < len(allODBALovings); {

		flog := 0
		for kk := start_i; kk < len(allODBALovings); kk++ {
			if allODBALovings[kk].DeviceID != allODBALovings[start_i].DeviceID {
				ii = kk
				flog = kk
				break
			}
			if kk == len(allODBALovings)-1 {
				ii = kk + 1
				flog = kk + 1
				break
			}
		}
		ODBALovings := allODBALovings[start_i:flog]
		start_i = flog

		if len(ODBALovings) < 1 {
			continue
		}
		/*
			device := models.DeviceMerge{}
			for _, thisDevice := range devices {
				if thisDevice.ID == ODBALovings[0].DeviceID {
					device = *thisDevice
				}
			}
		*/

		odbaAnalyzeresp = append(odbaAnalyzeresp, ODBALovings)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, odbaAnalyzeresp)

}
