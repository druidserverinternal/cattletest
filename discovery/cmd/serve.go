package cmd

import (
	"druid/public"
	"druid/cattle/discovery/handlers"
	"druid/cattle/discovery/middleware"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

const defaultAddress = ":8080"

// serveCmd represents the serve command
var serveCmd = &cobra.Command{
	Use:    "serve",
	Short:  "Start cattle server",
	PreRun: LoadConfiguration,
	Run:    serve,
}

func init() {
	RootCmd.AddCommand(serveCmd)

	serveCmd.Flags().StringP(
		"address",
		"a",
		defaultAddress,
		"Address the server binds to",
	)
	viper.BindPFlag("address", serveCmd.Flags().Lookup("address"))
}


func serve(cmd *cobra.Command, args []string) {
	middleware.LoggerConnInit()

	// Try to connect to the database
	if err := middleware.ConnectDB(
		viper.GetString("database"),
	); err != nil {
		log.Fatalln("connect to db: ", err)
	}

	if err := public.ConnectEtcd(
		viper.GetString("etcd.address"),
		viper.GetString("etcd.ca"),
		viper.GetString("etcd.cert"),
		viper.GetString("etcd.key"),
	); err != nil {
		log.Fatalln("connect to etcd: ", err)
	}


	address := viper.GetString("address")
	cert := viper.GetString("tls.cert")
	key := viper.GetString("tls.key")

	handlers.Init()

	if cert != "" && key != "" {
		log.Infof("Starting cattle discovery tls server on %s.", address)
		handlers.RootMux.RunTLS(address, cert, key)
	} else {
		log.Infof("Starting cattle discovery server on %s.", address)
		handlers.RootMux.Run(address)
	}
}
