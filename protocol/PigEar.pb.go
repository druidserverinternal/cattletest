// Code generated by protoc-gen-go. DO NOT EDIT.
// source: PigEar.proto

package protocol

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

type PigEarReq struct {
	Iden             *IdentityMsg `protobuf:"bytes,1,req,name=Iden" json:"Iden,omitempty"`
	PigEarInfo       []*PigEar    `protobuf:"bytes,2,rep,name=PigEarInfo" json:"PigEarInfo,omitempty"`
	XXX_unrecognized []byte       `json:"-"`
}

func (m *PigEarReq) Reset()                    { *m = PigEarReq{} }
func (m *PigEarReq) String() string            { return proto.CompactTextString(m) }
func (*PigEarReq) ProtoMessage()               {}
func (*PigEarReq) Descriptor() ([]byte, []int) { return fileDescriptor14, []int{0} }

func (m *PigEarReq) GetIden() *IdentityMsg {
	if m != nil {
		return m.Iden
	}
	return nil
}

func (m *PigEarReq) GetPigEarInfo() []*PigEar {
	if m != nil {
		return m.PigEarInfo
	}
	return nil
}

type PigEar struct {
	Timestamp        *uint32 `protobuf:"varint,1,opt,name=Timestamp,def=0" json:"Timestamp,omitempty"`
	Voltage          *int32  `protobuf:"varint,2,opt,name=Voltage,def=0" json:"Voltage,omitempty"`
	Temperature      *int32  `protobuf:"varint,10,opt,name=Temperature,def=0" json:"Temperature,omitempty"`
	ActivityTime     *int32  `protobuf:"varint,11,opt,name=ActivityTime,def=0" json:"ActivityTime,omitempty"`
	ActivityExpend   *int32  `protobuf:"varint,12,opt,name=ActivityExpend,def=0" json:"ActivityExpend,omitempty"`
	TotalExpend      *int32  `protobuf:"varint,13,opt,name=TotalExpend,def=0" json:"TotalExpend,omitempty"`
	XXX_unrecognized []byte  `json:"-"`
}

func (m *PigEar) Reset()                    { *m = PigEar{} }
func (m *PigEar) String() string            { return proto.CompactTextString(m) }
func (*PigEar) ProtoMessage()               {}
func (*PigEar) Descriptor() ([]byte, []int) { return fileDescriptor14, []int{1} }

const Default_PigEar_Timestamp uint32 = 0
const Default_PigEar_Voltage int32 = 0
const Default_PigEar_Temperature int32 = 0
const Default_PigEar_ActivityTime int32 = 0
const Default_PigEar_ActivityExpend int32 = 0
const Default_PigEar_TotalExpend int32 = 0

func (m *PigEar) GetTimestamp() uint32 {
	if m != nil && m.Timestamp != nil {
		return *m.Timestamp
	}
	return Default_PigEar_Timestamp
}

func (m *PigEar) GetVoltage() int32 {
	if m != nil && m.Voltage != nil {
		return *m.Voltage
	}
	return Default_PigEar_Voltage
}

func (m *PigEar) GetTemperature() int32 {
	if m != nil && m.Temperature != nil {
		return *m.Temperature
	}
	return Default_PigEar_Temperature
}

func (m *PigEar) GetActivityTime() int32 {
	if m != nil && m.ActivityTime != nil {
		return *m.ActivityTime
	}
	return Default_PigEar_ActivityTime
}

func (m *PigEar) GetActivityExpend() int32 {
	if m != nil && m.ActivityExpend != nil {
		return *m.ActivityExpend
	}
	return Default_PigEar_ActivityExpend
}

func (m *PigEar) GetTotalExpend() int32 {
	if m != nil && m.TotalExpend != nil {
		return *m.TotalExpend
	}
	return Default_PigEar_TotalExpend
}

func init() {
	proto.RegisterType((*PigEarReq)(nil), "protocol.PigEarReq")
	proto.RegisterType((*PigEar)(nil), "protocol.PigEar")
}

func init() { proto.RegisterFile("PigEar.proto", fileDescriptor14) }

var fileDescriptor14 = []byte{
	// 239 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x5c, 0x8d, 0x4d, 0x4b, 0xc3, 0x40,
	0x10, 0x86, 0xd9, 0xf8, 0xd9, 0x49, 0x2a, 0x3a, 0x20, 0x2c, 0x7a, 0x30, 0x54, 0x84, 0xf4, 0x12,
	0x4a, 0x8f, 0xde, 0x3c, 0xf4, 0xd0, 0x83, 0x20, 0xa1, 0x78, 0x5f, 0xda, 0x31, 0x2e, 0x24, 0xd9,
	0xb8, 0x1d, 0xc5, 0xfe, 0x5d, 0x7f, 0x89, 0x24, 0x9b, 0x65, 0xa3, 0xa7, 0x61, 0x9e, 0xf7, 0x79,
	0x67, 0x20, 0x79, 0xd1, 0xe5, 0x4a, 0xd9, 0xbc, 0xb5, 0x86, 0x0d, 0x9e, 0xf7, 0x63, 0x6b, 0xaa,
	0x9b, 0xab, 0xf5, 0x8e, 0x1a, 0xd6, 0x7c, 0x78, 0xde, 0x97, 0x2e, 0x9c, 0xbd, 0xc3, 0xc4, 0xc9,
	0x05, 0x7d, 0xe0, 0x1c, 0x8e, 0x3b, 0x43, 0x8a, 0x34, 0xca, 0xe2, 0xe5, 0x75, 0xee, 0x8b, 0xf9,
	0xa8, 0x57, 0xf4, 0x0a, 0x2e, 0x00, 0x5c, 0x6f, 0xdd, 0xbc, 0x19, 0x19, 0xa5, 0x47, 0x59, 0xbc,
	0xbc, 0x0c, 0x85, 0xe1, 0xe6, 0xc8, 0x99, 0xfd, 0x08, 0x38, 0x75, 0x2b, 0xde, 0xc1, 0x64, 0xa3,
	0x6b, 0xda, 0xb3, 0xaa, 0x5b, 0x29, 0x52, 0x91, 0x4d, 0x1f, 0xc5, 0xa2, 0x08, 0x0c, 0x6f, 0xe1,
	0xec, 0xd5, 0x54, 0xac, 0x4a, 0x92, 0x51, 0x2a, 0xb2, 0x93, 0x2e, 0xf6, 0x04, 0xef, 0x21, 0xde,
	0x50, 0xdd, 0x92, 0x55, 0xfc, 0x69, 0x49, 0x82, 0x17, 0xc6, 0x14, 0x1f, 0x20, 0x79, 0xda, 0xb2,
	0xfe, 0xd2, 0x7c, 0xe8, 0xce, 0xca, 0xd8, 0x5b, 0x7f, 0x30, 0xce, 0xe1, 0xc2, 0xef, 0xab, 0xef,
	0x96, 0x9a, 0x9d, 0x4c, 0xbc, 0xf8, 0x2f, 0xe8, 0xdf, 0x1a, 0x56, 0xd5, 0xe0, 0x4d, 0xc3, 0xdb,
	0x40, 0x7f, 0x03, 0x00, 0x00, 0xff, 0xff, 0x1f, 0x8d, 0x4b, 0x96, 0x7a, 0x01, 0x00, 0x00,
}
