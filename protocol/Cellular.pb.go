// Code generated by protoc-gen-go. DO NOT EDIT.
// source: Cellular.proto

package protocol

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

type CellularReq struct {
	Iden             *IdentityMsg `protobuf:"bytes,1,req,name=Iden" json:"Iden,omitempty"`
	CellularInfo     []*Cellular  `protobuf:"bytes,2,rep,name=CellularInfo" json:"CellularInfo,omitempty"`
	XXX_unrecognized []byte       `json:"-"`
}

func (m *CellularReq) Reset()                    { *m = CellularReq{} }
func (m *CellularReq) String() string            { return proto.CompactTextString(m) }
func (*CellularReq) ProtoMessage()               {}
func (*CellularReq) Descriptor() ([]byte, []int) { return fileDescriptor4, []int{0} }

func (m *CellularReq) GetIden() *IdentityMsg {
	if m != nil {
		return m.Iden
	}
	return nil
}

func (m *CellularReq) GetCellularInfo() []*Cellular {
	if m != nil {
		return m.CellularInfo
	}
	return nil
}

type Cellular struct {
	Timestamp             *uint32 `protobuf:"varint,1,opt,name=Timestamp,def=0" json:"Timestamp,omitempty"`
	Longitude             *int32  `protobuf:"zigzag32,2,opt,name=Longitude,def=2000000000" json:"Longitude,omitempty"`
	Latitude              *int32  `protobuf:"zigzag32,3,opt,name=Latitude,def=2000000000" json:"Latitude,omitempty"`
	BatteryVoltage        *int32  `protobuf:"varint,4,opt,name=BatteryVoltage,def=0" json:"BatteryVoltage,omitempty"`
	FirmwareVersion       *int32  `protobuf:"varint,5,opt,name=FirmwareVersion,def=0" json:"FirmwareVersion,omitempty"`
	ErrorFlag             *uint32 `protobuf:"varint,6,opt,name=ErrorFlag,def=0" json:"ErrorFlag,omitempty"`
	TotalTime             *int32  `protobuf:"varint,7,opt,name=TotalTime,def=0" json:"TotalTime,omitempty"`
	HwtTime               *int32  `protobuf:"varint,8,opt,name=HwtTime,def=0" json:"HwtTime,omitempty"`
	SimTime               *int32  `protobuf:"varint,9,opt,name=SimTime,def=0" json:"SimTime,omitempty"`
	NumTime               *int32  `protobuf:"varint,10,opt,name=NumTime,def=0" json:"NumTime,omitempty"`
	RssiTime              *int32  `protobuf:"varint,11,opt,name=RssiTime,def=0" json:"RssiTime,omitempty"`
	RegisterTime          *int32  `protobuf:"varint,12,opt,name=RegisterTime,def=0" json:"RegisterTime,omitempty"`
	AttachTime            *int32  `protobuf:"varint,13,opt,name=AttachTime,def=0" json:"AttachTime,omitempty"`
	PdpTime               *int32  `protobuf:"varint,14,opt,name=PdpTime,def=0" json:"PdpTime,omitempty"`
	ConnectionTime        *int32  `protobuf:"varint,15,opt,name=ConnectionTime,def=0" json:"ConnectionTime,omitempty"`
	CommunicationTime     *int32  `protobuf:"varint,16,opt,name=CommunicationTime,def=0" json:"CommunicationTime,omitempty"`
	NetworkOperator       *uint32 `protobuf:"varint,17,opt,name=NetworkOperator,def=0" json:"NetworkOperator,omitempty"`
	SignalStrength        *int32  `protobuf:"varint,18,opt,name=SignalStrength,def=0" json:"SignalStrength,omitempty"`
	BitErrorRate          *int32  `protobuf:"varint,19,opt,name=BitErrorRate,def=0" json:"BitErrorRate,omitempty"`
	RadioAccessTechnology *int32  `protobuf:"varint,20,opt,name=RadioAccessTechnology,def=0" json:"RadioAccessTechnology,omitempty"`
	Temperature           *int32  `protobuf:"varint,21,opt,name=Temperature,def=0" json:"Temperature,omitempty"`
	SmsTime               *int32  `protobuf:"varint,22,opt,name=SmsTime,def=0" json:"SmsTime,omitempty"`
	ExitFlag              *uint32 `protobuf:"varint,23,opt,name=ExitFlag,def=0" json:"ExitFlag,omitempty"`
	XXX_unrecognized      []byte  `json:"-"`
}

func (m *Cellular) Reset()                    { *m = Cellular{} }
func (m *Cellular) String() string            { return proto.CompactTextString(m) }
func (*Cellular) ProtoMessage()               {}
func (*Cellular) Descriptor() ([]byte, []int) { return fileDescriptor4, []int{1} }

const Default_Cellular_Timestamp uint32 = 0
const Default_Cellular_Longitude int32 = 2000000000
const Default_Cellular_Latitude int32 = 2000000000
const Default_Cellular_BatteryVoltage int32 = 0
const Default_Cellular_FirmwareVersion int32 = 0
const Default_Cellular_ErrorFlag uint32 = 0
const Default_Cellular_TotalTime int32 = 0
const Default_Cellular_HwtTime int32 = 0
const Default_Cellular_SimTime int32 = 0
const Default_Cellular_NumTime int32 = 0
const Default_Cellular_RssiTime int32 = 0
const Default_Cellular_RegisterTime int32 = 0
const Default_Cellular_AttachTime int32 = 0
const Default_Cellular_PdpTime int32 = 0
const Default_Cellular_ConnectionTime int32 = 0
const Default_Cellular_CommunicationTime int32 = 0
const Default_Cellular_NetworkOperator uint32 = 0
const Default_Cellular_SignalStrength int32 = 0
const Default_Cellular_BitErrorRate int32 = 0
const Default_Cellular_RadioAccessTechnology int32 = 0
const Default_Cellular_Temperature int32 = 0
const Default_Cellular_SmsTime int32 = 0
const Default_Cellular_ExitFlag uint32 = 0

func (m *Cellular) GetTimestamp() uint32 {
	if m != nil && m.Timestamp != nil {
		return *m.Timestamp
	}
	return Default_Cellular_Timestamp
}

func (m *Cellular) GetLongitude() int32 {
	if m != nil && m.Longitude != nil {
		return *m.Longitude
	}
	return Default_Cellular_Longitude
}

func (m *Cellular) GetLatitude() int32 {
	if m != nil && m.Latitude != nil {
		return *m.Latitude
	}
	return Default_Cellular_Latitude
}

func (m *Cellular) GetBatteryVoltage() int32 {
	if m != nil && m.BatteryVoltage != nil {
		return *m.BatteryVoltage
	}
	return Default_Cellular_BatteryVoltage
}

func (m *Cellular) GetFirmwareVersion() int32 {
	if m != nil && m.FirmwareVersion != nil {
		return *m.FirmwareVersion
	}
	return Default_Cellular_FirmwareVersion
}

func (m *Cellular) GetErrorFlag() uint32 {
	if m != nil && m.ErrorFlag != nil {
		return *m.ErrorFlag
	}
	return Default_Cellular_ErrorFlag
}

func (m *Cellular) GetTotalTime() int32 {
	if m != nil && m.TotalTime != nil {
		return *m.TotalTime
	}
	return Default_Cellular_TotalTime
}

func (m *Cellular) GetHwtTime() int32 {
	if m != nil && m.HwtTime != nil {
		return *m.HwtTime
	}
	return Default_Cellular_HwtTime
}

func (m *Cellular) GetSimTime() int32 {
	if m != nil && m.SimTime != nil {
		return *m.SimTime
	}
	return Default_Cellular_SimTime
}

func (m *Cellular) GetNumTime() int32 {
	if m != nil && m.NumTime != nil {
		return *m.NumTime
	}
	return Default_Cellular_NumTime
}

func (m *Cellular) GetRssiTime() int32 {
	if m != nil && m.RssiTime != nil {
		return *m.RssiTime
	}
	return Default_Cellular_RssiTime
}

func (m *Cellular) GetRegisterTime() int32 {
	if m != nil && m.RegisterTime != nil {
		return *m.RegisterTime
	}
	return Default_Cellular_RegisterTime
}

func (m *Cellular) GetAttachTime() int32 {
	if m != nil && m.AttachTime != nil {
		return *m.AttachTime
	}
	return Default_Cellular_AttachTime
}

func (m *Cellular) GetPdpTime() int32 {
	if m != nil && m.PdpTime != nil {
		return *m.PdpTime
	}
	return Default_Cellular_PdpTime
}

func (m *Cellular) GetConnectionTime() int32 {
	if m != nil && m.ConnectionTime != nil {
		return *m.ConnectionTime
	}
	return Default_Cellular_ConnectionTime
}

func (m *Cellular) GetCommunicationTime() int32 {
	if m != nil && m.CommunicationTime != nil {
		return *m.CommunicationTime
	}
	return Default_Cellular_CommunicationTime
}

func (m *Cellular) GetNetworkOperator() uint32 {
	if m != nil && m.NetworkOperator != nil {
		return *m.NetworkOperator
	}
	return Default_Cellular_NetworkOperator
}

func (m *Cellular) GetSignalStrength() int32 {
	if m != nil && m.SignalStrength != nil {
		return *m.SignalStrength
	}
	return Default_Cellular_SignalStrength
}

func (m *Cellular) GetBitErrorRate() int32 {
	if m != nil && m.BitErrorRate != nil {
		return *m.BitErrorRate
	}
	return Default_Cellular_BitErrorRate
}

func (m *Cellular) GetRadioAccessTechnology() int32 {
	if m != nil && m.RadioAccessTechnology != nil {
		return *m.RadioAccessTechnology
	}
	return Default_Cellular_RadioAccessTechnology
}

func (m *Cellular) GetTemperature() int32 {
	if m != nil && m.Temperature != nil {
		return *m.Temperature
	}
	return Default_Cellular_Temperature
}

func (m *Cellular) GetSmsTime() int32 {
	if m != nil && m.SmsTime != nil {
		return *m.SmsTime
	}
	return Default_Cellular_SmsTime
}

func (m *Cellular) GetExitFlag() uint32 {
	if m != nil && m.ExitFlag != nil {
		return *m.ExitFlag
	}
	return Default_Cellular_ExitFlag
}

func init() {
	proto.RegisterType((*CellularReq)(nil), "protocol.CellularReq")
	proto.RegisterType((*Cellular)(nil), "protocol.Cellular")
}

func init() { proto.RegisterFile("Cellular.proto", fileDescriptor4) }

var fileDescriptor4 = []byte{
	// 492 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x6c, 0x92, 0x5d, 0x6f, 0x12, 0x41,
	0x14, 0x86, 0xb3, 0xf4, 0x0b, 0x0e, 0x94, 0xca, 0x28, 0x3a, 0xd1, 0x18, 0xb1, 0x46, 0xb3, 0x8d,
	0x09, 0x36, 0x5c, 0x68, 0xd2, 0xbb, 0x96, 0xb4, 0xb1, 0x49, 0xad, 0x66, 0x21, 0xbd, 0x9f, 0x2c,
	0xc7, 0x65, 0xe2, 0xee, 0x0c, 0xce, 0x1e, 0x82, 0xfc, 0x64, 0xff, 0x85, 0x99, 0x81, 0x99, 0x05,
	0x2a, 0x37, 0x24, 0xef, 0xf3, 0x9c, 0xe1, 0x7c, 0x00, 0xed, 0x21, 0xe6, 0xf9, 0x3c, 0x17, 0xa6,
	0x3f, 0x33, 0x9a, 0x34, 0xab, 0xbb, 0xaf, 0x54, 0xe7, 0x2f, 0x3b, 0xb7, 0x13, 0x54, 0x24, 0x69,
	0xf9, 0xad, 0xcc, 0x56, 0xf0, 0x74, 0x06, 0x4d, 0xaf, 0x27, 0xf8, 0x9b, 0x9d, 0xc1, 0xbe, 0x75,
	0x78, 0xd4, 0xab, 0xc5, 0xcd, 0x41, 0xb7, 0xef, 0x4b, 0xfb, 0x1b, 0x95, 0x89, 0x53, 0xd8, 0x67,
	0x68, 0xf9, 0xca, 0x5b, 0xf5, 0x53, 0xf3, 0x5a, 0x6f, 0x2f, 0x6e, 0x0e, 0x58, 0x55, 0x12, 0xde,
	0xdd, 0xf2, 0x4e, 0xff, 0x1e, 0x42, 0xdd, 0x07, 0xec, 0x0d, 0x34, 0xc6, 0xb2, 0xc0, 0x92, 0x44,
	0x31, 0xe3, 0x51, 0x2f, 0x8a, 0x8f, 0x2f, 0xa2, 0xf3, 0xa4, 0xca, 0x58, 0x0c, 0x8d, 0x3b, 0xad,
	0x32, 0x49, 0xf3, 0x09, 0xf2, 0x5a, 0x2f, 0x8a, 0x3b, 0x17, 0x30, 0x38, 0xf7, 0x9f, 0xa4, 0x82,
	0xec, 0x03, 0xd4, 0xef, 0x04, 0xad, 0xc4, 0xbd, 0x47, 0x62, 0x60, 0xec, 0x0c, 0xda, 0x57, 0x82,
	0x08, 0xcd, 0xf2, 0x41, 0xe7, 0x24, 0x32, 0xe4, 0xfb, 0xbd, 0x28, 0x3e, 0xb0, 0xbf, 0xbb, 0x03,
	0xd8, 0x47, 0x38, 0xb9, 0x91, 0xa6, 0x58, 0x08, 0x83, 0x0f, 0x68, 0x4a, 0xa9, 0x15, 0x3f, 0xf0,
	0xee, 0x2e, 0xb1, 0xa3, 0x5c, 0x1b, 0xa3, 0xcd, 0x4d, 0x2e, 0x32, 0x7e, 0x18, 0x46, 0x09, 0x99,
	0x9b, 0x55, 0x93, 0xc8, 0xed, 0x70, 0xfc, 0xc8, 0xbf, 0x53, 0x65, 0xec, 0x15, 0x1c, 0x7d, 0x5d,
	0x90, 0xc3, 0x75, 0x8f, 0x7d, 0x62, 0xe1, 0x48, 0x16, 0x0e, 0x36, 0x02, 0x5c, 0x27, 0x16, 0xde,
	0xcf, 0x57, 0x10, 0x02, 0x5c, 0x27, 0xec, 0x35, 0xd4, 0x93, 0xb2, 0x94, 0x8e, 0x36, 0x3d, 0x0d,
	0x11, 0x7b, 0x0f, 0xad, 0x04, 0x33, 0x59, 0x12, 0x1a, 0xa7, 0xb4, 0xbc, 0xb2, 0x15, 0xb3, 0xb7,
	0x00, 0x97, 0x44, 0x22, 0x9d, 0x3a, 0xe9, 0xd8, 0x4b, 0x1b, 0xa1, 0xed, 0xe2, 0xc7, 0x64, 0xe6,
	0x78, 0x3b, 0x74, 0xb1, 0x4e, 0xec, 0xda, 0x87, 0x5a, 0x29, 0x4c, 0x49, 0x6a, 0xe5, 0x9c, 0x93,
	0xb0, 0xf6, 0x6d, 0xc0, 0x3e, 0x41, 0x67, 0xa8, 0x8b, 0x62, 0xae, 0x64, 0x2a, 0x82, 0xfd, 0xc4,
	0xdb, 0x8f, 0x99, 0xbd, 0xd3, 0x3d, 0xd2, 0x42, 0x9b, 0x5f, 0xdf, 0x67, 0x68, 0x04, 0x69, 0xc3,
	0x3b, 0xfe, 0x00, 0xbb, 0xc4, 0x36, 0x32, 0x92, 0x99, 0x12, 0xf9, 0x88, 0x0c, 0xaa, 0x8c, 0xa6,
	0x9c, 0x85, 0x46, 0xb6, 0x81, 0x5d, 0xcd, 0x95, 0x24, 0x77, 0xc1, 0x44, 0x10, 0xf2, 0xa7, 0x61,
	0x35, 0x9b, 0x31, 0xfb, 0x02, 0xdd, 0x44, 0x4c, 0xa4, 0xbe, 0x4c, 0x53, 0x2c, 0xcb, 0x31, 0xa6,
	0x53, 0xa5, 0x73, 0x9d, 0x2d, 0xf9, 0x33, 0xef, 0xff, 0x9f, 0xb3, 0x77, 0xd0, 0x1c, 0x63, 0xe1,
	0x1a, 0x9b, 0x1b, 0xe4, 0x5d, 0xaf, 0x6f, 0xa6, 0xee, 0xf0, 0x45, 0xe9, 0x76, 0xf0, 0xbc, 0x3a,
	0xfc, 0x2a, 0xb1, 0xb7, 0xbd, 0xfe, 0x23, 0xc9, 0xfd, 0xe7, 0x5e, 0xf8, 0x91, 0x43, 0xf4, 0x2f,
	0x00, 0x00, 0xff, 0xff, 0x4e, 0x82, 0xb3, 0x60, 0x0b, 0x04, 0x00, 0x00,
}
