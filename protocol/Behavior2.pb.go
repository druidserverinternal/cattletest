// Code generated by protoc-gen-go. DO NOT EDIT.
// source: Behavior2.proto

package protocol

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

type Behavior2Req struct {
	Iden             *IdentityMsg `protobuf:"bytes,1,req,name=Iden" json:"Iden,omitempty"`
	BehaviorInfo     []*Behavior2 `protobuf:"bytes,2,rep,name=BehaviorInfo" json:"BehaviorInfo,omitempty"`
	XXX_unrecognized []byte       `json:"-"`
}

func (m *Behavior2Req) Reset()                    { *m = Behavior2Req{} }
func (m *Behavior2Req) String() string            { return proto.CompactTextString(m) }
func (*Behavior2Req) ProtoMessage()               {}
func (*Behavior2Req) Descriptor() ([]byte, []int) { return fileDescriptor3, []int{0} }

func (m *Behavior2Req) GetIden() *IdentityMsg {
	if m != nil {
		return m.Iden
	}
	return nil
}

func (m *Behavior2Req) GetBehaviorInfo() []*Behavior2 {
	if m != nil {
		return m.BehaviorInfo
	}
	return nil
}

type Behavior2 struct {
	Timestamp        *uint32 `protobuf:"varint,1,opt,name=Timestamp,def=0" json:"Timestamp,omitempty"`
	ODBAX            *int32  `protobuf:"varint,2,opt,name=ODBAX,def=0" json:"ODBAX,omitempty"`
	ODBAY            *int32  `protobuf:"varint,3,opt,name=ODBAY,def=0" json:"ODBAY,omitempty"`
	ODBAZ            *int32  `protobuf:"varint,4,opt,name=ODBAZ,def=0" json:"ODBAZ,omitempty"`
	MeandlX          *int32  `protobuf:"varint,5,opt,name=MeandlX,def=0" json:"MeandlX,omitempty"`
	MeandlY          *int32  `protobuf:"varint,6,opt,name=MeandlY,def=0" json:"MeandlY,omitempty"`
	MeandlZ          *int32  `protobuf:"varint,7,opt,name=MeandlZ,def=0" json:"MeandlZ,omitempty"`
	ODBA             *int32  `protobuf:"varint,8,opt,name=ODBA,def=0" json:"ODBA,omitempty"`
	XXX_unrecognized []byte  `json:"-"`
}

func (m *Behavior2) Reset()                    { *m = Behavior2{} }
func (m *Behavior2) String() string            { return proto.CompactTextString(m) }
func (*Behavior2) ProtoMessage()               {}
func (*Behavior2) Descriptor() ([]byte, []int) { return fileDescriptor3, []int{1} }

const Default_Behavior2_Timestamp uint32 = 0
const Default_Behavior2_ODBAX int32 = 0
const Default_Behavior2_ODBAY int32 = 0
const Default_Behavior2_ODBAZ int32 = 0
const Default_Behavior2_MeandlX int32 = 0
const Default_Behavior2_MeandlY int32 = 0
const Default_Behavior2_MeandlZ int32 = 0
const Default_Behavior2_ODBA int32 = 0

func (m *Behavior2) GetTimestamp() uint32 {
	if m != nil && m.Timestamp != nil {
		return *m.Timestamp
	}
	return Default_Behavior2_Timestamp
}

func (m *Behavior2) GetODBAX() int32 {
	if m != nil && m.ODBAX != nil {
		return *m.ODBAX
	}
	return Default_Behavior2_ODBAX
}

func (m *Behavior2) GetODBAY() int32 {
	if m != nil && m.ODBAY != nil {
		return *m.ODBAY
	}
	return Default_Behavior2_ODBAY
}

func (m *Behavior2) GetODBAZ() int32 {
	if m != nil && m.ODBAZ != nil {
		return *m.ODBAZ
	}
	return Default_Behavior2_ODBAZ
}

func (m *Behavior2) GetMeandlX() int32 {
	if m != nil && m.MeandlX != nil {
		return *m.MeandlX
	}
	return Default_Behavior2_MeandlX
}

func (m *Behavior2) GetMeandlY() int32 {
	if m != nil && m.MeandlY != nil {
		return *m.MeandlY
	}
	return Default_Behavior2_MeandlY
}

func (m *Behavior2) GetMeandlZ() int32 {
	if m != nil && m.MeandlZ != nil {
		return *m.MeandlZ
	}
	return Default_Behavior2_MeandlZ
}

func (m *Behavior2) GetODBA() int32 {
	if m != nil && m.ODBA != nil {
		return *m.ODBA
	}
	return Default_Behavior2_ODBA
}

func init() {
	proto.RegisterType((*Behavior2Req)(nil), "protocol.Behavior2Req")
	proto.RegisterType((*Behavior2)(nil), "protocol.Behavior2")
}

func init() { proto.RegisterFile("Behavior2.proto", fileDescriptor3) }

var fileDescriptor3 = []byte{
	// 227 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0xe2, 0x77, 0x4a, 0xcd, 0x48,
	0x2c, 0xcb, 0xcc, 0x2f, 0x32, 0xd2, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0xe2, 0x00, 0x53, 0xc9,
	0xf9, 0x39, 0x52, 0x82, 0x9e, 0x29, 0xa9, 0x79, 0x25, 0x99, 0x25, 0x95, 0xbe, 0xc5, 0xe9, 0x10,
	0x49, 0xa5, 0x22, 0x2e, 0x1e, 0xb8, 0xfa, 0xa0, 0xd4, 0x42, 0x21, 0x4d, 0x2e, 0x16, 0x90, 0x22,
	0x09, 0x46, 0x05, 0x26, 0x0d, 0x6e, 0x23, 0x51, 0x3d, 0x98, 0x5e, 0x3d, 0x24, 0xad, 0x41, 0x60,
	0x25, 0x42, 0xe6, 0x08, 0xad, 0x9e, 0x79, 0x69, 0xf9, 0x12, 0x4c, 0x0a, 0xcc, 0x1a, 0xdc, 0x46,
	0xc2, 0x08, 0x2d, 0x08, 0x83, 0x51, 0x14, 0x2a, 0x3d, 0x65, 0xe4, 0xe2, 0x84, 0xcb, 0x09, 0xc9,
	0x73, 0x71, 0x86, 0x64, 0xe6, 0xa6, 0x16, 0x97, 0x24, 0xe6, 0x16, 0x48, 0x30, 0x2a, 0x30, 0x6a,
	0xf0, 0x5a, 0x31, 0x1a, 0x04, 0x21, 0xc4, 0x84, 0xc4, 0xb9, 0x58, 0xfd, 0x5d, 0x9c, 0x1c, 0x23,
	0x24, 0x98, 0x14, 0x18, 0x35, 0x58, 0x41, 0x92, 0x10, 0x3e, 0x4c, 0x22, 0x52, 0x82, 0x19, 0x45,
	0x22, 0x12, 0x26, 0x11, 0x25, 0xc1, 0x82, 0x22, 0x11, 0x25, 0x24, 0xcd, 0xc5, 0xee, 0x9b, 0x9a,
	0x98, 0x97, 0x92, 0x13, 0x21, 0xc1, 0x0a, 0x93, 0x82, 0x89, 0x20, 0x24, 0x23, 0x25, 0xd8, 0xd0,
	0x24, 0x23, 0x11, 0x92, 0x51, 0x12, 0xec, 0x68, 0x92, 0x51, 0x42, 0xa2, 0x5c, 0x2c, 0x20, 0xf3,
	0x25, 0x38, 0x60, 0x32, 0x60, 0x2e, 0x20, 0x00, 0x00, 0xff, 0xff, 0xeb, 0xd8, 0x24, 0x4d, 0x8a,
	0x01, 0x00, 0x00,
}
