// Code generated by protoc-gen-go. DO NOT EDIT.
// source: Define.proto

package protocol

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

type HeaderType int32

const (
	HeaderType_TypeRegisterReq        HeaderType = 1
	HeaderType_TypeRegisterRsp        HeaderType = 2
	HeaderType_TypeGPSReq             HeaderType = 3
	HeaderType_TypeGPSRsp             HeaderType = 4
	HeaderType_TypeBehaviorReq        HeaderType = 5
	HeaderType_TypeBehaviorRsp        HeaderType = 6
	HeaderType_TypeDownloadReq        HeaderType = 7
	HeaderType_TypeDownloadRsp        HeaderType = 8
	HeaderType_TypeStatusReq          HeaderType = 9
	HeaderType_TypeStatusRsp          HeaderType = 10
	HeaderType_TypeSettingReq         HeaderType = 11
	HeaderType_TypeSettingRsp         HeaderType = 12
	HeaderType_TypePingReq            HeaderType = 13
	HeaderType_TypePingRsp            HeaderType = 14
	HeaderType_TypeAppReq             HeaderType = 15
	HeaderType_TypeAppRsp             HeaderType = 16
	HeaderType_TypeTransferSettingReq HeaderType = 17
	HeaderType_TypeTransferSettingRsp HeaderType = 18
	HeaderType_TypePigSettingReq      HeaderType = 19
	HeaderType_TypePigSettingRsp      HeaderType = 20
	HeaderType_TypeEnvSettingReq      HeaderType = 21
	HeaderType_TypeEnvSettingRsp      HeaderType = 22
	HeaderType_TypePigEarReq          HeaderType = 23
	HeaderType_TypePigEarRsp          HeaderType = 24
	HeaderType_TypeEnvironmentReq     HeaderType = 25
	HeaderType_TypeEnvironmentRsp     HeaderType = 26
	HeaderType_TypeTransferReq        HeaderType = 27
	HeaderType_TypeTransferRsp        HeaderType = 28
	HeaderType_TypeGPS2Req            HeaderType = 29
	HeaderType_TypeSensorReq          HeaderType = 31
	HeaderType_TypeSensorRsp          HeaderType = 32
	HeaderType_TypeCellularReq        HeaderType = 33
	HeaderType_TypeCellularRsp        HeaderType = 34
	HeaderType_TypeDebugReq           HeaderType = 10001
	HeaderType_TypeDebugRsp           HeaderType = 10002
	HeaderType_TypeBehavior2Req       HeaderType = 35
	HeaderType_TypeBehavior2Rsp       HeaderType = 36
)

var HeaderType_name = map[int32]string{
	1:     "TypeRegisterReq",
	2:     "TypeRegisterRsp",
	3:     "TypeGPSReq",
	4:     "TypeGPSRsp",
	5:     "TypeBehaviorReq",
	6:     "TypeBehaviorRsp",
	7:     "TypeDownloadReq",
	8:     "TypeDownloadRsp",
	9:     "TypeStatusReq",
	10:    "TypeStatusRsp",
	11:    "TypeSettingReq",
	12:    "TypeSettingRsp",
	13:    "TypePingReq",
	14:    "TypePingRsp",
	15:    "TypeAppReq",
	16:    "TypeAppRsp",
	17:    "TypeTransferSettingReq",
	18:    "TypeTransferSettingRsp",
	19:    "TypePigSettingReq",
	20:    "TypePigSettingRsp",
	21:    "TypeEnvSettingReq",
	22:    "TypeEnvSettingRsp",
	23:    "TypePigEarReq",
	24:    "TypePigEarRsp",
	25:    "TypeEnvironmentReq",
	26:    "TypeEnvironmentRsp",
	27:    "TypeTransferReq",
	28:    "TypeTransferRsp",
	29:    "TypeGPS2Req",
	31:    "TypeSensorReq",
	32:    "TypeSensorRsp",
	33:    "TypeCellularReq",
	34:    "TypeCellularRsp",
	10001: "TypeDebugReq",
	10002: "TypeDebugRsp",
	35:    "TypeBehavior2Req",
	36:    "TypeBehavior2Rsp",
}
var HeaderType_value = map[string]int32{
	"TypeRegisterReq":        1,
	"TypeRegisterRsp":        2,
	"TypeGPSReq":             3,
	"TypeGPSRsp":             4,
	"TypeBehaviorReq":        5,
	"TypeBehaviorRsp":        6,
	"TypeDownloadReq":        7,
	"TypeDownloadRsp":        8,
	"TypeStatusReq":          9,
	"TypeStatusRsp":          10,
	"TypeSettingReq":         11,
	"TypeSettingRsp":         12,
	"TypePingReq":            13,
	"TypePingRsp":            14,
	"TypeAppReq":             15,
	"TypeAppRsp":             16,
	"TypeTransferSettingReq": 17,
	"TypeTransferSettingRsp": 18,
	"TypePigSettingReq":      19,
	"TypePigSettingRsp":      20,
	"TypeEnvSettingReq":      21,
	"TypeEnvSettingRsp":      22,
	"TypePigEarReq":          23,
	"TypePigEarRsp":          24,
	"TypeEnvironmentReq":     25,
	"TypeEnvironmentRsp":     26,
	"TypeTransferReq":        27,
	"TypeTransferRsp":        28,
	"TypeGPS2Req":            29,
	"TypeSensorReq":          31,
	"TypeSensorRsp":          32,
	"TypeCellularReq":        33,
	"TypeCellularRsp":        34,
	"TypeDebugReq":           10001,
	"TypeDebugRsp":           10002,
	"TypeBehavior2Req":       35,
	"TypeBehavior2Rsp":       36,
}

func (x HeaderType) Enum() *HeaderType {
	p := new(HeaderType)
	*p = x
	return p
}
func (x HeaderType) String() string {
	return proto.EnumName(HeaderType_name, int32(x))
}
func (x *HeaderType) UnmarshalJSON(data []byte) error {
	value, err := proto.UnmarshalJSONEnum(HeaderType_value, data, "HeaderType")
	if err != nil {
		return err
	}
	*x = HeaderType(value)
	return nil
}
func (HeaderType) EnumDescriptor() ([]byte, []int) { return fileDescriptor6, []int{0} }

func init() {
	proto.RegisterEnum("protocol.HeaderType", HeaderType_name, HeaderType_value)
}

func init() { proto.RegisterFile("Define.proto", fileDescriptor6) }

var fileDescriptor6 = []byte{
	// 358 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x74, 0xd2, 0xdb, 0x4e, 0xf2, 0x40,
	0x10, 0x07, 0xf0, 0x7c, 0x9f, 0x8a, 0x38, 0x9c, 0xa6, 0xcb, 0x41, 0x45, 0x8d, 0xc7, 0x2b, 0x2f,
	0xbc, 0xf0, 0x0d, 0x54, 0x08, 0x5e, 0x91, 0x06, 0x78, 0x81, 0x2a, 0x43, 0x6d, 0x52, 0xb7, 0x63,
	0xa7, 0x60, 0xbc, 0xf4, 0x11, 0xf4, 0x89, 0xcd, 0x16, 0xd6, 0x94, 0x16, 0xaf, 0x9a, 0xfd, 0xf5,
	0xbf, 0xa7, 0xd9, 0x81, 0x6a, 0x8f, 0x66, 0x81, 0xa6, 0x1b, 0x8e, 0xa3, 0x24, 0x52, 0xe5, 0xf4,
	0xf3, 0x1c, 0x85, 0xd7, 0x9f, 0x25, 0x80, 0x47, 0xf2, 0xa6, 0x14, 0x4f, 0x3e, 0x98, 0x54, 0x13,
	0x1a, 0xe6, 0x3b, 0x22, 0x3f, 0x90, 0x84, 0xe2, 0x11, 0xbd, 0xe1, 0xbf, 0x02, 0x0a, 0xe3, 0x7f,
	0x55, 0x07, 0x30, 0x38, 0x70, 0xc7, 0x26, 0xb4, 0x95, 0x1d, 0x0b, 0xe3, 0xb6, 0x9d, 0x74, 0x4f,
	0x2f, 0xde, 0x22, 0x88, 0xd2, 0x95, 0x76, 0x0a, 0x28, 0x8c, 0x25, 0x8b, 0xbd, 0xe8, 0x5d, 0x87,
	0x91, 0x37, 0x35, 0xc9, 0xdd, 0x02, 0x0a, 0x63, 0x59, 0x39, 0x50, 0x33, 0x38, 0x4e, 0xbc, 0x64,
	0x2e, 0x26, 0xb7, 0x97, 0x23, 0x61, 0x04, 0xa5, 0xa0, 0x9e, 0x12, 0x25, 0x49, 0xa0, 0x7d, 0x13,
	0xab, 0xe4, 0x4d, 0x18, 0xab, 0xaa, 0x01, 0x15, 0x63, 0xee, 0x2a, 0x54, 0x5b, 0x03, 0x61, 0xac,
	0xdb, 0x3b, 0xdd, 0x31, 0x9b, 0x40, 0x23, 0x3b, 0x16, 0x46, 0x54, 0x5d, 0xe8, 0x98, 0xf1, 0x24,
	0xf6, 0xb4, 0xcc, 0x28, 0xce, 0xec, 0xe8, 0xfc, 0xf5, 0x4f, 0x18, 0x95, 0x6a, 0x83, 0xb3, 0xdc,
	0xc8, 0xcf, 0x4c, 0x69, 0x6e, 0x60, 0x61, 0x6c, 0x59, 0xee, 0xeb, 0x45, 0x26, 0xdd, 0xde, 0xc0,
	0xc2, 0xd8, 0xb1, 0x05, 0x71, 0x03, 0xbf, 0xef, 0xa5, 0x55, 0xdf, 0xcf, 0x91, 0x30, 0x1e, 0xa8,
	0x0e, 0xa8, 0xd5, 0xe4, 0x20, 0x8e, 0xf4, 0x2b, 0xe9, 0xc4, 0x44, 0x0f, 0x37, 0xb9, 0x30, 0x76,
	0xed, 0x73, 0xd8, 0xdb, 0x98, 0xf0, 0x51, 0x01, 0x85, 0xf1, 0xd8, 0x16, 0x71, 0xe0, 0x8e, 0x6f,
	0x4d, 0xea, 0xe4, 0xf7, 0x85, 0x48, 0xcb, 0xb2, 0x0d, 0x4e, 0x73, 0x24, 0x8c, 0x67, 0x76, 0xad,
	0x07, 0x0a, 0xc3, 0x79, 0xb8, 0x3c, 0xf8, 0x79, 0x01, 0x85, 0xf1, 0x42, 0x39, 0x50, 0x4d, 0x3b,
	0x83, 0x9e, 0xe6, 0x69, 0x25, 0xbe, 0x86, 0xeb, 0x24, 0x8c, 0xdf, 0x43, 0xd5, 0x02, 0xcc, 0x76,
	0x5a, 0x7a, 0x96, 0xcb, 0xa2, 0x0a, 0xe3, 0xd5, 0x4f, 0x00, 0x00, 0x00, 0xff, 0xff, 0xfc, 0x8e,
	0x1b, 0xf9, 0x1c, 0x03, 0x00, 0x00,
}
