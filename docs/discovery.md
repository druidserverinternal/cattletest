# 圈养用户端后台API
 
**最后修改时间 ：2018.4.5**

## [API目录](#Cmd11)

## 相关注意事项
### Protocol and Format
The API server through HTTP now. Both request and response are encoded in `application/json`using`utf-8`.

### Authentication
**由登录后返回的head中，X-Druid-Authentication：字段得到**
例如
```
"X-Druid-Authentication" : "v=1;id=59f06af9a8d1675578d3d58d;n=16dw535t;expiry=2017-11-24T10:49:59Z;hmac=coOwxMAgFIlk17AGVVAz6vAIm0rhk9eSt1OUibuXOF0="
```

### Errors

The following are common error responses from the server. They will not be repeated in each API specification.

Code | Meaning  |
:----|:---------|
400  | Bad Request - The request is malformed.
401  | Unauthorized - The authentication token is not valid.
403  | Forbidden - The user is not authorized to make this request.
404  | Not Found - The resource could not be found.
500  | Internal Server Error - Something went wrong on the server side.

The body of the error response is encoded in JSON, like the following:

```
{
  "Code": 500,
  "message": "Internal Server Error"
}
```

### Time Format

The time in the API request and response is using UTC timezone and [RFC3339](https://www.ietf.org/rfc/rfc3339.txt) standard format.

## Request page parmeter

```
HTTP Request:
X-Result-Limit: 100
X-Result-Offset: 100
X-Result-Sort: -updated_at,-timestamp
X-Result-Sort: updated_at


HTTP Response:
X-Result-Count: 100000000

sort is sort by xxx, - is reverse
offset is sorted result offset
limit is sorted result limit
```

<div id="Cmd0"></div>   


####注意如果没有特别说明 该API请求方式和奶牛的一直，以下是奶牛的API，新增API [点击](#Cmd001)
## API 目录
|API|说明|地址|
|---|---|---|
|[Login](#Cmd101) |[用户登录](#Cmd101)|POST v1/login|
|·····|·····|······|
||**用户**||
|[Update Password](#Cmd201)|[更新密码](#Cmd201)|PUT v1/user/password|
|[Update User Info](#Cmd202)|[更新用户信息](#Cmd202)|PUT v1/user/info|
|[Update Profile](#Cmd203)|[更新配置文件 ](#Cmd203)|PUT v1/user/profile|
|[GET Myself](#Cmd204)|[获取用户信息](#Cmd204)|GET  v1/user/myself|
|·····|·····|······|
||**公司**||
|[Get Company](#Cmd301) |[获取公司信息](#Cmd301)|GET v1/company/|
|·····|·····|······|
||**设备**||
|[List Devices](#Cmd401)|[设备列表](#Cmd401)|GET v1/device/|
|[Get Device By UUID](#Cmd402)|[通过UUDI获取设备](#Cmd402)|GET v1/device/uuid/:id|
|[Get List Device By Specise](#Cmd403)|[通过物种查找设备](#Cmd403)|GET v1/device/species/:id|
|[GET Device By ID](#Cmd404)|[通过ID获取设备](#Cmd404)|GET  v1/device/id/:id|
|[GET Area By DeivceID](#Cmd405)|[通过设备ID获取围栏列表](#Cmd405)|GET  v1/device/id/:id/area|
|[Search Device By mark](#Cmd406)|[通过Mark搜索设备](#Cmd406)|GET  v1/device/search/mark/:mark|
|[Search Device By SN](#Cmd407)|[通过SN搜索设备](#Cmd407)|GET  v1/device/search/sn/:sn|
|[GET Room Is idle](#Cmd408)|[获取闲置的围栏列表](#Cmd408)|GET  v1/device/idle/room|
|[List Many Devices By id](#Cmd409)|[通过IDs获取设备信息](#Cmd409)|POST  v1/device/many|
|[Regsier](#Cmd410)|[注册设备](#Cmd410)|POST  v1/device/register|
|[List Devices By Exclude](#Cmd411)|[列出条件以外的设备](#Cmd411)|POST  v1/device/exclude|
|[Put Device](#Cmd412)|[更新设备信息](#Cmd412)|PUT  v1/device/id/:id|
|·····|·····|······|
||**GPS**||
|[Get Device GPS](#Cmd501)|[获取设备GPS](#Cmd501)|GET v1/gps/device/:id|
|·····|·····|······|
||**行为分析**||
|[Get Device Behavior](#Cmd601)|[获取设备行为分析数据](#Cmd601)|GET v1/behavior/device/:id|
|[Get Behavior analyze](#Cmd602)|[获取行为分析数据列表](#Cmd602)|POST v1/behavior/analyze/|
|·····|·····|······|
||**活动量/发情分析**||
|[Get ODBA Analyze Data](#Cmd701)|[获取活动量分析数据](#Cmd701)|POST v1/odba/analyze/|
|[Get Loving Analyze Data](#Cmd702)|[获取发情分析数据列表](#Cmd702)|POST v1/odba/loving/|
|·····|······|·····|
||**搜索**||
|[Search Device](#Cmd801)|[搜索设备](#Cmd801)|POST v1/search/device/|
|·····|······|·····|
||**围栏**||
|[Create area](#Cmd901)|[创建围栏](#Cmd901)|POST v1/ditu/|
|[Add Device to Area](#Cmd902)|[添加设备进围栏](#Cmd902)|PUT v1/ditu/area/:id/adddevice/:device|
|[Delete Device to Area](#Cmd903)|[删除围栏里的设备](#Cmd903)|PUT v1/ditu/area/:id/deldevice/:device|
|[Update Area Info](#Cmd904)|[更新围栏信息](#Cmd904)|PUT v1/ditu/area/:id|
|[Delete Area](#Cmd905)|[删除围栏](#Cmd905)|DELETE v1/ditu/area/:id|
|[Get Device in area](#Cmd906)|[获取围栏里的设备](#Cmd906)|GET v1/ditu/area/:id/device|
|[List Area](#Cmd907)|[获取围栏列表](#Cmd907)|GET v1/ditu/area|
|[Get Area info with ID](#Cmd908)|[获取围栏信息](#Cmd908)|GET v1/ditu/area/:id|
|······|······|······|
||**分组**||
|[Create Room](#Cmd1001)|[创建分组](#Cmd1001)|POST v1/room/|
|[Add Device to Room](#Cmd1002)|[添加设备进分组](#Cmd1002)|PUT v1/room/id/:id/adddevice|
|[Delete Device to Room](#Cmd1003)|[移除分组里的设备](#Cmd1003)|PUT v1/room/id/:id/deldevice|
|[Update room Info](#Cmd1004)|[更新分组信息](#Cmd1004)|PUT v1/room/id/:id|
|[Delete Room](#Cmd1005)|[删除分组](#Cmd1005)|DELETE v1/room/id/:id|
|[List Room](#Cmd1006)|[获取分组列表](#Cmd1006)|GET v1/room/|
|[Get Room info](#Cmd1007)|[获取分组信息](#Cmd1007)|GET v1/room/id/:id/info|
|[Get Device in Room](#Cmd1008)|[获取分组内的设备列表](#Cmd1008)|GET v1/room/id/:id/device| 
|······|······|·······|
||**生物**||
|[Delete Image by biological](#Cmd1101)|[删除图片通过生物ID](#Cmd1101)|DELETE v1/biological/cattle/id/:id/image/:image_id|
|[Delete Image By device](#Cmd1102)|[删除图片通过设备ID](#Cmd1102)|DELETE v1/biological/cattle/device/:id/image/:image_id|
|[Get Cattle By DeviceId](#Cmd1103)|[通过设备ID得到生物信息](#Cmd1103)|GET v1/biological/cattle/device/:id|
|[Update Cattle Info By DeviceID](#Cmd1104)|[通过设备ID更新生物信息](#Cmd1104)|PUT v1/biological/cattle/device/:id|
|[Update image by Device](#Cmd1105)|[通过设备ID上传照片](#Cmd1105)|PUT v1/device/cattle/device/:id/image/:name|
|[Update iamge by Device(no name)](#Cmd1106)|[通过设备ID上传照片](#Cmd1106)|GET v1/biological/cattle/device/:id/image|
|·····|······|······|
||**文件**||
|[Get iamge](#Cmd1201)|[获取图片](#Cmd1201)|GET v1/file/device/:id/image/:image_id|
|[Get thumbnail Image](#Cmd1202)|[获取小图](#Cmd1202)|GET v1/file/device/:id/image/:image_id/thumbnail|
|·····|·····|······|
||**weixin**||
|[Get Token](#Cmd1301)|[获取认证](#Cmd1301)|GET v1/weixin/token|
|[Get signature](#Cmd1302)|[获取签名](#Cmd1302)|GET v1/weixin/signature|
|·····|·····|······|
||**消息**||
|[Get Unread Message](#Cmd1401)|[获取未读消息](#Cmd1401)|GET v1/message/unread|
|[List message](#Cmd1402)|[获取消息列表](#Cmd1402)|GET v1/message/|
|[List message by type](#Cmd1403)|[获取消息列表](#Cmd1403)|GET v1/message/type/:type|
|[PUT message read](#Cmd1404)|[标记消息为已读消息](#Cmd1404)|PUT v1/message/|
|[Delete message](#Cmd1405)|[删除一条消息](#Cmd1405)|DELETE v1/message/id/:id|
|[Deltet Many message](#Cmd1406)|[删除多条消息](#Cmd1406)|PUT v1/message/delete| 

<div id="Cmd001"></div>

<div id="Cmd101"></div>

### Login

+ **URL**

    `POST v1/login`

+ **Request body:**

    ```
    {
        "username": "abc",
        "password": "abc"    
    }
    ```
    
    `password is generated by: sha256(myname + " + " + "druid" + " + " + mypassword + "heifeng")`
    `example: mypassword is : sha256("myname + druid + mypassword + heifeng") `
    `could use http://tool.oschina.net/encrypt?type=2 to generate password`

+ **Response:**

    **Code:** 200 Ok<br>

    **Header**<br>
    `X-Druid-Authentication-Token: <token-value>`

    **Response Body:** 

    ```
    {
        "id": "2342342342",
        "username": "abc",
        "email": "xx@xx.com",
        "phone": "xx",
        "address": "xx",
        "udpated_at":"",
        "role": "",
        "creator_id": "23423424234",
        "company_id": "23423424234",
        "company_name": "afwef"
    }
    ```
    
 [返回目录](#Cmd0)
    
 <div id="Cmd201"></div>   
    
 ### Update Password

+ **URL**

    `PUT v1/user/password`

+ **Request body:**  

```
old_password:"xxx"
password:"xxx"
```

  
+ **Response:**

    **Code:** 200 Ok<br>
    
    
  [返回目录](#Cmd0)
    
 <div id="Cmd202"></div>   
    
 ### Update User Info

+ **URL**

    `PUT v1/user/info`

+ **Request body:**

```
address:"12333333333333333333333333墨迹",
email:"ni484sha@163.com",
id_card:"511001198608121510",
phone:"13384765712"
```
  
+ **Response:**

    **Code:** 200 Ok<br>

 [返回目录](#Cmd0)

 <div id="Cmd203"></div>   
    
 ### Update Profile

+ **URL**

    `PUT v1/user/profile`

+ **Request body:**

**暂无**

  
+ **Response:**

    **Code:** 200 Ok<br>
    
  [返回目录](#Cmd0)   

 <div id="Cmd204"></div>   
    
 ### GET Myself

+ **URL**

    `GET  v1/user/myself`

+ **Request body:**

  
+ **Response:**

```
{
    "id": "59a76fff415bac49aa828ceb",
    "username": "doudou",
    "updated_at": "2018-03-19T06:24:47.611Z",
    "address": "12333333333333333333333333墨迹",
    "phone": "13384765712",
    "email": "ni484sha@163.com",
    "id_card": "511001198608121510",
    "role": "user",
    "creator_id": "580099199e190d6e5fdc2662",
    "company_id": "57b66fb6ec7e90884efbad92",
    "company_name": "成都德鲁伊科技有限公司",
    "description": "",
    "profile": {
        "page_size": 10,
        "time_zone": 0,
        "language": 1
    },
    "total_device": 16,
    "total_area": 4,
    "total_room": 1
}
```

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0)     
   

 <div id="Cmd301"></div>   
    
 ### Get Company

+ **URL**

    `GET v1/company/`

+ **Request body:**

  
+ **Response:**

```
{
    "id": "57b66fb6ec7e90884efbad92",
    "company_name": "成都德鲁伊科技有限公司",
    "phone": "111",
    "address": "1111111111111",
    "device_count": 55,
    "user_count": 52
}
```

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0) 
    
 
 <div id="Cmd401"></div>   
    
 ### List Devices

+ **URL**

    `GET v1/device/`

+ **Request body:**

  
+ **Response:**

```
[
    {
        "id": "582c046b21a43e671cd55e12",
        "updated_at": "2017-12-06T08:54:00.909Z",
        "uuid": "3e0033000651353334393332",
        "device_type": 1,
        "hardware_version": 2,
        "firmware_version": 0,
        "company_id": "57b66fb6ec7e90884efbad92",
        "company_name": "成都德鲁伊科技有限公司",
        "mark": 0,
        "battery_voltage": 0,
        "temperature": 23,
        "longitude": 22.8911513,
        "latitude": 22.6051139,
        "point_location": 0,
        "total_gps": 1,
        "total_behavior": 5,
        "total_behavior2": 0,
        "total_area": 0,
        "room_id": "",
        "room_name": "",
        "owner_id": "59a76fff415bac49aa828ceb",
        "owner_name": "doudou",
        "survive": 0,
        "activity": 1,
        "signal_strength": 13,
        "nickname": "禽流感",
        "birth_date": "2007-03-16T00:00:00Z",
        "gender": 0,
        "weight": 122,
        "height": 0,
        "bust": 0,
        "cannon": 0,
        "coat_color": "",
        "description": "",
        "images": [],
        "behavior": 0,
        "species": 4
    },
    {
        "id": "58ca3c319e62ccfcb6003ff1",
        "updated_at": "2017-03-16T08:02:22.482Z",
        "uuid": "3b005b000651353334393332",
        "device_type": 1,
        "hardware_version": 2,
        "firmware_version": 5,
        "company_id": "57b66fb6ec7e90884efbad92",
        "company_name": "成都德鲁伊科技有限公司",
        "mark": 2004,
        "battery_voltage": 3.84,
        "temperature": 0,
        "longitude": 0,
        "latitude": 0,
        "point_location": 0,
        "total_gps": 0,
        "total_behavior": 0,
        "total_behavior2": 0,
        "total_area": 1,
        "room_id": "",
        "room_name": "",
        "owner_id": "59a76fff415bac49aa828ceb",
        "owner_name": "doudou",
        "survive": 0,
        "activity": 0,
        "signal_strength": 0,
        "nickname": "44444444",
        "birth_date": "2018-03-05T03:14:18Z",
        "gender": 0,
        "weight": 222,
        "height": 0,
        "bust": 0,
        "cannon": 0,
        "coat_color": "",
        "description": "",
        "images": [
            "5aa0ce68a32d80091af93fcf",
            "5aa246d6a32d8048ee4e3af1"
        ],
        "behavior": 0,
        "species": 3
    },
    ...
]
```

    **Code:** 200 Ok<br>
 [返回目录](#Cmd0) 
 
  <div id="Cmd402"></div>   
    
 ### Get Device By UUID

+ **URL**

    `GET v1/device/uuid/:id`

+ **Request body:**

  
+ **Response:**

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0) 
   
 <div id="Cmd403"></div>   
    
 ### Get List Device By Specise

+ **URL**

    `GET v1/device/species/:id`

+ **Request body:**

  
+ **Response:**

```
{
    "id": "58ca3c319e62ccfcb6003ff1",
    "updated_at": "2017-03-16T08:02:22.482Z",
    "uuid": "3b005b000651353334393332",
    "device_type": 1,
    "hardware_version": 2,
    "firmware_version": 5,
    "company_id": "57b66fb6ec7e90884efbad92",
    "company_name": "成都德鲁伊科技有限公司",
    "biological_id": "5a7eb61ba32d805e3058c238",
    "biological_type": "cattle",
    "mark": 2004,
    "battery_voltage": 3.84,
    "temperature": 0,
    "longitude": 0,
    "latitude": 0,
    "point_location": 0,
    "total_gps": 0,
    "total_behavior": 0,
    "total_behavior2": 0,
    "total_area": 1,
    "room_id": "",
    "room_name": "",
    "owner_id": "59a76fff415bac49aa828ceb",
    "owner_name": "doudou",
    "survive": 0,
    "activity": 0,
    "signal_strength": 0
}
```

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0) 
   
   
  <div id="Cmd404"></div>   
    
 ### GET Device By ID

+ **URL**

    `GET  v1/device/id/:id`

+ **Request body:**

  
+ **Response:**

```
{
    "id": "58ca3c319e62ccfcb6003ff1",
    "updated_at": "2017-03-16T08:02:22.482Z",
    "uuid": "3b005b000651353334393332",
    "device_type": 1,
    "hardware_version": 2,
    "firmware_version": 5,
    "company_id": "57b66fb6ec7e90884efbad92",
    "company_name": "成都德鲁伊科技有限公司",
    "mark": 2004,
    "battery_voltage": 3.84,
    "temperature": 0,
    "longitude": 0,
    "latitude": 0,
    "point_location": 0,
    "total_gps": 0,
    "total_behavior": 0,
    "total_behavior2": 0,
    "total_area": 1,
    "room_id": "",
    "room_name": "",
    "owner_id": "59a76fff415bac49aa828ceb",
    "owner_name": "doudou",
    "survive": 0,
    "activity": 0,
    "signal_strength": 0,
    "nickname": "44444444",
    "birth_date": "2018-03-05T03:14:18Z",
    "gender": 0,
    "weight": 222,
    "height": 0,
    "bust": 0,
    "cannon": 0,
    "coat_color": "",
    "description": "",
    "images": [
        "5aa0ce68a32d80091af93fcf",
        "5aa246d6a32d8048ee4e3af1"
    ],
    "behavior": 0,
    "species": 3,
    "geo_fence": [
        {
            "id": "5aab2ef2a32d80047f46fc4e",
            "updated_at": "2018-03-16T02:45:18.443Z",
            "area_name": "啦啦",
            "company_id": "57b66fb6ec7e90884efbad92",
            "type": "Round",
            "distance": 69,
            "point": {
                "lng": 104.06374386620452,
                "lat": 30.547104990426924
            },
            "total_device": 1,
            "msg_type": 1,
            "owner_id": "59a76fff415bac49aa828ceb",
            "owner_name": "doudou"
        }
    ]
}
```

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0) 
   
  <div id="Cmd405"></div>   
    
 ### GET Area By DeivceID

+ **URL**

    `GET  v1/device/id/:id/area`

+ **Request body:**

  
+ **Response:**

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0) 

 <div id="Cmd406"></div>   
    
 ### Search Device By mark

+ **URL**

    `GET  v1/device/search/mark/:mark`

+ **Request body:**

  
+ **Response:**

```
[
    {
        "id": "5aab2ef2a32d80047f46fc4e",
        "updated_at": "2018-03-16T02:45:18.443Z",
        "area_name": "啦啦",
        "company_id": "57b66fb6ec7e90884efbad92",
        "type": "Round",
        "distance": 69,
        "point": {
            "lng": 104.06374386620452,
            "lat": 30.547104990426924
        },
        "total_device": 1,
        "msg_type": 1,
        "owner_id": "59a76fff415bac49aa828ceb",
        "owner_name": "doudou"
    }
]
```

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0) 

 <div id="Cmd407"></div>   
    
 ### Search Device By SN

+ **URL**

    `GET  v1/device/search/sn/:sn`

+ **Request body:**

  
+ **Response:**

```
[]
```
**Code:** 200 Ok<br>

   [返回目录](#Cmd0) 

<div id="Cmd408"></div>   
    
 ### GET Room Is idle

+ **URL**

    `GET  v1/device/idle/room`

+ **Request body:**

  
+ **Response:**
```
[
    {
        "id": "582c046b21a43e671cd55e12",
        "updated_at": "2017-12-06T08:54:00.909Z",
        "uuid": "3e0033000651353334393332",
        "device_type": 1,
        "hardware_version": 2,
        "firmware_version": 0,
        "company_id": "57b66fb6ec7e90884efbad92",
        "company_name": "成都德鲁伊科技有限公司",
        "mark": 0,
        "battery_voltage": 0,
        "temperature": 23,
        "longitude": 22.8911513,
        "latitude": 22.6051139,
        "point_location": 0,
        "total_gps": 1,
        "total_behavior": 5,
        "total_behavior2": 0,
        "total_area": 0,
        "room_id": "",
        "room_name": "",
        "owner_id": "59a76fff415bac49aa828ceb",
        "owner_name": "doudou",
        "survive": 0,
        "activity": 1,
        "signal_strength": 13,
        "nickname": "禽流感",
        "birth_date": "2007-03-16T00:00:00Z",
        "gender": 0,
        "weight": 122,
        "height": 0,
        "bust": 0,
        "cannon": 0,
        "coat_color": "",
        "description": "",
        "images": [],
        "behavior": 0,
        "species": 4
    },
    {
        "id": "58ca3c319e62ccfcb6003ff1",
        "updated_at": "2017-03-16T08:02:22.482Z",
        "uuid": "3b005b000651353334393332",
        "device_type": 1,
        "hardware_version": 2,
        "firmware_version": 5,
        "company_id": "57b66fb6ec7e90884efbad92",
        "company_name": "成都德鲁伊科技有限公司",
        "mark": 2004,
        "battery_voltage": 3.84,
        "temperature": 0,
        "longitude": 0,
        "latitude": 0,
        "point_location": 0,
        "total_gps": 0,
        "total_behavior": 0,
        "total_behavior2": 0,
        "total_area": 1,
        "room_id": "",
        "room_name": "",
        "owner_id": "59a76fff415bac49aa828ceb",
        "owner_name": "doudou",
        "survive": 0,
        "activity": 0,
        "signal_strength": 0,
        "nickname": "44444444",
        "birth_date": "2018-03-05T03:14:18Z",
        "gender": 0,
        "weight": 222,
        "height": 0,
        "bust": 0,
        "cannon": 0,
        "coat_color": "",
        "description": "",
        "images": [
            "5aa0ce68a32d80091af93fcf",
            "5aa246d6a32d8048ee4e3af1"
        ],
        "behavior": 0,
        "species": 3
    },
    ...
]
```

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0) 


<div id="Cmd409"></div>   
    
### List Many Devices By id

+ **URL**

    `POST  v1/device/many`

+ **Request body:**

```
{
	"id":[
		"582c046b21a43e671cd55e12"
	]
}
```
  
+ **Response:**
```
[
    {
        "id": "582c046b21a43e671cd55e12",
        "updated_at": "2017-12-06T08:54:00.909Z",
        "uuid": "3e0033000651353334393332",
        "device_type": 1,
        "hardware_version": 2,
        "firmware_version": 0,
        "company_id": "57b66fb6ec7e90884efbad92",
        "company_name": "成都德鲁伊科技有限公司",
        "mark": 0,
        "battery_voltage": 0,
        "temperature": 23,
        "longitude": 22.8911513,
        "latitude": 22.6051139,
        "point_location": 0,
        "total_gps": 1,
        "total_behavior": 5,
        "total_behavior2": 0,
        "total_area": 0,
        "room_id": "",
        "room_name": "",
        "owner_id": "59a76fff415bac49aa828ceb",
        "owner_name": "doudou",
        "survive": 0,
        "activity": 1,
        "signal_strength": 13,
        "nickname": "禽流感",
        "birth_date": "2007-03-16T00:00:00Z",
        "gender": 0,
        "weight": 122,
        "height": 0,
        "bust": 0,
        "cannon": 0,
        "coat_color": "",
        "description": "",
        "images": [],
        "behavior": 0,
        "species": 4
    }
]
```

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0) 

<div id="Cmd410"></div>   
    
### Regsier

+ **URL**

    `POST  v1/device/register`

+ **Request body:**

```
{
        "uuid": "3e0033000651353334393332",
        "device_type": 1,
        "hardware_version": 2,
        "firmware_version": 0,
        "company_id": "57b66fb6ec7e90884efbad92",
        "company_name": "成都德鲁伊科技有限公司",
        "mark": 0,
        "battery_voltage": 0,
        "temperature": 23,
        "longitude": 22.8911513,
        "latitude": 22.6051139,
        "point_location": 0,
        "total_gps": 1,
        "total_behavior": 5,
        "total_behavior2": 0,
        "total_area": 0,
        "room_id": "",
        "room_name": "",
        "owner_id": "59a76fff415bac49aa828ceb",
        "owner_name": "doudou",
        "survive": 0,
        "activity": 1,
        "signal_strength": 13,
        "nickname": "禽流感",
        "birth_date": "2007-03-16T00:00:00Z",
        "gender": 0,
        "weight": 122,
        "height": 0,
        "bust": 0,
        "cannon": 0,
        "coat_color": "",
        "description": "",
        "images": [],
        "behavior": 0,
        "species": 4
    }

```

  
+ **Response:**

    **Code:** 201 Ok<br>
   [返回目录](#Cmd0) 
   
   
<div id="Cmd411"></div>   
    
### List Devices By Exclude

+ **URL**

    `POST  v1/device/exclude`

+ **Request body:**
```
{
    "id":[
    "5881e103f01fce1212f038a4",
    "58ca3c319e62ccfcb6003ff1"
    ]
}
```
  
+ **Response:**

```
[
    {
        "id": "5881e103f01fce1212f038a4",
        "updated_at": "2017-04-13T09:03:12.524Z",
        "uuid": "3d001f000651353334393332",
        "device_type": 1,
        "hardware_version": 2,
        "firmware_version": 0,
        "company_id": "57b66fb6ec7e90884efbad92",
        "company_name": "成都德鲁伊科技有限公司",
        "mark": 2003,
        "battery_voltage": 3.975,
        "temperature": 0,
        "longitude": -111.7691008,
        "latitude": 33.2608,
        "point_location": 0,
        "total_gps": 51,
        "total_behavior": 1,
        "total_behavior2": 0,
        "total_area": 0,
        "room_id": "",
        "room_name": "",
        "owner_id": "59a76fff415bac49aa828ceb",
        "owner_name": "doudou",
        "survive": 0,
        "activity": 0,
        "signal_strength": 0,
        "nickname": "",
        "gender": 0,
        "weight": 0,
        "height": 0,
        "bust": 0,
        "cannon": 0,
        "coat_color": "",
        "description": "",
        "images": [
            "5aab2ddca32d80047f46fc41",
            "5aab2ddea32d80047f46fc45",
            "5aab2ddfa32d80047f46fc49"
        ],
        "behavior": 0,
        "species": 0
    },
    {
        "id": "58ca3c319e62ccfcb6003ff1",
        "updated_at": "2017-03-16T08:02:22.482Z",
        "uuid": "3b005b000651353334393332",
        "device_type": 1,
        "hardware_version": 2,
        "firmware_version": 5,
        "company_id": "57b66fb6ec7e90884efbad92",
        "company_name": "成都德鲁伊科技有限公司",
        "mark": 2004,
        "battery_voltage": 3.84,
        "temperature": 0,
        "longitude": 0,
        "latitude": 0,
        "point_location": 0,
        "total_gps": 0,
        "total_behavior": 0,
        "total_behavior2": 0,
        "total_area": 1,
        "room_id": "",
        "room_name": "",
        "owner_id": "59a76fff415bac49aa828ceb",
        "owner_name": "doudou",
        "survive": 0,
        "activity": 0,
        "signal_strength": 0,
        "nickname": "44444444",
        "birth_date": "2018-03-05T03:14:18Z",
        "gender": 0,
        "weight": 222,
        "height": 0,
        "bust": 0,
        "cannon": 0,
        "coat_color": "",
        "description": "",
        "images": [
            "5aa0ce68a32d80091af93fcf",
            "5aa246d6a32d8048ee4e3af1"
        ],
        "behavior": 0,
        "species": 3
    }
]
```

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0) 
   
   
<div id="Cmd412"></div>   

    
### Put Device

+ **URL**

    `PUT  v1/device/id/:id`

+ **Request body:**

```
{
        "uuid": "3d001f000651353334393332",
        "device_type": 1,
        "hardware_version": 2,
        "firmware_version": 0,
        "company_id": "57b66fb6ec7e90884efbad92",
        "company_name": "成都德鲁伊科技有限公司",
        "mark": 2003,
        "battery_voltage": 3.975,
        "temperature": 0,
        "longitude": -111.7691008,
        "latitude": 33.2608,
        "point_location": 0,
        "total_gps": 51,
        "total_behavior": 1,
        "total_behavior2": 0,
        "total_area": 0,
        "room_id": "",
        "room_name": "",
        "owner_id": "59a76fff415bac49aa828ceb",
        "owner_name": "doudou",
        "survive": 0,
        "activity": 0,
        "signal_strength": 0,
        "nickname": "",
        "gender": 0,
        "weight": 0,
        "height": 0,
        "bust": 0,
        "cannon": 0,
        "coat_color": "嘿嘿嘿",
        "description": "",
        "images": [
            "5aab2ddca32d80047f46fc41",
            "5aab2ddea32d80047f46fc45",
            "5aab2ddfa32d80047f46fc49"
        ],
        "behavior": 0,
        "species": 0
    }
  
  ```
+ **Response:**

    **Code:** 201 Created<br>
   [返回目录](#Cmd0) 

<div id="Cmd501"></div>   
    
### Get Device GPS

+ **URL**

    `GET v1/gps/device/:id`

+ **Request body:**

  
+ **Response:**

```
[
    {
        "id": "5aa113cdb1abed6a33533123",
        "device_id": "5a9d1515699f4f978694a9e0",
        "uuid": "220056001851353239333631",
        "firmware_version": 19,
        "updated_at": "2018-03-01T04:09:00Z",
        "timestamp": "2018-03-01T04:09:00Z",
        "mark": 1,
        "sms": 1,
        "longitude": 104.3607026250158,
        "latitude": 30.749013633549758,
        "relative_altitude": 0,
        "ground_altitude": 0,
        "altitude": 0,
        "temperature": 21.1139360550816,
        "humidity": 0,
        "light": 1874,
        "pressure": 0,
        "used_star": 0,
        "view_star": 0,
        "dimension": 0,
        "speed": 0,
        "horizontal": 0,
        "vertical": 0,
        "course": 0,
        "battery_voltage": 3.942,
        "signal_strength": 19,
        "point_location": 0,
        "fix_time": 0
    },
    {
        "id": "5aa113cdb1abed6a33533121",
        "device_id": "5a9d1515699f4f978694a9e0",
        "uuid": "220056001851353239333631",
        "firmware_version": 19,
        "updated_at": "2018-03-01T05:08:16Z",
        "timestamp": "2018-03-01T05:08:16Z",
        "mark": 1,
        "sms": 1,
        "longitude": 104.3609111671534,
        "latitude": 30.749001003187992,
        "relative_altitude": 0,
        "ground_altitude": 0,
        "altitude": 0,
        "temperature": 19.982460278003458,
        "humidity": 0,
        "light": 2074,
        "pressure": 0,
        "used_star": 0,
        "view_star": 0,
        "dimension": 0,
        "speed": 0,
        "horizontal": 0,
        "vertical": 0,
        "course": 0,
        "battery_voltage": 3.942,
        "signal_strength": 19,
        "point_location": 0,
        "fix_time": 0
    },
    ···
]
```

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0) 

<div id="Cmd601"></div>   
    
### Get Device Behavior


+ **URL**

    `GET v1/behavior/device/:id`

+ **Request body:**

  
+ **Response:**

```
[
    {
        "id": "5a279c08f5000cb6a236cf38",
        "device_id": "582c046b21a43e671cd55e12",
        "uuid": "3e0033000651353334393332",
        "firmware_version": 0,
        "timestamp": "2017-12-05T10:40:10Z",
        "updated_at": "2017-12-06T07:28:07.863Z",
        "sleep_time": 11111,
        "other_time": 0,
        "activity_time": 0,
        "fly_time": 0,
        "peck_time": 0,
        "crawl_time": 0,
        "run_time": 0,
        "total_expend": 666666,
        "sleep_expend": 100,
        "other_expend": 0,
        "activity_expend": 0,
        "fly_expend": 0,
        "peck_expend": 0,
        "crawl_expend": 0,
        "run_expend": 0
    },
    {
        "id": "5a279c37f5000cb6a236cfe6",
        "device_id": "582c046b21a43e671cd55e12",
        "uuid": "3e0033000651353334393332",
        "firmware_version": 0,
        "timestamp": "2017-12-05T10:50:10Z",
        "updated_at": "2017-12-06T07:28:55.379Z",
        "sleep_time": 11111,
        "other_time": 0,
        "activity_time": 0,
        "fly_time": 0,
        "peck_time": 0,
        "crawl_time": 0,
        "run_time": 0,
        "total_expend": 666666,
        "sleep_expend": 100,
        "other_expend": 0,
        "activity_expend": 0,
        "fly_expend": 0,
        "peck_expend": 0,
        "crawl_expend": 0,
        "run_expend": 0
    }
```


    **Code:** 200 Ok<br>
   [返回目录](#Cmd0) 
   
<div id="Cmd602"></div>   
    
### Get Behavior analyze

**行为分析**

+ **URL**

    `POST v1/behavior/analyze/`

+ **Request body:**

    ```
    {
	"device_ids":[
		"xxxxxxxxxxxxx",
		"xxxxxxxxxxxxx"
		],    
	"room_id":"11231231231231",
	"time_start":"2014-11-12T11:45:26.371Z",
	"time_end":"2014-11-12T11:45:26.371Z",
	"survive":6,
	"time_cell":3,
	"species": -1
	
	
        
    }
    ```
**注意说明：**
* device_ids为指定设备，填写后room_id,survive字段无效
*  room_id为分组号 
*  survive为奶牛的持续状态值,目前只有发情状态和其它状态（另有状态等待大数据部确认。）6为发情，正常则不填,或者填0
*  可以不填任何值，则默认返回该用户所有数据，时间为1天。
*  species 为物种请求筛选， -1 为请求全部物种， 0 为其它物种


+ **Response:**

    **Code:** 200 OK <br>
    
+ **Response body:** 

```
[
    [
        {
            "device_id": "596e00a20059ea0a31a50013",
            "company_id": "57b66fb6ec7e90884efbad92",
            "company_name": "成都德鲁伊科技有限公司",
            "mark": 2033,
            "nickname": "Edei",
            "time_num": 1518051600,
            "motion_eat_num": 0,
            "motion_rumination_num": 0,
            "motion_other_low_num": 30,
            "motion_other_medium_num": 0,
            "motion_other_hight_num": 180
        },
        {
            "device_id": "596e00a20059ea0a31a50013",
            "company_id": "57b66fb6ec7e90884efbad92",
            "company_name": "成都德鲁伊科技有限公司",
            "mark": 2033,
            "nickname": "Edei",
            "time_num": 1518062400,
            "motion_eat_num": 0,
            "motion_rumination_num": 0,
            "motion_other_low_num": 80,
            "motion_other_medium_num": 0,
            "motion_other_hight_num": 180
        }
        
    ],
    [
        { 
            "device_id": "xxxxxx",
            "company_id": "xxxxxx",
            "company_name": "成都德鲁伊科技有限公司",
            "mark": 2034,
            "nickname": "xxxxxxxx",
            "time_num": 1518062400,
            "motion_eat_num": 0,
            "motion_rumination_num": 0,
            "motion_other_low_num": 80,
            "motion_other_medium_num": 0,
            "motion_other_hight_num": 180
            
        },
    ]
]
```

**注意说明：**
*  返回格式为二维数组[设备][时间]数据
*  返回的数据是排好序的，时间单位为1小时，如果数据分析展示时间间隔为3小时请将3个连续数据相加。1天则24个数据相加，以此类推
*  motion_eat_num为1个小时吃的个数，motion_rumination_num为1个小时反刍的个数。则百分比数据为 motion_eat_num/(motion_rumination_num + motion_eat_num + motion_other_low_num + motion_other_medium_num + motion_other_hight_num) * 100 %
 [返回目录](#Cmd0) 
 
 
<div id="Cmd701"></div>   
    
 ### Get ODBA Analyze Data

+ **URL**

    `POST v1/odba/analyze/`

+ **Request body:**

 ```
    {
	"device_ids":[
		"xxxxxxxxxxxxx",
		"xxxxxxxxxxxxx"
		],    
	"room_id":"11231231231231",
	"time_start":"2014-11-12T11:45:26.371Z",
	"time_end":"2014-11-12T11:45:26.371Z",
	"survive":6,
	"time_cell":3,
	"species": -1     
    }
```


  
+ **Response:**

```
[
    [ 
        {
            "device_id": "596e00a20059ea0a31a50013",
            "mark": 2033,
            "owner_id": "",
            "nickname": "Edei",
            "time_num": 1518019200,
            "act_valid_num": 180,
            "act_total_num": 180,
            "survive": 0
        },
        {
            "device_id": "596e00a20059ea0a31a50013",
            "mark": 2033,
            "owner_id": "",
            "nickname": "Edei",
            "time_num": 1518030000,
            "act_valid_num": 180,
            "act_total_num": 180,
            "survive": 0
        }       
    ],
    [
        {             
        },
    ]
]
```


    **Code:** 200 Ok<br>
   [返回目录](#Cmd0) 
 

<div id="Cmd702"></div>   
    
 ### Get Loving Analyze Data

+ **URL**

    `POST v1/odba/loving/`

+ **Request body:**

 ```
    {
	"device_ids":[
		"xxxxxxxxxxxxx",
		"xxxxxxxxxxxxx"
		],    
	"room_id":"11231231231231",
	"time_start":"2014-11-12T11:45:26.371Z",
	"time_end":"2014-11-12T11:45:26.371Z",
	"survive":6,
	"time_cell":3,
	"species": -1     
    }
```

  
+ **Response:**


```
[
    [
        {
            "id": "5a927e07020ef5cc5cbabb6b",
            "device_id": "596e00a20059ea0a31a50013",
            "mark": 2033,
            "owner_id": "59a76fff415bac49aa828ceb",
            "nickname": "代表作品有",
            "updated_at": "2018-02-25T09:12:39.227Z",
            "time_num": 1518051600,
            "time_keep": 21600
        }
    ]
]

```

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0) 

<div id="Cmd801"></div>   
    
 ### Search Device

+ **URL**

    `POST v1/search/device/`

+ **Request body:**

**暂无**

  
+ **Response:**

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0) 

<div id="Cmd901"></div>   
    
 ### Create area

+ **URL**

    `POST v1/ditu/`

+ **Request body:**

```
{
    "id": "5aaf775ca32d80047f46fc62",
    "updated_at": "2018-03-19T08:39:56.436077917Z",
    "area_name": "2222",
    "company_id": "5a9cf616020ef5cc5ce7cd61",
    "type": "Round",
    "distance": 55,
    "point": {
        "lng": 104.36093623215488,
        "lat": 30.748972942654728
    },
    "total_device": 0,
    "msg_type": 2,
    "owner_id": "5a9cf689a32d8005d22e52d6",
    "owner_name": "h3c"
}
```

  
+ **Response:**

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0)  


<div id="Cmd902"></div>   
    
 ### Add Device to Area

+ **URL**

    `PUT v1/ditu/area/:id/adddevice/:device`
    
```
https://cattle.coolhei.com/api/v1/ditu/area/5aaf775ca32d80047f46fc62/adddevice/5a9d1515699f4f978694a9e0
```

+ **Request body:**

  
+ **Response:**

    **Code:** 201 Created<br>
   [返回目录](#Cmd0)  
   
<div id="Cmd903"></div>   
    
 ### Delete Device to Area

+ **URL**

    `PUT v1/ditu/area/:id/deldevice/:device`

```
https://cattle.coolhei.com/api/v1/ditu/area/5aaf775ca32d80047f46fc62/deldevice/5a9d1515699f4f978694a9e0
```

+ **Request body:**

  
+ **Response:**

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0)  
   
<div id="Cmd904"></div>   
    
 ### Update Area Info

+ **URL**

    `PUT v1/ditu/area/:id`

+ **Request body:**

```
{
	"area_name":"2222",
	"distance":55,
    "msg_type":2,
    "point": {
    	"lat":30.748972942654728,
    	"lng":104.36093623215488
    },
	"type":"Round"
}
```
  
+ **Response:**

```
{
    "id": "5aaf775ca32d80047f46fc62",
    "updated_at": "2018-03-19T08:39:56.436077917Z",
    "area_name": "2222",
    "company_id": "5a9cf616020ef5cc5ce7cd61",
    "type": "Round",
    "distance": 55,
    "point": {
        "lng": 104.36093623215488,
        "lat": 30.748972942654728
    },
    "total_device": 0,
    "msg_type": 2,
    "owner_id": "5a9cf689a32d8005d22e52d6",
    "owner_name": "h3c"
}
```
    **Code:** 200 Ok<br>
   [返回目录](#Cmd0)  
   
  
<div id="Cmd905"></div>   
    
 ### Delete Area

+ **URL**

    `DELETE v1/ditu/area/:id`

+ **Request body:**

  
+ **Response:**

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0)  
 
 <div id="Cmd906"></div>   
    
 ### Get Device in area

+ **URL**

    `GET v1/ditu/area/:id/device`

+ **Request body:**

  
+ **Response:**

```
暂无
```

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0)  

<div id="Cmd907"></div>   
    
 ### List Area

+ **URL**

    `GET v1/ditu/area`

+ **Request body:**

  
+ **Response:**

```
[
    {
        "id": "5aa0b4d3a32d80091af93fb1",
        "updated_at": "2018-03-14T07:52:36.662Z",
        "area_name": "A-08",
        "company_id": "5a9cf616020ef5cc5ce7cd61",
        "type": "Round",
        "distance": 103,
        "point": {
            "lng": 104.36082346683563,
            "lat": 30.749216981030617
        },
        "total_device": 0,
        "devices_id": [
            "5a9d1515699f4f978694a9e0",
            "5a9e108d9c2660a40a404228"
        ],
        "msg_type": 2,
        "owner_id": "5a9cf689a32d8005d22e52d6",
        "owner_name": "h3c"
    },
    {
        "id": "5aa0d7d6a32d80091af93fe9",
        "updated_at": "2018-03-08T07:01:37.491Z",
        "area_name": "A-02",
        "company_id": "5a9cf616020ef5cc5ce7cd61",
        "type": "Round",
        "distance": 51,
        "point": {
            "lng": 104.36149197756605,
            "lat": 30.749957653816526
        },
        "total_device": 2,
        "devices_id": [
            "5a9e108d9c2660a40a404228",
            "5a9d1515699f4f978694a9e0"
        ],
        "msg_type": 1,
        "owner_id": "5a9cf689a32d8005d22e52d6",
        "owner_name": "h3c"
    }
```


    **Code:** 200 Ok<br>
   [返回目录](#Cmd0)  

<div id="Cmd908"></div>   
    
 ### Get Area info with ID

+ **URL**

    `GET v1/ditu/area/:id`

+ **Request body:**

  
+ **Response:**

```
{
    "id": "5aa0b4d3a32d80091af93fb1",
    "updated_at": "2018-03-14T07:52:36.662Z",
    "area_name": "A-08",
    "company_id": "5a9cf616020ef5cc5ce7cd61",
    "type": "Round",
    "distance": 103,
    "point": {
        "lng": 104.36082346683563,
        "lat": 30.749216981030617
    },
    "total_device": 2,
    "devices_id": [
        "5a9d1515699f4f978694a9e0",
        "5a9e108d9c2660a40a404228"
    ],
    "msg_type": 2,
    "owner_id": "5a9cf689a32d8005d22e52d6",
    "owner_name": "h3c"
}

```


    **Code:** 200 Ok<br>
   [返回目录](#Cmd0)  


<div id="Cmd1001"></div>   
    
 ### Create Room

+ **URL**

    `POST v1/room/`

+ **Request body:**

```
{
	"description":"我是分组2",
	"room_name":"分组2"
}
```
  
+ **Response:**

```
{
    "id": "5aaf7daba32d80047f46fc64",
    "user_id": "5a9cf689a32d8005d22e52d6",
    "description": "我是分组2",
    "room_name": "分组2",
    "device_count": 0
}
```

    **Code:** 201 Created<br>
   [返回目录](#Cmd0)  
   
<div id="Cmd1002"></div>   
    
 ### Add Device to Room

+ **URL**

    `PUT v1/room/id/:id/adddevice`

+ **Request body:**

```
{
   "id":[
        "5a9e108d9c2660a40a404228"
   ] 
}
```
  
+ **Response:**

    **Code:** 201 Created<br>
   [返回目录](#Cmd0)  

<div id="Cmd1003"></div>   
    
 ### Delete Device to Room

+ **URL**

    `PUT v1/room/id/:id/deldevice`

+ **Request body:**

  
+ **Response:**

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0)  

<div id="Cmd1004"></div>   
    
 ### Update room Info

+ **URL**

    `PUT v1/room/id/:id`

+ **Request body:**

```
{
	"description":"我是分组3",
	"room_name":"分组3"
}
```
  
+ **Response:**

```
{
    "id": "5aaf7daba32d80047f46fc64",
    "user_id": "5a9cf689a32d8005d22e52d6",
    "description": "我是分组3",
    "room_name": "分组3",
    "device_count": 1
}
```

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0)  
   
<div id="Cmd1005"></div>   
    
 ### Delete Room

+ **URL**

    `DELETE v1/room/id/:id`

+ **Request body:**

  
+ **Response:**

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0)  
   
   
<div id="Cmd1006"></div>   
    
 ### List Room

+ **URL**

    `GET v1/room/`

+ **Request body:**

  
+ **Response:**

```
[
    {
        "id": "5aa0b154a32d80091af93fb0",
        "updated_at": "2018-03-15T02:10:08.536Z",
        "user_id": "5a9cf689a32d8005d22e52d6",
        "room_name": "A-01",
        "device_count": 2
    },
    {
        "id": "5aaf7daba32d80047f46fc64",
        "updated_at": "2018-03-19T09:11:00.256Z",
        "user_id": "5a9cf689a32d8005d22e52d6",
        "description": "我是分组2",
        "room_name": "分组2",
        "device_count": 1
    }
]
```
 **Code:** 200 Ok<br>
   [返回目录](#Cmd0)  

   
<div id="Cmd1007"></div>   
    
 ### Get Room info

+ **URL**

    `GET v1/room/id/:id/info`

+ **Request body:**

  
+ **Response:**

```
{
    "id": "5aa0b154a32d80091af93fb0",
    "updated_at": "2018-03-15T02:10:08.536Z",
    "user_id": "5a9cf689a32d8005d22e52d6",
    "room_name": "A-01",
    "device_count": 2
}
```

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0)  

<div id="Cmd1008"></div>   
    
 ### Get Device in Room

+ **URL**

    `GET v1/room/id/:id/device`

+ **Request body:**

  
+ **Response:**


    **Code:** 200 Ok<br>
   [返回目录](#Cmd0)  

<div id="Cmd1101"></div>   
    
 ### Delete Image by biological

+ **URL**

    `DELETE v1/biological/cattle/id/:id/image/:image_id`

+ **Request body:**

  
+ **Response:**

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0)  
   
 <div id="Cmd1102"></div>   
    
 ### Delete Image By device

+ **URL**

    `DELETE v1/biological/cattle/device/:id/image/:image_id`

+ **Request body:**

  
+ **Response:**

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0)  

<div id="Cmd1103"></div>   
    
 ### Get Cattle By DeviceId

+ **URL**

    `GET v1/biological/cattle/device/:id`

+ **Request body:**

  
+ **Response:**

```
{
    "id": "5a9e0e3918184d29920cde49",
    "updated_at": "2018-03-19T09:25:37.515Z",
    "device_id": "5a9d1515699f4f978694a9e0",
    "owner_id": "5a9cf689a32d8005d22e52d6",
    "owner_name": "hc3",
    "mark": 1,
    "nickname": "奶牛1号",
    "birth_date": "2017-03-02T02:58:01.072Z",
    "gender": 0,
    "weight": 320,
    "height": 0,
    "bust": 0,
    "cannon": 0,
    "coat_color": "",
    "description": "",
    "images": [
        "5aaf2a3fa32d80047f46fc54"
    ],
    "behavior": 0,
    "species": 4
}
```

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0)  

<div id="Cmd1104"></div>   
    
 ### Update Cattle Info By DeviceID

+ **URL**

    `PUT v1/biological/cattle/device/:id`

+ **Request body:**

```
{
    "device_id": "5a9d1515699f4f978694a9e0",
    "owner_id": "5a9cf689a32d8005d22e52d6",
    "owner_name": "hc3",
    "mark": 1,
    "nickname": "奶牛2号",
    "birth_date": "2017-03-02T02:58:01.072Z",
    "gender": 0,
    "weight": 320,
    "height": 0,
    "bust": 0,
    "cannon": 0,
    "coat_color": "",
    "description": "",
    "images": [
        "5aaf2a3fa32d80047f46fc54"
    ],
    "behavior": 0,
    "species": 4
}

```

  
+ **Response:**


```
{
    "id": "5a9e0e3918184d29920cde49",
    "updated_at": "2018-03-19T09:25:37.515Z",
    "device_id": "5a9d1515699f4f978694a9e0",
    "owner_id": "5a9cf689a32d8005d22e52d6",
    "owner_name": "hc3",
    "mark": 1,
    "nickname": "奶牛2号",
    "birth_date": "2017-03-02T02:58:01.072Z",
    "gender": 0,
    "weight": 320,
    "height": 0,
    "bust": 0,
    "cannon": 0,
    "coat_color": "",
    "description": "",
    "images": [
        "5aaf2a3fa32d80047f46fc54"
    ],
    "behavior": 0,
    "species": 4
}

```

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0)  

<div id="Cmd1105"></div>   
    
 ### Update image by Device

+ **URL**

    `PUT v1/device/cattle/device/:id/image/:name`

```
https://cattle.coolhei.com/api/v1/biological/cattle/device/5a9d1515699f4f978694a9e0/image/http2.jpg
```

+ **Request body:**

  
+ **Response:**

    **Code:** 201 Created<br>
   [返回目录](#Cmd0)  


<div id="Cmd1106"></div>   
    
 ### Update iamge by Device(no name)

+ **URL**

    `PUT v1/biological/cattle/device/:id/image`

+ **Request body:**

  
+ **Response:**

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0)  

<div id="Cmd1201"></div>   
    
 ### Get iamge

+ **URL**

    `GET v1/file/device/:id/image/:image_id`

+ **Request body:**

  
+ **Response:**

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0)  
   
   
<div id="Cmd1202"></div>   
    
 ### Get thumbnail Image

+ **URL**

    `GET v1/file/device/:id/image/:image_id/thumbnail`

+ **Request body:**

  
+ **Response:**

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0)  

<div id="Cmd1301"></div>   
    
 ### Get Token

+ **URL**

    `GET v1/weixin/token`

+ **Request body:**

**暂无**

  
+ **Response:**

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0) 
   
   
<div id="Cmd1302"></div>   
    
 ### Get signature

+ **URL**

    `GET v1/weixin/signature`

+ **Request body:**

**暂无**
  
+ **Response:**

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0) 

<div id="Cmd1401"></div>   
    
 ### Get Unread Message

+ **URL**

    `GET v1/message/unread`

+ **Request body:**

  
+ **Response:**
```
[
    {
        "id": "5a705bb9020ef5cc5c2bfcc5",
        "dst": "59a76fff415bac49aa828ceb",
        "type": 5,
        "level": 1,
        "target": [
            "5a6705d1a32d80461763112b"
        ],
        "target_str": [
            "龟龟"
        ],
        "msg": "生物(设备: 2030)活动微弱!",
        "msg_cn": "生物(设备: 2030)活动微弱!",
        "msg_type": 1,
        "src": "5965c1ee0059ea0a31896cea",
        "src_name": "2030",
        "timestamp": "2018-01-30T11:49:13.048Z",
        "readed_at": "2018-02-01T02:05:08.1Z"
    }
]
```
    **Code:** 200 Ok<br>
   [返回目录](#Cmd0) 


<div id="Cmd1402"></div>   
    
 ### List message

+ **URL**

    `GET v1/message/`

+ **Request body:**

  
+ **Response:**

```
[
 {
        "id": "5a28d9e9f5000cb6a239e753",
        "dst": "59a76fff415bac49aa828ceb",
        "type": 5,
        "level": 1,
        "target": null,
        "target_str": null,
        "msg": "生物(设备: 2033)活动剧烈! ",
        "msg_cn": "生物(设备: 2033)活动剧烈! ",
        "msg_type": 2,
        "src": "596e00a20059ea0a31a50013",
        "src_name": "2033",
        "timestamp": "2017-12-07T06:04:25.765Z",
        "readed_at": "2017-12-07T09:51:45.199Z"
    },
    {
        "id": "5a27c870f5000cb6a2375df8",
        "dst": "59a76fff415bac49aa828ceb",
        "type": 6,
        "level": 1,
        "target": [
            "5a27ae95a32d8008adb191b9"
        ],
        "target_str": [
            "2065入"
        ],
        "msg": "Device: 2065 Entered GeoFence: 2065入!",
        "msg_cn": "设备: 2065 进入围栏:2065入 !",
        "msg_type": 1,
        "src": "598aab570059ea0a3108b460",
        "src_name": "2065",
        "timestamp": "2017-12-06T10:37:36.175Z",
        "readed_at": "2017-12-07T02:00:47.853Z"
    }
]
```
    **Code:** 200 Ok<br>
   [返回目录](#Cmd0) 

<div id="Cmd1403"></div>   
    
 ### List message by type

+ **URL**

    `GET v1/message/type/:type`


+ **Request body:**

  
+ **Response:**

```
[ 
    {
        "id": "5a28de9ef5000cb6a239f801",
        "dst": "59a76fff415bac49aa828ceb",
        "type": 5,
        "level": 1,
        "target": null,
        "target_str": null,
        "msg": "生物(设备: 2033)活动剧烈! ",
        "msg_cn": "生物(设备: 2033)活动剧烈! ",
        "msg_type": 2,
        "src": "596e00a20059ea0a31a50013",
        "src_name": "2033",
        "timestamp": "2017-12-07T06:24:30.698Z",
        "readed_at": "2017-12-07T09:51:45.199Z"
    },
    {
        "id": "5a28de5ff5000cb6a239f6e9",
        "dst": "59a76fff415bac49aa828ceb",
        "type": 5,
        "level": 1,
        "target": null,
        "target_str": null,
        "msg": "生物(设备: 2068)活动剧烈! ",
        "msg_cn": "生物(设备: 2068)活动剧烈! ",
        "msg_type": 2,
        "src": "598d8b120059ea0a3127da71",
        "src_name": "2068",
        "timestamp": "2017-12-07T06:23:27.93Z",
        "readed_at": "2017-12-07T09:51:45.199Z"
    },
    {
        "id": "5a28dd2af5000cb6a239f26a",
        "dst": "59a76fff415bac49aa828ceb",
        "type": 5,
        "level": 1,
        "target": null,
        "target_str": null,
        "msg": "生物(设备: 2068)活动剧烈! ",
        "msg_cn": "生物(设备: 2068)活动剧烈! ",
        "msg_type": 2,
        "src": "598d8b120059ea0a3127da71",
        "src_name": "2068",
        "timestamp": "2017-12-07T06:18:18.234Z",
        "readed_at": "2017-12-07T09:51:45.199Z"
    }
]
```

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0) 

<div id="Cmd1404"></div>   
    
 ### PUT message Read

+ **URL**

    `PUT v1/message/`

+ **Request body:**

```
{
        "dst": "59a76fff415bac49aa828ceb",
        "type": 5,
        "level": 1,
        "target": null,
        "target_str": null,
        "msg": "生物(设备: 2068)活动剧烈! ",
        "msg_cn": "生物(设备: 2068)活动剧烈! ",
        "msg_type": 2,
        "src": "598d8b120059ea0a3127da71",
        "src_name": "2068"
}
```
  
+ **Response:**

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0) 

<div id="Cmd1405"></div>   
    
 ### Delete message

+ **URL**

    `DELETE v1/message/id/:id`

+ **Request body:**

  
+ **Response:**

    **Code:** 200 Ok<br>
   [返回目录](#Cmd0) 

<div id="Cmd1406"></div>   
    
 ### Deltet Many message

+ **URL**

    `PUT v1/message/delete`

+ **Request body:**

```
{
    id:[
        "5a705bb9020ef5cc5c2bfcc5",
        "5a705b6d020ef5cc5c2bf765"
    ]
}
```

  
+ **Response:**

    **Code:** 204 Ok<br>
   [返回目录](#Cmd0) 

