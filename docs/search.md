# Druid Search Server REST API
This document is REST API for druid search.

## Search GPS:

+ **URL**

`POST /v1/search/gps`

+ **Request body:**

```
{
    "device_id": "4234234234",
    "uuid": "4234234234",
    "firmware_version": [1],
    "timestamp": ["2016-04-09T23:05:15.163126Z", "2016-04-09T23:05:15.163126Z"],
    "longitude": [11, 22],
    "latitude": [11, 22],
    "altitude": [11, 22],
    "temperature": [11, 22],
    "humidity": [11, 22],
    "light": [11, 22],
    "pressure": [11, 22],
    "used_star": [11, 22],
    "view_star": [11, 22],
    "dimension": [11, 22],
    "speed": [11, 22],
    "horizontal": [11, 22],
    "vertical": [11, 22],
    "course": [11, 22],
    "battery_voltage": [11, 22],
    "signal_strength": [11, 22],
}
```
`firmware version [1] is version=1, light [11, 22] is 11<=light<=22`
`timestamp ["2016-04-09T23:05:15.163126Z"] is begin to now`

+ **Response:**

    **Code:** 200 OK<br>

    **Response body:**

```
[
]
```

## Search Device:

+ **URL**

`POST /v1/search/device`

+ **Request body:**

```
{
    "updated_at": ["2016-04-09T23:05:15.163126Z"],
    "uuid": "xxx",
    "device_type": [1],
    "hardware_version": [2],
    "firmware_version": [4],
    "company_name": "abc",
    "biological_type": "xxx",
    "imsi": "232",
    "sim_number": "1231",
    "mac": "axafew",
    "description": "awfwef"
}

```
`company_name "abc" match ***abc***`

+ **Response:**

    **Code:** 200 OK<br>

    **Response body:**

```
[
]
```

## Search Sim:

+ **URL**

`POST /v1/search/sim`

+ **Request body:**

```
{
    "imsi": "wefwefw"
}
```

+ **Response:**

    **Code:** 200 OK<br>

    **Response body:**

```
[
]
```

## Search Behivior:

+ **URL**

`POST /v1/search/behavior`

+ **Request body:**

```
{
    "SleepTime": [10, 22]
}
```

+ **Response:**

    **Code:** 200 OK<br>

    **Response body:**

```
[
]
```

## Search Device by sn:

+ **URL**

`GET /v1/device/search/:sn`

+ **Response:**

    **Code:** 200 OK<br>

    **Response body:**

```
{
    "updated_at": ["2016-04-09T23:05:15.163126Z"],
    "uuid": "xxx",
    "device_type": [1],
    "hardware_version": [2],
    "firmware_version": [4],
    "company_name": "abc",
    "biological_type": "xxx",
    "imsi": "232",
    "sim_number": "1231",
    "mac": "axafew",
    "description": "awfwef"
}
```





