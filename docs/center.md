# Push Server API
This document is API for push server.

## Protocol and Format
The API server through HTTP Request and websocket now. Both request and response are encoded in `application/json`using`utf-8`.

## Authentication
...

## Errors

The following are common error responses from the server. They will not be repeated in each API specification.

Code | Meaning  |
:----|:---------|
400  | Bad Request - The request is malformed.
401  | Unauthorized - The authentication token is not valid.
403  | Forbidden - The user is not authorized to make this request.
404  | Not Found - The resource could not be found.
500  | Internal Server Error - Something went wrong on the server side.

The body of the error response is encoded in JSON, like the following:

```
{
  "Code": 500,
  "message": "Internal Server Error"
}
```

## Time Format

The time in the API request and response is using UTC timezone and [RFC3339](https://www.ietf.org/rfc/rfc3339.txt) standard format.

# Request page parmeter
```
HTTP Request:
X-Result-Limit: 100
X-Result-Offset: 100
X-Result-Sort: -updated_at


HTTP Response:
X-Result-Count: 100000000

sort is sort by xxx, - is reverse
offset is sorted result offset
limit is sorted result limit
```

## message format
```
id:"58635ddceceb701fed4fb63c"
消息id，唯一

dst:"580099199e190d6e5fdc2662"
目的用户

level:1
消息级别: 
	LevelInfo		= 0     普通
	LevelWarning    = 1     警告
	LevelError      = 2     严重

msg:"Modify biological info!"
推送的消息

msg_type:0
消息格式: 
    MsgTypeString   = 0     string数据
    MsgTypeProto    = 1     protobuffer数据
    MsgTypeJson     = 2     json数据

src:"580099199e190d6e5fdc2662"
发送人用户id

src_name:"abc"
发送人用户名

type:2
消息类型:
	TypeSystem		=	0       系统通知消息
	TypeUser        =   1       与用户相关消息
	TypeDevice      =   2       与设备相关消息
	TypeKeep        =   3       心跳包          //暂未使用

target:"58300d9921a43e671cf99a82"
如果是设备消息，这个就是设备id，    如果是用户类型消息，这个就是用户id 

target_str:"0"
如果是设备，就是设备编号，      如果是用户类型消息，这个就是用户名

timestamp:"2016-12-28T06:38:20.862Z"
消息时间

```

# websocket

## push

+ **URL**

    `GET /v1/ws/push`

+ **Request http header:**

    ```
    `X-Druid-Authentication-Token: <token-value>`

    ```

+ **Send message:**

    ```
    {
        "dst": "ef22222222wef2",
        "type": 1,
        "level": 1,
        "msg": "hello world",
    }
    ```

+ **Recv message:**

    ```
    [
        {
        "id": "wefwef",
        "dst": "efwef2",
        "type": 1,
        "level": 1,
        "msg": "hello world",
        "src": "234234234",
        "src_name": "test",
        "timestamp": "2016-10-20T03:28:44.692Z""
        },
    ]
    ```


# rest api http

## get unread message

+ **URL**

    `GET /v1/http/message/unread`

+ **Request http header:**

    ```
    `X-Druid-Authentication-Token: <token-value>`

    ```

+ **Response body:**

    ```
    [
        {
        "id": "wefwef",
        "dst": "efwef2",
        "type": 1,
        "level": 1,
        "msg": "hello world",
        "src": "234234234",
        "src_name": "test",
        "timestamp": "2016-10-20T03:28:44.692Z",
        },
    ]
    ```

## list all of message

+ **URL**

    `GET /v1/http/message/`

+ **Request http header:**

    ```
    `X-Druid-Authentication-Token: <token-value>`

    ```

+ **Response body:**

    ```
    [
        {
        "id": "wefwef",
        "dst": "efwef2",
        "type": 1,
        "level": 1,
        "msg": "hello world",
        "src": "234234234",
        "src_name": "test",
        "timestamp": "2016-10-20T03:28:44.692Z",
        "readed_at"  : "2016-10-20T03:28:44.692Z"
        },
    ]
    ```

## post message to other user

+ **URL**

    `POST /v1/http/message`

+ **Request http header:**

    ```
    `X-Druid-Authentication-Token: <token-value>`

    ```

+ **Request body:**

    ```
    {
        "dst": "ef22222222wef2",
        "type": 1,
        "level": 1,
        "msg": "hello world",
    }
    ```

## modify message to readed

+ **URL**

    `PUT /v1/http/message/`

+ **Request http header:**

    ```
    `X-Druid-Authentication-Token: <token-value>`

    ```

+ **Request body:**

    ```
    {
        "id": [
            "we323232322fwef",
            "we323232322fwef",
        ]
    }
    ```

## delete message

+ **URL**

    `DELETE /v1/http/message/`

+ **Request http header:**

    ```
    `X-Druid-Authentication-Token: <token-value>`

    ```

+ **Request body:**

    ```
    {
        "id": [
            "we323232322fwef",
            "we323232322fwef",
        ]
    }
    ```

