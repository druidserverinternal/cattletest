# Druid Config
配置服务器运行参数

## Config json

```
{
    "address": ":9090",
    "database": "druid:druid-heifeng@151.80.42.192:9017/druid",
     "authentication" : { 
        "secret" : "AES256-Druid-heifeng-huteng-1234"
     },  
     "files": {
         "dir": "/Users/abc/druid/file/"
     },
     "log": {
        "level": "info",
        "path": "/Users/abc/druid/druid.log"
     }
}
```

`log level: debug, info, warning, error, fatal, panic`


