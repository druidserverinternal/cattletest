# Druid Application Server Database Design
This document is mongodb design.

## users collection
```
{
    "_id": "234234",    
    "updated_at": "xxx",
    "deleted_at": "xxx",
    "username": "abc",
    "hashed_password": "weofheohrrwerwe",
    "token": "awefwefewf",
    "email": "aa@aa.com",
    "role": "admin",
    "creator_id": ObjectId("4241412312312"),
    "company_id": ObjectId("33232312144"),
    "company_name": "abc",
}
```

+ **Token**
```
v=1;username=xxxaaa;n=g28vba6p;expiry=2016-04-16T00:13:40Z;hmac=vR9JawjSfWqFdcaE3AUTobUIHYUjs857e6KMEs5fUHI=
```

+ **Role**

Role  | Mean 
:-----|:-----
root  | root user, could be create company and admin user
admin | admin user, company administrator user, could be create company user
user  | general user

+ **Index:**<br> 
    `user_name`<br>
    `company_id`<br>
    `creator_id`


## company collection

```
{
    "_id": "234324324234",
    "updated_at": "xxx",
    "deleted_at": "xxx",
    "company_name": "xxx",
    "phone": "",
    "address": "",
}
```

+ **Index:**<br> 
    `company_name`

## devices collection
```
{
    "_id": "234324324234",
    "updated_at": "xxx",
    "deleted_at": "xxx",
//    "timestamp": "2016-03-09T23:05:15.163126Z",
    "uuid": "xxx",
    "device_type": 1,
    "hardware_version": 2,
    "firmware_version": 4,
    "firmware_history": [
        1,
        3
    ],
    "company_id": "234234234",
    "company_name": "abc",
    "biological_type": "xxx",
    "biological_ref": ,
//    "setting_id": "424234",
//    "active_status": false,             //是否激活
//    "init_status": 2,                   //初始化检测结果
    "imsi": "",
    "sim_number": "",
    "mac": "",
    "description": "", 
}
```

+ **Index:**<br>
    `{"cpu_id": 1}`<br>
    `{"company_id": 1, "device_type": 1}`

## setting setting
```
{
    "_id": "234324324234",
    "device_id": "234324324234",
    "updated_at": "xxx",
    "uuid": "",
    "env_sampling_mode": 2,            //环境采样模式
    "env_sampling_freq": 66,            //环境采样间隔
    "behavior_sampling_mode": 1,       //行为采样模式
    "behavior_sampling_freq": 11,       //行为采样间隔
//    "assist_information_mode": "",
//    "assist_information_freq": "",
    "gprs_mode": 3,                      //GPRS模式
    "gprs_freq": 3600,                  //GRPS通信间隔
    "env_voltage_threshold": 2,        //环境电压门限
    "behavior_voltage_threshold": 2,   //行为电压门限
    "gprs_voltage_threshold": 3,       //GPRS电压门限
    "ota_voltage_threshold": 34,        //OTA电压门限
    "sp_number": "",                    //短信sp号码
    "gprs_version": 5,                 //GPRS反转门限
    "firmware_id": "32232323232",       //固件id
    "firmware_version": 1,       //固件version
}
```

## device status collection

```
{
    "_id": "234234",
    "device_id": ObjectId("3223232422234"),
    "uuid": "",
    "firmware_version": 4,
    "timestamp": "2016-03-09T23:05:15.163126Z",
    "status_type": "",
    "status_value": "",
}
```

+ **Index:**<br>
    `device_id, timestamp`
    `timestamp`

## bird collection

```
{
    "_id": "234234",
    "updated_at": "xxx",
    "deleted_at": "xxx",
    "device_id": ObjectId("3422234"),
//    "company_id": ObjectId("234234234"),
    "animal_type": "",
    "xx": "xx",
    "xx": "xx",
    "xx": "xx",
}
```

+ **Index:**<br>
    `{"device_id: 1", "company_id": 1}`<br>
    `{"company_id": 1}`

## behavior collection

```
{
    "_id": "323232",
    "device_id": ObjectId("3422234"),
    "uuid": "",
    "firmware_version": 2,
    "collection-_ime": "2016-03-09T23:05:15.163126Z",

    "sleep_time": 66,                //睡觉时间
    "other_time": 22,                //其他行为
    "activity_time": 1,                //活动

    "fly_time": 12,                  //飞行
    "peck_time": 23,                  //啄食
    "crawl_time": 23,                 //爬行
    "run_time": 23,                  //奔跑


    "activity_expend": 11,      
    "other_expend": 22,
    "sport_expend": 3,
    "fly_expend": 5,
    "peck_expend": 4,
    "crawl_expend": 3,
    "run_expend": 3,
}
```
## gps collection
```
{
    "_id": "323232",
    "device_id": ObjectId("3422234"),
    "uuid": "",
    "firmware_version": 2,
    "timestamp": "2016-03-09T23:05:15.163126Z",
    "uuid": "323232",
    "longitude": "xxx",         //经度
    "latitude": "",             //纬度
    "altitude": "",             //海拔
//    "accuracy": "",
//    "bearing": "",
    "temperature": "",          //温度
    "humidity": "",             //相对湿度
    "light": 99,                //光强
    "pressure": 44,             //气压
        
        //assist data
    "usedstar": 4,              //定位卫星数
//    "viewstar": "",
    "dimension": "",            // 定位类型
    "speed": "",                // 速度
	"horizontal": "",           // 水平定位精度
	"vertical": "",             // 垂直定位精度
	"course": "",               // 航向


    "battery_voltage": "",      // 电池电压            
    "signal_strength": "",      //信号强度
}
```

+ **Index:**<br> 
    `"device_id", "timestamp"` <br>

## wifi collection

```
{
    "_id": "23423423",
    "device_id": ObjectId("3422234"),
    "firmware_version": "2.2",
    "timestamp": "2016-03-09T23:05:15.163126Z",
    "ap": [
    {
        "ssid": "aa",
        "mac": "aaaa",
        "signal": "11"    
    }, {
        "ssid": "bb",
        "mac": "bb",
        "signal": "22"    
    }
    ]
}
```

+ **Index:**<br> 
    `"device_id"` <br>

## gsensor collection

```
{
    "id": "2123232",
    "device_id": ObjectId("3422234"),
    "firmware_version": "2.2",
    "timestamp": "2016-03-09T23:05:15.163126Z",
    "x": "xxx",
    "y": "yyy",
    "z": "zzz"
}
```

+ **Index:**<br> 
    `"device_id"` <br>

## Firmware Collection

```
{
    "id": "2123232",
    "updated_at": "xxx",
    "deleted_at": "xxx",
    "device_type": "xxx",
    "hardware_version": "xxx",
    "firmware_version": "xxx",
    "path": "xxx",
    "md5": "",
    "name": "",
}
```

+ **Index:**
    `device_type, hardware_version, firmware_version`


## sim collection
```
{
    "_id": "234324324234",
    "deleted_at": "xxx",
    "imsi": "11121212",
    "sim_number": "121212121",
    "description": "", 
}
```


