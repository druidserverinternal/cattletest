package main

import (
	"druid/cattle/models"

	"fmt"
)

func main() {
	ch := make(chan []byte, models.MaxChannel)
	_, err := models.NewNSQReader("name", "abc", ch, []string{"127.0.0.1:4161"})
	if err != nil {
		fmt.Println("Init nsq read failed.", err)
		return
	}

	for {
		select {
			case data := <- ch:
				fmt.Println("recv data:", data)

		}
	}
}

