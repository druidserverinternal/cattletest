package cmd

import (
	"druid/cattle/micro/common"
	"druid/cattle/micro/server"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	log "github.com/sirupsen/logrus"
)

// serveCmd represents the serve command
var serveCmd = &cobra.Command{
	Use:    "serve",
	Short:  "Start micro server",
	PreRun: LoadConfiguration,
	Run:    serve,
}

func init() {
	RootCmd.AddCommand(serveCmd)

	serveCmd.Flags().StringP(
		"cmd",
		"m",
		"",
		"valid command: sync_mysql, gps_count",
	)
	viper.BindPFlag("cmd", serveCmd.Flags().Lookup("cmd"))
}

func serve(cmd *cobra.Command, args []string) {
	// Init the logger
	common.LoggerInit()

	/*
		if err := public.ConnectEtcd(
			viper.GetString("etcd.address"),
			viper.GetString("etcd.ca"),
			viper.GetString("etcd.cert"),
			viper.GetString("etcd.key"),
		); err != nil {
			log.Fatalln("connect to etcd: ", err)
		}
	*/

	log.Info("Starting micro server.")
	server.Server()
}
