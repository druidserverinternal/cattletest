package main

import (
	"druid/cattle/micro/cmd"
	"druid/cattle/micro/handlers"
	"druid/cattle/micro/server"
	log "github.com/sirupsen/logrus"
	"time"
)

// Exported onstants for storing build information
var (
	BuildVersion string
	BuildDate    string
	BuildCommit  string
)

func init() {
	time.Local = time.UTC
}

func main() {
	cmd.BuildInfo.Version = BuildVersion
	cmd.BuildInfo.Date = BuildDate
	cmd.BuildInfo.Commit = BuildCommit

	server.Run = handlers.Dead

	if err := cmd.RootCmd.Execute(); err != nil {
		log.Fatalln(err)
	}
}
