package handlers

import (
	"druid/cattle/models"
	"druid/cattle/micro/common"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"

	"time"
	"bytes"
	"encoding/gob"
)

func Dead() {
	logger := common.GetLogger(&logrus.Fields{"method": "dead"})
	db, err := common.ConnectDB(viper.GetString("database"))
	if err != nil {
		logger.Error("Connect to db failed.", err)
		return
	}

	ch := make(chan []byte, models.MaxChannel)
	nr, err := models.NewNSQReader(models.GetTopicBehavior(), models.GetChannelDead(), ch, models.StringSplit(viper.GetString("nsq.nsqlookupd.address"), ", "))
	if err != nil {
		logger.Error("Init nsq reader failed.", err)
		return
	}
	defer nr.Close()

	devices := make(map[bson.ObjectId]*(time.Time), models.MaxChannel / 10)

	timeout := 2 * time.Minute
	for {
		stop := time.After(timeout)
		select {
			case data := <- ch:
				if len(data) == 0 {
					continue
				}

				buff := bytes.NewBuffer(data)
				dec := gob.NewDecoder(buff)
				device := models.Device{}
				err = dec.Decode(&device)
				if err != nil {
					logger.Error("Decode device failed:", err)
					continue
				}
				if devices[device.ID] != nil {
					if devices[device.ID].Before(*(device.UpdatedAt)) {
						devices[device.ID] = device.UpdatedAt
					}
				} else {
					devices[device.ID] = device.UpdatedAt
				}

				if time.Now().Sub(*(devices[device.ID])) > timeout {
					if err := checkDead(db.Clone(), device.ID); err != nil {
						logger.Error("Check dead failed.", err)
					}
					delete(devices, device.ID)
				}
			case <- stop:
				for k, _ := range devices {
					if err := checkDead(db.Clone(), k); err != nil {
						logger.Error("Check dead failed.", err)
					}
					delete(devices, k)
				}
		}
	}
}

func checkDead(db *mgo.Session, deviceID bson.ObjectId) error {
	defer db.Close()
	logger := common.GetLogger(&logrus.Fields{"method": "dead"})

	device, err := models.FindDeviceByID(db, deviceID, nil)
	if err != nil {
		return models.Error("Find device by id failed:", err)
	}

	status, err := models.CheckDead(db, device)
	if err != nil {
		return models.Error("Check dead failed:", err)
	} 
	logger.Info("Device:", device.Mark, " status:", status, " last survive:", device.Survive)

	if status {
		err := models.SendDeadMessage(db, device)
		if err != nil {
			return models.Error("Send dead message failed.", err)
		}
	}

	return nil
}

