package handlers

import (
	"druid/cattle/micro/common"
	"druid/cattle/models"
	//"fmt"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"

	"bytes"
	"encoding/gob"
	"time"
)

func BiologicalAlgorithm() {
	logger := common.GetLogger(&logrus.Fields{"method": "biological_algorithm"})

	db, err := common.ConnectDB(viper.GetString("database"))
	if err != nil {
		logger.Error("Connect to db failed.", err)
		return
	}

	BeAlgo := models.GetBehaviorAlgorithm(db)
	if BeAlgo == nil {
		logger.Error("no have Paraneter for Motion algorithm")
		return
	}
	//如果默写参数为空，设为默认值
	if BeAlgo.MotionLimen1 == 0 || BeAlgo.MotionLimen2 == 0 {
		BeAlgo.MotionLimen1 = models.MotionODBALimen1
		BeAlgo.MotionLimen2 = models.MotionODBALimen2
	}

	ch := make(chan []byte, models.MaxChannel)
	nr, err := models.NewNSQReader(models.GetTopicBehavior2(), models.GetChannelBiologicalAlgorithm(), ch, models.StringSplit(viper.GetString("nsq.nsqlookupd.address"), ", "))
	if err != nil {
		logger.Error("Init nsq reader failed.", err)
		return
	}
	defer nr.Close()

	devices := make(map[bson.ObjectId]*(time.Time), models.MaxChannel/10)

	timeout := 2 * time.Minute
	for {
		stop := time.After(timeout)
		select {
		case data := <-ch:
			if len(data) == 0 {
				continue
			}

			buff := bytes.NewBuffer(data)
			dec := gob.NewDecoder(buff)
			device := models.Device{}
			err = dec.Decode(&device)
			if err != nil {
				logger.Error("Decode device failed:", err)
				continue
			}
			if devices[device.ID] != nil {
				if devices[device.ID].Before(*(device.UpdatedAt)) {
					devices[device.ID] = device.UpdatedAt
				}
			} else {
				devices[device.ID] = device.UpdatedAt
			}
			if time.Now().Sub(*(devices[device.ID])) > timeout {
				if err := checkMotion(db.Clone(), device.ID, BeAlgo); err != nil {
					logger.Error("checkBehavior failed : ", err)
				}
				delete(devices, device.ID)
			}
		case <-stop:
			for k, _ := range devices {
				if err := checkMotion(db.Clone(), k, BeAlgo); err != nil {
					logger.Error("CheckBehavior dead failed.", err)
				}
				delete(devices, k)
			}
		}
	}
}

func checkMotion(db *mgo.Session, deviceID bson.ObjectId, BeAlgo *models.BehaviorAlgorithm) error {
	defer db.Close()

	device, err := models.FindDeviceByID(db, deviceID, nil)
	if err != nil {
		return models.Error("Find device by id failed:", err)
	}

	if device.BehaviorVers != BeAlgo.Version {
		return updateAllBehaviorMotion(db, device, BeAlgo)
	} else {
		return AddBehaviorMotion(db, device, BeAlgo)
	}

}

func AddBehaviorMotion(db *mgo.Session, device *models.Device, BeAlgo *models.BehaviorAlgorithm) error {
	//顺序查找   优化
	t1 := time.Now()
	logger := common.GetLogger(&logrus.Fields{"method": "biological_algorithm"})
	rp := models.RequestParameter{}
	rp.Sort = append(rp.Sort, "+timestamp")

	Behs, err := models.ListBehavior2ByDeviceIDWithNoMotion(db, device.ID, &rp)
	if err != nil {
		return models.Error("Find behavior by id failed:", err, "  deviceID : ", device.ID, "  --新增")
	}
	//fmt.Println("一共有 ：", len(Behs))
	if len(Behs) == 0 {
		return models.Error("Find behavior by id failed:", err, "  deviceID : ", device.ID, "  --新增")
	}

	allBehaviorTypeHistory := []*models.BehaviorTypeHistory{}

	for i, lastBeh := range Behs {
		//处理数据
		dealMotionWithBeh(lastBeh, device, BeAlgo)
		//加入或者更新历史数据
		t := lastBeh.Timestamp
		timeNum := t.Unix() / 3600 * 3600
		if len(allBehaviorTypeHistory) == 0 {
			behaviorTypeHistory := models.GetBehaviorTypeHisoryWithTimeNum(db, device.ID, uint32(timeNum))
			if behaviorTypeHistory != nil { //说明上一条数据没完
				allBehaviorTypeHistory = append(allBehaviorTypeHistory, behaviorTypeHistory)
			}
		}
		if len(allBehaviorTypeHistory) == 0 || uint32(timeNum) > allBehaviorTypeHistory[len(allBehaviorTypeHistory)-1].TimeNum {
			thisBehavirTypeHistory := models.BehaviorTypeHistory{}
			thisBehavirTypeHistory.DeviceID = device.ID
			thisBehavirTypeHistory.OwnerID = device.OwnerID
			thisBehavirTypeHistory.Motions = append(thisBehavirTypeHistory.Motions, device.Motion)
			thisBehavirTypeHistory.TimeNum = uint32(timeNum)
			thisBehavirTypeHistory.MotionEatNum = 0
			thisBehavirTypeHistory.MotionRuminationNum = 0
			thisBehavirTypeHistory.MotionOtherLowNum = 0
			thisBehavirTypeHistory.MotionOtherMediumNum = 0
			thisBehavirTypeHistory.MotionOtherHightNum = 0
			switch device.Motion {
			case models.MotionEat:
				thisBehavirTypeHistory.MotionEatNum = thisBehavirTypeHistory.MotionEatNum + 1
			case models.MotionRumination:
				thisBehavirTypeHistory.MotionRuminationNum = thisBehavirTypeHistory.MotionRuminationNum + 1
			case models.MotionOtherLow:
				thisBehavirTypeHistory.MotionOtherLowNum = thisBehavirTypeHistory.MotionOtherLowNum + 1
			case models.MotionOtherMedium:
				thisBehavirTypeHistory.MotionOtherMediumNum = thisBehavirTypeHistory.MotionOtherMediumNum + 1
			case models.MotionOtherHight:
				thisBehavirTypeHistory.MotionOtherHightNum = thisBehavirTypeHistory.MotionOtherHightNum + 1
			}
			allBehaviorTypeHistory = append(allBehaviorTypeHistory, &thisBehavirTypeHistory)

		} else {
			ii := len(allBehaviorTypeHistory) - 1
			allBehaviorTypeHistory[ii].Motions = append(allBehaviorTypeHistory[ii].Motions, device.Motion)
			switch device.Motion {
			case models.MotionEat:
				allBehaviorTypeHistory[ii].MotionEatNum = allBehaviorTypeHistory[ii].MotionEatNum + 1
			case models.MotionRumination:
				allBehaviorTypeHistory[ii].MotionRuminationNum = allBehaviorTypeHistory[ii].MotionRuminationNum + 1
			case models.MotionOtherLow:
				allBehaviorTypeHistory[ii].MotionOtherLowNum = allBehaviorTypeHistory[ii].MotionOtherLowNum + 1
			case models.MotionOtherMedium:
				allBehaviorTypeHistory[ii].MotionOtherMediumNum = allBehaviorTypeHistory[ii].MotionOtherMediumNum + 1
			case models.MotionOtherHight:
				allBehaviorTypeHistory[ii].MotionOtherHightNum = allBehaviorTypeHistory[ii].MotionOtherHightNum + 1
			}
		}

		Behs[i].Motion = device.Motion
	}

	q1 := models.GetBehaviorTypeHisoryBulk(db)
	for ii, beh := range allBehaviorTypeHistory {
		t := time.Now()
		beh.UpdatedAt = &t
		q1.Upsert(bson.M{"device_id": beh.DeviceID, "time_num": beh.TimeNum}, bson.M{"$set": beh})

		if (ii+1)%models.BulkMaxCount == 0 || ii >= len(allBehaviorTypeHistory)-1 {
			_, err = q1.Run()
			if err != nil {
				return models.Error("Upsert  BehaviorTypeHistory err :", err, "   deviceID : ", device.ID, "  --新增")
			}
			q1 = models.GetBehaviorTypeHisoryBulk(db)
		}
	}

	q2 := models.GetBehavior2Bulk(db)
	for ii, beh := range Behs {
		t := time.Now()
		beh.UpdatedAt = &t
		q2.Upsert(bson.M{"_id": beh.ID}, bson.M{"$set": bson.M{"motion": beh.Motion}})

		if (ii+1)%models.BulkMaxCount == 0 || ii >= len(Behs)-1 {
			_, err = q2.Run()
			if err != nil {
				return models.Error("Upsert behavior2  err : ", err, "  deviceID : ", device.ID, "  --新增")
			}
			q2 = models.GetBehavior2Bulk(db)
		}

	}
	err = device.UpdateMotion(db)
	if err != nil {
		return models.Error("Update device err : ", err, "  deviceID : ", device.ID, "  --新增")
	}

	logger.Info("(跟新)完成设备：", device.ID, " 处理beh个数:", len(Behs), "  生成behType个数:", len(allBehaviorTypeHistory), "  花费时间：", time.Since(t1))
	return nil

}

func updateAllBehaviorMotion(db *mgo.Session, device *models.Device, BeAlgo *models.BehaviorAlgorithm) error {
	logger := common.GetLogger(&logrus.Fields{"method": "biological_algorithm"})

	t1 := time.Now()
	rp := models.RequestParameter{}
	rp.Sort = append(rp.Sort, "+timestamp")

	Behs, err := models.ListBehavior2ByDeviceID(db, device.ID, &rp)
	if err != nil {
		return models.Error("Find behavior by id failed:", err, "  deviceID : ", device.ID, "  --重算")
	}

	/*oldBehaviorTypeHistory, err := models.GetBehaviorTypeHisoryWithDeviceId(db, device.ID, &rp)
	if err != nil {
		return models.Error("Find behaviorTypeHistory by device failed: ", err)
	} */
	//fmt.Println("一共有 ：", len(Behs))

	allBehaviorTypeHistory := []*models.BehaviorTypeHistory{}
	for i, lastBeh := range Behs {
		//处理数据
		dealMotionWithBeh(lastBeh, device, BeAlgo)
		//加入或者更新历史数据
		t := lastBeh.Timestamp
		timeNum := t.Unix() / 3600 * 3600
		/*               重新生成的不需要检查第一条数据
		if len(allBehaviorTypeHistory) == 0 {
			behaviorTypeHistory := models.GetBehaviorTypeHisoryWithTimeNum(db, device.ID, uint32(timeNum))
			if behaviorTypeHistory != nil { //说明上一条数据没完
				allBehaviorTypeHistory = append(allBehaviorTypeHistory, behaviorTypeHistory)
			}
		}
		*/
		if len(allBehaviorTypeHistory) == 0 || uint32(timeNum) > allBehaviorTypeHistory[len(allBehaviorTypeHistory)-1].TimeNum {
			thisBehavirTypeHistory := models.BehaviorTypeHistory{}
			thisBehavirTypeHistory.DeviceID = device.ID
			thisBehavirTypeHistory.OwnerID = device.OwnerID
			thisBehavirTypeHistory.Motions = append(thisBehavirTypeHistory.Motions, device.Motion)
			thisBehavirTypeHistory.TimeNum = uint32(timeNum)
			thisBehavirTypeHistory.MotionEatNum = 0
			thisBehavirTypeHistory.MotionRuminationNum = 0
			thisBehavirTypeHistory.MotionOtherLowNum = 0
			thisBehavirTypeHistory.MotionOtherMediumNum = 0
			thisBehavirTypeHistory.MotionOtherHightNum = 0
			switch device.Motion {
			case models.MotionEat:
				thisBehavirTypeHistory.MotionEatNum = thisBehavirTypeHistory.MotionEatNum + 1
			case models.MotionRumination:
				thisBehavirTypeHistory.MotionRuminationNum = thisBehavirTypeHistory.MotionRuminationNum + 1
			case models.MotionOtherLow:
				thisBehavirTypeHistory.MotionOtherLowNum = thisBehavirTypeHistory.MotionOtherLowNum + 1
			case models.MotionOtherMedium:
				thisBehavirTypeHistory.MotionOtherMediumNum = thisBehavirTypeHistory.MotionOtherMediumNum + 1
			case models.MotionOtherHight:
				thisBehavirTypeHistory.MotionOtherHightNum = thisBehavirTypeHistory.MotionOtherHightNum + 1
			}
			allBehaviorTypeHistory = append(allBehaviorTypeHistory, &thisBehavirTypeHistory)

		} else {
			ii := len(allBehaviorTypeHistory) - 1
			allBehaviorTypeHistory[ii].Motions = append(allBehaviorTypeHistory[ii].Motions, device.Motion)
			switch device.Motion {
			case models.MotionEat:
				allBehaviorTypeHistory[ii].MotionEatNum = allBehaviorTypeHistory[ii].MotionEatNum + 1
			case models.MotionRumination:
				allBehaviorTypeHistory[ii].MotionRuminationNum = allBehaviorTypeHistory[ii].MotionRuminationNum + 1
			case models.MotionOtherLow:
				allBehaviorTypeHistory[ii].MotionOtherLowNum = allBehaviorTypeHistory[ii].MotionOtherLowNum + 1
			case models.MotionOtherMedium:
				allBehaviorTypeHistory[ii].MotionOtherMediumNum = allBehaviorTypeHistory[ii].MotionOtherMediumNum + 1
			case models.MotionOtherHight:
				allBehaviorTypeHistory[ii].MotionOtherHightNum = allBehaviorTypeHistory[ii].MotionOtherHightNum + 1
			}
		}

		Behs[i].Motion = device.Motion
	}

	q1 := models.GetBehaviorTypeHisoryBulk(db)
	for ii, beh := range allBehaviorTypeHistory {
		t := time.Now()
		beh.UpdatedAt = &t
		q1.Upsert(bson.M{"device_id": beh.DeviceID, "time_num": beh.TimeNum}, bson.M{"$set": beh})

		if (ii+1)%models.BulkMaxCount == 0 || ii >= len(allBehaviorTypeHistory)-1 {

			_, err = q1.Run()
			if err != nil {
				return models.Error("Upsert  BehaviorTypeHistory err :", err, "  deviceID : ", device.ID, "  --重算")
			}
			q1 = models.GetBehaviorTypeHisoryBulk(db)
		}
	}

	q2 := models.GetBehavior2Bulk(db)
	for ii, beh := range Behs {
		t := time.Now()
		beh.UpdatedAt = &t
		q2.Upsert(bson.M{"_id": beh.ID}, bson.M{"$set": bson.M{"motion": beh.Motion}})

		if (ii+1)%models.BulkMaxCount == 0 || ii >= len(Behs)-1 {
			_, err = q2.Run()
			if err != nil {
				return models.Error("Upsert behavior2  err : ", err, "  deviceID : ", device.ID, "  --重算")
			}
			q2 = models.GetBehavior2Bulk(db)
		}

	}
	/*
		if len(oldBehaviorTypeHistory) > len(allBehaviorTypeHistory) {
			// 数据数量错误
			return models.Error("BehaviorTypeHistory data num is err: old: ", len(oldBehaviorTypeHistory), "  new: ", len(allBehaviorTypeHistory))
		}

		for k, _ := range oldBehaviorTypeHistory {
			allBehaviorTypeHistory[k].ID = oldBehaviorTypeHistory[k].ID
		}

		err = models.BehaviorTypeHistoryListCreate(db, allBehaviorTypeHistory)
		if err != nil {
			return models.Error("Create BehaviorTypeHistory err :", err)
		}

		err = models.Behavior2ListCreate(db, Behs) //就是替换以前所有的behs统一更新 motion
		if err != nil {
			return models.Error("Update device err : ", err)
		}
	*/
	err = device.UpdateMotion(db)
	if err != nil {
		return models.Error("Update device err : ", err, "  deviceID : ", device.ID, "  --重算")
	}

	device.BehaviorVers = BeAlgo.Version
	// deal data
	err = device.UpdateParameterVersionBeh(db)
	if err != nil {
		return models.Error("Update Parameter Version device  failed:", err, "  deviceID : ", device.ID, "  --重算")
	}

	logger.Info("(重算)完成设备：", device.ID, " 处理beh个数:", len(Behs), "  生成behType个数:", len(allBehaviorTypeHistory), "  花费时间：", time.Since(t1))
	return nil

}

func dealMotionWithBeh(lastBeh *models.Behavior2, device *models.Device, BeAlgo *models.BehaviorAlgorithm) {
	results := make([]int, 10, 10)
	Trees := BeAlgo.Trees
	parameter := make([]float32, 7)
	parameter[1] = float32(lastBeh.ODBAX) / 10000.0
	parameter[2] = float32(lastBeh.ODBAY) / 10000.0
	parameter[3] = float32(lastBeh.ODBAZ) / 10000.0
	parameter[4] = float32(lastBeh.MeandlX) / 10000.0
	parameter[5] = float32(lastBeh.MeandlY) / 10000.0
	parameter[6] = float32(lastBeh.MeandlZ) / 10000.0
	parameter[0] = parameter[1] + parameter[2] + parameter[3]
	for _, Tree := range Trees {
		num := models.RecursiveTheBehaviorTrees(Tree, 0, parameter)
		if num < 0 || num >= 9 {
			continue
		}
		results[num] = results[num] + 1
	}
	motion := getMaxResults(results)
	if motion != models.MotionOtherLow {
		device.Motion = motion
	} else {
		if parameter[0] <= float32(BeAlgo.MotionLimen1) {
			device.Motion = models.MotionOtherLow
		}

		if float32(BeAlgo.MotionLimen1) < parameter[0] && parameter[0] <= float32(BeAlgo.MotionLimen2) {
			device.Motion = models.MotionOtherMedium
		}

		if float32(BeAlgo.MotionLimen2) < parameter[0] {
			device.Motion = models.MotionOtherHight
		}
	}
}

func getMaxResults(results []int) int {
	maxNum := 0
	index := 0
	for i, k := range results {
		if k > maxNum {
			maxNum = k
			index = i
		}
	}
	return index
}
