package handlers

import (
	"druid/cattle/micro/common"
	"druid/cattle/models"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"

	"bytes"
	"encoding/gob"
	"time"
)

func Area() {
	logger := common.GetLogger(&logrus.Fields{"method": "area"})

	db, err := common.ConnectDB(viper.GetString("database"))
	if err != nil {
		logger.Error("Connect to db failed.", err)
		return
	}

	ch := make(chan []byte, models.MaxChannel)
	_, err = models.NewNSQReader(models.GetTopicStatus(), models.GetChannelGeofence(), ch, models.StringSplit(viper.GetString("nsq.nsqlookupd.address"), ", "))
	if err != nil {
		logger.Error("Init nsq read failed.", err)
		return
	}

	devices := make(map[bson.ObjectId]*(time.Time), models.MaxChannel/10)

	timeout := 10 * time.Second
	for {
		stop := time.After(timeout)
		select {
		case data := <-ch:
			if len(data) == 0 {
				continue
			}

			buff := bytes.NewBuffer(data)
			dec := gob.NewDecoder(buff)
			device := models.Device{}
			err = dec.Decode(&device)
			if err != nil {
				logger.Error("Decode device failed:", err)
				continue
			}
			if devices[device.ID] != nil {
				if devices[device.ID].Before(*(device.UpdatedAt)) {
					devices[device.ID] = device.UpdatedAt
				}
			} else {
				devices[device.ID] = device.UpdatedAt
			}

			if time.Now().Sub(*(devices[device.ID])) > timeout {
				if err := checkArea(db.Copy(), device.ID); err != nil {
					logger.Error("Check dead failed.", err)
				}
				delete(devices, device.ID)
			}
		case <-stop:
			for k, _ := range devices {
				if err := checkArea(db.Copy(), k); err != nil {
					logger.Error("Check dead failed.", err)
				}
				delete(devices, k)
			}
		}
	}
}

func checkArea(db *mgo.Session, deviceID bson.ObjectId) error {
	defer db.Close()

	logger := common.GetLogger(&logrus.Fields{"method": "area"})

	device, err := models.FindDeviceByID(db, deviceID, nil)
	if err != nil {
		return models.Error("Find device by id failed:", err)
	}
	if device.Loc == nil {
		return models.Error("Device last gps invalid.")
	}

	dhs, err := models.ListDituHistoryByDeviceID(db, device.ID)
	if err != nil {
		return models.Error("List ditu area failed:", err)
	}

	logger.Info("start check area:", len(dhs), " device:", *device)
	for _, dh := range dhs {
		area, err := models.FindDituAreaByID(db, dh.AreaID)
		if err != nil {
			return models.Error("Find ditu area failed:", err)
		}

		status := area.CheckContains(device.Longitude, device.Latitude)
		logger.Debugf("Start check device:%+v, area:%+v, status:%v", device, area, status)
		if status {
			if dh.SendedAt == nil || dh.SendedAt.Before(*area.UpdatedAt) {
				logger.Infof("Start send message device:%+v, area:%+v", device, area)
				if err := models.SendAreaMessage(db, device, area); err != nil {
					return err
				}
				if err := models.MarkDituHistorySended(db, device.ID, area.ID); err != nil {
					return models.Error("Mark ditu area history failed:", err)
				}
			}

			continue
		}

		if dh.SendedAt != nil {
			logger.Infof("Clean area history: %+v", area)
			models.MarkDituHistoryUnSended(db, device.ID, area.ID)
		}
	}

	return nil
}
