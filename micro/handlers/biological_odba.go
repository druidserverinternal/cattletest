package handlers

import (
	"druid/cattle/micro/common"
	"druid/cattle/models"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"bytes"
	"encoding/gob"
	"fmt"
	"time"
)

func BiologicalODBA() {
	logger := common.GetLogger(&logrus.Fields{"method": "biological_obda"})

	db, err := common.ConnectDB(viper.GetString("database"))
	if err != nil {
		logger.Error("Connect to db failed.", err)
		return
	}

	odbaAlgo := models.GetODBAAlgorithm(db)
	if odbaAlgo == nil {
		logger.Error("lack odbaAlgo!!! plase update odbaAlgoParamerte")
		return
	}

	if odbaAlgo.ActThreshold == 0 || odbaAlgo.ActSlop == 0 {
		odbaAlgo.ActThreshold = models.ODBAActThreshold
		odbaAlgo.ActSlop = models.ODBAActSlop
	}

	ch := make(chan []byte, models.MaxChannel)
	nr, err := models.NewNSQReader(models.GetTopicBehavior2(), models.GetChannelBiologicalODBA(), ch, models.StringSplit(viper.GetString("nsq.nsqlookupd.address"), ", "))
	if err != nil {
		logger.Error("Init nsq reader failed.", err)
		return
	}
	defer nr.Close()

	devices := make(map[bson.ObjectId]*(time.Time), models.MaxChannel/10)

	timeout := 2 * time.Minute
	for {
		stop := time.After(timeout)
		select {
		case data := <-ch:
			if len(data) == 0 {
				continue
			}

			buff := bytes.NewBuffer(data)
			dec := gob.NewDecoder(buff)
			device := models.Device{}
			err = dec.Decode(&device)
			if err != nil {
				logger.Error("Decode device failed:", err)
				continue
			}
			if devices[device.ID] != nil {
				if devices[device.ID].Before(*(device.UpdatedAt)) {
					devices[device.ID] = device.UpdatedAt
				}
			} else {
				devices[device.ID] = device.UpdatedAt
			}
			if time.Now().Sub(*(devices[device.ID])) > timeout {
				if err := checkODBA(db.Clone(), device.ID, odbaAlgo); err != nil {
					logger.Error("checkODBA failed : ", err)
					return
				}
				delete(devices, device.ID)
			}
		case <-stop:
			for k, _ := range devices {
				if err := checkODBA(db.Clone(), k, odbaAlgo); err != nil {
					logger.Error("CheckODBA dead failed.", err)
				}
				delete(devices, k)
			}
		}
	}
}
func AddODBAData(db *mgo.Session, device *models.Device, odbaAlgo *models.ODBAAlgorithm) error {

	t1 := time.Now()
	logger := common.GetLogger(&logrus.Fields{"method": "biological_obda"})
	rp := models.RequestParameter{}
	rp.Sort = append(rp.Sort, "+timestamp")

	Behs, err := models.ListBehavior2ByDeviceIDWithNoAct(db, device.ID, &rp)
	if err != nil {
		return models.Error("Find behavior by id failed:", err, "  deviceID : ", device.ID, "  --新增")
	}
	//fmt.Println("一共有：", len(Behs))
	if len(Behs) == 0 {
		return models.Error("Find behavior by id failed:", err, "  deviceID : ", device.ID, "  --新增")
	}

	//allODBAParamters := []*models.ODBAParameter{}

	//找到第一条数据之前的23小时的数据保存  如果没有则存空 且确保后面数据能向后加
	odbaEndTime := Behs[0].Timestamp.Unix() / 3600 * 3600
	odbaStartTime := odbaEndTime - 3600*24 // 24小时前,每次查前一个小时的
	beforeODBAParamters, _, _ := models.ListODBAParameterRange(db, device.ID, uint32(odbaStartTime), uint32(odbaEndTime), &rp)

	for I, lastBeh := range Behs {

		odba := float32(lastBeh.ODBAX)/10000.0 + float32(lastBeh.ODBAY)/10000.0 + float32(lastBeh.ODBAZ)/10000.0
		act := 0
		if odba >= odbaAlgo.ActThreshold {
			act = 1
			lastBeh.Act = 1
			Behs[I].Act = 1
		} else {
			act = 0
			lastBeh.Act = -1
			Behs[I].Act = -1
		}

		t := lastBeh.Timestamp
		timeNum := t.Unix() / 3600 * 3600
		//odbaParamter := models.GetODBAParameterWithTimeNum(db, device.ID, uint32(timeNum))

		if len(beforeODBAParamters) == 0 || uint32(timeNum) > beforeODBAParamters[len(beforeODBAParamters)-1].TimeNum {
			thisodbaParamter := models.ODBAParameter{}
			thisodbaParamter.DeviceID = device.ID
			thisodbaParamter.OwnerID = device.OwnerID
			thisodbaParamter.TimeNum = uint32(timeNum)
			thisodbaParamter.Survive = 1
			thisodbaParamter.ActValidNum = act
			thisodbaParamter.ActTotalNum = 1

			beforeODBAParamters = append(beforeODBAParamters, &thisodbaParamter)
		} else {
			ii := len(beforeODBAParamters) - 1
			beforeODBAParamters[ii].Activity = append(beforeODBAParamters[ii].Activity, act)
			beforeODBAParamters[ii].ActTotalNum = beforeODBAParamters[ii].ActTotalNum + 1
			beforeODBAParamters[ii].ActValidNum = beforeODBAParamters[ii].ActValidNum + act
		}

		//判断最后一个状态是否存在，不存在则加入
		/*
			if len(beforeODBAParamters) > 0 && beforeODBAParamters[len(beforeODBAParamters)-1].TimeNum < uint32(timeNum)-3600 {
			tempODBA := models.GetODBAParameterWithTimeNum(db, device.ID, uint32(timeNum)-3600)
			if tempODBA != nil {
				beforeODBAParamters = append(beforeODBAParamters, tempODBA)
			}
		}*/

		//处理之前24小时的数据 判断是否发情
		heatAlgorithm(db, beforeODBAParamters, uint32(timeNum-3600), odbaAlgo)

	}
	//存储 beh2
	q1 := models.GetBehavior2Bulk(db)
	for ii, beh := range Behs {
		t := time.Now()
		beh.UpdatedAt = &t
		q1.Upsert(bson.M{"_id": beh.ID}, bson.M{"$set": bson.M{"act": beh.Act}})

		if (ii+1)%models.BulkMaxCount == 0 || ii >= len(Behs)-1 {
			_, err = q1.Run()
			if err != nil {
				return models.Error("Upsert behavior2 is err :", err, "  deviceID : ", device.ID, "  --新增")
			}
			q1 = models.GetBehavior2Bulk(db)
		}
	}

	q2 := models.GetODBAParameterBulk(db)
	//存储
	for ii, odba := range beforeODBAParamters {
		t := time.Now()
		odba.UpdatedAt = &t
		q2.Upsert(bson.M{"device_id": odba.DeviceID, "time_num": odba.TimeNum}, bson.M{"$set": odba})
		if (ii+1)%models.BulkMaxCount == 0 || ii >= len(beforeODBAParamters)-1 {
			_, err = q2.Run()
			if err != nil {
				return models.Error("Upsert ODBAParaneter list err :", err, "  deviceID : ", device.ID, "  --新增")
			}
			q2 = models.GetODBAParameterBulk(db)
		}
	}

	logger.Info("(跟新)完成设备：", device.ID, " 处理beh个数:", len(Behs), "  生成ODBA个数:", len(beforeODBAParamters), "  花费时间：", time.Since(t1))
	return nil

}

func updateAllODBA(db *mgo.Session, device *models.Device, odbaAlgo *models.ODBAAlgorithm) error {
	t1 := time.Now()

	logger := common.GetLogger(&logrus.Fields{"method": "biological_obda"})
	rp := models.RequestParameter{}
	rp.Sort = append(rp.Sort, "+timestamp")

	Behs, err := models.ListBehavior2ByDeviceID(db, device.ID, &rp)
	if err != nil {
		return models.Error("Find behavior by id failed:", err, "  deviceID : ", device.ID, "  --重算")
	}
	if len(Behs) == 0 {
		return models.Error("Find behavior by id failed:", err, "  deviceID : ", device.ID, "  --重算")
	}

	beforeODBAParamters := []*models.ODBAParameter{}

	//在计算之前删除该设备所有发情数据
	err = models.RemoveAllODBALovingByDeviceID(db, device.ID)
	if err != nil {
		return models.Error("Remove old ODBa Loving by deviceId failed ", err, "  deviceID : ", device.ID, "  --重算")
	}

	for I, lastBeh := range Behs {
		odba := float32(lastBeh.ODBAX)/10000.0 + float32(lastBeh.ODBAY)/10000.0 + float32(lastBeh.ODBAZ)/10000.0
		act := 0
		if odba >= odbaAlgo.ActThreshold {
			act = 1
			lastBeh.Act = 1
			Behs[I].Act = 1
		} else {
			act = 0
			lastBeh.Act = -1
			Behs[I].Act = -1
		}

		t := lastBeh.Timestamp
		timeNum := t.Unix() / 3600 * 3600
		//odbaParamter := models.GetODBAParameterWithTimeNum(db, device.ID, uint32(timeNum))

		if len(beforeODBAParamters) == 0 || uint32(timeNum) > beforeODBAParamters[len(beforeODBAParamters)-1].TimeNum {
			thisodbaParamter := models.ODBAParameter{}
			thisodbaParamter.DeviceID = device.ID
			thisodbaParamter.OwnerID = device.OwnerID
			thisodbaParamter.TimeNum = uint32(timeNum)
			thisodbaParamter.Survive = 1
			thisodbaParamter.ActValidNum = act
			thisodbaParamter.ActTotalNum = 1

			beforeODBAParamters = append(beforeODBAParamters, &thisodbaParamter)
		} else {
			ii := len(beforeODBAParamters) - 1
			beforeODBAParamters[ii].Activity = append(beforeODBAParamters[ii].Activity, act)
			beforeODBAParamters[ii].ActTotalNum = beforeODBAParamters[ii].ActTotalNum + 1
			beforeODBAParamters[ii].ActValidNum = beforeODBAParamters[ii].ActValidNum + act
		}

		//判断最后一个状态是否存在，不存在则加入
		/*
			if len(beforeODBAParamters) > 0 && beforeODBAParamters[len(beforeODBAParamters)-1].TimeNum < uint32(timeNum)-3600 {
			tempODBA := models.GetODBAParameterWithTimeNum(db, device.ID, uint32(timeNum)-3600)
			if tempODBA != nil {
				beforeODBAParamters = append(beforeODBAParamters, tempODBA)
			}
		}*/

		//处理之前24小时的数据 判断是否发情
		heatAlgorithm(db, beforeODBAParamters, uint32(timeNum-3600), odbaAlgo)

	}

	//存储 beh2
	q1 := models.GetBehavior2Bulk(db)
	for ii, beh := range Behs {
		t := time.Now()
		beh.UpdatedAt = &t
		q1.Upsert(bson.M{"_id": beh.ID}, bson.M{"$set": bson.M{"act": beh.Act}})
		if (ii+1)%models.BulkMaxCount == 0 || ii >= len(Behs)-1 {
			_, err = q1.Run()
			if err != nil {
				return models.Error("Upsert behavior2 is err :", err, "  deviceID : ", device.ID, "  --重算")
			}
			q1 = models.GetBehavior2Bulk(db)
		}
	}

	q2 := models.GetODBAParameterBulk(db)
	//存储
	for ii, odba := range beforeODBAParamters {
		t := time.Now()
		odba.UpdatedAt = &t
		q2.Upsert(bson.M{"device_id": device.ID, "time_num": odba.TimeNum}, bson.M{"$set": odba})
		if (ii+1)%models.BulkMaxCount == 0 || ii >= len(beforeODBAParamters)-1 {
			_, err = q2.Run()
			if err != nil {
				return models.Error("Upsert ODBAParaneter list err :", err, "  deviceID : ", device.ID, "  --重算")
			}
			q2 = models.GetODBAParameterBulk(db)
		}
	}
	device.ODBAVers = odbaAlgo.Version
	err = device.UpdateParameterVersionODBA(db)
	if err != nil {
		return models.Error("Update Parameter Version device  failed:", err, "  deviceID : ", device.ID, "  --重算")
	}
	logger.Info("(重算)完成设备：", device.ID, " 处理beh个数:", len(Behs), "  生成ODBA个数:", len(beforeODBAParamters), "  花费时间：", time.Since(t1))
	return nil
}

func checkODBA(db *mgo.Session, deviceID bson.ObjectId, odbaAlgo *models.ODBAAlgorithm) error {
	defer db.Close()

	//logger := common.GetLogger(&logrus.Fields{"method": "biological_odba"})
	device, err := models.FindDeviceByID(db, deviceID, nil)
	if err != nil {
		return models.Error("Find device by id failed:", err)
	}

	if device.ODBAVers != odbaAlgo.Version {
		return updateAllODBA(db, device, odbaAlgo)
	} else {
		return AddODBAData(db, device, odbaAlgo)
	}

}
func heatAlgorithm(db *mgo.Session, beforeODBA []*models.ODBAParameter, thisTime uint32, odbaAlgo *models.ODBAAlgorithm) {

	logger := common.GetLogger(&logrus.Fields{"method": "biological_odba"})
	if len(beforeODBA) == 0 {
		return
	}

	thisCellNum := 0
	for i := len(beforeODBA) - 1; i >= 0; i-- {
		if beforeODBA[i].TimeNum == thisTime {
			thisCellNum = i
		}
	}
	if beforeODBA[thisCellNum].ActNow != 0.0 {
		return
	}

	//计算act_percenage
	beforeODBA[thisCellNum].ActPerecentage = float32(beforeODBA[thisCellNum].ActValidNum) * 1.0 / float32(beforeODBA[thisCellNum].ActTotalNum) * 100.0
	//logger.Debug("thisCellNum :", thisCellNum, " ActPerecentage:", beforeODBA[thisCellNum].ActPerecentage, " actNum:", beforeODBA[thisCellNum].ActValidNum, " actTolal:", beforeODBA[thisCellNum].ActTotalNum)

	var err error
	if thisCellNum < 23 {
		err = beforeODBA[thisCellNum].Upsert(db)
		if err != nil {
			logger.Error(" Upsert is failed  deviceID :", beforeODBA[thisCellNum].DeviceID, "  err : ", err)
		}
		return
	}

	//判断开始项是否为正确的时间
	startTime := thisTime - 3600*23
	if beforeODBA[thisCellNum-23].TimeNum != startTime {
		logger.Debug("err is heatAlgorithm Time : thisCellNum:", thisCellNum, " startTime:", beforeODBA[thisCellNum-23].TimeNum, " nowStartTime:", startTime)
		return
	}

	//开始处理
	logger.Debug("正式处理发情状态数据····")

	var act_now_all float32
	act_now_all = 0.0
	for i := thisCellNum - 23; i <= thisCellNum; i++ {
		act_now_all += beforeODBA[i].ActPerecentage
	} // 不用再次判断是否存在了
	beforeODBA[thisCellNum].ActNow = act_now_all / 24.0

	if beforeODBA[thisCellNum-9].ActNow != 0.0 {
		//处理得到act_slope的数据
		act_slope, err := GetActSlope(beforeODBA[thisCellNum-9 : thisCellNum+1]) //注意是闭区间
		if err == nil {
			if act_slope <= 0.0 { // 根据算法去掉发情后产生的波谷
				act_slope = 0.000001
			}
			beforeODBA[thisCellNum].ActSlope = act_slope
		}
	}
	//判断是否发情
	b_loving := 0
	if beforeODBA[thisCellNum-2].ActSlope > odbaAlgo.ActSlop && beforeODBA[thisCellNum-1].ActSlope > odbaAlgo.ActSlop && beforeODBA[thisCellNum].ActSlope > odbaAlgo.ActSlop && !(beforeODBA[thisCellNum].ActSlope < beforeODBA[thisCellNum-1].ActSlope && beforeODBA[thisCellNum-1].ActSlope < beforeODBA[thisCellNum-2].ActSlope) {
		logger.Debug("发情状态：", beforeODBA[thisCellNum-2].ActSlope, " ", beforeODBA[thisCellNum-1].ActSlope, " ", beforeODBA[thisCellNum].ActSlope)
		b_loving = 1
		beforeODBA[thisCellNum].Survive = models.SurviveLoving
	} else {
		logger.Debug("非发情状态：", beforeODBA[thisCellNum-2].ActSlope, " ", beforeODBA[thisCellNum-1].ActSlope, " ", beforeODBA[thisCellNum].ActSlope)
		beforeODBA[thisCellNum].Survive = models.SurviveLive
	}
	//判断是上一个状态是否是发情
	if beforeODBA[thisCellNum-1].Survive == models.SurviveLoving {
		// 发情
		if b_loving == 1 {
			//上一个状态同样为发情不处理
		} else {
			//上一个状态发情，本状态不发情为结束发情
			//1 标记设备正常状态
			err = models.UpdateDeviceWithSurvive(db, beforeODBA[thisCellNum].DeviceID, models.SurviveLive)
			if err != nil {
				logger.Error("UpdateDeviceWithSurvive is failed", err)
			}

			//2-找到该设备最后一个发情状态标记结束时间
			err := models.UpdateODBALovingWithEndOne(db, beforeODBA[thisCellNum].DeviceID, beforeODBA[thisCellNum].TimeNum)
			if err != nil {
				logger.Error("UpdateODBALovingWithEndOne is err", err)
			}

		}
	} else {
		//非发情状态
		if b_loving == 1 {
			//开始发情的状态
			//处理发情
			//将前2项置为发情状态
			beforeODBA[thisCellNum-1].Survive = models.SurviveLoving
			err = beforeODBA[thisCellNum-1].Upsert(db)
			if err != nil {
				logger.Error(" upsert is failed  deviceID :", beforeODBA[thisCellNum-1].DeviceID, "  err : ", err)
			}
			beforeODBA[thisCellNum-2].Survive = models.SurviveLoving
			err = beforeODBA[thisCellNum-2].Upsert(db)
			if err != nil {
				logger.Error(" upsert is failed  deviceID :", beforeODBA[thisCellNum-2].DeviceID, "  err : ", err)
			}

			//将设备置为发情
			err = models.UpdateDeviceWithSurvive(db, beforeODBA[thisCellNum].DeviceID, models.SurviveLoving)
			if err != nil {
				logger.Error("UpdateDeviceWithSurvive is failed ", err)
			}

			//创建发情历史
			odbaLoving := models.ODBALoving{}
			odbaLoving.TimeNum = beforeODBA[thisCellNum-2].TimeNum
			odbaLoving.DeviceID = beforeODBA[thisCellNum-2].DeviceID
			odbaLoving.OwnerID = beforeODBA[thisCellNum-2].OwnerID
			err = odbaLoving.Create(db)
			if err != nil {
				logger.Error("odbaLoving Create is failed", err)
			}
		} else {
			//上次和本次均为非发情状态不更改
		}
	}

	err = beforeODBA[thisCellNum].Upsert(db)
	if err != nil {
		logger.Error("upsert is failed ", err)
	}

}

func GetActSlope(ODBA []*models.ODBAParameter) (float32, error) {
	if len(ODBA) != 10 {
		return 0.0, fmt.Errorf("GetActSlope is err len(IDBA):", len(ODBA))
	}
	var A, B, C, D float32
	A = 0.0
	B = 0.0
	C = 0.0
	D = 0.0

	for i := 1; i <= 10; i++ {
		A = A + float32(i)*ODBA[i-1].ActNow
		B = B + float32(i)
		C = C + ODBA[i-1].ActNow
		D = D + float32(i)*float32(i)*1.0
	}

	return float32((A - B*C/10.0) / (D - B*B/10.0)), nil
}
