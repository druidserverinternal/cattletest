package handlers

import (
	"druid/public"
	"druid/cattle/models"
	"druid/cattle/micro/common"
	"druid/cattle/protocol"

	"gopkg.in/mgo.v2"
//	"gopkg.in/mgo.v2/bson"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/golang/protobuf/proto"

	"time"
	"bytes"
	"encoding/gob"
	"fmt"
)

func DeviceSMS() {
	logger := common.GetLogger(&logrus.Fields{"method": "device_sms"})

	db, err := common.ConnectDB(viper.GetString("database"))
	if err != nil {
		logger.Error("Connect to db failed.", err)
		return
	}

	ch := make(chan []byte, models.MaxChannel)
	_, err = models.NewNSQReader(models.GetTopicDeviceSMS(), models.GetChannelDeviceSMS(), ch, models.StringSplit(viper.GetString("nsq.nsqlookupd.address"), ", "))
	if err != nil {
		logger.Error("Init nsq read failed.", err)
		return
	}

	logger.Info("Start recv device sms service.")

	for {
		data := <- ch
		if len(data) == 0 {
			continue
		}

		buff := bytes.NewBuffer(data)
		dec := gob.NewDecoder(buff)
		sms := public.SMS{}
		err = dec.Decode(&sms)
		if err != nil {
			logger.Error("Decode sms failed:", err)
			continue
		}
		if err := insertSMS(db.Copy(), &sms); err != nil {
			logger.Error("Insert sms failed.", err, sms)
			continue
		}
		logger.Info("Insert sms success:", sms)
	}
}

func insertSMS(db *mgo.Session, sms *public.SMS) error {
	defer db.Close()

	var err error
	var sim *models.SIM

	if sms.SrcNumber != "" {
		sim, err = models.FindSIMByNumber(db, sms.SrcNumber)
		if err != nil {
			fmt.Printf("number:%x\n", sms.SrcNumber)
			return models.Error("Find sim card by number failed:", err, sms.SrcNumber, len(sms.SrcNumber), *sms)
		}
	} else if sms.SrcICCID != "" {
		sim, err = models.FindSIMByICCID(db, sms.SrcICCID)
		if err != nil {
			return models.Error("Find sim card by iccid failed:", err, sms.SrcICCID, *sms)
		}
	} else if sms.SrcIMSI != "" {
		sim, err = models.FindSIMByIMSI(db, sms.SrcIMSI)
		if err != nil {
			return models.Error("Find sim card by imsi failed:", err, sms.SrcIMSI, *sms)
		}
	}
	if sim == nil {
		return models.Error("Could not found sim card by sms,", *sms)
	}
	if sim.Device == nil {
		return models.Error("The sim card did not been attached.", *sim)
	}

	device, err := models.FindDeviceByID(db, sim.Device.ID, nil)
	if err != nil {
		return models.Error("Could not found sim's device.", *sim)
	}

	data, err := models.Base64ToBytes(sms.Message)
	if err != nil {
		return models.Error("Decode sms failed:", err, sms)
	}

	head, _ := models.MakeMsgHead(data)
	if protocol.HeaderType(head.MsgType) != protocol.HeaderType_TypeGPS2Req {
		fmt.Printf("msg:%x\n", data, sms.Message)
		return models.Error("Unknown msg type:", head.MsgType)
	}

	gps := &protocol.GPS2Req{}
	err = proto.Unmarshal(data[models.MSG_HEAD_SIZE:], gps)
	if err != nil {
		return models.Error("Parse pb msg failed:", err)
	}

	gpsInfo := gps.GetGPSInfo()
	if gpsInfo == nil {
		return models.Error("Recv zero sms msg.")
	}

	m := (*models.GPS)(nil)
	for _, gps := range gpsInfo {
		m = Gps2ToModels(gps)
		m.DeletedAt = device.DeletedAt
		m.DeviceID = device.ID
//		m.CompanyID = device.CompanyID
//		m.CompanyName = device.CompanyName
		m.FirmwareVersion = device.FirmwareVersion
		m.UUID = device.UUID
		m.Mark = device.Mark
		m.DeletedAt = device.DeletedAt
		now := time.Now()
		m.UpdatedAt = &now
		m.SMS = models.SrcFromSMS

		if m.Longitude < 190 {
			offset, err := models.FindOffsetByLocation(db, m.Longitude, m.Latitude)
			if err != nil {
				fmt.Println("Find offset by location failed.")
			}
			m.PointLocation = offset.PointLocation

	//		device.LastValidGPS = m
		}

		if m.FirmwareVersion < 11 && m.FixTime == 0 {
			t := m.Timestamp.Unix()
			ft := t % 300
			if ft >= 280 {
				m.FixTime = int32(ft - 300)
			} else {
				m.FixTime = int32(ft)
			}
		}

		if err := m.UpsertByTimestampSMS(db); err != nil {
			return models.Error("Upload gps failed:", err)
		}
	}

	if len(gpsInfo) > 0 {
		if err = models.SendMessageToNSQD(device, models.GetTopicGPS()); err != nil {
			fmt.Println("Send device to gps mq failed:", err)
		}
	}

	if err = models.SendMessageToNSQD(device, models.GetTopicDevice()); err != nil {
		fmt.Println("Send device to mq failed:", err)
	}

	return nil
}

// GpsToModels is convert protobuf to internal struct
func Gps2ToModels(in *protocol.GPS2) *models.GPS {
	t := time.Unix(int64(in.GetTimestamp()), 0)
	long := float64(in.GetLongitude()) / models.LocationAccuracy
	lat := float64(in.GetLatitude()) / models.LocationAccuracy
	gps := &models.GPS{
		Timestamp:      &t,
		Longitude:      long,
		Latitude:       lat,
		Altitude:       float64(in.GetAltitude()) / models.AltitudeAccuracy,
		Temperature:    float64(in.GetTemperature()) / models.TemperatureAccuracy,
		Humidity:       in.GetHumidity(),
		Light:          in.GetLight(),
		Pressure:       in.GetPressure(),
		Dimension:      in.GetDimension(),
		Viewstar:       in.GetViewStar(),
		UsedStar:       in.GetUsedStar(),
		BatteryVoltage: float64(in.GetBatteryVoltage()) / models.VoltageAccuracy,
		Horizontal:     float64(in.GetHorizontal()) / models.HorizontalAccuracy,
		Vertical:       float64(in.GetVertical()) / models.VerticalAccuracy,
		Speed:          float64(in.GetSpeed()) / models.SpeedAccuracy,
		Course:         float64(in.GetCourse()) / models.CourseAccuracy,
		SignalStrength: in.GetSignalStrength(),
		FixTime: in.GetFixTime(),
	}
	if long > 190 {
		return gps
	}

	gps.Loc = models.NewPoint([]float64{long, lat})
	return gps
}


