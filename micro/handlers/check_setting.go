package handlers

import (
	"druid/cattle/models"
	"druid/cattle/micro/common"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"

	"time"
	"fmt"
	"bytes"
	"encoding/gob"
)

func CheckSetting() {
	logger := common.GetLogger(&logrus.Fields{"method": "check_setting"})

	db, err := common.ConnectDB(viper.GetString("database"))
	if err != nil {
		logger.Error("Connect to db failed.", err)
		return
	}

	ch := make(chan []byte, models.MaxChannel)
	nr, err := models.NewNSQReader(models.GetTopicDevice(), models.GetChannelCheckSetting(), ch, models.StringSplit(viper.GetString("nsq.nsqlookupd.address"), ", "))
	if err != nil {
		logger.Error("Init nsq reader failed.", err)
		return
	}
	defer nr.Close()

	devices := make(map[bson.ObjectId]*(time.Time), models.MaxChannel / 10)

	timeout := 1 * time.Minute
	for {
		select {
			case data := <- ch:
				if len(data) == 0 {
					continue
				}

				buff := bytes.NewBuffer(data)
				dec := gob.NewDecoder(buff)
				device := models.Device{}
				err = dec.Decode(&device)
				if err != nil {
					logger.Error("Decode device failed:", err)
					continue
				}
				if devices[device.ID] != nil {
					if devices[device.ID].Before(*(device.UpdatedAt)) {
						devices[device.ID] = device.UpdatedAt
					}
				} else {
					devices[device.ID] = device.UpdatedAt
				}

				if time.Now().Sub(*(devices[device.ID])) > timeout {
					if err := checkDownloaded(db.Copy(), device.ID); err != nil {
						logger.Error("Check downloaded failed.", err)
					}
					delete(devices, device.ID)
				}
			case <- time.After(timeout):
				for k, _ := range devices {
					if err := checkDownloaded(db.Copy(), k); err != nil {
						logger.Error("Check downloaded failed.", err)
					}
					delete(devices, k)
				}
		}
	}
}

func checkDownloaded(db *mgo.Session, deviceID bson.ObjectId) error {
	defer db.Close()

	device, err := models.FindDeviceByID(db, deviceID, nil)
	if err != nil {
		return models.Error("Find device by id failed:", err)
	}

	s, err := models.FindSettingByDeviceID(db, device.ID)
	if err != nil {
		return models.Error("Find setting by deviceid failed:", err)
	} 
	
	if s.DownloadedAt == nil {
		fmt.Printf("device: %d setting download\n", device.Mark)

		now := time.Now()
		s.DownloadedAt = &now

		if err := s.MarkDownload(db); err != nil {
			return models.Error("Mark setting downloaded failed.", err)
		}
		if err := s.SaveHistory(db); err != nil {
			return models.Error("Save setting history failed.", err)
		}
	}

	if s.FirmwareID != "" && device.FirmwareVersion == s.FirmwareVersion {
		fmt.Printf("device: %d clean firmware\n", device.Mark)

		if err := s.SaveFirmwareHistory(db); err != nil {
			return models.Error("Save firmware history failed.", err)
		}

		if err = models.ClearSettingFirmware(db, deviceID); err != nil {
			return models.Error("Clean Setting firmware id failed.", err)
		}
	}

	return nil
}


