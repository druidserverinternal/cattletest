package handlers

import (
	"druid/cattle/models"
	"druid/cattle/micro/common"

	"golang.org/x/net/context"

	"gopkg.in/mgo.v2"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/0987363/google-maps-services-go"

	"time"
	"bytes"
	"encoding/gob"
)

func Elevation() {
	logger := common.GetLogger(&logrus.Fields{"method": "elevation"})
	gpss := []*models.GPS{}
	client := common.ConnectGoogle()

	db, err := common.ConnectDB(viper.GetString("database"))
	if err != nil {
		logger.Error("Connect to db failed.", err)
		return
	}


	ch := make(chan []byte, models.MaxChannel * 10)
	_, err = models.NewNSQReader(models.GetTopicGPS(), models.GetChannelElevation(), ch, models.StringSplit(viper.GetString("nsq.nsqlookupd.address"), ", "))
	if err != nil {
		logger.Error("Init nsq read failed.", err)
		return
	}

	for {
		stop := time.After(5 * time.Second)
		select {
			case data := <- ch:
				if len(data) == 0 {
					continue
				}

				buff := bytes.NewBuffer(data)
				dec := gob.NewDecoder(buff)
				gps := models.GPS{}
				err = dec.Decode(&gps)
				if err != nil {
					logger.Error("Decode gps failed:", err)
					continue
				}
				if gps.Longitude > 191 {
					continue
				}

				gpss = append(gpss, &gps)
				if len(gpss) >= 500 {
//					logger.Info("Start request elevation, length:", len(gpss))
					if err := requestElevation(db.Copy(), client, gpss); err != nil {
						logger.Error("request elevation failed.", err)
					}
					gpss = gpss[:0]
				}
			case <- stop:
				if len(gpss) > 0 {
//					logger.Info("Start request elevation, length:", len(gpss))
					if err := requestElevation(db.Copy(), client, gpss); err != nil {
						logger.Error("request elevation failed.", err)
					}
					gpss = gpss[:0]
				}
		}
	}
}

func requestElevation(db *mgo.Session, client *maps.Client, gpss []*models.GPS) error {
	defer db.Close()

	locations := []maps.LatLng{}
	for _, gps := range gpss {
		locations = append(locations, maps.LatLng{gps.Latitude, gps.Longitude})
	}

    request := &maps.ElevationRequest {
        Locations  :   locations,
    }

	ctx, cancel := context.WithTimeout(context.Background(), time.Second * 10)
	defer cancel()

	resp, err := client.Elevation(ctx, request)
	if err != nil {
		return models.Error("Get elevation failed.", err, resp)
	}
	if len(resp) != len(gpss) {
		return models.Error("Result length failed.")
	}
	for i, gps := range gpss {
		gps.GroundAltitude = resp[i].Elevation
		gps.RelativeAltitude = gps.Altitude - gps.GroundAltitude
		if gps.ID != "" {
			if err := gps.UpdateElevationByID(db); err != nil {
				return models.Error("update gps elevation by id failed:", err, *gps)
			}
		} else {
			if err := gps.UpdateElevation(db); err != nil {
				return models.Error("update gps elevation failed:", err)
			}
		}
	}
	return nil
}

