package handlers

import (
	"druid/cattle/models"
	"druid/cattle/micro/common"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"

	"time"
	"bytes"
	"encoding/gob"
//	"fmt"
)

func CheckUpload() {
	logger := common.GetLogger(&logrus.Fields{"method": "check_upload"})

	db, err := common.ConnectDB(viper.GetString("database"))
	if err != nil {
		logger.Error("Connect to db failed.", err)
		return
	}

	ch := make(chan []byte, models.MaxChannel)
	nr, err := models.NewNSQReader(models.GetTopicDevice(), models.GetChannelCheckUpload(), ch, models.StringSplit(viper.GetString("nsq.nsqlookupd.address"), ", "))
	if err != nil {
		logger.Error("Init nsq reader failed.", err)
		return
	}
	defer nr.Close()

	devices := make(map[bson.ObjectId]*(time.Time), models.MaxChannel / 10)

	timeout := 1 * time.Minute
	for {
		select {
			case data := <- ch:
				if len(data) == 0 {
					continue
				}

				buff := bytes.NewBuffer(data)
				dec := gob.NewDecoder(buff)
				device := models.Device{}
				err = dec.Decode(&device)
				if err != nil {
					logger.Error("Decode device failed:", err)
					continue
				}
				if devices[device.ID] != nil {
					if devices[device.ID].Before(*(device.UpdatedAt)) {
						devices[device.ID] = device.UpdatedAt
					}
				} else {
					devices[device.ID] = device.UpdatedAt
				}

				if time.Now().Sub(*(devices[device.ID])) > timeout {
					logger.Debug("Check device.", device.ID)
					if err := checkFinish(db.Copy(), device.ID); err != nil {
						logger.Error("Check upload failed.", err)
					}
					delete(devices, device.ID)
				}
			case <- time.After(timeout):
				for k, _ := range devices {
					logger.Debug("Check device.", k)
					if err := checkFinish(db.Copy(), k); err != nil {
						logger.Error("Check upload failed.", err)
					}
					delete(devices, k)
				}
		}
	}
}

func checkFinish(db *mgo.Session, deviceID bson.ObjectId) error {
	defer db.Close()

	device, err := models.FindDeviceByID(db, deviceID, nil)
	if err != nil {
		return models.Error("Find device by id failed:", err)
	}

	now := time.Now()

	m := bson.M{
		"today_beh" : 0,
		"today_beh2" : 0,
		"today_gps" : 0,
	}

	mm := bson.M{"device_id": deviceID, "timestamp": bson.M{"$lt": now.AddDate(2, 0, 0)}}
	beh2, err := models.FindLatestBehavior2ByFilter(db, mm)
	if err != nil {
		return models.Error("Find latest behavior2 failed:", err)
	}
	if beh2 != nil {
		if beh2.ID != device.Behavior2ID {
			count, err := models.CountBehavior2ByFilter(db, mm)
			if err != nil {
				return models.Error("Count behavior2 failed:", err)
			}
			m["total_behavior2"] = count
			m["behavior2_id"] = beh2.ID
			m["last_beh2_timestamp"] = beh2.Timestamp
		}

		if models.IsCurrentFinish(beh2.Timestamp) {
			m["today_beh2"] = 1
		}
	}

	gps, err := models.FindLatestGPSByFilter(db, bson.M{"device_id": deviceID, "timestamp": bson.M{"$lt": now.AddDate(2, 0, 0)}})
	if err != nil {
		return models.Error("Find latest gps failed:", err)
	}
	if gps != nil {
		if gps.ID != device.GPSID {
			count, err := models.CountGPSByFilter(db, mm)
			if err != nil {
				return models.Error("Count gps failed:", err)
			}
			m["total_gps"] = count
			m["gps_id"] = gps.ID

			m["timestamp"] = gps.Timestamp
			m["temperature"] = gps.Temperature
			m["signal_strength"] = gps.SignalStrength
			m["last_gps_timestamp"] = gps.Timestamp
		}

		if gps.Loc != nil {
			m["longitude"] = gps.Longitude
			m["latitude"] = gps.Latitude
			m["loc"] = models.NewPoint([]float64{ gps.Longitude, gps.Latitude })
			m["point_location"] = gps.PointLocation
			m["location_timestamp"] = gps.Timestamp
		} else {
			mm["loc"] = bson.M{"$ne": nil}
			g, err := models.FindLatestGPSByFilter(db, mm)
			if err != nil {
				return models.Error("Find latest valid gps failed:", err)
			}
			if g != nil {
				m["longitude"] = g.Longitude
				m["latitude"] = g.Latitude
				m["loc"] = models.NewPoint([]float64{g.Longitude, g.Latitude})
				m["point_location"] = gps.PointLocation
				m["location_timestamp"] = gps.Timestamp
			}
		}

		if models.IsCurrentFinish(gps.Timestamp) {
			m["today_gps"] = 1
		}
	}

	if err := device.UpdateDataStatus(db, m); err != nil {
		return models.Error("Update device data status failed:", err)
	}

	if err = models.SendMessageToNSQD(device, models.GetTopicStatus()); err != nil {
		return models.Error("Send device to mq failed:", err)
	}

	return nil
}

