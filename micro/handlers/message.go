package handlers

import (
	"druid/cattle/models"
	"druid/cattle/micro/common"


	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"

//	"time"
	"bytes"
	"fmt"
	"encoding/gob"
)

func Message() {
	logger := common.GetLogger(&logrus.Fields{"method": "message"})

	ch := make(chan []byte, models.MaxChannel)
	nr, err := models.NewNSQReader(models.GetTopicMessage(), models.GetChannelJpush(), ch, models.StringSplit(viper.GetString("nsq.nsqlookupd.address"), ", "))
	if err != nil {
		logger.Error("Init nsq reader failed.", err)
		return
	}
	defer nr.Close()

	jp := models.NewJPush(viper.GetString("jpush.secret"), viper.GetString("jpush.appkey"))
	if jp == nil {
		logger.Error("Init jpush failed.")
		return
	}

	logger.Info("Start message service.")
	for {
		select {
		case data := <- ch:
			if len(data) == 0 {
				continue
			}

			buff := bytes.NewBuffer(data)
			dec := gob.NewDecoder(buff)
			msg := models.Message{}
			err = dec.Decode(&msg)
			if err != nil {
				logger.Error("Decode message failed:", err)
				continue
			}

			if err = sendMessage(&msg, jp); err != nil {
				logger.Error("Send message failed:", err)
			}
		}
	}
}

func sendMessage(msg *models.Message, jp *models.JPush) error {
	if len (msg.Dsts) == 0 {
		return nil
	}

	ss := models.ConvertObjectIdToString(msg.Dsts)
	release := false
	if viper.GetString("release") == "release" {
		release = true
	}

	fmt.Println("Start send message:", msg)
	if err := jp.SendCN(msg.MsgCN, ss, release); err != nil {
		return models.Error("Send cn message failed:", err)
	}
	if err := jp.Send(msg.Msg, ss, release); err != nil {
		return models.Error("Send message failed:", err)
	}

	return nil
}

