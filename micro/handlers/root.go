package handlers

import (
	"gopkg.in/mgo.v2/bson"

	"encoding/gob"
)


func init() {
	gob.Register(bson.NewObjectId())
}

