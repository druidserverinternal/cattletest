package server

import (
	"druid/cattle/micro/common"

	"github.com/sirupsen/logrus"

	"time"
	"runtime/debug"
)

var Run func()

// Server is main loop service
func Server() {
	common.LoggerInit()

	for {
		run()
	}
}

func run() {
	logger := common.GetLogger(&logrus.Fields{})
	defer func() {
		if err := recover(); err != nil {
			logger.Errorf("%s\n%s", err, string(debug.Stack()))
		}
	}()

	Run()
	time.Sleep(time.Second)
}
