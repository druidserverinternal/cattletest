package common

import (
	"github.com/bshuster-repo/logrus-logstash-hook"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"

	"druid/cattle/models"

	"net"
	"os"
	"time"
)

var logger *logrus.Logger

// InitLogger init a logger
func LoggerInit() *logrus.Logger {
	logger = logrus.New()
	host, _ := os.Hostname()

	logger.Level = models.ConvertLevel(viper.GetString("log.level"))
	logger.Formatter = &logrus.TextFormatter{FullTimestamp: true, TimestampFormat: time.RFC3339Nano}

	if viper.GetString("log.dst") != "" {
		conn, err := net.Dial("udp", viper.GetString("log.dst"))
		if err != nil {
			logger.Fatal(err)
		}

		hook := logrustash.New(conn, logrustash.DefaultFormatter(logrus.Fields{
			"type":     "cattle",
			"hostname": host,
			"service":  "micro",
			"release":  viper.GetString("release"),
		}))
		if err != nil {
			logrus.Fatal(err)
		}
		logger.Hooks.Add(hook)
	}

	return logger
}

func GetLogger(fields *logrus.Fields) *logrus.Entry {
	return logger.WithFields(*fields)
}
