package common

import (
	log "github.com/sirupsen/logrus"
	
	"github.com/0987363/google-maps-services-go"
	"github.com/spf13/viper"
)

const GoogleMapConnectionKey = "GoogleMapConnection"

var gClient *maps.Client

var googleRateLimit = 50

func ConnectGoogle() *maps.Client {
	secret := viper.GetString("google.maps.secret")
	if secret == "" {
		log.Error("google secret is invalid.")
		return nil
	}
	c, err := maps.NewClient(maps.WithAPIKey(secret), maps.WithRateLimit(googleRateLimit))
	if err != nil {
		log.Error("Create google map connection error: %s", err)
		return nil
	}
	address := viper.GetString("google.maps.address")
	if address != "" {
		maps.GeocodeHost(address)
		maps.ElevationHost(address)
	} else {
		log.Warning("Not found proxy maps address")
	}

	log.Info("Create google map connection success.")
	gClient = c
	return gClient
}

func GetGoogleMapConnection() *maps.Client {
	if gClient == nil {
		return ConnectGoogle()
	}

	return gClient
}

