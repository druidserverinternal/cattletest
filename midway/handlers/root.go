package handlers

import (
	"druid/cattle/midway/middleware"

	"druid/cattle/midway/handlers/company"
	"druid/cattle/midway/handlers/device"
	"druid/cattle/midway/handlers/sim"

	"github.com/gin-gonic/gin"
	//	"github.com/gin-gonic/contrib/gzip"
)

// RootMux is the root handler for the cattle server
var RootMux = gin.New()

func init() {
	RootMux.Use(gin.Logger())
	//	RootMux.Use(gzip.Gzip(gzip.BestSpeed))

	RootMux.Use(middleware.RequestID())
	RootMux.Use(middleware.Logger())
	RootMux.Use(gin.Recovery())
	RootMux.Use(middleware.DBConnector())
	//	RootMux.Use(middleware.PushConnection())
	//	RootMux.Use(middleware.Etcd())

	v1Mux := RootMux.Group("/v1")
	{
		companyMux := v1Mux.Group("/company")
		{
			companyMux.GET("/:id", company.Get)
			companyMux.GET("/", company.List)
		}

		deviceMux := v1Mux.Group("/device")
		{
			deviceMux.GET("/company/:id", device.ListCompany)
			deviceMux.GET("/", device.List)
		}

		simMux := v1Mux.Group("/sim")
		{
			simMux.GET("/number/:id", sim.GetSIMByNumber)
			simMux.GET("/imsi/:id", sim.GetSIMByIMSI)
			simMux.GET("/iccid/:id", sim.GetSIMByICCID)
		}
	}
}
