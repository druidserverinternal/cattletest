package sim

import (
	"errors"
	"net/http"

	//"gopkg.in/mgo.v2/bson"

	"druid/cattle/midway/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

func GetSIMByNumber(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)
	id := c.Param("id")

	sim, err := models.FindSIMByNumber(db, id)
	if err != nil || sim == nil {
		logger.Warning("The SIM by Number is invalid:", id)
		c.AbortWithError(http.StatusBadRequest, errors.New("The SIM is invalid."))
		return
	}

	c.JSON(http.StatusOK, sim)
}

func GetSIMByIMSI(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)
	id := c.Param("id")

	sim, err := models.FindSIMByIMSI(db, id)
	if err != nil || sim == nil {
		logger.Warning("The SIM by IMSI is invalid:", id)
		c.AbortWithError(http.StatusBadRequest, errors.New("The SIM is invalid."))
		return
	}

	c.JSON(http.StatusOK, sim)
}

func GetSIMByICCID(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)
	id := c.Param("id")

	sim, err := models.FindSIMByICCID(db, id)
	if err != nil || sim == nil {
		logger.Warning("The SIM by ICCID is invalid:", id)
		c.AbortWithError(http.StatusBadRequest, errors.New("The SIM is invalid."))
		return
	}

	c.JSON(http.StatusOK, sim)
}
