package company

import (
	"net/http"
	"errors"
	"strconv"

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

// List companies list
func List(c *gin.Context) {
	db := middleware.GetDB(c)
//	logger := middleware.GetLogger(c)

	companies, err := models.ListCompanies(db, models.ParseRequestParameter(c))
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, errors.New("List companies failed." + err.Error()))
		return
	}

	for i, company := range companies {
		companies[i].DeviceCount, _ = models.CountDeviceByCompanyID(db, company.ID)
		companies[i].UserCount, _ = models.UserCountByCompanyID(db, company.ID)
	}

	count := 0
	if len(companies) > 0 {
		count, _ = models.CompanyCount(db)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, companies)
}
