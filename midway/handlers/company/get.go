package company

import (
	"gopkg.in/mgo.v2/bson"
	"net/http"
	"errors"
	"fmt"

	"druid/cattle/midway/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

// Get himself company infomation
func Get(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	id := c.Param("id")
	if !bson.IsObjectIdHex(id) {
		c.AbortWithError(http.StatusBadRequest, errors.New(fmt.Sprintf("Company id:%s is invalid", id)))
		return
	}

	company := models.FindCompanyByID(db, bson.ObjectIdHex(id))
	if company == nil {
		logger.Warning("The company id is invalid:", id)
		c.AbortWithError(http.StatusBadRequest, errors.New("The company is invalid."))
		return
	}
	company.DeviceCount, _ = models.CountDeviceByCompanyID(db, company.ID)
	company.UserCount, _ = models.UserCountByCompanyID(db, company.ID)

	c.JSON(http.StatusOK, company)
}
