package device

import (
	"net/http"
	"strconv"
	"fmt"
	"errors"

	"gopkg.in/mgo.v2/bson"

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

// ListCompany  by company
func ListCompany(c *gin.Context) {
	db := middleware.GetDB(c)

	id := c.Param("id")
	if !bson.IsObjectIdHex(id) {
		c.AbortWithError(http.StatusBadRequest, errors.New(fmt.Sprintf("Company id:%s is invalid", id)))
		return
	}

	result, err := models.ListDeviceByCompanyID(db, bson.ObjectIdHex(id), models.ParseRequestParameter(c))
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, errors.New("List devices by companyid failed." + err.Error()))
		return
	}

	count := 0
	if len(result) > 0 {
		count, err = models.CountDeviceByCompanyID(db, bson.ObjectIdHex(id))
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, result)
}
