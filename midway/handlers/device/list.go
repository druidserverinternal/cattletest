package device

import (
	"net/http"
	"strconv"
//	"errors"

	"druid/cattle/midway/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

// List all devices
func List(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	rp := models.ParseRequestParameter(c)

	result, err := models.ListDevice(db, rp)
	if err != nil {
		logger.Error("List device by filter failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List device by filter failed.", err))
		return
	}

	count := 0
	if len(result) > 0 {
		count, err = models.CountDeviceByFilter(db, nil)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, result)
}

