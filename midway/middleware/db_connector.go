package middleware

import (
//	"net/http"

//	"golang.org/x/net/context"

	"gopkg.in/mgo.v2"

	"github.com/gin-gonic/gin"
	"druid/cattle/models"
)

const dbKey = "Db"


// DBConnector middleware stores a mongo db handler in context
func DBConnector() gin.HandlerFunc {
	return func(c *gin.Context) {
		d := models.GetMongoDB()
		defer d.Close()

		c.Set(dbKey, d)
		c.Next()
	}
}

// GetDB returns the db pointer from context or nil if db has not been connected
func GetDB(c *gin.Context) (*mgo.Session) {
	if db, ok := c.Get(dbKey); ok {
		return db.(*mgo.Session)
	}

	return nil
}
