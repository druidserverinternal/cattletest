package middleware

import (
	"druid/cattle/models"

	"errors"
	"net/http"

	"github.com/spf13/viper"

	"gopkg.in/mgo.v2/bson"

	"github.com/gin-gonic/gin"
)

// AuthenticationHeader is the http header for authentication token
const AuthenticationHeader = "X-Druid-Authentication"

const userIDKey = "UserID"

func AdminAuthenticator() gin.HandlerFunc {
	return func(c *gin.Context) {
		logger := GetLogger(c)
		db := GetDB(c)

		ah := c.Request.Header.Get(AuthenticationHeader)
		if ah == "" {
			ah = c.Query(AuthenticationHeader)
		}
		if ah == "" {
			logger.Error("Authentication token is nil.")
			writeError(c, http.StatusUnauthorized, "Token is nil.")
			return
		}

		token, err := models.ParseToken(ah)
		if err != nil {
			if logger != nil {
				logger.Infof(
					"Failed to parse authentication token: %s due to: %s.",
					ah,
					err,
				)
			}
			writeError(c, http.StatusUnauthorized, "Token is invalid.")
			return
		}

		if !token.Validate(viper.GetString("authentication.secret")) {
			if logger != nil {
				logger.Errorf("Authentication token: %s is not valid.", ah)
			}
			writeError(c, http.StatusUnauthorized, "Token is in valid.")
			return
		}

		user := models.FindUserByID(db, bson.ObjectIdHex(token.UserID))
		if user == nil {
			logger.Error("User is invalid:", token.UserID)
			writeError(c, http.StatusUnauthorized, "User is invalid.")
			return
		}

		if user.Role != models.RoleAdmin {
			logger.Error("You don't have permission to auth.", user)
			writeError(c, http.StatusUnauthorized, "You don't have permission to auth")
			return
		}

		if user.Token != ah {
			logger.Errorf("Authentication token: %s is expiry. Relogin please", ah)
			writeError(c, http.StatusUnauthorized, "Token is expiry.")
			return
		}

		c.Set(userIDKey, token.UserID)
		c.Next()
	}
}

func Authenticator() gin.HandlerFunc {
	return func(c *gin.Context) {
		logger := GetLogger(c)
		db := GetDB(c)

		ah := c.Request.Header.Get(AuthenticationHeader)
		if ah == "" {
			ah = c.Query(AuthenticationHeader)
		}
		if ah == "" {
			logger.Error("Authentication token is nil.")
			writeError(c, http.StatusUnauthorized, "Token is nil.")
			return
		}

		token, err := models.ParseToken(ah)
		if err != nil {
			if logger != nil {
				logger.Infof(
					"Failed to parse authentication token: %s due to: %s.",
					ah,
					err,
				)
			}
			writeError(c, http.StatusUnauthorized, "Token is invalid.")
			return
		}

		if !token.Validate(viper.GetString("authentication.secret")) {
			if logger != nil {
				logger.Errorf("Authentication token: %s is not valid.", ah)
			}
			writeError(c, http.StatusUnauthorized, "Token is in valid.")
			return
		}

		user := models.FindUserByID(db, bson.ObjectIdHex(token.UserID))
		if user == nil {
			logger.Error("User is invalid:", token.UserID)
			writeError(c, http.StatusUnauthorized, "User is invalid.")
			return
		}

		if user.Role != models.RoleUser {
			logger.Error("You don't have permission to auth.", user)
			writeError(c, http.StatusUnauthorized, "You don't have permission to auth")
			return
		}

		if user.Token != ah {
			logger.Errorf("Authentication token: %s is expiry. Relogin please", ah)
			writeError(c, http.StatusUnauthorized, "Token is expiry.")
			return
		}



		c.Set(userIDKey, token.UserID)
		c.Next()
	}
}

// GetUserID returns the signed-in user id or 0 if not signed in
func GetUserID(c *gin.Context) string {
	if id, ok := c.Get(userIDKey); ok {
		return id.(string)
	}

	return ""
}

func writeError(c *gin.Context, code int, err string) {
	c.AbortWithError(code, errors.New(err))
}
