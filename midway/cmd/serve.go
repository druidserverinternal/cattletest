package cmd

import (
	"druid/cattle/models"
	"druid/public"
	"druid/cattle/midway/handlers"
	"druid/cattle/midway/middleware"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

const defaultAddress = ":8080"

// serveCmd represents the serve command
var serveCmd = &cobra.Command{
	Use:    "serve",
	Short:  "Start cattle server",
	PreRun: LoadConfiguration,
	Run:    serve,
}

func init() {
	RootCmd.AddCommand(serveCmd)

	serveCmd.Flags().StringP(
		"address",
		"a",
		defaultAddress,
		"Address the server binds to",
	)
	viper.BindPFlag("address", serveCmd.Flags().Lookup("address"))
}


func serve(cmd *cobra.Command, args []string) {
	middleware.LoggerConnInit()

	// Try to connect to the database
	if err := models.ConnectMongoDB( viper.GetString("database")); err != nil {
		log.Fatalln("connect to db failed: ", err)
	}
	
	log.Info("Connect to mongodb success.")

	if err := public.ConnectEtcd(
		viper.GetString("etcd.address"),
		viper.GetString("etcd.ca"),
		viper.GetString("etcd.cert"),
		viper.GetString("etcd.key"),
	); err != nil {
		log.Fatalln("connect to etcd: ", err)
	}
	log.Info("Connect to etcd success.")

	if err := Service(); err != nil {
		log.Fatalln("Register service failed: ", err)
	}
	go models.Signal(DelService)

	address := viper.GetString("address")
	cert := viper.GetString("tls.cert")
	key := viper.GetString("tls.key")

	if cert != "" && key != "" {
		log.Infof("Starting cattle midway tls server on %s.", address)
		handlers.RootMux.RunTLS(address, cert, key)
	} else {
		log.Infof("Starting cattle midway server on %s.", address)
		handlers.RootMux.Run(address)
	}
}
