package cmd

import (
	"druid/cattle/models"

	log "github.com/sirupsen/logrus"
)

func Service() error {
	log.Info("start register service.")
	if err := models.RegService("ListDevice", "/v1/device/"); err != nil {
		return err
	}
	if err := models.RegService("ListDeviceByCompanyID", "/v1/device/company/%s"); err != nil {
		return err
	}

	if err := models.RegService("ListCompany", "/v1/company/"); err != nil {
		return err
	}
	if err := models.RegService("GetCompany", "/v1/company/%s"); err != nil {
		return err
	}
	if err := models.RegService("GetSIMByNumber", "/v1/sim/number/%s"); err != nil {
		return err
	}
	if err := models.RegService("GetSIMByIMSI", "/v1/sim/imsi/%s"); err != nil {
		return err
	}
	if err := models.RegService("GetSIMByICCID", "/v1/sim/iccid/%s"); err != nil {
		return err
	}

	return nil
}

func DelService() error {
	log.Info("start delete service.")
	if err := models.DelService("ListDevice", "/v1/device/"); err != nil {
		return err
	}
	if err := models.DelService("ListDeviceByCompanyID", "/v1/device/company/%s"); err != nil {
		return err
	}

	if err := models.DelService("ListCompany", "/v1/company/"); err != nil {
		return err
	}
	if err := models.DelService("GetCompany", "/v1/company/%s"); err != nil {
		return err
	}
	if err := models.DelService("GetSIMByNumber", "/v1/sim/number/%s"); err != nil {
		return err
	}
	if err := models.DelService("GetSIMByIMSI", "/v1/sim/imsi/%s"); err != nil {
		return err
	}
	if err := models.DelService("GetSIMByICCID", "/v1/sim/iccid/%s"); err != nil {
		return err
	}

	return nil
}
