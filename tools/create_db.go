package main

import (
	"druid/cattle/models"
	//"druid/manager/cmd"
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gopkg.in/mgo.v2"
	//	"gopkg.in/mgo.v2/bson"
)

const defaultDatabase = "127.0.0.1:27017"

var serveCmd = &cobra.Command{
	Use:    "db tool",
	Short:  "clean and create mongodb tables",
	PreRun: loadConfiguration,
}

var configFilePath string

func loadConfiguration(cmd *cobra.Command, args []string) {
	// Load the configuration
	if configFilePath != "" {
		viper.SetConfigFile(configFilePath)
	} else {
		viper.SetConfigName("config")
		viper.AddConfigPath("/etc/cattle")
		viper.AddConfigPath(".")
	}

	if err := viper.ReadInConfig(); err != nil {
		if viper.ConfigFileUsed() == "" {
			fmt.Println("Unable to find configuration file.")
		}

		fmt.Printf("Failed to load %s: %v\n", viper.ConfigFileUsed(), err)
	} else {
		fmt.Printf("Using config file: %s\n", viper.ConfigFileUsed())
	}
}

func init() {
	serveCmd.Flags().StringP(
		"database",
		"d",
		defaultDatabase,
		"mongodb database, default is 127.0.0.1:27017",
	)
	viper.BindPFlag("database", serveCmd.Flags().Lookup("database"))
}

func main() {
	if err := serveCmd.Execute(); err != nil {
		panic(err)
	}

	fmt.Println("database is:", viper.GetString("database"))

	session, err := mgo.Dial(viper.GetString("database"))
	if err != nil {
		panic(err)
	}
	defer session.Close()

	session.SetMode(mgo.Eventual, true)

	// Drop Database
	fmt.Println("start delete db:manager")
	err = session.DB("manager").DropDatabase()
	if err != nil {
		panic(err)
	}

	fmt.Println("start create table devices")
	c := session.DB("manager").C("device")
	index := mgo.Index{
		Key: []string{"device_id"},
		//		Unique:     false,
		//		DropDups:   false,
		Background: true,
		//		Sparse:     true,
	}

	fmt.Println("start create devices index with: ", index.Key)
	err = c.EnsureIndex(index)
	if err != nil {
		panic(err)
	}

	fmt.Println("start create table gps")
	c = session.DB("manager").C("gps")
	index = mgo.Index{
		Key:        []string{"device_id"},
		Background: true,
	}
	fmt.Println("start create gps index with: ", index.Key)
	err = c.EnsureIndex(index)
	if err != nil {
		panic(err)
	}

	fmt.Println("start create table gsensor")
	c = session.DB("manager").C("gsensor")
	index = mgo.Index{
		Key:        []string{"device_id"},
		Background: true,
	}
	fmt.Println("start create gsensor index with: ", index.Key)
	err = c.EnsureIndex(index)
	if err != nil {
		panic(err)
	}
}
