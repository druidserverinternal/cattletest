package main

import (
	"druid/cattle/models"

	"fmt"

//	"gopkg.in/guregu/null.v3"

	"gopkg.in/mgo.v2"

)

func ListDevice(db *mgo.Session) ([]*models.Device, error) {
	c := db.DB(models.DruidDB).C(models.DeviceCollection)
	devices := []*models.Device{}
	return devices, models.MongoList(c, nil, &devices, nil)
}

func exportDevice(session *mgo.Session){
//	c := session.DB(models.DruidDB).C(models.DeviceCollection)
//	c2 := dst.DB(models.DruidDB+"_bk").C(models.DeviceCollection)

	fmt.Println("start select device from old mongodb")
	devices, err := ListDevice(session)
	if err != nil {
		panic(err)
	}

	fmt.Println("start loop check device from mongo, device length:", len(devices))

	for _, device := range devices {
		if device.Mark == 0 {
			continue
		}

		gps, err := models.FindDeviceLatestValidGPS(session, device.ID)
		if err != nil {
			fmt.Println("Find device latest gps failed:", err, device.ID)
			continue
		}
		if gps != nil {
			fmt.Println("Find latest gps :", gps)
			device.LastValidGPS = gps
			device.LastGPS.Mark = device.Mark

			if err := device.UpdateLastGPS(session); err != nil {
				fmt.Println("Update device latest gps failed:", err, device.ID)
				continue
			}
		}
	}
}

func main() {
	dst, err := mgo.Dial("mongodb://root:Ke2BwrxBQFR9GH8k@mongo-1:3717,mongo-2:3717,mongo-3:3717")
	if err != nil {
		panic(err)
	}
	defer dst.Close()

	exportDevice(dst)
}
