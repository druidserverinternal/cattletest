package main

import (
	"crypto/sha256"
	"encoding/hex"
	"golang.org/x/crypto/bcrypt"

	"fmt"
	"os"
)

func main() {
	originalPwd := os.Args[1] + " + druid + " + os.Args[2] + " + heifeng"
	fmt.Println("original password: ", originalPwd)

	password := sha256.Sum256([]byte(originalPwd))

	fmt.Println("password:", hex.EncodeToString(password[:]))

	hashedPassword, _ := bcrypt.GenerateFromPassword(password[:], bcrypt.DefaultCost)
	fmt.Println("hashed password:", hex.EncodeToString(hashedPassword[:]))
	fmt.Println("hashed2 password:", string(hashedPassword))

}
