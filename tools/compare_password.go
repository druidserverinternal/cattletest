package main

import (
	//"druid/manager/cmd"
	//"crypto/md5"
	"crypto/sha256"
	"encoding/hex"

	"fmt"

	"golang.org/x/crypto/bcrypt"
)

func main() {

	originalPwd := "root + " + "druid" + " + root + " + "heifeng"
	fmt.Println("original password: ", originalPwd)

	password := sha256.Sum256([]byte(originalPwd))

	fmt.Printf("password: %x\n", password[:])
	p1 := hex.EncodeToString(password[:])
	fmt.Println(p1)
	fmt.Println(hex.DecodeString(p1))
	fmt.Println(password)

	// Hashing the password with the default cost of 10
	hashedPassword, err := bcrypt.GenerateFromPassword(password[:], bcrypt.DefaultCost)
	if err != nil {
		panic(err)
	}
	fmt.Println("created hash password: ", string(hashedPassword[:]))

	err = bcrypt.CompareHashAndPassword([]byte("$2a$10$LUBKD0faG9aqLYhBrX3saOR8WN0MGKpe2o5hVKITOyFUjzY4VJWpO"), password[:])
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("compare success")
}
