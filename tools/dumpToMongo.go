package main

import (
	"druid/cattle/models"

	"fmt"

//	"gopkg.in/guregu/null.v3"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	log "github.com/sirupsen/logrus"
)

func ListDevice(db *mgo.Session) ([]*models.Device, error) {
	c := db.DB(models.DruidDB).C(models.DeviceCollection)
	devices := []*models.Device{}
	return devices, models.MongoList(c, bson.M{"mark": bson.M{"$ne": nil}, "deleted_at": nil}, &devices, nil)
}


func exportDevice(session *mgo.Session, dst *mgo.Session){
//	c := session.DB(models.DruidDB).C(models.DeviceCollection)
//	c2 := dst.DB(models.DruidDB+"_bk").C(models.DeviceCollection)

	fmt.Println("start select device from old mongodb")
	devices, err := ListDevice(session)
	if err != nil {
		panic(err)
	}

	fmt.Println("start loop check device from mongo")
	qg := models.GetGPSQueue(dst)
	qb := models.GetBehiviorQueue(dst)

	cd := dst.DB(models.DruidDB).C(models.DeviceCollection)
	qd := cd.Bulk()

	cs := dst.DB(models.DruidDB).C(models.SettingCollection)
	qs := cs.Bulk()

	for _, device := range devices {
		gpss, err := models.ListGPSByDeviceID(session, device.ID, nil)
		if err != nil {
			panic("find device gps failed:" + device.UUID)
		}
		for i, gps := range gpss {
			qg.Upsert(bson.M{"_id": gps.ID}, gps)
			if i % 900 == 0 {
				_, err = qg.Run()
				if err != nil {
					log.Error("run gps queue failed.", err)
					panic(err)
				}
				qg = models.GetGPSQueue(dst)
			}
		}
		_, err = qg.Run()
		if err != nil {
			log.Error("run gps queue failed.", err)
			panic(err)
		}


		setting, err := models.FindSettingByDeviceID(session, device.ID)
		if err != nil {
			panic("find device setting failed:" + device.UUID)
		}
		qs.Upsert(bson.M{"_id": setting.ID}, setting)


		behs, err := models.ListBehiviorByDeviceID(session, device.ID, nil)
		if err != nil {
			panic("find device beh failed:" + device.UUID)
		}
		for i, beh := range behs {
			qb.Upsert(bson.M{"_id": beh.ID}, beh)
			if i % 900 == 0 {
				_, err = qb.Run()
				if err != nil {
					log.Error("run beh queue failed.", err)
					panic(err)
				}
				qb = models.GetBehiviorQueue(dst)
			}
		}
		_, err = qb.Run()
		if err != nil {
			log.Error("run beh queue failed.", err)
			panic(err)
		}


		qd.Upsert(bson.M{"_id": device.ID}, device)
	}

	_, err = qs.Run()
	if err != nil {
		log.Error("run setting queue failed.", err)
		panic(err)
	}

	_, err = qd.Run()
	if err != nil {
		log.Error("run Behivior queue failed.", err)
		panic(err)
	}

}



func main() {
	src, err := mgo.Dial("127.0.0.1")
	if err != nil {
		panic(err)
	}
	defer src.Close()

	dst, err := mgo.Dial("mongodb://root:5EuMWkZGl9QF@dds-uf659ac60ff899c42.mongodb.rds.aliyuncs.com:3717")
	if err != nil {
		panic(err)
	}
	defer dst.Close()


	exportDevice(src, dst)
}
