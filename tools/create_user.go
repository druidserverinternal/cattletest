package main

import (
	//"druid/manager/cmd"
	//"crypto/md5"
	"crypto/sha256"
	"druid/cattle/models"

	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"golang.org/x/crypto/bcrypt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	//"time"
)

const defaultDatabase = "127.0.0.1:27017/druid"

var serveCmd = &cobra.Command{
	Use:    "cattle tool",
	Short:  "create cattle user tool",
	PreRun: LoadConfiguration,
	Run:    run,
}

var configFilePath string

// LoadConfiguration read config from dir
func LoadConfiguration(cmd *cobra.Command, args []string) {
	// Load the configuration
	if configFilePath != "" {
		viper.SetConfigFile(configFilePath)
	} else {
		viper.SetConfigName("config")
		viper.AddConfigPath("/etc/cattle")
		viper.AddConfigPath(".")
		viper.AddConfigPath("..")
	}

	if err := viper.ReadInConfig(); err != nil {
		if viper.ConfigFileUsed() == "" {
			fmt.Println("Unable to find configuration file.", err)
		}

		fmt.Printf("Failed to load %s: %v", viper.ConfigFileUsed(), err)
	} else {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}

func init() {
	serveCmd.Flags().StringP(
		"database",
		"d",
		defaultDatabase,
		"mongodb database, default is 127.0.0.1:27017",
	)
	serveCmd.Flags().StringP(
		"name",
		"n",
		"root",
		"super admin user",
	)
	serveCmd.Flags().StringP(
		"password",
		"p",
		"root",
		"super admin user password",
	)
	serveCmd.Flags().StringP(
		"role",
		"r",
		"root",
		"user role",
	)

	viper.BindPFlag("database", serveCmd.Flags().Lookup("database"))
	viper.BindPFlag("name", serveCmd.Flags().Lookup("name"))
	viper.BindPFlag("password", serveCmd.Flags().Lookup("password"))
	viper.BindPFlag("role", serveCmd.Flags().Lookup("role"))
}

func main() {
	if err := serveCmd.Execute(); err != nil {
		panic(err)
	}
}

func run(cmd *cobra.Command, args []string) {
	database := viper.GetString("database")
	fmt.Println("database is:", database)

	session, err := mgo.Dial(database)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	c := session.DB(models.DruidDB).C(models.UserCollection)

	name := viper.GetString("name")
	pwd := viper.GetString("password")

	fmt.Println("start delete old user:", name)
	info, err := c.RemoveAll(bson.M{"username": name})
	if err != nil {
		panic(err)
	}
	fmt.Println("remove user:", info)

	originalPwd := name + " + druid + " + pwd + " + heifeng"
	fmt.Println("original password: ", originalPwd)
	password := sha256.Sum256([]byte(originalPwd))
	fmt.Printf("generated hash password: %x\n", password)

	// Hashing the password with the default cost of 10
	hashedPassword, err := bcrypt.GenerateFromPassword(password[:], bcrypt.DefaultCost)
	if err != nil {
		panic(err)
	}
	fmt.Println("created encryption password: ", string(hashedPassword))
	//	err = bcrypt.CompareHashAndPassword(hashedPassword, password)

	user := &models.User{
		UserName:       name,
		HashedPassword: string(hashedPassword),
		Email:          "0987363@gmail.com",
//		Expiry:         time.Now(),
		Role:           viper.GetString("role"),
		Profile:        &models.UserProfile{10, 8, 1},
//		UpdatedAt:      time.Now(),
	}

	fmt.Println("start create new user:", name)
	err = c.Insert(user)
	if err != nil {
		panic(err)
	}
	fmt.Println("create new user success")
}
