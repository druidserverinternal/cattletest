#!/bin/bash

set -e

host="$1"
port="$2"
>&2 echo $1, $2
shift
shift
cmd="$@"

until nc -z $host $port; do
    >&2 echo "Mongodb is unavailable - sleeping"
    sleep 1
done

>&2 echo "Mongodb is up - executing command"
exec $cmd
