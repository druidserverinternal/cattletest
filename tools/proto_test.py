#!/usr/local/bin/python

import socket, sys, struct, time, rfc3339


sys.path.append('../bbco/protocol')

import Setting_pb2, Define_pb2, Status_pb2, Sample_pb2, GPS_pb2, Register_pb2

def getToken():
    return 123456

def registerRequest():
    register = Register_pb2.RegisterReq()
    register.Iden.UUID = '2E001C000251343432353932'
    register.Iden.Token = getToken()

    register.IMSI = '23423423424'
    register.Mac = 'aa:aa:aa:aa:aa'
    register.DeviceType = 1
    register.HardwareVersion = 1
    register.FirewareVersion = 1

    body = register.SerializeToString()
    header = struct.pack("2i",  Define_pb2.TypeRegisterReq, len(body))

    return header + body

def settingRequest():
    setting = Setting_pb2.SettingReq()
    setting.Iden.UUID = '2E001C000251343432353932'
    setting.Iden.Token = getToken()

    body = setting.SerializeToString()
    header = struct.pack("2i",  Define_pb2.TypeSettingReq, len(body))

    return header + body

def statusRequest():
    status = Status_pb2.StatusReq()
    status.Iden.UUID = '2E001C000251343432353932'
    status.Iden.Token = getToken()

    si = status.StatusInfo.add()
    si.StatusType = 1
    si.StatusValue = 1
    si.Timestamp = int(time.time())

    body = status.SerializeToString()
    header = struct.pack("2i",  Define_pb2.TypeStatusReq, len(body))

    return header + body

def gpsRequest():
    gps = GPS_pb2.GPSReq()
    gps.Iden.UUID = '2E001C000251343432353932'
    gps.Iden.Token = getToken()

    si = gps.GPSInfo.add()
    si.Timestamp = int(time.time())
    si.Longitude = 222
    si.Latitude = 333

    body = gps.SerializeToString()
    header = struct.pack("2i",  Define_pb2.TypeGPSReq, len(body))

    return header + body


def settingResponse(data):
    setting = Setting_pb2.SettingRsp()
    setting.ParseFromString(data)
    print setting

def sampleResponse(data):
    sample = Sample_pb2.SampleRsp()
    sample.ParseFromString(data)
    print sample


switch={
    Define_pb2.TypeSettingRsp: settingResponse,
    Define_pb2.TypeStatusRsp: sampleResponse,
    Define_pb2.TypeGPSRsp: sampleResponse,
    Define_pb2.TypeRegisterRsp: sampleResponse,
}

def parseResponse(sock):
    header = sock.recv(8)
    t, l = struct.unpack("2i", header)

    data = sock.recv(l)
    print 'recv header:', t, l
    switch[t](data)


sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  
sock.connect(('127.0.0.1', 9010))  

print "start send register request"
sock.send(registerRequest())
parseResponse(sock)

print "start send setting request"
sock.send(settingRequest())
parseResponse(sock)

print "start send status request"
sock.send(statusRequest())
parseResponse(sock)

print "start send gps request"
sock.send(gpsRequest())
parseResponse(sock)



sock.close()


