#!/usr/bin/perl

use strict;
use warnings;

use Cwd;

my $release = $ARGV[0];
my $target = $ARGV[1];
my $service = $ARGV[2];

my $type = "cattle";

if (@ARGV < 2) {
    die "argument count is invalid:", @ARGV;
}

unless (defined $target) {
    die "target is invalid";
}

unless (defined $release) {
    $release = "beta";
}

chdir $target;
print "entering ".getcwd."\n";

my $commit=`git rev-parse --short HEAD 2>/dev/null`;
my $buildDate = `date -u +%Y-%m-%dT%H:%M:%SZ`;
my $version = `git tag| tail -1`;
chomp($commit);
chomp($buildDate);
chomp($version);

print "start build $release $target $version $buildDate $commit\n";
if (defined $service) {
    buildService($service)
} else {
    if ($target eq "micro") {
        my $name;
        my @dir;
        opendir (DIR, "apps") or die "can't open the app directory!";
        @dir = readdir DIR;
        foreach $name (@dir) {
            if ($name eq "." or $name eq "..") {
                next;
            }
            if ($name =~ /(^[^\.]+)\.go$/) {
                buildService($1)
            }
        }
    } else {
        build()
    }
}


if ($release eq "release") {
    if ($target ne "micro" && $target ne "midway") {
#        upload("mongo-1");
    }
    upload("mongo-2");
} elsif ($release eq "test") {
    upload("test.druidtech.net");
} else {
    upload("coolhei.com");
}

sub build {
    my $m = "GOARCH=amd64 GOOS=linux go build -v -ldflags \"-X main.BuildVersion=$version -X main.BuildCommit=$commit -X main.BuildDate=$buildDate\"";
    print "$m\n";
    my $exit_code = system $m;
    if ($exit_code != 0) {
        print "go build $target failed: $exit_code\n";
        exit;
    }
}

sub buildService {
    my $m = "GOARCH=amd64 GOOS=linux go build -v -ldflags \"-X main.BuildVersion=$version -X main.BuildCommit=$commit -X main.BuildDate=$buildDate\" -o build/$_[0] apps/$_[0].go";
    print "$m\n";
    my $exit_code = system $m;
    if ($exit_code != 0) {
        print "make release: $target - $_[0] failed: $exit_code\n";
        exit;
    }
}


sub upload {
    my $cmd = scp($_[0]);
    print "run cmd: $cmd\n";
    my $code = system $cmd;
    if ($code != 0) {
        print "scp $target failed: $code $cmd\n";
        exit;
    }

    $cmd = restart($_[0]);
    print "run cmd: $cmd\n";
    $code = system $cmd;
    if ($code != 0) {
        print "restart $target failed: $code $cmd\n";
        exit;
    }
    
    return
    $cmd = rm($_[0]);
    print "rm cmd: $cmd\n";
    $code = system $cmd;
    if ($code != 0) {
        print "rm $target failed: $code $cmd\n";
        exit;
    }
    return;
}

sub rm {
    if (defined $service) {
        return "rm build/$service ";
    }
    if ($target eq "micro") {
        return "rm  build/* ";
    }
    return "rm $target ";
}

sub scp {
    if (defined $service) {
        return "scp -P222 build/$service root\@$_[0]:/home/druid/$type/bin/bk/";
    }
    if ($target eq "micro") {
        return "scp -P222 build/* root\@$_[0]:/home/druid/$type/bin/bk/";
    }
    return "scp -P222 $target root\@$_[0]:/home/druid/$type/bin/bk/";
}

sub restart {
    if (defined $service) {
        return "ssh -p222 root\@$_[0] 2>&1 << eeooff
        systemctl stop $type-$target-$service.service
        mv /home/druid/$type/bin/bk/$service /home/druid/$type/bin/$target/
        systemctl start $type-$target-$service.service
        exit
        eeooff";
    }

    if ($target eq "micro") {
        my $cmd = "ssh -p222 root\@$_[0] 2>&1 << eeooff\n";

        my $name;
        my @dir;
        opendir (DIR, "build") or die "can't open the directory!";
        @dir = readdir DIR;
        foreach $name (@dir) {
            if ($name eq "." or $name eq "..") {
                next;
            }
            $cmd = $cmd."systemctl stop $type-$target-$name.service\n";
            $cmd = $cmd."mv /home/druid/$type/bin/bk/$name /home/druid/$type/bin/$target/\n";
            $cmd = $cmd."systemctl start $type-$target-$name.service\n";
        }

        return $cmd." exit\n eeooff";
    }

    if ($target eq "backman") {
        return "ssh -p222 root\@$_[0] 2>&1 << eeooff
        mv /home/druid/$type/bin/bk/$target /home/druid/$type/bin/$target
        exit
        eeooff";
    }

    return "ssh -p222 root\@$_[0] 2>&1 << eeooff
    systemctl stop $type-$target.service
    mv /home/druid/$type/bin/bk/$target /home/druid/$type/bin/$target
    systemctl start $type-$target.service
    exit
    eeooff";
}
