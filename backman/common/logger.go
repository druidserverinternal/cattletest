package common

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/bshuster-repo/logrus-logstash-hook"
	"druid/cattle/models"

	"os"
	"net"
	"time"
)

//var logger *logrus.Logger

var logConn net.Conn

func LoggerConnInit() {
	if viper.GetString("log.dst") != "" {
		conn, err := net.Dial("udp", viper.GetString("log.dst"))
		if err != nil {
			logrus.Fatal(err)
		}
		logConn = conn
	}
}

func LoggerInit() *logrus.Logger {
	logger := logrus.New()
	host, _ := os.Hostname()

	logger.Level = models.ConvertLevel(viper.GetString("log.level"))
	logger.Formatter = &logrus.TextFormatter{FullTimestamp: true, TimestampFormat: time.RFC3339Nano}

	if logConn == nil {
		return logger
	}

	hook := logrustash.New(logConn, logrustash.LogstashFormatter{
		Fields: logrus.Fields{
			"type":	"cattle",
			"hostname":    host,
			"service": "backman",
			"release": viper.GetString("release"),
		},
		Formatter: &logrus.JSONFormatter{
			FieldMap: logrus.FieldMap{
				logrus.FieldKeyTime: "@timestamp",
				logrus.FieldKeyMsg:  "message",
			},
			TimestampFormat: time.RFC3339Nano,
		}})
	logger.Hooks.Add(hook)

	return logger
}

// GetLogger return a logger
func GetLogger(fields logrus.Fields) *logrus.Entry {
	return LoggerInit().WithFields(fields)
}

func LoggerFields(log *logrus.Entry, fields logrus.Fields) *logrus.Entry {
	return log.WithFields(fields)
}

