package common

import (
	"gopkg.in/mgo.v2"

	"github.com/jmoiron/sqlx"
	_ "github.com/go-sql-driver/mysql"
)

//var db *mgo.Session

// ConnectDB connect to a remote mongo
func ConnectDB(dataURL string) (*mgo.Session, error) {
	db, err := mgo.Dial(dataURL)
	if err != nil {
		return nil, err
	}

//	db.SetMode(mgo.Eventual, true)
	return db, nil
}

/*
// GetDB returns the db pointer from context or nil if db has not been connected
func GetDB() *mgo.Session {
	return db
}
*/

// ConnectMysql connect to a remote mysql
func ConnectMysql(dataURL string) (*sqlx.DB, error) {
	db, err := sqlx.Connect("mysql", dataURL)
	if err != nil {
		return nil, err
	}
	return db, nil
}
