package handlers

import (
	"druid/cattle/backman/common"
	"druid/cattle/models"

	"sort"
//	"time"

	"github.com/spf13/viper"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	log "github.com/sirupsen/logrus"
)

func StatisticsODBAAll() {
	db, err := common.ConnectDB(viper.GetString("database"))
	if err != nil {
		log.Error("Connect to db failed.", err)
		return
	}

	devices, err := models.ListDevice(db, nil, nil)
	if err != nil {
		log.Error("list device failed.", err)
		return
	}

	if err := statisticsDayAll(devices); err != nil {
		log.Error("Statistics day failed.", err)
		return
	}

	if err := statisticsWeekAll(devices); err != nil {
		log.Error("Statistics week failed.", err)
		return
	}
	if err := statisticsMonthAll(devices); err != nil {
		log.Error("Statistics month failed.", err)
		return
	}
}

func statisticsDayAll(devices []*models.Device) error {
	db, err := common.ConnectDB(viper.GetString("database"))
	if err != nil {
		return models.Error("Connect to db failed.", err)
	}

	m := bson.M{"timestamp": bson.M{"$gte": models.TimestampBegin, "$lt": models.GetTimestampEnd()}}
	for _, device := range devices {
		if device.OwnerID == "" {
			continue
		}
		user := models.FindUserByID(db, device.OwnerID)
		if user == nil {
			continue
		}
		zoneOffset := 480
		if user.Profile != nil {
			zoneOffset = user.Profile.TimeZone
		}

		m["device_id"] = device.ID
		behs, err := models.ListBehavior2ByFilter(db, m, nil, nil)
		if err != nil {
			return models.Error("list device failed.", err)
		}
		if behs == nil || len(behs) == 0 {
			continue
		}

		ms := make(map[string]*models.StatisticsODBA)
		for _, beh := range behs {
			date := models.ConvertTimeToOffsetDate(*beh.Timestamp, zoneOffset)

		//	log.Println("beh date:", date, " beh time:", beh.Timestamp)
			if ms[date] == nil {
				ms[date] = &models.StatisticsODBA{
					DeviceID: device.ID,
					UUID:     device.UUID,
					Mark:     device.Mark,
					Date:	  date,
					OwnerID:  device.OwnerID,
					Type:     models.StatisticsODBATypeDay,
				}		
			}

			ms[date].ODBA = ms[date].ODBA + int(beh.ODBA)
			continue
		}

		q := models.GetStatisticsODBAQueue(db)
		for _, v := range ms {
			q.Upsert(bson.M{"device_id": v.DeviceID, "type": v.Type, "date": v.Date }, bson.M{"$set": v})
		}

		if _, err := q.Run(); err != nil {
			return models.Error("Queue upsert statistics odba day failed.", err)
		}
	}

		if err := statisticsSort(db.Copy(), devices, models.StatisticsODBATypeDay); err != nil {
			log.Error("Statistics sort day failed.", err)
		}

	return nil
}
func statisticsWeekAll(devices []*models.Device) error {
	db, err := common.ConnectDB(viper.GetString("database"))
	if err != nil {
		return models.Error("Connect to db failed.", err)
	}

	for _, device := range devices {
		sos, err := models.ListStatisticsODBAByFilter(db, bson.M{"device_id": device.ID, "type": models.StatisticsODBATypeDay}, nil, nil)
		if err != nil {
			return models.Error("Find latest so failed.", err)
		}

		ms := make(map[string]*models.StatisticsODBA)
		for _, so := range sos {
			w := models.ConvertDateToMonday(so.Date)

			if ms[w] == nil {
				ms[w] = &models.StatisticsODBA{
					DeviceID: device.ID,
					UUID:     device.UUID,
					Mark:     device.Mark,
					Date:	  w,
					OwnerID:  device.OwnerID,
					Type:     models.StatisticsODBATypeWeek,
				}		
			}
			ms[w].ODBA = ms[w].ODBA + int(so.ODBA)
		}

		q := models.GetStatisticsODBAQueue(db)
		for _, v := range ms {
			q.Upsert(bson.M{"device_id": v.DeviceID, "type": v.Type, "date": v.Date }, bson.M{"$set": v})
		}
		if _, err := q.Run(); err != nil {
			return models.Error("Queue upsert statistics odba day failed.", err)
		}
	}

		if err := statisticsSort(db.Copy(), devices, models.StatisticsODBATypeWeek); err != nil {
			log.Error("Statistics sort day failed.", err)
		}

	return nil
}

func statisticsMonthAll(devices []*models.Device) error {
	db, err := common.ConnectDB(viper.GetString("database"))
	if err != nil {
		return models.Error("Connect to db failed.", err)
	}

	for _, device := range devices {
		sos, err := models.ListStatisticsODBAByFilter(db, bson.M{"device_id": device.ID, "type": models.StatisticsODBATypeDay}, nil, nil)
		if err != nil {
			return models.Error("Find latest so failed.", err)
		}

		ms := make(map[string]*models.StatisticsODBA)
		for _, so := range sos {
			w := models.ConvertDateToMonthOne(so.Date)

			if ms[w] == nil {
				ms[w] = &models.StatisticsODBA{
					DeviceID: device.ID,
					UUID:     device.UUID,
					Mark:     device.Mark,
					Date:	  w,
					OwnerID:  device.OwnerID,
					Type:     models.StatisticsODBATypeMonth,
				}		
			}
			ms[w].ODBA = ms[w].ODBA + int(so.ODBA)
		}

		q := models.GetStatisticsODBAQueue(db)
		for _, v := range ms {
			q.Upsert(bson.M{"device_id": v.DeviceID, "type": v.Type, "date": v.Date }, bson.M{"$set": v})
		}
		if _, err := q.Run(); err != nil {
			return models.Error("Queue upsert statistics odba day failed.", err)
		}
	}

	if err := statisticsSort(db.Copy(), devices, models.StatisticsODBATypeMonth); err != nil {
		log.Error("Statistics sort day failed.", err)
	}

	return nil
}

type StatisticsSort struct {
	Objects	map[bson.ObjectId]map[string][]*models.StatisticsODBA
}

func newStatisticsSort() *StatisticsSort {
	return &StatisticsSort{
		Objects : make(map[bson.ObjectId]map[string][]*models.StatisticsODBA),
	}
}

func (ss *StatisticsSort)AddObjects(id bson.ObjectId, sos []*models.StatisticsODBA) {
	if ss.Objects[id] == nil {
		ss.Objects[id] = make(map[string][]*models.StatisticsODBA)
	}

	mp := ss.Objects[id]
	for _, so := range sos {
		if mp[so.Date] == nil {
			mp[so.Date] = []*models.StatisticsODBA{}
		}
		mp[so.Date] = append(mp[so.Date], so)
	}
}

func (ss *StatisticsSort)Sort(db *mgo.Session) error {
	for _, obj := range ss.Objects {
		q := models.GetStatisticsODBAQueue(db)

		for _, v := range obj {
			sort.Sort(models.StatisticsODBASlice(v))
			for i, so := range v {
				so.Index = i + 1
				q.Upsert(bson.M{"device_id": so.DeviceID, "type": so.Type, "date": so.Date}, bson.M{"$set": so})
			}

		}
		if _, err := q.Run(); err != nil {
			return models.Error("Queue upsert statistics odba index failed.", err) } }
	return nil
}

func statisticsSort(db *mgo.Session, devices []*models.Device, t int) error {
	defer db.Close()

	m := bson.M{"type": t }
	ss := newStatisticsSort()

	for _, device := range devices {
		if device.OwnerID == "" {
			continue
		}
	
		m["device_id"] = device.ID

		rp := &models.RequestParameter{
			Sort: []string{"date"},
		}

		sos, err := models.ListStatisticsODBAByFilter(db, m, rp, nil)
		if err != nil {
			return models.Error("List statistics odba failed.", err)
		}
		if len(sos) == 0 {
			continue
		}
		ss.AddObjects(device.OwnerID, sos)
	}

	if err := ss.Sort(db); err != nil {
		return err
	}

	return nil
}
