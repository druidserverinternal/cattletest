package handlers


import (
	"time"
	"fmt"
)

const shortForm = "2006-01-02"

func GetNextDayTimer(hour int) time.Duration {
	now := time.Now()
	next := now.Add(time.Hour * 24)
	next = time.Date(next.Year(), next.Month(), next.Day(), hour, 0, 0, 0, time.UTC)

	return next.Sub(now)
}

func SplitTime(t *time.Time) (*time.Time, *time.Time, string) {
	begin := time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, time.UTC)
	end := time.Date(t.Year(), t.Month(), t.Day() + 1, 0, 0, 0, 0, time.UTC)
	date := fmt.Sprintf("%04d-%02d-%02d", t.Year(), t.Month(), t.Day())

	return &begin, &end, date
}

func ConvertTimeToDate(t *time.Time) (*time.Time) {
	begin := time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, time.UTC)
	return &begin
}

func ConvertTimeToDateString(t *time.Time) (string) {
	return fmt.Sprintf("%04d-%02d-%02d", t.Year(), t.Month(), t.Day())
}

func GetTodayDate() (*time.Time) {
	t := time.Now()
	begin := time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, time.UTC)
	return &begin
}

func GetLastDate(t *time.Time) (*time.Time) {
	y := t.AddDate(0, 0, -1)
	begin := time.Date(y.Year(), y.Month(), y.Day(), 0, 0, 0, 0, time.UTC)
	return &begin
}

func GetDateRange(date string) (*time.Time, *time.Time, error) {
	begin, err := time.Parse(shortForm, date)
	if err != nil {
		return nil, nil, err
	}
	end := begin.AddDate(0, 0, 1)
	return &begin, &end, nil
}
