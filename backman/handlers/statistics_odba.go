package handlers

import (
	"druid/cattle/backman/common"
	"druid/cattle/models"

//	"sort"
	"time"

	"github.com/spf13/viper"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	log "github.com/sirupsen/logrus"
)

func StatisticsODBA() {
	db, err := common.ConnectDB(viper.GetString("database"))
	if err != nil {
		log.Error("Connect to db failed.", err)
		return
	}

	devices, err := models.ListDevice(db, nil, nil)
	if err != nil {
		log.Error("list device failed.", err)
		return
	}

	for _, device := range devices {
		if device.OwnerID == "" {
			continue
		}
		user := models.FindUserByID(db, device.OwnerID)
		if user == nil {
			continue
		}
		zoneOffset := 480
		if user.Profile != nil {
			zoneOffset = user.Profile.TimeZone
		}

		if err := statisticsDay(device, zoneOffset); err != nil {
			log.Error("Statistics day failed.", err)
			return
		}

		end := models.GetZoneMonday(time.Now(), zoneOffset)
		if models.GetZoneBegin(time.Now(), zoneOffset).Equal(end) {
			if err := statisticsWeek(device, end); err != nil {
				log.Error("Statistics week failed.", err)
				return
			}
		}

		end = models.GetZoneMonthOne(time.Now(), zoneOffset)
		if models.GetZoneBegin(time.Now(), zoneOffset).Equal(end) {
			if err := statisticsMonth(device, end); err != nil {
				log.Error("Statistics month failed.", err)
				return
			}
		}
	}

	if err := statisticsSortDay(db.Copy(), devices); err != nil {
		log.Error("Statistics sort day failed.", err)
	}

	if err := statisticsSortMonth(db.Copy(), devices); err != nil {
		log.Error("Statistics sort day failed.", err)
	}

	if err := statisticsSortWeek(db.Copy(), devices); err != nil {
		log.Error("Statistics sort day failed.", err)
	}

}


func statisticsDay(device *models.Device, zoneOffset int) error {
	db, err := common.ConnectDB(viper.GetString("database"))
	if err != nil {
		return models.Error("Connect to db failed.", err)
	}

	// gmt 02-11 02:00,  utc: 02-10 18:00
	// gmt 02-11 08:00,  utc: 02-11 00:00
	// gmt 02-11 00:00   utc: 02-10 16:00
	end := models.ConvertTimeToZoneBegin(time.Now(), zoneOffset)
	begin := end.AddDate(0, 0, -1)

	m := bson.M{"device_id": device.ID, "timestamp": bson.M{"$gte": begin, "$lt": end}}
	behs, err := models.ListBehavior2ByFilter(db, m, nil, nil)
	if err != nil {
		return models.Error("list device failed.", err)
	}
	if behs == nil || len(behs) == 0 {
		return nil
	}
	//	log.Println("Search beh::", m)

	ms := make(map[string]*models.StatisticsODBA)
	for _, beh := range behs {
		date := models.ConvertTimeToOffsetDate(*beh.Timestamp, zoneOffset)
		if ms[date] == nil {
			ms[date] = &models.StatisticsODBA{
				DeviceID: device.ID,
				UUID:     device.UUID,
				Mark:     device.Mark,
				Date:	  date,
				OwnerID:  device.OwnerID,
				Type:     models.StatisticsODBATypeDay,
			}		
		}

		ms[date].ODBA = ms[date].ODBA + int(beh.ODBA)
		continue
	}

	q := models.GetStatisticsODBAQueue(db)
	for _, v := range ms {
		q.Upsert(bson.M{"device_id": v.DeviceID, "type": v.Type, "date": v.Date }, bson.M{"$set": v})
	}
	if _, err := q.Run(); err != nil {
		return models.Error("Queue upsert statistics odba day failed.", err)
	}

	return nil
}

func statisticsMonth(device *models.Device, end time.Time) error {
	db, err := common.ConnectDB(viper.GetString("database"))
	if err != nil {
		return models.Error("Connect to db failed.", err)
	}

	begin := end.AddDate(0, -1, 0)

	m := bson.M{"device_id": device.ID, "type": models.StatisticsODBATypeDay, "date": bson.M{"$gte": begin.Format(time.RFC3339), "$lt": end}}
	sos, err := models.ListStatisticsODBAByFilter(db, m, nil, nil)
	if err != nil {
		return models.Error("Find latest so failed.", err)
	}

	ms := make(map[string]*models.StatisticsODBA)
	for _, so := range sos {
		w := models.ConvertDateToMonthOne(so.Date)

		if ms[w] == nil {
			ms[w] = &models.StatisticsODBA{
				DeviceID: device.ID,
				UUID:     device.UUID,
				Mark:     device.Mark,
				Date:	  w,
				OwnerID:  device.OwnerID,
				Type:     models.StatisticsODBATypeMonth,
			}		
		}
		ms[w].ODBA = ms[w].ODBA + int(so.ODBA)
	}

	q := models.GetStatisticsODBAQueue(db)
	for _, v := range ms {
		q.Upsert(bson.M{"device_id": v.DeviceID, "type": v.Type, "date": v.Date }, bson.M{"$set": v})
	}
	if _, err := q.Run(); err != nil {
		return models.Error("Queue upsert statistics odba day failed.", err)
	}

	return nil
}


func statisticsSortMonth(db *mgo.Session, devices []*models.Device) error {
	defer db.Close()

	m := bson.M{"type":  models.StatisticsODBATypeMonth }
	ss := newStatisticsSort()
	for _, device := range devices {
		if device.OwnerID == "" {
			continue
		}
		user := models.FindUserByID(db, device.OwnerID)
		if user == nil {
			continue
		}
		zoneOffset := 480
		if user.Profile != nil {
			zoneOffset = user.Profile.TimeZone
		}

		begin := models.GetZoneMonthOne(time.Now(), zoneOffset).AddDate(0, -1, 0)
		date := models.ConvertTimeToDate(&begin)
		//		log.Println("sort month begin:", begin, " date:", date)

		m["date"] = date
		m["device_id"] = device.ID

		rp := &models.RequestParameter{
			Sort: []string{"date"},
		}

		sos, err := models.ListStatisticsODBAByFilter(db, m, rp, nil)
		if err != nil {
			return models.Error("List statistics odba failed.", err)
		}
		if len(sos) == 0 {
			continue
		}

		ss.AddObjects(device.OwnerID, sos)
	}

	if err := ss.Sort(db); err != nil {
		return err
	}

	return nil
}


func statisticsWeek(device *models.Device, end time.Time) error {
	db, err := common.ConnectDB(viper.GetString("database"))
	if err != nil {
		return models.Error("Connect to db failed.", err)
	}

	begin := end.AddDate(0, 0, -7).Format(time.RFC3339)

	m := bson.M{"device_id": device.ID, "type": models.StatisticsODBATypeDay, "date": bson.M{"$gte": begin, "$lt": end.Format(time.RFC3339)}}
	sos, err := models.ListStatisticsODBAByFilter(db, m, nil, nil)
	if err != nil {
		return models.Error("Find latest so failed.", err)
	}

	ms := make(map[string]*models.StatisticsODBA)
	for _, so := range sos {
		w := models.ConvertDateToMonday(so.Date)

		if ms[w] == nil {
			ms[w] = &models.StatisticsODBA{
				DeviceID: device.ID,
				UUID:     device.UUID,
				Mark:     device.Mark,
				Date:	  w,
				OwnerID:  device.OwnerID,
				Type:     models.StatisticsODBATypeWeek,
			}		
		}
		ms[w].ODBA = ms[w].ODBA + int(so.ODBA)
	}

	q := models.GetStatisticsODBAQueue(db)
	for _, v := range ms {
		q.Upsert(bson.M{"device_id": v.DeviceID, "type": v.Type, "date": v.Date }, bson.M{"$set": v})
	}
	if _, err := q.Run(); err != nil {
		return models.Error("Queue upsert statistics odba day failed.", err)
	}

	return nil
}

func statisticsSortWeek(db *mgo.Session, devices []*models.Device) error {
	defer db.Close()

	m := bson.M{"type":  models.StatisticsODBATypeWeek }
	ss := newStatisticsSort()
	for _, device := range devices {
		if device.OwnerID == "" {
			continue
		}
		user := models.FindUserByID(db, device.OwnerID)
		if user == nil {
			continue
		}
		zoneOffset := 480
		if user.Profile != nil {
			zoneOffset = user.Profile.TimeZone
		}

		begin := models.ConvertTimeToOffsetTime(time.Now(), zoneOffset).AddDate(0, 0, -7)
		date := models.ConvertTimeToDate(&begin)
		//		log.Println("sort week begin:", begin, " date:", date)

		m["date"] = date
		m["device_id"] = device.ID

		rp := &models.RequestParameter{
			Sort: []string{"date"},
		}

		sos, err := models.ListStatisticsODBAByFilter(db, m, rp, nil)
		if err != nil {
			return models.Error("List statistics odba failed.", err)
		}
		if len(sos) == 0 {
			continue
		}

		ss.AddObjects(device.OwnerID, sos)
	}

	if err := ss.Sort(db); err != nil {
		return err
	}

	return nil
}

func statisticsSortDay(db *mgo.Session, devices []*models.Device) error {
	defer db.Close()

	m := bson.M{"type": models.StatisticsODBATypeDay }
	ss := newStatisticsSort()
	for _, device := range devices {
		if device.OwnerID == "" {
			continue
		}
		user := models.FindUserByID(db, device.OwnerID)
		if user == nil {
			continue
		}
		zoneOffset := 480
		if user.Profile != nil {
			zoneOffset = user.Profile.TimeZone
		}

		begin := models.ConvertTimeToOffsetTime(time.Now(), zoneOffset).AddDate(0, 0, -1)
		date := models.ConvertTimeToDate(&begin)
		//	log.Println("begin:", begin, " date:", date)

		m["date"] = date
		m["device_id"] = device.ID

		rp := &models.RequestParameter{
			Sort: []string{"date"},
		}

		sos, err := models.ListStatisticsODBAByFilter(db, m, rp, nil)
		if err != nil {
			return models.Error("List statistics odba failed.", err)
		}
		if len(sos) == 0 {
			continue
		}

		ss.AddObjects(device.OwnerID, sos)
	}

	if err := ss.Sort(db); err != nil {
		return err
	}

	return nil
}
