package handlers

import (
	"time"

	"druid/cattle/backman/common"
	"druid/cattle/models"

	"gopkg.in/mgo.v2/bson"

	"github.com/spf13/viper"
	"github.com/sirupsen/logrus"
)

func CheckSignal() {
	fields := logrus.Fields{ "method": "check_signal" }
	log := common.GetLogger(fields)


	db, err := common.ConnectDB(viper.GetString("database"))
	if err != nil {
		log.Error("Connect to db failed.", err)
		return
	}
	devices, err := models.ListAllDevice(db, nil, nil)
	if err != nil {
		log.Error("list devices failed.", err)
		return
	}

//	log.Info("Start check device connection, length:", len(devices))
	q := models.GetDeviceQueue(db)
	for _, device := range devices {
		setting, err := models.FindSettingByDeviceID(db, device.ID)
		if err != nil {
			log.Error("Find setting by device id failed.", err)
			return
		}
		if setting == nil {
			continue
		}

		spend := time.Now().Sub(*device.UpdatedAt).Seconds() 
		lost := int32(spend) - setting.GprsFreq
		if lost > 3600 {
			q.Update(bson.M{ "_id": device.ID }, bson.M{"$set": bson.M{"conn_status": models.DeviceConnectionLost, "lost_time": lost / 3600}})
		}
	}
	_, err = q.Run()
	if err != nil {
		log.Error("Update device connection status queue failed.", err)
		return
	}
}

