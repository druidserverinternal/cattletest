package handlers

import (
	"druid/cattle/models"
	"druid/cattle/backman/common"

//	"time"

	"gopkg.in/mgo.v2/bson"
	"github.com/spf13/viper"

	log "github.com/sirupsen/logrus"
)

func FixBehavior2() {
	db, err := common.ConnectDB(viper.GetString("database"))
	if err != nil {
		log.Error("Connect to db failed.", err)
		return
	}

	behs, err := models.ListBehavior2ByFilter(db, bson.M{"odba": 0}, nil, nil)
	if err != nil {
		log.Error("list device failed.", err)
		return
	}
	log.Info("Found null odba behavior2 length:", len(behs))

	for _, beh := range behs {
		beh.ODBA = beh.ODBAX + beh.ODBAY + beh.ODBAZ
		if err := beh.UpdateODBA(db); err != nil {
			log.Error("Update behavior2 odba failed.", err)
			return
		}
	}
}

