package handlers

import (
	"druid/cattle/models"
	"druid/cattle/backman/common"

//	"gopkg.in/mgo.v2/bson"
	"github.com/spf13/viper"

	"time"
)

func FixBiological() {
	logger := common.LoggerInit()

	db, err := common.ConnectDB(viper.GetString("database"))
	if err != nil {
		logger.Error("Connect to db failed.", err)
		return
	}

	devices, err := models.ListDevice(db, nil, nil)
	if err != nil {
		logger.Error("list device failed.", err)
		return
	}
	logger.Info("Found fix biological length:", len(devices))

	for _, device := range devices {
		if err = models.SendMessageToNSQD(device, models.GetTopicBehavior()); err != nil {
			logger.Error("Send device to mq failed:", err)
		}
		time.Sleep(time.Millisecond * 10)
	}
}

