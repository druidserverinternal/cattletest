package handlers

import (
	"druid/cattle/backman/common"
	"druid/cattle/models"
	"math/rand"
	"time"

	"fmt"

	"github.com/spf13/viper"
)

func AddH3cGps() {

	logger := common.LoggerInit()

	db, err := common.ConnectDB(viper.GetString("database"))
	if err != nil {
		logger.Error("Connect to db failed.", err)
		return
	}

	devices, err := models.ListDeviceByOwner(db, "TEST", nil)
	if err != nil {
		fmt.Println("没有找到设备")
		return
	}

	fmt.Println("找到设备数：", len(devices))

	var timeNum int64
	for _, device := range devices {
		GpsCounts, gpsErr := models.GetGPSCountByDeviceID(db, device.ID)
		if gpsErr != nil || GpsCounts != 0 {
			continue
		}
		lastLongitude := device.Longitude
		lastlatitude := device.Latitude

		timeNum = 1520478277

		for i := 0; i < 24*7; i++ {
			s2 := rand.NewSource(time.Now().UnixNano())
			r1 := rand.New(s2)
			gps := models.GPS{}
			gps.UUID = device.UUID
			//gps.CompanyID = device.CompanyID
			gps.Longitude = lastLongitude + r1.NormFloat64()*0.00010
			gps.Latitude = lastlatitude + r1.NormFloat64()*0.00010
			gps.BatteryVoltage = device.BatteryVoltage
			t1 := time.Unix(timeNum, 0)
			gps.Timestamp = &t1
			gps.UpdatedAt = &t1
			gps.SignalStrength = device.SignalStrength
			gps.Temperature = 20.3 + r1.NormFloat64()
			gps.Light = 2000 + int32(r1.NormFloat64()*100)
			gps.FirmwareVersion = device.FirmwareVersion
			gps.DeviceID = device.ID
			gps.Mark = device.Mark
			gps.UploadAPP(db)

			timeNum = timeNum - 3600 + int64(r1.NormFloat64()*60)

		}
		//device.UpdateStatus()
	}
}
