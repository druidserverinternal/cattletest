package handlers

import (
	"druid/cattle/backman/common"
	"druid/cattle/models"
	"math/rand"
	"time"

	//	"time"

	"github.com/spf13/viper"
	"gopkg.in/mgo.v2/bson"

	log "github.com/sirupsen/logrus"
)

func AddBehavior2() {
	db, err := common.ConnectDB(viper.GetString("database"))
	if err != nil {
		log.Error("Connect to db failed.", err)
		return
	}

	//添加测试数据
	var timess uint32 
	timess = 1517936400
	for i := 100; i <= 2000; i++ {
		s2 := rand.NewSource(time.Now().UnixNano())
		r1 := rand.New(s2)
		beh2 := models.Behavior2{}
		beh2.DeviceID = bson.ObjectIdHex("596e00a20059ea0a31a50013")
		beh2.FirmwareVersion = 11
		beh2.Mark = 2033
		beh2.MeandlX = int32(i) + int32(r1.Intn(100)) - 50 
		beh2.MeandlY = int32(i) + int32(r1.Intn(100)) - 50  
		beh2.MeandlZ = int32(i) + int32(r1.Intn(100)) - 50 
		beh2.ODBAX = int32(i) + int32(r1.Intn(100)) - 50 
		beh2.ODBAY = int32(i) + int32(r1.Intn(100)) - 50 
		beh2.ODBAZ = int32(i) + int32(r1.Intn(100)) - 50 
		beh2.ODBA = beh2.ODBAX + beh2.ODBAY + beh2.ODBAZ

		tm := time.Unix(int64(timess), 0)
		beh2.Timestamp = &tm
		beh2.UpdatedAt = &tm 
		beh2.Create(db)

		timess = timess + 60 
	}
	for i := 2000; i >= 100; i-- {
		s2 := rand.NewSource(time.Now().UnixNano())
		r1 := rand.New(s2)
		beh2 := models.Behavior2{}
		beh2.DeviceID = bson.ObjectIdHex("596e00a20059ea0a31a50013")
		beh2.FirmwareVersion = 11
		beh2.Mark = 2033
		beh2.MeandlX = int32(i) + int32(r1.Intn(100)) - 50 
		beh2.MeandlY = int32(i) + int32(r1.Intn(100)) - 50  
		beh2.MeandlZ = int32(i) + int32(r1.Intn(100)) - 50 
		beh2.ODBAX = int32(i) + int32(r1.Intn(100)) - 50 
		beh2.ODBAY = int32(i) + int32(r1.Intn(100)) - 50 
		beh2.ODBAZ = int32(i) + int32(r1.Intn(100)) - 50 

		beh2.ODBA = beh2.ODBAX + beh2.ODBAY + beh2.ODBAZ

		tm := time.Unix(int64(timess), 0)
		beh2.Timestamp = &tm
		beh2.UpdatedAt = &tm 
		beh2.Create(db)

		timess = timess + 60 
	}
}
