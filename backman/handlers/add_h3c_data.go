package handlers

import (
	"druid/cattle/backman/common"
	"druid/cattle/models"
	"math/rand"
	"time"

	//	"time"
	"fmt"

	"github.com/spf13/viper"
	mgo "gopkg.in/mgo.v2"

	"encoding/csv"
	"os"
	"strconv"
)

func AddH3cData() {
	logger := common.LoggerInit()

	db, err := common.ConnectDB(viper.GetString("database"))
	if err != nil {
		logger.Error("Connect to db failed.", err)
		return
	}

	BeAlgo := models.GetBehaviorAlgorithm(db)
	if BeAlgo == nil {
		logger.Error("no have Paraneter for Motion algorithm")
		return
	}

	devices, err := models.ListDeviceByOwner(db, "TEST", nil)
	if err != nil {
		fmt.Println("没有找到设备")
		return
	}
	fmt.Println("我将要开始添加数据了  设备个数:", len(devices))

	for i, device := range devices {
		ODBACount, OdbaErr := models.GetODBACountWithDeviceId(db, device.ID)
		if OdbaErr != nil || ODBACount != 0 {
			continue
		}

		pathAct := "/home/druid/cattle/data/cow" + strconv.Itoa(i%3+1) + "_act.csv"
		FileDateA, err := os.Open(pathAct)
		if err != nil {
			fmt.Println("读取文件错误:", pathAct)
			return
		}

		ActR1 := csv.NewReader(FileDateA)
		sheet, err := ActR1.ReadAll()
		if err != nil {
			fmt.Println("读取文件错误:", pathAct)
			return
		}
		timeAct := 1518368461
		timeNum := timeAct / 3600 * 3600

		//找到第一条数据之前的23小时的数据保存  如果没有则存空 且确保后面数据能向后加
		beforeODBAParamters := []*models.ODBAParameter{}
		for j := 1; j < len(sheet); j++ {
			thisCell := sheet[j]
			if len(thisCell) == 2 {
				s2 := rand.NewSource(time.Now().UnixNano())
				r1 := rand.New(s2)
				thisodbaParamter := models.ODBAParameter{}
				thisodbaParamter.DeviceID = device.ID
				thisodbaParamter.OwnerID = device.OwnerID
				thisodbaParamter.TimeNum = uint32(timeNum)
				thisodbaParamter.Survive = 1
				//thisodbaParamter.ActValidNum = int(strconv.ParseFloat(thisCell[1], 64) * 60)
				v, _ := strconv.ParseFloat(thisCell[1], 64)
				v = v + r1.NormFloat64()*1.5
				if v > 60 {
					v = 60
				}
				if v < 0 {
					v = 0
				}
				thisodbaParamter.ActValidNum = int(v)
				thisodbaParamter.ActTotalNum = 60
				err := thisodbaParamter.Create(db)
				if err != nil {
					fmt.Println(thisodbaParamter)
					logger.Errorln("thisodbaParamter create failed ", err)
				} else {
				}
				if len(beforeODBAParamters) > 0 && beforeODBAParamters[len(beforeODBAParamters)-1].TimeNum < uint32(timeNum)-3600 {
					tempODBA := models.GetODBAParameterWithTimeNum(db, device.ID, uint32(timeNum)-3600)
					if tempODBA != nil {
						beforeODBAParamters = append(beforeODBAParamters, tempODBA)
					}
				}

				//如果没有数据也是需要加入
				if len(beforeODBAParamters) == 0 {
					tempODBA := models.GetODBAParameterWithTimeNum(db, device.ID, uint32(timeNum-3600))
					if tempODBA != nil {
						//fmt.Println("加入了哦````")
						beforeODBAParamters = append(beforeODBAParamters, tempODBA)
					}

				}
				//处理之前24小时的数据 判断是否发情
				heatAlgorithm(db, beforeODBAParamters, uint32(timeNum-3600))
				timeNum = timeNum + 3600
			}
		}

		//行为数据
		pathBeh := "/home/druid/cattle/data/cow" + strconv.Itoa(i%3+1) + "_behav.csv"
		FileDateB, err := os.Open(pathBeh)
		if err != nil {
			fmt.Println("读取文件错误:", pathBeh)
			return
		}

		BehR1 := csv.NewReader(FileDateB)
		sheetB, err := BehR1.ReadAll()
		if err != nil {
			fmt.Println("读取文件错误:", pathBeh)
			return
		}
		timeNumB := timeAct / 3600 * 3600
		for j := 1; j < len(sheetB); j++ {
			thisCell := sheetB[j]
			if len(thisCell) == 6 {
				for kk := 0; kk < 3; kk++ {
					s2 := rand.NewSource(time.Now().UnixNano())
					r1 := rand.New(s2)

					thisBehavirTypeHistory := models.BehaviorTypeHistory{}
					thisBehavirTypeHistory.DeviceID = device.ID
					thisBehavirTypeHistory.OwnerID = device.OwnerID
					thisBehavirTypeHistory.TimeNum = uint32(timeNumB)

					v, _ := strconv.ParseFloat(thisCell[1], 64)
					thisBehavirTypeHistory.MotionEatNum = int(v*60 + r1.NormFloat64()*1.5)
					if thisBehavirTypeHistory.MotionEatNum < 0 {
						thisBehavirTypeHistory.MotionEatNum = 0
					}
					v, _ = strconv.ParseFloat(thisCell[2], 64)
					thisBehavirTypeHistory.MotionRuminationNum = int(v*60 + r1.NormFloat64()*1.5)
					if thisBehavirTypeHistory.MotionRuminationNum < 0 {
						thisBehavirTypeHistory.MotionRuminationNum = 0
					}
					v, _ = strconv.ParseFloat(thisCell[3], 64)
					thisBehavirTypeHistory.MotionOtherLowNum = int(v*60 + r1.NormFloat64()*1.5)
					if thisBehavirTypeHistory.MotionOtherLowNum < 0 {
						thisBehavirTypeHistory.MotionOtherLowNum = 0
					}
					v, _ = strconv.ParseFloat(thisCell[4], 64)
					thisBehavirTypeHistory.MotionOtherMediumNum = int(v*60 + r1.NormFloat64()*1.5)
					if thisBehavirTypeHistory.MotionOtherMediumNum < 0 {
						thisBehavirTypeHistory.MotionOtherMediumNum = 0
					}
					v, _ = strconv.ParseFloat(thisCell[5], 64)
					thisBehavirTypeHistory.MotionOtherHightNum = int(v*60 + r1.NormFloat64()*1.5)
					if thisBehavirTypeHistory.MotionOtherHightNum < 0 {
						thisBehavirTypeHistory.MotionOtherHightNum = 0 
					}

					err := thisBehavirTypeHistory.Create(db)
					if err != nil {
						fmt.Println("behaviorTypeHistory create failed")
					}
					timeNumB = timeNumB + 3600
				}
			}
		}

	}
}

func heatAlgorithm(db *mgo.Session, beforeODBA []*models.ODBAParameter, thisTime uint32) {

	if len(beforeODBA) == 0 {
		return
	}

	thisCellNum := 0
	for i := len(beforeODBA) - 1; i >= 0; i-- {
		if beforeODBA[i].TimeNum == thisTime {
			thisCellNum = i
		}
	}
	if beforeODBA[thisCellNum].ActNow != 0.0 {
		return
	}

	//计算act_percenage
	beforeODBA[thisCellNum].ActPerecentage = float32(beforeODBA[thisCellNum].ActValidNum) * 1.0 / float32(beforeODBA[thisCellNum].ActTotalNum) * 100.0
	//fmt.Println("thisCellNum :", thisCellNum, " ActPerecentage:", beforeODBA[thisCellNum].ActPerecentage, " actNum:", beforeODBA[thisCellNum].ActValidNum, " actTolal:", beforeODBA[thisCellNum].ActTotalNum)

	var err error
	if thisCellNum < 23 {
		err = beforeODBA[thisCellNum].UpdateAll(db)
		if err != nil {
			fmt.Println("beforeODBA[thisCellNum] UpdateALL is failed")
		}
		return
	}

	//判断开始项是否为正确的时间
	startTime := thisTime - 3600*23
	if beforeODBA[thisCellNum-23].TimeNum != startTime {
		fmt.Println("err is heatAlgorithm Time : thisCellNum:", thisCellNum, " startTime:", beforeODBA[thisCellNum-23], " nowStartTime:", startTime)
		return
	}

	//开始处理
	fmt.Println("正式处理发情状态数据····")

	var act_now_all float32
	act_now_all = 0.0
	for i := thisCellNum - 23; i <= thisCellNum; i++ {
		act_now_all += beforeODBA[i].ActPerecentage
	} // 不用再次判断是否存在了
	beforeODBA[thisCellNum].ActNow = act_now_all / 24.0

	if beforeODBA[thisCellNum-9].ActNow != 0.0 {
		//处理得到act_slope的数据
		act_slope, err := GetActSlope(beforeODBA[thisCellNum-9 : thisCellNum+1]) //注意是闭区间
		if err == nil {
			if act_slope <= 0.0 { // 根据算法去掉发情后产生的波谷
				act_slope = 0.000001
			}
			beforeODBA[thisCellNum].ActSlope = act_slope
		}
	}
	//判断是否发情  注意发情值为0.5 以后还将为改
	b_loving := 0
	if beforeODBA[thisCellNum-2].ActSlope > 0.5 && beforeODBA[thisCellNum-1].ActSlope > 0.5 && beforeODBA[thisCellNum].ActSlope > 0.5 && !(beforeODBA[thisCellNum].ActSlope < beforeODBA[thisCellNum-1].ActSlope && beforeODBA[thisCellNum-1].ActSlope < beforeODBA[thisCellNum-2].ActSlope) {
		//fmt.Println("发情状态：", beforeODBA[thisCellNum-2].ActSlope, " ", beforeODBA[thisCellNum-1].ActSlope, " ", beforeODBA[thisCellNum].ActSlope)
		b_loving = 1
		beforeODBA[thisCellNum].Survive = models.SurviveLoving
	} else {
		//fmt.Println("非发情状态：", beforeODBA[thisCellNum-2].ActSlope, " ", beforeODBA[thisCellNum-1].ActSlope, " ", beforeODBA[thisCellNum].ActSlope)
		beforeODBA[thisCellNum].Survive = models.SurviveLive
	}
	//判断是上一个状态是否是发情
	if beforeODBA[thisCellNum-1].Survive == models.SurviveLoving {
		// 发情
		if b_loving == 1 {
			//上一个状态同样为发情不处理
		} else {
			//上一个状态发情，本状态不发情为结束发情
			//1 标记设备正常状态
			err = models.UpdateDeviceWithSurvive(db, beforeODBA[thisCellNum].DeviceID, models.SurviveLive)
			if err != nil {
				fmt.Println("UpdateDeviceWithSurvive is failed", err)
			}

			//2-找到该设备最后一个发情状态标记结束时间
			err := models.UpdateODBALovingWithEndOne(db, beforeODBA[thisCellNum].DeviceID, beforeODBA[thisCellNum].TimeNum)
			if err != nil {
				fmt.Println("UpdateODBALovingWithEndOne is err", err)
			}

		}
	} else {
		//非发情状态
		if b_loving == 1 {
			//开始发情的状态
			//处理发情
			//将前2项置为发情状态
			beforeODBA[thisCellNum-1].Survive = models.SurviveLoving
			err = beforeODBA[thisCellNum-1].UpdateAll(db)
			if err != nil {
				fmt.Println("beforeODBA[thisCellNum-1] UpdateALL is failed")
			}
			beforeODBA[thisCellNum-2].Survive = models.SurviveLoving
			err = beforeODBA[thisCellNum-2].UpdateAll(db)
			if err != nil {
				fmt.Println("beforeODBA[thisCellNum-2] UpdateALL is failed")
			}

			//将设备置为发情
			err = models.UpdateDeviceWithSurvive(db, beforeODBA[thisCellNum].DeviceID, models.SurviveLoving)
			if err != nil {
				fmt.Println("UpdateDeviceWithSurvive is failed")
			}

			//创建发情历史
			odbaLoving := models.ODBALoving{}
			odbaLoving.TimeNum = beforeODBA[thisCellNum-2].TimeNum
			odbaLoving.DeviceID = beforeODBA[thisCellNum-2].DeviceID
			odbaLoving.OwnerID = beforeODBA[thisCellNum-2].OwnerID
			err = odbaLoving.Create(db)
			if err != nil {
				fmt.Println("odbaLoving Create is failed")
			}
		} else {
			//上次和本次均为非发情状态不更改
		}
	}

	err = beforeODBA[thisCellNum].UpdateAll(db)
	if err != nil {
		fmt.Println("beforeODBA[thisCellNum] UpdateALL is failed")
	}

}

func GetActSlope(ODBA []*models.ODBAParameter) (float32, error) {
	if len(ODBA) != 10 {
		fmt.Println("GetActSlope is err ODBA len is :", len(ODBA))
		return 0.0, fmt.Errorf("GetActSlope is err")
	}
	var A, B, C, D float32
	A = 0.0
	B = 0.0
	C = 0.0
	D = 0.0

	for i := 1; i <= 10; i++ {
		A = A + float32(i)*ODBA[i-1].ActNow
		B = B + float32(i)
		C = C + ODBA[i-1].ActNow
		D = D + float32(i)*float32(i)*1.0
	}

	return float32((A - B*C/10.0) / (D - B*B/10.0)), nil
}
