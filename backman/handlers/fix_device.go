package handlers

import (
	"druid/cattle/models"
	"druid/cattle/backman/common"

//	"gopkg.in/mgo.v2/bson"
	"github.com/spf13/viper"
)

func FixDevice() {
	logger := common.LoggerInit()

	db, err := common.ConnectDB(viper.GetString("database"))
	if err != nil {
		logger.Error("Connect to db failed.", err)
		return
	}

	devices, err := models.ListDevice(db, nil, nil)
	if err != nil {
		logger.Error("list device failed.", err)
		return
	}
	logger.Info("Found fix device length:", len(devices))

	for _, device := range devices {
		if err = models.SendMessageToNSQD(device, models.GetTopicDevice()); err != nil {
			logger.Error("Send device to mq failed:", err)
		}
	}
}


