package handlers

import (
	"druid/cattle/backman/common"
	"druid/cattle/models"
	"math/rand"
	"strconv"
	"time"

	"github.com/spf13/viper"
	"gopkg.in/mgo.v2/bson"
)

func AddH3cDevice() {
	logger := common.LoggerInit()

	db, err := common.ConnectDB(viper.GetString("database"))
	if err != nil {
		logger.Error("Connect to db failed.", err)
		return
	}

	for i := 4; i < 101; i++ {
		s2 := rand.NewSource(time.Now().UnixNano())
		r1 := rand.New(s2)
		device := models.Device{}
		device.ID = bson.NewObjectId()
		device.UUID = "3b006a000751353334393" + iToStr(i)
		device.CompanyID = bson.ObjectIdHex("5ac98436b1abed6a331a420f")
		device.HardwareVersion = 2
		device.Mark = i 
		device.BatteryVoltage = 3.950 + r1.NormFloat64()*0.0700
		device.Mac = "38:1c:4a:9a:f3:" + iToX16(i)
		device.CompanyName = "德鲁伊科技测试"
		device.DeviceType = 1
		device.OwnerID = bson.ObjectIdHex("5ac984efa32d8003377330f5")
		device.SIMNumber = "+8614528073" + iToStr(i)
		device.FirmwareVersion = 2
		device.IMSI = "460018072704" + iToStr(i)
		device.TodayGPS = 0
		device.Latitude = 30.74880 + r1.NormFloat64()*0.00005
		device.Longitude = 104.36080 + r1.NormFloat64()*0.00005
		device.SignalStrength = 24 + int32(r1.NormFloat64())
		device.PointLocation = 1
		device.OwnerName = "TEST"
		t1 := time.Unix(1520478277+int64(i), 0)
		device.UpdatedAt = &t1
		device.Timestamp = &t1
		device.Temperature = 20.3 + r1.NormFloat64()
		if r1.Int()%5 == 0 {
			device.Activity = 2
		} else {
			device.Activity = 3
		}
		if i == 44 {
			device.Activity = 1
		}

		err := device.Register(db)
		if err != nil {
			continue
		}
		cattle := models.Cattle{}
		cattle.ID = bson.NewObjectId()
		cattle.DeviceID = device.ID
		cattle.OwnerID = bson.ObjectIdHex("5ac984efa32d8003377330f5")
		cattle.OwnerName = "TEST"
		cattle.Mark = i 
		cattle.NickName = "奶牛" + strconv.Itoa(i) + "号"
		cattle.Gender = r1.Int() % 2
		cattle.Weight = 300 + r1.Intn(100) - 50
		cattle.Height = 120 + r1.Intn(30) - 15
		//cattle.Bust = 300 + r1.Intn(100) - 50
		//cattle.Cannon = 40 + r1.Intn(20) - 10
		cattle.CoatColor = "黑白"

		cattle.Description = "健康的奶牛"
		t := time.Unix(1399120461+int64(r1.Intn(6000000)), 0)
		cattle.BirthDate = &t
		cattle.Create(db)
	}
}

func iToStr(i int) string {
	if i > 99 {
		return strconv.Itoa(i)
	}

	if i > 9 {
		return "0" + strconv.Itoa(i)
	}

	return "00" + strconv.Itoa(i)
}

func iToX16(i int) string {

	str := ""
	a := i / 16
	b := i % 16
	if a > 9 {
		str = string(int('A') + a - 10)
	} else {
		str = strconv.Itoa(a)
	}

	if b > 9 {
		str = str + string(int('A')+b-10)
	} else {
		str = str + strconv.Itoa(b)
	}
	return str

}
