package handlers

import (
	"druid/cattle/backman/common"
	"druid/cattle/models"

	"gopkg.in/mgo.v2/bson"

	"github.com/spf13/viper"
	"github.com/sirupsen/logrus"
)

func CheckOffset() {
	fields := logrus.Fields{ "method": "check_offset" }
	log := common.GetLogger(fields)


	db, err := common.ConnectDB(viper.GetString("database"))
	if err != nil {
		log.Error("Connect to db failed.", err)
		return
	}
	gpss, err := models.ListGPS(db, nil, nil)
	if err != nil {
		log.Error("list gpss failed.", err)
		return
	}

	log.Info("Start check gps location offset, length:", len(gpss))
	size := 0
	q := models.GetGPSQueue(db)
	for i, gps := range gpss {
		if gps.Longitude > 190 {
//			log.Info("jump gps:", gps)
			continue
		}
		offset, err := models.FindOffsetByLocation(db, gps.Longitude, gps.Latitude)
		if err != nil {
			log.Error("Find offset by location failed.", err)
			return
		}
		size++
		q.Update(bson.M{ "_id": gps.ID }, bson.M{"$set": bson.M{"point_location": offset.PointLocation}})

		if size % 1000 == 0 {
			log.Info("Start update gps location offset.")
			_, err = q.Run()
			if err != nil {
				log.Error("Update gps point location queue failed.", err)
				return
			}

			q = models.GetGPSQueue(db)
			log.Info("updated:", int(float32(i) / float32(len(gpss)) * 100), "%")
		}
	}

	if size % 1000 > 0 {
		_, err = q.Run()
		if err != nil {
			log.Error("Update gps point location queue failed.", err)
			return
		}

		log.Info("updat gps final.")
	}
}

