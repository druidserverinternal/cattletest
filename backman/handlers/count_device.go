package handlers

import (
//	"time"

	"druid/cattle/backman/common"
	"druid/cattle/models"

	"gopkg.in/mgo.v2/bson"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

func CountDevice() {
	db, err := common.ConnectDB(viper.GetString("database"))
	if err != nil {
		log.Error("Connect to db failed.", err)
		return
	}
	users, err := models.ListUsers(db, nil)
	if err != nil {
		log.Error("list users failed.", err)
		return
	}

	us, err := models.TotalDevice(db)
	if err != nil {
		log.Error("Count gps users failed.", err)
		return
	}

	for _, user := range users {
		c := getDeviceCount(user.ID, us)
		if user.TotalDevice == c {
			continue
		}

		user.TotalDevice = c

		log.Info("Update user:", user)
		if err := user.UpdateTotalDevice(db); err != nil {
			log.Error("Update users data total failed.", err)
			return
		}
	}
}

func getDeviceCount(id bson.ObjectId, us []*models.User) int {
	for _, d := range us {
		if id == d.ID {
			return d.TotalDevice
		}
	}

	return 0
}




