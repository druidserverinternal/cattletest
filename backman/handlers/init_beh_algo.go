package handlers

import (
	"druid/cattle/backman/common"
	"druid/cattle/models"
	"fmt"

	"github.com/spf13/viper"
	"gopkg.in/mgo.v2/bson"
)

func InitBehAlgo() {
	logger := common.LoggerInit()

	db, err := common.ConnectDB(viper.GetString("database"))
	if err != nil {
		logger.Error("Connect to db failed.", err)
		return
	}

	device, _ := models.FindDeviceByID(db, bson.ObjectIdHex("596e00a20059ea0a31a50013"), nil)
	fmt.Println(device)
	if err = models.SendMessageToNSQD(device, models.GetTopicBehavior2()); err != nil {
		logger.Error("Send device to mq failed:", err)
	}
	/*
		devices, err := models.ListDeviceByFilter(db, bson.M{"deleted_at": nil}, nil, nil)
		if err != nil {
			fmt.Println("没有找到设备")
			return
		}
		logger.Info("Found fix device length:", len(devices))

		for _, device := range devices {
			if err = models.SendMessageToNSQD(device, models.GetTopicBehavior2()); err != nil {
				logger.Error("Send device to mq failed:", err)
			}
		}
	*/
}
