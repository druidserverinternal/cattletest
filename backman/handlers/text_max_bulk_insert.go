package handlers

import (
	"druid/cattle/backman/common"
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gopkg.in/mgo.v2/bson"
)

type Testt struct {
	ID bson.ObjectId `json:"id,omitempty" bson:"_id,omitempty"`
	I  int           `json:"i" bson:"i"`
}

func TextMaxBulkInsert() {
	db, err := common.ConnectDB(viper.GetString("database"))
	if err != nil {
		log.Error("Connect to db failed.", err)
		return
	}

	c := db.DB("cattle").C("test")

	q := c.Bulk()
	t := time.Now().UnixNano()

	fmt.Println("我将运行10000条的哦`````")
	t1 := time.Now().UnixNano()
	for i := 1; i <= 10000; i++ {
		T := Testt{}
		T.I = i
		q.Upsert(bson.M{"i": i}, bson.M{"$set": T})
		if i%1000 == 0 {
			q.Run()
			fmt.Println("我运行了到了 ", i)
			q = c.Bulk()
			t2 := time.Now().UnixNano()
			fmt.Println("总时间：", (t2-t)/10000000.0, "  1000条时间：", (t2-t1)/10000000.0)
			t1 = t2
		}
	}

	t = time.Now().UnixNano()

	q = c.Bulk()
	fmt.Println("我将运行10000条的哦`````")
	t1 = time.Now().UnixNano()
	for i := 1; i <= 50000; i++ {
		T := Testt{}
		T.I = i
		q.Upsert(bson.M{"i": i}, bson.M{"$set": T})
	}
	q.Run()
	t2 := time.Now().UnixNano()
	tt := float64(t2 - t) / 1000000000
	fmt.Println("总时间：", tt ,"秒")

}
