package handlers

import (
	"druid/cattle/models"
	"druid/cattle/backman/common"

	"time"

	"gopkg.in/mgo.v2/bson"
	"github.com/spf13/viper"

	log "github.com/sirupsen/logrus"
)

func FixAltitude() {
	db, err := common.ConnectDB(viper.GetString("database"))
	if err != nil {
		log.Error("Connect to db failed.", err)
		return
	}

	gpss, err := models.ListGPSByFilter(db, bson.M{"loc": bson.M{"$ne": nil}, "relative_altitude": nil}, nil, nil)
	if err != nil {
		log.Error("list gps failed.", err)
		return
	}
	log.Info("Found null elevation gps length:", len(gpss))

	for _, gps := range gpss {
		if err = models.SendMessageToNSQD(gps, models.GetTopicGPS()); err != nil {
			log.Error("Send device to mq failed:", err)
			return
		}

		time.Sleep(time.Millisecond * 10)
	}
}

