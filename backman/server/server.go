package server

import (
	"druid/cattle/backman/handlers"

	"github.com/spf13/viper"

//	"time"

	log "github.com/sirupsen/logrus"
)

var mapMux = make(map[string]func())

func init() {
//	mapMux["gps_count"] = handlers.GPSCount
	mapMux["check_signal"] = handlers.CheckSignal
	mapMux["check_offset"] = handlers.CheckOffset
//	mapMux["sync_release"] = handlers.SyncRelease
//	mapMux["sync_alashan"] = handlers.SyncAlashan
	mapMux["fix_device"] = handlers.FixDevice
	mapMux["fix_biological"] = handlers.FixBiological
	mapMux["count_device"] = handlers.CountDevice
	mapMux["init_beh_algo"] = handlers.InitBehAlgo
	mapMux["fix_behavior2"] = handlers.FixBehavior2
	mapMux["add_behavior2"] = handlers.AddBehavior2
	mapMux["add_h3c_data"] = handlers.AddH3cData
	mapMux["add_h3c_gps"] = handlers.AddH3cGps
	mapMux["add_h3c_davice"] = handlers.AddH3cDevice
	mapMux["fix_altitude"] = handlers.FixAltitude
	mapMux["text_max_bulk"] = handlers.TextMaxBulkInsert
	mapMux["statistics_odba"] = handlers.StatisticsODBA
	mapMux["statistics_odba_all"] = handlers.StatisticsODBAAll
//	mapMux["bird_dead"] = handlers.BirdDead
}

// Server is main loop service
func Server() {
	cmd := viper.GetString("cmd")

	if mapMux[cmd] == nil {
		log.Info("Unknown cmd:", cmd)
		return
	}

	log.Info("Start cmd:", cmd)
	mapMux[cmd]()
}
