package cmd

import (
	"druid/cattle/backman/server"
	"druid/cattle/backman/common"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	log "github.com/sirupsen/logrus"
)

// serveCmd represents the serve command
var serveCmd = &cobra.Command{
	Use:    "serve",
	Short:  "Start backman server",
	PreRun: LoadConfiguration,
	Run:    serve,
}

func init() {
	RootCmd.AddCommand(serveCmd)

	serveCmd.Flags().StringP(
		"cmd",
		"m",
		"",
		"valid command: gps_count, signal_zone",
	)
	viper.BindPFlag("cmd", serveCmd.Flags().Lookup("cmd"))
}

func serve(cmd *cobra.Command, args []string) {
	// Init the logger
	common.LoggerInit()

	log.Info("Starting backman server.")
	server.Server()
}
