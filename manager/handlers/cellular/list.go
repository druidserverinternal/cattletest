package cellular

import (
	"net/http"
	"strconv"

	
	"gopkg.in/mgo.v2/bson"

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

// List all of cellular record
func List(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckDataRead() {
		logger.Error("You did not have permissions to read data.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to read data."))
		return
	}


//	m := bson.M{"sms": models.SrcFromDevice}
	result, err := models.ListCellular(db, models.ParseRequestParameter(c))
	if err != nil {
		logger.Error("List cellular failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List cellular failed..", err))
		return
	}

	count := 0
	if len(result) > 0 {
		count, _ = models.CountCellular(db)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, result)
}
