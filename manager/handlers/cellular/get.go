package cellular

import (
	"net/http"
	"strconv"

	"gopkg.in/mgo.v2/bson"

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

func getValidate(c *gin.Context) (string) {
	deviceID := c.Param("id")
	if !bson.IsObjectIdHex(deviceID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("Device id is invalid.", deviceID))
		return ""
	}
	return deviceID
}

// Get all of cellular record with device
func Get(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckDataRead() {
		logger.Error("You did not have permissions to read data.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to read data."))
		return
	}

	deviceID := getValidate(c)
	if deviceID == "" {
		return
	}

	device, err := models.FindDeviceByID(db, bson.ObjectIdHex(deviceID), nil)
	if err != nil {
		logger.Error("Find device by id failed :", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find device by id failed:", err))
		return
	}

	m := bson.M{ "device_id": device.ID }

	begin, _ := models.ConvertStringToTime(c.Query(models.QueryTimeBegin))
	end, _ := models.ConvertStringToTime(c.Query(models.QueryTimeEnd))

	if begin != nil && end != nil {
		m["timestamp"] = bson.M{"$gte": begin, "$lte": end}
	}

	rp := models.ParseRequestParameter(c)
	if rp.LimitOrign == 0 {
		rp.Limit = 0
	}

	logger.Info("Get cellular fileter:", m)
	

	result, err := models.ListCellularByFilter(db, m, rp)
	if err != nil {
		logger.Error("Can't find device's cellular.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Can't find device's cellular.", err))
		return
	}

	count := 0
	if len(result) > 0 {
		count, _ = models.CountCellularByFilter(db, m)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, result)
}
