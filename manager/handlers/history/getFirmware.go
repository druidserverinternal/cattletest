package history

import (
	"net/http"
	"strconv"
	

	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"

	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

func getFWValidate(c *gin.Context) (string) {
	deviceID := c.Param("id")
	if !bson.IsObjectIdHex(deviceID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("Device is invalid:" + deviceID))
		return ""
	}
	return deviceID
}

// GetFirmware list gps record of a device
func GetFirmware(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckDataRead() {
		logger.Error("You did not have permissions to view data.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to view data."))
		return
	}

	deviceID := getFWValidate(c)
	if deviceID == "" {
		return
	}
	rp := models.ParseRequestParameter(c)
	if rp.Limit == models.MaxLimit {
		rp.Limit = 0
	}

	result, err := models.ListFirmwareHistoryByDeviceID(db, bson.ObjectIdHex(deviceID), rp)
	if err != nil {
		logger.Error("List firmware history failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Can't find firmware history.", err))
		return
	}

	count := 0
	if len(result) > 0 {
		count, _ = models.CountFirmwareHistoryByDeviceID(db, bson.ObjectIdHex(deviceID))
	}
	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, result)
}
