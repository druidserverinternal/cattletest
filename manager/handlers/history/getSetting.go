package history

import (
	"net/http"
	"strconv"
	

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"

	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

func getSettingValidate(c *gin.Context) (string) {
	deviceID := c.Param("id")
	if !bson.IsObjectIdHex(deviceID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("Device is invalid:" + deviceID))
		return ""
	}
	return deviceID
}

func GetSetting(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckDataRead() {
		logger.Error("You did not have permissions to view data.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to view data."))
		return
	}

	deviceID := getSettingValidate(c)
	if deviceID == "" {
		return
	}

	rp := models.ParseRequestParameter(c)
	if rp.Limit == models.MaxLimit {
		rp.Limit = 0
	}
	result, err := models.ListSettingHistoryByDeviceID(db, bson.ObjectIdHex(deviceID), rp)
	if err != nil {
		logger.Error("List setting history failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Can't find setting history.", err))
		return
	}

	count := 0
	if len(result) > 0 {
		count, _ = models.CountSettingHistoryByDeviceID(db, bson.ObjectIdHex(deviceID))
	}
	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, result)
}
