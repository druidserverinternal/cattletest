package gps

import (
	"net/http"
	"strconv"
	

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/manager/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

// List all of gps record with device
func List(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckDataRead() {
		logger.Error("You did not have permissions to view data.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to view data."))
		return
	}

	rp := models.ParseRequestParameter(c)
	result, err := models.ListValidGPS(db, rp, nil)
	if err != nil {
		logger.Error("List gps failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List gps by filter failed." , err))
		return
	}

	count := 0
	if len(result) > 0 {
		count, _ = models.GetGPSValidCount(db)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, result)
}
