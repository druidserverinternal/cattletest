package gps

import (
	"net/http"
	"strconv"
	

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/manager/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

func getValidate(c *gin.Context) (string) {
	deviceID := c.Param("id")
	if !bson.IsObjectIdHex(deviceID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("Device is invalid:" + deviceID))
		return ""
	}
	return deviceID
}

// Get one of device record with device
func Get(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckDataRead() {
		logger.Error("You did not have permissions to view gps.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to view gps."))
		return
	}

	deviceID := getValidate(c)
	if deviceID == "" {
		return
	}

	device, err := models.FindDeviceByID(db, bson.ObjectIdHex(deviceID), nil)
	if err != nil {
		logger.Error("Find device failed by id:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find device failed:", err))
		return
	}

	m := bson.M{ "device_id": device.ID }

	begin, _ := models.ConvertStringToTime(c.Query(models.QueryTimeBegin))
	end, _ := models.ConvertStringToTime(c.Query(models.QueryTimeEnd))

	if begin != nil && end != nil {
		m["timestamp"] = bson.M{"$gte": begin, "$lte": end}
	}

	logger.Info("Get gps fileter:", m)
	

	rp := models.ParseRequestParameter(c)
	if rp.Limit == models.MaxLimit {
		rp.Limit = 0
	}

	result, err := models.ListGPSByDeviceID(db, device.ID, rp, nil)
	if err != nil {
		logger.Error("List gps failed :", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List gps failed:", err))
		return
	}


	count := 0
	if len(result) > 0 {
		count, _ = models.GetGPSCountByDeviceID(db, bson.ObjectIdHex(deviceID))
	}
	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, result)
}
