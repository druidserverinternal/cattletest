package gps

import (
	"net/http"
	"strconv"

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"

	"druid/cattle/discovery/middleware"
	"druid/cattle/models"
)

// ListSMS list all of device record with sms
func ListSMS(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckDataRead() {
		logger.Error("You did not have permissions to read data.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to read data."))
		return
	}

	m := bson.M{"sms": models.SrcFromSMS}

	rp := models.ParseRequestParameter(c)

//	begin := time.Now()
	logger.Info("Get gps fileter:", m)
	gps, err := models.ListGPSByFilter(db, m, rp, nil)
	if err != nil {
		logger.Error("Can't find device gps.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List gps by filter failed.", err))
		return
	}

//	logger.Info("mongo spend:", time.Now().Sub(begin))

	count := 0
	if len(gps) > 0 {
		count, _ = models.CountGPSByFilter(db, m)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, gps)
}

