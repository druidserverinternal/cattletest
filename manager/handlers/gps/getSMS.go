package gps

import (
	"net/http"
	"strconv"

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"

	"druid/cattle/discovery/middleware"
	"druid/cattle/models"
)

// Get one of device record with sms
func GetSMS(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	deviceID := getValidate(c)
	if deviceID == "" {
		return
	}

	m := bson.M{ "device_id": bson.ObjectIdHex(deviceID) , "sms": models.SrcFromSMS}

	rp := models.ParseRequestParameter(c)
	if rp.LimitOrign == 0 {
		rp.Limit = 0
	}

//	begin := time.Now()
	logger.Info("Get gps fileter:", m)
	gps, err := models.ListGPSByFilter(db, m, rp, nil)
	if err != nil {
		logger.Error("Can't find device gps.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List gps by filter failed.", err))
		return
	}

//	logger.Info("mongo spend:", time.Now().Sub(begin))

	count := 0
	if len(gps) > 0 {
		count, _ = models.CountGPSByFilter(db, m)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, gps)
}

