package handlers

import (
	"druid/cattle/manager/middleware"

	"druid/cattle/manager/handlers/algorithm"
	"druid/cattle/manager/handlers/behavior2"
	"druid/cattle/manager/handlers/cellular"
	"druid/cattle/manager/handlers/company"
	"druid/cattle/manager/handlers/device"
	"druid/cattle/manager/handlers/firmware"
	"druid/cattle/manager/handlers/gps"
	"druid/cattle/manager/handlers/search"
	"druid/cattle/manager/handlers/setting"
	"druid/cattle/manager/handlers/sim"
	"druid/cattle/manager/handlers/status"
	"druid/cattle/manager/handlers/user"
	//	"druid/cattle/manager/handlers/file"
	"druid/cattle/manager/handlers/history"
	"druid/cattle/manager/handlers/manager"
	"druid/cattle/manager/handlers/push"
	"druid/cattle/manager/handlers/versions"

	"github.com/gin-gonic/contrib/gzip"
	"github.com/gin-gonic/gin"
	"github.com/itsjamie/gin-cors"

	//	"github.com/0987363/cache"
	//	"github.com/0987363/cache/persistence"

	//	"github.com/spf13/viper"
	//log "github.com/sirupsen/logrus"

	"time"
	//	"strings"
)

// RootMux is the root handler for the manager server
var RootMux = gin.New()

func init() {
	RootMux.Use(gin.Logger())

	RootMux.Use(middleware.RequestID())
	RootMux.Use(middleware.Logger())
	RootMux.Use(gin.Recovery())
	RootMux.Use(gzip.Gzip(gzip.DefaultCompression))
	RootMux.Use(middleware.DBConnector())

	RootMux.Use(cors.Middleware(cors.Config{
		Origins:         "*",
		Methods:         "GET, PUT, POST, DELETE",
		RequestHeaders:  "Origin, X-Druid-Authentication, Content-Type, X-Result-Limit, X-Result-Offset, X-Result-Sort, Accept-Language, Accept",
		ExposedHeaders:  "X-Druid-Authentication, X-Result-Count, Content-Length, Content-Type, Content-Disposition",
		MaxAge:          time.Hour * 24 * 7,
		Credentials:     true,
		ValidateHeaders: false,
	}))

	/*
		cache.SetPageKey("manager:1:" + viper.GetString("release"))

		var store persistence.CacheStore
		memAddrs := viper.GetString("memcached")
		addrs := strings.Split(memAddrs, ",")
		if memAddrs == "" || len(addrs) == 0 {
			store = persistence.NewInMemoryStore(time.Hour * 12)
			log.Info("Init memory store success.")
		} else {
			memStore := persistence.NewMemcachedStore(addrs, time.Hour * 12)
			memStore.Client.Timeout = time.Second * 5
			store = memStore
			log.Info("Init memcached store success.", addrs, len(addrs))
		}
	*/

	v1Mux := RootMux.Group("/v1")
	{
		{
			v1Mux.POST("/login", user.Login)
		}

		v1Mux.Use(middleware.Authenticator())

		algorithmMux := v1Mux.Group("/algorithm")
		{
			algorithmMux.POST("/update/trees", algorithm.UpdateParameter)
			algorithmMux.POST("/CheckAlgo", algorithm.CheckAlgo1)
			algorithmMux.POST("/update/odba", algorithm.UpdateODBAParameter)
		}
		versionMux := v1Mux.Group("/version")
		{
			versionMux.POST("/updateapk", versions.UpdateApk)
			versionMux.GET("/:id", versions.Get)
			versionMux.GET("/", versions.List)
			versionMux.POST("/base/info", versions.GetBaseVersion)
			versionMux.POST("/up/info", versions.GetUpVersion)
			versionMux.POST("/update/:id", versions.UpdateInfo)
			versionMux.DELETE("/:id", versions.Delete)
			versionMux.PUT("/update/base", versions.UpdataBase)
			versionMux.PUT("/update/up", versions.UpdateUp)
		}
		ossMux := v1Mux.Group("/oss")
		{
			ossMux.GET("/app/", versions.OssList)
		}
		companyMux := v1Mux.Group("/company")
		{
			companyMux.POST("/", company.Create)

			companyMux.GET("/", company.List)
			companyMux.GET("/id/:id", company.Get)

			companyMux.DELETE("/id/:id", company.Delete)

			companyMux.PUT("/id/:id", company.Update)
		}

		deviceMux := v1Mux.Group("/device")
		{
			deviceMux.GET("/", device.List)
			deviceMux.GET("/company/:id", device.ListByCompany)
			deviceMux.GET("/idle", device.ListIdle)
			deviceMux.GET("/deleted", device.ListDeleted)
			deviceMux.GET("/id/:id", device.Get)
			deviceMux.GET("/search/mark/:sn", device.Search)
			deviceMux.GET("/search/phone/:number", device.SearchPhone)

			deviceMux.DELETE("/id/:id", device.Delete)
			deviceMux.DELETE("/deleted", device.DestroyAll)
			deviceMux.DELETE("/deleted/:id", device.Destroy)

			deviceMux.PUT("/recover/:id", device.Recover)
			deviceMux.PUT("/company", device.UpdateMany)
			deviceMux.PUT("/idle", device.FreeMany)
			deviceMux.PUT("/description", device.UpdateDescription)
			deviceMux.PUT("/stock", device.UpdateStock)

			deviceMux.POST("/many", device.ListMany)
			deviceMux.POST("/kml", device.KmlMultiple)
			deviceMux.POST("/csv/device", device.CSVDevice)
			deviceMux.POST("/csv/env", device.CSV)
			deviceMux.POST("/csv_multiple/env", device.CSVMultiple)
			deviceMux.POST("/csv/behavior", device.CSVBehavior)
			deviceMux.POST("/csv_multiple/behavior", device.CSVMultipleBehavior)
			deviceMux.POST("/csv_multiple/status", device.CSVMultipleStatus)
		}

		settingMux := v1Mux.Group("/setting")
		{
			settingMux.GET("/", setting.List)
			settingMux.GET("/device/:id", setting.Get)

			settingMux.PUT("/many", setting.UpdateMany)
			settingMux.PUT("/firmware", setting.UpdateFirmware)
		}

		gpsMux := v1Mux.Group("/gps")
		{
			gpsMux.GET("/", gps.List)
			gpsMux.GET("/sms", gps.ListSMS)
			gpsMux.GET("/device/:id", gps.Get)
			gpsMux.GET("/device/:id/sms", gps.GetSMS)
		}

		cellularMux := v1Mux.Group("/cellular")
		{
			cellularMux.GET("/", cellular.List)
			cellularMux.GET("/device/:id", cellular.Get)
		}

		behavior2Mux := v1Mux.Group("/behavior")
		{
			behavior2Mux.GET("/", behavior2.List)
			behavior2Mux.GET("/device/:id", behavior2.Get)
		}

		statusMux := v1Mux.Group("/status")
		{
			statusMux.GET("/", status.List)
			statusMux.GET("/device/:id", status.Get)
		}

		userMux := v1Mux.Group("/user")
		{
			userMux.GET("/", user.List)
			userMux.GET("/company/:id", user.ListByCompany)
			userMux.GET("/myself", user.Me)
			userMux.GET("/id/:id", user.Get)

			userMux.POST("/", user.Create)

			userMux.PUT("/password", user.UpdatePassword)
			userMux.PUT("/info", user.UpdateInfo)
			userMux.PUT("/id/:id/info", user.UpdateUserInfo)
			userMux.PUT("/id/:id/password", user.UpdateUserPwd)
			userMux.PUT("/id/:id/profile", user.UpdateOtherProfile)

			userMux.DELETE("/id/:id", user.Delete)
		}

		simMux := v1Mux.Group("/sim")
		{
			simMux.POST("/", sim.Upload)
			simMux.POST("/excel", sim.UploadIndex)

			simMux.PUT("/id/:id", sim.Update)

			simMux.GET("/", sim.List)

			simMux.DELETE("/id/:id", sim.Delete)
		}

		searchMux := v1Mux.Group("/search")
		{
			searchMux.POST("/cellular", search.Cellular)
			searchMux.POST("/gps", search.GPS)
			searchMux.POST("/device", search.Device)
			searchMux.POST("/sim", search.SIM)
			searchMux.POST("/status", search.Status)
		}

		/*
			fileMux := v1Mux.Group("/file")
			{
				fileMux.GET("/:id", file.Download)
			}
		*/

		managerMux := v1Mux.Group("/manager")
		{
			managerMux.POST("/export/:type", manager.Export)

			managerMux.PUT("/company/:company_id", manager.UploadCompany)

			managerMux.GET("/sn", manager.GetSN)
		}

		firmwareMux := v1Mux.Group("/firmware")
		{
			firmwareMux.GET("/", firmware.List)
			//			firmwareMux.GET("/id/:id", firmware.Get)

			firmwareMux.DELETE("/id/:id", firmware.Delete)

			firmwareMux.POST("/name/:name", firmware.UploadToMongo)
			firmwareMux.POST("/", firmware.Upload)
		}

		historyMux := v1Mux.Group("/history")
		{
			historyMux.GET("/firmware", history.ListFirmware)
			historyMux.GET("/setting", history.ListSetting)

			historyMux.GET("/firmware/:id", history.GetFirmware)
			historyMux.GET("/setting/:id", history.GetSetting)
		}

		pushMux := v1Mux.Group("/push")
		{
			pushMux.POST("/setting", push.CreateSetting)

			pushMux.PUT("/setting/:id", push.UpdateSetting)

			pushMux.GET("/id/:id", push.GetSetting)
			pushMux.GET("/", push.ListSetting)
		}

	}
}
