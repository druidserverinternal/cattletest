package status

import (
	"net/http"
	"strconv"
	

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"

	"gopkg.in/mgo.v2/bson"
)

// List status list
func List(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
    if user.IsManager() && !user.CheckDataRead() {
        logger.Error("You did not have permissions to read data.", user)
        c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to read data."))
        return
    }


	status, err := models.ListStatus(db, models.ParseRequestParameter(c))
	if err != nil {
		logger.Error("List status failed:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List status failed." , err))
		return
	}

	count := 0
	if len(status) > 0 {
		count, _ = models.StatusCount(db)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, status)
}
