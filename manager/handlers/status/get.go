package status

import (
	//	"encoding/json"
	"net/http"
	
	"strconv"

	"gopkg.in/mgo.v2/bson"

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

// Get a device status
func Get(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
    if user.IsManager() && !user.CheckDataRead() {
        logger.Error("You did not have permissions to read data.", user)
        c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to read data."))
        return
    }

	id := c.Param("id")
	if !bson.IsObjectIdHex(id) {
		logger.Error("Device id is invalid:", id)
		c.AbortWithError(http.StatusBadRequest, models.Error("Device id is invalid:" + id))
		return
	}

	status, err := models.ListStatusByDeviceID(db, bson.ObjectIdHex(id), models.ParseRequestParameter(c))
	if err != nil {
		logger.Error("Can't find device status:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Can't find device status." , err))
		return
	}

	count := 0
	if len(status) > 0 {
		count, _ = models.StatusCountByDeviceID(db, bson.ObjectIdHex(id))
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, status)
}
