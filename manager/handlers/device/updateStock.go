package device

import (
	
	"net/http"
	"time"
	
	"gopkg.in/mgo.v2/bson"

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

func updateStockValidate(c *gin.Context) (*models.DeviceMany) {
	deviceMany := models.DeviceMany{}
	if err := c.BindJSON(&deviceMany); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	if deviceMany.StockTime == nil {
		now := time.Now()
		deviceMany.StockTime = &now
	}

	if len(deviceMany.ID) == 0 {
		c.AbortWithError(http.StatusBadRequest, models.Error("devices_id must be some valid value."))
		return nil
	}

	return &deviceMany
}

// UpdateStock 
func UpdateStock(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckDeviceWrite() {
		logger.Error("You did not have permissions to edit device.")
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to edit device."))
		return
	}


	deviceMany := updateStockValidate(c)
	if deviceMany == nil {
		return
	}

	if err := deviceMany.UpdateStock(db); err != nil {
		logger.Error("Mark stock failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Mark stock failed.", err))
		return
	}

	c.Status(http.StatusCreated)
}
