package device

import (
	"net/http"
	"strconv"

	"gopkg.in/mgo.v2/bson"

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

// Search devices
func SearchPhone(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckSearchRead() {
		logger.Error("You did not have permissions to read device.")
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to read device."))
		return
	}

	nu := c.Param("number")
	if !models.RegexpMark.MatchString(nu) {
		logger.Error("The number is invalid.", nu)
		c.AbortWithError(http.StatusBadRequest, models.Error("The number is invalid:", nu))
		return
	}


	m := bson.M{
		"sim_number": bson.M{"$regex": bson.RegEx{nu, ""}},
	}
	//  db.device.find({mark: {$ne: null},  $where: "/.*12.*/.test(obj.mark)" })

	logger.Info("Search fileter:", m)

	rp := &models.RequestParameter{
		Limit : 20,
		Offset : 0,
		Sort: []string{"mark"},
	}

	result, err := models.SearchDevice(db, m, rp, nil)
	if err != nil {
		logger.Error("Can't find device.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Can't find device:", err))
		return
	}

	/*
	count := 0
	if len(device) > 0 {
		count, _ = models.SearchDeviceCount(db, m)
	}
	*/

	c.Header(models.ResultCountHeader, strconv.Itoa(len(result)))
	c.JSON(http.StatusOK, result)
}


