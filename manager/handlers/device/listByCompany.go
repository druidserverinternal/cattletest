package device

import (
	"net/http"
	"strconv"
	"fmt"
	

	"gopkg.in/mgo.v2/bson"

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

// ListByCompany  by company
func ListByCompany(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckDeviceRead() {
		logger.Error("You did not have permissions to read device.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to read device."))
		return
	}

	id := c.Param("id")
	if !bson.IsObjectIdHex(id) {
		logger.Error("Company id is invalid.", id)
		c.AbortWithError(http.StatusBadRequest, models.Error(fmt.Sprintf("Company id:%s is invalid", id)))
		return
	}

	result, err := models.ListDeviceByCompanyID(db, bson.ObjectIdHex(id), models.ParseRequestParameter(c), nil)
	if err != nil {
		logger.Error("List devices by company failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List devices by companyid failed." , err))
		return
	}

	count := 0
	if len(result) > 0 {
		count, err = models.CountDeviceByCompanyID(db, bson.ObjectIdHex(id))
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, result)
}
