package device

import (
	"fmt"
	"net/http"

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"

	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

func destroyValidate(c *gin.Context) (string) {
	deviceID := c.Param("id")
	if !bson.IsObjectIdHex(deviceID) {
		c.AbortWithError(http.StatusBadRequest, models.Error(fmt.Sprintf("Device id:%s is invalid", deviceID)))
		return ""
	}

	return deviceID
}

// Destroy a devices
func Destroy(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckDeviceWrite() {
		logger.Error("You did not have permissions to edit device.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to edit device."))
		return
	}


	deviceID := deleteValidate(c)
	if deviceID == "" {
		return
	}

	device, err := models.FindDeviceByID(db, bson.ObjectIdHex(deviceID), nil)
	if err != nil {
		logger.Error("Find device failed:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find device failed." , err))
		return
	}

	if device.DeletedAt == nil {
		logger.Error("Can't delete a none deleted device.", device.Mark)
		c.AbortWithError(http.StatusBadRequest, models.Error("Can't destroy none deleted device:", device.Mark))
		return
	}

	models.DestroyCellularByDeviceID(db, bson.ObjectIdHex(deviceID))
	models.DestroyStatusByDeviceID(db, bson.ObjectIdHex(deviceID))
	models.DestroyGPSByDeviceID(db, bson.ObjectIdHex(deviceID))
	models.DestroyBehavior2ByDeviceID(db, bson.ObjectIdHex(deviceID))
	models.DestroySettingByDeviceID(db, bson.ObjectIdHex(deviceID))

	if err = models.DestroyDeviceByID(db, bson.ObjectIdHex(deviceID)); err != nil {
		logger.Error("Destroy device failed:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Destroy device failed:" , err))
		return
	}

	logger.Infof("User:%s destroy device:%s", middleware.GetUserID(c), deviceID)
	c.Status(http.StatusNoContent)
}
