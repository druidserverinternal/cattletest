package device

import (
//	"bytes"
	"net/http"
	"time"
	"sync"
//	"archive/zip"
//	"strconv"

	//"github.com/klauspost/compress/zip"
	
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
	"gopkg.in/mgo.v2"

	"druid/cattle/discovery/middleware"
	"druid/cattle/models"
)

func CSVBehavior(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	deviceMany := csvValidate(c)
	if deviceMany == nil {
		return
	}

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckExportRead() {
		logger.Error("You did not have permissions to export csv.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to export csv."))
		return
	}

	if len(deviceMany.ID) == 0 {
		logger.Error("Device count is zero.")
		c.AbortWithError(http.StatusBadRequest, models.Error("Device count is zero."))
		return
	}

	begin, _ := models.ConvertDateToTime(c.Query(models.QueryTimeBegin))
	end, _ := models.ConvertDateToTime(c.Query(models.QueryTimeEnd))
	tt := bson.M{}
	if begin != nil {
		tt["$gte"] = begin
	}
	if end != nil {
		tt["$lte"] = end
	}

	 rp := &models.RequestParameter{
		Sort: []string{"-timestamp"},
	}

	b := time.Now()

	ds := []*models.Device{}


	ch := make(chan []*models.Behavior2, 10)

	wg := sync.WaitGroup{}
	er := make(chan error, 1)

	go func() {
		for _, deviceID := range deviceMany.ID {
			d, _ := models.FindDeviceByID(db, deviceID, nil)
			if d == nil {
				er <- models.Error("Could not find device:", deviceID)
				return
			}

			mt := bson.M{}
			if tt["$gte"] != nil {
				mt["$gte"] = tt["$gte"]
			}
			if tt["$lte"] != nil {
				mt["$lte"] = tt["$lte"]
			}
			if d.StockTime != nil {
				if begin == nil || begin.Sub(*d.StockTime) < 0 {
					mt["$gte"] = d.StockTime
				}
			}
			m := bson.M{"device_id": deviceID}
			if len(mt) > 0 {
				m["timestamp"] = mt
			}
			ds = append(ds, d)

			wg.Add(1)
			go requestBehaviorByDeviceID(db.Clone(), m, rp, &wg, ch)
		}

		wg.Wait()
		ch <- nil
	}()

	behaviors := []*models.Behavior2{}
	for {
		select {
		case behavior := <-ch:
			if behavior == nil {
				goto run
			}
			if len(behavior) == 0 {
				continue
			}
			behaviors = append(behaviors, behavior...)
		case err := <-er:
			logger.Error(err)
			c.AbortWithError(http.StatusInternalServerError, err)
			return
		}
	}

	run:
	if len(behaviors) == 0 {
		logger.Error("Download invalid behavior record.")
		c.Status(http.StatusNoContent)
		return
	}

	logger.Info("request behavior spent:", time.Now().Sub(b))
	b = time.Now()

	csv, err := models.DataToCSV(behaviors)
	if err != nil {
		logger.Error("Behavior convert to csv failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Behavior convert to csv failed.", err))
		return
	}

	logger.Info("Gen csv spent:", time.Now().Sub(b))
	b = time.Now()

	name := "csv-bhv-" + time.Now().Format("2006-01-02T15-04-05Z")
	data, err := models.WriteToZip(name + ".csv", csv)
	if err != nil {
		logger.Error("Write to zip failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Write to zip failed.", err))
		return
	}

	/*

	buf := new(bytes.Buffer)
	z := zip.NewWriter(buf)

	f, err := z.Create("behavior_record.csv")
	if err != nil {
		z.Close()
		logger.Error("Zip create failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Zip create failed.", err))
		return }
	if _, err := f.Write(csv); err != nil {
		z.Close()
		logger.Error("Zip write failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Zip write failed.", err))
		return
	}
	z.Close()
	*/

	logger.Info("zip csv spent:", time.Now().Sub(b))

	logger.Info("Start write csv zip. length:", len(data), len(csv))
	

	c.Header("Content-Disposition", "attachment; filename=" + name + ".zip")
	c.Data(http.StatusOK, "application/octet-stream", data)
//	c.Header("Content-Disposition", "attachment; filename=csv.zip")
//	c.Header("Content-Type", "application/octet-stream")
//	c.Data(http.StatusOK, "application/octet-stream", buf.Bytes())
}

func requestBehaviorByDeviceID(db *mgo.Session, m bson.M, rp *models.RequestParameter, wg *sync.WaitGroup, ch chan []*models.Behavior2) (error) {
	defer wg.Done()
	defer db.Close()
	db.SetMode(mgo.Monotonic, true)

	behavior, err := models.ListBehavior2ByFilter(db, m, rp, models.UserSelect)
	if err != nil {
		return models.Error("List behavior by filter failed.", err)
	}
	if len(behavior) == 0 {
		return nil
	}

	ch <- behavior
	return nil
}




