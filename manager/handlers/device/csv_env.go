package device

import (
//	"bytes"
	"net/http"
	"time"
	"sync"
//	"archive/zip"
//	"strconv"

	//"github.com/klauspost/compress/zip"
	
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
	"gopkg.in/mgo.v2"

	"druid/cattle/discovery/middleware"
	"druid/cattle/models"
)

func csvValidate(c *gin.Context) (*models.DeviceMany) {
	deviceMany := models.DeviceMany{}
	if err := c.BindJSON(&deviceMany); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	if len(deviceMany.ID) > 200 {
		c.AbortWithError(http.StatusBadRequest, models.Error("Request device quantity more than 200."))
		return nil
	}

	return &deviceMany
}

func CSV(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	deviceMany := csvValidate(c)
	if deviceMany == nil {
		return
	}

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckExportRead() {
		logger.Error("You did not have permissions to export csv.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to export csv."))
		return
	}

	if len(deviceMany.ID) == 0 {
		logger.Error("Device count is zero.")
		c.AbortWithError(http.StatusBadRequest, models.Error("Device count is zero."))
		return
	}

	begin, _ := models.ConvertDateToTime(c.Query(models.QueryTimeBegin))
	end, _ := models.ConvertDateToTime(c.Query(models.QueryTimeEnd))
	tt := bson.M{}
	if begin != nil {
		tt["$gte"] = begin
	}
	if end != nil {
		tt["$lte"] = end
	}

	 rp := &models.RequestParameter{
		Sort: []string{"-timestamp"},
	}

	b := time.Now()

	ds := []*models.Device{}


	ch := make(chan []*models.GPS, 10)

	wg := sync.WaitGroup{}
	er := make(chan error, 1)

	go func() {
		for _, deviceID := range deviceMany.ID {
			d, _ := models.FindDeviceByID(db, deviceID, nil)
			if d == nil {
				er <- models.Error("Could not find device:", deviceID)
				return
			}

			mt := bson.M{}
			if tt["$gte"] != nil {
				mt["$gte"] = tt["$gte"]
			}
			if tt["$lte"] != nil {
				mt["$lte"] = tt["$lte"]
			}
			if d.StockTime != nil {
				if begin == nil || begin.Sub(*d.StockTime) < 0 {
					mt["$gte"] = d.StockTime
				}
			}
			m := bson.M{"device_id": deviceID}
			if len(mt) > 0 {
				m["timestamp"] = mt
			}
			ds = append(ds, d)

			wg.Add(1)
			go requestGPSByDeviceID(db.Clone(), m, rp, &wg, ch)
		}

		wg.Wait()
		ch <- nil
	}()

	gpss := []*models.GPS{}
	for {
		select {
		case gps := <-ch:
			if gps == nil {
				goto run
			}
			if len(gps) == 0 {
				continue
			}
			gpss = append(gpss, gps...)
		case err := <-er:
			logger.Error(err)
			c.AbortWithError(http.StatusInternalServerError, err)
			return
		}
	}

	run:
	if len(gpss) == 0 {
		logger.Error("Download invalid gps record.")
		c.Status(http.StatusNoContent)
		return
	}

	logger.Info("request gps spent:", time.Now().Sub(b))
	b = time.Now()

	csv, err := models.DataToCSV(gpss)
	if err != nil {
		logger.Error("GPS convert to csv failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("GPS convert to csv failed.", err))
		return
	}

	logger.Info("Gen csv spent:", time.Now().Sub(b))
	b = time.Now()

	name := "csv-env-" + time.Now().Format("2006-01-02T15-04-05Z")
	data, err := models.WriteToZip(name + ".csv", csv)
	if err != nil {
		logger.Error("Write to zip failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Write to zip failed.", err))
		return
	}

	/*

	buf := new(bytes.Buffer)
	z := zip.NewWriter(buf)

	f, err := z.Create("gps_record.csv")
	if err != nil {
		z.Close()
		logger.Error("Zip create failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Zip create failed.", err))
		return }
	if _, err := f.Write(csv); err != nil {
		z.Close()
		logger.Error("Zip write failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Zip write failed.", err))
		return
	}
	z.Close()
	*/

	logger.Info("zip csv spent:", time.Now().Sub(b))


	logger.Info("Start write csv zip. length:", len(data), len(csv))
	

	c.Header("Content-Disposition", "attachment; filename=" + name + ".zip")
	c.Data(http.StatusOK, "application/octet-stream", data)
//	c.Header("Content-Disposition", "attachment; filename=csv.zip")
//	c.Header("Content-Type", "application/octet-stream")
//	c.Data(http.StatusOK, "application/octet-stream", buf.Bytes())
}

func requestGPSByDeviceID(db *mgo.Session, m bson.M, rp *models.RequestParameter, wg *sync.WaitGroup, ch chan []*models.GPS) (error) {
	defer wg.Done()
	defer db.Close()
	db.SetMode(mgo.Monotonic, true)

	gps, err := models.ListGPSByFilter(db, m, rp, models.UserSelect)
	if err != nil {
		return models.Error("List gps by filter failed.", err)
	}
	if len(gps) == 0 {
		return nil
	}

	ch <- gps
	return nil
}




