package device

import (
	"net/http"
	"strconv"

	"gopkg.in/mgo.v2/bson"

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

func kmlValidate(c *gin.Context) (string) {
	deviceID := c.Param( "id")
	if !bson.IsObjectIdHex(deviceID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("Device is invalid:", deviceID))
		return ""
	}
	return deviceID
}

// Kml all of gps record with device
func Kml(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckExportRead() {
		logger.Error("You did not have permissions to export data.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to export data."))
		return
	}

	deviceID := kmlValidate(c)
	if deviceID == "" {
		return
	}
	parameter := models.ParseRequestParameter(c)
	parameter.Sort = []string{"-timestamp"}

	gps, err := models.ListGPSByDeviceID(db, bson.ObjectIdHex(deviceID), parameter, nil)
	if err != nil {
		logger.Error("Can't find:", deviceID, " gps.")
		c.AbortWithError(http.StatusInternalServerError, models.Error("Can't find device's gps:", err))
		return
	}
	if len(gps) == 0 {
		logger.Error("Device did not have gps record.")
		c.AbortWithError(http.StatusBadRequest, models.Error("Device did not have gps record."))
		return
	}

	kml, err := models.GPSToKml(gps)
	if err != nil {
		logger.Error("GPS convert to kml failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("GPS convert to kml failed.", err))
		return
	}

	name := strconv.Itoa(gps[0].Mark) + "-" + gps[0].UUID + ".kml"
	c.Header("Content-Disposition", "attachment; filename="+name)
	c.Data(http.StatusOK, "application/octet-stream", kml)
}
