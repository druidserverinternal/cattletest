package device

import (
	"bytes"
	"net/http"
	"strconv"
	"archive/zip"
	"time"
	"sync"

	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
	"gopkg.in/mgo.v2"

	"druid/cattle/discovery/middleware"
	"druid/cattle/models"
)

func CSVMultipleBehavior(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	deviceMany := csvValidate(c)
	if deviceMany == nil {
		return
	}

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckExportRead() {
		logger.Error("You did not have permissions to export csv.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to export csv."))
		return
	}


	if len(deviceMany.ID) == 0 {
		logger.Error("Device count is zero.")
		c.AbortWithError(http.StatusBadRequest, models.Error("Device count is zero."))
		return
	}

	begin, _ := models.ConvertDateToTime(c.Query(models.QueryTimeBegin))
	end, _ := models.ConvertDateToTime(c.Query(models.QueryTimeEnd))
	tt := bson.M{}

	if begin != nil {
		tt["$gte"] = begin
	}
	if end != nil {
		tt["$lte"] = end
	}

	rp := &models.RequestParameter{
		Sort: []string{"-timestamp"},
	}


//	csv := []byte(nil)
	ds := []*models.Device{}

	wg := sync.WaitGroup{}
	ch := make(chan *channelCSV, 10)
	exit := false
	er := make(chan error, 1)
	
	go func() {
		for _, deviceID := range deviceMany.ID {
			if exit {
				return
			}
			d, _ := models.FindDeviceByID(db, deviceID, nil)
			if d == nil {
				er <- models.Error("Could not find device:", deviceID)
				return
			}

			mt := bson.M{}
			if tt["$gte"] != nil {
				mt["$gte"] = tt["$gte"]
			}
			if tt["$lte"] != nil {
				mt["$lte"] = tt["$lte"]
			}
			if d.StockTime != nil {
				if begin == nil || begin.Sub(*d.StockTime) < 0 {
					//				logger.Info("found stocktime:", d.StockTime, begin)
					mt["$gte"] = d.StockTime
				}
			}
			m := bson.M{"device_id": deviceID}
			if len(mt) > 0 {
				m["timestamp"] = mt
			}
			ds = append(ds, d)

			wg.Add(1)
			go requestBehaviorByDeviceIDToCSV(db.Clone(), m, rp, &wg, ch)
		}

		wg.Wait()
		ch <- (*channelCSV)(nil)
	}()


	buf := new(bytes.Buffer)
	z := zip.NewWriter(buf)

	for {
		select {
		case cs := <-ch:
			if cs == nil {
				z.Close()
				goto run
			}
			f, err := z.Create(strconv.Itoa(cs.Mark) + "-" + cs.UUID + ".csv")
			if err != nil {
				exit = true
				z.Close()
				logger.Error("Zip create failed.", err)
				c.AbortWithError(http.StatusInternalServerError, models.Error("Zip create failed.", err))
				return
			}
			_, err = f.Write(cs.Data)
			if err != nil {
				exit = true
				z.Close()
				logger.Error("Zip write failed.", err)
				c.AbortWithError(http.StatusInternalServerError, models.Error("Zip write failed.", err))
				return
			}
		case err := <- er:
			z.Close()
			logger.Error(err)
			c.AbortWithError(http.StatusInternalServerError, err)
			return
		}
	}

	run:

	logger.Info("Start write csv zip. length:", len(buf.Bytes()))
	//c.Header(models.ResultCountHeader, strconv.Itoa(count))
	//	w.WriteFile(http.StatusOK, "csv.zip", buf.Bytes())

	name := "csv-bhv-all-" + time.Now().Format("2006-01-02T15-04-05") + ".zip"
	c.Header("Content-Disposition", "attachment; filename=" + name)
	c.Data(http.StatusOK, "application/octet-stream", buf.Bytes())
}


func requestBehaviorByDeviceIDToCSV(db *mgo.Session, m bson.M, rp *models.RequestParameter, wg *sync.WaitGroup, ch chan *channelCSV) (error) {
	defer wg.Done()
	defer db.Close()
	db.SetMode(mgo.Monotonic, true)

	behaviors, err := models.ListBehavior2ByFilter(db, m, rp, models.UserSelect)
	if err != nil {
		return models.Error("List behavior by filter failed.", err)
	}
	if len(behaviors) == 0 {
		return nil
	}

	csv, err := models.DataToCSV(behaviors)
	if err != nil {
		return models.Error("Behavior convert to csv failed.", err)
	}
	if len(csv) == 0 {
		return nil
	}

	ch <- &channelCSV{
		Mark	: behaviors[0].Mark, 
		UUID	: behaviors[0].UUID,
		Data	: csv,
	}
	return nil
}

