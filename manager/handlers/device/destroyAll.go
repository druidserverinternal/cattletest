package device

import (
	"net/http"
	
	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"

	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

// Destroy a devices
func DestroyAll(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckDeviceWrite() {
		logger.Error("You did not have permissions to edit device.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to edit device."))
		return
	}

	devices, err := models.ListDeviceByDeleted(db, nil, nil)
	if err != nil {
		logger.Error("List device failed:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List deleted device failed:" , err))
		return
	}
	for _, device := range devices {
		if device.DeletedAt == nil {
			continue
		}

		models.DestroyCellularByDeviceID(db, device.ID)
		models.DestroyStatusByDeviceID(db, device.ID)
		models.DestroyGPSByDeviceID(db, device.ID)
		models.DestroyBehavior2ByDeviceID(db, device.ID)
		models.DestroySettingByDeviceID(db, device.ID)

		if err = models.DestroyDeviceByID(db, device.ID); err != nil {
			logger.Error("Destroy device failed:", err)
			continue
		}

		logger.Infof("User:%s destroy device:%s", middleware.GetUserID(c), device.ID)
	}

	c.Status(http.StatusNoContent)
}
