package device

import (
//	"bytes"
	"net/http"
	"time"
//	"archive/zip"
//	"strconv"

	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
//	"gopkg.in/mgo.v2"

	"druid/cattle/discovery/middleware"
	"druid/cattle/models"
)

func CSVDevice(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	deviceMany := csvValidate(c)
	if deviceMany == nil {
		return
	}

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckExportRead() {
		logger.Error("You did not have permissions to export csv.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to export csv."))
		return
	}

	if len(deviceMany.ID) == 0 {
		logger.Error("Device count is zero.")
		c.AbortWithError(http.StatusBadRequest, models.Error("Device count is zero."))
		return
	}

	gpss, err := models.ListDeviceByIDs(db, deviceMany.ID, models.UserSelect)
	if err != nil {
		logger.Error("List device by ids failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List device by ids failed.", err))
		return
	}

	csv, err := models.DataToCSV(gpss)
	if err != nil {
		logger.Error("GPS convert to csv failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("GPS convert to csv failed.", err))
		return
	}

	name := "csv-device-" + time.Now().Format("2006-01-02T15-04-05Z")
	data, err := models.WriteToZip(name + ".csv", csv)
	if err != nil {
		logger.Error("Write to zip failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Write to zip failed.", err))
		return
	}

	logger.Info("Start write csv zip. length:", len(data), len(csv))

	c.Header("Content-Disposition", "attachment; filename=" + name + ".zip")
	c.Data(http.StatusOK, "application/octet-stream", data)
}

