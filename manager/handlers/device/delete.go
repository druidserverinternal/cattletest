package device

import (
	"fmt"
	"net/http"

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"

	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

func deleteValidate(c *gin.Context) (string) {
	deviceID := c.Param("id")
	if !bson.IsObjectIdHex(deviceID) {
		c.AbortWithError(http.StatusBadRequest, models.Error(fmt.Sprintf("Device id:%s is invalid", deviceID)))
		return ""
	}

	return deviceID
}

// Delete a devices
func Delete(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckDeviceWrite() {
		logger.Error("You did not have permissions to edit device.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to edit device."))
		return
	}

	deviceID := deleteValidate(c)
	if deviceID == "" {
		return
	}

	device, err := models.FindDeviceByID(db, bson.ObjectIdHex(deviceID), nil)
	if err != nil {
		logger.Error("Find device failed:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find device failed." , err))
		return
	}
	if device.DeletedAt != nil {
		logger.Error("Can't delete a deleted device.")
		c.AbortWithError(http.StatusBadRequest, models.Error("Can't delete a deleted device."))
		return
	}
	if device.CompanyID != "" {
		logger.Error("Can't delete used device.")
		c.AbortWithError(http.StatusBadRequest, models.Error("Can't delete used device."))
		return
	}

//	models.RemoveUserFromDevices(db, []bson.ObjectId{bson.ObjectIdHex(deviceID)})
//	models.RemoveDevicesFromDitu(db, []bson.ObjectId{bson.ObjectIdHex(deviceID)})
	models.DeleteStatusByDeviceID(db, bson.ObjectIdHex(deviceID))
	models.DeleteCellularByDeviceID(db, bson.ObjectIdHex(deviceID))
	models.DeleteGPSByDeviceID(db, bson.ObjectIdHex(deviceID))
	models.DeleteBehavior2ByDeviceID(db, bson.ObjectIdHex(deviceID))
	models.DeleteSettingByDeviceID(db, bson.ObjectIdHex(deviceID))

	if err = models.DeleteDeviceByID(db, device.ID); err != nil {
		logger.Error("Delete device failed:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Delete device failed:" , err))
		return
	}

	logger.Infof("User:%s deleted device:%s", middleware.GetUserID(c), deviceID)
	c.Status(http.StatusNoContent)
}
