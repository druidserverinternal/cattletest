package device

import (
	"fmt"
	"net/http"
	"time"
	

	"gopkg.in/mgo.v2/bson"

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

func recoverValidate(c *gin.Context) (string) {
	deviceID := c.Param("id")
	if !bson.IsObjectIdHex(deviceID) {
		c.AbortWithError(http.StatusBadRequest, models.Error(fmt.Sprintf("Device id:%s is invalid", deviceID)))
		return ""
	}

	return deviceID
}

func Recover(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckDeviceWrite() {
		logger.Error("You did not have permissions to edit device.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to edit device."))
		return
	}

	deviceID := recoverValidate(c)
	if deviceID == "" {
		return
	}

	device, err := models.FindDeviceByID(db, bson.ObjectIdHex(deviceID), nil)
	if err != nil {
		logger.Error("Find device failed:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find device failed." , err))
		return
	}
	if device.DeletedAt == nil {
		logger.Error("Can't recover a normal device.")
		c.AbortWithError(http.StatusBadRequest, models.Error("Can't recover a normal device."))
		return
	}


	if err = models.DeviceLockTimeout(db, device.ID, time.Now().Add(time.Second)); err != nil {
		logger.Error("Can't lock the device.", device.Mark)
		c.AbortWithError(http.StatusRequestTimeout, models.Error("Can't lock the device:" + device.ID.Hex()))
		return
	}
	defer models.DeviceUnLock(db, device.ID)


	models.RecoverSettingByDeviceID(db, device.ID)
	models.RecoverGPSByDeviceID(db, device.ID)
	models.RecoverBehavior2ByDeviceID(db, device.ID)
	models.RecoverStatusByDeviceID(db, device.ID)

	if err = models.RecoverDeviceByID(db, device.ID); err != nil {
		logger.Error("Recover device failed:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Recover device failed:" , err))
		return
	}

	logger.Infof("User:%s recover device:%s", middleware.GetUserID(c), deviceID)
	c.Status(http.StatusNoContent)
}
