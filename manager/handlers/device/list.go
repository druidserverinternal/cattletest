package device

import (
	"net/http"
	"strconv"
	
	"gopkg.in/mgo.v2/bson"

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

// List device
func List(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckDeviceRead() {
		logger.Error("You did not have permissions to read device.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to read device."))
		return
	}

	rp := models.ParseRequestParameter(c)
	if rp.LimitOrign > rp.Limit {
		rp.Limit = rp.LimitOrign
	} else if rp.LimitOrign == 0 {
		rp.Limit = 0
	}

	result, err := models.ListDevice(db, rp, nil)
	if err != nil {
		logger.Error("List devices failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List devices failed." , err))
		return
	}

	count := 0
	if len(result) > 0 {
		count, err = models.DeviceCount(db)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, result)
}
