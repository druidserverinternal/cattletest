package device

import (
	"encoding/json"
	"net/http"

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"

	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

func updateManyValidate(c *gin.Context) (*models.DeviceMany) {
	deviceMany := models.DeviceMany{}
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&deviceMany); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	if deviceMany.CompanyID == "" {
		c.AbortWithError(http.StatusBadRequest, models.Error("company_id must be some valid value."))
		return nil
	}

	if len(deviceMany.ID) == 0 {
		c.AbortWithError(http.StatusBadRequest, models.Error("devices_id must be some valid value."))
		return nil
	}

	return &deviceMany
}

// UpdateMany a deviceMany
func UpdateMany(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckDeviceWrite() {
		logger.Error("You did not have permissions to edit device.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to edit device."))
		return
	}

	deviceMany := updateManyValidate(c)
	if deviceMany == nil {
		return
	}

	devices, err := models.CheckDevicesByManyID(db, deviceMany.ID, nil)
	if err != nil || len(deviceMany.ID) != len(devices) {
		logger.Error("Could not find some device")
		c.AbortWithError(http.StatusBadRequest, models.Error("Could not find some device.", devices))
		return
	}

	for _, device := range devices {
		if device.CompanyID != "" {
			logger.Error("You can't update used device", device)
			c.AbortWithError(http.StatusBadRequest, models.Error("You can't update used device.", device))
			return
		}
	}

	companyName := models.GetCompanyNameByID(db, deviceMany.CompanyID)
	if companyName == "" {
		logger.Error("Company id is invalid.", deviceMany.CompanyID.Hex())
		c.AbortWithError(http.StatusBadRequest, models.Error("Company id is invalid:", deviceMany.CompanyID))
		return
	}
	deviceMany.CompanyName = companyName

	if err = deviceMany.UpdateDeviceCompany(db); err != nil {
		logger.Error("Update device by company id failed. ", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Update device by company id failed.", err))
		return
	}

	c.Status(http.StatusCreated)
}
