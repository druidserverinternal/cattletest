package device

import (
	"fmt"
	"net/http"
	

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"

	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

// Get a device by id
func Get(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckDeviceRead() {
		logger.Error("You did not have permissions to read device.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to read device."))
		return
	}

	deviceID := c.Param("id")
	if !bson.IsObjectIdHex(deviceID) {
		logger.Error("Device id is invalid.", deviceID)
		c.AbortWithError(http.StatusBadRequest, models.Error(fmt.Sprintf("Device id:%s is invalid", deviceID)))
		return
	}

	device, _ := models.FindDeviceByID(db, bson.ObjectIdHex(deviceID), nil)
	if device == nil {
		logger.Error("The device id is invalid.", deviceID)
		c.AbortWithError(http.StatusInternalServerError, models.Error("The device id is invalid:" + deviceID))
		return
	}

	c.JSON(http.StatusOK, device)
}
