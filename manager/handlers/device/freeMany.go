package device

import (
	"encoding/json"
	"net/http"
	
	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"

	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

func freeManyValidate(c *gin.Context) (*models.DeviceMany) {
	deviceMany := models.DeviceMany{}
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&deviceMany); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	if len(deviceMany.ID) == 0 {
		c.AbortWithError(http.StatusBadRequest, models.Error("devices_id must be some valid value."))
		return nil
	}

	return &deviceMany
}

func FreeMany(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckDeviceWrite() {
		logger.Error("You did not have permissions to edit device.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to edit device."))
		return
	}


	deviceMany := freeManyValidate(c)
	if deviceMany == nil {
		return
	}

	devices, err := models.CheckDevicesByManyID(db, deviceMany.ID, nil)
	if err != nil || len(deviceMany.ID) != len(devices) {
		logger.Error("Could not find some device", devices)
		c.AbortWithError(http.StatusBadRequest, models.Error("Could not find some device.", devices))
		return
	}

	for _, device := range devices {
		if device.OwnerID != "" {
			logger.Error("You can't free used device", device)
			c.AbortWithError(http.StatusBadRequest, models.Error("You can't free used device.", device))
			return
		}
	}

	if err = models.ClearManyGPSCompany(db, deviceMany.ID); err != nil {
		logger.Error("Update gps by company id failed. ", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Clear gps by company id failed.", err))
		return
	}

	if err = models.ClearManyBehavior2Company(db, deviceMany.ID); err != nil {
		logger.Error("Update behavior by company id failed. ", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Clear behavior by company id failed.", err))
		return
	}

	if err = deviceMany.ClearSettingCompany(db); err != nil {
		logger.Error("Clear setting by company id failed. ", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Clear setting by company id failed.", err))
		return
	}

	if err = deviceMany.ClearDeviceCompany(db); err != nil {
		logger.Error("Clear device by company id failed. ", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Clear device by company id failed.", err))
		return
	}

	c.Status(http.StatusOK)
}
