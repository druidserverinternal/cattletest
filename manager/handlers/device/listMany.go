package device

import (
	"net/http"
	
	"encoding/json"
	//	"regexp"
	//	"strings"

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/admin/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

func listManyValidate(c *gin.Context) (*models.DeviceMany) {
	devices := models.DeviceMany{}
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&devices); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	if len(devices.ID) == 0 {
		ds := []*models.Device{}
		c.JSON(http.StatusOK, ds)
		return nil
	}

	return &devices
}


// ListMany get many devices by id
func ListMany(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	devices := listManyValidate(c)
	if devices == nil {
		return
	}

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckDeviceRead() {
		logger.Error("You did not have permissions to read device.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to read device."))
		return
	}

	m := bson.M{
		"_id": bson.M{"$in": devices.ID },
	}

	ds, err := models.ListDeviceByFilter(db, m, nil, models.UserSelect)
	if err != nil {
		logger.Warning("Find device by ID failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find device by ID failed:", err))
		return
	}

	c.JSON(http.StatusOK, ds)
}
