package device

import (
//	"encoding/json"
	"net/http"
	
	//	"regexp"

	//	"github.com/spf13/viper"

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/discovery/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

func updateDescriptionValidate(c *gin.Context) (*models.DeviceMany) {
	deviceMany := models.DeviceMany{}
	if err := c.BindJSON(&deviceMany); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	if len(deviceMany.ID) == 0 {
		c.AbortWithError(http.StatusBadRequest, models.Error("devices_id must be some valid value."))
		return nil
	}

	return &deviceMany
}


func UpdateDescription(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckDeviceWrite() {
		logger.Error("You did not have permissions to edit device.")
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to edit device."))
		return
	}


	deviceMany := updateDescriptionValidate(c)
	if deviceMany == nil {
		return
	}

	if err := deviceMany.UpdateDescription(db); err != nil {
		logger.Error("Update device failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Update device failed.", err))
		return
	}

	c.Status(http.StatusCreated)
}
