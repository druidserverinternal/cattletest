package device

import (
	"bytes"
	"net/http"
	"strconv"
	"archive/zip"
	
	"gopkg.in/mgo.v2/bson"

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

func kmlMultipleValidate(c *gin.Context) (*models.DeviceMany) {
	deviceMany := models.DeviceMany{}
	if err := c.BindJSON(&deviceMany); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	return &deviceMany
}

// KmlMultiple all of gps record with device
func KmlMultiple(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckExportRead() {
		logger.Error("You did not have permissions to export data.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to export data."))
		return
	}

	deviceMany := kmlMultipleValidate(c)
	if deviceMany == nil {
		return
	}

	if len(deviceMany.ID) == 0 {
		logger.Error("Device count is zero.")
		c.AbortWithError(http.StatusBadRequest, models.Error("Device count is zero."))
		return
	}

	parameter := &models.RequestParameter{
		Sort: []string{"timestamp"},
	}

	buf := new(bytes.Buffer)
	z := zip.NewWriter(buf)

	for _, deviceID := range deviceMany.ID {
		gps, err := models.ListGPSByDeviceID(db, deviceID, parameter, nil)
		if err != nil {
			z.Close()
			logger.Error("Can't find:", deviceID.Hex(), " gps.")
			c.AbortWithError(http.StatusInternalServerError, models.Error("Can't find device's gps.", err))
			return
		}
		if len(gps) == 0 {
			continue
			/*
				logger.Error("Device did not have gps record.")
				w.WriteError(
					common.NewStatusError(
						http.StatusBadRequest,
						"Device did not have gps record."))
				return
			*/
		}

		kml, err := models.GPSToKml(gps)
		if err != nil {
			z.Close()
			logger.Error("GPS convert to kml failed.", err)
			c.AbortWithError(http.StatusInternalServerError, models.Error("GPS convert to kml failed.", err))
			return
		}

		f, err := z.Create(strconv.Itoa(gps[0].Mark)  + "-" + gps[0].UUID + ".kml")
		if err != nil {
			z.Close()
			logger.Error("Zip create failed.", err)
			c.AbortWithError(http.StatusInternalServerError, models.Error("Zip create failed.", err))
			return
		}
		_, err = f.Write(kml)
		if err != nil {
			z.Close()
			logger.Error("Zip write failed.", err)
			c.AbortWithError(http.StatusInternalServerError, models.Error("Zip write failed.", err))
			return
		}
	}
	z.Close()

	logger.Info("Start write kml zip. length:", len(buf.Bytes()))

	c.Header("Content-Disposition", "attachment; filename=kml.zip")
	c.Data(http.StatusOK, "application/octet-stream", buf.Bytes())
}
