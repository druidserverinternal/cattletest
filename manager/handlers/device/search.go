package device

import (
	"net/http"
	"strconv"
	

	"gopkg.in/mgo.v2/bson"

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

// Search devices
func Search(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckSearchRead() {
		logger.Error("You did not have permissions to search.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to search."))
		return
	}

	sn := c.Param("sn")
	if !models.RegexpMark.MatchString(sn) {
		logger.Error("The sn is invalid.", sn)
		c.AbortWithError(http.StatusBadRequest, models.Error("sn must be a valid sn"))
		return
	}

	rp := &models.RequestParameter{
		Limit : 20,
		Offset : 0,
		Sort: []string{"mark"},
	}

	m := bson.M{}
	m["mark"] = bson.M{"$ne": nil}
	reg := "/.*" + sn + ".*/.test(this.mark)"
	m["$where"] = reg
	//  db.device.find({mark: {$ne: null},  $where: "/.*12.*/.test(obj.mark)" })

	logger.Info("Search fileter:", m)

	devices, err := models.SearchDevice(db, m, rp, nil)
	if err != nil {
		logger.Error("Search devices failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Search devices failed." , err))
		return
	}

	count := 0
	if len(devices) > 0 {
		count, _ = models.SearchDeviceCount(db, m)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, devices)
}


