package company

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	

	"gopkg.in/mgo.v2/bson"

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

func updateValidate(c *gin.Context) (*models.Company) {
	var company models.Company
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&company); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	companyID := c.Param("id")
	if !bson.IsObjectIdHex(companyID) {
		c.AbortWithError(http.StatusBadRequest, models.Error(fmt.Sprintf("Company id:%s is invalid", companyID)))
		return nil
	}
	company.ID = bson.ObjectIdHex(companyID)

	company.Address = strings.TrimSpace(company.Address)
	company.Phone = strings.TrimSpace(company.Phone)
	//company.CompanyName = ""

	return &company
}

// Update a company infomation
func Update(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckCompanyWrite() {
		logger.Error("You did not have permissions to edit company.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to edit company."))
		return
	}

	company := updateValidate(c)
	if company == nil {
		return
	}


	originalCompany := models.FindCompanyByID(db, company.ID)
	if originalCompany == nil {
		logger.Error("The company id is invalid:", company.ID)
		c.AbortWithError(http.StatusBadRequest, models.Error("The company id is invalid:", company.ID))
		return
	}

	if err := company.Update(db); err != nil {
		logger.Error("Update company failed:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Update company failed:", err))
		return
	}

	logger.Infof("User:%s updated company:%s", middleware.GetUserID(c), company.ID)
	c.JSON(http.StatusCreated, company)
}
