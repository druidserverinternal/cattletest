package company

import (
	"net/http"
	
	"strconv"

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"

	"gopkg.in/mgo.v2/bson"
)

// List companies list
func List(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckCompanyRead() {
		logger.Error("You did not have permissions to read company.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to read company."))
		return
	}

	companies, err := models.ListCompanies(db, models.ParseRequestParameter(c))
	if err != nil {
		logger.Error("List companies failed:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List companies failed.", err))
		return
	}

	for i, company := range companies {
		companies[i].DeviceCount, _ = models.CountDeviceByCompanyID(db, company.ID)
		companies[i].UserCount, _ = models.UserCountByCompanyID(db, company.ID)
	}

	count := 0
	if len(companies) > 0 {
		count, _ = models.CompanyCount(db)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, companies)
}
