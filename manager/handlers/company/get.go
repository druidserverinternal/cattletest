package company

import (
	"fmt"
	"net/http"
	

	"gopkg.in/mgo.v2/bson"

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

func getValidate(c *gin.Context) (string) {
	companyID := c.Param("id")
	if !bson.IsObjectIdHex(companyID) {
		c.AbortWithError(http.StatusBadRequest, models.Error(fmt.Sprintf("Company id:%s is invalid", companyID)))
		return ""
	}

	return companyID
}

// Get a company by id
func Get(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckCompanyRead() {
		logger.Error("You did not have permissions to read company.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to read company."))
		return
	}

	companyID := getValidate(c)
	if companyID == "" {
		return
	}

	company := models.FindCompanyByID(db, bson.ObjectIdHex(companyID))
	if company == nil {
		logger.Error("The company id is invalid:", companyID)
		c.AbortWithError(http.StatusBadRequest, models.Error("The company id is invalid:", companyID))
		return
	}

	company.DeviceCount, _ = models.CountDeviceByCompanyID(db, company.ID)
	company.UserCount, _ = models.UserCountByCompanyID(db, company.ID)

	c.JSON(http.StatusOK, company)
}
