package company

import (
	"fmt"
	"net/http"
	"gopkg.in/mgo.v2/bson"

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

func deleteValidate(c *gin.Context) (string) {
	companyID := c.Param("id")
	if !bson.IsObjectIdHex(companyID) {
		c.AbortWithError(http.StatusBadRequest, models.Error(fmt.Sprintf("Company id:%s is invalid", companyID)))
		return ""
	}

	return companyID
}

// Delete is mark a company to deleted
func Delete(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckCompanyWrite() {
		logger.Error("You did not have permissions to edit company.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to edit company."))
		return
	}

	companyID := deleteValidate(c)
	if companyID == "" {
		return
	}

	count, err := models.CountDeviceByCompanyID(db, bson.ObjectIdHex(companyID))
	if err != nil {
		logger.Error("Count device by companyid failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Count device by companyid failed.", err))
		return
	}
	if count != 0 {
		logger.Error("Can't delete none empty company.")
		c.AbortWithError(http.StatusBadRequest, models.Error("Can't delete none empty company."))
		return
	}


	if err := models.DeleteUsersByCompanyID(db, bson.ObjectIdHex(companyID)); err != nil {
		logger.Error("Delete company users failed:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Delete company users failed:", err))
		return
	}

	if err := models.DeleteCompanyByID(db, bson.ObjectIdHex(companyID)); err != nil {
		logger.Error("Delete company failed:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Delete company failed:", err))
		return
	}

	logger.Infof("User:%s deleted company:%s", middleware.GetUserID(c), companyID)
	c.Status(http.StatusNoContent)
}
