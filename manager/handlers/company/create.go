package company

import (
	"encoding/json"
	"net/http"
	"strings"
	

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/manager/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

func createValidate(c *gin.Context) (*models.Company) {
	var company models.Company
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&company); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	company.CompanyName = strings.TrimSpace(company.CompanyName)
	if !models.RegexpCompanyName.MatchString(company.CompanyName) {
		c.AbortWithError(http.StatusBadRequest, models.Error("company-name must be a valid name."))
		return nil
	}

	company.Address = strings.TrimSpace(company.Address)
	company.Phone = strings.TrimSpace(company.Phone)

	return &company
}

// Create a new company
func Create(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckCompanyWrite() {
		logger.Error("You did not have permissions to edit company.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to edit company."))
		return
	}

	company := createValidate(c)
	if company == nil {
		return
	}

	if company.CompanyName == "None" {
		logger.Error("The company name is exsisted.")
		c.AbortWithError(http.StatusBadRequest, models.Error("The company name is exsisted."))
		return
	}

	if models.FindCompanyByName(db, company.CompanyName) != nil {
		logger.Error("The company name is exsisted:", company.CompanyName)
		c.AbortWithError(http.StatusInternalServerError, models.Error("The company name is exsisted:", company.CompanyName))
		return
	}

	if err := company.Create(db); err != nil {
		logger.Error("Create company failed:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Create company failed:", err))
		return
	}

	logger.Infof("User:%s created company:%s", middleware.GetUserID(c), company.CompanyName)

	c.JSON(http.StatusCreated, company)
}
