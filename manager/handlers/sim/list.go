package sim

import (
	"net/http"
	"strconv"
	

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"

	"gopkg.in/mgo.v2/bson"
)

// List all of sim record
func List(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckSIMRead() {
		logger.Error("You did not have permissions to view sim.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to view sim."))
		return
	}

	sim, err := models.ListSIM(db, models.ParseRequestParameter(c))
	if err != nil {
		logger.Error("List sims failed:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List sims failed." + err.Error()))
		return
	}

	count := 0
	if len(sim) > 0 {
		count, _ = models.SIMCount(db)
	}
	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, sim)
}
