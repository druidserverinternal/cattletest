package sim

import (
	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"

	
	"net/http"

	"gopkg.in/mgo.v2/bson"
)

func deleteValidate(c *gin.Context) (string) {
	simID := c.Param("id")
	if !bson.IsObjectIdHex(simID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("SIM id is invalid:" + simID))
		return ""
	}

	return simID
}

// Delete sim info
func Delete(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckSIMWrite() {
		logger.Error("You did not have permissions to edit sim.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to edit sim."))
		return
	}

	simID := deleteValidate(c)
	if simID == "" {
		return
	}

	if err := models.DeleteSIMByID(db, bson.ObjectIdHex(simID)); err != nil {
		logger.Error("Delete sim failed:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Delete sim failed." , err))
		return
	}

	logger.Infof("User:%s deleted sim:%s", middleware.GetUserID(c), simID)
	c.Status(http.StatusNoContent)
}
