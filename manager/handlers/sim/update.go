package sim

import (
	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"

	"encoding/json"
	"net/http"
	"strings"
	

	"gopkg.in/mgo.v2/bson"
)

func updateValidate(c *gin.Context) (*models.SIM) {
	sim := models.SIM{}
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&sim); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	simID := c.Param("id")
	if !bson.IsObjectIdHex(simID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("SIM id is invalid:" + simID))
		return nil
	}
	sim.ID = bson.ObjectIdHex(simID)

	sim.IMSI = strings.TrimSpace(sim.IMSI)
	sim.SIMNumber = strings.TrimSpace(sim.SIMNumber)
	if sim.IMSI == "" || sim.SIMNumber == "" {
		c.AbortWithError(http.StatusBadRequest, models.Error("Imsi or SIMNumber is invalid."))
		return nil
	}

	return &sim
}

// Update sim info
func Update(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckSIMWrite() {
		logger.Error("You did not have permissions to edit sim.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to edit sim."))
		return
	}

	sim := updateValidate(c)
	if sim == nil {
		return
	}

	s, _ := models.FindSIMByID(db, sim.ID)
	if s == nil {
		logger.Error("Find sims failed.")
		c.AbortWithError(http.StatusBadRequest, models.Error("Imsi is invalid:" + sim.ID.Hex()))
		return
	}

	if err := sim.Update(db); err != nil {
		logger.Error("Update sim failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Update sims failed." , err))
		return
	}
	logger.Infof("User:%s update sim:%+v", middleware.GetUserID(c), sim)

	c.Status(http.StatusCreated)
}
