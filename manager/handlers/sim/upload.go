package sim

import (
	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"

	"encoding/json"
	"net/http"
	

	"gopkg.in/mgo.v2/bson"
)

func uploadValidate(c *gin.Context) (*models.SIM) {
	sim := &models.SIM{}
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&sim); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	return sim
}

// Upload many sim info
func Upload(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckSIMWrite() {
		logger.Error("You did not have permissions to edit sim.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to edit sim."))
		return
	}


	sim := uploadValidate(c)
	if sim == nil {
		return
	}

	if !sim.Verify() {
		logger.Error("Verify sim failed.")
		c.AbortWithError(http.StatusBadRequest, models.Error("Some sim info is invalid."))
		return
	}

	s, _ := models.FindSIMByIMSI(db, sim.IMSI)
	if s != nil && s.SIMNumber == sim.SIMNumber {
		logger.Warning("SIM info equal old sim:", *s)
		c.AbortWithError(http.StatusBadRequest, models.Error("SIM info equal old sim:." , *sim))
		return
	}

	if err := sim.UpdateNumberByIMSI(db); err != nil {
		logger.Error("Update sim failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Upload sims failed." , err))
		return
	}
	logger.Infof("User:%s upload sim:%+v", middleware.GetUserID(c), sim)

	c.Status(http.StatusCreated)
}
