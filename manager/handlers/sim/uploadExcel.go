package sim

import (
	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"

	"gopkg.in/mgo.v2/bson"

	
	
	"io/ioutil"
	"net/http"
	"strconv"
)

func uploadExcelValidate(c *gin.Context) ([]byte) {
	data, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to read the request." , err))
		return nil
	}

	size, err := strconv.Atoi(c.Request.Header.Get("Content-Length"))
	if err != nil || len(data) != size {
		c.AbortWithError(http.StatusBadRequest, models.Error("Recv file length failed."))
		return nil
	}
	if size == 0 {
		c.AbortWithError(http.StatusBadRequest, models.Error("FileSize is invalid."))
		return nil
	}
	return data
}

// UploadIndex upload sim excel
func UploadIndex(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckSIMWrite() {
		logger.Error("You did not have permissions to edit sim.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to edit sim."))
		return
	}

	data := uploadExcelValidate(c)
	if data == nil {
		return
	}

	sims, err := models.ExcelToSIM(data)
	if err != nil {
		logger.Error("Convert to structs failed:", err)
		c.AbortWithError(http.StatusBadRequest, models.Error("Convert excel to struct failed." , err))
		return
	}

	if len(sims) == 0 {
		logger.Error("Upload excel is invalid.", err)
		c.AbortWithError(http.StatusBadRequest, models.Error("Upload excel is invalid."))
		return
	}
	for _, sim := range sims {
		if sim.IMSI == "" || sim.SIMNumber == "" {
			logger.Warn("Imsi or No is nil:", sim.IMSI, sim.SIMNumber)
			continue
		}
		s, _ := models.FindSIMByIMSI(db, sim.IMSI)
		if s != nil && s.SIMNumber == sim.SIMNumber {
			logger.Warnf("SIM info equal old sim:%+v\n", s)
			continue
		}
	//	logger.Info("Update sim:", sim.Imsi, sim.SIMNumber)
		if err = sim.UpdateNumberByIMSI(db); err != nil {
			logger.Error("Update number failed.", err)
			c.AbortWithError(http.StatusInternalServerError, models.Error("Upload number failed." , err))
			return
		}
		logger.Infof("User:%s upload sim:%+v", middleware.GetUserID(c), sim)
	}

	c.Status(http.StatusCreated)
}
