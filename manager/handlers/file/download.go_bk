package file

import (
	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"

	"net/http"
	"strconv"

	"gopkg.in/mgo.v2/bson"
)

func Download(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	id := c.Param("id")

	file, err := models.FindFileByID(db, bson.ObjectIdHex(id))
	if err != nil {
		logger.Error("Find file from request id failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find file from request id failed, ", err))
		return
	}

	if err := file.ReadFile(db); err != nil {
		logger.Error("Read file from request failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Read file from request failed, ", err))
		return
	}

	logger.Info("Start download file. length:", file.Size, len(file.Data))

	c.Header("Content-Type", "application/octet-stream")
	c.Header("Content-Length", strconv.Itoa(file.Size))
	c.Header("Content-Disposition", "attachment; filename=" + file.Name)


	c.Data(http.StatusOK, "application/octet-stream", file.Data)
}
