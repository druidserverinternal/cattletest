package algorithm

import (
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
	"encoding/json"
	"net/http"

	"github.com/gin-gonic/gin"
)

func updateODBAValidate(c *gin.Context) *models.ODBAAlgorithm {
	odbaAlgo := models.ODBAAlgorithm{}

	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&odbaAlgo); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode this requset.", err))
		return nil
	}

	if odbaAlgo.ActSlop == 0 || odbaAlgo.ActThreshold == 0 {
		c.AbortWithError(http.StatusBadRequest, models.Error("parameter is err: ActThreshold is ", odbaAlgo.ActThreshold, "  ActSlop is :", odbaAlgo.ActSlop))
		return nil
	}

	return &odbaAlgo
}

func UpdateODBAParameter(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	odbaAlgo := updateODBAValidate(c)
	if odbaAlgo == nil {
		return
	}
	odbaAlgo.Version = 1

	lastOdbaAlg := models.GetODBAAlgorithm(db)
	if lastOdbaAlg != nil {
		odbaAlgo.Version = lastOdbaAlg.Version + 1
	}
	err := odbaAlgo.Create(db)
	if err != nil {
		logger.Error("Create odbaAlgo is err")
		c.AbortWithError(http.StatusBadRequest, models.Error("Create odbaAlgo is err"))
	}

	c.Status(http.StatusOK)

}
