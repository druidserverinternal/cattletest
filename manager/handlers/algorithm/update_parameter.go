package algorithm

import (
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"

	"archive/zip"

	"gopkg.in/mgo.v2/bson"

	"github.com/gin-gonic/gin"
)

func updateParameterValidate(c *gin.Context) *models.BehaviorAlgorithm {
	behaviorAlgorithm := models.BehaviorAlgorithm{}

	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&behaviorAlgorithm); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}
	if behaviorAlgorithm.MotionLimen1 == 0 {
		behaviorAlgorithm.MotionLimen1 = models.MotionODBALimen1
	}

	if behaviorAlgorithm.MotionLimen2 == 0 {
		behaviorAlgorithm.MotionLimen2 = models.MotionODBALimen2
	}

	return &behaviorAlgorithm

}

func UpdateParameter111(c *gin.Context) {
	if err := c.Request.ParseForm(); err != nil {
		// handle error
	}

	//c.Request.ParseMultipartForm(32 << 20)
	fmt.Println("mnotio:", c.Request.MultipartForm)
	fmt.Println("motion: ", c.Request.Form)
	fmt.Println("Motion:", c.Request.PostForm)
	fmt.Println("motion: ", c.Request.Form["motion"])
	fmt.Println("我要解析了")
	motion_limen1 := c.Request.FormValue("motion")
	fmt.Println("解析了完了  ", motion_limen1, "  ", "   !!!!")

	 oo := c.Request.PostFormValue("motion")

	 fmt.Println("我又解析了：",oo)
}

func UpdateParameter(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)
	/*
		user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
		if !user.CheckAppRead() {
			logger.Error("You did not have permissions to read app Version .", user)
			c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to read app Version ."))
			return
		}
	*/
	//behAlgo := updateParameterValidate(c)
	//if behAlgo == nil {
	//	return
	//	
	c.Request.ParseMultipartForm(32 << 20)
	motion_limen1 := c.Request.FormValue("limen1")
	motion_limen2 := c.Request.FormValue("limen2")
	fmt.Println("解析了完了  ", motion_limen1, "  ", motion_limen2, "   !!!!")
	f, err := c.FormFile("file")

	ff, _ := f.Open()
	data, err := ioutil.ReadAll(ff)
	if err != nil {
		logger.Error(err)
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to read the request.", err))
		return
	}

	name := "/tmp/" + bson.NewObjectId().Hex() + ".zip"
	F, err := os.Create(name)
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("file Create fail "))
		return
	}
	defer F.Close()
	F.Write(data)
	defer os.Remove(name)

	r, err := zip.OpenReader(name)
	if err != nil {
		fmt.Println(err)
		return
	}
	filesNum := 0
	Trees := [][]*models.BehaviorAlgoTreeCell{}
	for _, k := range r.Reader.File {
		if k.FileInfo().IsDir() {
			continue
		}
		fmt.Println("unzip: ", k.Name)
		ATree := []*models.BehaviorAlgoTreeCell{}

		names := strings.Split(k.Name, ".")
		if len(names) > 2 {
			fmt.Println("jump ", k.Name)
			continue
		}
		//判断文件
		r, err := k.Open()
		if err != nil {
			fmt.Println("Open:-- ", err)
			continue
		}
		defer r.Close()
		r2 := csv.NewReader(r)
		sheet, err := r2.ReadAll()
		if err != nil {
			fmt.Println(err)
			continue
		}
		if len(sheet) < 2 {
			fmt.Println(err)
			c.AbortWithError(http.StatusBadRequest, models.Error("file Create fail "))
			return
		}
		cell := sheet[0]
		if checkTreeHead(cell) == false {
			fmt.Println("标签头出错了-----")
			c.AbortWithError(http.StatusBadRequest, models.Error("file Create fail "))
			return
		}

		for j := 1; j < len(sheet); j++ {
			thisCell := sheet[j]
			thisTree := models.BehaviorAlgoTreeCell{}
			for i, _ := range cell {
				key := cell[i]
				switch key {
				case "left daughter":
					thisTree.LeftDaughter, _ = strconv.Atoi(thisCell[i])
				case "right daughter":
					thisTree.RightDaughter, _ = strconv.Atoi(thisCell[i])
				case "split var":
					thisTree.SplitVar, _ = strconv.Atoi(thisCell[i])
				case "split point":
					floatNum, _ := strconv.ParseFloat(thisCell[i], 32)
					thisTree.SplitPoint = float32(floatNum)
				case "status":
					thisTree.Status, _ = strconv.Atoi(thisCell[i])
				case "prediction":
					thisTree.Prediction, _ = strconv.Atoi(thisCell[i])
				}
			}
			ATree = append(ATree, &thisTree)
		}
		Trees = append(Trees, ATree)
		filesNum++
	}

	behaviorAlgorithm := models.BehaviorAlgorithm{}
	behaviorAlgorithm.Trees = Trees
	behaviorAlgorithm.Version = 1
	MotionLimen1, err := strconv.ParseFloat(motion_limen1, 32)
	if err != nil {
		fmt.Println(" motione_limen1 err :", err)
		c.AbortWithError(http.StatusBadRequest, models.Error("motione_limen1 err :"))
	}
	
	MotionLimen2,err := strconv.ParseFloat(motion_limen2,32)
		if err != nil {
			fmt.Println(" motione_limen2 err :", err)
			c.AbortWithError(http.StatusBadRequest, models.Error("motione_limen2 err :"))
	}


	behaviorAlgorithm.MotionLimen1 = float32(MotionLimen1)
		behaviorAlgorithm.MotionLimen2 = float32(MotionLimen2)

	// 版本为上一个版本加1
	lastBehAlg := models.GetBehaviorAlgorithm(db)
	if lastBehAlg != nil {
		behaviorAlgorithm.Version = lastBehAlg.Version + 1
	}
	err = behaviorAlgorithm.Create(db)
	if err != nil {
		fmt.Println("Create  err :", err)
		c.AbortWithError(http.StatusBadRequest, models.Error("file Create fail "))
	}
	c.Status(http.StatusOK)
}

func checkTreeHead(cell []string) bool {
	flog := 0
	for i := 0; i < 6; i++ {
		flog += shiftNum(i)
	}
	num := 0
	for i, _ := range cell {
		key := cell[i]
		switch key {
		case "left daughter":
			num += shiftNum(0)
		case "right daughter":
			num += shiftNum(1)
		case "split var":
			num += shiftNum(2)
		case "split point":
			num += shiftNum(3)
		case "status":
			num += shiftNum(4)
		case "prediction":
			num += shiftNum(5)
		}
	}
	if flog != num {
		return false
	}
	return true
}

func shiftNum(k int) int {
	kk := 1
	kk <<= uint32(k)
	return kk
}
