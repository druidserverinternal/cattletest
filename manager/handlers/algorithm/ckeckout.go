package algorithm

import (
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
	"encoding/json"
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
)

type ResultAlgo struct {
	ResultTrue int `json:"result_true"`
	ResultNow  int `json:"result_now"`
}

func CheckAlgo1Validate(c *gin.Context) []*models.BehaviorParaneter {
	beParna := []*models.BehaviorParaneter{}
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&beParna); err != nil {
		c.AbortWithError(http.StatusForbidden, errors.New("Unable to parse and decode the request."))
		return nil
	}
	return beParna
}

func CheckAlgo1(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	beParnas := CheckAlgo1Validate(c)
	if beParnas == nil {
		return
	}

	BeAlgo := models.GetBehaviorAlgorithm(db)
	if BeAlgo == nil {
		logger.Error("没有基础算法参数请上传 :")
		c.AbortWithError(http.StatusBadRequest, models.Error("没有基础算法参数请上传:"))
	}

	result := []*ResultAlgo{}
	for _, beParna := range beParnas {
		reseltThis := ResultAlgo{}
		Trees := BeAlgo.Trees

		results := make([]int, 10, 10)
		parameter := make([]float32, 7)
		parameter[0] = float32(beParna.ODBA)
		parameter[1] = float32(beParna.ODBAX)
		parameter[2] = float32(beParna.ODBAY)
		parameter[3] = float32(beParna.ODBAZ)
		parameter[4] = float32(beParna.MeandlX)
		parameter[5] = float32(beParna.MeandlY)
		parameter[6] = float32(beParna.MeandlZ)
		parameter[0] = parameter[1] + parameter[2] + parameter[3]
		for _, Tree := range Trees {
			num := models.RecursiveTheBehaviorTrees(Tree, 0, parameter)
			if num < 0 || num >= 9 {
				continue
			}
			results[num] = results[num] + 1
		}
		motion := getMaxResults(results)
		if motion != models.MotionOtherLow {
			reseltThis.ResultNow = motion
		} else {
			if parameter[0] <= float32(models.MotionODBALimen1) {
				reseltThis.ResultNow = models.MotionOtherLow
			}

			if float32(models.MotionODBALimen1) < parameter[0] && parameter[0] <= float32(models.MotionODBALimen2) {
				reseltThis.ResultNow = models.MotionOtherMedium
			}

			if float32(models.MotionODBALimen2) < parameter[0] {
				reseltThis.ResultNow = models.MotionOtherHight
			}
		}
		result = append(result, &reseltThis)
	}
	c.JSON(http.StatusOK, result)
}
func getMaxResults(results []int) int {
	maxNum := 0
	index := 0
	for i, k := range results {
		if k > maxNum {
			maxNum = k
			index = i
		}
	}
	return index
}
