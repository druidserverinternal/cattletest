package behavior2

import (
	"net/http"
	"strconv"
	

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/manager/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

func getValidate(c *gin.Context) (string) {
	deviceID := c.Param("id")
	if !bson.IsObjectIdHex(deviceID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("Device id is invalid:" + deviceID))
		return ""
	}
	return deviceID
}

// Get one of device record with device
func Get(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckDataRead() {
		logger.Error("You did not have permissions to read data.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to read data."))
		return
	}


	deviceID := getValidate(c)
	if deviceID == "" {
		return
	}

	device, err := models.FindDeviceByID(db, bson.ObjectIdHex(deviceID), nil)
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Device is is invalid:" + deviceID))
		return
	}

	rp := models.ParseRequestParameter(c)
	if rp.Limit == models.MaxLimit {
		rp.Limit = 0
	}

	behavior2, err := models.ListBehavior2ByDeviceID(db, device.ID, rp)
	if err != nil {
		logger.Error("List behavior2 by device failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List behavior2 by device failed.", err))
		return
	}


	count := 0
	if len(behavior2) > 0 {
		count, _ = models.CountBehavior2ByDeviceID(db, bson.ObjectIdHex(deviceID))
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, behavior2)
}
