package biological

import (
	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"

	"fmt"
	"net/http"

	
	"gopkg.in/mgo.v2/bson"

	
	//	"time"
)

// Get a biological record
func Get(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)
	deviceID := c.Param("id")
	if !bson.IsObjectIdHex(deviceID) {
		w.WriteError(
			common.NewStatusError(
				http.StatusBadRequest,
				fmt.Sprintf("Device id:%s is invalid", deviceID)))
	}

	// get valid device
	device, err := models.FindDeviceByID(db, bson.ObjectIdHex(deviceID))
	if err != nil {
		logger.Error("Find device from biological failed:", err)
		w.WriteError(
			common.NewStatusError(
				http.StatusBadRequest,
				"The device id is invalid!"))
		return
	}

	biological, err := models.FindBiologicalByRef(db, device.BiologicalRef)
	if err != nil {
		w.WriteError(
			common.NewStatusError(
				http.StatusInternalServerError,
				"Find biological failed."))
		return
	}

	if biological == nil {
		w.WriteError(
			common.NewStatusError(
				http.StatusNotFound,
				"This device not have biological."))
		return
	}

	w.WriteGzip(http.StatusOK, biological)
}
