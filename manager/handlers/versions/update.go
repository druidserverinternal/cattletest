package versions

import (
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

func updateInfoValidate(c *gin.Context) *models.AppInfo {

	var updateInfo models.AppInfo
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&updateInfo); err != nil {
		c.AbortWithError(http.StatusForbidden, errors.New("Unable to parse and decode the request."))
		return nil
	}

	return &updateInfo
}

func UpdateInfo(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	appInfo := updateInfoValidate(c)
	if appInfo == nil {
		return
	}

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if !user.CheckAppWrite() {
		logger.Error("You did not have permissions to write app Version .", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to write  app Version ."))
		return
	}

	id := c.Param("id")
	if !bson.IsObjectIdHex(id) {
		logger.Error("appVersion id:", id, " is invalid")
		c.AbortWithError(http.StatusBadRequest, models.Error(fmt.Sprintf("appVersion id:%s is invalid", id)))
		return
	}

	appVersion := models.FindAppVersionById(db, bson.ObjectIdHex(id))
	if appVersion == nil {
		logger.Error("appVersion is nil id:", id)
		c.AbortWithError(http.StatusBadRequest, models.Error("appVersion is nil id:", id))
		return
	}

	appVersion.Info = appInfo.Info
	appVersion.ChangeLog = appInfo.ChangeLog

	err := appVersion.Update(db)
	if err != nil {
		logger.Error("appVersion Update is err :", err)
		c.AbortWithError(http.StatusBadRequest, models.Error("appVersion Update is err :", err))
		return
	}
	c.JSON(http.StatusOK, appVersion)
}
