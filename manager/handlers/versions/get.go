package versions

import (
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

func Get(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if !user.CheckAppRead() {
		logger.Error("You did not have permissions to read app Version .", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to read  app Version ."))
		return
	}

	id := c.Param("id")
	if !bson.IsObjectIdHex(id) {
		logger.Error("appVersion id:", id, " is invalid")
		c.AbortWithError(http.StatusBadRequest, models.Error(fmt.Sprintf("appVersion id:%s is invalid", id)))
		return
	}

	appVersion := models.FindAppVersionById(db, bson.ObjectIdHex(id))
	if appVersion == nil {
		logger.Error("appVersion is nil id:", id)
		c.AbortWithError(http.StatusBadRequest, models.Error("appVersion is nil id:", id))
		return
	}

	c.JSON(http.StatusOK, appVersion)
}

func GetBaseVersion(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if !user.CheckAppRead() {
		logger.Error("You did not have permissions to read app Version .", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to read  app Version ."))
		return
	}

	appVersion := models.FindAppVersionByStatus(db, models.APK_Status_Issue_Base_ed)
	c.JSON(http.StatusOK, appVersion)
}

func GetUpVersion(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if !user.CheckAppRead() {
		logger.Error("You did not have permissions to read app Version .", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to read  app Version ."))
		return
	}

	appVersion := models.FindAppVersionByStatus(db, models.APK_Status_Issue_Up_ed)
	c.JSON(http.StatusOK, appVersion)
}
