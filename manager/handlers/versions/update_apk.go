package versions

import (
	"bytes"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"

	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	"github.com/gin-gonic/gin"
	"github.com/lunny/axmlParser"
	"github.com/spf13/viper"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func UpdateApk(c *gin.Context) {

	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if !user.CheckAppWrite() {
		logger.Error("You did not have permissions to write app Version .", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to write  app Version ."))
		return
	}

	f, err := c.FormFile("file")
	if err != nil {
		logger.Error(err)
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to read the request.", err))
		return
	}
	ff, err := f.Open()
	if err != nil {
		logger.Error(err)
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to read the request.", err))
		return
	}
	data, err := ioutil.ReadAll(ff)
	if err != nil {
		logger.Error(err)
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to read the request.", err))
		return
	}
	F, err := os.Create("/home/druid/cattle/bin/apk/text.apk")
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("file Create fail "))
		return
	}
	defer F.Close()
	defer os.Remove("/home/druid/cattle/bin/apk/text.apk")
	F.Write(data)

	listener := new(axmlParser.AppNameListener)
	_, err = axmlParser.ParseApk("/home/druid/cattle/bin/apk/text.apk", listener)
	if err != nil {
		logger.Error(err)
		c.AbortWithError(http.StatusBadRequest, models.Error("read  apk  fail ", err))
		return
	}
	appVersion := models.AppVersion{}
	name := viper.GetString("app.name")
	downLoad := viper.GetString("oss.download")
	name = name + "_" + listener.VersionName + "_" + listener.VersionCode + ".apk"

	code, err := strconv.Atoi(listener.VersionCode)
	if err != nil {
		logger.Error(err)
		c.AbortWithError(http.StatusBadRequest, models.Error("APK code is err ", err))
		return
	}
	appVersion.Name = name
	appVersion.Code = code
	appVersion.Version = listener.VersionName
	appVersion.Status = models.APK_Status_Up_ing
	appVersion.Path = downLoad + name
	appVersion.Admin = user.UserName

	err = appVersion.Create(db)
	if err != nil {
		logger.Error(err)
		c.AbortWithError(http.StatusBadRequest, models.Error("Creat  AppVersion  fail "))
		return
	}

	go ossPut(name, data, appVersion.ID, db.Copy())
	c.JSON(http.StatusCreated, appVersion)
}

func ossPut(name string, date []byte, id bson.ObjectId, db *mgo.Session) {
	BucketName := viper.GetString("oss.BucketName")
	AccessKeyId := viper.GetString("oss.AccessKeyId")
	AccessKeySecret := viper.GetString("oss.AccessKeySecret")
	Endpoint := viper.GetString("oss.address")
	Path := viper.GetString("oss.path")

	// New Client
	client, err := oss.New(Endpoint, AccessKeyId, AccessKeySecret)
	if err != nil {
		fmt.Println("err :", err)
		return
	}

	// Get Bucket
	bucket, err := client.Bucket(BucketName)
	if err != nil {
		fmt.Println("err :", err)
		return
	}

	fd := bytes.NewReader(date)

	appVersion := models.FindAppVersionById(db, id)
	if appVersion == nil {
		fmt.Println("appVserios is nil ")
		return
	}

	err = bucket.PutObject(Path+name, fd)
	if err != nil {
		appVersion.Status = models.APK_Status_Up_Err
		err = appVersion.Update(db)
		if err != nil {
			fmt.Println("err :", err)
			return
		}
		fmt.Println("err :", err)
		return
	}

	appVersion.Status = models.APK_Status_Up_ed
	err = appVersion.Update(db)
	if err != nil {
		fmt.Println("err :", err)
	}

}
