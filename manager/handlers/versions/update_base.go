package versions

import (
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func updateBaseValidate(c *gin.Context) *models.AppVersion {
	var updateInfo models.AppVersion
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&updateInfo); err != nil {
		c.AbortWithError(http.StatusForbidden, errors.New("Unable to parse and decode the request."))
		return nil
	}
	/*
		if updateInfo.ForceCode == "" {
			c.AbortWithError(http.StatusBadRequest, errors.New("ForceCode is nil."))
		}
	*/
	return &updateInfo
}

func UpdataBase(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if !user.CheckAppWrite() {
		logger.Error("You did not have permissions to write app Version .", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to write  app Version ."))
		return
	}

	id := c.Param("id")
	if !bson.IsObjectIdHex(id) {
		logger.Error("appVersion id:", id, " is invalid")
		c.AbortWithError(http.StatusBadRequest, models.Error(fmt.Sprintf("appVersion id:%s is invalid", id)))
		return
	}

	/*
		appVersion := updateBaseValidate(c)
		if appVersion == nil {
			return
		}
	*/

	thisAppVersion := models.FindAppVersionById(db, bson.ObjectIdHex(id))
	if thisAppVersion == nil {
		logger.Error("appVersion is nil id:", id)
		c.AbortWithError(http.StatusBadRequest, models.Error("appVersion is nil id:", id))
		return
	}
	if thisAppVersion.Status != models.APK_Status_Up_ed &&
		thisAppVersion.Status != models.APK_Status_Issue_Up_Err &&
		thisAppVersion.Status != models.APK_Status_Issue_Base_Err {
		logger.Error("this App Verison Up err Status:", thisAppVersion.Status)
		c.AbortWithError(http.StatusBadRequest, models.Error("this App Verison Up err Status:", thisAppVersion.Status))
		return
	}

	/*
		oldAppVersion := models.FindAppVersionById(db, appVersion.ID)
		if oldAppVersion == nil {
			logger.Error("appVersion is nil id:", appVersion.ID)
			c.AbortWithError(http.StatusBadRequest, models.Error("appVersion is nil id:", appVersion.ID))
			return
		}
	*/

	//clear old ForceCode
	/*
		err := models.AppClearOldForceCode(db, models.APK_Status_Issue_Base_ed)
		if err != nil {
			logger.Error("AppClearOldForceCode is err :", err)
		}

	*/
	//thisAppVersion.ForceCode = oldAppVersion.Code
	thisAppVersion.Status = models.APK_Status_Issue_Base_ing
	err := thisAppVersion.Update(db)
	if err != nil {
		logger.Error("appVersion Update is err :", err)
		c.AbortWithError(http.StatusBadRequest, models.Error("appVersion Update is err :", err))
		return
	}

	go ossUpdateBase(thisAppVersion.Name, thisAppVersion.ID, db.Copy())

	c.Status(http.StatusOK)
}

func ossUpdateBase(name string, id bson.ObjectId, db *mgo.Session) {

	BucketName := viper.GetString("oss.BucketName")
	AccessKeyId := viper.GetString("oss.AccessKeyId")
	AccessKeySecret := viper.GetString("oss.AccessKeySecret")
	Endpoint := viper.GetString("oss.address")
	Path := viper.GetString("oss.path")
	appName := viper.GetString("app.name")
	way := viper.GetString("release")

	client, err := oss.New(Endpoint, AccessKeyId, AccessKeySecret)
	if err != nil {
		fmt.Println("err :", err)
		return
	}

	// Get Bucket
	bucket, err := client.Bucket(BucketName)
	if err != nil {
		fmt.Println("err :", err)
		return
	}

	appVersion := models.FindAppVersionById(db, id)
	if appVersion == nil {
		fmt.Println("appVserios is nil ")
		return
	}

	descObjectKey := appName + "." + way + ".apk"
	_, err = bucket.CopyObject(Path+name, Path+descObjectKey)
	if err != nil {
		appVersion.Status = models.APK_Status_Issue_Base_Err
		err = appVersion.Update(db)
		if err != nil {
			fmt.Println("err :", err)
			return
		}
		fmt.Println("err :", err)
		return
	}

	appVersion.Status = models.APK_Status_Issue_Base_ed
	err = appVersion.Update(db)
	if err != nil {
		fmt.Println("err :", err)
	}

}
