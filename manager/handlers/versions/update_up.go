package versions

import (
	"bytes"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func updateUpValidate(c *gin.Context) *models.AppVersion {
	var updateInfo models.AppVersion
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&updateInfo); err != nil {
		//		c.AbortWithError(http.StatusForbidden, errors.New("Unable to parse and decode the request."))
		return nil
	}

	return &updateInfo
}

func UpdateUp(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if !user.CheckAppWrite() {
		logger.Error("You did not have permissions to write app Version .", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to write  app Version ."))
		return
	}

	id := c.Param("id")
	if !bson.IsObjectIdHex(id) {
		logger.Error("appVersion id:", id, " is invalid")
		c.AbortWithError(http.StatusBadRequest, models.Error(fmt.Sprintf("appVersion id:%s is invalid", id)))
		return
	}

	appVersion := updateUpValidate(c)
	if appVersion == nil {
		//		return
	}

	thisAppVersion := models.FindAppVersionById(db, bson.ObjectIdHex(id))
	if thisAppVersion == nil {
		logger.Error("appVersion is nil id:", id)
		c.AbortWithError(http.StatusBadRequest, models.Error("appVersion is nil id:", id))
		return
	}
	if thisAppVersion.Status != models.APK_Status_Up_ed &&
		thisAppVersion.Status != models.APK_Status_Issue_Up_Err &&
		thisAppVersion.Status != models.APK_Status_Issue_Base_Err {
		logger.Error("this App Verison Up err Status:", thisAppVersion.Status)
		c.AbortWithError(http.StatusBadRequest, models.Error("this App Verison Up err Status:", thisAppVersion.Status))
		return
	}

	err := models.AppClearOldForceCode(db, models.APK_Status_Issue_Up_ed)
	if err != nil {
		logger.Error("AppClearOldForceCode is err ", err)
	}

	if appVersion == nil || appVersion.ID == "" {

		thisAppVersion.ForceCode = 0
	} else {

		oldAppVersion := models.FindAppVersionById(db, appVersion.ID)
		if oldAppVersion == nil {
			logger.Error("appVersion is nil id:", appVersion.ID)
			c.AbortWithError(http.StatusBadRequest, models.Error("appVersion is nil id:", appVersion.ID))
			return
		}

		thisAppVersion.ForceCode = oldAppVersion.Code
	}
	thisAppVersion.Status = models.APK_Status_Issue_Up_ing

	appConfig := models.AppConfig{}
	appConfig.Code = thisAppVersion.Code
	appConfig.Verison = thisAppVersion.Version
	appConfig.Path = thisAppVersion.Path
	appConfig.Tips = thisAppVersion.ChangeLog
	appConfig.ForceCode = thisAppVersion.ForceCode

	data, err := json.Marshal(appConfig)
	if err != nil {
		logger.Error("appConfig Marshal Json is err :", err)
		c.AbortWithError(http.StatusBadRequest, models.Error("appConfig Marshal Json is err :", err))
		return
	}

	err = thisAppVersion.Update(db)
	if err != nil {
		logger.Error("appVersion Update is err :", err)
		c.AbortWithError(http.StatusBadRequest, models.Error("appVersion Update is err :", err))
		return
	}

	versionHistoryInfo, _ := models.GetVersionHistoryInfo(db, "V1")
	if versionHistoryInfo == nil {
		ver := models.VersionHistoryInfo{}
		versionHistoryInfo.ID = bson.NewObjectId()
		versionHistoryInfo = &ver
		versionHistoryInfo.Verison = "V1"
		versionHistoryInfo.Creat(db)
	}

	versionCell := models.VersionCell{}
	versionCell.Version = appConfig.Verison
	versionCell.Updates = appConfig.Tips
	versionCell.Time = thisAppVersion.UpdatedAt
	versionHistoryInfo.Tips = append(versionHistoryInfo.Tips, &versionCell)

	err = versionHistoryInfo.Update(db)
	if err != nil {
		logger.Error("update version history info err:", err)
		c.AbortWithError(http.StatusBadRequest, models.Error("update version history info err:", err))
		return
	}

	datafileJson, err := json.Marshal(versionHistoryInfo)
	if err != nil {
		logger.Error("versionHistoryInfo Marshal Json is err :", err)
		c.AbortWithError(http.StatusBadRequest, models.Error("versionHistoryInfo Marshal Json is err :", err))
		return
	}

	go ossUpdateUpWithName(datafileJson, "log.json")

	go ossUpdateUp(data, thisAppVersion.ID, db.Copy())

	c.Status(http.StatusOK)
}
func ossUpdateUpWithName(data []byte, name string) {

	BucketName := viper.GetString("oss.BucketName")
	AccessKeyId := viper.GetString("oss.AccessKeyId")
	AccessKeySecret := viper.GetString("oss.AccessKeySecret")
	Endpoint := viper.GetString("oss.address")
	Path := viper.GetString("oss.path")
	//	appName := viper.GetString("app.name")

	client, err := oss.New(Endpoint, AccessKeyId, AccessKeySecret)
	if err != nil {
		fmt.Println("err :", err)
		return
	}

	// Get Bucket
	bucket, err := client.Bucket(BucketName)
	if err != nil {
		fmt.Println("err :", err)
		return
	}

	fd := bytes.NewReader(data)

	err = bucket.PutObject(Path+name, fd)
	if err != nil {
		fmt.Println("err :", err)
		return
	}

}

func ossUpdateUp(data []byte, id bson.ObjectId, db *mgo.Session) {

	BucketName := viper.GetString("oss.BucketName")
	AccessKeyId := viper.GetString("oss.AccessKeyId")
	AccessKeySecret := viper.GetString("oss.AccessKeySecret")
	Endpoint := viper.GetString("oss.address")
	Path := viper.GetString("oss.path")
	//	appName := viper.GetString("app.name")

	client, err := oss.New(Endpoint, AccessKeyId, AccessKeySecret)
	if err != nil {
		fmt.Println("err :", err)
		return
	}

	// Get Bucket
	bucket, err := client.Bucket(BucketName)
	if err != nil {
		fmt.Println("err :", err)
		return
	}

	fd := bytes.NewReader(data)

	appVersion := models.FindAppVersionById(db, id)
	if appVersion == nil {
		fmt.Println("appVserios is nil ")
		return
	}

	err = bucket.PutObject(Path+"version.json", fd)
	if err != nil {
		appVersion.Status = models.APK_Status_Issue_Up_Err
		err = appVersion.Update(db)
		if err != nil {
			fmt.Println("err :", err)
			return
		}
		fmt.Println("err :", err)
		return
	}

	appVersion.Status = models.APK_Status_Issue_Up_ed
	err = appVersion.Update(db)
	if err != nil {
		fmt.Println("err :", err)
	}

}
