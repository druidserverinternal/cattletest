package versions

import (
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
	"fmt"
	"net/http"
	"strconv"

	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"gopkg.in/mgo.v2/bson"
)

func List(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if !user.CheckAppRead() {
		logger.Error("You did not have permissions to read app Version .", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to read app Version ."))
		return
	}

	rp := models.ParseRequestParameter(c)

	result, count, err := models.AppVersionList(db, rp)
	if err != nil {
		logger.Error("List versions by filter failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List versions by filter failed.", err))
		return
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, result)
}

func OssList(c *gin.Context) {

	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if !user.CheckAppRead() {
		logger.Error("You did not have permissions to read app Version .", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to read app Version ."))
		return
	}

	BucketName := viper.GetString("oss.BucketName")
	AccessKeyId := viper.GetString("oss.AccessKeyId")
	AccessKeySecret := viper.GetString("oss.AccessKeySecret")
	Endpoint := viper.GetString("oss.address")
	Path := viper.GetString("oss.path")
	name := viper.GetString("app.name")

	fmt.Println("AccessKeyId:",AccessKeyId)
	fmt.Println("AccessKeySecret:",AccessKeySecret)
	fmt.Println("Endpoint:",Endpoint)
	fmt.Println("BucketName:",BucketName)
	fmt.Println("PAth:",Path)
	fmt.Println(name)
	

	client, err := oss.New(Endpoint, AccessKeyId, AccessKeySecret)
	if err != nil {
		fmt.Println("err :", err)
		return
	}

	// Get Bucket
	bucket, err := client.Bucket(BucketName)
	if err != nil {
		fmt.Println("client err :", err)
		return
	}
	lsRes, err := bucket.ListObjects(oss.Prefix(Path),oss.Delimiter("/"))
//	lsRes, err := bucket.ListObjects()
	if err != nil {
		fmt.Println("list err :", err)
		return
	}
	fmt.Println("fixer:",lsRes.CommonPrefixes)
	c.JSON(http.StatusOK, lsRes.Objects)
}
