package versions

import (
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

func GetLog(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if !user.CheckAppRead() {
		logger.Error("You did not have permissions to read app Version .", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to read app Version ."))
		return
	}

	versionHistoryInfo,err := models.GetVersionHistoryInfo(db, "V1")
	if err != nil {
		fmt.Println("Get Version versionHistoryInfo err :",err )
	}
	fmt.Println("versionHistoryInfo ", versionHistoryInfo)
	c.JSON(http.StatusOK, versionHistoryInfo)
}
