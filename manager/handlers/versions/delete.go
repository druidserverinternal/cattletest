package versions

import (
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
	"fmt"
	"net/http"

	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"gopkg.in/mgo.v2/bson"
)

func Delete(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if !user.CheckAppWrite() {
		logger.Error("You did not have permissions to write app Version .", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to write  app Version ."))
		return
	}

	id := c.Param("id")
	if !bson.IsObjectIdHex(id) {
		logger.Error("appVersion id:", id, " is invalid")
		c.AbortWithError(http.StatusBadRequest, models.Error(fmt.Sprintf("appVersion id:%s is invalid", id)))
		return
	}

	appVersion := models.FindAppVersionById(db, bson.ObjectIdHex(id))
	if appVersion == nil {
		logger.Error("appVersion is nil id:", id)
		c.AbortWithError(http.StatusBadRequest, models.Error("appVersion is nil id:", id))
		return
	}

	err := ossDelete(appVersion.Name)
	if err != nil {
		logger.Error("Oss Delete err :", appVersion.Name)
		c.AbortWithError(http.StatusBadRequest, models.Error("Oss Delete err :", appVersion.Name))
		return
	}
	err = appVersion.Delete(db)
	if err != nil {
		logger.Error("appVersion delete err :", err)
		c.AbortWithError(http.StatusBadRequest, models.Error("appVersion delete err :", err))
		return
	}
	c.Status(http.StatusOK)
}

func ossDelete(name string) error {

	BucketName := viper.GetString("oss.BucketName")
	AccessKeyId := viper.GetString("oss.AccessKeyId")
	AccessKeySecret := viper.GetString("oss.AccessKeySecret")
	Endpoint := viper.GetString("oss.address")
	Path := viper.GetString("oss.path")

	// New Client
	client, err := oss.New(Endpoint, AccessKeyId, AccessKeySecret)
	if err != nil {
		fmt.Println("err :", err)
		return err
	}

	// Get Bucket
	bucket, err := client.Bucket(BucketName)
	if err != nil {
		fmt.Println("err :", err)
		return err
	}
	err = bucket.DeleteObject(Path + name)
	if err != nil {
		fmt.Println("err :", err)
		return err
	}
	return nil
}
