package statistics

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"

	"gopkg.in/mgo.v2/bson"
)

// ListGPSCount List gps statistics count
func ListGPSCount(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckDataRead() {
		logger.Error("You did not have permissions to read data.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to read data."))
		return
	}

	gs, err := models.ListGPSStatistcsBySearch(db, nil, models.ParseRequestParameter(c))
	if err != nil {
		logger.Warning("Get gps statistics failed, ", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Get gps statistics failed.", err))
		return
	}

	count := 0
	if len(gs) > 0 {
		count, _ = models.GPSStatistcsSearchCount(db, nil)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, gs)
}
