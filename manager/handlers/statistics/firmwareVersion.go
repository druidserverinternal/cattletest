package statistics

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

// List FirmwareVersion count
func FirmwareVersion(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckDataRead() {
		logger.Error("You did not have permissions to read data.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to read data."))
		return
	}

	fv, err := models.CountFirmwareVersion(db)
	if err != nil {
		logger.Error("Get firmware version failed, ", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List firmware version failed." , err))
		return
	}

	c.JSON(http.StatusOK, fv)
}
