package user

import (
	"github.com/gin-gonic/gin"

	"druid/cattle/manager/middleware"
	"druid/cattle/models"

	"encoding/json"
	"fmt"
	"net/http"
	
	"strings"

	"github.com/spf13/viper"
	"gopkg.in/mgo.v2/bson"

)

func updateUserPwdValidate(c *gin.Context) (*models.User) {
	var user models.User
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&user); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	userID := c.Param("id")
	if !bson.IsObjectIdHex(userID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("User id is invalid:" + userID))
		return nil
	}
	user.ID = bson.ObjectIdHex(userID)

	user.OldPassword = strings.TrimSpace(user.OldPassword)
	if !models.RegexpPwd.MatchString(user.OldPassword) {
		c.AbortWithError(http.StatusBadRequest, models.Error("oldpassword must be a valid oldpassword."))
		return nil
	}

	user.Password = strings.TrimSpace(user.Password)
	if user.Password != "" && !models.RegexpPwd.MatchString(user.Password) {
		c.AbortWithError(http.StatusBadRequest, models.Error("password must be a valid password."))
		return nil
	}

	return &user
}

// Update a any user
func UpdateUserPwd(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if me.IsManager() && !me.CheckUserWrite() {
		logger.Error("You did not have permissions to edit user.", me)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to edit user."))
		return
	}

	reqUser := updateUserPwdValidate(c)
	if reqUser == nil {
		return
	}

	if err := models.PasswordVerify(reqUser.OldPassword, me.HashedPassword); err != nil {
		logger.Error("Password verify failled.", err)
		c.AbortWithError(http.StatusUnauthorized, models.Error("The manager password is invalid!"))
		return
	}

	// check original user is valid
	originalUser := models.FindUserByID(db, reqUser.ID)
	if originalUser == nil {
		logger.Error("Find origin user failed.")
		c.AbortWithError(http.StatusBadRequest, models.Error(fmt.Sprintf("User:%s cannot be found", reqUser.ID)))
		return
	}

	if me.IsManager() && originalUser.Role != models.RoleAdmin {
		logger.Error("Manager did not have permissions to edit other user")
		c.AbortWithError(http.StatusUnauthorized, models.Error("Manager did not have permissions to edit other user."))
		return
	}

	if !reqUser.HashPassword() {
		logger.Error("The password is invalid.")
		c.AbortWithError(http.StatusBadRequest, models.Error("The password is invalid."))
		return
	}
	token := models.NewToken(originalUser.ID, viper.GetString("authentication.secret"))
	reqUser.Token = token.String()
	reqUser.Expiry = &token.Expiry

	if err := reqUser.UpdatePassword(db); err != nil {
		logger.Error("Update user password failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Update user password failed." , err))
		return
	}
	logger.Infof("User:%s update user:%s password", middleware.GetUserID(c), reqUser.UserName)

	c.Status(http.StatusNoContent)
}
