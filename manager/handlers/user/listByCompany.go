package user

import (
	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
	

	"net/http"
	"strconv"

	"gopkg.in/mgo.v2/bson"
)

// ListByCompany company's users
func ListByCompany(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckUserRead() {
		logger.Error("You did not have permissions to view user.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to view user."))
		return
	}


	companyID := c.Param("id")
	if !bson.IsObjectIdHex(companyID) {
		logger.Error("Company id is invalid:", companyID)
		c.AbortWithError(http.StatusBadRequest, models.Error("Company id is invalid:", companyID))
		return
	}

	users, err := models.ListAllUsersByCompanyID(db, bson.ObjectIdHex(companyID), models.ParseRequestParameter(c))
	if err != nil {
		logger.Error("List user by companyfailed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List user from company failed." , err))
		return
	}

	count := 0
	if len(users) > 0 {
		count, _ = models.CountAllUsersByCompanyID(db, bson.ObjectIdHex(companyID))
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, users)
}
