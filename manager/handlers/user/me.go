package user

import (
	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
	//	"fmt"

	"net/http"
	"gopkg.in/mgo.v2/bson"
)

// Me mine user info
func Me(c *gin.Context) {
	db := middleware.GetDB(c)
	//	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	c.JSON(http.StatusOK, user)
}
