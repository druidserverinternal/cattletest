package user

import (
	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
	"fmt"
	

	"net/http"

	"gopkg.in/mgo.v2/bson"
)

func deleteValidate(c *gin.Context) (string) {
	userID := c.Param("id")
	if !bson.IsObjectIdHex(userID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("User id is invalid:" + userID))
		return ""
	}

	return userID
}

// Delete a any user
func Delete(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if me.IsManager() && !me.CheckUserWrite() {
		logger.Error("You did not have permissions to edit user.", me)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to edit user."))
		return
	}

	userID := deleteValidate(c)
	if userID == "" {
		return
	}

	if userID == me.ID.Hex() {
		logger.Error("You an't delete yourself.")
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to delete your."))
		return
	}


	user := models.FindUserByID(db, bson.ObjectIdHex(userID))
	if user == nil {
		logger.Error("Find user failed.")
		c.AbortWithError(http.StatusNotFound, models.Error(fmt.Sprintf("User:%s cannot be found", user.ID)))
		return
	}

	if user.TotalDevice > 0 {
		if err := user.FixTotalDevice(db); err != nil {
			logger.Error("Fix total device failed.", err)
			c.AbortWithError(http.StatusInternalServerError, models.Error("Fix total device failed.", err))
			return
		}

		if user.TotalDevice > 0 {
			logger.Error("Could not delete had device user.")
			c.AbortWithError(http.StatusBadRequest, models.Error("Could not delete had device user."))
			return
		}
	}

	user.DeletedBy = bson.ObjectIdHex(middleware.GetUserID(c))

	if err := user.Delete(db); err != nil {
		logger.Error("Delete user failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Delete user failed." , err))
		return
	}
	logger.Infof("User:%s delete user:%s", middleware.GetUserID(c), user.UserName)

	c.Status(http.StatusNoContent)
}
