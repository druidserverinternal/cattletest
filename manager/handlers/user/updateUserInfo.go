package user

import (
	"druid/cattle/manager/middleware"
	"druid/cattle/models"

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"

	"strings"
	"encoding/json"
	"net/http"
	
)

func updateUserInfoValidate(c *gin.Context) (*models.User) {
	var user models.User
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&user); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	userID := c.Param("id")
	if !bson.IsObjectIdHex(userID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("User id is invalid:" + userID))
		return nil
	}
	user.ID = bson.ObjectIdHex(userID)

	user.Email = strings.TrimSpace(user.Email)
	if !models.RegexpEmail.MatchString(user.Email) {
		c.AbortWithError(http.StatusBadRequest, models.Error("email must be a valid value."))
		return nil
	}

	user.Phone = strings.TrimSpace(user.Phone)
	if !models.RegexpPhone.MatchString(user.Phone) {
		c.AbortWithError(http.StatusBadRequest, models.Error("phone must be a valid value."))
		return nil
	}

	return &user
}

// UpdateUserInfo a any user
func UpdateUserInfo(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if me.IsManager() && !me.CheckUserWrite() {
		logger.Error("You did not have permissions to edit user.", me)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to edit user."))
		return
	}


	user := updateUserInfoValidate(c)
	if user == nil {
		return
	}

	// check original user is valid
	originalUser := models.FindUserByID(db, user.ID)
	if originalUser == nil {
		logger.Error("Find origin user failed.")
		c.AbortWithError(http.StatusNotFound, models.Error("user id is invalid:" + user.ID.Hex()))
		return
	}

	if me.IsManager() && originalUser.Role != models.RoleAdmin {
		logger.Error("Manager did not have permissions to edit other user")
		c.AbortWithError(http.StatusUnauthorized, models.Error("Manager did not have permissions to edit other user."))
		return
	}


	switch originalUser.Role {
	case models.RoleAdmin:
		user.Permissions = nil
	case models.RoleManager:
		if !user.CheckManagerPermissions() {
			logger.Error("permissions must be a valid permissions.")
			c.AbortWithError(http.StatusInternalServerError, models.Error("permissions must be a valid permissions."))
			return
		}
	}

	if err := user.UpdateManager(db); err != nil {
		logger.Error("Update user info failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Update user info failed:" , err))
		return
	}
	logger.Infof("User:%s update user:%s info", middleware.GetUserID(c), user.UserName)

	c.Status(http.StatusNoContent)
}
