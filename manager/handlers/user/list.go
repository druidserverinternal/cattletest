package user

import (
	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"

	"net/http"
	"strconv"
	

	"gopkg.in/mgo.v2/bson"
)

// List company's users
func List(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckUserRead() {
		logger.Error("You did not have permissions to view user.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to view user."))
		return
	}

	users, err := models.ListUsers(db, models.ParseRequestParameter(c))
	if err != nil {
		logger.Error("List user failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List user failed." , err))
		return
	}

	count := 0
	if len(users) > 0 {
		count, _ = models.UserCount(db)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, users)
}
