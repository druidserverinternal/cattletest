package user

import (
	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
	"encoding/json"

	"net/http"

	"gopkg.in/mgo.v2/bson"

	"strings"
)

func createValidate(c *gin.Context) (*models.User) {
	var user models.User
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&user); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	user.UserName = strings.TrimSpace(user.UserName)
	if !models.RegexpUser.MatchString(user.UserName) {
		c.AbortWithError(http.StatusBadRequest, models.Error("username must be a valid name."))
		return nil
	}

	user.Password = strings.TrimSpace(user.Password)
	if !models.RegexpPwd.MatchString(user.Password) {
		c.AbortWithError(http.StatusBadRequest, models.Error("password must be a valid value."))
		return nil
	}

	user.Email = strings.TrimSpace(user.Email)
	if !models.RegexpEmail.MatchString(user.Email) {
		c.AbortWithError(http.StatusBadRequest, models.Error("email must be a valid value."))
		return nil
	}

	user.Phone = strings.TrimSpace(user.Phone)
	if !models.RegexpPhone.MatchString(user.Phone) {
		c.AbortWithError(http.StatusBadRequest, models.Error("phone must be a valid value."))
		return nil
	}

	user.Role = strings.TrimSpace(user.Role)
	switch user.Role {
	case "", models.RoleAdmin:
		user.Role = models.RoleAdmin
		user.Permissions = nil
	case models.RoleManager:
		if !user.CheckPermissions() {
			c.AbortWithError(http.StatusBadRequest, models.Error("permissions must be a valid value."))
			return nil
		}
	default:
		c.AbortWithError(http.StatusBadRequest, models.Error("role must be a valid value."))
		return nil
	}
	
	if user.Profile == nil {
		user.Profile = &models.UserProfile{
			PageSize: models.ProfilePageMin,
			TimeZone: models.ProfileTimezoneChina,
			Language: models.ProfileLanguageCH,
		}
	} else {
		if user.Profile.PageSize < models.ProfilePageMin || user.Profile.PageSize > models.ProfilePageMax  {
			user.Profile.PageSize = models.ProfilePageMin
		}

		if user.Profile.TimeZone < models.ProfileTimezoneMin || user.Profile.PageSize > models.ProfileTimezoneMax {
			user.Profile.TimeZone = models.ProfileTimezoneChina
		}

		if user.Profile.Language < models.ProfileLanguageBegin || user.Profile.Language > models.ProfileLanguageEnd {
			user.Profile.Language = models.ProfileLanguageCH
		}
	}

//	user.CreatorID = bson.ObjectIdHex(middleware.GetUserID(c))

	return &user
}

// Create a new user, admin or user
func Create(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if me.IsManager() && !me.CheckUserWrite() {
		logger.Error("You did not have permissions to edit user.", me)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to edit user."))
		return
	}

	user := createValidate(c)
	if user == nil {
		return
	}

	origUser, err := models.FindUserByUserName(db, user.UserName)
	if err != nil {
		logger.Error("find user failed:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find user failed:" , err))
		return
	}
	if origUser != nil {
		logger.Error("The user already exists.")
		c.AbortWithError(http.StatusBadRequest, models.Error("The user already exists."))
		return
	}

	if user.Password == "" || !user.HashPassword() {
		logger.Error("The password is invalid.")
		c.AbortWithError(http.StatusBadRequest, models.Error("The password is invalid."))
		return
	}
	user.ID = bson.NewObjectId()
//	user.CreatedBy = bson.ObjectIdHex(middleware.GetUserID(c))

	if user.Role == models.RoleAdmin {
		// get valid company
		company := models.FindCompanyByID(db, user.CompanyID)
		if company == nil {
			logger.Error("The company id is invalid.")
			c.AbortWithError(http.StatusInternalServerError, models.Error("The company id is invalid."))
			return
		}
		user.CompanyName = company.CompanyName
	}

	//user.Profile = &models.UserProfile{10, 8, 1}
	if err := user.Create(db); err != nil {
		logger.Error("Create user failed .", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Create user failed:" , err))
		return
	}
	logger.Infof("User:%s create user:%s", middleware.GetUserID(c), user.UserName)

	c.Status(http.StatusCreated)
}
