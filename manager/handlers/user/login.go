package user

import (
	"encoding/json"
	"net/http"
	"strings"
	

	"github.com/spf13/viper"

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

func loginValidate(c *gin.Context) (*models.User) {
	var user models.User
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&user); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	user.UserName = strings.TrimSpace(user.UserName)
	if !models.RegexpUser.MatchString(user.UserName) {
		c.AbortWithError(http.StatusBadRequest, models.Error("username must be a valid name."))
		return nil
	}

	user.Password = strings.TrimSpace(user.Password)
	if !models.RegexpPwd.MatchString(user.Password) {
		c.AbortWithError(http.StatusBadRequest, models.Error("password must be a valid value."))
		return nil
	}

	return &user
}

// Login in with username and sha256 password
func Login(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	userReq := loginValidate(c)
	if userReq == nil {
		return
	}

	user, err := models.FindUserByUserName(db, userReq.UserName)
	if err != nil {
		logger.Error("Find user failed:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find user failed:" , err))
		return
	}
	if user == nil {
		logger.Error("The user name is invalid.")
		c.AbortWithError(http.StatusBadRequest, models.Error("The user was invalid."))
		return
	}

	if user.Role != models.RoleRoot && user.Role != models.RoleManager {
		logger.Error("You don't have permission to access login.")
		c.AbortWithError(http.StatusUnauthorized, models.Error("You don't have permission to access login."))
		return
	}

	if err := models.PasswordVerify(userReq.Password, user.HashedPassword); err != nil {
		logger.Error("Password verify failed.", err)
		c.AbortWithError(http.StatusBadRequest, models.Error("password is invalid:" + userReq.Password))
		return
	}

	if !user.CheckTokenValid() {
		token := models.NewToken(user.ID, viper.GetString("authentication.secret"))
		user.Token = token.String()
		user.Expiry = &token.Expiry

		if err = user.UpdateToken(db); err != nil {
			logger.Error("Update user token failed.", err)
			c.AbortWithError(http.StatusInternalServerError, models.Error("Update user token failed:" , err))
			return
		}
		logger.Infof(
			"User:%s update new token: %s in mongo.",
			user.UserName, user.Token)
	}

	c.Header(middleware.AuthenticationHeader, user.Token)
	c.JSON(http.StatusOK, user)
}
