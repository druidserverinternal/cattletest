package user

import (
	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"

	"net/http"
	

	"gopkg.in/mgo.v2/bson"
)

func getValidate(c *gin.Context) (string) {
	userID := c.Param("id")
	if !bson.IsObjectIdHex(userID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("User id is invalid:" + userID))
		return ""
	}

	return userID
}

// Get a any user
func Get(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	me := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if me.IsManager() && !me.CheckUserRead() {
		logger.Error("You did not have permissions to view user.", me)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to view user."))
		return
	}


	userID := getValidate(c)
	if userID == "" {
		return
	}

	user := models.FindUserByID(db, bson.ObjectIdHex(userID))
	if user == nil {
		logger.Error("Find user failed.")
		c.AbortWithError(http.StatusInternalServerError, models.Error("User cannot be found:" + userID))
		return
	}

	c.JSON(http.StatusOK, user)
}
