package user

import (
	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"

	"encoding/json"
	"strings"
	"net/http"
	

	"gopkg.in/mgo.v2/bson"
)

func updateInfoValidate(c *gin.Context) (*models.User) {
	var user models.User
	decoder := json.NewDecoder(c.Request.Body)

	if err := decoder.Decode(&user); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	user.Email = strings.TrimSpace(user.Email)
	if !models.RegexpEmail.MatchString(user.Email) {
		c.AbortWithError(http.StatusBadRequest, models.Error("email must be a valid value."))
		return nil
	}

	user.Phone = strings.TrimSpace(user.Phone)
	if !models.RegexpPhone.MatchString(user.Phone) {
		c.AbortWithError(http.StatusBadRequest, models.Error("phone must be a valid value."))
		return nil
	}

	return &user
}

// UpdateInfo myself info
func UpdateInfo(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	userReq := updateInfoValidate(c)
	if userReq == nil {
		return
	}

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	userReq.ID = user.ID

	if err := userReq.UpdateInfo(db); err != nil {
		logger.Error("Update info failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("User update info failed:" , err))
		return
	}
	logger.Infof("User:%s updated info.", middleware.GetUserID(c))

	c.Status(http.StatusCreated)
}
