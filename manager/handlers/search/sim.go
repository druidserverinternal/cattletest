package search

import (
//	"encoding/json"
	"net/http"
	"strconv"
	

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"

	"gopkg.in/mgo.v2/bson"
)

func SIM(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckSearchRead() {
		logger.Error("You did not have permissions to search.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to search."))
		return
	}

	search := deviceValidate(c)
	if search == nil {
		return
	}

	m := search.Bson()
	logger.Info("Search fileter:", m, search)
	result, err := models.SearchSIM(db, m, models.ParseRequestParameter(c))
	if err != nil {
		logger.Error("Search sim failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Search sim failed." , err))
		return
	}

	count := 0
	if len(result) > 0 {
		count, _ = models.SearchSIMCount(db, m)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, result)
}
