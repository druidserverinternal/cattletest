package search

import (
	
	"net/http"
	"strconv"

	
	"gopkg.in/mgo.v2/bson"

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

func statusValidate(c *gin.Context) (*models.Search) {
	search := models.Search{}

	if err := c.BindJSON(&search); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	return &search
}

// Status search
func Status(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	search := statusValidate(c)
	if search == nil {
		return
	}

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckSearchRead() {
		logger.Error("You did not have permissions to search.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to search."))
		return
	}

	m := search.Bson()
	result, err := models.SearchStatus(db, m, models.ParseRequestParameter(c))
	if err != nil {
		logger.Error("Can't find status.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Can't find status.", err))
		return
	}

	count := 0
	if len(result) > 0 {
		count, _ = models.SearchStatusCount(db, m)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, result)
}
