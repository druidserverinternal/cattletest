package search

import (
	
	"net/http"
	"strconv"

	
	"gopkg.in/mgo.v2/bson"

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

func GPS(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckSearchRead() {
		logger.Error("You did not have permissions to search.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to search."))
		return
	}

	search := deviceValidate(c)
	if search == nil {
		return
	}

	m := search.Bson()
	logger.Info("Search fileter:", m, search)
	gps, err := models.SearchGPS(db, m, models.ParseRequestParameter(c), nil)
	if err != nil {
		logger.Error("Search gps failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Search gps failed." , err))
		return
	}

	count := 0
	if len(gps) > 0 {
		count, _ = models.SearchGPSCount(db, m)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, gps)
}
