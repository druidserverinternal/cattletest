package search

import (
	"encoding/json"
	"net/http"
	"strconv"
	
	"gopkg.in/mgo.v2/bson"

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

func deviceValidate(c *gin.Context) (*models.Search) {
	search := models.Search{}

	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&search); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	return &search
}

// Device search
func Device(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckSearchRead() {
		logger.Error("You did not have permissions to search.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to search."))
		return
	}


	search := deviceValidate(c)
	if search == nil {
		return
	}

	m := search.Bson()
	logger.Info("Search fileter:", m)
	device, err := models.SearchDevice(db, m, models.ParseRequestParameter(c), nil)
	if err != nil {
		logger.Error("Search device failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Search device failed." , err))
		return
	}

	count := 0
	if len(device) > 0 {
		count, _ = models.SearchDeviceCount(db, m)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, device)
}
