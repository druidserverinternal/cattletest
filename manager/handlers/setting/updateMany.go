package setting

import (
	"net/http"
	"regexp"
	"strings"

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"

	"gopkg.in/mgo.v2/bson"
)

var regexpSpNumber = regexp.MustCompile(`^\+?[0-9]{12,13}$`)

func updateManyValidate(c *gin.Context) (*models.SettingMany) {
	settingMany := models.SettingMany{}
	if err := c.BindJSON(&settingMany); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	if settingMany.EnvSamplingMode != models.SamplingEnable && settingMany.EnvSamplingMode != models.SamplingDisable {
		c.AbortWithError(http.StatusBadRequest, models.Error("EnvSamplingMode is invalid.", settingMany.EnvSamplingMode))
		return nil
	}
	if settingMany.BehaviorSamplingMode != models.SamplingEnable && settingMany.BehaviorSamplingMode != models.SamplingDisable {
		c.AbortWithError(http.StatusBadRequest, models.Error("BehaviorSamplingMode is invalid.", settingMany.BehaviorSamplingMode))
		return nil
	}

	if settingMany.EnvSamplingFreq < models.EnvFreqMin || settingMany.EnvSamplingFreq > models.EnvFreqMax {
		c.AbortWithError(http.StatusBadRequest, models.Error("EnvSamplingFreq is invalid.", settingMany.EnvSamplingFreq))
		return nil
	}
	if settingMany.BehaviorSamplingFreq < models.BehFreqMin || settingMany.BehaviorSamplingFreq > models.BehFreqMax {
		c.AbortWithError(http.StatusBadRequest, models.Error("BehaviorSamplingFreq is invalid.", settingMany.BehaviorSamplingFreq))
		return nil
	}
	if settingMany.GprsFreq < models.GprsFreqMin || settingMany.GprsFreq > models.GprsFreqMax {
		c.AbortWithError(http.StatusBadRequest, models.Error("GprsFreq is invalid.", settingMany.GprsFreq))
		return nil
	}

	if settingMany.EnvVoltageThreshold < models.VoltageThresholdMin || settingMany.EnvVoltageThreshold > models.VoltageThresholdMax {
		c.AbortWithError(http.StatusBadRequest, models.Error("EnvVoltageThreshold is invalid.", settingMany.EnvVoltageThreshold))
		return nil
	}
	if settingMany.BehaviorVoltageThreshold < models.VoltageThresholdMin || settingMany.BehaviorVoltageThreshold > models.VoltageThresholdMax {
		c.AbortWithError(http.StatusBadRequest, models.Error("BehaviorVoltageThreshold is invalid.", settingMany.BehaviorVoltageThreshold))
		return nil
	}
	if settingMany.GprsVoltageThreshold < models.VoltageThresholdMin || settingMany.GprsVoltageThreshold > models.VoltageThresholdMax {
		c.AbortWithError(http.StatusBadRequest, models.Error("GprsVoltageThreshold is invalid.", settingMany.GprsVoltageThreshold))
		return nil
	}
	if settingMany.OtaVoltageThreshold < models.VoltageThresholdMin || settingMany.OtaVoltageThreshold > models.VoltageThresholdMax {
		c.AbortWithError(http.StatusBadRequest, models.Error("OtaVoltageThreshold is invalid.", settingMany.OtaVoltageThreshold))
		return nil
	}

	if settingMany.GprsType != models.GprsTypeInterval && settingMany.GprsType != models.GprsTypeTable {
		c.AbortWithError(http.StatusBadRequest, models.Error("GprsType is invalid.", settingMany.GprsType))
		return nil
	}

	if settingMany.GprsType == models.GprsTypeTable {
		if settingMany.GprsTimeTable == "000000000000000000000000" {
			c.AbortWithError(http.StatusBadRequest, models.Error("GprsTimeTable is zero.", settingMany.GprsTimeTable))
			return nil
		}
		tableReg := regexp.MustCompile("^[0-1]{24}$")
		if !tableReg.MatchString(settingMany.GprsTimeTable) {
			c.AbortWithError(http.StatusBadRequest, models.Error("GprsTimeTable is invalidd.",settingMany.GprsTimeTable))
			return nil
		}
	}

	if settingMany.GprsPowerSavingMode == models.SamplingEnable {
		if settingMany.GprsPowerSavingDistance < 10 || settingMany.GprsPowerSavingDistance > 10000  {
			c.AbortWithError(http.StatusBadRequest, models.Error("GprsPowerSavingDistance is invalidd.", settingMany.GprsPowerSavingDistance))
			return nil
		}
		if settingMany.GprsPowerSavingTime < 3600 || settingMany.GprsPowerSavingTime > 86400  {
			c.AbortWithError(http.StatusBadRequest, models.Error("GprsPowerSavingTime is invalidd.", settingMany.GprsPowerSavingTime))
			return nil
		}
	}

	settingMany.GprsMode = models.SamplingEnable 

	settingMany.SpNumber = strings.TrimSpace(settingMany.SpNumber)
	if !regexpSpNumber.MatchString(settingMany.SpNumber) {
		c.AbortWithError(http.StatusBadRequest, models.Error("SpNumber must be a valid sp number.", settingMany.SpNumber))
		return nil
	}

	return &settingMany
}

// UpdateMany update  many device setting
func UpdateMany(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckSettingWrite() {
		logger.Error("You did not have permissions to edit setting.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to edit setting."))
		return
	}

	settingMany := updateManyValidate(c)
	if settingMany == nil {
		return
	}

	count, err := models.CountSettingByFilter(db, bson.M{"device_id": bson.M{"$in": settingMany.Devices }})
	if err != nil || len(settingMany.Devices) != count {
		logger.Error("Could not find some setting")
		c.AbortWithError(http.StatusBadRequest, models.Error("Could not find some setting."))
		return
	}

	count, err = models.CountDeviceByFilter(db, bson.M{"_id": bson.M{"$in": settingMany.Devices }, "deleted_at": nil})
	if err != nil || len(settingMany.Devices) != count {
		logger.Error("Could not find some device")
		c.AbortWithError(http.StatusBadRequest, models.Error("Could not find some device."))
		return
	}

	if err = settingMany.Update(db, user.ID); err != nil {
		logger.Error("Update many setting failed:", err)
		c.AbortWithError(http.StatusBadRequest, models.Error("Update many setting failed:", err))
		return
	}

	c.Status(http.StatusCreated)
}
