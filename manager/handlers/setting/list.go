package setting

import (
	"net/http"
	"strconv"

	"gopkg.in/mgo.v2/bson"
	

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

// List devices setting
func List(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckSettingRead() {
		logger.Error("You did not have permissions to view setting.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to view setting."))
		return
	}

	result, err := models.ListValidSetting(db, models.ParseRequestParameter(c))
	if err != nil {
		logger.Error("List setting failed", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List setting failed.", err))
		return
	}

	count := 0
	if len(result) > 0 {
		count, _ = models.GetSettingValidCount(db)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, result)
}
