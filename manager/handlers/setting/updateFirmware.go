package setting

import (
	"encoding/json"
	"net/http"

	"gopkg.in/mgo.v2/bson"
	

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

func updateFirmwareValidate(c *gin.Context) (*models.SettingMany) {
	settingMany := models.SettingMany{}
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&settingMany); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	if settingMany.FirmwareID == "" {
		c.AbortWithError(http.StatusBadRequest, models.Error("firmware_id must be a valid value."))
		return nil
	}

	return &settingMany
}

// UpdateFirmware a settingMany
func UpdateFirmware(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckSettingWrite() {
		logger.Error("You did not have permissions to edit setting.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to edit setting."))
		return
	}

	settingMany := updateFirmwareValidate(c)
	if settingMany == nil {
		return
	}

	count, err := models.CountSettingByFilter(db, bson.M{"device_id": bson.M{"$in": settingMany.Devices }})
	if err != nil || len(settingMany.Devices) != count {
		logger.Error("Could not find some setting")
		c.AbortWithError(http.StatusBadRequest, models.Error("Could not find some setting."))
		return
	}

	firmware, _ := models.FindFirmwareByID(db, settingMany.FirmwareID)
	if firmware == nil {
		logger.Error("Firmware id is invalid.", settingMany.FirmwareID.Hex())
		c.AbortWithError(http.StatusBadRequest, models.Error("Firmware id is invalid: ", settingMany.FirmwareID))
		return
	}

	devices, err := models.CheckDevicesByManyID(db, settingMany.Devices, nil)
	if err != nil || len(settingMany.Devices) != len(devices) {
		logger.Error("Could not find some device")
		c.AbortWithError(http.StatusBadRequest, models.Error("Could not find some device."))
		return
	}

	for _, device :=range devices {
		if device.HardwareVersion != firmware.HardwareVersion {
			logger.Error("Hardware version not same:", device.Mark)
			c.AbortWithError(http.StatusBadRequest, models.Error("Hardware version not same:", device.Mark))
			return
		}

		if device.DeviceType == 101 && device.FirmwareVersion == 2 {
			// for lierda upgrade
			continue
		}

		if device.DeviceType != firmware.DeviceType {
			logger.Error("Device type not same:", device.Mark)
			c.AbortWithError(http.StatusBadRequest, models.Error("Device type not same:", device.Mark))
			return
		}

		if firmware.FirmwareVersion != 0 && device.FirmwareVersion >= firmware.FirmwareVersion {
			logger.Error("Device firmware version more than the firmware:", device.Mark)
			c.AbortWithError(http.StatusBadRequest, models.Error("Device firmware version more than the firmware:", device.Mark))
			return
		}
	}

	settingMany.FirmwareVersion = firmware.FirmwareVersion
	if err = settingMany.UpdateFirmware(db); err != nil {
		logger.Error("Update firmware failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Update firmware failed.", err))
		return
	}

	c.Status(http.StatusCreated)
}
