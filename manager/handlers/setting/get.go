package setting

import (
	//	"encoding/json"
	"net/http"
	//	"regexp"
	//	"strings"

	
	
	"gopkg.in/mgo.v2/bson"

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

// Get a device setting
func Get(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckSettingRead() {
		logger.Error("You did not have permissions to view setting.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to view setting."))
		return
	}

	id := c.Param("id")
	if !bson.IsObjectIdHex(id) {
		logger.Error("Device id is invalid.", id)
		c.AbortWithError(http.StatusBadRequest, models.Error("DeviceID is invalid.", id))
		return
	}

	setting, err := models.FindSettingByDeviceID(db, bson.ObjectIdHex(id))
	if err != nil {
		logger.Error("Find setting failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find device setting failed.", err))
		return
	}

	c.JSON(http.StatusOK, setting)
}
