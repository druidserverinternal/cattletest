package push

import (
	"net/http"

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/manager/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

func createValidate(c *gin.Context) (*models.PushSetting) {
	var pushSetting models.PushSetting
	if err := c.BindJSON(&pushSetting); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	if !pushSetting.CheckClass() {
		c.AbortWithError(http.StatusBadRequest, models.Error("Class is invalid.", pushSetting.Class))
		return nil
	}

	return &pushSetting
}

// CreateSetting a new pushSetting
func CreateSetting(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckSettingWrite() {
		logger.Error("You did not have permissions to edit setting.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to edit setting."))
		return
	}

	pushSetting := createValidate(c)
	if pushSetting == nil {
		return
	}

	ps, err := models.FindPushSettingByClass(db, pushSetting.Class)
	if err != nil {
		logger.Error("Find pushSetting failed:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find pushSetting failed:", err))
		return
	}
	if ps != nil {
		logger.Error("Pushsetting class was exists.")
		c.AbortWithError(http.StatusBadRequest, models.Error("Pushsetting class was exists."))
		return
	}

	if err := pushSetting.Create(db); err != nil {
		logger.Error("Create Pushsetting failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Create pushSetting failed:", err))
		return
	}

	logger.Infof("User:%s created pushSetting:%s", middleware.GetUserID(c), pushSetting)

	c.Status(http.StatusCreated)
}
