package push

import (
	"net/http"

	"gopkg.in/mgo.v2/bson"

	"druid/cattle/manager/middleware"
	"druid/cattle/models"

	"github.com/gin-gonic/gin"
)

func updateValidate(c *gin.Context) (*models.PushSetting) {
	var pushSetting models.PushSetting
	if err := c.BindJSON(&pushSetting); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	id := c.Param("id")
	if !bson.IsObjectIdHex(id) {
		c.AbortWithError(http.StatusBadRequest, models.Error("Request id is invalid.", id))
		return nil
	}
	pushSetting.ID = bson.ObjectIdHex(id)

	return &pushSetting
}

// UpdateSetting a new pushSetting
func UpdateSetting(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckSettingWrite() {
		logger.Error("You did not have permissions to edit setting.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to edit setting."))
		return
	}

	pushSetting := updateValidate(c)
	if pushSetting == nil {
		return
	}

	logger.Info("Request:.", pushSetting)
	if err := pushSetting.Update(db); err != nil {
		logger.Error("Update Pushsetting failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Update pushSetting failed:", err))
		return
	}

	c.Status(http.StatusCreated)
}
