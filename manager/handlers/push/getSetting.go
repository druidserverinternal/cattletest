package push

import (
	"net/http"
	
	"gopkg.in/mgo.v2/bson"

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

func getValidate(c *gin.Context) (string) {
	psID := c.Param("id")
	if !bson.IsObjectIdHex(psID) {
		c.AbortWithError(http.StatusBadRequest, models.Error("Push setting id is invalid,", psID))
		return ""
	}

	return psID
}

// GetSetting get a ps by id
func GetSetting(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	psID := getValidate(c)
	if psID == "" {
		return
	}

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckUserRead() {
		logger.Error("You did not have permissions to read push setting.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to read push setting."))
		return
	}

	ps, err := models.FindPushSettingByID(db, bson.ObjectIdHex(psID))
	if err != nil {
		logger.Warning("Find push setting failed:", err)
		c.AbortWithError(http.StatusForbidden, models.Error("Find pushSetting failed.", err))
		return
	}


	c.JSON(http.StatusOK, ps)
}
