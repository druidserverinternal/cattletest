package push

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"

	
	"gopkg.in/mgo.v2/bson"
)

// ListSetting list push setting
func ListSetting(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckUserRead() {
		logger.Error("You did not have permissions to read push setting.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to read push setting."))
		return
	}

	result, err := models.ListPushSetting(db, models.ParseRequestParameter(c))
	if err != nil {
		logger.Warning("List push setting failed.")
		c.AbortWithError(http.StatusInternalServerError, models.Error("List push setting failed."))
		return
	}

	count := 0
	if len(result) > 0 {
		count, _ = models.CountPushSetting(db)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, result)
}
