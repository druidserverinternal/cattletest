package manager

import (
	"net/http"
	

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

// GetSN get current sn status by type
func GetSN(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckPlatformRead() {
		logger.Error("You did not have permissions to view platform.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to view platform."))
		return
	}

	sn, err := models.FindSNByDeviceType(db, "biological")
	if err != nil {
		logger.Error("Find sn by device type failed:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find sn by device type failed."))
		return
	}

	c.JSON(http.StatusOK, sn)
}
