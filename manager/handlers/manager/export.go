package manager

import (
	"encoding/json"
	"net/http"
	"strings"
	"reflect"
	"time"
	
	"gopkg.in/mgo.v2/bson"

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

func getObj(o string) interface{} {
	switch o {
	case "gps":
		return &models.GPS{}
	case "device":
		return &models.Device{}
	case "behavior":
		return &models.Behavior2{}
	default:
		return nil
	}
}

func exportValidate(c *gin.Context) (*models.Export) {
	export := models.Export{}
	decoder := json.NewDecoder(c.Request.Body)
	if err := decoder.Decode(&export); err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to parse and decode the request.", err))
		return nil
	}

	if len(export.Field) == 0 {
		c.AbortWithError(http.StatusBadRequest, models.Error("Field must be a valid field."))
		return nil
	}

	export.Type = strings.TrimSpace(c.Param("type"))
	inter := getObj(export.Type)
	if inter == nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Type must be a valid type."))
		return nil
	}

	to := reflect.TypeOf(inter).Elem()
	for _, s := range export.Field {
		flag := 0
		for i := 0; i < to.NumField(); i++ {
			f := to.Field(i)
			if s == models.GetTag(f.Tag.Get("json")) {
				flag = 1
				break
			}
		}
		if flag == 0 {
			c.AbortWithError(http.StatusBadRequest, models.Error("Field must be a valid type."))
			return nil
		}
	}

	return &export
}

// ListDruidOld get all of owner druid old device
func Export(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckExportRead() {
		logger.Error("You did not have permissions to export.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to export."))
		return
	}

	export := exportValidate(c)
	if export == nil {
		return
	}

	rp := models.ParseRequestParameter(c)
	rp.Limit = 10000

	var inters interface{}
	m := export.Search.Bson()
	logger.Info("Search fileter:", m)
	switch export.Type {
		case "device":
			devices, err := models.SearchDevice(db, m, rp, nil)
			if err != nil {
				logger.Error("Search device failed.", err)
				c.AbortWithError(http.StatusInternalServerError, models.Error("Search device." , err))
				return
			}
			if len(devices) == 0 {
				logger.Error("Searched device zero reslut.")
				c.AbortWithError(http.StatusInternalServerError, models.Error("Searched device zero reslut."))
				return
			}
			inters = devices
		case "gps":
			gpss, err := models.SearchGPS(db, m, rp, nil)
			if err != nil {
				logger.Error("Search gps failed.", err)
				c.AbortWithError(http.StatusInternalServerError, models.Error("Search gps failed." , err))
				return
			}
			if len(gpss) == 0 {
				logger.Error("Searched gps zero reslut.")
				c.AbortWithError(http.StatusInternalServerError, models.Error("Searched gps zero reslut."))
				return
			}
			inters = gpss
		case "behavior":
			behaviors, err := models.SearchBehavior2(db, m, rp, nil)
			if err != nil {
				logger.Error("Search behavior failed.", err)
				c.AbortWithError(http.StatusInternalServerError, models.Error("Search behavior failed." , err))
				return
			}
			if len(behaviors) == 0 {
				logger.Error("Searched behavior zero reslut.")
				c.AbortWithError(http.StatusInternalServerError, models.Error("Searched behavior zero reslut."))
				return
			}
			inters = behaviors
		default:
			logger.Error("Unknown export type.", export.Type)
			c.AbortWithError(http.StatusBadRequest, models.Error("Unknown export type.", export.Type))
			return
	}

	/*
	csv, err := models.DataToCSV(inters)
	if err != nil {
		logger.Error("GPS convert to csv failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("GPS convert to csv failed.", err))
		return
	}
	*/

	data, err := models.FieldToExcel(inters, export.Field)
	if err != nil {
		logger.Error("Convert to excel failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Convert to excel failed.", err))
		return
	}

	/*
	data, err := models.WriteToZip(export.Type + ".csv", csv)
	if err != nil {
		logger.Error("Write to zip failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Write to zip failed.", err))
		return
	}
	*/


	name := export.Type + time.Now().Format("2006-01-02T15-04-05") + ".xlsx"
	c.Header("Content-Disposition", "attachment; filename=" + name)
	c.Data(http.StatusOK, "application/octet-stream", data)
}
