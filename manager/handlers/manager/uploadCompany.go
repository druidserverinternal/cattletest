package manager

import (
	"net/http"
	"strconv"
	"io/ioutil"
	
	"gopkg.in/mgo.v2/bson"

	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"
)

func uploadCompanyValidate(c *gin.Context) (bson.ObjectId, []byte) {
	id := c.Param("company_id")
	if !bson.IsObjectIdHex(id) {
		c.AbortWithError(http.StatusBadRequest, models.Error("Setting id is invalid:" + id))
		return "", nil
	}
	companyID := bson.ObjectIdHex(id)

	data, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, models.Error("Unable to read the request." , err))
		return "", nil
	}

	size, err := strconv.Atoi(c.Request.Header.Get("Content-Length"))
	if err != nil || len(data) != size {
		c.AbortWithError(http.StatusBadRequest, models.Error("Recv file length failed."))
		return "", nil
	}
	if size == 0 {
		c.AbortWithError(http.StatusBadRequest, models.Error("FileSize is invalid."))
		return "", nil
	}
	return companyID, data
}

// UploadCompany some devices
func UploadCompany(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckDeviceWrite() {
		logger.Error("You did not have permissions to edit device.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to edit device."))
		return
	}

	companyID, data := uploadCompanyValidate(c)
	if data == nil {
		return
	}

	ids, err := models.ExcelToDevice(data)
	if err != nil {
		logger.Error("Convert excel to struct failed:", err)
		c.AbortWithError(http.StatusBadRequest, models.Error("Convert excel to struct failed." , err))
		return
	}
	if len(ids) == 0 {
		logger.Error("Upload excel invalid.")
		c.AbortWithError(http.StatusBadRequest, models.Error("Upload excel is invalid."))
		return
	}

	devices, err := models.CheckDevicesByManyUUID(db, ids, nil)
	if err != nil || len(devices) != len(ids) {
		logger.Error("Check many devices by uuid failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Check many devices by uuid failed." , err))
		return
	}

	backup := []bson.ObjectId{}
	err = nil
	for _, device := range devices {
		backup = append(backup, device.ID)
	}

	companyName := models.GetCompanyNameByID(db, companyID)
	if companyName == "" {
		logger.Error("Company id is invalid.", companyID)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Company id is invalid:", companyID))
		return
	}

	deviceMany := models.DeviceMany{
		ID:	backup, 
		CompanyID:	companyID, 
		CompanyName:	companyName,
	}

	if err = deviceMany.UpdateDeviceCompany(db); err != nil {
		logger.Error("Update device company failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Update device company failed:" , err))
		return
	}

	c.Status(http.StatusCreated)
}
