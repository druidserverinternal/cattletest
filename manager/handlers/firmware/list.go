package firmware

import (
	"net/http"
	"strconv"
	

	"druid/cattle/manager/middleware"
	"druid/cattle/models"

	"gopkg.in/mgo.v2/bson"
	"github.com/gin-gonic/gin"
)

// List all of firmware file
func List(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckFirmwareRead() {
		logger.Error("You did not have permissions to view firmware.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to view firmware."))
		return
	}

	firmwares, err := models.ListFirmwares(db, models.ParseRequestParameter(c))
	if err != nil {
		logger.Error("List firmware failed:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("List firmware failed.", err))
		return
	}

	count := 0
	if len(firmwares) > 0 {
		count, _ = models.CountFirmware(db)
	}

	c.Header(models.ResultCountHeader, strconv.Itoa(count))
	c.JSON(http.StatusOK, firmwares)
}
