package firmware

import (
	"druid/cattle/manager/middleware"
	"druid/cattle/models"

	"net/http"
	
	"fmt"

	"gopkg.in/mgo.v2/bson"

	"github.com/gin-gonic/gin"
)

// UploadFirmware2 firmware
func Delete(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckFirmwareWrite() {
		logger.Error("You did not have permissions to edit firmware.", user)
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to edit firmware."))
		return
	}

	firmwareID := c.Param("id")
	if !bson.IsObjectIdHex(firmwareID) {
		logger.Error("Firmware id is invalid.", firmwareID)
		c.AbortWithError(http.StatusBadRequest, models.Error(fmt.Sprintf("Firmware id:%s is invalid", firmwareID)))
		return 
	}

	firmware, err := models.FindFirmwareByID(db, bson.ObjectIdHex(firmwareID)) 
	if err != nil {
		logger.Error("Find firmware failed by id:", firmwareID, err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find firmware failed by id:", firmwareID, err))
		return
	}

	if firmware == nil {
		logger.Error("Not found firmware:", firmwareID)
		c.AbortWithError(http.StatusBadRequest, models.Error("Not found firmware:", firmwareID))
		return
	}

	if err := models.DeleteFileByID(db, firmware.Point); err != nil {
		logger.Error("Delete file failed:", err)
		c.AbortWithError(http.StatusBadRequest, models.Error("Delete file failed:", err))
		return
	}

	if err := models.DeleteFirmwareByID(db, bson.ObjectIdHex(firmwareID)); err != nil {
		logger.Error("Delete firmware failed:", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Delete firmware failed.", err))
		return
	}

	c.Status(http.StatusNoContent)
}
