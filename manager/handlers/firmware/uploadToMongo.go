package firmware

import (
	"github.com/gin-gonic/gin"
	"druid/cattle/manager/middleware"
	"druid/cattle/models"

	
	
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"

	"gopkg.in/mgo.v2/bson"
)

func uploadToMongoValidate(c *gin.Context) (*models.Firmware) {
	firmware := models.Firmware{}

	fileName := c.Param("name")
	fileName = strings.TrimSpace(fileName)
	if fileName == "" {
		c.AbortWithError(http.StatusForbidden, models.Error("File name is invalid."))
		return nil
	}
	firmware.Name = fileName

	data, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.AbortWithError(http.StatusForbidden, models.Error("Read data from request failed.", err))
		return nil
	}

	size, err := strconv.Atoi(c.GetHeader("Content-Length"))
	if err != nil || len(data) != size {
		c.AbortWithError(http.StatusForbidden, models.Error("Recv file length failed.", err))
		return nil
	}
	if size == 0 {
		c.AbortWithError(http.StatusForbidden, models.Error("File length invalid.", err))
		return nil
	}
	firmware.Size = size
	firmware.Data = data

	return &firmware
}

// UploadFirmware2 firmware
func UploadToMongo(c *gin.Context) {
	db := middleware.GetDB(c)
	logger := middleware.GetLogger(c)

	firmware := uploadToMongoValidate(c)
	if firmware == nil {
		return
	}

	user := models.FindUserByID(db, bson.ObjectIdHex(middleware.GetUserID(c)))
	if user.IsManager() && !user.CheckFirmwareWrite() {
		logger.Error("You did not have permissions edit firmware.")
		c.AbortWithError(http.StatusForbidden, models.Error("You did not have permissions to edit firmware."))
		return
	}

	if err := firmware.ParseFirmware(firmware.Data); err != nil {
		logger.Error("Parse firmware failed :", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Parse firmware failed:", err))
		return
	}
	fv, err := firmware.FindFirmwareByVersion(db)
	if err != nil {
		logger.Error("Find firmware by version failed :", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Find firmware by version failed:", err))
		return
	}
	if fv != nil {
		logger.Error("Had exists firmware version.")
		c.AbortWithError(http.StatusInternalServerError, models.Error("Firmware had exists version."))
		return
	}


	file := &models.File{
		Data	:	firmware.Data,
		Size	:	firmware.Size,
		Name	:	firmware.Name,
	}
	if err := file.Upload(db); err != nil {
		logger.Error("Upload file failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Upload file failed.", err))
		return
	}

	firmware.Point = file.ID.(bson.ObjectId)
//	firmware.MD5 = file.MD5

	if err = firmware.Upload2(db); err != nil {
		logger.Error("Upload firmware failed.", err)
		c.AbortWithError(http.StatusInternalServerError, models.Error("Upload firmware failed.", err))
		return
	}

	c.JSON(http.StatusCreated, firmware)
}
