package middleware

import (
	"net"

	"github.com/spf13/viper"

	"github.com/gin-gonic/gin"
)


const PushConnectionKey = "PushConnection"

func PushConnection() gin.HandlerFunc {
	return func(c *gin.Context) {
		if GetPushConnection(c) == nil {
			logger := GetLogger(c)
			if address := viper.GetString("push_address"); address != "" {
//				local_addr, _ := net.ResolveUDPAddr("udp", address)
				conn, err := net.Dial("udp", address)
				if err != nil {
					logger.Error("Dial to push server failed, address:", address)
				} else {
					c.Set(PushConnectionKey, conn)
				}
			} else {
				logger.Error("Read push server address failed.")
			}

		}

		c.Next()
	}
}

func GetPushConnection(c *gin.Context) *net.Conn {
	if conn, ok := c.Get(PushConnectionKey); ok {
		return conn.(*net.Conn)
	}

	return nil
}
