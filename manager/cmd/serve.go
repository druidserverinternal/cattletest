package cmd

import (
	//	"io"
	//	"os"
	//	"syscall"
	//	"time"
	//	"net/http"

	"druid/cattle/manager/handlers"
	"druid/cattle/manager/middleware"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

const defaultAddress = ":8080"

// serveCmd represents the serve command
var serveCmd = &cobra.Command{
	Use:    "serve",
	Short:  "Start cattle server",
	PreRun: LoadConfiguration,
	Run:    serve,
}

func init() {
	RootCmd.AddCommand(serveCmd)

	serveCmd.Flags().StringP(
		"address",
		"a",
		defaultAddress,
		"Address the server binds to",
	)
	viper.BindPFlag("address", serveCmd.Flags().Lookup("address"))
}

func serve(cmd *cobra.Command, args []string) {
	middleware.LoggerConnInit()

	// Try to connect to the database
	if err := middleware.ConnectDB(
		viper.GetString("database"),
	); err != nil {
		log.Fatalln("connect to db: ", err)
	}

	address := viper.GetString("address")
	cert := viper.GetString("tls.cert")
	key := viper.GetString("tls.key")

	if cert != "" && key != "" {
		log.Infof("Starting cattle manager tls server on %s.", address)
		handlers.RootMux.RunTLS(address, cert, key)
	} else {
		log.Infof("Starting cattle manager server on %s.", address)
		handlers.RootMux.Run(address)
	}
}
